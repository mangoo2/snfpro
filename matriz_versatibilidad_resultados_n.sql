-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 16-02-2023 a las 23:50:39
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `snf_pro_1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matriz_versatibilidad_resultados_n`
--

DROP TABLE IF EXISTS `matriz_versatibilidad_resultados_n`;
CREATE TABLE IF NOT EXISTS `matriz_versatibilidad_resultados_n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idproyecto` int(11) NOT NULL,
  `codigo` varchar(30) NOT NULL,
  `row` int(11) NOT NULL,
  `tabla` longtext DEFAULT NULL,
  `charbar` longtext DEFAULT NULL,
  `charpie` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `matriz_versatibilidad_resultados_n`
--

INSERT INTO `matriz_versatibilidad_resultados_n` (`id`, `idproyecto`, `codigo`, `row`, `tabla`, `charbar`, `charpie`) VALUES
(1, 4, '20230216174743', 1, 'tabla_20230216174743_91.png', 'charbar_20230216174743_71.png', 'charpie_20230216174743_123.png'),
(2, 4, '20230216174743', 2, 'tabla_20230216174743_163.png', 'charbar_20230216174743_69.png', 'charpie_20230216174743_103.png'),
(3, 4, '20230216174743', 3, 'tabla_20230216174743_96.png', 'charbar_20230216174743_96.png', 'charpie_20230216174743_14.png');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
