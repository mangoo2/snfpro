var base_url=$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_data').DataTable();
	load();	
});
function load(){
	var proyecto=$('#id_proy').val();
	table.destroy();
	table=$("#table_data").DataTable({
    		//stateSave: true,
    		responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    "ajax": {
		       "url": base_url+"Matriz_versatibilidad/getDataTable",
		       type: "post",
		       "data": {
                	'proyecto':proyecto
            	},
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        //{"data": "rid","width": "10%","orderable":false},
		        {"data": "idmatriz"},
				{"data": "nombre"},
				{"data": "cumplimiento"},
				{"data": "objetivo"},
				{"data": "faltante"},
				{"data": "reg"},
				{"data": "fecha_update"},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		            	var html="";
		            		html+='<a href="'+base_url+'Matriz_versatibilidad/add/'+proyecto+'/'+row.idempleado+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
		            		html+='<a onclick="delete_data('+row.idmatriz+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></a>';
		            	return html;
		            }
		        },
		    ],
    		"order": [[ 0, "DESC" ]],
    		"lengthMenu": [[10,20,30, 50, 100], [10,20,30, 50, 100]],
    		
			createdRow: function (row, data, index) {
				//$( row ).find('td:eq(0)').addClass('td_colum_0');
				//$( row ).find('td:eq(9)').addClass('td_colum_9');
			}
  	}).on('draw',function(row){
  		
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        /*
        $('.img-thumbnail').click(function(event) {
        	var img=$(this).data('imagen');
        	console.log(img);
        	$('#modalimages').modal('show');
        	$('.imagemodalbody').html('<img src="'+base_url+'uploads/files_ar/'+img+'" style="max-width: -webkit-fill-available;">');

        });
        */
   	});
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Matriz_versatibilidad/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        load();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}