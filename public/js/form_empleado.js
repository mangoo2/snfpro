var base_url = $('#base_url').val();
$(document).ready(function() {
    $('#foto_avatar').change(function(){
	    document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
	});
	$('#foto_firma').change(function(){
	    document.getElementById('img_firma_empleado').src = window.URL.createObjectURL(this.files[0]);
	});
	$('#addPuesto').click(function(){
	    $("#modalPuestos").modal("show");
	    $("#id_puesto").val("0");
        $("#puesto_modal").val("");
	    getPuestos();
	});
	$('#savePuesto').click(function(){
	    savePuesto();
	});
});

function editPuesto(id,puesto){
    $("#id_puesto").val(id);
   	$("#puesto_modal").val(puesto);    
}

function getPuestos(){
    $.ajax({
        url:base_url+'Empleados/getPuestos',
        type:'POST',
        success: function(data) {
        	var trs=data;
            
            $("#table_puesto").dataTable({destroy:true,"paging": false,"searching": false,"info":false});
            $("#body_tablep").html(trs);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
        }
    });     
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Empleados/deletePuesto",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        $(".tr_puesto"+id).remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function getPuestosSelect(){
    $.ajax({
        url:base_url+'Empleados/getPuestosSelect',
        type:'POST',
        success: function(data) {
        	var ops=data;
            $("#puesto").html(ops);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
        }
    });     
}

function savePuesto(){
    $.ajax({
        url:base_url+'Empleados/savePuestos',
        type:'POST',
        data: { id:$("#id_puesto").val(), puesto:$("#puesto_modal").val() },
        success: function(data) {
            swal("Éxito", "Puesto guardado correctamente", "success");
            $("#id_puesto").val("0");
            $("#puesto_modal").val("");
            getPuestosSelect();
            $("#modalPuestos").modal("hide");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
        }
    });      
}

function reset_img(){
	$('.img_personal').html('<img id="img_avatar" src="'+base_url+'public/img/avatar.png" class="rounded mr-3" height="64" width="64">');
}

function reset_img_firma(){
	$('.img_firma').html('<img id="img_firma_empleado" src="'+base_url+'public/img/avatar.png" class="rounded mr-3" height="64" width="64">');
}

function add_form(){
	if($('#check_acceso').is(':checked')){
	    add_user();
	}else{
		var form_register = $('#form_data');
	    var error_register = $('.alert-danger', form_register);
	    var success_register = $('.alert-success', form_register);
	    var $validator1 = form_register.validate({
	        errorElement: 'div', //default input error message container
	        errorClass: 'vd_red', // default input error message class
	        focusInvalid: false, // do not focus the last invalid input
	        ignore: "",
	        rules: {
	            nombre: {
	                required: true
	            },
	            correo: {
	                required: true
	            },
	            celular:{
	              required: true
	            },
	            perfilId: {
		            required: true
		        },
		        genero: {
		            required: true
		        },
	        },
	        errorPlacement: function(error, element) {
	            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
	                element.parent().append(error);
	            } else if (element.parent().hasClass("vd_input-wrapper")) {
	                error.insertAfter(element.parent());
	            } else {
	                error.insertAfter(element);
	            }
	        },

	        invalidHandler: function(event, validator) { //display error alert on form submit              
	            success_register.fadeOut(500);
	            error_register.fadeIn(500);
	            scrollTo(form_register, -100);

	        },

	        highlight: function(element) { // hightlight error inputs

	            $(element).addClass('vd_bd-red');
	            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

	        },

	        unhighlight: function(element) { // revert the change dony by hightlight
	            $(element)
	                .closest('.control-group').removeClass('error'); // set error class to the control group
	        },

	        success: function(label, element) {
	            label
	                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
	                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
	            $(element).removeClass('vd_bd-red');
	        }
	    });

	    var $valid = $("#form_data").valid();

	    if ($valid) {
	        ////////////////////
	    	$('.btn_registro').attr('disabled',true);
	        var datos = form_register.serialize();
	        if($("#hombre").is(":checked")==true)
	        	genero=1;
	        else
	        	genero=2;
	        $.ajax({
	            type: 'POST',
	            url: base_url+'Empleados/guardar',
	            data: datos+"&genero="+genero,
	            statusCode: {
	                404: function(data){
	                    swal("Error!", "No Se encuentra el archivo", "error");
	                },
	                500: function(){
	                    swal("Error!", "500", "error");
	                }
	            },
	            success: function(data){
	            	$('#personalId').val(data);
	            	add_file(data);
	            	swal("Éxito", "Guardado Correctamente", "success");
	                setTimeout(function(){ 
	                    window.location = base_url+'Empleados';
	                }, 3000);
	            }
	        });         
	    }
	}
}

function add_user(){
	$.validator.addMethod("usuarioexiste", function(value, element) {
    var respuestav;
    $.ajax({
        type: 'POST',
        url: base_url + 'Empleados/validar',
        data: {
            Usuario: value
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                if ($('#UsuarioID').val() > 0) {
                    respuestav = true;
                } else {
                    respuestav = false;
                }

            } else {
                respuestav = true;
            }



        }
    });
    //console.log(respuestav);
    return respuestav;
	});

	var form_register2 = $('#form_data');
	var error_register = $('.alert-danger', form_register2);
	var success_register = $('.alert-success', form_register2);

	var $validator1 = form_register2.validate({
	    errorElement: 'div', //default input error message container
	    errorClass: 'vd_red', // default input error message class
	    focusInvalid: false, // do not focus the last invalid input
	    ignore: "",
	    rules: {
	    	nombre: {
                required: true
            },
            correo: {
                required: true
            },
            celular:{
              required: true
            },
	        genero: {
	            required: true
	        },
	        Usuario: {
	            usuarioexiste: true,
	            minlength: 3,
	            required: true
	        },
	        contrasena: {
	            minlength: 6,
	            required: true

	        },
	        contrasena2: {
	            equalTo: contrasena,
	            required: true
	        },
	        perfilId: {
	            required: true
	        },
	    },
	    messages: {
	        Usuario: {
	            usuarioexiste: 'Seleccione otro nombre de usuario'
	        }

	    },
	    errorPlacement: function(error, element) {
	        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
	            element.parent().append(error);
	        } else if (element.parent().hasClass("vd_input-wrapper")) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },

	    invalidHandler: function(event, validator) { //display error alert on form submit              
	        success_register.fadeOut(500);
	        error_register.fadeIn(500);
	        scrollTo(form_register2, -100);

	    },

	    highlight: function(element) { // hightlight error inputs

	        $(element).addClass('vd_bd-red');
	        $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

	    },

	    unhighlight: function(element) { // revert the change dony by hightlight
	        $(element)
	            .closest('.control-group').removeClass('error'); // set error class to the control group
	    },

	    success: function(label, element) {
	        label
	            .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
	            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
	        $(element).removeClass('vd_bd-red');
	    }
	});
	var $valid2 = $("#form_data").valid();
	if ($valid2) {
		
		$('.btn_registro').attr('disabled',true);
	    var datos = form_register2.serialize();
	    if($("#hombre").is(":checked")==true)
        	genero=1;
        else
        	genero=2;
        $.ajax({
            type: 'POST',
            url: base_url + 'Empleados/guardar',
            data: datos+"&genero="+genero,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            	var id_rg=data;
            	$('#personalId').val(data);
            	add_file(data);
            	add_file_firma(data);
            	swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Empleados';
                }, 3000);
                ///
                $.ajax({
		            type: 'POST',
		            url: base_url + 'Empleados/add_user',
		            data: {
		            	personalId:id_rg,
                        UsuarioID:$('#UsuarioID').val(),
                        perfilId:$('#perfilId').val(),
                        Usuario:$('#Usuario').val(),
                        contrasena:$('#contrasena').val(),
                        contrasena2:$('#contrasena2').val(),
		            },
		            statusCode: {
			            404: function(data){
		                    swal("Error!", "No Se encuentra el archivo", "error");
		                },
		                500: function(){
		                    swal("Error!", "500", "error");
		                }
		            },
		            success: function(data) {

		            }
		        });
                ///
            }
        });
	}
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
	//var name=$('#foto_avatar').val()
	extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	extensiones_permitidas = new Array(".jpeg",".png",".jpg");
	permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
	    for (var i = 0; i < extensiones_permitidas.length; i++) {
	       if (extensiones_permitidas[i] == extension) {
	       permitida = true;
	       break;
	       }
	    }  
	    if(permitida==true){
	      //console.log("tamaño permitido");
	        var inputFileImage = document.getElementById('foto_avatar');
	        var file = inputFileImage.files[0];
	        var data = new FormData();
	        data.append('foto',file);
	        data.append('id',id);
	        $.ajax({
	            url:base_url+'Empleados/cargafiles',
	            type:'POST',
	            contentType:false,
	            data:data,
	            processData:false,
	            cache:false,
	            success: function(data) {
	                var array = $.parseJSON(data);
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                var data = JSON.parse(jqXHR.responseText);
	            }
	        });
	    }
    }        
}

function btn_check_acceso(){
    if($('#check_acceso').is(':checked')){
        $('.text_usuario').css('display','block');
    }else{
        $('.text_usuario').css('display','none');
    }
}

function add_file_firma(id){
    var archivo=$('#foto_firma').val()
	extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	extensiones_permitidas = new Array(".jpeg",".png",".jpg");
	permitida = false;
    if($('#foto_firma')[0].files.length > 0) {
	    for (var i = 0; i < extensiones_permitidas.length; i++) {
	       if (extensiones_permitidas[i] == extension) {
	       permitida = true;
	       break;
	       }
	    }  
	    if(permitida==true){
	      //console.log("tamaño permitido");
	        var inputFileImage = document.getElementById('foto_firma');
	        var file = inputFileImage.files[0];
	        var data = new FormData();
	        data.append('foto',file);
	        data.append('id',id);
	        $.ajax({
	            url:base_url+'Empleados/cargafiles_firma',
	            type:'POST',
	            contentType:false,
	            data:data,
	            processData:false,
	            cache:false,
	            success: function(data) {
	                var array = $.parseJSON(data);
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                var data = JSON.parse(jqXHR.responseText);
	            }
	        });
	    }
    }        
}