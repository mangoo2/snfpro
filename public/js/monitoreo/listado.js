var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
    $("#tipo,#id_limpieza").on("change",function(){
        loadtable();
        changeFiltro();
    });
    $("#semana").on("change",function(){
        loadtable();
    });

    $("#edit_status").on("click",function(){
        cambiaEstatus();
    });

    $("#id_limpieza").select2({});
    
});

function changeFiltro(){
    $.ajax({
        type:'POST',
        url: base_url+"Monitoreo/cambiaFiltro",
        data: {
            tipo:$("#tipo option:selected").val(), id_limpieza:$("#id_limpieza option:selected").val()
        },
        success:function(response){  
            
        }
    });
}

function loadtable(){
    tabla=$("#table_activ").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        stateSave:true,
        "ajax": {
            "url": base_url+"Monitoreo/getDataTable",
            type: "post",
            data: { id_proy:$("#id_proy").val(),tipo:$("#tipo option:selected").val(),id_act:$("#id_limpieza option:selected").val(),semana:$("#semana option:selected").val() },
            error: function(){
               $("#table_activ").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"zona"},
            {"data":"inspeccion"},
            {"data":"nombre"},
            {"data":"frecuencia"},
            //{"data":"mes"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    //console.log("mes: "+row.mes);
                    //let mes_name = new Intl.DateTimeFormat('es-ES', { month: 'long'}).format(row.mes);
                    mes_name=viewmes(row.mes);
                    return mes_name;
                }
            },
            {"data":"fecha"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button title="Seguimiento" type="button" onclick="changeSeguimiento('+row.id+','+row.seguimiento+',\'' + row.observaciones + '\',\'' + row.inspeccion + '\')"><i class="fa fa-gear"></i></button>\
                <button type="button" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<a href="'+base_url+'Monitoreo/registro/'+row.id+'/'+$("#id_proy").val()+'/'+$("#id_cli").val()+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Monitoreo/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function viewmes(mes){
    var mesl='';
    switch (mes) {
        case '01':
            mesl='Enero';
        break;
        case '02':
            mesl='Febrero';
        break;
        case '03':
            mesl='Marzo';
        break;
        case '04':
            mesl='Abril';
        break;
        case '05':
            mesl='Mayo';
        break;
        case '06':
            mesl='Junio';
        break;
        case '07':
            mesl='Julio';
        break;
        case '08':
            mesl='Agosto';
        break;
        case '09':
            mesl='Septiembre';
        break;
        case '10':
            mesl='Octubre';
        break;
        case '11':
            mesl='Noviembre';
        break;
        case '12':
            mesl='Diciembre';
        break;
    }
    return mesl;
}

function changeSeguimiento(id,status,obs,insp){
    $("#modalEdit").modal("show");
    $("#id_mon").val(id);
    $("#estatus").val(status);
    $("#observaciones").val(obs);
    $("#inspeccion").val(insp);
}

function cambiaEstatus(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de cambiar el estatus?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Monitoreo/cambiaSeguimiento",
                    data: {
                        id:$("#id_mon").val(), estatus:$("#estatus option:selected").val(), observaciones:$("#observaciones").val(),inspeccion:$("#inspeccion").val()
                    },
                    success:function(response){  
                        loadtable();
                        $("#modalEdit").modal("hide");
                        swal("Éxito", "Se ha cambiado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });   
}

function excel_exportar(){
    location.href= base_url+'Monitoreo/excel_monitoreo/'+$("#id_proy").val()+'/'+$("#tipo option:selected").val()+'/'+$("#id_limpieza option:selected").val();
}
