var base_url = $('#base_url').val();
let cont_emp=0;
let cont_emp_sup=0;
$(document).ready(function(){
    $("#btn_sub").on("click",function(){
        save();
    });

    $("#id_limpieza").on("change",function(){
        changeActiv();
    });
    if($("#id").val()=="0"){
        changeActiv();
    }
    
    if($("#id").val()>0){
        $("#fecha").val($("#fecha_aux").val());
        /*jQuery('#fecha').datepicker({
            //datetimePicker.Value: $("#fecha_aux").val(),
            defaultDate: $("#fecha_aux").val()
        }).datepicker("setDate", $("#fecha_aux").val());*/
        
    }
    $("#id_limpieza").on("click",function(){
        changeActiv();
    });
    /*jQuery('#fecha').datepicker({
        //minDate: new Date(2022, 12, 1),
        //maxDate: new Date(2022, 12, 31),
        //dateFormat: 'DD, MM, d, yy',
        constrainInput: true,
        //beforeShowDay: noWeekendsOrHolidays
        beforeShowDay: deshabilitar
    });*/
    $("#id_limpieza").select2({});

    $('#foto').change(function(){
        document.getElementById('img_foto').src = window.URL.createObjectURL(this.files[0]);
    });

    $("#tipo1,#tipo2").on("change",function(){
        //showFrec();
        //showDays();
    });
    //showDays();
});

function showFrec(){
    if($("#tipo1").is(":checked")){//l-s
        $(".d").show();
    }else{
        $(".ds").show();
        $(".d").hide();
    }
}

function showDays(){
    if($("#tipo1").is(":checked")){//l-s
        $(".di").show();
        $(".fin").hide();
    }else{
        $(".fin").show();
        $(".di").hide();
    }
}

function reset_img(){
    
    $.ajax({
        type: 'POST',
        url : base_url+'Monitoreo/deleteFoto',
        data: { id: $("#id").val(), foto: $("#name_foto").val() },
        async: false,
        success: function(data){
            $('.img_foto_cont').html('<img id="img_foto" src="'+base_url+'public/img/no-image.png" class="rounded mr-3 img-fluid" height="142" width="142">');
        }
    }); 
}

var QuitarFechas = ["6/8/2022","17/8/2022","02/11/2022","25/12/2022","31/12/2022"];
        
function deshabilitar(date) {
    hoy = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear();
    if ($.inArray(hoy, QuitarFechas) < 0) {return $.datepicker.noWeekends (date)} 
    else {return [false,"","Fecha deshabilitada"];}
}

function changeActiv(){
    let zona = $("#id_limpieza option:selected").data("zona");
    let frecuencia = $("#id_limpieza option:selected").data("frecuencia");
    //console.log("zona: "+zona);
    $("#zona").val(zona);
    $("#frecuencia").val(frecuencia);
}

function save(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input  
        ignore: "",  
        rules: {
            /*tipo:{
              required: true
            },*/
            id_limpieza:{
              required: true
            },
            zona:{
              required: true
            },
            inspeccion:{
              required: true
            },
            frecuencia_tipo:{
              required: true
            },
            frecuencia:{
              required: true
            },
            num_inspeccion:{
              required: true
            },
            fecha:{
              required: true
            },
            fecha_fin:{
              required: true
            },
            /*dia_activ:{
                required:true
            }*/
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio") || element.is(":checkbox")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_data").valid();
    var dia_activ = $("#dia_activ").val();
    //console.log("dia_activ: "+dia_activ);
    if(dia_activ.length==0){
        swal("Álerta!", "Se debe elegir al menos un día de la semana ", "warning");  
    }
    if(valid && dia_activ.length>0 && $("#id").val()==0) {
        var band_ot=0;
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Desea crear replica de puntos de inspección con este registro?',
            type: 'blue',
            typeAnimated: true,
            //Si: 'btn-confim',
            buttons:{
                Si: function (){
                    //$(this).addClass("btn-confim");
                    //$(this).find("button").addClass("btn-confim");
                    quitar(this);
                    saveConfirm(1);
                },
                No: function () 
                {
                    saveConfirm(0);
                }
            }
        }); 
    }else if(valid && dia_activ.length>0 && $("#id").val()>0){
        saveConfirm(0);
    }
}

function quitar(elemento){
    //console.log("disabled");
    $(".jconfirm-open").hide();
    $(elemento).attr('disabled',true);   
}

function saveConfirm(band){
    /*if($("#tipo1").is(":checked")==true)
        tipo=0;
    else if($("#tipo2").is(":checked")==true)
        tipo=1;*/
    tipo=0;
    let fecha = $("#fecha").val();
    if($("#fecha").val()!=""){
        var form_register = $('#form_data');
        $.ajax({
            data: form_register.serialize()+"&tipo="+tipo+"&fecha="+fecha+"&band="+band,
            type: 'POST',
            url : base_url+'Monitoreo/submit',
            async: false,
            beforeSend: function(){
                $("#btn_sub").attr("disabled",true);
                $(".btn-default").attr("disabled",true);
            },
            success: function(data){
                let id=data;
                swal("¡Éxito!", "Guardado correctamente", "success");
                setTimeout(function(){ 
                    add_file(id,"foto");
                }, 500);
                setTimeout(function () { 
                    window.location = base_url+'Monitoreo/listado/'+$("#id_proy").val()+"/"+$("#id_cli").val();
                }, 1500);
            }
        }); 
    }else{
        swal("Error!", "Elija una fecha", "warning");
    }
}

function add_file(id,name){
    var archivo=$('#'+name+'').val();
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg","webp");
    permitida = false;
    if($('#'+name+'')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById(name);
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            data.append('name',name);
            $.ajax({
                url:base_url+'Monitoreo/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}