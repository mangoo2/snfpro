var base_url = $('#base_url').val();
let cont_emp=0;
let cont_emp_sup=0;
let cont_p=0;
$(document).ready(function(){
    $('#id_cliente').select2({
        width: '95%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione un cliente',
        ajax: {
            url: base_url+'Proyectos/searchClientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    $('#id_servicio').select2({
        width: '95%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione un servicio',
        ajax: {
            url: base_url+'Proyectos/searchServicios',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.servicio+" / "+element.descripcion
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

	$('#id_empleado').select2({
		width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
		placeholder: 'Seleccione un empleado',
		//allowClear: true,
		//sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)),
		ajax: {
            url: base_url+'Proyectos/searchPersonal',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public',
                    tipo:2,
                    id_cli: $('#id_cliente option:selected').val()
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    $('#id_super').select2({
        width: '90%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione un empleado',
        ajax: {
            url: base_url+'Proyectos/searchPersonal',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public',
                    tipo: 1,
                    id_cli: $('#id_cliente option:selected').val()
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    $("#add_all_emp").on("click",function(){
        addAllEmp(); 
    });

    $("#add_emp").on("click",function(){
        if($("#id_empleado option:selected").val()!=undefined){
            cont_emp++;
            addEmp(cont_emp,1); 
        }
    });
    $("#add_empsup").on("click",function(){
        if($("#id_super option:selected").val()!=undefined){
            cont_emp_sup++;
            addEmp(cont_emp_sup,2); 
        }
    });

    $("#btn_sub").on("click",function(){
        save();
    });

    if($("#id").val()!='0'){
        getEmpleados();
    }

    $("#tipo").on("click",function(){
        showPuesto();
    });

    $("#add_puesto").on("click",function(){
        cont_p++;
        addPuesto(cont_p);
    });
    showPuesto();
});

function showPuesto(){
    if($("#tipo").is(":checked"))
        $("#cont_rotacion").show("slow");
    else
        $("#cont_rotacion").hide("slow");
}

function getEmpleados(){
    /*$.ajax({
        type:'POST',
        url: base_url+"Proyectos/getEmpleadosProyecto",
        data: { id:$("#id").val()},
        async:false,
        success: function (response){
            if(tipo==1)
                $(".body_emp").append(html);
            else
               $(".body_emp_sup").append(html);
        },
    });*/
}

function save(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input  
        ignore: "",  
        rules: {
            id_cliente:{
              required: true
            },
            id_servicio:{
              required: true
            },
            fecha_ini:{
              required: true
            },
            fecha_fin:{
              required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_data").valid();
    if(valid) {
        let turno=0; let turno2=0; let turno3=0;
        if($("#turno1").is(":checked")==true)
            turno=1;
        if($("#turno2").is(":checked")==true)
            turno2=1;
        if($("#turno3").is(":checked")==true)
            turno3=1;

        $.ajax({
            data: form_register.serialize()+"&turno="+turno+"&turno2="+turno2+"&turno3="+turno3,
            type: 'POST',
            url : base_url+'Proyectos/submit',
            async: false,
            beforeSend: function(){
                $("#btn_sub").attr("disabled",true);
             },
            success: function(data){
                let id=data;
                guardarEmpleados(id);
                swal("¡Éxito!", "Guardado correctamente", "success");
                setTimeout(function () { 
                    window.location = base_url+'Proyectos';
                }, 2500);
            }
        }); 
    }
}

function addAllEmp(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de agregar empleados participantes?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Proyectos/getAllEmpleados",
                    data: { id_cli: $('#id_cliente option:selected').val(), id: $('#id').val() },
                    success:function(response){
                        $(".body_emp").append(response); 
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function addEmp(cont,tipo){
    let html=""; let btn=""; let class_row=""; var repetido=0;
    //console.log("tipo: "+tipo);
    if(tipo==1){
        id_emp=$("#id_empleado option:selected").val();
        txt_emp=$("#id_empleado option:selected").text();
        //console.log("id_emp: "+id_emp);
        let TABLA = $("#tabla_emp tbody > tr");
        TABLA.each(function(){         
            id_emp_table = $(this).find("input[id*='id']").data("id_emp");
            //console.log("id_emp_table: "+id_emp_table);
            if(id_emp==id_emp_table)
                repetido++;
        });
        if(repetido==0){
            btn='<button onclick="eliminaRow('+cont+','+tipo+')" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
            class_row="row_emp_"+cont+"";
            $("#id_empleado").val(0).change();
        }
    }
    else{
        id_emp=$("#id_super option:selected").val();
        txt_emp=$("#id_super option:selected").text();
        let TABLA = $("#tabla_emp_sup tbody > tr");
        TABLA.each(function(){         
            id_emp_table = $(this).find("input[id*='id']").data("id_emp");
            if(id_emp==id_emp_table)
                repetido++;
        });
        if(repetido==0){
            btn='<button onclick="eliminaRow('+cont+','+tipo+')" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
            class_row="row_emp_sup_"+cont+"";
            $("#id_super").val(0).change();
        }
    }
    //console.log("repetido: "+repetido);
    if(repetido==0){
        html='<tr class="'+class_row+'">\
                <td>'+cont+'<input id="id" type="hidden" value="0" data-id_emp="'+id_emp+'"></td>\
                <td><input id="id_emp" type="hidden" value="'+id_emp+'">'+txt_emp+'</td>\
                <td><input class="form-control" id="fecha_asignacion" type="date" value=""></td>\
                <td>'+btn+'</td>\
            </tr>';

        if(tipo==1)
            $(".body_emp").append(html);
        else
           $(".body_emp_sup").append(html);
    }else{
        swal("Error!", "Empleado ya asignado previamente", "error");
        if(tipo==1)
            $("#id_empleado").val(0).change();
        else
           $("#id_super").val(0).change();
    }
}

function eliminaEmple(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Proyectos/deleteEmpleado",
                    data: {
                        id:id
                    },
                    success:function(response){
                        if(tipo==1)  
                            $("#idrow_emp_"+id+"").remove();
                        else
                            $("#idrow_emp_sup_"+id+"").remove();

                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function guardarEmpleados(id_proy){
	let DATAr = [];
    let TABLA = $("#tabla_emp tbody > tr");
        TABLA.each(function(){         
        item = {};
        item["id"] = $(this).find("input[id*='id']").val();
        item["id_empleado"] = $(this).find("input[id*='id_emp']").val();
        item["fecha_asignacion"] = $(this).find("input[id*='fecha_asignacion']").val();
        DATAr.push(item);
    });
    array_emp = JSON.stringify(DATAr);
    //console.log("array_emp: "+array_emp);
    let DATAES = [];
    let TABLA2 = $("#tabla_emp_sup tbody > tr");
        TABLA2.each(function(){         
        item2 = {};
        item2["id"] = $(this).find("input[id*='id']").val();
        item2["id_empleado"] = $(this).find("input[id*='id_emp']").val();
        item2["fecha_asignacion"] = $(this).find("input[id*='fecha_asignacion']").val();
        DATAES.push(item2);
    });
    array_empsup = JSON.stringify(DATAES);
    //console.log("array_empsup: "+array_empsup);

    let DATAPUES = [];
    let TABLA3 = $("#tabla_puestos tbody > tr");
        TABLA3.each(function(){         
        item3 = {};
        item3["id"] = $(this).find("input[id*='id']").val();
        item3["puesto"] = $(this).find("input[id*='puesto']").val();
        item3["objetivo"] = $(this).find("input[id*='objetivo']").val();
        item3["dias_laborables"] = $(this).find("input[id*='dias_laborables']").val();
        DATAPUES.push(item3);
    });
    array_pues = JSON.stringify(DATAPUES);

    var datos = 'id_proyecto='+id_proy+'&array_emp='+array_emp+'&array_empsup='+array_empsup;
    if(TABLA.length>0 || TABLA2.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Proyectos/submitEmpleadosProyecto",
            data: datos,
            async:false,
            success: function (response){

            },
            error: function(response){
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
            }
        });
    }else{
        swal("Error!", "Elija como mínimo 1 empleado", "error")
    }
    if(TABLA3.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Proyectos/submitPuestosRota",
            data: "id_proyecto="+id_proy+"&array_pues="+array_pues,
            async:false,
            success: function (response){

            },
            error: function(response){
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
            }
        });
    }
    
}

/* *********************************** */
function addPuesto(contp){
    let html=""; let btn=""; let class_row="";
        btn='<button onclick="eliminaRow('+contp+',3)" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
        class_row="row_puesto"+contp+"";

    if($(".puesto_pri").val()!="" && $(".objetivo_pri").val()!="")
    {
        html='<tr class="'+class_row+'">\
                <td>'+contp+'<input id="id" type="hidden" value="0"></td>\
                <td><input id="puesto" type="hidden" value="'+$(".puesto_pri option:selected").val()+'">'+$(".puesto_pri option:selected").text()+'</td>\
                <td><input id="objetivo" type="hidden" value="'+$(".objetivo_pri").val()+'">'+$(".objetivo_pri").val()+'</td>\
                <td><input id="dias_laborables" type="hidden" value="'+$(".dias_laborables_pri").val()+'">'+$(".dias_laborables_pri").val()+'</td>\
                <td>'+btn+'</td>\
            </tr>';
        $(".body_puestos").append(html); 
        $(".puesto_pri").val(""); $(".objetivo_pri").val("");
    }else{
        swal("Álerta!", "LLene los campos de puesto y/o objetivo", "warning");
    }
}

function eliminaPuesto(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Proyectos/deletePuestoRotacion",
                    data: {
                        id:id
                    },
                    success:function(response){
                        $("#idrow_puesto_"+id+"").remove();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function eliminaRow(row,tipo){
    if(tipo==1){
        $(".row_emp_"+row+"").remove();
        cont_emp--;
    }
    else if(tipo==2){
        $(".row_emp_sup_"+row+"").remove();
        cont_emp_sup--;
    }else if(tipo==3){
        $(".row_puesto"+row+"").remove();
        cont_p--;
    }
}