var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
});

function loadtable(){
    tabla=$("#table_proyectos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        responsive: !0,
        columnDefs: [ {
            sortable: false,
            "class": "index",
            targets: 0
        } ],
        "ajax": {
            "url": base_url+"Proyectos/getDataTable",
            type: "post",
            error: function(){
               $("#table_proyectos").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"empresa"},
            {"data":"servicio"},
            {"data":"fecha_ini"},
            {"data":"fecha_fin"},
            {"data":"Usuario"},
            {"data":"reg"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" onclick="ejecucion('+row.id+','+row.id_cliente+')">Ejecución</button>\
                <button type="button" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<a href="'+base_url+'Proyectos/registro/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        //"order": [[ 0, "desc" ]],
        "lengthMenu": [[50, 75, 100], [50, 75, 100]],
        language: languageTables
    });
    tabla.on( 'order.dt search.dt', function () {
        tabla.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}

function ejecucion(id,id_cli){
    window.location = base_url+'Proyectos/ejecucion/'+id+'/'+id_cli;
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Proyectos/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}