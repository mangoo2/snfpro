var base_url = $('#base_url').val();
var tabla;
var idactividad=0;
$(document).ready(function() {
    loadtable();

    $("#edit_status").on("click",function(){
        cambiaEstatus();
    });
    $("#upload_pre").on("click",function(){
        modalCarga();
    });
    $("#aceptar_carga").on("click",function(){
        if($("#inputFile").val()!="")
            $('#inputFile').fileinput('upload');
        else
            swal("Error", "Seleccione un documento a cargar", "warning");
    });
    $("#files").fileinput({
        showCaption: false,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["docx","xlsx","doc","xls","pdf","pptx"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Actividades/actividades_documentos',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    actividad:idactividad
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(idactividad);
    });

    $("#critica").on("change",function(){
        loadtable();
    });
});

function loadtable(){
    //$('#table_activ thead tr').clone(true).addClass('filters').appendTo( '#table_activ thead' );
    tabla=$("#table_activ").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        responsive: !0,
        /*orderCellsTop: true,
        fixedHeader: true,
        initComplete: function () {
            var api = this.api();
            // For each column
            api
            .columns()
            .eq(0)
            .each(function (colIdx) {
                // Set the header cell to contain the input element
                var cell = $('.filters th').eq(
                    $(api.column(colIdx).header()).index()
                );
                var title = $(cell).text();
                $(cell).html('<input type="text" placeholder="' + title + '" />');
                var cursorPosition = this.selectionStart;
                // On every keypress in this input
                $(
                    'input',
                    $('.filters th').eq($(api.column(colIdx).header()).index())
                )
                    .off('keyup change')
                    .on('change', function (e) {
                        // Get the search value
                        //console.log("cambia dato");
                        $(this).attr('title', $(this).val());
                        //var regexr = '({search})'; //$(this).parents('th').find('select').val();
                        var regexr = $(this).val();
                        //console.log("busca el  dato: "+regexr);
                        var cursorPosition = this.selectionStart;
                        // Search the column for that value
                        //console.log("colIdx: "+colIdx);
                        api
                            .column(colIdx)
                            .search(
                                this.value != ''
                                    ? regexr.replace('{search}', '(((' + this.value + ')))')
                                    : '',
                                this.value != '',
                                this.value == ''
                            )
                            .draw();
                            //console.log("regexr: "+regexr);
                    })
                    .on('keyup', function (e) {
                        e.stopPropagation();

                        $(this).trigger('change');
                        $(this)
                            .focus()[0];
                            //.setSelectionRange(cursorPosition, cursorPosition);
                    });
            });
        }, */
        "ajax": {
            "url": base_url+"Actividades/getDataTable",
            type: "post",
            data: { id_proy:$("#id_proy").val(), id_cliente:$("#id_cli").val(), critica: $("#critica option:selected").val()},
            error: function(){
               $("#table_activ").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"zona"},
            {"data":"nombre"},
            {"data":"mp"},
            //{"data":"frecuencia_cotiza"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.frecuencia_cotiza==1)
                        html="Semanal";
                    else if(row.frecuencia_cotiza==2)
                        html="Quincenal";
                    else if(row.frecuencia_cotiza==3)
                        html="Mensual";
                    else if(row.frecuencia_cotiza==4)
                        html="Bimestral";
                    else if(row.frecuencia_cotiza==5)
                        html="Anual";

                    return html;
                }
            },
            /*{"data":"frecuencia_modi1"},
            {"data":"frecuencia_modi2"},
            {"data":"propuesta"},*/
            {"data":"recursos"},
            {"data":"tiempo"},
            {"data":"horas_hombre"},
            {"data":"days"},
            {"data":"grupo"},
            {"data":"fecha_ini"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button title="Seguimiento" type="button" onclick="changeSeguimiento('+row.id+','+row.seguimiento+',\'' + row.fecha_realiza + '\')"><i class="fa fa-gear"></i></button>';
                html+='<button type="button" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<a  href="'+base_url+'Actividades/registro/'+row.id+'/'+$("#id_proy").val()+'/'+$("#id_cli").val()+'"><button type="button"><i class="fa fa-edit"></i></button></a>';
                html+='<button type="button" onclick="upload_data('+row.id+')"><i class="fa fa-upload"></i></button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function changeSeguimiento(id,seg,fecha){
    $("#modalEdit").modal("show");
    $("#id_activ").val(id);
    $("#fecha_realiza").val(fecha);
}

function modalCarga(){
    $("#modalCarga").modal("show");
    $("#inputFile").fileinput({
        showCaption: false,
        showUpload: false,// quita el boton de upload
        showRemove:false,
        //rtl: true,
        allowedFileExtensions: ["csv"],
        browseLabel: 'Seleccionar Archivo',
        uploadUrl: base_url+'Actividades/guardar_actividades_malla',
        maxFilePreviewSize: 6000,
        previewFileIconSettings: {
            'csv': '<i class="fa fa-file-excel-o text-success"></i>',
        },
        uploadExtraData: function (previewId, index) {
            var info = {
                        id_proy:$('#id_proy').val(), id_cli:$('#id_cli').val(), genera_orden:$("#genera_orden option:selected").val()
                    };
            return info;
        }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
        $("#modalCarga").modal("hide");
        swal("Éxito", "Carga de datos realizada correctamente", "success");
        loadtable();
    });
}

function cambiaEstatus(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de cambiar el estatus?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Actividades/cambiaSeguimiento",
                    data: {
                        id:$("#id_activ").val(), estatus:$("#estatus option:selected").val(), fecha_realiza:$("#fecha_realiza").val()
                    },
                    success:function(response){  
                        loadtable();
                        $("#modalEdit").modal("hide");
                        swal("Éxito", "Se ha cambiado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });   
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Actividades/delete",
                    data: {
                        id:id, tabla:"limpieza_actividades"
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function upload_data(id){
    $('#files').fileinput('clear');
    idactividad=id;
    $("#modalfiles").modal('show');
    upload_data_view(id);
}
function upload_data_view(id){
    $.ajax({
        type:'POST',
        url: base_url+"Actividades/upload_data_view",
        data: {
            id:id
        },
        success:function(response){  
            $('.tbody_view_files').html(response);
        }
    });
}
function delete_files(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este documento?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Actividades/delete_files",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        upload_data_view(idactividad);
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}