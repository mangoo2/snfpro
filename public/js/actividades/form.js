var base_url = $('#base_url').val();
let cont_emp=0;
let cont_emp_sup=0;
$(document).ready(function(){
    $("#btn_sub").on("click",function(){
        save();
    });
});

function save(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input  
        ignore: "",  
        rules: {
            tipo:{
              required: true
            },
            nombre:{
              required: true
            },
            zona:{
              required: true
            },
            frecuencia_cotiza:{
              required: true
            },
            /*frecuencia_modi1:{
              required: true
            },
            frecuencia_modi2:{
              required: true
            },
            propuesta:{
              required: true
            },*/
            recursos:{
              required: true
            },
            tiempo:{
              required: true
            },
            horas_hombre:{
              required: true
            },
            days:{
              required: true
            },
            grupo:{
              required: true
            },
            fecha_ini:{
              required: true
            },
            fecha_fin:{
              required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_data").valid();
    if(valid) {
        var band_ot=0;
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Desea crear ordenes de trabajo al editar esta actividad?',
            type: 'blue',
            typeAnimated: true,
            
            buttons:{
                Si: function (){
                    saveConfirm(1);
                },
                No: function () 
                {
                    saveConfirm(0);
                }
            }
        }); 
        
    }
}

function saveConfirm(band_ot) {
    band_mac=0;
    /*if($("#critica").is(":checked")==true){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Desea crear registro de monitoreo de área crítica?',
            type: 'blue',
            typeAnimated: true,
            
            buttons:{
                Si: function (){
                    saveAactividad(band_ot,1);
                },
                No: function () 
                {
                    saveAactividad(band_ot,0);
                }
            }
        }); 
    }else{
        saveAactividad(band_ot,0);
    }*/
    saveAactividad(band_ot,0); //se comenta lo de arriba, y se manda en 0 para que no guarde el replicado de monitoreo
}


function saveAactividad(band_ot,band_mac) {
    //console.log("band_ot: "+band_ot);
    if($("#tipo1").is(":checked")==true)
        tipo=0;
    else if($("#tipo2").is(":checked")==true)
        tipo=1;

    var critica=0;
    if($("#critica").is(":checked")==true)
        critica=1; 
    var form_register = $('#form_data');
    $.ajax({
        data: form_register.serialize()+"&tabla=limpieza_actividades&tipo="+tipo+"&band_ot="+band_ot+"&critica="+critica+"&band_mac="+band_mac,
        type: 'POST',
        url : base_url+'Actividades/submit',
        async: false,
        beforeSend: function(){
            $("#btn_sub").attr("disabled",true);
        },
        success: function(data){
            let id=data;
            swal("¡Éxito!", "Guardado correctamente", "success");
            setTimeout(function () { 
                window.location = base_url+'Actividades/listado/'+$("#id_proy").val()+"/"+$("#id_cli").val();
            }, 1500);
        }
    });
}
