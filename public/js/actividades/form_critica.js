var base_url = $('#base_url').val();
let cont_emp=0;
let cont_emp_sup=0;
$(document).ready(function(){
    $("#btn_sub").on("click",function(){
        save();
    });
});

function save(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input  
        ignore: "",  
        rules: {
            nombre:{
              required: true
            },
            zona:{
              required: true
            },
            gente:{
              required: true
            },
            horas:{
              required: true
            },
            horas:{
              required: true
            },
            turno:{
              required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_data").valid();
    if(valid) {
        $.ajax({
            data: form_register.serialize()+"&tabla=limpieza_actividades_critica",
            type: 'POST',
            url : base_url+'Actividades/submit',
            async: false,
            beforeSend: function(){
                $("#btn_sub").attr("disabled",true);
            },
            success: function(data){
                let id=data;
                swal("¡Éxito!", "Guardado correctamente", "success");
                setTimeout(function () { 
                    window.location = base_url+'Actividades/listadoCriticas/'+$("#id_proy").val()+"/"+$("#id_cli").val();
                }, 1500);
            }
        }); 
    }
}
