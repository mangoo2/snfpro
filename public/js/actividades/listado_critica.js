var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
    /*$("#table_activ_crit").jsGrid({
        width: "100%",
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 15,
        pageButtonCount: 5,
        deleteConfirm: "Do you really want to delete the client?",
        controller: db,
        fields: [

        { name: "ID", type: "text", width: 150 },
        { name: "Zona", type: "text", width: 50 },
        { name: "Actividad", type: "text", width: 200 },
        { name: "Gente", type: "text", width: 200 },
        { name: "Horas", type: "text", width: 200 },
        { name: "Frecuencia", type: "text", width: 200 },
        { type: "control" }
        ]
    });*/
});

function loadtable(){
    $('#table_activ_crit thead tr').clone(true).addClass('filters').appendTo( '#table_activ_crit thead' );
    tabla=$("#table_activ_crit").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        responsive: !0,
        orderCellsTop: true,
        fixedHeader: true,
        initComplete: function() {
            var api = this.api();
            // For each column
            api.columns().eq(0).each(function(colIdx) {
                // Set the header cell to contain the input element
                var cell = $('.filters th').eq($(api.column(colIdx).header()).index());
                var title = $(cell).text();
                $(cell).html( '<input type="text" placeholder="'+title+'" />' );
                // On every keypress in this input
                $('input', $('.filters th').eq($(api.column(colIdx).header()).index()) )
                    .off('keyup change')
                    .on('keyup change', function (e) {
                        e.stopPropagation();
                        // Get the search value
                        $(this).attr('title', $(this).val());
                        var regexr = '({search})'; //$(this).parents('th').find('select').val();
                        var cursorPosition = this.selectionStart;
                        // Search the column for that value
                        api
                            .column(colIdx)
                            .search((this.value != "") ? regexr.replace('{search}', '((('+this.value+')))') : "", this.value != "", this.value == "")
                            .draw();
                        $(this).focus()[0].setSelectionRange(cursorPosition, cursorPosition);
                    });
            });
        },
        "ajax": {
            "url": base_url+"Actividades/getDataTableCritica",
            type: "post",
            data: { id_proy:$("#id_proy").val(), id_cliente:$("#id_cli").val() },
            error: function(){
               $("#table_activ_crit").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"zona"},
            {"data":"nombre"},
            {"data":"gente"},
            {"data":"horas"},
            {"data":"frecuencia"},
            {"data":"turno"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<a href="'+base_url+'Actividades/registroCritica/'+row.id+'/'+$("#id_proy").val()+'/'+$("#id_cli").val()+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
 
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Actividades/delete",
                    data: {
                        id:id, tabla:"limpieza_actividades_critica"
                    },
                    success:function(response){  
                        reload_registro();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}