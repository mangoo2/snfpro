var base_url = $('#base_url').val();
var cont1 = 0;

$(document).ready(function() {
    
});


function add_form(){
    $.validator.addMethod("usuarioexiste", function(value, element) {
    var respuestav;
    let idCliente = $('#idcliente').val();
    $.ajax({
        type: 'POST',
        url: base_url + 'Mis_clientes/validar',
        data: {
            Usuario: value
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                if ($('#idcliente').val() > 0) {
                    respuestav = true;
                } else {
                    respuestav = false;
                }

            } else {
                respuestav = true;
            }
        }
    });
    console.log(respuestav);
    return respuestav;
    });


	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            contacto: {
                required: true
            },
            correo: {
                    usuarioexiste: true,
                    minlength: 3,
                    required: true
            },
            contrasena: {
                minlength: 6,
                required: true
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
        },
        messages: {
            correo: {
                usuarioexiste: 'Seleccione otro correo ya existe en otro registro'
            }

        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function(element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();

    if ($valid) {
        ////////////////////
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Mis_clientes/guardar',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                console.log(data);
                swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Mis_clientes';
                }, 1500);
            }
        });           
    }
}
