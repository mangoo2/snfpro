var base_url = $('#base_url').val();
var id_reg = $('#id_reg').val();
$(document).ready(function() {
    if(id_reg!=0){
        table_data_detalles_valor(id_reg);
    }else{
        valor_add();
    }
});
function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            tipo: {
                required: true
            },
            acero_inoxidable: {
                required: true
            },
            esfuerzo_tencion:{
              required: true
            },
            esfuerzo_cadencia: {
                required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function(element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();

    if ($valid) {
        ////////////////////
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Material/guardar',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                guardar_detalles(data);
            	swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Material';
                }, 1500);
            }
        });           
    }
}

function valor_add(){
    tabla_data_valor(0,'','');     
}

var cont=0;
function tabla_data_valor(id,numero,valor){
    var btn_x='';
    if(cont==0){
        btn_x='<button type="button" class="btn btn-social-icon btn-sistema round" onclick="valor_add()"><i class="fa fa-plus"></i></button>';
    }else{
        btn_x='<button type="button" class="btn btn-social-icon btn-google round mr-2" onclick="remove_reg('+id+','+cont+')"><i class="fa fa-trash"></i></button>';
    }
    var html='<tr class="row_'+cont+'">\
        <td>Introducir valor manual<input class="form-control" type="hidden" id="id_v" value="'+id+'"></td>\
        <td><input class="form-control" type="number" id="numero_v" placeholder="#"  value="'+numero+'"></td>\
        <td><input class="form-control" type="number" id="valor_v" placeholder="Valor"  value="'+valor+'"></td>\
        <td>'+btn_x+'</td>\
    </tr>';
    $('#t_body').append(html);
    cont++;
}

function remove_reg(id,cont){
    if(id==0){
        $('.row_'+cont).remove();
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Está seguro de eliminar el registro?',
            type: 'blue',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Material/delete_detalles",
                        data: {
                            id:id
                        },
                        success:function(response){  
                            $('.row_'+cont).remove();
                            swal("Éxito", "Se ha eliminado correctamente", "success");
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}

function guardar_detalles(id){
    var cont=0;
    var DATA  = [];
    var TABLA   = $("#table_data_detalles tbody > tr");
    TABLA.each(function(){ 
        cont=1;
        item = {};
        item ["idmaterial"] = id;
        item ["id"] = $(this).find("input[id*='id_v']").val();
        item ["numero"] = $(this).find("input[id*='numero_v']").val();
        item ["valor"] = $(this).find("input[id*='valor_v']").val();
        DATA.push(item);

    });     
    if(cont==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Material/registro_detalles',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            }
        });  
    }
}

function table_data_detalles_valor(id){
    $.ajax({
        type:'POST',
        url: base_url+"Material/get_tabla_detalles",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element) {
                    tabla_data_valor(element.id,element.numero,element.valor)
                });
            }else{
                valor_add();
            }  
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}