var base_url = $('#base_url').val();
$(document).ready(function() {
    $('#btn_sub').click(function(){
        save();
    });
    $('.tipo_incidencia').change(function(){
        showCausa();
    });
    showCausa();

    $("#id_personal").select2({});
});

function showCausa(){
    if($("#tipo_5").is(":checked")==true)
        $("#cont_causas").show("slow");
    else
        $("#cont_causas").hide("slow");
}

function save(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            tipo: {
                required: true
            },
            fecha: {
                required: true
            },
            fecha_fin: {
                required: true
            },
            id_personal: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio") || element.is("radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function(element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();
    if ($valid) {
    	let tipo_hallazgo=0;
        if($("#tipo_1").is(":checked")==true)
            tipo=1;
        else if($("#tipo_2").is(":checked")==true)
            tipo=2;
        else if($("#tipo_3").is(":checked")==true)
            tipo=3;
        else if($("#tipo_4").is(":checked")==true)
            tipo=4;
        else if($("#tipo_5").is(":checked")==true)
            tipo=5;
        else if($("#tipo_6").is(":checked")==true)
            tipo=6;
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Presentismo/submit',
            data: datos+"&tipo_incidencia="+tipo,
            beforeSend: function(){
                $("#btn_sub").attr("disabled",true);
            },
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            	swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Presentismo/listado/'+$("#id_proy").val()+'/'+$("#id_cli").val();
                }, 3500);
            }
        });         
    }	
}
