var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
    $("#tipo").on("change",function(){
        loadtable();
    });
});

function loadtable(){
    tabla=$("#table_incidencias").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        "ajax": {
            "url": base_url+"Presentismo/getDataTable",
            type: "post",
            data: { id_proyecto:$("#id_proy").val(), tipo:$("#tipo option:selected").val() },
            error: function(){
               $("#table_incidencias").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.tipo_incidencia=="1")
                        html="Falta";
                    else if(row.tipo_incidencia=="2")
                        html="Permiso";
                    else if(row.tipo_incidencia=="3")
                        html="Retardo";
                    else if(row.tipo_incidencia=="4")
                        html="Incapacidad";
                    else if(row.tipo_incidencia=="5")
                        html="Baja";
                    else if(row.tipo_incidencia=="6")
                        html="Vacaciones";

                    return html;
                }
            },
            {"data":"fecha"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<a href="'+base_url+'Presentismo/registro/'+row.id+'/'+$("#id_proy").val()+'/'+$("#id_cli").val()+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Presentismo/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function excel_exportar(){
    location.href= base_url+'Presentismo/excel_incidencias/'+$("#id_proy").val()+'/'+$("#tipo option:selected").val();
}