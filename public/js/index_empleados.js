var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
    $("#empresa").on("change",function(){
        loadtable();
    });
});

function loadtable(){
	tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        //responsive: !0,
        destroy:true,
        //"info":     false,
        //"paging": false,
        columnDefs: [ {
            sortable: false,
            "class": "index",
            targets: 0
        } ],
        "ajax": {
            "url": base_url+"Empleados/getlistado",
            type: "post",
            data: { empresa:$("#empresa option:selected").val() },
            error: function(){
               $("#table_data").css("display","none");
            }
        },
        "columns": [
            //{"data":"personalId"},
            {"data": null,
                "render": function ( data, type, full, meta ) {
                    return  meta.row + 1;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
               
                    if(row.foto!='' && row.foto!=null){
                        html+='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'uploads/empleados/'+row.foto+'">';  
                    }else{
                        html+='<img style="width: 40px; height: 40px; border-radius: 20px;" src="'+base_url+'public/img/avatar.png">';
                    }
                return html;
                }
            },
            {"data":"nombre"},
            {"data":"puesto"},
            /*{"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';  
                    if(row.puesto=="1"){
                        html='Ajustador';  
                    }else if(row.puesto=="2"){
                        html='Filtración';  
                    }else if(row.puesto=="3"){
                        html='Técnico de limpieza';  
                    }else if(row.puesto=="4"){
                        html='Administrativo';  
                    }else if(row.puesto=="5"){
                        html='Staff';  
                    }else{
                       html='Otro';   
                    }
                return html;
                }
            },*/
            {"data":"perfil"},
            {"data":"correo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a onclick="delete_data('+row.personalId+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></a> ';
                    html+='<a href="'+base_url+'Empleados/registro/'+row.personalId+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[50, 75, 100], [50, 75, 100]],
    });
    tabla.on( 'order.dt search.dt', function () {
        tabla.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Empleados/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}