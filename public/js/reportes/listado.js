var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    $('#id_cliente').select2({
        width: '95%',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Seleccione un cliente',
        ajax: {
            url: base_url+'Reportes/searchClientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        getProyectos(data.id);
    });

    $("#id_proyecto").on("change",function(){
        getMesesProy();
    });

    $("#id_cliente,#id_proyecto,#mes").on("change",function(){
        setTimeout(function () { 
           loadtable();
        }, 1000);
    });

    if($("#tu").val()!=1){
        getProyectos($('#id_cliente option:selected').val());
        setTimeout(function () { 
           //loadtable();
        }, 1000);
    }
});

function getMesesProy(){
    //console.log("cambia select proyecto");
    let mes_ini = parseInt($("#id_proyecto option:selected").data("ini"));
    let mes_fin = parseInt($("#id_proyecto option:selected").data("fin"));
    let anio_ini = $("#id_proyecto option:selected").data("anio_ini");
    let anio_fin = $("#id_proyecto option:selected").data("anio_fin");
    let sel_mes="";
    const months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    let name = 0;
    if(anio_ini==anio_fin){
        mes_ini2 = mes_ini-1;
        for (var i=mes_ini2; i<mes_fin; i++) {
            name = month[i];
            let j=i+1;
            sel_mes+='<option data-anio_fin_opt="'+anio_fin+'" value="'+j+'">'+name+'</option>';
        }
        $("#mes").html(sel_mes);  
    }else{
        //console.log("anios distintos");
        /*console.log("anio_ini: "+anio_ini);
        console.log("anio_fin: "+anio_fin);
        console.log("mes_ini: "+mes_ini);
        console.log("mes_fin: "+mes_fin); */

        let currentYear = anio_ini;
        let currentMonth = mes_ini - 1; // Convertir mes a índice de array (0-11)
        let optionsHTML = '';
        const select = document.getElementById('mes');

        // Iterar a través de los años y meses
        while (currentYear < anio_fin || (currentYear === anio_fin && currentMonth <= mes_fin - 1)) {
            // Obtener el nombre del mes
            let name = months[currentMonth];

            // Insertar la opción con el año correcto para el atributo data
            optionsHTML += `<option data-anio_fin_opt="${currentYear}" value="${currentMonth + 1}">${name} - ${currentYear}</option>`;

            // Avanzar al siguiente mes
            currentMonth++;
            if (currentMonth > 11) { // Cambiar de año si el mes es mayor que 11
                currentMonth = 0;
                currentYear++;
            }
        }
        // Insertar las opciones en el select
        select.innerHTML = optionsHTML;

       /*if(mes_ini==12)
            mes_ini2=0;
        else    
            mes_ini2 = mes_ini-1;
        anio_aux=anio_ini; let j=0; var j_aux=0;
        rest = Number(anio_fin)-Number(anio_ini);
        console.log("rest: "+rest);
        anio_fin2= rest*12;
        //console.log("mes_ini2: "+mes_ini2);
        //console.log("anio_fin2: "+anio_fin2);
        if(anio_fin2>11){
           // anio_fin2=anio_fin2+1;
        }
        if(anio_fin>11){
            anio_fin2=anio_fin2+Number(mes_fin);
            anio_fin2=anio_fin2-1;
        }
        //console.log("anio_fin2: "+anio_fin2);
        for (var k=mes_ini2; k<=anio_fin2; k++) {
            k_aux=k;
            if(k_aux==12){
                k_aux=0;
            }
            if(k_aux==11){
                //anio_aux+1;
                //console.log("cambio el año "+anio_aux);
            }
            //console.log("k "+k);
            //console.log("k_aux "+k_aux);
            name = month[k_aux];
            j=k+1;
            j_aux++;
            //console.log("j_aux: "+j_aux);
            //console.log("nombre de mes: "+name);
            if(j_aux%12==0 && anio_fin2>11){
                //anio_aux++;
            }
            if(j_aux%13==0 || name=="Enero" && j_aux>12){
                anio_aux++;
                j_aux=1;
            }
            if(j>=13 && j<=24){
                j2=j-13;
                j=j2+1;
                name = month[j2];   
            }
            if(j>=25 && j<=36){
                j2=j-25;
                j=j2+1;
                name = month[j2];   
            }
            if(j>=37 && j<=48){
                j2=j-37;
                j=j2+1;
                name = month[j2];   
            }
            if(j>=49 && j<=60){
                j2=j-49;
                j=j2+1;
                name = month[j2];   
            }
            if(j>=61 && j<=72){
                j2=j-61;
                j=j2+1;
                name = month[j2];   
            }
            console.log("j "+j);
            sel_mes+='<option data-anio_fin_opt="'+anio_aux+'" value="'+j+'">'+name+' - '+anio_aux+'</option>';
        }*/
    }
    
}


function getMesesSelect(){
    fi= new Date($("#id_proyecto option:selected").data("fi"));
    ff= new Date($("#id_proyecto option:selected").data("ff"));
    let meses = (ff.getFullYear() - fi.getFullYear()) * 12;
    meses -= fi.getMonth();
    meses += ff.getMonth();
    return meses;
    //console.log("meses: "+meses);
}

function getProyectos(id_cli){
    $.ajax({
        type:'POST',
        url: base_url+"Reportes/getProyectosCliente",
        async:false,
        data: { id_cli:id_cli },
        success:function(response){  
            $("#id_proyecto").html(response);
            getMesesProy();
        }
    });
}

function loadtable(){
    var mes=$("#mes option:selected").val();
    var anio=$("#mes option:selected").data("anio_fin_opt");
    tabla=$("#table_ordenes").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Reportes/getDataTableReporte",
            type: "post",
            data: { id_proy:$("#id_proyecto").val(), id_cliente:$("#id_cliente option:selected").val(), anio:$("#mes option:selected").data("anio_fin_opt"), mes:$("#mes option:selected").val() },
            error: function(){
               //$("#table_ordenes").css("display","none");
            }
        },
        "columns": [
            //{"data":"id"},
            {"data":"tipo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html=$("#id_cliente option:selected").text();
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html=$("#id_proyecto option:selected").data("nameserv");
                    return html;
                }
            },
            {"data":"name_salida"},
            {"data":"reg"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.tipo==1){
                        uri_pdf=base_url+'Reportes/pdfLimpieza/'+row.id+'/'+mes+'/'+anio;
                    }
                    else if(row.tipo==2){
                        uri_pdf=base_url+'Reportes/pdfMatrizAcciones/'+row.id+'/'+mes+'/'+anio;
                    }else if(row.tipo==3){
                        uri_pdf=base_url+'Matriz_versatibilidad/documento/'+row.id;
                    }else if(row.tipo==4){
                        //uri_pdf=base_url+'Reportes/pdfActividades_realizadas/'+row.id+'/'+mes+'/'+anio;
                    }else if(row.tipo==5){
                        uri_pdf=base_url+'Reportes/pdfMonitoreo/'+row.id+'/'+mes+'/'+anio;
                    }else if(row.tipo==6){
                        uri_pdf=base_url+'Reportes/pdfPresentismo/'+row.id+'/'+mes+'/'+anio;
                    }             
                    if(row.tipo!=4){
                        html='<button title="Generar Resultados" type="button" onclick="generar('+row.id+','+row.tipo+','+anio+')"><i class="fa fa-gears" style="font-size: 20px;"></i> Generar</button>';
                    }

                    uri_img=base_url+'Reportes/carrusel/'+row.id;
                    if(row.tipo!=4){
                        html+=' <a title="Generar PDF" href="'+uri_pdf+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        html+='<button title="Enviar PDF por mail" type="button" onclick="enviarMail('+row.id+','+row.tipo+','+anio+')"><i class="fa fa-envelope-o" style="font-size: 20px;"></i> </button>';
                    }else{
                        html+=' <a onclick="generarPDFModal('+row.id+','+anio+',1)" title="Generar PDF" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        html+='<button title="Enviar PDF por mail" type="button" onclick="generarPDFModal('+row.id+','+anio+',2)"><i class="fa fa-envelope-o" style="font-size: 20px;"></i> </button>';
                    }
                    html+='<button title="Enviar whatsapp con link de PDF" class="btn btn-success" type="button" onclick="enviarWhats('+row.id+','+row.telefono+','+row.tipo+','+anio+')"><i class="fa fa-whatsapp" style="font-size: 20px;"></i> </button>';
                    if(row.tipo==4){
                       html+=' <a title="Imagenes" href="'+uri_img+'" type="button" class="btn btn-danger"><i class="fa fa-picture-o"></i></a>';
                    }
                    
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function generarPDFModal(id_proy,anio,tipo,num_tel=0){
    $("#modaldepartamento").modal("show");
    $("#id_pd").val(id_proy);
    $("#anio_pd").val(anio);
    $("#tipo_sol").val(tipo);
    $("#num_tel").val(num_tel);
    $.ajax({
        type:'POST',
        url: base_url+"Reportes/getDeptoProy",
        async:false,
        data: { id_proy:$("#id_proyecto option:selected").val() },
        success:function(response){  
            $("#departamento").html(response);
        }
    });
}

function generarPDFDpto(){
    if($("#tipo_sol").val()==1){ //realiza pdf 
        window.open(base_url+'Reportes/pdfActividades_realizadas/'+$("#id_pd").val()+'/'+$("#mes option:selected").val()+'/'+$("#departamento option:selected").val()+'/'+$("#mes option:selected").data("anio_fin_opt"));
    }else if($("#tipo_sol").val()==2){ //envia pdf por mail
        enviarMail($("#id_pd").val(),4,$("#mes option:selected").data("anio_fin_opt"),$("#departamento option:selected").val());
    }else if($("#tipo_sol").val()==3){ //envia pdf por whats
        var url_pdf=base_url+"pdf_limpieza/limpieza_ar_"+$("#id_pd").val()+"_"+$("#mes option:selected").val()+"_"+$("#mes option:selected").data("anio_fin_opt")+"_"+$("#departamento option:selected").val()+".pdf";
        $("#modaldepartamento").modal("hide");
        window.open("https://wa.me/"+$("#num_tel").val()+"?text="+url_pdf+"");
    }
}

function generar(id_proy,tipo,anio){
    if(tipo==1)
        window.open(base_url+'Reportes/resultadosOrden/'+id_proy+"/"+$("#mes option:selected").val()+"/"+anio);
    else if(tipo==2)
        window.open(base_url+'Reportes/resultadosMatrizAccion/'+id_proy+"/"+$("#mes option:selected").val()+"/"+anio);
    else if(tipo==3)
        window.open(base_url+'Matriz_versatibilidad/generar/'+id_proy);
    else if(tipo==5) //monitoreo de actividades
        window.open(base_url+'Reportes/resultadosMonitoreo/'+id_proy+"/"+$("#id_cliente option:selected").val()+"/"+$("#mes option:selected").val()+"/"+anio);
    else if(tipo==6) //presentismo de rotación
        window.open(base_url+'Reportes/resultadosPresentismo/'+id_proy+"/"+$("#mes option:selected").val()+"/"+anio);
}

function enviarMail(id_proy,tipo,anio,tipo_dpto=0){
    $.ajax({
        type: 'POST',
        url: base_url+'Reportes/enviarMail',
        data: {id_proy:id_proy,tipo:tipo, mes:$("#mes option:selected").val(), anio:anio,tipo_dpto:tipo_dpto },
        success: function(data){
            swal("Éxito", "Enviado correctamente", "success");
            if(tipo_dpto>0){
                $("#modaldepartamento").modal("hide");
            }
        }
    }); 
}

function enviarWhats(id_proy,num_tel,tipo,anio,tipo_dpto=0){
    var mes = $("#mes option:selected").val();
    if(tipo==4){
        generarPDFModal(id_proy,anio,3,num_tel);
    }else{
        /*if(tipo==4){
            var url_pdf=base_url+"pdf_limpieza/limpieza_ar_"+id_proy+"_"+mes+"_"+anio+"_"+tipo_dpto+".pdf";
        }else */if(tipo==1){
            var url_pdf=base_url+"pdf_limpieza/limpieza_"+id_proy+"_"+mes+"_"+anio+".pdf";    
        }else if(tipo==2){ //monitoreo de actividades
            var url_pdf=base_url+"pdf_matriz_acciones/matriz_"+id_proy+"_"+mes+"_"+anio+".pdf";    
        }else if(tipo==3){ //matriz de versatilidad
            var url_pdf=base_url+"pdf_matriz_versa/matriz_"+id_proy+".pdf";    
        }else if(tipo==5){ //monitoreo de actividades
            var url_pdf=base_url+"pdf_monitoreo/monitoreo_"+id_proy+"_"+mes+"_"+anio+".pdf";    
        }else if(tipo==6){ //presentismo de rotación
            var url_pdf=base_url+"pdf_presentismo/presentismo_"+id_proy+"_"+mes+"_"+anio+".pdf";    
        }
        window.open("https://wa.me/"+num_tel+"?text="+url_pdf+"");
    }
    
}