var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    $.blockUI({ 
        message: '<div class="fa-3x spinner-grow" style="justify-content: center; align-items:center;" role="status" >\
            \
          </div><br>Generando resultados..... Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff'
        } 
    });

    setTimeout(function(){
        saveGraph("graph_act_mes",5,0); 
        //console.log("save month");
    }, 3500);
    
});

function cambiaEstatus(tipo,val,dia,id_act,insp){
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/insertEditaEstatus',
        async:false,
        data: { tipo:tipo,id_proy:$("#id_proy").val(), mes:$("#mes").val(), anio:$("#anio").val(), val:val,dia:dia,id_cli:$("#id_cli").val(),id_act:id_act,insp:insp },
        success:function(data){
            //console.log(data);
        }
    });
}

function saveGraph(idname,tipo,id_act){
    var canvasx= document.getElementById(idname);
    var dataURL = canvasx.toDataURL('image/png', 1);
    //console.log("tipo :"+tipo);
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/insertGraphMonitor',
        data: {
            id:0,
            id_proyecto:$("#id_proy").val(),
            grafica:dataURL,
            mes:$("#mes_table option:selected").val(),
            anio:$("#anio").val(),
            id_act:id_act,
            tipo:tipo
        },
        success:function(data){
            //console.log("hecho");
            if(tipo==5){
                setTimeout(function(){
                    $.unblockUI();
                }, 4500);
            }
        }
    });
}