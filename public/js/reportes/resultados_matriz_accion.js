var base_url = $('#base_url').val();
var tabla;
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.load('current', {'packages':['line']});
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(graficaCumplimiento);

$(document).ready(function() {
    $.blockUI({ 
        message: '<div class="fa-3x spinner-grow" style="justify-content: center; align-items:center;" role="status" >\
            \
          </div><br>Generando resultados..... Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff'
        } 
    });

    if($("#id_cli").val()=="0"){
        setTimeout(function(){ 
            html2canvas(document.querySelector("#grafica_cumple")).then(canvas => {
            document.body.appendChild(canvas).setAttribute('id','canvastg2');
                saveGraph('canvastg2',1);
            });
            html2canvas(document.querySelector("#grafica_cumple_cli")).then(canvas => {
            document.body.appendChild(canvas).setAttribute('id','canvastg2_cli');
                saveGraph('canvastg2_cli',2);
            });
        }, 1500);
    }

    //graficaCumplimiento();
    $("#btn_cerrado").on("click", function(){
        $("#cont_cerrado").show("slow");
        $("#cont_progreso").hide("slow");
        $("#cont_abierto").hide("slow");
    });
    $("#btn_progreso").on("click", function(){
        $("#cont_cerrado").hide("slow");
        $("#cont_progreso").show("slow");
        $("#cont_abierto").hide("slow");
    });
    $("#btn_abierto").on("click", function(){
        $("#cont_cerrado").hide("slow");
        $("#cont_progreso").hide("slow");
        $("#cont_abierto").show("slow");
    });
    setTimeout(function(){ 
        if($("#cerrado").val()!=0 || $("#cerrado").val()==0 /* && $("#id_cli").val()=="0"*/){
            creaPDFTipo(1);
        }
    }, 500);
    setTimeout(function(){ 
        if($("#progreso").val()!=0 || $("#progreso").val()==0 /* && $("#id_cli").val()=="0"*/){
            creaPDFTipo(2);
        }
    }, 1000);
    setTimeout(function(){ 
        if($("#abierto").val()!=0 || $("#abierto").val()==0 /* && $("#id_cli").val()=="0"*/){
            creaPDFTipo(3);
        }
        //$.unblockUI();
    }, 1200);
    setTimeout(function(){ 
        if($("#cerrado").val()==0 && $("#progreso").val()==0 && $("#abierto").val()==0){
            //$.unblockUI();
        }
    }, 5500);
});

function graficaCumplimiento(){
    let cerrados = parseFloat($("#cerrado").val() * 100/$("#total").val());
    let progreso = parseFloat($("#progreso").val() * 100/$("#total").val());
    let abierto = parseFloat($("#abierto").val() * 100/$("#total").val());

    let cerrados_cli = parseFloat($("#cerrado_cli").val() * 100/$("#total").val());
    let progreso_cli = parseFloat($("#progreso_cli").val() * 100/$("#total").val());
    let abierto_cli = parseFloat($("#abierto_cli").val() * 100/$("#total").val());

    /*console.log("cerrados: "+cerrados);
    console.log("progreso: "+progreso);
    console.log("abierto: "+abierto);*/

    if ($("#grafica_cumple").length > 0) {
      var data = google.visualization.arrayToDataTable([
        ['Task', '% de cumplimiento'],
        ['Cerrados',  cerrados],
        ['En progreso', progreso],
        ['Abiertos',  abierto]
      ]);
      var options = {
        title: 'CUMPLIMIENTO SNF',
        is3D: true,
        width:'100%',
        height: 460,
        colors: [vihoAdminConfig.primary, "#e2c636", "#bb2d3b"]
      };
      var chart = new google.visualization.PieChart(document.getElementById('grafica_cumple'));
      chart.draw(data, options);
    }

    if ($("#grafica_cumple_cli").length > 0) {
      var data = google.visualization.arrayToDataTable([
        ['Task', '% de cumplimiento'],
        ['Cerrados',  cerrados_cli],
        ['En progreso', progreso_cli],
        ['Abiertos',  abierto_cli]
      ]);
      var options = {
        title: 'CUMPLIMIENTO CLIENTE',
        is3D: true,
        width:'100%',
        height: 460,
        colors: [vihoAdminConfig.primary, "#e2c636", "#bb2d3b"]
      };
      var chart = new google.visualization.PieChart(document.getElementById('grafica_cumple_cli'));
      chart.draw(data, options);
    }    
}

function saveGraph(name,tipo){
    var canvasx= document.getElementById(name);
    var dataURL = canvasx.toDataURL('image/png', 1);
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/saveGrafica',
        data: {
            id_proyecto:$("#id_proy").val(),
            grafica:dataURL,
            tipo:tipo,
            anio:$("#anio").val(),
            mes: $("#mes").val()
        },
        success:function(data){
            //console.log("hecho");
        }
    });
}

function creaPDFTipo(tipo){
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/pdfMatrizAccionesTipo',
        data: {
            id_proy:$("#id_proy").val(),
            tipo:tipo,
            anio:$("#anio").val(),
            mes: $("#mes").val()
        },
        success:function(data){
            //console.log("pdf de matriz tipo: "+tipo );
            setTimeout(function(){ 
                $.unblockUI();
            }, 12500);
        }
    });
}

