var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    $.blockUI({ 
        message: '<div class="fa-3x spinner-grow" style="justify-content: center; align-items:center;" role="status" >\
            \
          </div><br>Generando resultados..... Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff'
        } 
    });

    tabla=$("#getMonitoreoFin").DataTable({
        "searching": false,
        "lengthMenu": [500],
        "order": [[ 4, "asc" ]],
        /*dom: 'Bftrip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<button type="button" class="btn btn-success">Exportar a Excel <i class="fa fa-file-excel-o"></i></button>',
                title: 'Check List',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    },
                    format: {
                        header: function (data, columnIdx) {
                            return data.trim();
                        },
                        body: function (data, row, column) {

                            return data;
                        }
                    }
                }
            }
        ],*/
    });
    $(".dataTables_length").hide();
    $("#getMonitoreoFin_info").hide();
    $("#getMonitoreoFin_paginate").hide();

    setTimeout(function(){
        html2canvas(document.querySelector("#graph2")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2');
            saveGraph("canvastg2",1); //GRAFICA DE BARRAS X FIN DE SEMANA - MES
        });
        /*html2canvas(document.querySelector("#cont_completa")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2_2');
            saveGraph("canvastg2_2",2); //GRAFICA DE TOTAL DEL MES
        });*/
    }, 2000);
    
    loadTable();
    setTimeout(function(){ 
        //loadTableDiario();
        //loadTableCompleta();
    }, 1000);
    
    $("#mes_table").on("change",function(){
        loadTable();
        setTimeout(function(){ 
            //loadTableDiario();
            //loadTableCompleta();
        }, 1000);
    });

    $("#export_excel1").on("click",function(){
        window.open(base_url+'Reportes/exportExcelMonitoreo/'+$("#id_proy").val()+"/"+$("#mes_table option:selected").val()+"/"+$("#anio").val());
    });

    $("#export_excel2").on("click",function(){
        window.open(base_url+'Reportes/exportExcelMonitoreoDiario/'+$("#id_proy").val()+"/"+$("#mes_table option:selected").val()+"/"+$("#anio").val());
    });
});

function graficaCheck(){
    $("#graph2").html(""); var cont=1; 
    var ZONAS = []; var INSPECC = []; var ACTIV = []; var VALAREA = []; var VALAREAINS = []; var zona_ant=""; var zona_ant2=""; var zona_ant2=""; var zona_ant2_2=""; var s_f_activ=0;
    var TABLAFP   = $("#getMonitoreoFin tbody > tr");
    TABLAFP.each(function(){  
        zona = $(this).find("input[id*='zona']").val();
        inspeccion = $(this).find("input[id*='inspeccion']").val();
        val_area = $(this).find("input[id*='val_area']").val();
        id_act = $(this).find("input[id*='id_act']").val();
        activ = $(this).find("input[id*='activ']").val();
        ZONAS.push(zona);
        INSPECC.push(inspeccion);
        VALAREAINS.push(val_area);
        Number(val_area);
        zona_ant=zona;

        if(zona_ant2==zona && zona_ant2!=""){   
            cont++; 
        }else{
            cont=1;
        }

        if(zona_ant==zona && zona_ant2!=zona){
            ACTIV.push(activ);
            s_f_activ=$( "."+id_act+" input" ).last().val();
            VALAREA.push(s_f_activ);
        }
        /*if(zona_ant2==zona && zona_ant==zona){
            VALAREA.push(s_f_activ);
        }*/
        zona_ant2=zona;
    });
    var myLineChart = {
        labels: ACTIV,
        datasets: [{
            fillColor: "rgba(68, 114, 196, 0.2)",
            strokeColor: "#4472C4",
            pointColor: "#4472C4",
            data: VALAREA
        }]
    }
    var ctx = document.getElementById("graph").getContext("2d");
    var LineChartDemo = new Chart(ctx).Line(myLineChart, {
        animation: false,
        pointDotRadius: 2,
        pointDotStrokeWidth: 5,
        pointDotStrokeColor: "#ffffff",
        bezierCurve: true,
        scaleShowVerticalLines: true,
        scaleGridLineColor: "#eeeeee",
        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
    });

    //grafica con inspeccion
    var myLineChart = {
        labels: INSPECC,
        datasets: [{
            fillColor: "rgba(68, 114, 196, 0.2)",
            strokeColor: "#4472C4",
            pointColor: "#4472C4",
            data: VALAREAINS
        }]
    }
    var ctx = document.getElementById("graph_inspecc").getContext("2d");
    var LineChartDemo = new Chart(ctx).Line(myLineChart, {
        pointDotRadius: 2,
        pointDotStrokeWidth: 5,
        pointDotStrokeColor: "#ffffff",
        bezierCurve: true,
        scaleShowVerticalLines: true,
        scaleGridLineColor: "#eeeeee",
        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
        duration:0
    });
    setTimeout(function(){ 
        saveGraph("graph",2);
    }, 5500);   

    /* ***************************** */
    color="#00CC00";
    if($("#ft1").val()>=90)
         color="#00CC00";
    else if($("#ft1").val()>=80 && $("#ft1").val()<90)
         color="#FFFF00";
    else if($("#ft1").val()<=79)
         color="#FF0000";
    if($("#ft2").val()>=90)
         color2="#00CC00";
    else if($("#ft2").val()>=80 && $("#ft2").val()<90)
         color2="#FFFF00";
    else if($("#ft2").val()<=79)
         color2="#FF0000";
    if($("#ft3").val()>=90)
         color3="#00CC00";
    else if($("#ft3").val()>=80 && $("#ft3").val()<90)
         color3="#FFFF00";
    else if($("#ft3").val()<=79)
         color3="#FF0000";
    if($("#ft4").val()>=90)
         color4="#00CC00";
    else if($("#ft4").val()>=80 && $("#ft4").val()<90)
         color4="#FFFF00";
    else if($("#ft4").val()<=79)
         color4="#FF0000";

    color5="#00CC00";
    if(Number($("#cant_dia").val())==5){
        if($("#ft5").val()>=90)
            color5="#00CC00";
        else if($("#ft5").val()>=80 && $("#ft5").val()<90)
            color5="#FFFF00";
        else if($("#ft5").val()<=79)
            color5="#FF0000";
    }

    if ($("#graph2").length > 0) {
        var DAYS = []; var COLOR = []; var ACTIV = []; var VALAREA = []; var OBJETIVO=[]; var zona_ant=""; var zona_ant2=""; var zona_ant2=""; var zona_ant2_2=""; var s_f_activ=0;
        /*var TABLAFP   = $("#getMonitoreoFin tbody > tr");
        TABLAFP.each(function(){  
            zona = $(this).find("input[id*='zona']").val();
            id_act = $(this).find("input[id*='id_act']").val();
            activ = $(this).find("input[id*='activ']").val();
            zona_ant=zona;

            if(zona_ant2==zona && zona_ant2!=""){   
                cont++; 
            }else{
                cont=1;
            }
            
            if(zona_ant==zona && zona_ant2!=zona){
                ACTIV.push(activ);
                s_f_activ=$( "."+id_act+" input" ).last().val();
                //VALAREA.push(s_f_activ);
                //console.log("activ: "+activ);
                //console.log("val_cump: "+val_cump);
                //OBJETIVO.push(100);
                color="#00CC00";
                if(s_f_activ>=90)
                    color="#00CC00";
                else if(s_f_activ>=80 && s_f_activ<90)
                    color="#FFFF00";
                else if(s_f_activ<=79)
                    color="#FF0000";
                COLOR.push(color);
                

                var tot_day=$("#cant_dia").val();
                for(c=1; c<=tot_day; c++){
                    DAYS.push($("#dia_num"+c+"").text());
                    console.log("days: "+$("#dia_num"+c+"").text());
                    VALAREA.push($("#ft"+c+"").val());
                    OBJETIVO.push(100);
                    console.log("activ: "+activ);
                    console.log("VALAREA: "+$("#ft"+c+"").val());
                }
                
                new Chart2(document.getElementById("graph2"), {
                    type: 'bar',
                    data: {
                      labels: DAYS,
                      datasets: [
                        {
                            type: 'line',
                            label: "Objetivo",
                            borderColor: "#00CC00",
                            data: OBJETIVO
                        },
                        {
                            type: 'bar',
                            label: "",
                            backgroundColor: COLOR,
                            data: VALAREA,
                            fill: false,
                        }
                        
                      ]
                    },
                    options: {
                      legend: { 
                            display: true 
                      },
                      title: {
                        display: true,
                        text: ''
                      },
                      scales: {
                      xAxes: [{
                        ticks: {
                          autoSkip: false,
                          rotation: -90
                        }
                      }]
                    }
                    }
                });
            }
            zona_ant2=zona;
        });*/
        
        if(Number($("#cant_dia").val())<5){
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                [$("#dia_num1").text(), Number($("#ft1").val()), 'color:'+color+'', Number($("#ft1").val()),100],
                [$("#dia_num2").text(), Number($("#ft2").val()), 'color:'+color2+'', Number($("#ft2").val()),100],
                [$("#dia_num3").text(), Number($("#ft3").val()), 'color:'+color3+'', Number($("#ft3").val()),100],
                [$("#dia_num4").text(), Number($("#ft4").val()), 'color:'+color4+'', Number($("#ft4").val()),100],
            ]);
        }else if(Number($("#cant_dia").val())==5){
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                [$("#dia_num1").text(), Number($("#ft1").val()), 'color:'+color+'', Number($("#ft1").val()),100],
                [$("#dia_num2").text(), Number($("#ft2").val()), 'color:'+color2+'', Number($("#ft2").val()),100],
                [$("#dia_num3").text(), Number($("#ft3").val()), 'color:'+color3+'', Number($("#ft3").val()),100],
                [$("#dia_num4").text(), Number($("#ft4").val()), 'color:'+color4+'', Number($("#ft4").val()),100],
                [$("#dia_num5").text(), Number($("#ft5").val()), 'color:'+color5+'', Number($("#ft5").val()),100],
            ]);
        }
        var options = {
                title : $("#mes_table option:selected").text(),
                vAxis: {title: ""},
                hAxis: {title: 'DÍA'},
                seriesType: 'bars',
                series: {1: {type: 'line',color:'#00CC00'}},
                height: 500,
                width:'100%',
                colors: ["#92D050"]
        };
        var chart = new google.visualization.ComboChart(document.getElementById('graph2'));
        //chart.draw(data, options,options2,options3);
        chart.draw(data, options);

        setTimeout(function(){ 
            $("#canvastg2").html("");
            html2canvas(document.querySelector("#graph2")).then(canvas => {
            document.body.appendChild(canvas).setAttribute('id','canvastg3');
                //saveGraph("canvastg3",2); //GRAFICA DE BARRAS X FIN DE SEMANA - MES
            });
        }, 2000); 
        
    }
}

function graficaDiario(){
    var ZONAS = []; var VALAREA = []; var ACTIV = []; var zona_ant2="";
    var TABLAFP   = $("#getMonitoreoDiario tbody > tr");
    TABLAFP.each(function(){  
        /*zona = $(this).find("input[id*='zona']").val();
        val_area = $(this).find("input[id*='val_area']").val();
        ZONAS.push(zona);
        VALAREA.push(val_area);*/

        zona = $(this).find("input[id*='zona']").val();
        val_area = $(this).find("input[id*='val_area']").val();
        id_act = $(this).find("input[id*='id_act']").val();
        activ = $(this).find("input[id*='activ']").val();
        ZONAS.push(zona);
        //INSPECC.push(inspeccion);
        //VALAREA.push(val_area);
        Number(val_area);
        zona_ant=zona;

        if(zona_ant2==zona && zona_ant2!=""){   
            cont++; 
        }else{
            cont=1;
        }
        /*console.log("zona :" +zona);
        console.log("zona_ant :" +zona_ant);
        console.log("zona_ant2 :" +zona_ant2);*/
        if(zona_ant==zona && zona_ant2!=zona){
            ACTIV.push(activ);
            s_f_activ=$( "."+id_act+" input" ).last().val();
            VALAREA.push(s_f_activ);
        }
        zona_ant2=zona;
    });

    var myLineChart = {
        labels: ACTIV,
        datasets: [{
            fillColor: "rgba(68, 114, 196, 0.2)",
            strokeColor: "#4472C4",
            pointColor: "#4472C4",
            data: VALAREA
        }],
        /*options: {
            animation: {
                duration:0
            },
        }*/
    }
    var ctx = document.getElementById("graph_diario").getContext("2d");
    var LineChartDemo = new Chart(ctx).Line(myLineChart, {
        animation: false,
        animationEasing: 'linear',
        pointDotRadius: 2,
        pointDotStrokeWidth: 5,
        pointDotStrokeColor: "#ffffff",
        bezierCurve: true,
        scaleShowVerticalLines: true,
        scaleGridLineColor: "#eeeeee",
        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
        duration:0
    });
    setTimeout(function(){ 
        saveGraph("graph_diario",3);
    }, 6500);   
}

////////////////////////////////////////////
function loadTable(){
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/getMonitoreoFin',
        async:false,
        data: { id_proy:$("#id_proy").val(), mes:$("#mes_table option:selected").val(), anio:$("#anio").val() },
        success:function(data){
            //console.log(data);
            var array = $.parseJSON(data);
            $("#tble_head").html(array.html_head);
            $("#body_table").html(array.html);
            $("#tfoot").html(array.html_foot);
            $("#cant_dia").val(array.cant_dias);
            setTimeout(function(){ 
                $("#graph2").html("");
                graficaCheck();
                //$("#name_mes").html($("#mes_table").text());
            }, 1000);
        }
    });
}

function loadTableDiario(){
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/getMonitoreoDiario',
        async:false,
        data: { id_proy:$("#id_proy").val(), mes:$("#mes_table option:selected").val(), anio:$("#anio").val() },
        success:function(data){
            //console.log(data);
            var array = $.parseJSON(data);
            $("#tble_head_dia").html(array.html_head);
            $("#body_table_dia").html(array.html);
            $("#tfoot_dia").html(array.html_foot);
            graficaDiario();
        }
    });
}

function loadTableCompleta(){
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/getMonitoreoCompleta',
        async:false,
        data: { id_proy:$("#id_proy").val(), mes:$("#mes_table option:selected").val(), anio:$("#anio").val() },
        success:function(data){
            //console.log(data);
            var array = $.parseJSON(data);
            //console.log("band_dia: "+array.band_dia);
            $("#tble_head_diacomp").html(array.html_head);
            $("#body_table_diacomp").html(array.html);
            $("#tfoot_diacomp").html(array.html_foot);
            //$("#anio").val(array.anio);
            $("#total_dias").val(array.band_dia);
            graficaCompleta();
        }
    });
}

function graficaCompleta(){
    var DATAS = []; var DATAS2 = []; var DATAS3 = []; var DATAS4 = []; var DATAS5 = []; 
    var FECHAS = []; var VALAREA = []; var COLOR = []; var OBJETIVO = [];
    /*for(var i=1; i<=Number($("#total_dias").val()); i++){
        fec=i+"/"+$("#mes_table option:selected").val()+"/"+$("#anio").val();
        FECHAS.push(fec);
        //console.log("fec: "+fec);
        val_cump = $("input[id*='porc_"+i+"']").val();
        VALAREA.push(val_cump);
        //console.log("val_cump: "+val_cump);
        OBJETIVO.push(100);
        color="#00CC00";
        if(val_cump>=90)
            color="#00CC00";
        else if(val_cump>=80 && val_cump<90)
            color="#FFFF00";
        else if(val_cump<=79)
            color="#FF0000";
        COLOR.push(color);
        //console.log("color: "+color);

        //DATAS.push(FECHAS,VALAREA,COLOR,OBJETIVO); 
        //DATAS = [""+fec+"",val_cump,color,val_cump,100]
    }
    //console.log("DATAS: "+DATAS);
    var data = google.visualization.arrayToDataTable([
        ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
        //[""+FECHAS+"", Number(VALAREA), 'color:'+COLOR+'', Number(VALAREA),100],
        [""+DATAS+"",DATAS2,DATAS3,DATAS4,DATAS5]
        //["hahs", Number(12), 'color:'+color+'', Number(12),100],
    ]);

    var options = {
            title : 'EFICIENCIA DE LIMPIEZA '+$("#mes_table option:selected").text().toUpperCase()+'',
            vAxis: {title: "PORCENTAJE"},
            hAxis: {title: 'FECHA'},
            seriesType: 'bars',
            series: {1: {type: 'line',color:'#00CC00'}},
            height: 500,
            width:'100%',
            colors: ["#92D050"]
    };
    var chart = new google.visualization.ComboChart(document.getElementById('graph_completa'));
    chart.draw(data, options);*/

    for(var i=1; i<=Number($("#total_dias").val()); i++){
        fec=i+"/"+$("#mes_table option:selected").val()+"/"+$("#anio").val();
        FECHAS.push(fec);
        //console.log("fec: "+fec);
        val_cump = $("input[id*='porc_"+i+"']").val();
        VALAREA.push(val_cump);
        //console.log("val_cump: "+val_cump);
        OBJETIVO.push(100);
        color="#00CC00";
        if(val_cump>=90)
            color="#00CC00";
        else if(val_cump>=80 && val_cump<90)
            color="#FFFF00";
        else if(val_cump<=79)
            color="#FF0000";
        COLOR.push(color);
    }
    new Chart2(document.getElementById("graph_completa"), {
        type: 'bar',
        data: {
          labels: FECHAS,
          datasets: [
            {
                type: 'line',
                label: "Objetivo",
                borderColor: "#00CC00",
                data: OBJETIVO
            },
            {
                type: 'bar',
                label: "",
                backgroundColor: COLOR,
                data: VALAREA,
                fill: false,
            }
            
          ]
        },
        options: {
          legend: { 
                display: true 
          },
          title: {
            display: true,
            text: 'EFICIENCIA DE LIMPIEZA '+$("#mes_table option:selected").text().toUpperCase()+''
          },
          scales: {
          xAxes: [{
            ticks: {
              autoSkip: false,
              rotation: -90
            }
          }]
        }
        }
    });

    /*var FECHAS = []; var VALAREA = []; 
    for(var i=1; i<=Number($("#total_dias").val()); i++){
        fec=i+"/"+$("#mes_table option:selected").val()+"/"+$("#anio").val();
        FECHAS.push(fec);
        //console.log("fec: "+fec);
        val_cump = $("input[id*='porc_"+i+"']").val();
        VALAREA.push(val_cump);
        //console.log("val_cump: "+val_cump);
    }

    var myLineChart = {
        labels: FECHAS,
        datasets: [{
            fillColor: "rgba(68, 114, 196, 0.2)",
            strokeColor: "#4472C4",
            pointColor: "#4472C4",
            data: VALAREA
        }]
    }
    var ctx = document.getElementById("graph_completa").getContext("2d");
    var LineChartDemo = new Chart(ctx).Line(myLineChart, {
        pointDotRadius: 2,
        pointDotStrokeWidth: 5,
        pointDotStrokeColor: "#ffffff",
        bezierCurve: true,
        scaleShowVerticalLines: true,
        scaleGridLineColor: "#eeeeee",
        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",
    });*/
    setTimeout(function(){ 
        //saveGraph("graph_completa",2);
    }, 3000);   
}

function saveGraph(idname,tipo){
    var canvasx= document.getElementById(idname);
    var dataURL = canvasx.toDataURL('image/png', 1);
    //console.log("tipo :"+tipo);
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/insertGraphMonitor',
        data: {
            id:0,
            id_proyecto:$("#id_proy").val(),
            grafica:dataURL,
            mes:$("#mes_table option:selected").val(),
            anio:$("#anio").val(),
            tipo:tipo
        },
        success:function(data){
            //console.log("hecho");
            setTimeout(function(){
                $.unblockUI();
            }, 6500);
        }
    });
}