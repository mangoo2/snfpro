var base_url = $('#base_url').val();
$(document).ready(function() {
});

function get_img(){
    $('.cards-wrapper').html('');
	$.ajax({
        type:'POST',
        url: base_url+'Reportes/get_actividad_img',
        data: {
            idproyecto:$('#idproyecto').val(),
            iddepa:$('#iddepartamento option:selected').val(),
        },
        success:function(data){
            $('.carrusel_text').html(data);
            iniciarcarrucel();
        }
    });
    //get_img2();
}
/*
function get_img2(){
    $('.img_text_n').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/get_actividad_img2',
        data: {
            idproyecto:$('#idproyecto').val(),
            iddepa:$('#iddepartamento option:selected').val(),
        },
        success:function(data){
            //$('.img_text_n').html(data);
        }
    });
}

function shiftLeft() {
    const boxes = document.querySelectorAll(".box");
    const tmpNode = boxes[0];
    boxes[0].className = "box move-out-from-left";

    setTimeout(function() {
        if (boxes.length > 1) {
            tmpNode.classList.add("box--hide");
            boxes[1].className = "box move-to-position5-from-left";
        }
        boxes[1].className = "box move-to-position1-from-left";
        boxes[0].remove();

        document.querySelector(".cards__container").appendChild(tmpNode);

    }, 500);

}

function shiftRight() {
    const boxes = document.querySelectorAll(".box");
    boxes[1].className = "box move-out-from-right";
    setTimeout(function() {
        const noOfCards = boxes.length;
        if (noOfCards > 1) {
            boxes[1].className = "box box--hide";
        }

        const tmpNode = boxes[noOfCards - 1];
        tmpNode.classList.remove("box--hide");
        boxes[noOfCards - 1].remove();
        let parentObj = document.querySelector(".cards__container");
        parentObj.insertBefore(tmpNode, parentObj.firstChild);
        tmpNode.className = "box move-to-position1-from-right";
        boxes[0].className = "box move-to-position2-from-right";
        boxes[1].className = "box move-to-position3-from-right";
    }, 500);

}

function actividad_nomb(id){
    var nomb = $().data('descrip');
    $('.title_actividad').html(nomb);
}

function btn_caja(id){
    //$('.imgcj').css('display','none');
    //$('.img_'+id).css('display','block');
    $('.imgcj').hide(500);
    $('.img_'+id).show(500);
    
    $('.btn_background_naranja').css('background-color','#cf3117');
    $('.btn_'+id).css('background-color','green');

    

}
*/
function iniciarcarrucel(){
    const myCarousel = new Carousel({
      container: document.querySelector('.slider'),
      items: document.querySelectorAll('.slider__item'),
      displayControls: true,
      controlsContainer: document.querySelector('.slider__controls'),
      textControls: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      autoplay: true,
      autoplayTime: 4000
    });
}