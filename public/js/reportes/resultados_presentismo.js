var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	setTimeout(function(){
        /*html2canvas(document.querySelector("#cont_graph_line")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2');
            saveGraph("canvastg2",2); //GRAFICA DE PRESENTISMO
        });*/
    }, 5500);
    setTimeout(function(){
        /*html2canvas(document.querySelector("#cont_graph_line2")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2_2');
            saveGraph("canvastg2_2",3); //GRAFICA DE TURN OVER
        });*/
    }, 6000);

	/*let label_sem=[]; let vals_sem=[]; let objetivo=[];
	let TABLA = $("#table_presentismo tbody > tr");
    TABLA.each(function(){  
    	//console.log("nums_sems: "+$(this).find("input[id*='nums_sems']").val());
    	//console.log("val_sems: "+$(this).find("input[id*='nums_sems']").val());
		label_sem.push(String("SEM "+$(this).find("input[id*='nums_sems']").val()));
		vals_sem.push(parseFloat($(this).find("input[id*='val_sems']").val()));
		objetivo.push(100);
	});
	//console.log("label_sem: "+label_sem);
	//console.log("vals_sem: "+vals_sem);

	var chart = new Chartist.Line('#graph_line', {
	  	labels: label_sem,
	  	series: [
			vals_sem,
			objetivo
	  	]
	},
	{
	plugins: [
	    Chartist.plugins.ctPointLabels({
	      	textAnchor: 'middle',
	      	currency: '%',
	      	//appendToBody: true,
        	//pointClass: 'my-cool-point'
	    })
	  ]
	},
	{
	  	low: 0,
	  	//showLabel: false
	  	//showArea: true,
	  	//reverseData: true, 
	  	//createLabel:true,
	});
	var seq = 0,
	  	delays = 80,
	  	durations = 500;
		chart.on('created', function() {
	  		seq = 0;
		});
		chart.on('draw', function(data) {
	  	seq++;
		if(data.type === 'line') {
			data.element.animate({
		  		opacity: {
					begin: seq * delays + 1000,
					dur: durations,
					from: 0,
					to: 1
		  		}
			});
	  	} else if(data.type === 'label' && data.axis === 'x') {
			data.element.animate({
		  		y: {
					begin: seq * delays,
					dur: durations,
					from: data.y + 100,
					to: data.y,
					easing: 'easeOutQuart',
		  		}
			});
	  	} else if(data.type === 'label' && data.axis === 'y') {
			data.element.animate({
		  		x: {
					begin: seq * delays,
					dur: durations,
					from: data.x - 100,
					to: data.x,
					easing: 'easeOutQuart'
		  		}
			});
	  	} else if(data.type === 'point') {
			data.element.animate({
		  		x1: {
					begin: seq * delays,
					dur: durations,
					from: data.x - 10,
					to: data.x,
					easing: 'easeOutQuart',
					labels:"owa"
		  		},
		  		x2: {
					begin: seq * delays,
					dur: durations,
					from: data.x - 10,
					to: data.x,
					easing: 'easeOutQuart'
		  		},
		  		opacity: {
					begin: seq * delays,
					dur: durations,
					from: 0,
					to: 1,
					easing: 'easeOutQuart'
		  		}
			});
	  	} else if(data.type === 'grid') {
			var pos1Animation = {
		  		begin: seq * delays,
		  		dur: durations,
		  		from: data[data.axis.units.pos + '1'] - 30,
		  		to: data[data.axis.units.pos + '1'],
		  		easing: 'easeOutQuart'
			};
			var pos2Animation = {
		  		begin: seq * delays,
		  		dur: durations,
		  		from: data[data.axis.units.pos + '2'] - 100,
		  		to: data[data.axis.units.pos + '2'],
		  		easing: 'easeOutQuart'
			};
			var animations = {};
				animations[data.axis.units.pos + '1'] = pos1Animation;
				animations[data.axis.units.pos + '2'] = pos2Animation;
				animations['opacity'] = {
		  		begin: seq * delays,
		  		dur: durations,
		  		from: 0,
		  		to: 1,
		  		easing: 'easeOutQuart'
			};
			data.element.animate(animations);
	  	}
	});

	chart.on('created', function() {
		if(window.__exampleAnimateTimeout) {
			clearTimeout(window.__exampleAnimateTimeout);
			window.__exampleAnimateTimeout = null;
	  	}
	  	window.__exampleAnimateTimeout = setTimeout(chart.update.bind(chart), 12000);
	});

	//tabla de turn over
	let label_sem2=[]; let vals_sem2=[]; let objetivo2=[];
	let TABLA2 = $("#table_turn tbody > tr");
    TABLA2.each(function(){  
    	//console.log("nums_sems: "+$(this).find("input[id*='nums_sems']").val());
    	//console.log("val_sems: "+$(this).find("input[id*='nums_sems']").val());
		label_sem2.push(String("SEM "+$(this).find("input[id*='nums_sems']").val()));
		vals_sem2.push(Number($(this).find("input[id*='val_sems']").val()));
		objetivo2.push(0);
	});
	//console.log("label_sem: "+label_sem2);
	//console.log("vals_sem: "+vals_sem2);

	var chart = new Chartist.Line('#graph_line2', {
	  	labels: label_sem2,
	  	series: [
			vals_sem2,
			objetivo2
	  	]
	},
	{
	plugins: [
	    Chartist.plugins.ctPointLabels({
	      	textAnchor: 'middle',
	      	currency: '%',
	      	//appendToBody: true,
        	//pointClass: 'my-cool-point'
	    })
	  ]
	},
	{
	  	low: 0,
	  	//showLabel: false
	  	//showArea: true,
	  	//reverseData: true, 
	  	//createLabel:true,
	});
	var seq = 0,
	  	delays = 80,
	  	durations = 500;
		chart.on('created', function() {
	  		seq = 0;
		});
		chart.on('draw', function(data) {
	  	seq++;
		if(data.type === 'line') {
			data.element.animate({
		  		opacity: {
					begin: seq * delays + 1000,
					dur: durations,
					from: 0,
					to: 1
		  		}
			});
	  	} else if(data.type === 'label' && data.axis === 'x') {
			data.element.animate({
		  		y: {
					begin: seq * delays,
					dur: durations,
					from: data.y + 100,
					to: data.y,
					easing: 'easeOutQuart',
		  		}
			});
	  	} else if(data.type === 'label' && data.axis === 'y') {
			data.element.animate({
		  		x: {
					begin: seq * delays,
					dur: durations,
					from: data.x - 100,
					to: data.x,
					easing: 'easeOutQuart'
		  		}
			});
	  	} else if(data.type === 'point') {
			data.element.animate({
		  		x1: {
					begin: seq * delays,
					dur: durations,
					from: data.x - 10,
					to: data.x,
					easing: 'easeOutQuart',
					labels:"owa"
		  		},
		  		x2: {
					begin: seq * delays,
					dur: durations,
					from: data.x - 10,
					to: data.x,
					easing: 'easeOutQuart'
		  		},
		  		opacity: {
					begin: seq * delays,
					dur: durations,
					from: 0,
					to: 1,
					easing: 'easeOutQuart'
		  		}
			});
	  	} else if(data.type === 'grid') {
			var pos1Animation = {
		  		begin: seq * delays,
		  		dur: durations,
		  		from: data[data.axis.units.pos + '1'] - 30,
		  		to: data[data.axis.units.pos + '1'],
		  		easing: 'easeOutQuart'
			};
			var pos2Animation = {
		  		begin: seq * delays,
		  		dur: durations,
		  		from: data[data.axis.units.pos + '2'] - 100,
		  		to: data[data.axis.units.pos + '2'],
		  		easing: 'easeOutQuart'
			};
			var animations = {};
				animations[data.axis.units.pos + '1'] = pos1Animation;
				animations[data.axis.units.pos + '2'] = pos2Animation;
				animations['opacity'] = {
		  		begin: seq * delays,
		  		dur: durations,
		  		from: 0,
		  		to: 1,
		  		easing: 'easeOutQuart'
			};
			data.element.animate(animations);
	  	}
	});

	chart.on('created', function() {
		if(window.__exampleAnimateTimeout) {
			clearTimeout(window.__exampleAnimateTimeout);
			window.__exampleAnimateTimeout = null;
	  	}
	  	window.__exampleAnimateTimeout = setTimeout(chart.update.bind(chart), 12000);
	});*/
});