var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    $.blockUI({ 
        message: '<div class="fa-3x spinner-grow" style="justify-content: center; align-items:center;" role="status" >\
            \
          </div><br>Generando resultados..... Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff'
        } 
    });

    tabla=$("#result_cumpli").DataTable({
        "searching": false,
        "lengthMenu": [500],
        "order": [[ 0, "asc" ]],
    });
    $(".dataTables_length").hide();
    $("#result_cumpli_info").hide();
    $(".dataTables_paginate").hide();

    setTimeout(function(){ 
        resultadosSemanal();
        //resultadosTotalCumplimiento();
    }, 2000);

    setTimeout(function(){
        html2canvas(document.querySelector("#graph_turnos_2")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvas_gt_2');
            saveGraph("canvas_gt_2",1); //GRAFICA  turnos
        });
    }, 3000);
    setTimeout(function(){
        html2canvas(document.querySelector("#graph_semana_2")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvas_gs_2');
            saveGraph("canvas_gs_2",2); //GRAFICA  semanas
        });
        html2canvas(document.querySelector("#cont_table_turnos")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2');
            saveTable("canvastg2",4); //tabla turnos
        });
    }, 4000);
    setTimeout(function(){
        
        html2canvas(document.querySelector("#cont_table_semanas")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2_2');
            saveTable("canvastg2_2",5); //tabla semanas
        });

        html2canvas(document.querySelector("#cont_table_anual")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2_3');
            saveTable("canvastg2_3",6); //tabla anual
        });
    }, 5000);
    setTimeout(function(){
        html2canvas(document.querySelector("#cont_graph_cumplimiento")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvasgc');
            saveGraph("canvasgc",9); //GRAFICA cumplimiento de semana
        });
        html2canvas(document.querySelector("#cont_graph_eficiencia")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvasfe');
            saveGraph("canvasfe",10); //grafica eficacia 
        });
    }, 5500);

    selectSemana();
    $("#semana_act").on("change",function(){
        tablaSemanaActividades();
    });
    tabla=$("#table_rep_sem").DataTable({
        "searching": false,
        "lengthMenu": [500],
        "order": [[ 1, "asc" ]],
        //dom: 'Bftrip',
        /*buttons: [
            //'excel',
            {
                extend: 'excelHtml5',
                text: 'Exportar a Excel'
            },
        ]*/
        /*buttons: [
            //'pdfHtml5',
            {
                extend: 'excelHtml5',
                text: '<span class="btn btn-success">Exportar a Excel <i class="fa fa-file-excel-o"></i></span>',
                title: 'Lista de ordenes',
                exportOptions: {
                    modifier: {
                        page: 'all'
                    },
                    format: {
                        header: function (data, columnIdx) {
                            return data.trim();
                        },
                        body: function (data, row, column) {

                            return data;
                        }
                    }
                }
            }
        ],*/
    });
    $(".dataTables_length").hide();
    $("#table_rep_sem_info").hide();
    $(".dataTables_paginate").hide();

    $("#expot_list").on("click",function(){
        exportarListaOrd();
    });

});

function exportarListaOrd(){
    window.open(base_url+'Reportes/exportarListaOrdenes/'+$("#id_proy").val()+"/"+$("#semana_act option:selected").val()+"/"+$("#mes").val()+"/"+$("#anio").val());
}

function selectSemana(){
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/getSemanaProyecto',
        async:false,
        data: { id_proyecto:$("#id_proy").val(), anio:$("#anio").val(), mes:$("#mes").val() },
        success:function(data){
            //console.log(data);
            $("#semana_act").html(data);
            tablaSemanaActividades();
        }
    });
}

function saveTable(idname,tipo,semana=0){
    var canvasx= document.getElementById(idname);
    var dataURL = canvasx.toDataURL('image/png', 1);
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/insertGraph',
        data: {
            id:0,
            id_proyecto:$("#id_proy").val(),
            grafica:dataURL,
            mes:$("#mes").val(),
            anio:$("#anio").val(),
            tipo:tipo,
            semana:semana
        },
        success:function(data){
            //console.log("hecho, guarda tabla");
        }
    });
}

var semanai=0; var sem1=0; var sem2=0; var sem3=0; var sem4=0; var sem5=0; var sem6=0;
function resultadosSemanal(){
    let html1=""; let html2=""; let html3=""; let html4=""; let html5=""; let html6="";
    
    tot1=0; tot2=0; tot3=0; tot1_1=0; tot2_2=0; tot3_3=0; tot4_1=0; tot4_2=0; tot4_3=0; tot5_1=0; tot5_2=0; tot5_3=0;
    tot6_1=0; tot6_2=0; tot6_3=0;
    tot7_1=0; tot7_2=0; tot7_3=0;

    cump1=0; cump2=0; cump3=0; 
    cump2_1=0; cump2_2=0; cump2_3=0; 
    cump3_1=0; cump3_2=0; cump3_3=0; 
    cump4_1=0; cump4_2=0; cump4_3=0;
    cump5_1=0; cump5_2=0; cump5_3=0;
    cump6_1=0; cump6_2=0; cump6_3=0;

    ba1=0; ba2=0; ba3=0; ba1_1=0; ba2_2=0; ba3_3=0; ba4_1=0; ba4_2=0; ba4_3=0; ba5_1=0; ba5_2=0; ba5_3=0;
    ba6_1=0; ba6_2=0; ba6_3=0;
    ba7_1=0; ba7_2=0; ba7_3=0;
    let TABLA = $("#result_cumpli tbody > tr");
    TABLA.each(function(){         
        if($(this).find("input[id*='cont']").val()==1){
            if(!isNaN($(this).find("input[class*='semanal1']").val())){
                tot1=tot1+Number($(this).find("input[class*='semanal1']").val());
                cump1=cump1+Number($(this).find("input[class*='cumpli1']").val());
                ba1++;
            }
            if(!isNaN($(this).find("input[class*='semanal2']").val())){
                tot2=tot2+Number($(this).find("input[class*='semanal2']").val());
                cump2=cump2+Number($(this).find("input[class*='cumpli2']").val());
                ba2++;
            }
            if(!isNaN($(this).find("input[class*='semanal3']").val())){
                tot3=tot3+Number($(this).find("input[class*='semanal3']").val());
                cump3=cump3+Number($(this).find("input[class*='cumpli3']").val());
                ba3++;
            }
            sem1=$(this).find("input[id*='cont']").data("sem_ini");
            //console.log("sem_ini 1: "+$(this).find("input[id*='cont']").data("sem_ini"));
        }
        if($(this).find("input[id*='cont']").val()==2){
            if(!isNaN($(this).find("input[class*='semanal1_2']").val())){
                tot1_1=tot1_1+Number($(this).find("input[class*='semanal1_2']").val());
                cump2_1=cump2_1+Number($(this).find("input[class*='cumpli1_2']").val());
                ba1_1++;
            }
            if(!isNaN($(this).find("input[class*='semanal2_2']").val())){
                tot2_2=tot2_2+Number($(this).find("input[class*='semanal2_2']").val());
                cump2_2=cump2_2+Number($(this).find("input[class*='cumpli2_2']").val());
                ba2_2++;
            }
            if(!isNaN($(this).find("input[class*='semanal3_2']").val())){
                tot3_3=tot3_3+Number($(this).find("input[class*='semanal3_2']").val());
                cump2_3=cump2_3+Number($(this).find("input[class*='cumpli3_2']").val());
                ba3_3++;
            }
            sem2=$(this).find("input[id*='cont']").data("sem_ini");
        }
        if($(this).find("input[id*='cont']").val()==3){
            if(!isNaN($(this).find("input[class*='semanal1_3']").val())){
                tot4_1=tot4_1+Number($(this).find("input[class*='semanal1_3']").val());
                cump3_1=cump3_1+Number($(this).find("input[class*='cumpli1_3']").val());
                ba4_1++;
            }
            if(!isNaN($(this).find("input[class*='semanal2_3']").val())){
                tot4_2=tot4_2+Number($(this).find("input[class*='semanal2_3']").val());
                cump3_2=cump3_2+Number($(this).find("input[class*='cumpli2_3']").val());
                ba4_2++;
            }
            if(!isNaN($(this).find("input[class*='semanal3_3']").val())){
                tot4_3=tot4_3+Number($(this).find("input[class*='semanal3_3']").val());
                cump3_3=cump3_3+Number($(this).find("input[class*='cumpli3_3']").val());
                ba4_3++;
            }
            sem3=$(this).find("input[id*='cont']").data("sem_ini");
        }
        if($(this).find("input[id*='cont']").val()==4){
            if(!isNaN($(this).find("input[class*='semanal1_4']").val())){
                tot5_1=tot5_1+Number($(this).find("input[class*='semanal1_4']").val());
                cump4_1=cump4_1+Number($(this).find("input[class*='cumpli1_4']").val());
                ba5_1++;
            }
            if(!isNaN($(this).find("input[class*='semanal2_4']").val())){
                tot5_2=tot5_2+Number($(this).find("input[class*='semanal2_4']").val());
                cump4_2=cump4_2+Number($(this).find("input[class*='cumpli2_4']").val());
                ba5_2++;
            }
            if(!isNaN($(this).find("input[class*='semanal3_4']").val())){
                tot5_3=tot5_3+Number($(this).find("input[class*='semanal3_4']").val());
                cump4_3=cump4_3+Number($(this).find("input[class*='cumpli3_4']").val());
                ba5_3++;
            }
            sem4=$(this).find("input[id*='cont']").data("sem_ini");
        }
        if($(this).find("input[id*='cont']").val()==5){
            if(!isNaN($(this).find("input[class*='semanal1_5']").val())){
                tot6_1=tot6_1+Number($(this).find("input[class*='semanal1_5']").val());
                cump5_1=cump5_1+Number($(this).find("input[class*='cumpli1_5']").val());
                ba6_1++;
            }
            if(!isNaN($(this).find("input[class*='semanal2_5']").val())){
                tot6_2=tot6_2+Number($(this).find("input[class*='semanal2_5']").val());
                cump5_2=cump5_2+Number($(this).find("input[class*='cumpli2_5']").val());
                ba6_2++;
            }
            if(!isNaN($(this).find("input[class*='semanal3_5']").val())){
                tot6_3=tot6_3+Number($(this).find("input[class*='semanal3_5']").val());
                cump5_3=cump5_3+Number($(this).find("input[class*='cumpli3_5']").val());
                ba6_3++;
            }
            sem5=$(this).find("input[id*='cont']").data("sem_ini");
        }
        if($(this).find("input[id*='cont']").val()==6){
            if(!isNaN($(this).find("input[class*='semanal1_6']").val())){
                tot7_1=tot7_1+Number($(this).find("input[class*='semanal1_6']").val());
                cump6_1=cump6_1+Number($(this).find("input[class*='cumpli1_6']").val());
                ba7_1++;
            }
            if(!isNaN($(this).find("input[class*='semanal2_6']").val())){
                tot7_2=tot7_2+Number($(this).find("input[class*='semanal2_6']").val());
                cump6_2=cump6_2+Number($(this).find("input[class*='cumpli1_6']").val());
                ba7_2++;
            }
            if(!isNaN($(this).find("input[class*='semanal3_6']").val())){
                tot7_3=tot7_3+Number($(this).find("input[class*='semanal3_6']").val());
                cump6_3=cump6_3+Number($(this).find("input[class*='cumpli1_6']").val());
                ba7_3++;
            }
            sem6=$(this).find("input[id*='cont']").data("sem_ini");
        }
    });
    //console.log("cump1: "+cump1);
    //console.log("ba2_2: "+ba2_2);
    if(ba1>0){
        tot1=cump1/ba1;
    }
    if(ba2>0){
        tot2=cump2/ba2;
    }
    if(ba3>0){
        tot3=cump3/ba3;
    }

    if(ba1_1>0){
        tot1_1=cump2_1/ba1_1;
    }
    if(ba2_2>0){
        tot2_2=cump2_2/ba2_2;
    }
    if(ba3_3>0){
        tot3_3=cump2_3/ba3_3;
    }

    if(ba4_1>0){
        tot4_1=cump3_1/ba4_1;
    }
    if(ba4_2>0){
        tot4_2=cump3_2/ba4_2;
    }
    if(ba4_3>0){
        tot4_3=cump3_3/ba4_3;
    }

    if(ba5_1>0){
        tot5_1=cump4_1/ba5_1;
    }
    if(ba5_2>0){
        tot5_2=cump4_2/ba5_2;
    }
    if(ba5_3>0){
        tot5_3=cump4_3/ba5_3;
    }

    if(ba6_1>0){
        tot6_1=cump5_1/ba6_1;
    }
    if(ba6_2>0){
        tot6_2=cump5_2/ba6_2;
    }
    if(ba6_3>0){
        tot6_3=cump5_3/ba6_3;
    }

    if(ba7_1>0){
        tot7_1=cump6_1/ba7_1;
    }
    if(ba7_2>0){
        tot7_2=cump6_2/ba7_2;
    }
    if(ba7_3>0){
        tot7_3=cump6_3/ba7_3;
    }
    
    /*if(tot1>100){ tot1=100; } if(tot2>100){ tot2=100; } if(tot3>100){ tot3=100; }
    if(tot1_1>100){ tot1_1=100; } if(tot2_2>100){ tot2_2=100; } if(tot3_3>100){ tot3_3=100; }
    if(tot4_1>100){ tot4_1=100; } if(tot4_2>100){ tot4_2=100; } if(tot4_3>100){ tot4_3=100; }
    if(tot5_1>100){ tot5_1=100; } if(tot5_2>100){ tot5_2=100; } if(tot5_3>100){ tot5_3=100; }
    if(tot6_1>100){ tot6_1=100; } if(tot6_2>100){ tot6_2=100; } if(tot6_3>100){ tot6_3=100; }
    if(tot7_1>100){ tot7_1=100; } if(tot7_2>100){ tot7_2=100; } if(tot7_3>100){ tot7_3=100; }*/

    let super1=Number(tot1)+Number(tot2)+Number(tot3);
    let super2=Number(tot1_1)+Number(tot2_2)+Number(tot3_3);
    let super3=Number(tot4_1)+Number(tot4_2)+Number(tot4_3);
    let super4=Number(tot5_1)+Number(tot5_2)+Number(tot5_3);
    let super5=Number(tot6_1)+Number(tot6_2)+Number(tot6_3);
    let super6=Number(tot7_1)+Number(tot7_2)+Number(tot7_3);
    var entre=0;
    if($("#1ero").val()==1)
        entre++;
    if($("#2do").val()==1)
        entre++;
    if($("#3er").val()==1)
        entre++;
    //console.log("entre: "+entre);

    super1=parseFloat(super1/entre).toFixed(2);
    super2=parseFloat(super2/entre).toFixed(2);
    super3=parseFloat(super3/entre).toFixed(2);
    super4=parseFloat(super4/entre).toFixed(2);
    super5=parseFloat(super5/entre).toFixed(2);
    super6=parseFloat(super6/entre).toFixed(2);
    /*if(super1>100){
        super1=100;
    }if(super2>100){
        super2=100;
    }if(super3>100){
        super3=100;
    }if(super4>100){
        super4=100;
    }if(super5>100){
        super5=100;
    }if(super6>100){
        super6=100;
    }*/

    html1+='<tr>';
    if($("#1ero").val()==1 && ba1>0) html1+='<td>'+parseFloat(tot1).toFixed(2)+'%<input id="super1" type="hidden" value="'+super1+'"></td>';
    if($("#1ero").val()==1 && ba1==0 && $("#2do").val()==1) html1+='<td>0.00%<input id="super1" type="hidden" value="'+super1+'"></td>';
    if($("#2do").val()==1 && ba2>0) html1+='<td>'+parseFloat(tot2).toFixed(2)+'%<input id="super1" type="hidden" value="'+super1+'"></td>';
    if($("#3er").val()==1 && ba3>0) html1+='<td>'+parseFloat(tot3).toFixed(2)+'%<input id="super1" type="hidden" value="'+super1+'"></td>';
    html1+='</tr>';

    html2+='<tr>';
    if($("#1ero").val()==1 && ba1_1>0) html2+='<td>'+parseFloat(tot1_1).toFixed(2)+'%<input id="super2" type="hidden" value="'+super2+'"></td>';
    if($("#1ero").val()==1 && ba1_1==0 && $("#2do").val()==1) html2='<td>0.00%<input id="super2" type="hidden" value="'+super2+'"></td>';
    if($("#2do").val()==1 && ba2_2>0) html2+='<td>'+parseFloat(tot2_2).toFixed(2)+'%<input id="super2" type="hidden" value="'+super2+'"></td>';
    if($("#3er").val()==1 && ba3_3>0) html2+='<td>'+parseFloat(tot3_3).toFixed(2)+'%<input id="super2" type="hidden" value="'+super2+'"></td>';
    html2+='</tr>';
    html3+='<tr>';
    if($("#1ero").val()==1 && ba4_1>0) html3+='<td>'+parseFloat(tot4_1).toFixed(2)+'%<input id="super3" type="hidden" value="'+super3+'"></td>';
    if($("#1ero").val()==1 && ba4_1==0 && $("#2do").val()==1) html3+='<td>0.00%<input id="super3" type="hidden" value="'+super3+'"></td>';
    if($("#2do").val()==1 && ba4_2>0) html3+='<td>'+parseFloat(tot4_2).toFixed(2)+'%<input id="super3" type="hidden" value="'+super3+'"></td>';
    if($("#3er").val()==1 && ba4_3>0) html3+='<td>'+parseFloat(tot4_3).toFixed(2)+'%<input id="super3" type="hidden" value="'+super3+'"></td>';
    html3+='</tr>';
    html4+='<tr>';
    if($("#1ero").val()==1 && ba5_1>0) html4+='<td>'+parseFloat(tot5_1).toFixed(2)+'%<input id="super4" type="hidden" value="'+super4+'"></td>';
    if($("#1ero").val()==1 && ba5_1==0 && $("#2do").val()==1) html4+='<td>0.00%<input id="super4" type="hidden" value="'+super4+'"></td>';
    if($("#2do").val()==1 && ba5_2>0) html4+='<td>'+parseFloat(tot5_2).toFixed(2)+'%<input id="super4" type="hidden" value="'+super4+'"></td>';
    if($("#3er").val()==1 && ba5_3>0) html4+='<td>'+parseFloat(tot5_3).toFixed(2)+'%<input id="super4" type="hidden" value="'+super4+'"></td>';
    html5+='<tr>';
    if($("#1ero").val()==1 && ba6_1>0) html5+='<td>'+parseFloat(tot6_1).toFixed(2)+'%<input id="super5" type="hidden" value="'+super5+'"></td>';
    if($("#1ero").val()==1 && ba6_1==0 && $("#2do").val()==1) html5+='<td>0.00%<input id="super5" type="hidden" value="'+super5+'"></td>';
    if($("#2do").val()==1 && ba6_2>0) html5+='<td>'+parseFloat(tot6_2).toFixed(2)+'%<input id="super5" type="hidden" value="'+super5+'"></td>';
    if($("#3er").val()==1 && ba6_3>0) html5+='<td>'+parseFloat(tot6_3).toFixed(2)+'%<input id="super5" type="hidden" value="'+super5+'"></td>\
        </tr>';
    if(ba7_1>0 || ba7_2>0 || ba7_3>0){
        html6+='<tr>';
        if($("#1ero").val()==1 && ba7_1>0) html6+='<td>'+parseFloat(tot7_1).toFixed(2)+'%<input id="super6" type="hidden" value="'+super6+'"></td>';
        if($("#1ero").val()==1 && ba7_1==0 && $("#2do").val()==1) html6+='<td>0.00%<input id="super6" type="hidden" value="'+super6+'"></td>';
        if($("#2do").val()==1 && ba7_2>0) html6+='<td>'+parseFloat(tot7_2).toFixed(2)+'%<input id="super6" type="hidden" value="'+super6+'"></td>';
        if($("#3er").val()==1 && ba7_3>0) html6+='<td>'+parseFloat(tot7_3).toFixed(2)+'%<input id="super6" type="hidden" value="'+super6+'"></td>\
            </tr>';
    }
    $("#body_res_sem").append(html1+html2+html3+html4+html5+html6);

    /*Chart.defaults.global = {
        animation: true,
        animationSteps: 60,
        animationEasing: "easeOutIn",
        showScale: true,
        scaleOverride: false,
        scaleSteps: null,
        scaleStepWidth: null,
        scaleStartValue: null,
        scaleLineColor: "#eeeeee",
        scaleLineWidth: 1,
        scaleShowLabels: true,
        scaleLabel: "<%=value%>",
        scaleIntegersOnly: true,
        scaleBeginAtZero: false,
        scaleFontSize: 12,
        scaleFontStyle: "normal",
        scaleFontColor: "#717171",
        responsive: true,
        maintainAspectRatio: true,
        showTooltips: true,
        multiTooltipTemplate: "<%= value %>",
        tooltipFillColor: "#333333",
        tooltipEvents: ["mousemove", "touchstart", "touchmove"],
        tooltipTemplate: "Semana <%if (label){%><%=label%>: <%}%><%= value %>",
        tooltipFontSize: 14,
        tooltipFontStyle: "normal",
        tooltipFontColor: "#fff",
        tooltipTitleFontSize: 16,
        TitleFontStyle : "Raleway",
        tooltipTitleFontStyle: "bold",
        tooltipTitleFontColor: "#ffffff",
        tooltipYPadding: 10,
        tooltipXPadding: 10,
        tooltipCaretSize: 8,
        tooltipCornerRadius: 6,
        tooltipXOffset: 5,
        onAnimationProgress: function() {},
        onAnimationComplete: function() {}
    };*/
    color="#00CC00";
    if(tot1>=90)
         color="#00CC00";
    else if(tot1>=80 && tot1<90)
         color="#FFFF00";
    else if(tot1<=79)
         color="#FF0000";

    if(tot2>=90)
         color1_2="#00CC00";
    else if(tot2>=80 && tot2<90)
         color1_2="#FFFF00";
    else if(tot2<=79)
         color1_2="#FF0000";

    if(tot3>=90)
         color1_3="#00CC00";
    else if(tot3>=80 && tot3<90)
         color1_3="#FFFF00";
    else if(tot3<=79)
         color1_3="#FF0000";

    // --
    if(tot1_1>=90)
         color2="#00CC00";
    else if(tot1_1>=80 && tot1_1<90)
         color2="#FFFF00";
    else if(tot1_1<=79)
         color2="#FF0000";

    if(tot2_2>=90)
         color2_2="#00CC00";
    else if(tot2_2>=80 && tot2_2<90)
         color2_2="#FFFF00";
    else if(tot2_2<=79)
         color2_2="#FF0000";

    if(tot3_3>=90)
         color2_3="#00CC00";
    else if(tot3_3>=80 && tot3_3<90)
         color2_3="#FFFF00";
    else if(tot3_3<=79)
         color2_3="#FF0000";
    // --

    if(tot4_1>=90)
         color3="#00CC00";
    else if(tot4_1>=80 && tot4_1<90)
         color3="#FFFF00";
    else if(tot4_1<=79)
         color3="#FF0000";
    
    if(tot4_2>=90)
         color3_2="#00CC00";
    else if(tot4_2>=80 && tot4_2<90)
         color3_2="#FFFF00";
    else if(tot4_2<=79)
         color3_2="#FF0000"; 

    if(tot4_3>=90)
         color3_3="#00CC00";
    else if(tot4_3>=80 && tot4_3<90)
         color3_3="#FFFF00";
    else if(tot4_3<=79)
         color3_3="#FF0000"; 

    // --
    if(tot5_1>=90)
         color4="#00CC00";
    else if(tot5_1>=80 && tot5_1<90)
         color4="#FFFF00";
     else if(tot5_1<=79)
         color4="#FF0000";

    if(tot5_2>=90)
         color4_2="#00CC00";
    else if(tot5_2>=80 && tot5_2<90)
         color4_2="#FFFF00";
     else if(tot5_2<=79)
         color4_2="#FF0000"; 

    if(tot5_3>=90)
         color4_3="#00CC00";
    else if(tot5_3>=80 && tot5_3<90)
         color4_3="#FFFF00";
     else if(tot5_3<=79)
         color4_3="#FF0000"; 

     // --
    if(tot6_1>=90)
         color5="#00CC00";
    else if(tot6_1>=80 && tot6_1<90)
         color5="#FFFF00";
     else if(tot6_1<=79)
         color5="#FF0000";

    if(tot6_2>=90)
         color5_2="#00CC00";
    else if(tot6_2>=80 && tot6_2<90)
         color5_2="#FFFF00";
     else if(tot6_2<=79)
         color5_2="#FF0000"; 

    if(tot6_3>=90)
         color5_3="#00CC00";
    else if(tot6_3>=80 && tot6_3<90)
         color5_3="#FFFF00";
     else if(tot6_3<=79)
         color5_3="#FF0000";

    color6=""; color6_2=""; color6_3="";
    if(ba7_1>0 || ba7_2>0 || ba7_3>0){
        if(tot7_1>=90)
            color6="#00CC00";
        else if(tot7_1>=80 && tot7_1<90)
            color6="#FFFF00";
        else if(tot7_1<=79)
            color6="#FF0000";

        if(tot7_2>=90)
            color6_2="#00CC00";
        else if(tot7_2>=80 && tot7_2<90)
            color6_2="#FFFF00";
        else if(tot7_2<=79)
            color6_2="#FF0000"; 

        if(tot7_3>=90)
            color6_3="#00CC00";
        else if(tot7_3>=80 && tot7_3<90)
            color6_3="#FFFF00";
        else if(tot7_3<=79)
            color6_3="#FF0000";    
    }

    /*var barData = {
        labels: ["1RO", "2DO", "3RO","OBJETIVO"],
        datasets: [{
            label: "Series 1 punto 1RO",
            fillColor: [ color ],
            strokeColor: [color],
            highlightFill: [color],
            highlightStroke: [color],
            data: [tot1,tot2,tot3]
        },
        {
            label: "Series 2 punto 1RO",
            fillColor: [color2],
            strokeColor: [color2],
            highlightFill: [color2],
            highlightStroke: [color2],
            data: [tot1_1,tot2_2,tot3_3]
        },
        {
            label: "Series 3 punto 1RO",
            fillColor: [color3],
            strokeColor: [color3],
            highlightFill: [color3],
            highlightStroke: [color3],
            data: [tot4_1,tot4_2,tot4_3]
        },{
            label: "Series 3 punto 1RO",
            fillColor: color4,
            strokeColor: color4,
            highlightFill: color4,
            highlightStroke: color4,
            data: [tot5_1,tot5_2,tot5_3]
        },
        {
            label: "OBJETIVO",
            fillColor: "rgba(0, 204, 0, 0.9)",
            strokeColor: "rgba(0, 204, 0, 0.9)",
            highlightFill: "rgba(0, 204, 0, 0.9)",
            highlightStroke: "rgba(0, 204, 0, 0.9)",
            data: ["","","",100]
        }],
    };

    var barOptions = {
        scaleBeginAtZero: true,
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,0.1)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke: true,
        barStrokeWidth: 2,
        barValueSpacing: 5,
        barDatasetSpacing: 2
    };

    var barCtx = document.getElementById("graph_turnos").getContext("2d");
    var myBarChart = new Chart(barCtx).Bar(barData, barOptions);
    */

    if ($("#graph_turnos_2").length > 0) {
        /*var data = google.visualization.arrayToDataTable([
            ['TURNO',String(sem1), String(sem2), String(sem3), String(sem4),'OBJETIVO'],
            ["1RO",tot1,tot1_1,tot4_1,tot5_1,100],
            ["2DO",tot2,tot2_2,tot4_2,tot5_2,100],
            ["3RO",tot3,tot3_3,tot4_3,tot5_3,100],
        ]);*/
        var ar_1ero = ['','','','',100];var ar_1ero2 = ['','','','',100];var ar_1ero3 = ['','','','',100];var ar_1ero4 = ['','','','',100]; var ar_1ero5 = ['','','','',100]; var ar_1ero6 = ['','','','',100]; 
        var ar_2do = ['','','','',100];var ar_2do2 = ['','','','',100];var ar_2do3 = ['','','','',100];var ar_2do4 = ['','','','',100]; var ar_2do5 = ['','','','',100]; var ar_2do6 = ['','','','',100]; 
        var ar_3ro = ['','','','',100];var ar_3ro2 = ['','','','',100];var ar_3ro3 = ['','','','',100];var ar_3ro4 = ['','','','',100]; var ar_3ro5 = ['','','','',100]; var ar_3ro6 = ['','','','',100]; 
        if($("#1ero").val()==1 && color6==""){
            ar_1ero =[' ', tot1, 'color:'+color+'', tot1,100];
            ar_1ero2=['1', tot1_1, 'color:'+color2+'', tot1_1,100];
            ar_1ero3=['E', tot4_1, 'car_1eroolor:'+color3+'', tot4_1,100];
            ar_1ero4=['R', tot5_1, 'color: '+color4+'', tot5_1,100];
            ar_1ero5=['O', tot6_1, 'color: '+color5+'', tot6_1,100];
        }
        if($("#1ero").val()==1 && color6!=""){
            ar_1ero =[' ', tot1, 'color:'+color+'', tot1,100];
            ar_1ero2=['1', tot1_1, 'color:'+color2+'', tot1_1,100];
            ar_1ero3=['E', tot4_1, 'car_1eroolor:'+color3+'', tot4_1,100];
            ar_1ero4=['R', tot5_1, 'color: '+color4+'', tot5_1,100];
            ar_1ero5=['O', tot6_1, 'color: '+color5+'', tot6_1,100];
            ar_1ero6=['O', tot7_1, 'color: '+color6+'', tot7_1,100];
        }
        if($("#2do").val()==1 && color6_2==""){
            ar_2do =[' ', tot2, 'color:'+color1_2+'', tot2,100];
            ar_2do2= ['2', tot2_2, 'color:'+color2_2+'', tot2_2,100];
            ar_2do3=['D', tot4_2, 'color:'+color3_2+'', tot4_2,100];
            ar_2do4=['O', tot5_2, 'color: '+color4_2+'', tot5_2,100];
            ar_2do5=['O', tot6_2, 'color: '+color5_2+'', tot6_2,100];
        }
        if($("#2do").val()==1 && color6_2!=""){
            ar_2do =[' ', tot2, 'color:'+color1_2+'', tot2,100];
            ar_2do2= ['2', tot2_2, 'color:'+color2_2+'', tot2_2,100];
            ar_2do3=['D', tot4_2, 'color:'+color3_2+'', tot4_2,100];
            ar_2do4=['O', tot5_2, 'color: '+color4_2+'', tot5_2,100];
            ar_2do5=['O', tot6_2, 'color: '+color5_2+'', tot6_2,100];
            ar_2do6=['O', tot7_2, 'color: '+color6_2+'', tot7_2,100];
        }
        if($("#3er").val()==1 && color6_3==""){
            ar_3ro =[' ', tot3, 'color:'+color1_3+'', tot3,100];
            ar_3ro2=['3', tot3_3, 'color:'+color2_3+'', tot3_3,100];
            ar_3ro3=['R', tot4_3, 'color:'+color3_3+'', tot4_3,100];
            ar_3ro4=['O', tot5_3, 'color: '+color4_3+'', tot5_3,100];
            ar_3ro5=['O', tot6_3, 'color: '+color5_3+'', tot6_3,100];
        }
        if($("#3er").val()==1 && color6_3!=""){
            ar_3ro =[' ', tot3, 'color:'+color1_3+'', tot3,100];
            ar_3ro2=['3', tot3_3, 'color:'+color2_3+'', tot3_3,100];
            ar_3ro3=['R', tot4_3, 'color:'+color3_3+'', tot4_3,100];
            ar_3ro4=['O', tot5_3, 'color: '+color4_3+'', tot5_3,100];
            ar_3ro5=['O', tot6_3, 'color: '+color5_3+'', tot6_3,100];
            ar_3ro6=['O', tot7_3, 'color: '+color6_3+'', tot7_3,100];
        }
        if($("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==0 && color6==""){//solo 1er turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,
                [' ', "", "", "", 100],
            ]);
        }if($("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==0 && color6!=""){//solo 1er turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,ar_1ero6,
                [' ', "", "", "", 100],
            ]);
        }

        if($("#1ero").val()==0 && $("#2do").val()==1 && $("#3er").val()==0 && color6_2==""){//2do turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_2do,ar_2do2,ar_2do3,ar_2do4,ar_2do5,
                [' ', "", "", "", 100],
            ]);
        }if($("#1ero").val()==0 && $("#2do").val()==1 && $("#3er").val()==0 && color6_2!=""){//2do turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_2do,ar_2do2,ar_2do3,ar_2do4,ar_2do5,ar_2do6,
                [' ', "", "", "", 100],
            ]);
        }

        if($("#1ero").val()==0 && $("#2do").val()==0 && $("#3er").val()==1 && color6_3==""){//3er turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5,
                [' ', "", "", "", 100],
            ]);
        }if($("#1ero").val()==0 && $("#2do").val()==0 && $("#3er").val()==1 && color6_3!=""){//3er turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5,ar_3ro6,
                [' ', "", "", "", 100],
            ]);
        }

        if($("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==1 && color6=="" || $("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==1 && color6_3==""){ //1ero y 3er turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,
                [' ', "", "", "", 100],
                ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5,
                [' ', "", "", "", 100],
            ]);
        }if($("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==1 && color6!="" || $("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==1 && color6_3!=""){ //1ero y 3er turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,ar_1ero6,
                [' ', "", "", "", 100],
                ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5,ar_3ro6,
                [' ', "", "", "", 100],
            ]);
        }

        if($("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==0 && color6=="" || $("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==0 && color6_2==""){ //1ero y 2do turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,
                [' ', "", "", "", 100],
                ar_2do,ar_2do2,ar_2do3,ar_2do4,ar_2do5,
                [' ', "", "", "", 100],
            ]);
        }if($("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==0 && color6!="" || $("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==0 && color6_2!=""){ //1ero y 2do turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,ar_1ero6,
                [' ', "", "", "", 100],
                ar_2do,ar_2do2,ar_2do3,ar_2do4,ar_2do5,ar_2do6,
                [' ', "", "", "", 100],
            ]);
        }

        if($("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==1 && color6=="" || $("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==1 && color6_3==""){ //1ero y 3er turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,
                [' ', "", "", "", 100],
                ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5,
                [' ', "", "", "", 100],
            ]);
        }if($("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==1 && color6!="" || $("#1ero").val()==1 && $("#2do").val()==0 && $("#3er").val()==1 && color6_3!=""){ //1ero y 3er turno
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,ar_1ero6,
                [' ', "", "", "", 100],
                ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5,ar_3ro6,
                [' ', "", "", "", 100],
            ]);
        }

        if($("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==1 && color6=="" || $("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==1 && color6_2==""
            || $("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==1 && color6_3==""){ //los 3 turnos
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,
                [' ', "", "", "", 100],
                ar_2do,ar_2do2,ar_2do3,ar_2do4,ar_2do5,
                [' ', "", "", "", 100],
                ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5,
            ]);
        }
        if($("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==1 && color6!="" || $("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==1 && color6_2!=""
            || $("#1ero").val()==1 && $("#2do").val()==1 && $("#3er").val()==1 && color6_3!=""){ //los 3 turnos
            var data = google.visualization.arrayToDataTable([
                ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
                ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,ar_1ero6,
                [' ', "", "", "", 100],
                ar_2do,ar_2do2,ar_2do3,ar_2do4,ar_2do5,ar_2do6,
                [' ', "", "", "", 100],
                ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5,ar_3ro6,
            ]);
        }
        /*var data = google.visualization.arrayToDataTable([
            ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
            ar_1ero, ar_1ero2, ar_1ero3, ar_1ero4, ar_1ero5,
            [' ', "", "", "", 100],
            ar_2do,ar_2do2,ar_2do3,ar_2do4,ar_2do5,
            [' ', "", "", "", 100],
            ar_3ro,ar_3ro2,ar_3ro3,ar_3ro4,ar_3ro5
        ]);*/
        var options = {
                title : 'CUMPLIMIENTO OTS POR TURNO',
                vAxis: {title: 'CUMPLIMIENTO'},
                hAxis: {title: 'TURNO'},
                seriesType: 'bars',
                series: {1: {type: 'line',color:'#80FF33'}},
                height: 500,
                width:'100%',
                //bar: { groupWidth: '80%' },
                //colors: ["#567fc9", vihoAdminConfig.primary, "#222222", "#717171", "#19d119"]
                //colors: [color,color2, color3, color4, "#19d119"]
                /*colors: [color,color2, color3, color4, "#19d119",
                    color1_2,color2_2, color3_2, color4_2, "#19d119",      
                    color1_3,color2_3, color3_3, color4_3, "#19d119",      
                ]*/
        };
        var chart = new google.visualization.ComboChart(document.getElementById('graph_turnos_2'));
        //chart.draw(data, options,options2,options3);
        chart.draw(data, options);
    }

    setTimeout(function(){ 
        resultadosTotalSemanal();
    }, 1500);
}

function resultadosTotalSemanal(){
    let html1=""; let html2=""; let html3=""; let html4=""; let html5=""; let html6="";

    super1=Number($("#super1").val());
    super2=Number($("#super2").val());
    super3=Number($("#super3").val());
    super4=Number($("#super4").val());
    super5=Number($("#super5").val());
    super6=Number($("#super6").val());

    if(isNaN(super1))
        super1=0;
    if(isNaN(super2))
        super2=0;
    if(isNaN(super3))
        super3=0;
    if(isNaN(super4))
        super4=0;
    if(isNaN(super5))
        super5=0;
    if(isNaN(super6))
        super6=0;
    if(sem1>0){
        html1+='<tr>\
            <td>'+sem1+'</td>\
            <td>'+parseFloat(super1).toFixed(2)+'%</td>\
            <td>100%</td>\
        </tr>';
    }
    if(sem2>0){
        html2+='<tr>\
            <td>'+sem2+'</td>\
            <td>'+parseFloat(super2).toFixed(2)+'%</td>\
            <td>100%</td>\
        </tr>';
    }
    if(sem3>0){
        html3+='<tr>\
            <td>'+sem3+'</td>\
            <td>'+parseFloat(super3).toFixed(2)+'%</td>\
            <td>100%</td>\
        </tr>';
    }
    if(sem4>0){
        html4+='<tr>\
            <td>'+sem4+'</td>\
            <td>'+parseFloat(super4).toFixed(2)+'%</td>\
            <td>100%</td>\
        </tr>';
    }
    if(sem5>0){
        html5+='<tr>\
            <td>'+sem5+'</td>\
            <td>'+parseFloat(super5).toFixed(2)+'%</td>\
            <td>100%</td>\
        </tr>';
    }
    if(sem6>0){
        html6+='<tr>\
            <td>'+sem6+'</td>\
            <td>'+parseFloat(super6).toFixed(2)+'%</td>\
            <td>100%</td>\
        </tr>';
    }
    $("#body_total_sem").append(html1+html2+html3+html4+html5+html6);

    /*
    Chart.defaults.global = {
        animation: true,
        animationSteps: 60,
        animationEasing: "easeOutIn",
        showScale: true,
        scaleOverride: false,
        scaleSteps: null,
        scaleStepWidth: null,
        scaleStartValue: null,
        scaleLineColor: "#eeeeee",
        scaleLineWidth: 1,
        scaleShowLabels: true,
        scaleLabel: "<%=value%>",
        scaleIntegersOnly: true,
        scaleBeginAtZero: false,
        scaleFontSize: 12,
        scaleFontStyle: "normal",
        scaleFontColor: "#717171",
        responsive: true,
        maintainAspectRatio: true,
        showTooltips: true,
        multiTooltipTemplate: "<%= value %>",
        tooltipFillColor: "#333333",
        tooltipEvents: ["mousemove", "touchstart", "touchmove"],
        tooltipTemplate: "Semana <%if (label){%><%=label%>: <%}%><%= value %>",
        tooltipFontSize: 14,
        tooltipFontStyle: "normal",
        tooltipFontColor: "#fff",
        tooltipTitleFontSize: 16,
        TitleFontStyle : "Raleway",
        tooltipTitleFontStyle: "bold",
        tooltipTitleFontColor: "#ffffff",
        tooltipYPadding: 10,
        tooltipXPadding: 10,
        tooltipCaretSize: 8,
        tooltipCornerRadius: 6,
        tooltipXOffset: 5,
        onAnimationProgress: function() {},
        onAnimationComplete: function() {}
    };
    var barData = {
        labels: [sem1, sem2, sem3, sem4,"OBJETIVO"],
        datasets: [{
            label: "Resultados de cumplimiento semanal",
            fillColor: "rgba(68, 114, 196, 0.9)",
            strokeColor: vihoAdminConfig.primary,
            highlightFill: "rgba(68, 114, 196, 0.9)",
            highlightStroke: vihoAdminConfig.primary,
            data: [super1, super2, super3, super4]
        },
        {
            label: "Resultados de cumplimiento semanal",
            fillColor: "rgba(0, 204, 0, 0.9)",
            strokeColor: vihoAdminConfig.primary,
            highlightFill: "rgba(0, 204, 0, 0.9)",
            highlightStroke: vihoAdminConfig.primary,
            data: ["","","","",100]
        }]
    };
    var barCtx = document.getElementById("graph_semana").getContext("2d");
    var myBarChart = new Chart(barCtx).Bar(barData, barOptions);*/

    sem1_str = String(sem1); sem2_str = String(sem2); sem3_str = String(sem3); sem4_str = String(sem4); sem5_str = String(sem5); sem6_str = String(sem6);
    super1=parseFloat(super1).toFixed(2); super2=parseFloat(super2).toFixed(2); super3=parseFloat(super3).toFixed(2); super4=parseFloat(super4).toFixed(2); super5=parseFloat(super5).toFixed(2);
    super6=parseFloat(super6).toFixed(2);
    //console.log("sem1_str :"+sem1_str);
    //console.log("super1 :"+super1);
    arraysem1=["","","","",100];
    arraysem2=["","","","",100];
    arraysem3=["","","","",100];
    arraysem4=["","","","",100];
    arraysem5=["","","","",100];
    arraysem6=["","","","",100];
    if(sem1>0){
        arraysem1 = [sem1_str, Number(super1), 'stroke-color: #a1b7df; stroke-width: 4; fill-color: #567fc9', super1,100 ];
    }
    if(sem2>0){
        arraysem2 = [sem2_str, Number(super2), 'stroke-color: #a1b7df; stroke-width: 4; fill-color: #567fc9', super2,100 ];
    }
    if(sem3>0){
        arraysem3 = [sem3_str, Number(super3), 'stroke-color: #a1b7df; stroke-width: 4; fill-color: #567fc9', super3,100 ];
    }
    if(sem4>0){
        arraysem4 = [sem4_str, Number(super4), 'stroke-color: #a1b7df; stroke-width: 4; fill-color: #567fc9', super4,100 ];
    }
    if(sem5>0){
        arraysem5 = [sem5_str, Number(super5), 'stroke-color: #a1b7df; stroke-width: 4; fill-color: #567fc9', super5,100 ];
    }
    if(sem6>0){
        arraysem6 = [sem6_str, Number(super6), 'stroke-color: #a1b7df; stroke-width: 4; fill-color: #567fc9', super6,100 ];
    }

    //if ($("#graph_semana_2").length > 0) {
      var data2 = google.visualization.arrayToDataTable([
        //2 linesa siguientes funcionales
        /*['SEMANA', sem1_str, sem2_str, sem3_str, sem4_str, 'Cumplimiento'],
        ["Semana:",  Number(super1),Number(super2),Number(super3),Number(super4), 100],*/

        //['SEMANA', sem1_str, sem2_str, sem3_str, sem4_str, 'Cumplimiento'],
        /*['SEMANA', sem1_str, sem2_str, sem3_str, sem4_str,'OBJETIVO'],
        [sem1_str, Number(super1),0,0,0, 100],
        [sem2_str, 0,Number(super2),0,0, 100],
        [sem3_str, 0,0,Number(super3),0, 100],
        [sem4_str, 0,0,0,Number(super4), 100],*/

        ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
        arraysem1,
        arraysem2,
        arraysem3,
        arraysem4,
        arraysem5,
        arraysem6
      ]);
      var options2 = {
            title : 'CUMPLIMIENTO OTS '+$("#mes_sel").val()+'',
            vAxis: {title: 'CUMPLIMIENTO'},
            hAxis: {title: 'SEMANA'},
            seriesType: 'bars',
            series: {1: {type: 'line',color:'#80FF33'}},
            height: 500,
            width:'100%',
            bar: { groupWidth: '100%' },
            //colors: ["#567fc9", vihoAdminConfig.primary, "#222222", "#717171", "#19d119"]
            colors: ["#567fc9","#567fc9", "#567fc9", "#567fc9", "#19d119"]
        };
        var chart2 = new google.visualization.ComboChart(document.getElementById('graph_semana_2'));
        chart2.draw(data2, options2);
    //}
    
    setTimeout(function(){ 
       resultadosTotalAnual();
    }, 1500);
    //resultadosTotalAnual();

    // resultados de eficiencia por semana
    let label_sem=[]; let data_sem=[];

    sup1_parse = parseFloat(super1).toFixed(2);
    sup2_parse = parseFloat(super2).toFixed(2);
    sup3_parse = parseFloat(super3).toFixed(2);
    sup4_parse = parseFloat(super4).toFixed(2);
    sup5_parse = parseFloat(super5).toFixed(2);
    sup6_parse = parseFloat(super6).toFixed(2);

    if(sup1_parse>=99.99){
        label_sem.push(sem1);
        sup1_parse=Number(sup1_parse);
        data_sem.push(sup1_parse);
    }if(sup2_parse>=99.99){
        label_sem.push(sem2);
        sup2_parse=Number(sup2_parse);
        data_sem.push(sup2_parse);
    }if(sup3_parse>=99.99){
        label_sem.push(sem3);
        sup3_parse=Number(sup3_parse);
        data_sem.push(sup3_parse);
    }if(sup4_parse>=99.99){
        label_sem.push(sem4);
        sup4_parse=Number(sup4_parse);
        data_sem.push(sup4_parse);
    }if(sup5_parse>=99.99){
        label_sem.push(sem5);
        sup5_parse=Number(sup5_parse);
        data_sem.push(sup5_parse);
    }if(sup6_parse>=99.99){
        label_sem.push(sem6);
        sup6_parse=Number(sup6_parse);
        data_sem.push(sup6_parse);
    }

    /*console.log("sup1_parse: "+sup1_parse);
    console.log("sup2_parse: "+sup2_parse);
    console.log("sup3_parse: "+sup3_parse);
    console.log("sup4_parse: "+sup4_parse);
    console.log("data_sem: "+data_sem);*/

    /*Chart.defaults.global = {
        animation: true,
        animationSteps: 60,
        animationEasing: "easeOutIn",
        showScale: true,
        scaleOverride: false,
        scaleSteps: null,
        scaleStepWidth: null,
        scaleStartValue: null,
        scaleLineColor: "#eeeeee",
        scaleLineWidth: 1,
        scaleShowLabels: true,
        scaleLabel: "<%=value%>",
        scaleIntegersOnly: true,
        scaleBeginAtZero: false,
        scaleFontSize: 12,
        scaleFontStyle: "normal",
        scaleFontColor: "#717171",
        responsive: true,
        maintainAspectRatio: true,
        showTooltips: true,
        multiTooltipTemplate: "<%= value %>",
        tooltipFillColor: "#333333",
        tooltipEvents: ["mousemove", "touchstart", "touchmove"],
        tooltipTemplate: "Semana <%if (label){%><%=label%>: <%}%><%= value %>",
        tooltipFontSize: 14,
        tooltipFontStyle: "normal",
        tooltipFontColor: "#fff",
        tooltipTitleFontSize: 16,
        TitleFontStyle : "Raleway",
        tooltipTitleFontStyle: "bold",
        tooltipTitleFontColor: "#ffffff",
        tooltipYPadding: 10,
        tooltipXPadding: 10,
        tooltipCaretSize: 8,
        tooltipCornerRadius: 6,
        tooltipXOffset: 5,
        onAnimationProgress: function() {},
        onAnimationComplete: function() {}
    };
    var barData = {
        labels: label_sem,
        datasets: [{
            label: "Resultados de cumplimiento",
            fillColor: "rgba(0, 204, 0, 0.9)",
            strokeColor: vihoAdminConfig.primary,
            highlightFill: "rgba(0, 204, 0, 0.9",
            highlightStroke: vihoAdminConfig.primary,
            data: data_sem
        }]
    };

    var barCtx = document.getElementById("graph_sem_cumple").getContext("2d");
    var myBarChart = new Chart(barCtx).Bar(barData, barOptions);

    setTimeout(function(){ 
        saveGraph("graph_sem_cumple",7);
    }, 3500);*/
    
}

function resultadosTotalAnual(){
    /*var lineGraphOptions = {
        scaleShowGridLines: true,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 3,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        bezierCurve: true,
        bezierCurveTension: 0.4,
        pointDot: true,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
    };
    var lineGraphData = {
        labels: [$("#mes_sel").val()],
        datasets: [{
            label: $("#mes_sel").val(),
            fillColor: "rgba(36, 105, 92, 0.5)",
            strokeColor: vihoAdminConfig.primary,
            pointColor: vihoAdminConfig.primary,
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "#000",
            data: [$("#super_tot").val()]
        }]
    };
    var lineCtx = document.getElementById("graph_anual").getContext("2d");
    var myLineCharts = new Chart(lineCtx).Line(lineGraphData, lineGraphOptions);
    */

    var MESES = []; var DATOS = [];
    var TABLAAN   = $("#table_anual tbody tr > td");
    TABLAAN.each(function(){  
        meses = $(this).find("input[id*='mes_anual']").val();
        total_anual = parseFloat($(this).find("input[id*='total_anual']").val()).toFixed(2);
        MESES.push(meses);
        DATOS.push(total_anual);
    });
    //console.log("DATOS: "+MESES);
    //console.log("MESES: "+DATOS);

    //let super_tot=$("#super_tot").val();
    //super_tot2=parseFloat(super_tot).toFixed(2);
    var myLineChart = {
        //labels: ["","10", "20", "30", "40", "50", "60", "70", "80"],
        labels: MESES,
            datasets: [{
                label: "Cumplimiento",
                backgroundColor: "rgba(0, 191, 255, 0.9)",
                strokeColor: "#00bfff",
                pointColor: "#80FF33",
                data: DATOS
            }]
    };
    var horizonalLinePlugin = {
      afterDraw: function(chartInstance) {
        var yScale = chartInstance.scales["y-axis-0"];
        var canvas = chartInstance.chart;
        var ctx = canvas.ctx;
        var index;
        var line;
        var style;
        if (chartInstance.options.horizontalLine) {
          for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
            line = chartInstance.options.horizontalLine[index];
            if (!line.style) {
              style = "rgba(0,191,255, .6)";
            } else {
              style = line.style;
            }
            if (line.y) {
              yValue = yScale.getPixelForValue(line.y);
            } else {
              yValue = 0;
            }
            ctx.lineWidth = 3;
            if (yValue) {
              ctx.beginPath();
              ctx.moveTo(0, yValue);
              ctx.lineTo(canvas.width, yValue);
              ctx.strokeStyle = style;
              ctx.stroke();
            }
            if (line.text) {
              ctx.fillStyle = style;
              ctx.fillText(line.text, 0, yValue + ctx.lineWidth);
            }
          }
          return;
        };
      }
    };
    Chart.pluginService.register(horizonalLinePlugin);

    var ctx = document.getElementById("graph_anual").getContext("2d");
    var LineChartDemo = new Chart(ctx, {
        type: 'line',
        data: myLineChart,
        options: {
            "horizontalLine": [{
              "y": 100,
              "style": "rgba(128, 255, 1, .4)",
              "text": "Objetivo",
              "fillColor": "#00bfff",
            }],
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 20
                    }
                }],
            },
        }
    });
    setTimeout(function(){ 
        saveGraph("graph_anual",3);
    }, 3500);   
}

////////////////////////////////////////////
function tablaSemanaActividades(){

    $.ajax({
        type:'POST',
        url: base_url+'Reportes/getActivSemana',
        async:false,
        data: { id_proy:$("#id_proy").val(), semana:$("#semana_act option:selected").val(), anio:$("#anio").val() },
        success:function(data){
            //console.log(data);
            $("#chartWithMarkerOverlay").html("");
            $("#graph_eficiencia").html("");
            $("#canvastg3").html("");
            var array = $.parseJSON(data);
            $("#body_table_rep_sem").html(array.html);
            $("#foot_table_rep_sem").html(array.html_foot);
            //console.log("logo: "+array.logo);
            //$("#logo_cli").attr("src",base_url+"uploads/clientes/"+array.logo);
            $("#name_cli").html("CLIENTE: "+ array.empresa);
            $("#fecha_ini").html(array.fecha_ini);
            $("#fecha_final").html(array.fecha_final);
            $("#mes_anio").html($("#mes_sel").val()+" "+array.anio+" / ¿Cuándo?");
            $("#num_semana").html("SEMANA "+$("#semana_act option:selected").val());

            var datos = array.html_head;
            //console.log("datos.1: "+datos[0]);
            $("#day1").html(datos[0]);
            $("#day2").html(datos[1]);
            $("#day3").html(datos[2]);
            $("#day4").html(datos[3]);
            $("#day5").html(datos[4]);
            $("#day6").html(datos[5]);
            $("#day7").html(datos[6]);
            //resultadosTotalCumplimiento(array.anio);

            $("#head_fin_eficacia").html($("#fin_eficacia").html());
            //let head_fin_eficiencia=Number($("#fin_ok").html())/180*100;
            let head_fin_eficiencia=$("#head_fin_eficacia").html();
            $("#head_fin_eficiencia").html(parseFloat(head_fin_eficiencia).toFixed(2)+"%");

            setTimeout(function(){ 
                resultadosTotalCumplimiento(array.anio);
            }, 2000);
        }
    });
}

function resultadosTotalCumplimiento(anio){
    cont_ok=Number($("#cont_ok").html());
    cont_ok2=Number($("#cont_ok2").html());
    cont_ok3=Number($("#cont_ok3").html());
    cont_ok4=Number($("#cont_ok4").html());
    cont_ok5=Number($("#cont_ok5").html());
    cont_ok6=Number($("#cont_ok6").html());
    cont_ok7=Number($("#cont_ok7").html());

    cont_pro=Number($("#cont_pro").html());
    cont_pro2=Number($("#cont_pro2").html());
    cont_pro3=Number($("#cont_pro3").html());
    cont_pro4=Number($("#cont_pro4").html());
    cont_pro5=Number($("#cont_pro5").html());
    cont_pro6=Number($("#cont_pro6").html());
    cont_pro7=Number($("#cont_pro7").html());

    efi=$("#efi").html();
    efi=efi.replace("%", "");
    efi2=$("#efi2").html();
    efi2=efi2.replace("%", "");
    efi3=$("#efi3").html();
    efi3=efi3.replace("%", "");
    efi4=$("#efi4").html();
    efi4=efi4.replace("%", "");
    efi5=$("#efi5").html();
    efi5=efi5.replace("%", "");
    efi6=$("#efi6").html();
    efi6=efi6.replace("%", "");
    efi7=$("#efi7").html();
    efi7=efi7.replace("%", "");

    //if ($("#graph_cumplimiento").length > 0) {
      var data2 = google.visualization.arrayToDataTable([
        ['', 'Cump.', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
        ["1", cont_ok, 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', cont_ok, cont_pro ],
        ["2", cont_ok2, 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', cont_ok2, cont_pro2 ],
        ["3", cont_ok3, 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', cont_ok3, cont_pro3 ],
        ["4", cont_ok4, 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', cont_ok4, cont_pro4 ],
        ["5", cont_ok5, 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', cont_ok5, cont_pro5 ],
        ["6", cont_ok6, 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', cont_ok6, cont_pro6 ],
        ["7", cont_ok7, 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', cont_ok7, cont_pro7 ]

      ]);
      var options2 = {
            title : 'Cumplimiento al programa de limpieza semana '+$("#semana_act option:selected").val()+'/'+anio+'',
            vAxis: {title: 'LIMPIEZAS REALIZADAS DIARIAMENTE'},
            hAxis: {title: 'DÍAS CORRESPONDIENTES A SEMANA '+$("#semana_act option:selected").val()+'/'+anio+''},
            seriesType: 'bars',
            series: {1: {type: 'line',color:'#80FF33'}},
            height: 500,
            width:'100%',
            bar: { groupWidth: '100%' },
            //colors: ["#567fc9", vihoAdminConfig.primary, "#222222", "#717171", "#19d119"]
            colors: ["#77C7F8"]
        };
        var chart2 = new google.visualization.ComboChart(document.getElementById('graph_cumplimiento'));
        chart2.draw(data2, options2);
    //}
    

    var data = google.visualization.arrayToDataTable([
        ['', '', { role: 'style' }, { role: 'annotation' }, "OBJETIVO" ],
        ["1", Number(efi), 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', Number(efi), Number(100) ],
        ["2", Number(efi2), 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', Number(efi2), Number(100) ],
        ["3", Number(efi3), 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', Number(efi3), Number(100) ],
        ["4", Number(efi4), 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', Number(efi4), Number(100) ],
        ["5", Number(efi5), 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', Number(efi5), Number(100) ],
        ["6", Number(efi6), 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', Number(efi6), Number(100) ],
        ["7", Number(efi7), 'stroke-color: #33B2FF; stroke-width: 4; fill-color: #77C7F8', Number(efi7), Number(100) ]

    ]);
    var options = {
            title : 'EFICACIA DE LIMPIEZAS REALIZADAS '+$("#semana_act option:selected").val()+'/'+anio+'',
            vAxis: {title: '', ticks: [0,25,50,75,100] },
            hAxis: {title: 'DÍAS CORRESPONDIENTES A SEMANA '+$("#semana_act option:selected").val()+'/'+anio+''},
            seriesType: 'bars',
            series: {1: {type: 'line',color:'#80FF33'}},
            height: 500,
            width:'100%',
            bar: { groupWidth: '100%' },
            //colors: ["#567fc9", vihoAdminConfig.primary, "#222222", "#717171", "#19d119"]
            colors: ["#77C7F8"],
            /*annotations: {
                boxStyle: {
                  // Color of the box outline.
                  stroke: '#888',
                  // Thickness of the box outline.
                  strokeWidth: 2,
                  // x-radius of the corner curvature.
                  rx: 40,
                  // y-radius of the corner curvature.
                  ry: 40,
                  // Attributes for linear gradient fill.
                  gradient: {
                    // Start color for gradient.
                    color1: '#fbf6a7',
                    // Finish color for gradient.
                    color2: '#33b679',
                    // Where on the boundary to start and
                    // end the color1/color2 gradient,
                    // relative to the upper left corner
                    // of the boundary.
                    x1: '0%', y1: '0%',
                    x2: '100%', y2: '100%',
                    // If true, the boundary for x1,
                    // y1, x2, and y2 is the box. If
                    // false, it's the entire chart.
                    useObjectBoundingBoxUnits: true
                  }
                }
            }*/
    };

    function placeMarker(dataTable) {
        var cli = this.getChartLayoutInterface();
        var chartArea = cli.getChartAreaBoundingBox();
        document.querySelector('.overlay-marker').style.top = Math.floor(cli.getYLocation(dataTable.getValue(5, 1))) - 50 + "px";
        document.querySelector('.overlay-marker').style.left = Math.floor(cli.getXLocation(5)) - 10 + "px";
    };
    
    var chart = new google.visualization.ComboChart(document.getElementById('graph_eficiencia'));
    chart.draw(data, options);

    setTimeout(function(){  
       //saveGraph();
        html2canvas(document.querySelector(".chartWithMarkerOverlay")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg3');
            saveTable("canvastg3",7,$("#semana_act option:selected").val()); //grafica eficiencia
        });
    }, 2500);

}

function saveGraph(idname,tipo,semana=0){
    var canvasx= document.getElementById(idname);
    var dataURL = canvasx.toDataURL('image/png', 1);
    //console.log(dataURL);
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/insertGraph',
        data: {
            id:0,
            id_proyecto:$("#id_proy").val(),
            grafica:dataURL,
            mes:$("#mes").val(),
            anio:$("#anio").val(),
            tipo:tipo,
            semana:semana
        },
        success:function(data){
            //console.log("hecho");
            if(tipo==10){
                setTimeout(function(){
                    $.unblockUI();
                }, 4500);
            }
        }
    });
}

