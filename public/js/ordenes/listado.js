var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
    $("#semana,#turno,#anio,#frecuencia,#tipo_act,#id_act").on("change",function(){
        //loadtable();
        /*if($("#tipo option:selected").val()==1){
            loadtable();
        }else{ 
            loadtableDiario();
        }*/
        loadtable();
    });

    $("#semana").on("change",function(){
        changeFiltro();
    });
    $("#id_act").on("change",function(){
        changeFiltro();
    });
    $("#tipo").on("change",function(){
        /*if($("#tipo option:selected").val()==1){
            loadtable();
        }else{ 
            loadtableDiario();
        }*/
        loadtable();
    });

    $("#edit_status").on("click",function(){
        cambiaEstatus();
    });

    $("#edit_status2").on("click",function(){
        cambiarEstatus2();
    });

    $("#upload_pre").on("click",function(){
        modalCarga();
    });
    $("#aceptar_carga").on("click",function(){
        if($("#inputFile").val()!="")
            $('#inputFile').fileinput('upload');
        else
            swal("Error", "Seleccione un documento a cargar", "warning");
    });

    $("#id_act").select2({});

    $('#elimina_selec').on('click', function(){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Está seguro de eliminar las ordenes seleccionadas?',
            type: 'blue',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    var DATA  = [];
                    var TABLA   = $("#table_ordenes tbody > tr");
                    TABLA.each(function(){    
                        if ($(this).find("input[id*='id_ord']").is(':checked')) {
                            item = {};     
                            item ["id_ord"] = $(this).find("input[id*='id_ord']").val();
                            DATA.push(item);
                        }
                    });
                    ords   = JSON.stringify(DATA);
                    //console.log(ords);
                    $.ajax({
                        type: 'POST',
                        url: base_url+'Ordenes/delete_select',
                        data: {
                            ordenes:ords,
                        },
                        success: function(data) {
                           swal("Éxito", "Se han eliminado correctamente", "success");
                           loadtable();
                        },
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        }); 
        
    });
});

function changeFiltro(){
    $.ajax({
        type:'POST',
        url: base_url+"Ordenes/cambiaFiltro",
        data: {
            semana:$("#semana option:selected").val(), id_act:$("#id_act option:selected").val()
        },
        success:function(response){  
            //console.log("filtro cambia");
        }
    });
}

function loadtable(){
    $("#cont_table_ordenes_diario").hide("slow");
    $("#cont_table_ordenes").show("slow");
    tabla=$("#table_ordenes").DataTable({
        
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        responsive: !0,
        stateSave:true,
        "ajax": {
            "url": base_url+"Ordenes/getDataTable",
            type: "post",
            data: { id_proy:$("#id_proy").val(), id_cliente:$("#id_cli").val(),semana:$("#semana option:selected").val(),anio:$("#anio option:selected").val(), turno:$("#turno").val(), tipo:$("#tipo option:selected").val(),frecuencia:$("#frecuencia option:selected").val(),id_act:$("#id_act option:selected").val()},
            error: function(){
               $("#table_ordenes").css("display","none");
            }
        },
        "columns": [
            {"data": "[]"},
            {"data":"id"},
            {"data":"ot"},
            //{"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    html=row.nombre;
                    /*if(row.tipo_act=="1"){
                        html=row.nombre;
                    }else{
                        html=row.nombre_crit;
                    }*/
                    /*if(row.nombre_crit==null){
                      html="";  
                    }*/
                    return html;
                }
            },
            {"data":"fecha"},
            {"data":"semana"},
            {"data":"per_name"},
            /*{"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.firma!=""){
                        html="Cargada";
                    }
                    return html;
                }
            },*/
            {"data":"empresa"},
            /*{"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.firma_cliente!=""){
                        html="Cargada";
                    }
                    return html;
                }
            },*/
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.seguimiento=="1"){ //cancelado
                        html='<button class="btn btn-danger" type="button" data-bs-original-title="" title="">X</button>';
                    }else if(row.seguimiento=="2"){ //programado
                        html='<button class="btn btn-info" type="button" data-bs-original-title="" title="">P</button>';
                    }else if(row.seguimiento=="3"){ //ok
                        html='<button class="btn btn-success" type="button" data-bs-original-title="" title="">1</button>';
                    }else if(row.seguimiento=="4"){ //reprogramado
                        html='<button class="btn btn-warning" type="button" data-bs-original-title="" title="">-</button>';
                    }else if(row.seguimiento=="5"){ //no realizado
                        html='<button class="btn btn-warning" type="button" data-bs-original-title="" title="">N</button>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button title="Seguimiento" type="button" onclick="changeSeguimiento('+row.id+','+row.seguimiento+')"><i class="fa fa-gear"></i></button>\
                <button type="button" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<a href="'+base_url+'Ordenes/registro/'+row.id+'/'+$("#id_proy").val()+'/'+$("#id_cli").val()+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, row, meta){
                 return '<input type="checkbox" name="id[]" id="id_ord" value="'+row.id+'">';
                 
            }
        }],
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function changeSeguimiento(id,seg){
    $("#modalEdit").modal("show");
    $("#id_ord").val(id);
    $("#estatus").val(seg);
}

function modalCarga(){
    $("#modalCarga").modal("show");
    $("#inputFile").fileinput({
        showCaption: false,
        showUpload: false,// quita el boton de upload
        showRemove:false,
        //rtl: true,
        allowedFileExtensions: ["csv"],
        browseLabel: 'Seleccionar Archivo',
        uploadUrl: base_url+'Ordenes/guardar_actividades_malla',
        maxFilePreviewSize: 6000,
        previewFileIconSettings: {
            'csv': '<i class="fa fa-file-excel-o text-success"></i>',
        },
        uploadExtraData: function (previewId, index) {
            var info = {
                        id_proy:$('#id_proy').val(), id_cli:$('#id_cli').val()
                    };
            return info;
        }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
        $("#modalCarga").modal("hide");
        swal("Éxito", "Carga de datos realizada correctamente", "success");
        loadtable();
    });
}

function cambiaEstatus(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de cambiar el estatus?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Ordenes/cambiaSeguimiento",
                    data: {
                        id:$("#id_ord").val(), estatus:$("#estatus option:selected").val()
                    },
                    success:function(response){  
                        loadtable();
                        $("#modalEdit").modal("hide");
                        swal("Éxito", "Se ha cambiado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });   
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Ordenes/delete",
                    data: {
                        id:id, tabla:"ordenes_semanal"
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

 /* ************************************ */

 function loadtableDiario(){
    $("#cont_table_ordenes_diario").show("slow");
    $("#cont_table_ordenes").hide("slow");
    let mont=0;
    if($("#mon_compl").is(":checked")==true){
        mont=1;
    }
    tabla=$("#table_ordenes_diario").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Ordenes/getDataDiario2",
            type: "post",
            //data: { fecha:$("#fecha").val(),id_cli:$("#id_cli").val(),id_proy:$("#id_proy").val(),mont:mont },
            data: { id_proy:$("#id_proy").val(), id_cliente:$("#id_cli").val(),semana:$("#semana option:selected").val(),anio:$("#anio option:selected").val(), turno:$("#turno").val() },
            error: function(){
               $("#table_ordenes_diario").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.tipo_act=="1"){
                        html=row.nombre;
                    }else{
                        html=row.nombre_crit;
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.turno=="1"){
                        html="1ERO";
                    }else if(row.turno=="2"){
                        html="2ERO";
                    }else{
                        html="3ERO";
                    }
                    return html;
                }
            },
            {"data":"fecha"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.estatus=="1"){ //cancelado
                        html='<button class="btn btn-danger" type="button" data-bs-original-title="" title="">X</button>';
                    }else if(row.estatus=="2"){ //programado
                        html='<button class="btn btn-info" type="button" data-bs-original-title="" title="">P</button>';
                    }else if(row.estatus=="3"){ //ok
                        html='<button class="btn btn-success" type="button" data-bs-original-title="" title="">1</button>';
                    }else if(row.estatus=="4"){ //reprogramado
                        html='<button class="btn btn-warning" type="button" data-bs-original-title="" title="">-</button>';
                    }else if(row.estatus=="5"){ //no realizado
                        html='<button class="btn btn-warning" type="button" data-bs-original-title="" title="">N</button>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" onclick="delete_data_diario('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<button type="button" onclick="editarEstatus('+row.id+','+row.estatus+')"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function editarEstatus(id,status,causas){
    $("#modalEdit2").modal("show");
    $("#id_ord2").val(id);
    $("#estatus2").val(status);
    $("#causas").val(causas);
}

function cambiarEstatus2(){
    $.ajax({
        type:'POST',
        url: base_url+"Ordenes/editaEstatus",
        data: {
            id:$("#id_ord2").val(), estatus:$("#estatus2 option:selected").val(), causas:$("#causas").val()
        },
        success:function(response){  
            $("#modalEdit2").modal("hide");
            swal("Éxito", "Se ha actualizado correctamente", "success");
            loadtableDiario();
        }
    });
}

function delete_data_diario(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Ordenes/delete",
                    data: {
                        id:id, tabla:"ordenes_diario"
                    },
                    success:function(response){  
                        loadtableDiario();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}