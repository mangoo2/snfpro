var base_url = $('#base_url').val();
$(document).ready(function() {
    if($("#fecha").val()!="")
        loadtable();

    $("#fecha").on("click",function(){
        if($("#fecha").val()!=""){
            if($("#mon_compl").is(":checked")==false){
                $("#fecha_select").html("Fecha: " +$("#fecha").val());
                loadtable();
                $("#cont_table").show();
                $("#cont_table_mes").hide("slow");
            }else{
                getMes();
                $("#cont_table").hide();
                $("#cont_table_mes").show("slow");
            }
            
        }
        if($("#mon_compl").is(":checked")==true){
            getMes();
        }
    });

    $("#mon_compl").on("change",function(){
        if($("#fecha").val()!="" && $("#mon_compl").is(":checked")==true){ //por mes
            getMes();
            $("#cont_table").hide();
            $("#cont_table_mes").show("slow");
        }
        if($("#fecha").val()!="" && $("#mon_compl").is(":checked")==false){ //por dia
            loadtable();
            $("#cont_table").show();
            $("#cont_table_mes").hide("slow");
        }
    });
    $("#edit_status").on("click",function(){
        cambiarEstatus();
    });
});

function getMes(){
    const month = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    const d = new Date($("#fecha").val());
    let name = month[d.getMonth()];
    $("#fecha_select").html("Mes: " +name);
    
    /*var diasMes = new Date(d.getYear(), d.getMonth(), +1).getDate();
    $("#dia_mes").val(diasMes);
    //var diasSemana = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    let html_head="";
    html_head+='<tr>\
          <th scope="col">#</th>\
          <th scope="col">Actividad</th>\
          <th scope="col">Turno</th>';
    for (var dia = 1; dia <= diasMes; dia++) {
        // Ojo, hay que restarle 1 para obtener el mes correcto
        var indice = new Date(d.getYear(), d.getMonth() - 1, dia).getDay();
        //console.log(`El día número ${dia} del mes ${d.getMonth()} del año ${d.getYear()} es ${diasSemana[indice]}`);
        html_head+='<th>'+dia+'</th>';              
    }
    html_head+='</tr>';
    $("#thead_mes").html(data);*/

    $.ajax({
        data: { fecha:$("#fecha").val() },
        type: 'POST',
        url : base_url+'Ordenes/getHeadMes',
        async: false,
        success: function(data){
            $("#thead_mes").html(data);
        }
    }); 
    loadtableMes();
}

function loadtableMes(){
    $.ajax({
        data: { fecha:$("#fecha").val(),id_cli:$("#id_cli").val(),id_proy:$("#id_proy").val() },
        type: 'POST',
        url : base_url+'Ordenes/getActividadesMes',
        async: false,
        success: function(data){
            $("#body_mes").html(data);
        }
    }); 
}

function loadtable(){
    let mont=0;
    if($("#mon_compl").is(":checked")==true){
        mont=1;
    }
    tabla=$("#table_actividades").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Ordenes/getDataDiario",
            type: "post",
            data: { fecha:$("#fecha").val(),id_cli:$("#id_cli").val(),id_proy:$("#id_proy").val(),mont:mont },
            error: function(){
               $("#table_actividades").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.tipo_act=="1"){
                        html=row.nombre;
                    }else{
                        html=row.nombre_crit;
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.turno=="1"){
                        html="1ERO";
                    }else if(row.turno=="2"){
                        html="2ERO";
                    }else{
                        html="3ERO";
                    }
                    return html;
                }
            },
            /*{"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.estatus=="1"){
                        html='<button style="background-color:green" class="btn btn-pill" type="button" data-bs-original-title="" title="">1</button>';
                    }else if(row.estatus=="2"){
                        html='<button class="btn btn-pill btn-warning" type="button" data-bs-original-title="" title="">2</button>';
                    }else{
                        html='<button class="btn btn-pill btn-danger" type="button" data-bs-original-title="" title="">3</button>';
                    }
                    return html;
                }
            },*/
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="";
                    if(row.estatus=="1"){ //cancelado
                        html='<button class="btn btn-danger" type="button" data-bs-original-title="" title="">X</button>';
                    }else if(row.estatus=="2"){ //programado
                        html='<button class="btn btn-info" type="button" data-bs-original-title="" title="">P</button>';
                    }else if(row.estatus=="3"){ //ok
                        html='<button class="btn btn-success" type="button" data-bs-original-title="" title="">1</button>';
                    }else if(row.estatus=="4"){ //reprogramado
                        html='<button class="btn btn-warning" type="button" data-bs-original-title="" title="">-</button>';
                    }else if(row.estatus=="5"){ //no realizado
                        html='<button class="btn btn-warning" type="button" data-bs-original-title="" title="">N</button>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data":"causas"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<button type="button" onclick="editarEstatus('+row.id+','+row.estatus+',\''+row.causas+'\')"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function editarEstatus(id,status,causas){
    $("#modalEdit").modal("show");
    $("#id_ord").val(id);
    $("#estatus").val(status);
    $("#causas").val(causas);
}

function cambiarEstatus(){
    $.ajax({
        type:'POST',
        url: base_url+"Ordenes/editaEstatus",
        data: {
            id:$("#id_ord").val(), estatus:$("#estatus option:selected").val(), causas:$("#causas").val()
        },
        success:function(response){  
            $("#modalEdit").modal("hide");
            swal("Éxito", "Se ha actualizado correctamente", "success");
            loadtable();
        }
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Ordenes/delete",
                    data: {
                        id:id, tabla:"ordenes_diario"
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}