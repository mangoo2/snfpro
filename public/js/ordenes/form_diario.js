var base_url = $('#base_url').val();
$(document).ready(function(){
    $("#fecha").on("click",function(){
        //console.log("fecha: "+$("#fecha").val());
    });

    $("#btn_sub").on("click",function(){
        save();
    });
});

function save(){
    if($("#fecha").val()!=""){
        var form_register = $('#form_data');
        let fecha = $("#fecha").val();
        let tipo_act = $("#id_actividad option:selected").data("tipo");
        $.ajax({
            data: form_register.serialize()+"&tabla=ordenes_diario&fecha="+fecha+"&tipo_act="+tipo_act,
            type: 'POST',
            url : base_url+'Ordenes/submit',
            async: false,
            beforeSend: function(){
                $("#btn_sub").attr("disabled",true);
            },
            success: function(data){
                let id=data;
                swal("¡Éxito!", "Guardado correctamente", "success");
                setTimeout(function () { 
                    window.location = base_url+'Ordenes/listadoDiario/'+$("#id_proy").val()+"/"+$("#id_cli").val();
                }, 1500);
            }
        }); 
    }else{
        swal("Error!", "Elija una fecha", "warning");
    }
    
}