var base_url = $('#base_url').val();
var cont_emp=1;
$(document).ready(function(){
    $("#btn_sub").on("click",function(){
        save();
    });

    $('#otra_firma').click(function(event) {
        mostrarCapturaFirma();
    });
    $('#otra_firma_cli').click(function(event) {
        mostrarCapturaFirma();
    });

    $('#otra_firma2').click(function(event) {
        mostrarCapturaFirma();
    });
    $('#otra_firma_cli2').click(function(event) {
        mostrarCapturaFirma();
    });

    $(".firma").on("click",function(){ //guardar firma de quien realiza
        guardaFirma($("#id").val(),1);
    });
    $(".firma2").on("click",function(){ //guardar firma de quien realiza
        guardaFirma($("#id").val(),11);
    });

    $(".firma_cli").on("click",function(){ //guardar firma de cliente
        guardaFirma($("#id").val(),2);
    });
    $(".firma_cli2").on("click",function(){ //guardar firma de cliente
        guardaFirma($("#id").val(),22);
    });
    mostrarCapturaFirma();

    $("#realiza").select2({});

    $("#add_emp").on("click",function(){
        if($("#realiza option:selected").val()!=undefined){
            addReliza(cont_emp); 
        }
    });
    $("#id_limpieza").select2({});
});

function addReliza(cont){
    let html=""; let btn=""; let class_row=""; var repetido=0;

    id_emp=$("#realiza option:selected").val();
    txt_emp=$("#realiza option:selected").text();
    //console.log("id_emp: "+id_emp);
    let TABLA = $("#tabla_emp tbody > tr");
    TABLA.each(function(){         
        id_emp_table = $(this).find("input[id*='id']").data("id_emp");
        //console.log("id_emp_table: "+id_emp_table);
        if(id_emp==id_emp_table)
            repetido++;
    });
    if(repetido==0){
        btn='<button onclick="eliminaEmple('+cont+',0)" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
        class_row="row_emp_"+cont+"";
        $("#realiza").val(0).change();
    }

    //console.log("repetido: "+repetido);
    if(repetido==0){
        html='<tr class="'+class_row+'">\
                <td>'+cont+'<input id="id" type="hidden" value="0" data-id_emp="'+id_emp+'"></td>\
                <td><input id="id_emp" type="hidden" value="'+id_emp+'">'+txt_emp+'</td>\
                <td>'+btn+'</td>\
            </tr>';
        $(".body_emp").append(html);
        cont_emp++;
    }else{
        swal("Error!", "Empleado ya asignado previamente", "error");
        $("#realiza").val(0).change();
    }
}

function guardarEmpleados(id_orden){
    let DATAr = [];
    let TABLA = $("#tabla_emp tbody > tr");
        TABLA.each(function(){         
        item = {};
        item["id"] = $(this).find("input[id*='id']").val();
        item["id_empleado"] = $(this).find("input[id*='id_emp']").val();
        item["id_orden"] = id_orden;
        DATAr.push(item);
    });
    array_emp = JSON.stringify(DATAr);
    //console.log("array_emp: "+array_emp);

    var datos = 'id_proyecto='+$("#id_proy").val()+'&array_emp='+array_emp;
    if(TABLA.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Ordenes/submitEmpleados",
            data: datos,
            async:false,
            success: function (response){

            },
            error: function(response){
                swal("Error!", "Se produjo un error, intente de nuevamente o contacte al administrador del sistema", "error") 
            }
        });
    }
    
}

function eliminaEmple(id,tipo){
    if(tipo==0){ //row de empleado sin guardar aun
        $(".row_emp_"+id+"").remove();
        cont_emp--;
    }
    else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: '¡Atención!',
            content: '¿Está seguro de eliminar este registro?',
            type: 'blue',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"Ordenes/deleteEmpleado",
                        data: {
                            id:id
                        },
                        success:function(response){
                            $("#idrow_emp_"+id+"").remove();
                            swal("Éxito", "Se ha eliminado correctamente", "success");
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }
}


function mostrarCapturaFirma(){
    if($("#otra_firma").is(":checked")==true){
        $("#cont_firma").show("slow");
        $("#cont_firma_actual").hide("slow");
    }else{
        $("#cont_firma").hide("slow");
        $("#cont_firma_actual").show("slow");
    }
    if($("#otra_firma2").is(":checked")==true){
        $("#cont_firma2").show("slow");
        $("#cont_firma_actual2").hide("slow");
    }else{
        $("#cont_firma2").hide("slow");
        $("#cont_firma_actual2").show("slow");
    }
    if($("#otra_firma_cli").is(":checked")==true){
        $("#cont_firma_cli").show("slow");
        $("#cont_firma_actual_cli").hide("slow");
    }else{
        $("#cont_firma_cli").hide("slow");
        $("#cont_firma_actual_cli").show("slow");
    }
    if($("#otra_firma_cli2").is(":checked")==true){
        $("#cont_firma_cli2").show("slow");
        $("#cont_firma_actual_cli2").hide("slow");
    }else{
        $("#cont_firma_cli2").hide("slow");
        $("#cont_firma_actual_cli2").show("slow");
    }
}

function guardaFirma(id,tipo){
    if(tipo==1){
        var canvas = document.getElementById('patientSignature');
        var canv = $("#patientSignature").val();
    }
    else if(tipo==11){
        var canvas = document.getElementById('patientSignatureSNF2');
        var canv = $("#patientSignatureSNF2").val();
    }else if(tipo==2){
        var canvas = document.getElementById('patientSignature2');
        var canv = $("#patientSignature2").val();
    }else if(tipo==22){
        var canvas = document.getElementById('patientSignatureC2');
        var canv = $("#patientSignatureC2").val();
    }
    var dataURL = canvas.toDataURL();
    tipo_act = $("#id_limpieza option:selected").data("tipo");
    $.ajax({
        type:'POST',
        url: base_url+'Ordenes/guardarFirma',
        async:false,
        data: {
            firma:dataURL, id:id, id_proyecto:$("#id_proy").val(), id_limpieza:$("#id_limpieza").val(),tipo:tipo,tipo_act:tipo_act
        },
        success:function(data){
            let namefir=data;
            swal("Éxito!", "Firma registrada!", "success");
            if(tipo==1)
                $("#firma").val(namefir);
            else if(tipo==11)
                $("#firma2").val(namefir);
            else if(tipo==2)
                $("#firma_cliente").val(namefir);
             else if(tipo==22)
                $("#firma_cliente2").val(namefir);
        }
    });
}

function save(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input  
        ignore: "",  
        rules: {
            semana:{
              required: true
            },
            turno:{
              required: true
            },
            id_supervisor:{
              required: true
            },
            ot:{
              required: true
            },
            id_limpieza:{
              required: true
            },
            fecha:{
              required: true
            },
            seguimiento:{
              required: true
            },
            fecha_final:{
              required: true
            },
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    var valid = $("#form_data").valid();
    if(valid) {
        let tipo_act = $("#id_limpieza option:selected").data("tipo");
        $.ajax({
            data: form_register.serialize()+"&tabla=ordenes_semanal&tipo_act="+tipo_act,
            type: 'POST',
            url : base_url+'Ordenes/submit',
            async: false,
            beforeSend: function(){
                $("#btn_sub").attr("disabled",true);
            },
            success: function(data){
                let id=data;
                guardarEmpleados(id);
                swal("¡Éxito!", "Guardado correctamente", "success");
                setTimeout(function () { 
                    window.location = base_url+'Ordenes/listado/'+$("#id_proy").val()+"/"+$("#id_cli").val();
                }, 1500);
            }
        }); 
    }
}
