var base_url = $('#base_url').val();
var tabla; var id_cli_table=0;  
$(document).ready(function() {
    loadtable();
});

function reload_registro(){
    tabla.destroy();
    loadtable();
}
function loadtable(){
    console.log("Load");
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        columnDefs: [ {
            sortable: false,
            "class": "index",
            targets: 0
        } ],
        "ajax": {
            "url": base_url+"Mis_clientes/getlistado",
            type: "post",
            "data": {
                'tipo_equipo':$('#tipo_equipo option:selected').val(),"cliente_id":$('#idclientePadre').val()},
            error: function(){
               $("#table_data").css("display","none");
            }
        },
        "columns": [
            //{"data":"id"},
            {"data": null,
                "render": function ( data, type, full, meta ) {
                    return  meta.row + 1;
                }
            },
            {"data":"contacto"},
            {"data":"correo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    console.log("---");
                    console.log(row);
                var html='<a onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></a> ';
                    html+='<a href="'+base_url+'Mis_clientes/registro/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        fixedColumns: true
    });
    tabla.on( 'order.dt search.dt', function () {
        tabla.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Mis_clientes/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        reload_registro();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}