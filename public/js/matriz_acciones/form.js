var base_url = $('#base_url').val();
$(document).ready(function() {
    $('#antes').change(function(){
        document.getElementById('img_antes').src = window.URL.createObjectURL(this.files[0]);
	});
	$('#durante').change(function(){
        document.getElementById('img_durante').src = window.URL.createObjectURL(this.files[0]);
	});
	$('#despues').change(function(){
        document.getElementById('img_despues').src = window.URL.createObjectURL(this.files[0]);
	});
	$('#btn_sub').click(function(){
	    add_form();
	});

    $('.tipo_hallazgo').change(function(){
        verResponsable();
    });

    if($("#id").val()!="0"){
        verResponsable();
    }

    $("#id_resp").select2({});
});

function deleteEvidencia(name,id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar esta evidencia?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type: 'POST',
                    url: base_url+'MatrizAcciones/deleteEvidencia',
                    data: {name:name,id:id},
                    success: function(data){
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                        if(name=="antes"){
                            $('.img_evidencia').html('<img id="img_antes" src="'+base_url+'public/img/no-image.png" class="rounded mr-3 img-fluid" height="142" width="142">');
                        }else if(name=="durante"){
                            $('.img_evidencia_durante').html('<img id="img_durante" src="'+base_url+'public/img/no-image.png" class="rounded mr-3 img-fluid" height="142" width="142">');
                        }else if(name=="despues"){
                            $('.img_evidencia_despues').html('<img id="img_despues" src="'+base_url+'public/img/no-image.png" class="rounded mr-3 img-fluid" height="142" width="142">');
                        }
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });    
}

function verResponsable(){
    if($("#tipo_hallazgo1").is(":checked")==true){
        $("#cont_resp").show("slow");
        $("#cont_resp_cli").hide("slow");
    }
    else if($("#tipo_hallazgo2").is(":checked")==true){
        $("#cont_resp_cli").show("slow");
        $("#cont_resp").hide("slow");
    }
}

function reset_img(tipo){
	if(tipo==1){
        if($("#id").val()>0){
            deleteEvidencia("antes",$("#id").val());
        }else{
            $('.img_evidencia').html('<img id="img_antes" src="'+base_url+'public/img/no-image.png" class="rounded mr-3 img-fluid" height="142" width="142">');
        }	
    }
	else if(tipo==2){
        if($("#id").val()>0){
            deleteEvidencia("durante",$("#id").val());
        }else{
            $('.img_evidencia_durante').html('<img id="img_durante" src="'+base_url+'public/img/no-image.png" class="rounded mr-3 img-fluid" height="142" width="142">');
        }
    }
	else if(tipo==3){
        if($("#id").val()>0){
            deleteEvidencia("despues",$("#id").val());
        }else{
            $('.img_evidencia_despues').html('<img id="img_despues" src="'+base_url+'public/img/no-image.png" class="rounded mr-3 img-fluid" height="142" width="142">');
        }
    }
}

function add_form(){
	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            tipo_hallazgo: {
                required: true
            },
            area: {
                required: true
            },
            hallazgo: {
                required: true
            },
            fecha_deteccion:{
              required: true
            },
            falla: {
	            required: true
	        },
	        acciones: {
                required: true
            },
            afectacion:{
              required: true
            },
            /*avance: {
	            required: true
	        },*/
	        fecha_compromiso:{
              required: true
            },
            /*fecha_cierre: {
	            required: true
	        },*/
        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio") || element.is(":radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function(element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();
    if ($valid) {
    	let avance=0; let tipo_hallazgo=0;
    	if($("#avance").is(":checked")==true)
    		avance=1;
    	else if($("#avance2").is(":checked")==true)
    		avance=2;
    	else if($("#avance3").is(":checked")==true)
    		avance=3;
    	else if($("#avance4").is(":checked")==true)
    		avance=4;

        if($("#tipo_hallazgo1").is(":checked")==true)
            tipo_hallazgo=1;
        else if($("#tipo_hallazgo2").is(":checked")==true)
            tipo_hallazgo=2;

        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'MatrizAcciones/submit',
            data: datos+"&avance="+avance+"&tipo_hallazgo="+tipo_hallazgo,
            beforeSend: function(){
                $("#btn_sub").attr("disabled",true);
            },
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            	add_file(data,"antes");
            	setTimeout(function(){ 
                    add_file(data,"durante");
                }, 1000);
                setTimeout(function(){ 
                    add_file(data,"despues");
                }, 1000);
            	swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'MatrizAcciones/listado/'+$("#id_proy").val()+'/'+$("#id_cli").val();
                }, 3500);
            }
        });         
    }	
}

function add_file(id,name){
    //console.log("archivo: "+archivo);
    //console.log("name: "+name);
    var archivo=$('#'+name+'').val()
	//var name=$('#foto_avatar').val()
	extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	extensiones_permitidas = new Array(".jpeg",".png",".jpg","webp");
	permitida = false;
    if($('#'+name+'')[0].files.length > 0) {
	    for (var i = 0; i < extensiones_permitidas.length; i++) {
	       if (extensiones_permitidas[i] == extension) {
	       permitida = true;
	       break;
	       }
	    }  
	    if(permitida==true){
	      //console.log("tamaño permitido");
	        var inputFileImage = document.getElementById(name);
	        var file = inputFileImage.files[0];
	        var data = new FormData();
	        data.append('foto',file);
	        data.append('id',id);
	        data.append('name',name);
	        $.ajax({
	            url:base_url+'MatrizAcciones/cargafiles',
	            type:'POST',
	            contentType:false,
	            data:data,
	            processData:false,
	            cache:false,
	            success: function(data) {
	                var array = $.parseJSON(data);
	            },
	            error: function(jqXHR, textStatus, errorThrown) {
	                var data = JSON.parse(jqXHR.responseText);
	            }
	        });
	    }
    }        
}
