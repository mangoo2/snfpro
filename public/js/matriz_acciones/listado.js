var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
});

function loadtable(){
    tabla=$("#table_matriz").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        destroy:true,
        //responsive: !0,
        "ajax": {
            "url": base_url+"MatrizAcciones/getDataTable",
            type: "post",
            data: { id_proyecto:$("#id_proy").val(), id_cliente:$("#id_cli").val() },
            error: function(){
               $("#table_matriz").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"area"},
            {"data":"hallazgo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.tipo_hallazgo=="1")
                        html="SNF";
                    else if(row.tipo_hallazgo=="2")
                        html="Cliente"
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='<img id="img_antes" src="'+base_url+'uploads/evidencias/'+row.antes+'" class="rounded mr-3 img-fluid" height="142" width="142">';
                    return html;
                }
            },
            {"data":"fecha_deteccion"},
            {"data":"falla"},
            {"data":"acciones"},
            {"data":"afectacion"},
            //{"data":"criticidad"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    if(row.criticidad=="A")
                        var html='<span class="btn btn-danger">A</span>';
                    else if(row.criticidad=="B")
                        var html='<span class="btn btn-warning">B</span>';
                    if(row.criticidad=="C")
                        var html='<span class="btn btn-success">C</span>';
                    return html;
                }
            },
            //{"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    if(row.tipo_hallazgo=="1")
                        var html=row.nombre;
                    else
                        var html=row.responsable_cli;
                    return html;
                }
            },
            //{"data":"avance"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    //console.log("avance: "+row.avance);
                    if(row.avance==1){
                        var html='<div class="progress">\
                          <div class="progress-bar progress-bar-striped bg-warning progress-bar-animated" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 40%">25%</div>\
                        </div>';
                    }
                    else if(row.avance==2){
                        var html='<div class="progress">\
                          <div class="progress-bar progress-bar-striped bg-warning progress-bar-animated" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">50%</div>\
                        </div>';;
                    }
                    else if(row.avance==3){
                        var html='<div class="progress">\
                          <div class="progress-bar progress-bar-striped bg-warning progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">75%</div>\
                        </div>';
                    }
                    else if(row.avance==4){
                        var html='<div class="progress">\
                          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">100%</div>\
                        </div>';
                    }
                    else{
                        var html='<div class="progress">\
                          <div class="progress-bar progress-bar-striped bg-danger progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 30%">0%</div>\
                        </div>';
                    }
                    return html;
                }
            },
            {"data":"fecha_compromiso"},
            {"data":"fecha_cierre"},
            {"data":"observaciones"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button" onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button>';
                html+='<a href="'+base_url+'MatrizAcciones/registro/'+row.id+'/'+$("#id_proy").val()+'/'+$("#id_cli").val()+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "asc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"MatrizAcciones/delete",
                    data: {
                        id:id, tabla:"matriz_acciones"
                    },
                    success:function(response){  
                        loadtable();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}