var base_url=$('#base_url').val();
var table;
$(document).ready(function($) {
	table=$('#table_data').DataTable();
	load();	
});
function load(){
	var proyecto=$('#id_proy').val();
	table.destroy();
	table=$("#table_data").DataTable({
    		//stateSave: true,
    		//responsive: !0,
    		"bProcessing": true,
    		"serverSide": true,
		    "ajax": {
		       "url": base_url+"Actividades_realizadas/getDataTable",
		       type: "post",
		       "data": {
                	'proyecto':proyecto
            	},
		        error: function(){
		           $("#table").css("display","none");
		        }
		    },
		    "columns": [
		        //{"data": "rid","width": "10%","orderable":false},
		        {"data": "id"},
				{"data": "descripcion"},
				{"data": "fecha"},
				{"data": "horas"},
				{"data": "departamento"},
				{"data": null,
					"render": function ( data, type, row, meta ) {
		            	var html="";
		            		//html+='<div class="gallery my-gallery card-body row" itemscope="" data-pswp-uid="1">';
                    
		            	if(row.file1!='null' && row.file1!=null && row.file1!=''){
		            		
                    		html+='<img class="img-thumbnail img-thumbnail_'+row.id+'_1" src="'+base_url+'uploads/files_ar/thumbnail/'+row.file1+'" itemprop="thumbnail" alt="image" data-imagen="'+row.file1+'" onclick="img_thumbnail('+row.id+',1)">';
			          	}
			          	
			          	if(row.file2!='null' && row.file2!=null && row.file2!=''){
			          		html+='<img class="img-thumbnail img-thumbnail_'+row.id+'_2" src="'+base_url+'uploads/files_ar/thumbnail/'+row.file2+'" itemprop="thumbnail" alt="image" data-imagen="'+row.file2+'" onclick="img_thumbnail('+row.id+',2)">';
			          	}
			          	
			          	if(row.file3!='null' && row.file3!=null && row.file3!=''){
			          		html+='<img class="img-thumbnail img-thumbnail_'+row.id+'_3" src="'+base_url+'uploads/files_ar/thumbnail/'+row.file3+'" itemprop="thumbnail" alt="image" data-imagen="'+row.file3+'" onclick="img_thumbnail('+row.id+',3)">';
			          	}
			          	//html+='</div>';
		            	return html;
		            }
				},
		        {"data": null,
		            "render": function ( data, type, row, meta ) {
		            	var html="";
		            		html+='<a onclick="delete_data('+row.id+')" style="margin-right: 20px;"><i class="fa fa-trash-o" style="font-size: 20px;"></i></a>';
		            		html+='<a href="'+base_url+'Actividades_realizadas/add/'+proyecto+'/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
		            	
		            	return html;
		            }
		        },
		    ],
    		"order": [[ 0, "DESC" ]],
    		"lengthMenu": [[10,20,30, 50, 100], [10,20,30, 50, 100]],
    		
			createdRow: function (row, data, index) {
				//$( row ).find('td:eq(0)').addClass('td_colum_0');
				//$( row ).find('td:eq(9)').addClass('td_colum_9');
			}
  	}).on('draw',function(row){
  		
        //$('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        /*
        $('.img-thumbnail').click(function(event) {
        	var img=$(this).data('imagen');
        	console.log(img);
        	$('#modalimages').modal('show');
        	$('.imagemodalbody').html('<img src="'+base_url+'uploads/files_ar/'+img+'" style="max-width: -webkit-fill-available;">');

        });
        */
   	});
}
function img_thumbnail(id,tipo){
	var img=$('.img-thumbnail_'+id+'_'+tipo).data('imagen');
        	console.log(img);
        	$('#modalimages').modal('show');
        	$('.imagemodalbody').html('<img src="'+base_url+'uploads/files_ar/'+img+'" style="max-width: -webkit-fill-available;">');
}
function delete_data(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Actividades_realizadas/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        load();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function adddescripcion(){
	$('#modaldescripcion').modal('show');
	viewdes();
	$('#dg_id').val(0);
	$('#dg_titulo').val('');
	$('#dg_dep').val('');
	$('#dg_des').val('');
}
function savedescripcion(){
	$.ajax({
        type:'POST',
        url: base_url+"Actividades_realizadas/savedescripciong",
        data: {
            dg_id:$('#dg_id').val(),
			dg_mes:$('#dg_mes option:selected').val(),
			dg_anio:$('#dg_anio option:selected').val(),
			dg_titulo:$('#dg_titulo').val(),
			dg_dep:$('#dg_dep option:selected').val(),
			dg_des:$('#dg_des').val(),
			proyecto:$('#id_proy').val()
        },
        success:function(response){  
            viewdes();
            $('#dg_id').val(0);
			$('#dg_titulo').val('');
			$('#dg_dep').val('');
			$('#dg_des').val('');
            swal("Éxito", "Guardado correctamente", "success");
        }
    });
}
function viewdes(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Actividades_realizadas/obetenerdesg",
        data: {
			proyecto:$('#id_proy').val()
        },
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            $('#tabledes tbody').html('');
            array.forEach(function(element) {
            	var html='<tr>\
            					<td>'+viewmes(element.dg_mes)+'</td>\
            					<td>'+element.dg_anio+'</td>\
            					<td>'+element.dg_titulo+'</td>\
            					<td>'+element.departamento+'</td>\
            					<td>'+element.dg_des+'</td>\
            					<td>\
            						<a onclick="delete_des('+element.dg_id+')" style="margin-right: 20px;"><i class="fa fa-trash-o" style="font-size: 20px;"></i></a>\
            						<a onclick="edit_des('+element.dg_id+')" class="edit_des_'+element.dg_id+'" \
            						data-dg_mes="'+Number(element.dg_mes)+'" \
            						data-dg_anio="'+element.dg_anio+'" \
									data-dg_titulo="'+element.dg_titulo+'" \
									data-dg_dep="'+element.dg_dep+'" \
									data-dg_des="'+element.dg_des+'" \
            						><i class="fa fa-edit" style="font-size: 20px;"></i></a>\
            					</td></tr>';
                $('#tabledes tbody').append(html);
            });
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
    });
}
function delete_des(id){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Actividades_realizadas/delete_des",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        viewdes();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function viewmes(mes){
	var mesl='';
	switch (mes) {
	  	case '1':
	     	mesl='Enero';
	    break;
	   	case '2':
	     	mesl='Febrero';
	    break;
	    case '3':
	     	mesl='Marzo';
	    break;
	    case '4':
	     	mesl='Abril';
	    break;
	    case '5':
	     	mesl='Mayo';
	    break;
	    case '6':
	     	mesl='Junio';
	    break;
	    case '7':
	     	mesl='Julio';
	    break;
	    case '8':
	     	mesl='Agosto';
	    break;
	    case '9':
	     	mesl='Septiembre';
	    break;
	    case '10':
	     	mesl='Octubre';
	    break;
	    case '11':
	     	mesl='Noviembre';
	    break;
	    case '12':
	     	mesl='Diciembre';
	    break;
	}
	return mesl;
}
function edit_des(id){
	var dg_mes = $('.edit_des_'+id).data('dg_mes');
	var dg_anio = $('.edit_des_'+id).data('dg_anio');
	var dg_titulo = $('.edit_des_'+id).data('dg_titulo');
	var dg_dep = $('.edit_des_'+id).data('dg_dep');
	var dg_des = $('.edit_des_'+id).data('dg_des');
	$('#dg_id').val(id);
	$('#dg_mes').val(dg_mes);
	$('#dg_anio').val(dg_anio);
	$('#dg_titulo').val(dg_titulo);
	$('#dg_dep').val(dg_dep);
	$('#dg_des').val(dg_des);
}