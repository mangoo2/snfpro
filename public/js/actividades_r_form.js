var base_url=$('#base_url').val();
var idactividad;
$(document).ready(function($) {
    viewdep();
	$("#file1").fileinput({
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen 1',
        uploadUrl: base_url+'Actividades_realizadas/cargaimagen',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    filetipo:1,
                    actividad:idactividad
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
    $("#file2").fileinput({
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen 2',
        uploadUrl: base_url+'Actividades_realizadas/cargaimagen',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    filetipo:2,
                    actividad:idactividad
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
    $("#file3").fileinput({
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["png","jpg","jpeg","bmp"],
        browseLabel: 'Seleccionar imagen 3',
        uploadUrl: base_url+'Actividades_realizadas/cargaimagen',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    filetipo:3,
                    actividad:idactividad
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
    $('#btn_sub').click(function(event) {
    	$( "#btn_sub" ).prop( "disabled", true );
	    setTimeout(function(){ 
	         $("#btn_sub" ).prop( "disabled", false );
	    }, 5000);
	    var form = $('#form_data');
	    if(form.valid()){
	    	var datos =form.serialize(); 
            var idproyecto = $('#idproyecto').val();
	    	$.ajax({
		        type:'POST',
		        url: base_url+"index.php/Actividades_realizadas/saveform",
		        data: datos,
		        success: function (response){
		          toastr["success"]("Actividades almacenadas con éxito"); 
		          idactividad =parseInt(response);
		          $('#file1').fileinput('upload');
		          $('#file2').fileinput('upload');
		          $('#file3').fileinput('upload');
		          setTimeout(function(){
		            location.reload();
                    window.location.href = base_url+"index.php/Actividades_realizadas/listado/"+idproyecto;
		          }, 3000);

		        },
		        error: function(response){
		            toastr["error"]("Algo salio mal", "Error"); 
		             
		        }
		      });
	    }else{
	    	toastr["warning"]("Faltan campos requeridos"); 
	    }
    });
});
function obtenerdatosform(id){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Actividades_realizadas/obtenerdatosform",
        data: {id:id},
        success: function (response){
          console.log(response);
          var array = $.parseJSON(response);
          	$('#descripcion').val(array.descripcion);
            $('#descripcion_general').val(array.descripcion_general);
          	$('#fecha').val(array.fecha);
            $('#fechai').val(array.fechai);
            $('#fechaf').val(array.fechaf);
          	$('#horas').val(array.horas);
          	$('#departamento').val(array.departamento);
          	if(array.file1!=null || array.file1!=''){
          		$(".div_file1 .file-drop-zone").css({"background":"url("+base_url+"uploads/files_ar/"+array.file1+")","background-size":"contain","background-repeat":"no-repeat","background-position":"center"});
          	}
          	
          	if(array.file2!=null || array.file2!=''){
          		$(".div_file2 .file-drop-zone").css({"background":"url("+base_url+"uploads/files_ar/"+array.file2+")","background-size":"contain","background-repeat":"no-repeat","background-position":"center"});
          	}
          	
          	if(array.file3!=null || array.file3!=''){
          		$(".div_file3 .file-drop-zone").css({"background":"url("+base_url+"uploads/files_ar/"+array.file3+")","background-size":"contain","background-repeat":"no-repeat","background-position":"center"});
          	}
          	
            $('#titulo1').val(array.titulo1);
            $('#descripcion1').val(array.descripcion1);
            //$('#periodo_inicio1').val(array.periodo_inicio1);
            //$('#periodo_fin1').val(array.periodo_fin1);

            $('#titulo2').val(array.titulo2);
            $('#descripcion2').val(array.descripcion2);
            //$('#periodo_inicio2').val(array.periodo_inicio2);
            //$('#periodo_fin2').val(array.periodo_fin2);

            $('#titulo3').val(array.titulo3);
            $('#descripcion3').val(array.descripcion3);
            //$('#periodo_inicio3').val(array.periodo_inicio3);
            //$('#periodo_fin3').val(array.periodo_fin3);


        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
      });
}
function adddepartamento(){
    $('#modaldepartamento').modal('show');
    $('#iddep').val(0);
    $('#departamentonew').val('');
}
function savedep(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Actividades_realizadas/savedepartamento",
        data: {
            iddep:$('#iddep').val(),
            dep:$('#departamentonew').val(),
            id_proy:$('#idproyecto').val()
        },
        success: function (response){
            viewdep();
            $('#iddep').val(0);
            $('#departamentonew').val('');
            $('#modaldepartamento').modal('hide');
            toastr["success"]("Departemento agregado", "Hecho"); 
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
    });
}
function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Actividades_realizadas/deletedepartemento",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        viewdep();
                        toastr["success"]("Departemento Eliminado", "Hecho"); 
                        $('#modaldepartamento').modal('hide');
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function viewdep(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Actividades_realizadas/obetenerdepartamentos",
        data:{ id_proy:$('#idproyecto').val() },
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            $('#departamento').html('');
            $('#tabledep tbody').html('');
            array.forEach(function(element) {
                $('#departamento').append('<option value="'+element.id+'">'+element.departamento+'</option>');
                $('#tabledep tbody').append('<tr><td>'+element.departamento+'</td><td><a onclick="delete_data('+element.id+')"style="margin-right: 20px;"><i class="fa fa-trash-o" style="font-size: 20px;"></i></a><a onclick="edit_data('+element.id+')" class="edit_dep_'+element.id+'" data-dep="'+element.departamento+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a></td></tr>');
            });
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
    });
}
function  edit_data(id) {
    var dep=$('.edit_dep_'+id).data('dep');
    $('#iddep').val(id);
    $('#departamentonew').val(dep);
}