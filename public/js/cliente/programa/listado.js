var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    getProyectos();
    $("#id_proyecto").on("change",function(){
        getMesesProy();
    });

    $("#id_proyecto,#mes").on("change",function(){
        setTimeout(function () { 
           loadtable();
        }, 1000);
    });
    setTimeout(function () { 
       loadtable();
    }, 1000);
});

function getMesesProy(){
    //console.log("cambia select proyecto");
    let mes_ini = $("#id_proyecto option:selected").data("ini");
    let mes_fin = $("#id_proyecto option:selected").data("fin");
    let anio_ini = $("#id_proyecto option:selected").data("anio_ini");
    let anio_fin = $("#id_proyecto option:selected").data("anio_fin");
    let sel_mes="";
    const month = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
    let name = 0;
    if(anio_ini==anio_fin){
        mes_ini2 = mes_ini-1;
        for (var i=mes_ini2; i<mes_fin; i++) {
            name = month[i];
            let j=i+1;
            sel_mes+='<option data-anio_fin_opt="'+anio_fin+'" value="'+j+'">'+name+'</option>';
        }
    }else{
        //console.log("anios distintos");
        //console.log("anio_ini: "+anio_ini);
        //console.log("anio_fin: "+anio_fin);
        //console.log("mes_ini: "+mes_ini);
        if(mes_ini==12)
            mes_ini2=0;
        else    
            mes_ini2 = mes_ini-1;
        anio_aux=anio_ini; let j=0; var j_aux=0;
        rest = Number(anio_fin)-Number(anio_ini);
        //console.log("rest: "+rest);
        anio_fin2= rest*12;
        //console.log("mes_ini2: "+mes_ini2);
        //console.log("anio_fin2: "+anio_fin2);
        if(anio_fin2>11){
           // anio_fin2=anio_fin2+1;
        }
        if(anio_fin>11){
            anio_fin2=anio_fin2+Number(mes_fin);
            anio_fin2=anio_fin2-1;
        }
        //console.log("anio_fin2: "+anio_fin2);
        for (var k=mes_ini2; k<=anio_fin2; k++) {
            k_aux=k;
            if(k_aux==12){
                k_aux=0;
            }
            if(k_aux==11){
                //anio_aux+1;
                //console.log("cambio el año "+anio_aux);
            }
            //console.log("k "+k);
            //console.log("k_aux "+k_aux);
            name = month[k_aux];
            j=k+1;
            j_aux++;
            //console.log("j_aux: "+j_aux);
            //console.log("nombre de mes: "+name);
            if(j_aux%12==0 && anio_fin2>11){
                //anio_aux++;
            }
            if(j_aux%13==0 || name=="Enero" && j_aux>12){
                anio_aux++;
                j_aux=1;
            }
            if(j>=13 && j<=24){
                j2=j-13;
                j=j2+1;
                name = month[j2];   
            }
            if(j>=25 && j<=36){
                j2=j-25;
                j=j2+1;
                name = month[j2];   
            }
            //console.log("j "+j);
            sel_mes+='<option data-anio_fin_opt="'+anio_aux+'" value="'+j+'">'+name+' - '+anio_aux+'</option>';
        }
    }
    $("#mes").html(sel_mes);  
}

function getProyectos(){
    $.ajax({
        type:'POST',
        url: base_url+"Inicio/getProyectosCliente",
        data: { id_proy: $("#id_proy").val()},
        async:false,
        success:function(response){  
            //console.log(response);
            $("#id_proyecto").html(response);
            getMesesProy();
        }
    });
}

function loadtable(){
    var mes=$("#mes option:selected").val();
    var anio=$("#mes option:selected").data("anio_fin_opt");
    tabla=$("#table_ordenes").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Inicio/getDataTableReporte",
            type: "post",
            data: { id_proy:$("#id_proyecto option:selected").val(), mes:$("#mes option:selected").val(), tipo:$("#tipo").val() },
            error: function(){
               $("#table_ordenes").css("display","none");
            }
        },
        "columns": [
            //{"data":"id"},
            //{"data":"tipo"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="1";
                    return html;
                }
            },
            {"data":"name_salida"},
            //{"data":"supervisor"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html="SNF PRO";
                    return html;
                }
            },
            //{"data":"reg"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html="";
                    //console.log("tipo: "+row.tipo);
                    if(row.tipo==1){
                        if(fileExists("pdf_limpieza/limpieza_"+row.id+"_"+mes+"_"+anio+".pdf")=="exist"){
                            //html=row.reg;
                            html=$("#mes option:selected").text();
                        }else{
                            html="";
                        }
                    }
                    else if(row.tipo==2){
                        if(fileExists("pdf_matriz_acciones/matriz_"+row.id+"_"+mes+"_"+anio+".pdf")=="exist"){
                            //html=row.reg;
                            html=$("#mes option:selected").text();
                        }else{
                            html="";
                        }
                        
                    }else if(row.tipo==3){
                        //console.log("en tipo 3 de fecha de reg:"+fileExists("pdf_matriz_versa/matriz_"+row.id+".pdf"));
                        if(fileExists("pdf_matriz_versa/matriz_"+row.id+".pdf")=="exist"){
                            //html=row.reg;
                            html=$("#mes option:selected").text();
                        }else{
                            html="";
                        }
                    }
                    else if(row.tipo==4){
                        html="";
                    }else if(row.tipo==5){
                        if(fileExists("pdf_monitoreo/monitoreo_"+row.id+"_"+mes+"_"+anio+".pdf")=="exist"){
                            //html=row.reg;
                            html=$("#mes option:selected").text();
                        }else{
                            html="";
                        }
                    }else if(row.tipo==6){
                        if(fileExists("pdf_presentismo/presentismo_"+row.id+"_"+mes+"_"+anio+".pdf")=="exist"){
                            //html=row.reg;
                            html=$("#mes option:selected").text();
                        }else{
                            html="";
                        }
                    }
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html="";
                    //console.log("tipo: "+row.tipo);
                    if(row.tipo==1){
                        if(fileExists("pdf_limpieza/limpieza_"+row.id+"_"+mes+"_"+anio+".pdf")=="exist"){
                            html=' <a title="Generar PDF" href="'+base_url+'Reportes/pdfLimpieza/'+row.id+'/'+mes+'/'+anio+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        }else{
                            html="Reporte no creado por SNF";
                        }
                    }
                    else if(row.tipo==2){
                        if(fileExists("pdf_matriz_acciones/matriz_"+row.id+"_"+mes+"_"+anio+".pdf")=="exist"){
                            html='<a title="Generar PDF" href="'+base_url+'Reportes/pdfMatrizAcciones/'+row.id+'/'+mes+'/'+anio+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        }else{
                            html="Reporte no creado por SNF";
                        }
                        
                        html+='<button title="Ver Resultados" type="button" onclick="generar('+row.id+')"><i class="fa fa-gears" style="font-size: 20px;"></i> Resultados</button>';
                    }else if(row.tipo==3){
                        //console.log("en tipo 3 :"+fileExists("pdf_matriz_versa/matriz_"+row.id+".pdf"));
                        //html='<a title="Generar PDF" href="'+base_url+'Matriz_versatibilidad/documento/'+row.id+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        if(fileExists("pdf_matriz_versa/matriz_"+row.id+".pdf")=="exist"){
                            html='<a title="Generar PDF" href="'+base_url+'Matriz_versatibilidad/documento/'+row.id+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        }else{
                            html="Reporte no creado por SNF";
                        }
                    }
                    else if(row.tipo==4){
                        //html=base_url+'Reportes/pdfActividades_realizadas/'+row.id+'/'+mes;
                        //html='<a title="Generar PDF" href="'+base_url+'Reportes/pdfActividades_realizadas/'+row.id+'/'+mes+'/'+anio+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        html=' <a onclick="generarPDFModal('+row.id+','+anio+',1)" title="Generar PDF" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                    }else if(row.tipo==5){
                        if(fileExists("pdf_monitoreo/monitoreo_"+row.id+"_"+mes+"_"+anio+".pdf")=="exist"){
                            html='<a title="Generar PDF" href="'+base_url+'Reportes/pdfMonitoreo/'+row.id+'/'+mes+'/'+anio+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        }else{
                            html="Reporte no creado por SNF";
                        }
                    }else if(row.tipo==6){
                        if(fileExists("pdf_presentismo/presentismo_"+row.id+"_"+mes+"_"+anio+".pdf")=="exist"){
                            html='<a title="Generar PDF" href="'+base_url+'Reportes/pdfPresentismo/'+row.id+'/'+mes+'/'+anio+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';
                        }else{
                            html="Reporte no creado por SNF";
                        }
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: languageTables
    });
}

function fileExists(path) {
    var response="";
    $.ajax({
        type: "POST",
        url: base_url+"Inicio/fileExistPDF",
        data: { "path": path},
        async: false,
        success: function (result) {
            response=result;
        }
    });
    return response;
}

function generarPDFModal(id_proy,anio,tipo){
    $("#modaldepartamento").modal("show");
    $("#id_pd").val(id_proy);
    $("#anio_pd").val(anio);
    $("#tipo_sol").val(tipo);
}

function generarPDFDpto(){
    if($("#tipo_sol").val()==1){ //realiza pdf 
        if(fileExists("pdf_limpieza/limpieza_ar_"+$("#id_pd").val()+"_"+$("#mes option:selected").val()+"_"+$("#mes option:selected").data("anio_fin_opt")+"_"+$("#departamento option:selected").val()+".pdf")=="exist"){
            window.open(base_url+'Reportes/pdfActividades_realizadas/'+$("#id_pd").val()+'/'+$("#mes option:selected").val()+'/'+$("#departamento option:selected").val()+'/'+$("#mes option:selected").data("anio_fin_opt"));
            $("#modaldepartamento").modal("hide");
        }else{
            swal("Alerta", "Reporte no creado por SNF", "warning");
            $("#modaldepartamento").modal("hide");
        }
    }
}

function generar(id_proy){
    mes=$("#mes option:selected").val();
    anio=$("#mes option:selected").data("anio_fin_opt");
    window.open(base_url+'Reportes/resultadosMatrizAccion/'+id_proy+'/'+mes+'/'+anio);
}
