var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
    loadtable();
});

function reload_registro(){
    tabla.destroy();
    loadtable();
}
function loadtable(){
    tabla=$("#table_data").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        columnDefs: [ {
            sortable: false,
            "class": "index",
            targets: 0
        } ],
        "ajax": {
            "url": base_url+"Servicios/getlistado",
            type: "post",
            "data": {
                'tipo_registro':$('#tipo_registro option:selected').val()},
            error: function(){
               $("#table_data").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"servicio"},
            {"data":"nombre"},
            {"data":"reg2"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<a onclick="delete_data('+row.id+')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></a> ';
                    html+='<a href="'+base_url+'Servicios/registro/'+row.id+'"><i class="fa fa-edit" style="font-size: 20px;"></i></a>';
                return html;
                }
            },
        ],
        //"order": [[ 0, "desc" ]],
        "lengthMenu": [[50, 75, 100], [50, 75, 100]],
    });
     tabla.on( 'order.dt search.dt', function () {
        tabla.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}

function delete_data(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¡Atención!',
        content: '¿Está seguro de eliminar este registro?',
        type: 'blue',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Servicios/delete",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        reload_registro();
                        swal("Éxito", "Se ha eliminado correctamente", "success");
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}