var base_url = $('#base_url').val();
var cont1 = 0;

$(document).ready(function() {
    $('#foto_avatar').change(function(){
	     document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
	});
});

function reset_img(){
    $('.img_cliente').html('<img id="img_avatar" src="'+base_url+'public/img/avatar.png" class="rounded mr-3" height="64" width="64">');
}

function add_form(){
    $.validator.addMethod("usuarioexiste", function(value, element) {
    var respuestav;
    $.ajax({
        type: 'POST',
        url: base_url + 'Clientes/validar',
        data: {
            Usuario: value
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                if ($('#idcliente').val() > 0) {
                    respuestav = true;
                } else {
                    respuestav = false;
                }

            } else {
                respuestav = true;
            }



        }
    });
    //console.log(respuestav);
    return respuestav;
    });


	var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            empresa: {
                required: true
            },
            contacto: {
                required: true
            },
            telefono:{
              required: true
            },
            correo: {
                    usuarioexiste: true,
                    minlength: 3,
                    required: true
            },
            contrasena: {
                minlength: 6,
                required: true
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
        },
        messages: {
            correo: {
                usuarioexiste: 'Seleccione otro correo ya existe en otro registro'
            }

        },
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function(event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function(element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function(element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function(label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });

    var $valid = $("#form_data").valid();

    if ($valid) {
        ////////////////////
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Clientes/guardar',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                add_file(data);
                add_contactos(data);
                swal("Éxito", "Guardado Correctamente", "success");
                setTimeout(function(){ 
                    window.location = base_url+'Clientes';
                }, 1500);
            }
        });           
    }
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Clientes/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}

function add_contactos(id){
    var DATA = [];
    var TABLA = $("#table_contactos_container tbody > tr");

    TABLA.each(function() {
        item = {};
        
        //item["clienteId"] = $('#idcliente').val();
        item["clienteId"] = id;

        item["contacto"] = $(this).find("input[id*='contacto']").val();
        item["telefono"] = $(this).find("input[id*='telefono']").val();
        item["correo"] = $(this).find("input[id*='correo']").val();

        item["id"] = $(this).find("input[id*='id_prod']").val();
        item["estatus"] = $(this).find("input[id*='estatus']").val();

        if ($(this).find("input[id*='contacto']").val() != "") {
            DATA.push(item);
        }

    });
    
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    
    for (let [key, value] of INFO.entries()) {
        //console.log(key + ': ' + value);
    }

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Clientes/insertUpdate_contactos',
        processData: false,
        contentType: false,
        success: function(data) {
            //console.log(data);
        }
    });
}

//CloneFormContactos
let cont_ant = 0;
function CloneContact() {
    cont1++;
    clone = $("#tr_contactos").clone().attr('id', "contactos_form_" + cont1);
    clone.find("input").val("");
    clone.find("button").attr("onclick", "eliminaClone(" + cont1 + ")").removeClass("btn-success").addClass("btn-danger");
    clone.find("#btn_plus").removeClass("fa fa-plus").addClass("fa fa-minus");


    // Obtener todos los elementos con la clase 
    const elementos = document.querySelectorAll(".lastBtn");

    // Recorrer la lista de elementos y quitar la clase 
    for (let i = 0; i < elementos.length; i++) {
    elementos[i].classList.remove("lastBtn");
    }

    //Agregamos la clase al elemento actual
    clone.addClass("lastBtn");

    if (cont1 == 1) {
        clone.insertAfter("#tr_contactos");
        //console.log("Antes");
    } else {
        cont_ant = cont1 - 1;
        clone.insertAfter("#contactos_form_" + cont_ant + "");
        //console.log("Despues");
    }
    cont_ant = cont1;
}
//Fin CloneFormContactos

function eliminaClone(row_id) {

    const miElemento = document.getElementById('contactos_form_' + row_id);
    if(miElemento.classList.contains("lastBtn")){
        //console.log("Existe");
        cont1--;
        cont_ant--;
    }

    $('#contactos_form_' + row_id).remove();
}

function eliminarContact(row_id) {
    $('#tr_each_'+row_id).find("input[id*='estatus']").val("0");
    $('#tr_each_'+row_id).css("display","none");

}