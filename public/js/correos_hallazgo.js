var base_url = $('#base_url').val();
$(document).ready(function() {
    fecha_correo();
});

var validar=0;
function fecha_correo(){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Inicio/guardar_correos",
        success:function(response){ 
            var array = $.parseJSON(response);
            validar=array.validar;
            if(array.validar==0){
                fecha_correo_detalles(array.id_reg);
            }
        }
    });
}

function fecha_correo_detalles(id_reg){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Inicio/guardar_correos_detalles",
        data:{
            id_reg:id_reg
        },
        success:function(response){ 
            enviar_correos();
        }
    });
}

function enviar_correos(){
	if(validar==0){
	    $.ajax({
	        type:'POST',
	        url: base_url+"index.php/Inicio/enviar_correos_hallazgo",
	        success:function(response){ 
	        }
	    });
	}
}