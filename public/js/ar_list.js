var base_url = $('#base_url').val();
var tabla;
$(document).ready(function($) {
	tabla=$("#table_ordenes").DataTable();
	viewdep();
	loadtable();
});
function viewdep(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Actividades_realizadas/obetenerdepartamentos",
        success: function (response){
            var array = $.parseJSON(response);
            console.log(array);
            $('#departamento').html('<option value="0"></option>');
            array.forEach(function(element) {
                $('#departamento').append('<option value="'+element.id+'">'+element.departamento+'</option>');
            });
        },
        error: function(response){
            toastr["error"]("Algo salio mal", "Error"); 
             
        }
    });
}
function loadtable(){
	var departamento=$('#departamento').val();
    tabla=$("#table_ordenes").DataTable({
        destroy:true,
        "ajax": {
            "url": base_url+"Inicio/ar_listDataTable",
            type: "post",
            data: { 
            	proyecto:$("#id_proy").val(), 
            	dep:departamento 
            },
            error: function(){
               $("#table_ordenes").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    html='Actividades Realizadas';
                    return html;
                }
            },
            {"data":"nombre"},
            {"data":"fecha"},
            {"data": null,
            	"render": function ( data, type, row, meta ){
            		var html='';
            			html=viewmes(row.fecha_formateada);
            		return html;
            	}
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    

                   	uri_pdf=base_url+'Reportes/pdfActividades_realizadas/'+row.id+'/'+row.fecha_formateada;
                    html+=' <a title="Generar PDF" href="'+uri_pdf+'" target="_blank" type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i></a>';                    

                     uri_img=base_url+'Reportes/carrusel/'+row.id;
                    html+=' <a title="Imagenes" href="'+uri_img+'" type="button" class="btn btn-danger"><i class="fa fa-picture-o"></i></a>';
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]]
    });
}
function viewmes(mes){
	var mesl='';
	switch (mes) {
	  	case '1':
	     	mesl='Enero';
	    break;
	   	case '2':
	     	mesl='Febrero';
	    break;
	    case '3':
	     	mesl='Marzo';
	    break;
	    case '4':
	     	mesl='Abril';
	    break;
	    case '5':
	     	mesl='Mayo';
	    break;
	    case '6':
	     	mesl='Junio';
	    break;
	    case '7':
	     	mesl='Julio';
	    break;
	    case '8':
	     	mesl='Agosto';
	    break;
	    case '9':
	     	mesl='Septiembre';
	    break;
	    case '10':
	     	mesl='Octubre';
	    break;
	    case '11':
	     	mesl='Noviembre';
	    break;
	    case '12':
	     	mesl='Diciembre';
	    break;
	}
	return mesl;
}