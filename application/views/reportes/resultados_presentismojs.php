<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/fontawesome.css">

<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatables.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/chartist.css">

<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">

<script src="<?php echo base_url(); ?>assets/js/chart/chartist/chartist.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/chart/chartist/chartist-custom.js"></script>-->

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatable/datatables/datatable.custom.js"></script>

<!--<script src="<?php echo base_url(); ?>plugins/point/dist/chartist-plugin-tooltip.css"></script>-->
<script src="<?php echo base_url(); ?>plugins/point/dist/chartist-plugin-pointlabels.js"></script>
<script src="<?php echo base_url(); ?>plugins/point/dist/chartist-plugin-pointlabels.min.js"></script>

<script src="<?php echo base_url();?>public/js/reportes/resultados_presentismo.js?v=<?php echo date('YmdGis') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>