<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    $GLOBALS['logo_cli']=$logo_cli;
    foreach ($gra as $i) {
        if($i->tipo==1)
            $GLOBALS['grafica']=$i->grafica;
        if($i->tipo==2)
            $GLOBALS['grafica2']=$i->grafica;
    }

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logofinalsys2.png';
        if($GLOBALS['logo_cli']!="")
            $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
        else
            $logo_cliente ="#";
        //$this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);

        $html = '<table width="100%">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="15%"><img src="'.$logos.'" height="90px"></td>
                <td width="65%" style="font-weight:bold; color:rgba(68, 114, 196); text-align:center; font-size:32px">SERVICIOS NUEVOS DE FILTRACIÓN HISPANOMEXICANOS S.A DE C.V.</td>
                <td width="20%" align="center"><img src="'.$logo_cliente.'" height="90px"></td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $pie = base_url().'public/img/footer.png';
        $html = '
        <table width="100%" cellpadding="2">
        <tr>
            <td width="100%"><img src="'.$pie.'"></td>
        </tr>
      </table>';
    
        //$this->writeHTMLCell(188, '', 12, 100, $html, 0, 0, 0, true, 'C', true);
        $this->Image($pie, 0, 165, 310, 50, 'PNG', '', '', true, 310, '', false, false, 0);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('SALIDA - MATRIZ DE HALLAZGOS');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10,37,10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 21);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setPrintHeader(false);
// add a page
$pdf->AddPage('L', 'A4'); 

$img_file = base_url().'public/img/portada_mat_acc.png';

                            //ancho alto
//$pdf->Image($img_file, 0, 0, 452, 530, '', '', 10, false, 310, '', false, false, 0);
$pdf->Image($img_file, 0, 0, 400, 190, 'PNG', '', '', true, 310, '', false, false, 0);

$logos = base_url().'public/img/logofinalsys2.png';
$pdf->Image($logos, 16, 13, 39,25, 'PNG', '', '', true, 150, '', false, false, false);

if($GLOBALS['logo_cli']!=""){
    $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
    $pdf->Image($logo_cliente, 239, 13, 39,25, 'JPG', '', '', true, 150, '', false, false, 0);
}
$pdf->setPrintHeader(true);

// add a page
$pdf->AddPage('L', 'A4'); 
$pdf->setPrintFooter(false);
$html=""; $img=""; $img2="";

$grafica=$GLOBALS['grafica'];
if($grafica!=""){
    $img = '<img width="680px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica) . '">';
}

$grafica2=$GLOBALS['grafica2'];
if($grafica2!=""){
    $img2 = '<img width="680px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica2) . '">';
}

$pdf_cerrado="javascript:void(0)"; $pdf_progreso="javascript:void(0)"; $pdf_abierto="javascript:void(0)";

if($cerrado>0){
    //$pdf_cerrado="javascript:window.open('".base_url().'pdf_matriz_acciones/matriz_cerrados_'.$id_proy.".pdf');";
    $pdf_cerrado=base_url().'pdf_matriz_acciones/matriz_cerrados_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf';
    //$pdf_cerrado=base_url().'Reportes/pdfMatrizAccionesTipoWhere/'.$id_proy.'/1/'.$mes.'/'.$anio;
}
if($progreso>0){
    //$pdf_progreso="javascript:window.open('".base_url().'pdf_matriz_acciones/matriz_progreso_'.$id_proy.".pdf');";
    $pdf_progreso=base_url().'pdf_matriz_acciones/matriz_progreso_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf';
}
if($abierto>0){
    //$pdf_abierto="javascript:window.open('".base_url().'pdf_matriz_acciones/matriz_abierto_'.$id_proy.".pdf');";
    $pdf_abierto=base_url().'pdf_matriz_acciones/matriz_abierto_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf';
}

$html.='<style type="text/css">
    .btn-success {
      color: #fff;
      background-color: #198754;
      border-color: #198754;
      border-radius: 25px; 
    }
     
    .btn-warning {
      color: #000;
      background-color: #ffc107;
      border-color: #ffc107; }
    
    .btn-info {
      color: #000;
      background-color: #0dcaf0;
      border-color: #0dcaf0; 
      font-size:25px; }
      
    .btn-danger {
      color: #fff;
      background-color: #dc3545;
      border-color: #dc3545; }

    a{ color:white; font-size:25px; }
</style>
    <table>
        <tr>
            <th><br></th>
        </tr>
    </table>
    <table>
        <tr>
            <th width="30%"></th>
            <th width="40%">
                <table class="table" border="0" width="100%">
                    <tr class="btn-info">
                        <th><button id="btn_tot"> TOTAL DE HALLAZGOS '.$total.'</button></th>
                    </tr>
                    <tr>
                        <th></th>
                    </tr>
                    <tr class="btn-success">
                        <th><a href="'.$pdf_cerrado.'" id="btn_cerrado" > CERRADOS '.$cerrado.'</a></th>
                    </tr>
                    <tr>
                        <th></th>
                    </tr>
                    <tr class="btn-warning">
                        <th><a href="'.$pdf_progreso.'" id="btn_progreso"> EN PROGRESO '.$progreso.'</a></th>
                    </tr>
                    <tr>
                        <th></th>
                    </tr>
                    <tr class="btn-danger">
                        <th><a href="'.$pdf_abierto.'" id="btn_abierto"> ABIERTOS '.$abierto.'</a></th>
                    </tr>
                </table>
            </th>
            <th width="30%"></th>
            
        </tr>
        <tr>
            <th width="50%">
                '.$img.'
            </th>
            <th width="50%">
                '.$img2.'
            </th>
        </tr>
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');

//log_message('error', 'html : '.$html);

$ruta=$_SERVER['DOCUMENT_ROOT'];
//$pdf_str = $pdf->Output($ruta.'snfpro/pdf_matriz_acciones/matriz_'.$id_proy.'.pdf','F'); //local
//$pdf_str = $pdf->Output($ruta.'pdf_matriz_acciones/matriz_'.$id_proy.'.pdf','F'); //server

//$val_ruta=1; //local
$val_ruta=2; //server

if ($val_ruta == 1) //local
{   
    $url = $ruta.'snfpro/pdf_matriz_acciones/matriz_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //local
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'snfpro/pdf_matriz_acciones/matriz_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }else{
       $pdf_str = $pdf->Output($ruta.'snfpro/pdf_matriz_acciones/matriz_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }
}else{
    $url = $ruta.'pdf_matriz_acciones/matriz_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //server
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'pdf_matriz_acciones/matriz_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }else{
        $pdf_str = $pdf->Output($ruta.'pdf_matriz_acciones/matriz_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }
}


$pdf->endTOCPage();
$pdf->Output('EntregableMatrizAcciones.pdf', 'I');

?>