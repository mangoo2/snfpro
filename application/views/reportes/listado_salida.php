<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Reportes de Salida</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <input type="hidden" id="tu" value="<?php $this->session->userdata("idpersonal_tz") ?>">
              <br>
            </div>
            <div class="col-md-3">
              <?php if($this->session->userdata("idpersonal_tz")==1){ ?>
                <div class="form-group">
                  <label class="control-label">Cliente</label>
                  <select class="form-control round" id="id_cliente">
                  </select>
                </div>
              <?php } else{ ?>
                <div class="form-group">
                  <label class="control-label">Cliente</label>
                  <select class="form-control round" id="id_cliente">
                    <?php foreach ($clis->result() as $c) {
                      echo "<option value='".$c->id."'>".$c->empresa."</option>";
                    } ?>
                  </select>
                </div>
              <?php } ?>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="control-label">Proyecto</label>
                <select class="form-control round" id="id_proyecto">
                  <option value="0"></option>
                </select>
              </div>
            </div>
            <div class="col-sm-3">
              <label>Mes</label>
              <select class="form-control" id="mes">
                <option value="0"></option>
              </select>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_ordenes">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Cliente</th>
                      <th scope="col">Tipo de servicio</th>
                      <th scope="col">Tipo de reporte</th>
                      <th scope="col">Fecha y hora</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modaldepartamento" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Departamentos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" id="id_pd" value="0">
          <input type="hidden" id="anio_pd" value="0">
          <input type="hidden" id="tipo_sol" value="0">
          <input type="hidden" id="num_tel" value="0">
          <div class="mb-3 row">
            <label class="col-sm-2 col-form-label">Departamentos</label>
            <div class="col-sm-8">
              <select class="form-control" id="departamento" required>

              </select>
            </div>
          </div>
        </div>        
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button"  onclick="generarPDFDpto()" >Aceptar</button>
        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="Cerrar" title="">Cerrar</button>
      </div>
    </div>
  </div>
</div>