<?php 
  header("Content-Type: text/html;charset=UTF-8");
  header("Pragma: public");
  header("Expires:0");
  header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
  header("Content-Type: application/force-download");
  header("Content-Type: application/octet-stream");
  header("Content-Type: application/download");
  header("Content-Type: application/vnd.ms-excel;");
  header("Content-Disposition: attachment; filename=check_list".$id_proy."_".$mes.".xls");

  function redimensionar($src,$name){
    $imagen = $src; //Imagen original
    $imagenNueva = FCPATH."uploads/monitoreo/2_".$name; //Nueva imagen
    $nAncho = 105; //Nuevo ancho
    $nAlto = 75;  //Nuevo alto
    
    //Creamos una nueva imagen a partir del fichero inicial
    if(pathinfo($name,PATHINFO_EXTENSION)=="jpg" || pathinfo($name,PATHINFO_EXTENSION)=="jpeg" || pathinfo($name,PATHINFO_EXTENSION)=="JPG" || pathinfo($name,PATHINFO_EXTENSION)=="JPEG")
        $imagen = imagecreatefromjpeg($imagen); 
    else if(pathinfo($name,PATHINFO_EXTENSION)=="png" || pathinfo($name,PATHINFO_EXTENSION)=="PNG")
        $imagen = imagecreatefrompng($imagen); 

    //Obtenemos el tamaño 
    $x = imagesx($imagen);
    $y = imagesy($imagen);
    
    // Crear una nueva imagen, copia y cambia el tamaño de la imagen
    $img = imagecreatetruecolor($nAncho, $nAlto);
    imagecopyresized($img, $imagen, 0, 0, 0, 0, $nAncho, $nAlto, $x, $y);
    
    //Creamos el archivo jpg
    imagejpeg($img, $imagenNueva);
    return "2_".$name;
  }

    $html_head=""; $html=""; $html_foot=""; $i=0; $array=array();
    $fecha_f="";

    $params["id_proy"] = $id_proy;
    $params["mes"] = $mes;
    $params["anio"] = $anio;
    $params["id_act"] = $id_act;

    //log_message('error', 'id_proy: '.$id_proy);
    //log_message('error', 'mes : '.$mes);
    //log_message('error', 'anio : '.$anio);

    foreach ($act as $a) {
      $fecha_i = ''.$a->mes.'/01/'.$a->anio.'';
      //$fecha_f = ''.$a->mes.'/'.$a->ultimo.'/'.$a->anio.'';
      $fecha_f = $a->ultimo;
    }

    if($fecha_f==""){
      $fecha_i = ''.$mes.'/01/'.date("Y").'';
      $fecha_f = ''.$mes.'/31/'.date("Y").'';
    }

    $begin = new DateTime($fecha_i);
    $end = new DateTime($fecha_f);
    $end = $end->modify( '+1 day' );
    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);
    $html_head= "<tr>
                  <th>#</th>
                  <th>ZONA</th>
                  <th>INSPECCIÓN</th>
                  <th colspan='2'>FOTO</th>
                  <th>DESCRIPCIÓN DE LA ACTIVIDAD</th>
                  <th>COMENTARIOS</th>
                  <th>FRECUENCIA</th>";

    $band_dia=0;
    foreach($daterange as $date){
      if(date('l', strtotime($date->format("d-m-Y"))) == 'Sunday'){
        $band_dia++;
        //log_message('error', 'dia del fin : '.$date->format("d"));
        $html_head.= "<th id='dia_num".$band_dia."'>".$date->format("d")."</th>";
        array_push($array,$date->format("d"));
      }
    }
    $html_head.= "<th>ÁREA DE OPORTUNIDAD</th>
              </tr>";

    //log_message('error', 'contenido del array : '.$array[0]);
    //log_message('error', 'html_head : '.$html_head);
    $cont=0; $cont2=0; $cont3=0; $cont4=0; $cont5=0; 
    //log_message('error', 'count del array : '.count($array));

    $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].')';
    if(isset($array[4])){
      $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].' or DATE_FORMAT(ma.fecha, "%d")='.$array[4].')';
    }

    $cont_area=0; $cont_area1=0; $cont_area2=0; $cont_area3=0; $cont_area4=0; $i=0; $i1=0; $i2=0; $i3=0; $i4=0;
    //$act=$this->ModelReportes->get_result_monitoreoNvo($params,1,$where);
    $act=$this->ModelReportes->get_result_monitoreoNvo($params,0,$where,$id_act);
    foreach ($act as $a) {
      $cont=0;
      $fecha_camb = strtotime($a->fecha);
      $semana = date('W', $fecha_camb);
      $i++;
      $cont_exist=0;
      $logo=$a->foto;
      $anio=$a->anio;
      $tmp=explode(", ",$a->dias_act);
      $tmp_seg=explode(", ",$a->seguimiento_act);
      //log_message('error', 'tmp : '.count($tmp));
      //log_message('error', 'count array : '.count($array));
      if($i->foto!=""){
        $imagen = base_url()."uploads/monitoreo/".$a->foto;
        $img_med = redimensionar($imagen, $a->foto);
        $img='<img src="'.base_url()."uploads/monitoreo/".$img_med.'" />';
      }else{
        $img='';
      }

      $html.="<tr>
              <td>".$i."</td>
              <td>".$a->zona."</td>
              <td>".$a->inspeccion."</td>
              <td colspan='2' style='align-items: center; justify-content: center'><p class='pspaces'></p>
                <table>
                  <tr> <td colspan='3' width='100%''></td></tr>
                  <tr>
                    <td width='10%'></td>
                    <td width='80%'>".$img."</td>
                    <td width='10%'></td>
                  </tr>
                </table><br><br><br><br>
              </td>
              <td>".$a->nombre."</td>
              <td>".$a->observaciones."</td>
              <td>".$a->frecuencia."</td>";
              for($j=0; $j<count($array); $j++){
                //for($k=0; $k<count($tmp); $k++){
                //log_message('error', 'array : '.$array[$j]);
                //log_message('error', 'tmp : '.$tmp[$j]);
                /*if(isset($tmp[0]) && $array[$j]==$tmp[0] || isset($tmp[1]) && $array[$j]==$tmp[1] || isset($tmp[2]) && $array[$j]==$tmp[2] || isset($tmp[3]) && $array[$j]==$tmp[3] || isset($tmp[4]) && $array[$j]==$tmp[4]){
                  $html.="<td>".$a->seguimiento."</td>";
                  if($a->seguimiento=="1"){
                    $cont++;
                  }
                }else{
                  $html.="<td></td>";
                }*/
                if (in_array($array[$j], $tmp)){
                  $html.="<td>".$tmp_seg[$cont_exist]."</td>";
                  if($tmp_seg[$cont_exist]==1 || $tmp_seg[$cont_exist]==2){
                    $cont++;
                  }
                }else{
                  $html.="<td></td>";
                }
                if (in_array($array[$j], $tmp)){
                  //log_message('error', 'existe: '.$tmp[$j]);
                  //log_message('error', 'existe en: '.$j);
                  if(isset($j) && $j==0 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==0 && $tmp_seg[$cont_exist]==2){
                    $cont_area++;
                  }
                  if(isset($j) && $j==0 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==0 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==0 && $tmp_seg[$cont_exist]==2){
                    $i++;
                  }
                  if(isset($j) && $j==1 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==1 && $tmp_seg[$cont_exist]==2){
                    $cont_area1++;
                  }
                  if(isset($j) && $j==1 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==1 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==1 && $tmp_seg[$cont_exist]==2){
                      $i1++;
                  }
                  if(isset($j) && $j==2 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==2 && $tmp_seg[$cont_exist]==2){
                    $cont_area2++;
                  }
                  if(isset($j) && $j==2 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==2 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==2 && $tmp_seg[$cont_exist]==2){
                      $i2++;
                  }
                  if(isset($j) && $j==3 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==3 && $tmp_seg[$cont_exist]==2){
                     $cont_area3++;
                  }
                  if(isset($j) && $j==3 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==3 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==3 && $tmp_seg[$cont_exist]==2){
                      $i3++;
                  }
                  if(isset($j) && $j==4 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==4 && $tmp_seg[$cont_exist]==2){
                    $cont_area4++;
                  }
                  if(isset($j) && $j==4 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==4 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==4 && $tmp_seg[$cont_exist]==2){
                      $i4++;
                  }
                  $cont_exist++;
                }
              }
        $html.="<td><input id='val_area' type='hidden' value='".round(($cont/$a->tot_act*100),2)."'>".round(($cont/$a->tot_act*100),2)."</td>
              </tr>";
    }
    if($i==0){
      $html_foot="<tr>
              <td colspan='5'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>
              <td><input type='hidden' id='ft1' value='0'>0</td>
              <td><input type='hidden' id='ft2' value='0'>0</td>
              <td><input type='hidden' id='ft3' value='0'>0</td>
              <td><input type='hidden' id='ft4' value='0'>0</td>";
            if($band_dia==5){
              $html_foot.="<td><input type='hidden' id='ft5' value='0'></td>";
            }
          $html_foot.="</tr>";
    }
    else{
      //log_message('error', 'cont_area : '.$cont_area);
      //log_message('error', 'i : '.$i);
      $ft1=0; $ft2=0; $ft3=0; $ft4=0; $ft5=0;
      if($i>0)
        $ft1=round(($cont_area/$i*100),2);
      if($i1>0)
        $ft2=round(($cont_area1/$i1*100),2);
      if($i2>0)
        $ft3=round(($cont_area2/$i2*100),2);
      if($i3>0)
        $ft4=round(($cont_area3/$i3*100),2);
      if($i4>0)
        $ft5=round(($cont_area4/$i4*100),2);

      $html_foot="<tr>
              <td colspan='5'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>
              <td><input type='hidden' id='ft1' value='".$ft1."'>".$ft1."</td>
              <td><input type='hidden' id='ft2' value='".$ft2."'>".$ft2."</td>
              <td><input type='hidden' id='ft3' value='".$ft3."'>".$ft3."</td>
              <td><input type='hidden' id='ft4' value='".$ft4."'>".$ft4."</td>";
          if($band_dia==5){
            $html_foot.="<td><input type='hidden' id='ft5' value='".$ft5."'>".$ft5."</td>";
          }
      $html_foot.="<td></td>
      </tr>";
    }

    echo '<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <table style="text-align:center;" class="table" border="1" width="100%" id="getMonitoreoFin">
            <thead class="table_thead" id="tble_head">
             '.$html_head.'
            </thead>
            <tbody id="body_table">
              '.$html.'
            </tbody>
            <tfoot id="tfoot">
              '.$html_foot.'
              <tr>
                <td colspan="8"></td>
                <td colspan="4">Firma de cliente</td>
                <td height="70px"></td>
              </tr>
            </tfoot>
          </table>';

