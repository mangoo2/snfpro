<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $GLOBALS['tabla']="";
    $GLOBALS['tabla2']="";
    $GLOBALS['tabla3']="";
    $GLOBALS['tabla4']="";
    $GLOBALS['tabla5']="";
    $GLOBALS['grafica']="";
    $GLOBALS['grafica2']="";
    $GLOBALS['grafica3']="";
    $GLOBALS['grafica4']="";
    $GLOBALS['grafica5']="";
    $GLOBALS['logo_cli']=$logo_cli;
    foreach ($gra as $i) {
        if($i->tipo==0)
            $GLOBALS['tabla']=$i->grafica;
        if($i->tipo==1)
            $GLOBALS['grafica']=$i->grafica;
        if($i->tipo==2)
            $GLOBALS['grafica2']=$i->grafica;
        if($i->tipo==3)
            $GLOBALS['grafica3']=$i->grafica;
        if($i->tipo==4)
            $GLOBALS['tabla2']=$i->grafica;
        if($i->tipo==5)
            $GLOBALS['tabla3']=$i->grafica;
        if($i->tipo==6)
            $GLOBALS['tabla4']=$i->grafica;
        if($i->tipo==7)
            $GLOBALS['tabla5']=$i->grafica;
        if($i->tipo==8)
            $GLOBALS['grafica4']=$i->grafica;
        if($i->tipo==9)
            $GLOBALS['grafica5']=$i->grafica;
    }

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logofinalsys2.png';
        if($GLOBALS['logo_cli']!="")
            $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
        else
            $logo_cliente ="#";
        //$this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);

        $html = '<table width="100%">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="15%"><img src="'.$logos.'" height="90px"></td>
                <td width="65%" style="font-weight:bold; color:rgba(68, 114, 196); text-align:center; font-size:32px">SERVICIOS NUEVOS DE FILTRACIÓN HISPANOMEXICANOS S.A DE C.V.</td>
                <td width="20%" align="center"><img src="'.$logo_cliente.'" height="90px"></td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $pie = base_url().'public/img/footer.png';
        $html = '
        <table width="100%" cellpadding="2">
        <tr>
            <td width="100%"><img src="'.$pie.'"></td>
        </tr>
      </table>';
    
        //$this->writeHTMLCell(188, '', 12, 100, $html, 0, 0, 0, true, 'C', true);
        $this->Image($pie, 0, 165, 310, 50, 'PNG', '', '', true, 310, '', false, false, 0);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('SALIDA - PRESENTISMO DE PERSONAL');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10,37,10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 21);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setPrintHeader(false);
// add a page
$pdf->AddPage('L', 'A4'); 

$img_file = base_url().'public/img/portada_rotacion2.png';

                     //left, top, ancho alto
//$pdf->Image($img_file, 0, 0, 452, 530, '', '', 10, false, 310, '', false, false, 0);
$pdf->Image($img_file, 0, 0, 400, 190, 'PNG', '', '', true, 310, '', false, false, 0);

$logos = base_url().'public/img/logofinalsys2.png';
$pdf->Image($logos, 16, 13, 39,25, 'PNG', '', '', true, 150, '', false, false, false);

if($GLOBALS['logo_cli']!=""){
    $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
    $pdf->Image($logo_cliente, 239, 13, 39,25, 'JPG', '', '', true, 150, '', false, false, 0);
}
$pdf->setPrintHeader(true);


$img0=""; $img=""; $img2=""; $img3=""; $img4=""; $img5="";
$img6=""; $img7=""; $img8=""; $img9="";

$tabla=$GLOBALS['tabla'];
if($tabla!=""){
    $img0='<img align="center" width="560px" src="'.base_url()."pdf_presentismo/".$tabla.'">';
}
$grafica=$GLOBALS['grafica'];
if($grafica!=""){
    $img='<img align="center" width="560px" src="'.base_url()."pdf_presentismo/".$grafica.'">';
}

$grafica2=$GLOBALS['grafica2'];
if($grafica2!=""){
    $img2='<img align="center" width="820px" src="'.base_url()."pdf_presentismo/".$grafica2.'">';
}

$grafica3=$GLOBALS['grafica3'];
if($grafica3!=""){
    $img3='<img align="center" width="820px" src="'.base_url()."pdf_presentismo/".$grafica3.'">';
}
$tabla2=$GLOBALS['tabla2'];
if($tabla2!=""){
    $img4='<img align="center" width="560px" src="'.base_url()."pdf_presentismo/".$tabla2.'">';
}
$tabla3=$GLOBALS['tabla3'];
if($tabla3!=""){
    $img5='<img align="center" width="560px" src="'.base_url()."pdf_presentismo/".$tabla3.'">';
}

$tabla4=$GLOBALS['tabla4'];
if($tabla4!=""){
    $img6='<img align="center" width="560px" src="'.base_url()."pdf_presentismo/".$tabla4.'">';
}
$tabla5=$GLOBALS['tabla5'];
if($tabla5!=""){
    $img7='<img align="center" width="560px" src="'.base_url()."pdf_presentismo/".$tabla5.'">';
}

$grafica4=$GLOBALS['grafica4'];
if($grafica4!=""){
    $img8='<img align="center" width="780px" src="'.base_url()."pdf_presentismo/".$grafica4.'">';
}
$grafica5=$GLOBALS['grafica5'];
if($grafica5!=""){
    $img9='<img align="center" width="820px" src="'.base_url()."pdf_presentismo/".$grafica5.'">';
}
// add a page
/*$pdf->AddPage('L', 'A4');  //se comenta a peticion el dia 17 d enero de 2023
$pdf->setPrintFooter(false);
$html=""; 
$html.='
    <table>
        <tr>
            <th><br></th>
        </tr>
    </table>
    <table>
        <tr>
            <th align="center" width="100%"><b> '.mb_strtoupper($name_mes,"UTF-8").'</b></th>
        </tr>
        <tr>
            <th align="center" width="100%">'.$img0.'</th>
        </tr>
        <tr>
            <th align="center" width="100%">'.$img.'</th>
        </tr>
    </table>';
$pdf->writeHTML($html, true, false, true, false, ''); */

//log_message('error', 'html : '.$html);

$pdf->AddPage('L', 'A4'); 
$pdf->setPrintFooter(false);
$html="";
$html.='
    <table>
        <tr>
            <th><br></th>
        </tr>
    </table>
    <table>
        <tr>
            <th align="center" width="45%">'.$img4.'</th>
            <th width="10%"></th>
            <th align="center" width="45%">'.$img5.'</th>
        </tr>
        <tr>
            <th><br></th>
        </tr>
        <tr>
            <th align="center" width="50%"><b>PRESENTISMO '.mb_strtoupper($name_mes,"UTF-8").' '.$anio.'</b></th>
            <th align="center" width="50%"><b>TURN OVER '.mb_strtoupper($name_mes,"UTF-8").' '.$anio.'</b></th>
        </tr>
        <tr>
            <th style="font-size:9px" align="left" width="30%">% PRESENTISMO</th>
            <th align="left" width="25%"></th>
            <th style="font-size:9px" align="left" width="30%">% TURN OVER</th>
        </tr>
        <tr>
            <th><br></th>
        </tr>
        <tr>
            <th align="center" width="45%">'.$img2.'</th>
            <th width="10%"></th>
            <th align="center" width="45%">'.$img3.'</th>
        </tr>
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->AddPage('L', 'A4'); 
$pdf->setPrintFooter(false);
$html="";
$html.='
    <table>
        <tr>
            <th><br></th>
        </tr>
    </table>
    <table>
        <tr>
            <th align="center" width="95%"><b>VACACIONES '.$anio.'</b></th>
        </tr>
        <tr>
            <th align="center" width="90%">'.$img6.'</th>
        </tr>
        <tr>
            <th><br></th>
        </tr>
        <tr>
            <th align="center" width="100%">'.$img8.'</th>
        </tr>
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->AddPage('L', 'A4'); 
$pdf->setPrintFooter(false);
$html="";
$html.='
    <table>
        <tr>
            <th><br></th>
        </tr>
    </table>
    <table>
        <tr>
            <th align="center" width="95%"><b>VACACIONES '.$anio.'</b></th>
        </tr>
        <tr>
            <th align="center" width="90%">'.$img7.'</th>
        </tr>
        <tr>
            <th><br></th>
        </tr>
        <tr>
            <th align="center" width="100%">'.$img9.'</th>
        </tr>
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');

//log_message('error', 'html : '.$html);
$ruta=$_SERVER['DOCUMENT_ROOT'];

//$val_ruta=1; //local
$val_ruta=2; //server

if ($val_ruta == 1) //local
{   
    $url = $ruta.'snfpro/pdf_presentismo/presentismo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //local
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'snfpro/pdf_presentismo/presentismo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }else{
       $pdf_str = $pdf->Output($ruta.'snfpro/pdf_presentismo/presentismo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }
}else{
    $url = $ruta.'pdf_presentismo/presentismo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //server
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'pdf_presentismo/presentismo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }else{
        $pdf_str = $pdf->Output($ruta.'pdf_presentismo/presentismo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }
}

$pdf->endTOCPage();
$pdf->Output('EntregablePresentismo.pdf', 'I');

?>