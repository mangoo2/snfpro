<style type="text/css">
    table thead{
        background-color:#00B0F0;
        color: #FFFFFF;
        text-align: center;
        vertical-align: middle;
    }
    #canvastg2,#canvastg2_cli{
        display: none;
    }
</style>
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Resultados de cumplimiento</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <input type="hidden" id="id_proy" value="<?php echo $id_proy; ?>">
        <input type="hidden" id="id_cli" value="<?php echo $id_cli; ?>">
        <input type="hidden" id="anio" value="<?php echo $anio; ?>">
        <input type="hidden" id="mes" value="<?php echo $mes; ?>">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <table>
                                <tr>
                                    <th><input type="hidden" id="total" value="<?php echo $total; ?>">
                                    </th>
                                </tr>
                                <tr>
                                    <th><input type="hidden" id="cerrado" value="<?php echo $cerrado; ?>">
                                        <input type="hidden" id="cerrado_cli" value="<?php echo $cerrado_cli; ?>"></th>
                                </tr>
                                <tr>
                                    <th><input type="hidden" id="progreso" value="<?php echo $progreso; ?>">
                                        <input type="hidden" id="progreso_cli" value="<?php echo $progreso_cli; ?>"></th>
                                </tr>
                                <tr>
                                    <th><input type="hidden" id="abierto" value="<?php echo $abierto; ?>">
                                        <input type="hidden" id="abierto_cli" value="<?php echo $abierto_cli; ?>"></th>
                                </tr>
                            </table>
                            <table class="table" border="0" width="100%">
                                <tr>
                                    <th><button id="btn_tot" style="width:250px" class="btn btn-info">TOTAL DE HALLAZGOS <?php echo $total; ?></button></th>
                                </tr>
                                <tr>
                                    <th><button id="btn_cerrado" style="width:250px" class="btn btn-success">CERRADOS <?php echo $cerrado+$cerrado_cli; ?></button></th>
                                </tr>
                                <tr>
                                    <th><button id="btn_progreso" style="width:250px" class="btn btn-warning">EN PROGRESO <?php echo $progreso+$progreso_cli; ?></button></th>
                                </tr>
                                <tr>
                                    <th><button id="btn_abierto" style="width:250px" class="btn btn-danger">ABIERTOS <?php echo $abierto+$abierto_cli; ?></button></th>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-8">
                            <div class="card">
                              <div class="card-body p-0 chart-block">
                                <div class="chart-overflow" id="grafica_cumple"></div>
                              </div>
                            </div>
                        </div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8">
                            <div class="card">
                              <div class="card-body p-0 chart-block">
                                <div class="chart-overflow" id="grafica_cumple_cli"></div>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table>
                                <tr>
                                    <th><br></th>
                                </tr>
                            </table>
                        </div>
                        <?php $html_cerrado=""; $html_progeso=""; $html_abierto=""; $colsc=7; $colsp=7; $colsa=7;
                            foreach ($res as $r) {
                                $fecha2=date("Y-m-d", strtotime($r->fecha_deteccion));
                                $mes_detec=date("m", strtotime($fecha2));
                                $anio_detec=date("Y", strtotime($fecha2));
                                if($r->avance==4 && $mes_detec==$mes && $anio_detec==$anio){
                                    if($r->durante!=""){
                                        $colsc=8;
                                    }
                                }
                                if($r->avance>0 && $r->avance<4){
                                    if($r->durante!=""){
                                        $colsp=8;
                                    }
                                }
                                if($r->avance==0){
                                    if($r->durante!=""){
                                        $colsa=8;
                                    }
                                }
                            } 
                            foreach ($res as $r) {
                                $tipo_hallazgo="";
                                if($r->tipo_hallazgo==1)
                                    $tipo_hallazgo="SNF";
                                else if($r->tipo_hallazgo==2)
                                    $tipo_hallazgo="Cliente";

                                $fecha2=date("Y-m-d", strtotime($r->fecha_deteccion));
                                $mes_detec=date("m", strtotime($fecha2));
                                $anio_detec=date("Y", strtotime($fecha2));

                                if($r->avance==4 && $mes_detec==$mes && $anio_detec==$anio){
                                    $img_antes="";
                                    $img_durante="";
                                    $img_desps="";
                                    if($r->antes!=""){
                                        $img_antes='<img src="'.base_url().'uploads/evidencias/'.$r->antes.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    if($r->durante!=""){
                                        //$colsc=8;
                                        $img_durante='<img src="'.base_url().'uploads/evidencias/'.$r->durante.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    if($r->despues!=""){
                                        $img_desps='<img src="'.base_url().'uploads/evidencias/'.$r->despues.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    $html_cerrado.='<tr>
                                        <td>'.$r->area.'</td>
                                        <td>'.$r->hallazgo.'</td>
                                        <td>'.$tipo_hallazgo.'</td>
                                        <td>'.$img_antes.'</td>
                                        <td>'.$r->acciones.'</td>';
                                    if($colsc==8){
                                        $html_cerrado.='<td>'.$img_durante.'</td>';
                                    }
                                    $html_cerrado.='<td>'.$r->fecha_cierre.'</td>
                                        <td>'.$img_desps.'</td>
                                    </tr>';
                                }
                                if($r->avance>0 && $r->avance<4){
                                    $img_antes="";
                                    $img_durante="";
                                    $img_desps="";
                                    if($r->antes!=""){
                                        $img_antes='<img src="'.base_url().'uploads/evidencias/'.$r->antes.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    if($r->durante!=""){
                                        //$colsp=8;
                                        $img_durante='<img src="'.base_url().'uploads/evidencias/'.$r->durante.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    if($r->despues!=""){
                                        $img_desps='<img src="'.base_url().'uploads/evidencias/'.$r->despues.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    $html_progeso.='<tr>
                                        <td>'.$r->area.'</td>
                                        <td>'.$r->hallazgo.'</td>
                                        <td>'.$tipo_hallazgo.'</td>
                                        <td>'.$img_antes.'</td>
                                        <td>'.$r->acciones.'</td>';
                                    if($colsp==8){
                                        $html_progeso.='<td>'.$img_durante.'</td>';
                                    }
                                    $html_progeso.='<td>'.$r->fecha_cierre.'</td>
                                        <td>'.$img_desps.'</td>
                                    </tr>';
                                }
                                if($r->avance==0){
                                    $img_antes="";
                                    $img_durante="";
                                    $img_desps="";
                                    if($r->antes!=""){
                                        $img_antes='<img src="'.base_url().'uploads/evidencias/'.$r->antes.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    if($r->durante!=""){
                                        //$colsa=8;
                                        $img_durante='<img src="'.base_url().'uploads/evidencias/'.$r->durante.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    if($r->despues!=""){
                                        $img_desps='<img src="'.base_url().'uploads/evidencias/'.$r->despues.'" class="rounded mr-3 img-fluid" height="142" width="142">';
                                    }
                                    $html_abierto.='<tr>
                                        <td>'.$r->area.'</td>
                                        <td>'.$r->hallazgo.'</td>
                                        <td>'.$tipo_hallazgo.'</td>
                                        <td>'.$img_antes.'</td>
                                        <td>'.$r->acciones.'</td>';
                                    if($colsa==8){
                                        $html_abierto.='<td>'.$img_durante.'</td>';
                                    }
                                    $html_abierto.='<td>'.$r->fecha_cierre.'</td>
                                        <td>'.$img_desps.'</td>
                                    </tr>';
                                }
                            } 
                        ?>
                        <div class="col-md-12" id="cont_cerrado" style="display: none;">
                            <table class="table" border="1">
                                <thead>
                                        <tr><th colspan="<?php echo $colsc; ?>">CERRADO</th></tr>
                                        <tr>
                                            <th>ÁREA</th>
                                            <th>HALLAZGO</th>
                                            <th>TIPO</th>
                                            <th>EVIDENCIA ANTES</th>
                                            <th>ACCIONES REQUERIDAS</th>
                                            <?php if($colsc==8){ 
                                                echo "<th>EVIDENCIA DURANTE</th>";
                                            }?>
                                            <th>FECHA DE CIERRE</th>
                                            <th>EVIDENCIA DESPUÉS</th>
                                        </tr>
                                </thead>
                                <tbody>
                                    <?php echo $html_cerrado; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12" id="cont_progreso" style="display: none;">
                            <table class="table" border="1">
                                <thead>
                                        <tr><th colspan="<?php echo $colsp; ?>">EN PROGRESO</th></tr>
                                        <tr>
                                            <th>ÁREA</th>
                                            <th>HALLAZGO</th>
                                            <th>TIPO</th>
                                            <th>EVIDENCIA ANTES</th>
                                            <th>ACCIONES REQUERIDAS</th>
                                            <?php if($colsp==8){ 
                                                echo "<th>EVIDENCIA DURANTE</th>";
                                            }?>
                                            <th>FECHA DE CIERRE</th>
                                            <th>EVIDENCIA DESPUÉS</th>
                                        </tr>
                                </thead>
                                <tbody>
                                    <?php echo $html_progeso; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12" id="cont_abierto" style="display: none;">
                            <table class="table" border="1">
                                <thead>
                                        <tr><th colspan="<?php echo $colsa; ?>">ABIERTO</th></tr>
                                        <tr>
                                            <th>ÁREA</th>
                                            <th>HALLAZGO</th>
                                            <th>TIPO</th>
                                            <th>EVIDENCIA ANTES</th>
                                            <th>ACCIONES REQUERIDAS</th>
                                            <?php if($colsa==8){ 
                                                echo "<th>EVIDENCIA DURANTE</th>";
                                            }?>
                                            <th>FECHA DE CIERRE</th>
                                            <th>EVIDENCIA DESPUÉS</th>
                                        </tr>
                                </thead>
                                <tbody>
                                    <?php echo $html_abierto; ?>
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>