<script src="<?php echo base_url(); ?>plugins/chartjs-plugin/2.7.2/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/chartjs-plugin/chartjs-plugin-datalabels.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>

<style type="text/css">
    .table_thead{
        background-color:#00B0F0;
        color: #FFFFFF !important;
        text-align: center;
        vertical-align: middle;
    }
    #canvastg2,#canvastg2_2,#canvastg3{
        display: none;
    }
    button{
      background: black;
      cursor: pointer;
      border: none; 
      padding: 16px 32px;
      color: white;
      font-size: 12px;
      font-weight: bold;
      position: relative;
      border-radius: 10px;
      text-align: center;
    }

    button::before {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      z-index: -4;
      width: 100%;
      height:100%;
      background: linear-gradient( 60deg,
     #2ADB62, #E000B9, #C8DE3C, #DEA03C, #E82E20, #DEA03C, #E000B9);
      background-size: 900%;
      border-radius: 10px;
      filter: blur(8px);
      animation: glowing 20s linear infinite;
    }

    @keyframes glowing{
      0%{
        background-position: 0 0;
      }
      50%{
        background-position: 400% 0;
      }
      100%{
        background-position: 0 0;
      }
    }
</style>

<div class="container-fluid">
    <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3>Resultados de monitoreo de actividades</h3>
          </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <input type="hidden" id="id_proy" value="<?php echo $id_proy; ?>">
        <input type="hidden" id="cant_dia" value="0">
        <input type="hidden" id="mes" value="<?php echo $mes; ?>">
        <input type="hidden" id="anio" value="<?php echo $anio; ?>">
        <input type="hidden" id="id_cli" value="<?php echo $id_cli; ?>">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div style="
                            display: block;
                             position: fixed;
                             z-index: 1000;
                             right: 1px;
                             bottom: 35px;
                             padding: 10px;
                             width: 180px;
                             height: 50px;
                             margin: auto;"> 
                            <button id="save_all" style="border-radius: 4px;
                                          background-color: #f4511e;
                                          border: none;
                                          color: #FFFFFF;
                                          text-align: center;">
                                <p style="font-size: 12"><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Guardar <br>y <br>recargar </p>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <table>
                                        <tr>
                                            <th width="20%"><img class="img-fluid2" height="120px" src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                            <th width="40%"><h3>CHECK LIST FIN DE SEMANA</h3></th>
                                            <th width="20%"><img align="right" class="img-fluid2" height="120px" id="logo_cli" src="<?php echo base_url();?>uploads/clientes/<?php echo $logo; ?>"></th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <table class="table" border="1" width="100%">
                                        <tr>
                                            <th width="30%" style="background-color:#00CC00;"></th>
                                            <th width="70%">Porcentaje optimo 90 a 100%</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FFFF00;"></th>
                                            <th>Porcentaje aceptable del 80 al 89 %</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FF0000;"></th>
                                            <th>79 % o menos NO ACEPTABLE</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Mes: </label>
                                    <select class="form-control round" id="mes_table" disabled>
                                        <option <?php if($mes==1) echo "selected"; ?> value="01">Enero</option>
                                        <option <?php if($mes==2) echo "selected"; ?> value="02">Febrero</option>
                                        <option <?php if($mes==3) echo "selected"; ?> value="03">Marzo</option>
                                        <option <?php if($mes==4) echo "selected"; ?> value="04">Abril</option>
                                        <option <?php if($mes==5) echo "selected"; ?> value="05">Mayo</option>
                                        <option <?php if($mes==6) echo "selected"; ?> value="06">Junio</option>
                                        <option <?php if($mes==7) echo "selected"; ?> value="07">Julio</option>
                                        <option <?php if($mes==8) echo "selected"; ?> value="08">Agosto</option>
                                        <option <?php if($mes==9) echo "selected"; ?> value="09">Septiembre</option>
                                        <option <?php if($mes==10) echo "selected"; ?> value="10">Octubre</option>
                                        <option <?php if($mes==11) echo "selected"; ?> value="11">Noviembre</option>
                                        <option <?php if($mes==12) echo "selected"; ?> value="12">Diciembre</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <?php $where=""; $html_head="";
                                $params["id_proy"]=$id_proy; $params["mes"]=$mes; $params["anio"]=$anio; $params["tipo"]="1";
                               
                                $act=$this->ModelReportes->get_result_monitoreo($params,1,$where);
                                $i=0; $array=array();
                                $anio=0; $fecha_f="";
                                foreach ($act as $a) {
                                  $fecha_i = ''.$a->mes.'/01/'.$a->anio.'';
                                  $fecha_f = $a->ultimo;
                                }

                                if($fecha_f==""){
                                  $fecha_i = ''.$params["mes"].'/01/'.date("Y").'';
                                  $ult=date("t",mktime(0,0,0,$params["mes"],1,date("Y")));
                                  $fecha_f = ''.$params["mes"].'/'.$ult.'/'.date("Y").'';
                                }

                                $begin = new DateTime($fecha_i);
                                $end = new DateTime($fecha_f);
                                $end = $end->modify( '+1 day' );
                                $interval = new DateInterval('P1D');
                                $daterange = new DatePeriod($begin, $interval ,$end);
                                $band_dia=0; $array=array();
                                foreach($daterange as $date){
                                  if(date('l', strtotime($date->format("d-m-Y"))) == 'Sunday'){
                                    $band_dia++;
                                    //log_message('error', 'dia del fin : '.$date->format("d"));
                                    $html_head.="<th id='dia_num".$band_dia."'>".$date->format("d")."</th>";
                                    array_push($array,$date->format("d"));
                                    ${'dia_f'.$date->format("d")}=$date->format("d");
                                  }
                                }
                                $cont=0; $cont2=0; $cont3=0; $cont4=0; $cont5=0;
                                $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].')';
                                if(isset($array[4])){
                                  $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].' or DATE_FORMAT(ma.fecha, "%d")='.$array[4].')';
                                }

                                $get=$this->ModelReportes->getTotalActiv($params);
                                foreach ($get as $g) {
                                    $html=""; 
                                    $cont_area=0; $cont_area1=0; $cont_area2=0; $cont_area3=0; $cont_area4=0; $tmp2=[]; $i_aux=0; $i=0; $i1=0; $i2=0; $i3=0; $i4=0;
                                    $val_activ=0; $cont_activ=0; $efi=0;
                                    $dia_ant=0; $inputs_data=""; $array_act=array();
                                    $item=0;
                                    $act=$this->ModelReportes->get_result_monitoreoNvo($params,1,$where,$g->id_limpieza);
                                    foreach ($act as $a) {
                                        $cont=0;
                                        $fecha_camb = strtotime($a->fecha);
                                        $semana = date('W', $fecha_camb);
                                        $i_aux++;
                                        $cont_exist=0;
                                        $anio=$a->anio;
                                        $tmp=explode(",",$a->dias_act);
                                        $tot_act=$a->tot_act;
                                        $tmp2=$tmp;
                                        $tmp_seg=explode(", ",$a->seguimiento_act);
                                        $item++;
                                        ${'inspeccion'.$g->id_limpieza.'_'.$item}=$a->inspeccion;
                                        ${'objetivo'.$g->id_limpieza.'_'.$item}=100;
                                        $html.="<tr>
                                                  <td>".$item."</td>
                                                  <td>".$a->zona."</td>
                                                  <td>".$a->inspeccion."</td>
                                                  <td>".$a->nombre."</td>
                                                  <td>".$a->comentarios_br."</td>
                                                  <td>".$a->frecuencia."</td>";
                                        for($j=0; $j<count($array); $j++){
                                            $tot_act2=$tot_act-($tot_act+$j);
                                            if (in_array($array[$j], $tmp)){
                                                if(isset($tmp_seg[$cont_exist])){
                                                    if($tmp_seg[$cont_exist]==1) $sel1="selected"; else $sel1="";
                                                    if($tmp_seg[$cont_exist]==0) $sel0="selected"; else $sel0="";
                                                    if($tmp_seg[$cont_exist]==2) $sel2="selected"; else $sel2="";
                                                    //$html.="<td>".$tmp_seg[$cont_exist]."</td>";
                                                    $html.='<td><select class="form-control" id="estatus" onchange="cambiaEstatus(1,this.value,'.$array[$j].','.$g->id_limpieza.',\''.$a->inspeccion. '\')">
                                                              <option value="" disabled selected></option>
                                                              <option '.$sel1.' value="1">1</option>
                                                              <option '.$sel0.' value="0">0</option>
                                                              <option '.$sel2.' value="2">2-N/A</option>
                                                            </select></td>';
                                                    if($tmp_seg[$cont_exist]==1 || $tmp_seg[$cont_exist]=="2"){
                                                        $cont++;
                                                    }
                                                }
                                            }else{
                                                $html.='<td><div class="col-sm-12">
                                                        <select class="form-control" id="estatus" onchange="cambiaEstatus(1,this.value,'.$array[$j].','.$g->id_limpieza.',\''.$a->inspeccion. '\')">
                                                          <option value="" disabled selected></option>
                                                          <option value="1">1</option>
                                                          <option value="0">0</option>
                                                          <option value="2">2-N/A</option>
                                                        </select>
                                                      </div>
                                                    </td>';
                                            }
                                            if (in_array($array[$j], $tmp)){
                                                if(isset($j) && $j==0 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==0 && $tmp_seg[$cont_exist]==2){
                                                    $cont_area++;
                                                }
                                                if(isset($j) && $j==0 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==0 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==0 && $tmp_seg[$cont_exist]==2){
                                                    $i++;
                                                }
                                                if(isset($j) && $j==1 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==1 && $tmp_seg[$cont_exist]==2){
                                                    $cont_area1++;
                                                }
                                                if(isset($j) && $j==1 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==1 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==1 && $tmp_seg[$cont_exist]==2){
                                                    $i1++;
                                                }
                                                if(isset($j) && $j==2 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==2 && $tmp_seg[$cont_exist]==2){
                                                    $cont_area2++;
                                                }
                                                if(isset($j) && $j==2 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==2 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==2 && $tmp_seg[$cont_exist]==2){
                                                    $i2++;
                                                }
                                                if(isset($j) && $j==3 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==3 && $tmp_seg[$cont_exist]==2){
                                                    $cont_area3++;
                                                }
                                                if(isset($j) && $j==3 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==3 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==3 && $tmp_seg[$cont_exist]==2){
                                                    $i3++;
                                                }
                                                if(isset($j) && $j==4 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==4 && $tmp_seg[$cont_exist]==2){
                                                    $cont_area4++;
                                                }
                                                if(isset($j) && $j==4 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==4 && $tmp_seg[$cont_exist]==0 || isset($j) && $j==4 && $tmp_seg[$cont_exist]==2){
                                                    $i4++;
                                                }
                                                $cont_exist++;
                                            }
                                        }
                                        if($g->id_limpieza==$id_limpieza_ant){
                                            $cont_activ++;
                                            $val_activ = $val_activ+(round(($cont/$a->tot_act*100),2));
                                            //$val_activ = (round(($cont/$a->tot_act*100),2));
                                        }else{
                                            $cont_activ=1;
                                            $val_activ = (round(($cont/$a->tot_act*100),2));
                                        }
                                        //log_message('error', 'tot_act : '.$a->tot_act);
                                        //log_message('error', 'cont : '.$cont);
                                        $inputs_data="<td class='".$g->id_limpieza."'><input class='id_".$g->id_limpieza."' data-limp='".$g->id_limpieza."' type='hidden' id='".$cont_activ."s_f_activ_".$g->id_limpieza."' value='".round($val_activ/$cont_activ,2)."'></td>";
                                        $efi=round(($cont/$a->tot_act*100),2);
                                        $html.="<td><input id='val_area' type='hidden' value='".round(($cont/$a->tot_act*100),2)."'>".round(($cont/$a->tot_act*100),2)."</td>
                                              ".$inputs_data."</tr>";

                                        $id_limpieza_ant=$g->id_limpieza;
                                        if($i_aux==0){
                                          $html_foot="<tr>
                                                  <td colspan='4'></td>
                                                  <td colspan='2'>% CUMPLIMIENTO</td>
                                                  <td><input type='hidden' id='ft1' value='0'>0</td>
                                                  <td><input type='hidden' id='ft2' value='0'>0</td>
                                                  <td><input type='hidden' id='ft3' value='0'>0</td>
                                                  <td><input type='hidden' id='ft4' value='0'>0</td>";
                                                if($band_dia==5){
                                                  $html_foot.="<td><input type='hidden' id='ft5' value='0'>0</td>";
                                                }
                                              $html_foot.="</tr>";
                                        }
                                        else{
                                          //log_message('error', 'cont_area : '.$cont_area);
                                          //log_message('error', 'i : '.$i);
                                          $ft1=0; $ft2=0; $ft3=0; $ft4=0; $ft5=0;
                                          if($i>0){
                                            $ft1=round(($cont_area/$i*100),2);
                                            ${'ft1_'.$g->id_limpieza}=$ft1;
                                          }
                                          if($i1>0){
                                            $ft2=round(($cont_area1/$i1*100),2);
                                            ${'ft2_'.$g->id_limpieza}=$ft2;
                                          }
                                          if($i2>0){
                                            $ft3=round(($cont_area2/$i2*100),2);
                                            ${'ft3_'.$g->id_limpieza}=$ft3;
                                          }
                                          if($i3>0){
                                            $ft4=round(($cont_area3/$i3*100),2);
                                            ${'ft4_'.$g->id_limpieza}=$ft4;
                                          }
                                          if($i4>0){
                                            $ft5=round(($cont_area4/$i4*100),2);
                                            ${'ft5_'.$g->id_limpieza}=$ft5;
                                          }
                                          
                                          $html_foot="<tr>
                                                  <td colspan='4'></td>
                                                  <td colspan='2'>% CUMPLIMIENTO</td>
                                                  <td><input type='hidden' id='ft1' value='".$ft1."'>".$ft1."</td>
                                                  <td><input type='hidden' id='ft2' value='".$ft2."'>".$ft2."</td>
                                                  <td><input type='hidden' id='ft3' value='".$ft3."'>".$ft3."</td>
                                                  <td><input type='hidden' id='ft4' value='".$ft4."'>".$ft4."</td>";
                                              if($band_dia==5){
                                                $html_foot.="<td><input type='hidden' id='ft5' value='".$ft5."'>".$ft5."</td>";
                                              }
                                          $html_foot.="<td></td>
                                          </tr>";
                                        }                           
                                    }
                                    echo '<div class="col-md-12"><br><hr></div>
                                        <a title="Exportar a excel checklist '.$g->nombre.'" id="export_excel1_'.$g->id_limpieza.'" href="'.base_url().'Reportes/exportExcelMonitoreo/'.$id_proy.'/'.$mes.'/'.$anio.'/'.$g->id_limpieza.'" target="_blank" class="btn btn-success">Exportar a Excel <i class="fa fa-file-excel-o"></i></a>
                                        <table style="text-align:center;" class="table" border="1" width="100%" id="getMonitoreoFin_'.$g->id_limpieza.'">
                                          <thead class="table_thead" id="tble_head">
                                            <tr>
                                                <th>#</th>
                                                <th>ZONA</th>
                                                <th>INSPECCIÓN</th>
                                                <th>ACTIVIDAD</th>
                                                <th>COMENTARIOS</th>
                                                <th>FRECUENCIA</th>
                                                '.$html_head.'
                                                <th>ÁREA DE OPORTUNIDAD </th>
                                                <th></th>
                                            </tr>
                                          </thead>
                                          <tbody id="body_table">
                                            '.$html.'
                                          </tbody>
                                          <tfoot id="tfoot">
                                            '.$html_foot.'
                                          </tfoot>
                                        </table>';  
                                        ?>
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="col-md-2 pb-1">
                                                    <label style="font-size:11px">% Cumplimiento</label>
                                                </div>
                                                <canvas id="graph_<?php echo $g->id_limpieza;?>" width="600" height="450"></canvas>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="col-md-2 pb-1">
                                                    <label style="font-size:11px">% Cumplimiento</label>
                                                </div>
                                                <canvas id="graph2_<?php echo $g->id_limpieza;?>" width="600" height="450"></canvas>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function() { 
                                                var chartPluginLineaHorizontal = {
                                                    afterDraw: function(chartobj) {
                                                        if (chartobj.options.lineaHorizontal) {
                                                            var ctx = chartobj.chart.ctx;
                                                            var valorY = chartobj.scales["y-axis-0"].getPixelForValue(chartobj.options.lineaHorizontal);
                                                            ctx.beginPath();
                                                            ctx.moveTo(0, valorY);
                                                            ctx.lineTo(chartobj.chart.width, valorY);
                                                            ctx.strokeStyle = "#00CC00";
                                                            ctx.stroke();
                                                        }
                                                    }
                                                }
                                                Chart.pluginService.register(chartPluginLineaHorizontal);
                                                new Chart(document.getElementById("graph_<?php echo $g->id_limpieza;?>"), {
                                                    type: "bar",
                                                    data: {
                                                        labels: [<?php $i=0; foreach ($act as $a){ $i++; echo "'${'inspeccion'.$g->id_limpieza.'_'.$i}',"; }?>],
                                                        datasets: [
                                                        {
                                                            type: 'line',
                                                            label: "Objetivo",
                                                            borderColor: "#00CC00",
                                                            data: [100],
                                                            align: 'top',
                                                            datalabels:{
                                                                anchor:'end',
                                                                align:'end',
                                                                offset:'0',
                                                                color:"transparent",
                                                                
                                                            }
                                                        },
                                                        {
                                                            type: "bar",
                                                            //label: [<?php echo "'$a->inspeccion',"; ?>],
                                                            label: ["Inspecciones"],
                                                            <?php $i=0; foreach ($act as $a){ $i++; $cont=0; $tmp=explode(",",$a->dias_act); $tot_act=$a->tot_act; $cont_exist=0; $cont_existall=0; $array_exis=array();
                                                                $tmp_seg=explode(", ",$a->seguimiento_act);
                                                                for($j=0; $j<count($array); $j++){
                                                                    $tot_act2=$tot_act-($tot_act+$j);
                                                                    if (in_array($array[$j], $tmp)){
                                                                        if(isset($tmp_seg[$cont_exist])){
                                                                            if($tmp_seg[$cont_exist]==1 || $tmp_seg[$cont_exist]==2){
                                                                                $cont++;
                                                                            }
                                                                            $cont_exist++;
                                                                        }
                                                                        ${'dia_f'.$tmp[$cont_existall]}=$tmp[$cont_existall];
                                                                        array_push($array_exis,$tmp[$cont_existall]);
                                                                        //log_message('error', 'dia_f : '.${'dia_f'.$tmp[$cont_existall]});
                                                                        $cont_existall++;
                                                                    }
                                                                } 
                                                                $efi=round(($cont/$a->tot_act*100),2);
                                                                ${'efi'.$g->id_limpieza."_".$i}=$efi;
                                                                
                                                                if($efi>=90) {
                                                                    $color="'rgba(0, 204, 0, 0.7)',";
                                                                    $borde="'rgba(0,80,0,1)',";
                                                                }
                                                                else if($efi>=80 && $efi<90) {
                                                                    $color="'rgba(255, 255, 0, 0.7)',";
                                                                    $borde="'rgba(255,255,0,1)',"; 
                                                                }
                                                                else if($efi<80) {
                                                                    $color="'rgba(255, 0, 0, 0.7)',";
                                                                    $borde="'rgba(255,0,0,1)',";
                                                                }
                                                                ${'color'.$g->id_limpieza."_".$i}=$color;
                                                                ${'borde'.$g->id_limpieza."_".$i}=$borde;
                                                            } 
                                                            ?>
                                                            backgroundColor: [<?php $i=0; foreach($act as $a){ $i++; echo ${'color'.$g->id_limpieza."_".$i}.'';} ?> ],
                                                            borderColor: [<?php $i=0; foreach ($act as $a){ $i++; echo ${'borde'.$g->id_limpieza."_".$i}.''; } ?> ],
                                                            borderWidth: 2,
                                                            data: [<?php $i=0; foreach ($act as $a){ $i++; echo round(${'efi'.$g->id_limpieza."_".$i},2).',';} ?> ],
                                                            fill: false,
                                                        },
                                                      ]
                                                    },
                                                    options: {
                                                        animation: {
                                                            duration:0
                                                        },
                                                        lineaHorizontal: 100, 
                                                        legend: { 
                                                            display: true,
                                                            /*labels: {
                                                                fontSize: 26,
                                                            }*/
                                                        },
                                                        title: {
                                                            display: true,
                                                            text: "<?php echo mb_strtoupper("Eficiencia de limpieza en ".$a->nombre,'UTF-8'); ?>",
                                                            fontSize: 15,
                                                        },

                                                        scales: {
                                                            xAxes: [{
                                                                barPercentage: 1, //ancho de barra
                                                                categoryPercentage: 0.6,
                                                                ticks: {
                                                                  autoSkip: false,
                                                                  rotation: -90,
                                                                  barPercentage: 0.5,
                                                                  fontSize: 10
                                                                }
                                                            }],
                                                            yAxes: [{
                                                                ticks: {
                                                                    beginAtZero: true,
                                                                    stepSize: 10,
                                                                    suggestedMax: 100,
                                                                }
                                                            }]
                                                        },
                                                        plugins: {
                                                            datalabels: {
                                                                formatter: (value) => {
                                                                    return value;
                                                                },
                                                                anchor: "top",
                                                                color: "white",
                                                                font: {
                                                                  family: '"Times New Roman"',
                                                                  size: "12",
                                                                  weight: "bold",
                                                                },
                                                            },
                                                        },
                                                    }
                                                });
                                                setTimeout(function(){
                                                    //console.log("save weekend");
                                                    saveGraph("graph_<?php echo $g->id_limpieza;?>",1,<?php echo $g->id_limpieza;?>); //1=por inspeccion de fin de semana
                                                }, 1000); 
                                                
                                                new Chart(document.getElementById("graph2_<?php echo $g->id_limpieza;?>"), {
                                                    type: "bar",
                                                    data: {
                                                        labels: [<?php
                                                                for($j=0; $j<count($array_exis); $j++){
                                                                    echo "'${'dia_f'.$array_exis[$j]}',";
                                                                }
                                                                /*foreach($daterange as $date){
                                                                    if(date('l', strtotime($date->format("d-m-Y"))) == 'Sunday'){
                                                                        echo "'${'dia_f'.$date->format("d")}',";
                                                                    }
                                                                }*/ ?> ],
                                                        datasets: [
                                                        {
                                                            type: 'line',
                                                            label: "Objetivo",
                                                            borderColor: "#00CC00",
                                                            data: [100],
                                                            align: 'top',
                                                            datalabels:{
                                                                anchor:'end',
                                                                align:'end',
                                                                offset:'0',
                                                                color:"trasnparent"
                                                            }
                                                        },
                                                        {
                                                            type: "bar",
                                                            //label: [<?php for($j=0; $j<count($array); $j++){ echo "'$array[$j]',"; }?>],
                                                            label: ["Dias"],
                                                            <?php $i=0; foreach ($act as $a){
                                                                for($j=0; $j<count($array); $j++){
                                                                    $i++;
                                                                    if(isset(${'ft'.$i.'_'.$g->id_limpieza})){
                                                                        $val_dia=round(${'ft'.$i.'_'.$g->id_limpieza},2);
                                                                    }
                                                                    if($val_dia>=90) {
                                                                        $color="'rgba(0, 204, 0, 0.7)',";
                                                                        $borde="'rgba(0,80,0,1)',";
                                                                    }
                                                                    else if($val_dia>=80 && $val_dia<90) {
                                                                        $color="'rgba(255, 255, 0, 0.7)',";
                                                                        $borde="'rgba(255,255,0,1)',"; 
                                                                    }
                                                                    else if($val_dia<80) {
                                                                        $color="'rgba(255, 0, 0, 0.7)',";
                                                                        $borde="'rgba(255,0,0,1)',";
                                                                    }
                                                                    ${'color_'.$i.'_'.$a->id_limpieza."_".$i}=$color;
                                                                    ${'borde_'.$i.'_'.$a->id_limpieza."_".$i}=$borde;
                                                                }
                                                            } 
                                                            ?>
                                                            backgroundColor: [<?php $i=0; foreach($act as $a){ 
                                                                for($j=0; $j<count($array); $j++){
                                                                    $i++;
                                                                    echo ${'color_'.$i.'_'.$a->id_limpieza."_".$i}.'';
                                                                }
                                                            } ?> ],
                                                            borderColor: [<?php $i=0; foreach($act as $a){ 
                                                                for($j=0; $j<count($array); $j++){
                                                                    $i++;
                                                                    echo ${'borde_'.$i.'_'.$a->id_limpieza."_".$i}.'';
                                                                }
                                                            } ?> ],
                                                            borderWidth: 2,
                                                            data: [<?php $i=0; foreach($act as $a){
                                                                for($j=0; $j<count($array); $j++){
                                                                    $i++;
                                                                    if(isset(${'ft'.$i.'_'.$g->id_limpieza})){
                                                                        echo round(${'ft'.$i.'_'.$g->id_limpieza},2).',';
                                                                    }  
                                                                }
                                                            } ?> ],
                                                            fill: false,
                                                        },
                                                      ]
                                                    },
                                                    options: {
                                                        animation: {
                                                            duration:0
                                                        },
                                                        lineaHorizontal: 100, 
                                                        legend: { 
                                                            display: true 
                                                        },
                                                        title: {
                                                            display: true,
                                                            text: "<?php echo mb_strtoupper("Eficiencia de limpieza en ".$a->nombre,'UTF-8'); ?>",
                                                            fontSize: 15,
                                                        },
                                                        scales: {
                                                            xAxes: [{
                                                                barPercentage: 1, //ancho de barra
                                                                categoryPercentage: 0.6,
                                                                ticks: {
                                                                  autoSkip: false,
                                                                  rotation: -90,
                                                                  barPercentage: 0.5
                                                                }
                                                            }],
                                                            yAxes: [{
                                                                ticks: {
                                                                    beginAtZero: true,
                                                                    stepSize: 10,
                                                                    suggestedMax: 100,
                                                                }
                                                            }]
                                                        },
                                                        plugins: {
                                                            datalabels: {
                                                                formatter: (value) => {
                                                                    return value;
                                                                },
                                                                anchor: "top",
                                                                color: "white",
                                                                font: {
                                                                  family: '"Times New Roman"',
                                                                  size: "16",
                                                                  weight: "bold",
                                                                },
                                                            },
                                                        },
                                                    }
                                                });
                                                
                                                setTimeout(function(){
                                                    saveGraph("graph2_<?php echo $g->id_limpieza;?>",2,<?php echo $g->id_limpieza;?>); //2=por dia de fin de semana
                                                }, 1000);
                                            });
                                        </script>

                                <?php  }                                
                                ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><br><br></div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <table>
                                        <tr>
                                            <th width="20%"><img class="img-fluid2" height="120px" src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                            <th width="40%"><h3>CHECK LIST DIARIO</h3></th>
                                            <th width="20%"><img align="right" class="img-fluid2" height="120px" id="logo_cli" src="<?php echo base_url();?>uploads/clientes/<?php echo $logo; ?>"></th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <table class="table" border="1" width="100%">
                                        <tr>
                                            <th width="30%" style="background-color:#00CC00;"></th>
                                            <th width="70%">Porcentaje optimo 90 a 100%</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FFFF00;"></th>
                                            <th>Porcentaje aceptable del 80 al 89 %</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FF0000;"></th>
                                            <th>79 % o menos NO ACEPTABLE</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <br>
                            <?php $where=""; $html_head="";
                                $params["id_proy"]=$id_proy; $params["mes"]=$mes; $params["anio"]=$anio; $params["tipo"]="0";
                                $act=$this->ModelReportes->get_result_monitoreo($params,0,$where);
                                $i=0; $array=array(); $array_all=array();
                                $anio=0; $fecha_f="";
                                foreach ($act as $a) {
                                  $fecha_i = ''.$a->mes.'/01/'.$a->anio.'';
                                  $fecha_f = $a->ultimo;
                                }
                                if($fecha_f==""){
                                  $fecha_i = ''.$params["mes"].'/01/'.date("Y").'';
                                  $ult=date("t",mktime(0,0,0,$params["mes"],1,date("Y")));
                                  $fecha_f = ''.$params["mes"].'/'.$ult.'/'.date("Y").'';
                                }
                                $begin = new DateTime($fecha_i);
                                $end = new DateTime($fecha_f);
                                $end = $end->modify( '+1 day' );
                                $interval = new DateInterval('P1D');
                                $daterange = new DatePeriod($begin, $interval ,$end);
                        
                                foreach($daterange as $date){
                                    //array_push($array_all,$date->format("d"));
                                    if(date('l', strtotime($date->format("d-m-Y"))) != 'Sunday'){
                                        $html_head.="<th><input id='num_dia' type='hidden' value='".$date->format("d")."'>".$date->format("d")."</th>";
                                        //${'dias_'.intval($date->format("d"))}=array();
                                        array_push($array,$date->format("d"));
                                        $html_foot.="<td id='ft_".intval($date->format("d"))."'></td>";
                                        ${'dia_f2'.$date->format("d")}=$date->format("d");
                                    }
                                }
                                $cont=0; $cont2=0; $cont3=0; $cont4=0; $cont5=0;
                                $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].')';
                                if(isset($array[4])){
                                  $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].' or DATE_FORMAT(ma.fecha, "%d")='.$array[4].')';
                                }
                                $cont_area=0; $cont_area1=0; $cont_area2=0; $cont_area3=0; $cont_area4=0; ${'cont_1'}=0; $id_limpieza_ant=0; $cont_act=0;
                                $get=$this->ModelReportes->getTotalActiv($params);
                                foreach ($get as $g) {
                                    foreach($daterange as $date){
                                        if(date('l', strtotime($date->format("d-m-Y"))) != 'Sunday'){
                                            ${'dias_'.intval($date->format("d")).'_'.$g->id_limpieza}=array();
                                        }
                                    }
                                    $html="";
                                    $id_act_ant=0;
                                    $cont_act++;
                                    $cont_area=0; $cont_area1=0; $cont_area2=0; $cont_area3=0; $cont_area4=0; ${'cont_1'}=0; ${'cont_31'}=0;  $tmp2=[]; $i_aux=0; $i=0; $i1=0; $i2=0; $i3=0; $i4=0;
                                    $val_activ=0; $cont_activ=0; $efi=0;
                                    $dia_ant=0; $inputs_data=""; $array_act=array(); 
                                    $array_existd=array(); $cont_existalld=0; ${'cont_exist_0'}=0; ${'cont_exist_1'}=0; ${'cont_exist_31'}=0;
                                    $item=0; $html_foot2=""; $r=0; $cont_item=0; $html_foot2x=''; $dias_diario=array();
                                    $act=$this->ModelReportes->get_result_monitoreoNvo($params,0,$where,$g->id_limpieza);
                                    foreach ($act as $a) {
                                        $cont=0;
                                        $fecha_camb = strtotime($a->fecha);
                                        $semana = date('W', $fecha_camb);
                                        $i_aux++;
                                        $cont_exist=0;
                                        $anio=$a->anio;
                                        $tmp=explode(",",$a->dias_act);
                                        //sort($tmp);
                                        //log_message('error', 'tmp : '.var_dump($tmp));
                                        $tot_act=$a->tot_act;
                                        $tmp_seg=explode(", ",$a->seguimiento_act);
                                        //sort($tmp_seg);
                                        //log_message('error', 'tmp_seg : '.var_dump($tmp_seg));
                                        $item++;
                                        ${'inspecciond'.$g->id_limpieza.'_'.$item}=$a->inspeccion;
                                        //log_message('error', 'inspecciond: '.${'inspecciond'.$g->id_limpieza.'_'.$item});   
                                        ${'objetivod'.$g->id_limpieza.'_'.$item}=100;
                                        $html.="<tr>
                                                  <td>".$item."</td>
                                                  <td>".$a->zona."</td>
                                                  <td>".$a->inspeccion."</td>
                                                  <td>".$a->nombre."</td>
                                                  <td>".$a->comentarios_br."</td>
                                                  <td>".$a->frecuencia."</td>";
                                        for($j=0; $j<count($array); $j++){
                                            //${'dias_'.intval($array[$j]).'_'.$g->id_limpieza}[]=0;
                                            if (in_array($array[$j], $tmp)){
                                                if(isset($tmp_seg[$cont_exist])){
                                                    ${'dias_'.intval($array[$j]).'_'.$g->id_limpieza}[]=1;
                                                    if($tmp_seg[$cont_exist]=="1" || $tmp_seg[$cont_exist]=="2"){
                                                        $cont++;
                                                        ${'cont_'.intval($tmp[$cont_exist]).'_'.$g->id_limpieza}++;
                                                        ${'cont_asigna'.intval($tmp[$cont_exist]).'_'.$g->id_limpieza}++;
                                                    }else if($tmp_seg[$cont_exist]=="0"){
                                                        //${'cont_asigna'.intval($tmp[$cont_exist]).'_'.$g->id_limpieza}=0;
                                                    }
                                                    ${'cont_exist_'.intval($tmp[$cont_exist]).'_'.$g->id_limpieza}++;
                                                    ${'val_exist_'.intval($tmp[$cont_exist])}=intval($tmp[$cont_exist]);
                                                    //${'dia_f2_exist'.intval($array[$j])}=intval($array[$j]);
                                                    $dias_diario[]=intval($tmp[$cont_exist]);
                                                    array_push($array_existd,intval($tmp[$cont_exist]));
                                                    ${'dias_exist'.intval($tmp[$cont_exist])}[]=1;
                                                    //log_message('error', 'array_existd: '.var_dump($array_existd));                                      
                                                    /*log_message('error', 'tmp[cont_exist] : '.intval($tmp[$cont_exist]));
                                                    log_message('error', 'cont_exist_'.intval($tmp[$cont_exist]).'_'.$g->id_limpieza.' : '.${'cont_exist_'.intval($tmp[$cont_exist]).'_'.$g->id_limpieza});
                                                    log_message('error', 'cont_asigna'.intval($array[$j]).'_'.$g->id_limpieza.' : '.${'cont_asigna'.intval($array[$j]).'_'.$g->id_limpieza});*/
                                                    //log_message('error', 'dias_exist : '.var_dump(${'dias_exist'.intval($array[$j])}));
                                                    //log_message('error', 'val_exist_ : '.${'val_exist_'.intval($array[$j])});
                                                    //log_message('error', 'dia_f2_exist : '.${'dia_f2_exist'.intval($array[$j])});

                                                    if($tmp_seg[$cont_exist]==1) $sel1="selected"; else $sel1="";
                                                    if($tmp_seg[$cont_exist]==0) $sel0="selected"; else $sel0="";
                                                    if($tmp_seg[$cont_exist]==2) $sel2="selected"; else $sel2="";
                                                    //$html.="<td>".$tmp_seg[$cont_exist]."</td>";
                                                    $html.='<td><input data-dia="'.intval($array[$j]).'" id="val_dia'.intval($array[$j]).'" type="hidden" value="'.$tmp_seg[$cont_exist].'">
                                                        <select style="min-width:35px; font-size:10px" class="form-control" id="estatus" onchange="cambiaEstatus(0,this.value,'.$array[$j].','.$g->id_limpieza.',\''.$a->inspeccion. '\')">
                                                              <option value="" disabled></option>
                                                              <option '.$sel1.' value="1">1</option>
                                                              <option '.$sel0.' value="0">0</option>
                                                              <option '.$sel2.' value="2">2-N/A</option>
                                                            </select></td>';
                                                }else{
                                                    ${'dias_'.intval($array[$j]).'_'.$g->id_limpieza}[]=0;
                                                    
                                                    //${'dias_exist'.intval($array[$j])}[]=0;
                                                    //${'cont_asigna'.intval($array[$j]).'_'.$g->id_limpieza}=0;
                                                    //${'cont_exist_'.intval($array[$cont_exist]).'_'.$g->id_limpieza}=0;
                                                    $html.='<td><div class="col-sm-12">
                                                        <select style="min-width:35px; font-size:10px" class="form-control" id="estatus" onchange="cambiaEstatus(0,this.value,'.$array[$j].','.$g->id_limpieza.',\''.$a->inspeccion. '\')">
                                                          <option value="" disabled ></option>
                                                          <option value="1">1</option>
                                                          <option value="0">0</option>
                                                          <option value="2">2-N/A</option>
                                                        </select>
                                                      </div>
                                                    </td>';
                                                }
                                                $cont_exist++;
                                                $cont_existalld++;
                                            }else{
                                                ${'dias_'.intval($array[$j]).'_'.$g->id_limpieza}[]=0;
                                                //${'dias_exist'.$array[$j]}[]=0;
                                                ${'dias_exist'.intval($array[$j])}[]=0;
                                                
                                                //${'ftf_'.$j.'_'.$g->id_limpieza.'_'.intval($array[$j])}=0;
                                                //${'cont_asigna'.intval($array[$j]).'_'.$g->id_limpieza}=0;
                                                //${'cont_exist_'.intval($array[$j]).'_'.$g->id_limpieza}=0;
                                                $html.='<td><div class="col-sm-12">
                                                        <select style="min-width:35px; font-size:10px" class="form-control" id="estatus" onchange="cambiaEstatus(0,this.value,'.$array[$j].','.$g->id_limpieza.',\''.$a->inspeccion. '\')">
                                                          <option value="" disabled selected></option>
                                                          <option value="1">1</option>
                                                          <option value="0">0</option>
                                                          <option value="2">2-N/A</option>
                                                        </select>
                                                      </div>
                                                    </td>';
                                            }                                            
                                        }
                                        $html_foot2x=""; $val_activ=0; $cont_activ=0; 
                                        for($j2=0; $j2<count($array); $j2++){
                                            //log_message('error', 'id_limpieza : '.$a->id_limpieza);
                                            //log_message('error', 'cont_act : '.$cont_act);
                                            /*if($cont_act>1 && $id_act_ant!=$a->id_limpieza){
                                                //log_message('error', 'actividad anterior distinta : '.$id_act_ant);
                                                ${'cont_'.intval($array[$j2]).'_'.$g->id_limpieza}=0;
                                                ${'dias_'.intval($array[$j2]).'_'.$a->id_limpieza}[]=0;
                                            }*/
                                            //por el numero de filas
                                            if(isset(${'cont_'.intval($array[$j2]).'_'.$g->id_limpieza}) && ${'cont_'.intval($array[$j2]).'_'.$g->id_limpieza}>0){ //son dias del mes
                                                $porcentaje=(${'cont_'.intval($array[$j2]).'_'.$g->id_limpieza}/array_sum(${'dias_'.intval($array[$j2]).'_'.$g->id_limpieza}))*100;
                                                //${'ftf_'.$j.'_'.$g->id_limpieza}=$porcentaje;
                                            }else{
                                                $porcentaje=0;
                                                //${'ftf_'.$j.'_'.$g->id_limpieza}=$porcentaje;
                                            }
                                            //log_message('error', 'id_act_ant : '.$id_act_ant);
                                            //log_message('error', 'cont_(array[j2])} : '.${'cont_'.intval($array[$j2]).'_'.$g->id_limpieza});
                                            //log_message('error', 'array_sum dias : '.array_sum(${'dias_'.intval($array[$j2]).'_'.$g->id_limpieza}));
                                            $html_foot2x.='<td>'.round($porcentaje,2).'</td>';
                                        }
                                        for($j=0; $j<count($array_all); $j++){
                                            ${'porc_'.$j.'_'.$g->id_limpieza}=$porcentaje;
                                        }
                                        //para solo los días de actividades programadas
                                        sort($array_existd);
                                        //log_message('error', 'array_existd con sort: '.var_dump($array_existd));
                                        for($j=0; $j<count($array_existd); $j++){
                                            //log_message('error', 'j : '.$j);
                                            /*log_message('error', 'cont_asigna en porcentaje : '.${'cont_asigna'.intval($array_existd[$j]).'_'.$g->id_limpieza});
                                            log_message('error', 'cont_exist_ en porcentaje : '.${'cont_exist_'.intval($array_existd[$j]).'_'.$g->id_limpieza});
                                            log_message('error', 'array_existd [j] <br>: '.$array_existd[$j]);*/
                                            if(isset(${'cont_asigna'.intval($array_existd[$j]).'_'.$g->id_limpieza})){
                                                $porcentaje2=(${'cont_asigna'.intval($array_existd[$j]).'_'.$g->id_limpieza}/${'cont_exist_'.intval($array_existd[$j]).'_'.$g->id_limpieza}*100);
                                                //log_message('error', 'porcentaje2 : '.$porcentaje2);
                                                ${'ftf_'.$j.'_'.$g->id_limpieza.'_'.intval($array_existd[$j])}=$porcentaje2;
                                            }else{
                                                ${'ftf_'.$j.'_'.$g->id_limpieza.'_'.intval($array_existd[$j])}=0;
                                            }
                                            //log_message('error', 'ftf_ : '.${'ftf_'.$j.'_'.$g->id_limpieza.'_'.intval($array_existd[$j])});
                                        }
                                        $html_foot="<tr>
                                        <td colspan='4'></td>
                                          <td colspan='2'>% CUMPLIMIENTO</td>
                                          ".$html_foot2x."
                                          <td></td>
                                        </tr>";

                                        if($a->id_limpieza==$id_limpieza_ant){
                                            $cont_activ++;
                                            $val_activ = $val_activ+(round(($cont/$a->tot_act*100),2));
                                        }else{
                                            $cont_activ=1;
                                            $val_activ = (round(($cont/$a->tot_act*100),2));
                                        }
                                        $inputs_data="<td class='".$g->id_limpieza."'><input class='id_".$g->id_limpieza."' data-limp='".$g->id_limpieza."' type='hidden' id='".$cont_activ."s_f_activ_".$g->id_limpieza."' value='".round($val_activ/$cont_activ,2)."'></td>";
                                
                                        //log_message('error', 'cont : '.$cont);
                                        //log_message('error', 'tot_act : '.$a->tot_act);
                                        $html.="<td><input id='val_area' type='hidden' value='".round(($cont/$a->tot_act*100),2)."'>".round(($cont/$a->tot_act*100),2)."</td>
                                          ".$inputs_data."</tr>";  
                                        $id_limpieza_ant=$g->id_limpieza;
                                        $id_act_ant=$a->id_limpieza;                     
                                    }
                                    echo '<div class="col-md-12"><br><hr></div>
                                        <a title="Exportar a excel checklist '.$g->nombre.'" id="export_excel2_'.$g->id_limpieza.'" href="'.base_url().'Reportes/exportExcelMonitoreoDiario/'.$id_proy.'/'.$mes.'/'.$anio.'/'.$g->id_limpieza.'" target="_blank" class="btn btn-success">Exportar a Excel <i class="fa fa-file-excel-o"></i></a>
                                        <table style="text-align:center; font-size:11px" class="table display" border="1" width="100%" id="getMonitoreoDiario_'.$g->id_limpieza.'">
                                          <thead class="table_thead" id="tble_head">
                                            <tr>
                                                <th>#</th>
                                                <th>ZONA</th>
                                                <th>INSPECCIÓN</th>
                                                <th>ACTIVIDAD</th>
                                                <th>COMENTARIOS</th>
                                                <th>FRECUENCIA</th>
                                                '.$html_head.'
                                                <th>ÁREA DE OPORTUNIDAD </th>
                                                <th></th>
                                            </tr>
                                          </thead>
                                          <tbody id="body_table">
                                            '.$html.'
                                          </tbody>
                                          <tfoot id="tfoot">
                                            '.$html_foot.'
                                          </tfoot>
                                        </table>
                                        '; ?>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="col-md-2 pb-1">
                                                    <label style="font-size:11px">% Cumplimiento</label>
                                                </div>
                                                <canvas id="graph_fin_<?php echo $g->id_limpieza;?>" width="600" height="450"></canvas>
                                            </div>
                                            <!--<div class="col-md-6">
                                                <canvas id="graph2_fin_<?php echo $g->id_limpieza;?>" width="600" height="450"></canvas>
                                            </div>-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"><hr></div>
                                            <div class="col-md-12">
                                                <div class="col-md-2 pb-1">
                                                    <label style="font-size:11px">% Cumplimiento</label>
                                                </div>
                                                <canvas id="graph2_fin_<?php echo $g->id_limpieza;?>" width="1200" height="650"></canvas>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function($) { 
                                                new Chart(document.getElementById("graph_fin_<?php echo $g->id_limpieza;?>"), {
                                                    type: "bar",
                                                    data: {
                                                        labels: [<?php $i=0; foreach ($act as $a){ 
                                                                //log_message('error', 'inspecciond: '.${'inspecciond'.$a->id_limpieza.'_'.$i});   
                                                                $i++; echo "'${'inspecciond'.$a->id_limpieza.'_'.$i}',"; 
                                                            } ?>
                                                        ],
                                                        datasets: [
                                                        {
                                                            type: 'line',
                                                            label: "Objetivo",
                                                            borderColor: "#00CC00",
                                                            data: [100],
                                                            align: 'top',
                                                            datalabels:{
                                                                anchor:'end',
                                                                align:'end',
                                                                offset:'0',
                                                                color:"transparent"
                                                            }
                                                        },
                                                        {
                                                            type: "bar",
                                                            //label: [<?php foreach ($act as $a){ echo "'$a->inspeccion',"; } ?>],
                                                            label: ["Inspecciones"],
                                                            <?php $i=0; foreach ($act as $a){ $i++; $cont=0; $tmp=explode(",",$a->dias_act); $tot_act=$a->tot_act; $cont_exist=0; $cont_existall=0; $array_exisd=array();
                                                                $tmp_seg=explode(", ",$a->seguimiento_act);
                                                                for($j=0; $j<count($array); $j++){
                                                                    $tot_act2=$tot_act-($tot_act+$j);
                                                                    if (in_array($array[$j], $tmp)){
                                                                        if(isset($tmp_seg[$cont_exist])){
                                                                            if($tmp_seg[$cont_exist]==1 || $tmp_seg[$cont_exist]==2){
                                                                                $cont++;
                                                                            }
                                                                            $cont_exist++;
                                                                            ${'dia_f2'.intval($tmp[$cont_existall])}=intval($tmp[$cont_existall]);
                                                                            array_push($array_exisd,intval($tmp[$cont_existall]));
                                                                            //log_message('error', 'dia_f2 : '.${'dia_f2'.intval($tmp[$cont_existall])});
                                                                            $cont_existall++;
                                                                        }
                                                                    }
                                                                } 
                                                                $efi=round(($cont/$a->tot_act*100),2);
                                                                ${'efi'.$g->id_limpieza."_".$i}=$efi;
                                                                
                                                                if($efi>=90) {
                                                                    $color="'rgba(0, 204, 0, 0.7)',";
                                                                    $borde="'rgba(0,80,0,1)',";
                                                                }
                                                                else if($efi>=80 && $efi<90) {
                                                                    $color="'rgba(255, 255, 0, 0.7)',";
                                                                    $borde="'rgba(255,255,0,1)',"; 
                                                                }
                                                                else if($efi<80) {
                                                                    $color="'rgba(255, 0, 0, 0.7)',";
                                                                    $borde="'rgba(255,0,0,1)',";
                                                                }
                                                                ${'color'.$g->id_limpieza."_".$i}=$color;
                                                                ${'borde'.$g->id_limpieza."_".$i}=$borde;
                                                            } 
                                                            ?>
                                                            backgroundColor: [<?php $i=0; foreach($act as $a){ $i++; echo ${'color'.$g->id_limpieza."_".$i}.'';} ?> ],
                                                            borderColor: [<?php $i=0; foreach ($act as $a){ $i++; echo ${'borde'.$g->id_limpieza."_".$i}.''; } ?> ],
                                                            borderWidth: 2,
                                                            data: [<?php $i=0; foreach ($act as $a){ $i++; echo round(${'efi'.$g->id_limpieza."_".$i},2).',';} ?> ],
                                                            fill: false,
                                                        },
                                                      ]
                                                    },
                                                    options: {
                                                        animation: {
                                                            duration:0
                                                        },
                                                        lineaHorizontal: 100, 
                                                        legend: { 
                                                            display: true 
                                                        },
                                                        title: {
                                                            display: true,
                                                            text: "<?php echo mb_strtoupper("Eficiencia de limpieza en ".$a->nombre,'UTF-8'); ?>",
                                                            fontSize: 15,
                                                        },
                                                        scales: {
                                                            xAxes: [{
                                                                barPercentage: 1, //ancho de barra
                                                                categoryPercentage: 0.6,
                                                                ticks: {
                                                                  autoSkip: false,
                                                                  rotation: -90,
                                                                  barPercentage: 0.5
                                                                }
                                                            }],
                                                            yAxes: [{
                                                                ticks: {
                                                                    beginAtZero: true,
                                                                    stepSize: 10,
                                                                    suggestedMax: 100,
                                                                }
                                                            }]
                                                        },
                                                        plugins: {
                                                            datalabels: {
                                                                formatter: (value) => {
                                                                    return value;
                                                                },
                                                                anchor: "top",
                                                                color: "white",
                                                                font: {
                                                                  family: '"Times New Roman"',
                                                                  size: "16",
                                                                  weight: "bold",
                                                                },
                                                            },
                                                        },
                                                    }
                                                });    
                                                setTimeout(function(){
                                                    saveGraph("graph_fin_<?php echo $g->id_limpieza;?>",3,<?php echo $g->id_limpieza;?>); //3=por inspeccion de diario
                                                }, 1500);
                                                new Chart(document.getElementById("graph2_fin_<?php echo $g->id_limpieza;?>"), {
                                                    type: "bar",
                                                    data: {
                                                        labels: [<?php /* foreach($daterange as $date){
                                                                    if(date('l', strtotime($date->format("d-m-Y"))) != 'Sunday'){
                                                                        //echo "'${'dia_f2'.$date->format("d")}',";
                                                                    }
                                                                }*/
                                                                $diaa=0; 
                                                                sort($dias_diario);
                                                                //$dias_diario=array_unique($dias_diario);
                                                                //ORDENAR EL ARREGLO asort() o buscar desde arriba mandar bien las fechas
                                                                for($j=0; $j<count($array_existd); $j++){
                                                                    if($diaa!=intval($array_existd[$j])){
                                                                        echo "'${'val_exist_'.intval($array_existd[$j])}',";
                                                                    }
                                                                    $diaa=intval($array_existd[$j]);
                                                                }
                                                                ?> ],
                                                        datasets: [
                                                        {
                                                            type: 'line',
                                                            label: "Objetivo",
                                                            borderColor: "#00CC00",
                                                            data: [100],
                                                            align: 'top',
                                                            datalabels:{
                                                                anchor:'end',
                                                                align:'end',
                                                                offset:'0',
                                                                color:"transparent"
                                                            }
                                                        },
                                                        {
                                                            type: "bar",
                                                            //label: [<?php for($j=0; $j<count($array); $j++){ echo "'$array[$j]',"; }?>],
                                                            label: ["Dias"],
                                                            <?php $i=0; $vala=0; foreach ($act as $a){
                                                                for($j=0; $j<count($array_existd); $j++){
                                                                    $i++;
                                                                    //if($vala!=intval($array_existd[$j])){
                                                                        if(isset(${'ftf_'.$j.'_'.$g->id_limpieza.'_'.intval($array_existd[$j])})){
                                                                            $val_dia=round(${'ftf_'.$j.'_'.$g->id_limpieza.'_'.intval($array_existd[$j])},2);
                                                                        }
                                                                        if($val_dia>=90) {
                                                                            $color="'rgba(0, 204, 0, 0.7)',";
                                                                            $borde="'rgba(0,80,0,1)',";
                                                                        }
                                                                        else if($val_dia>=80 && $val_dia<90) {
                                                                            $color="'rgba(255, 255, 0, 0.7)',";
                                                                            $borde="'rgba(255,255,0,1)',"; 
                                                                        }
                                                                        else if($val_dia<80) {
                                                                            $color="'rgba(255, 0, 0, 0.7)',";
                                                                            $borde="'rgba(255,0,0,1)',";
                                                                        }
                                                                        ${'color_'.$i.'_'.$a->id_limpieza."_".intval($array_existd[$j])}=$color;
                                                                        ${'borde_'.$i.'_'.$a->id_limpieza."_".intval($array_existd[$j])}=$borde; 
                                                                    //}
                                                                    $vala=intval($array_existd[$j]);
                                                                }
                                                            } 
                                                            ?>
                                                            backgroundColor: [<?php $i=0; foreach($act as $a){ 
                                                                for($j=0; $j<count($array_existd); $j++){
                                                                    $i++;
                                                                    echo ${'color_'.$i.'_'.$a->id_limpieza."_".intval($array_existd[$j])}.'';
                                                                }
                                                            } ?> ],
                                                            borderColor: [<?php $i=0; foreach($act as $a){ 
                                                                for($j=0; $j<count($array_existd); $j++){
                                                                    $i++;
                                                                    echo ${'borde_'.$i.'_'.$a->id_limpieza."_".intval($array_existd[$j])}.'';
                                                                }
                                                            } ?> ],
                                                            borderWidth: 2,
                                                            data: [<?php $i=0; $vala=0; foreach($act as $a){
                                                                for($j=0; $j<count($array_existd); $j++){
                                                                    $i++;
                                                                    if($vala!=intval($array_existd[$j])){
                                                                        echo round(${'ftf_'.$j.'_'.$g->id_limpieza.'_'.intval($array_existd[$j])},2).','; 
                                                                    }
                                                                    $vala=intval($array_existd[$j]);
                                                                    //echo round(${'ftf_'.$j.'_'.$g->id_limpieza.'_'.intval($array_existd[$j])},2).',';  
                                                                }
                                                            } ?> ],
                                                            fill: false,
                                                        },
                                                      ]
                                                    },
                                                    options: {
                                                        animation: {
                                                            duration:0
                                                        },
                                                        lineaHorizontal: 100, 
                                                        legend: { 
                                                            display: true 
                                                        },
                                                        title: {
                                                            display: true,
                                                            text: " <?php echo mb_strtoupper("Eficiencia de limpieza en ".$a->nombre,'UTF-8'); ?>",
                                                            fontSize: 15,
                                                        },

                                                        scales: {
                                                            xAxes: [{
                                                                barPercentage: 1.5, //ancho de barra
                                                                categoryPercentage: 0.6,
                                                                ticks: {
                                                                  autoSkip: false,
                                                                  rotation: -90,
                                                                  barPercentage: 0.5
                                                                }
                                                            }],
                                                            yAxes: [{
                                                                ticks: {
                                                                    beginAtZero: true,
                                                                    //stepSize: 10,
                                                                    suggestedMax: 100,
                                                                }
                                                            }]
                                                        },
                                                        responsive:true,
                                                        //maintainAspectRatio: false,
                                                        plugins: {
                                                            datalabels: {
                                                                formatter: (value) => {
                                                                    return value;
                                                                },
                                                                anchor: "top",
                                                                color: "white",
                                                                font: {
                                                                  family: '"Times New Roman"',
                                                                  size: "16",
                                                                  weight: "bold",
                                                                },
                                                            },
                                                        },
                                                    }
                                                });
                                                
                                                setTimeout(function(){
                                                    saveGraph("graph2_fin_<?php echo $g->id_limpieza;?>",4,<?php echo $g->id_limpieza;?>); //4=por inspeccion de diario
                                                }, 1500);
                                            });
                                        </script>

                                <?php }                                
                                ?>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br><br></div>
                        </div>

                        <div class="row">
                            <div class="col-md-12"><br><br></div>
                        </div>
                        <div class="row">
                            <div class="col-12"></div>
                            <div class="col-12">
                                <center><span><b>TOTAL DE ACTIVIDADES POR MES</b> </span></center>
                                <canvas id="graph_act_mes"></canvas>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    $act=$this->ModelReportes->get_result_monitoreo2($params,1);
    $array=array(); $fecha_f="";
    foreach ($act as $a) {
      $fecha_i = ''.$a->mes.'/01/'.$a->anio.'';
      $fecha_f = $a->ultimo;
    }
    if($fecha_f==""){
        $fecha_i = ''.$params["mes"].'/01/'.date("Y").'';
        $ult=date("t",mktime(0,0,0,$params["mes"],1,date("Y")));
        $fecha_f = ''.$params["mes"].'/'.$ult.'/'.date("Y").'';
    }

    $begin = new DateTime($fecha_i);
    $end = new DateTime($fecha_f);
    $end = $end->modify( '+1 day' );
    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);

    foreach($daterange as $date){
        ${'dias_'.intval($date->format("d"))}=array();
        array_push($array,$date->format("d"));
    }
    ${'cont_1'}=0; $i=0; $id_limpieza_ant=0;
    $act=$this->ModelReportes->get_result_monitoreo2($params,1);
    foreach ($act as $a){
        $i++;
        $cont=0;
        $cont_exist=0;
        $tmp=explode(", ",$a->dias_act);
        $tmp_seg=explode(", ",$a->seguimiento_act);
        //${'activ'.$a->id_limpieza.'_'.$i}=$a->nombre;
        for($j=0; $j<count($array); $j++){
            if(in_array($array[$j], $tmp)){ //son dias del mes
                //$cont_limpieza=$this->ModelReportes->count_limpiezaz($params,$a->id_limpieza,$array[$j]);
                if(isset($tmp_seg[$cont_exist])){
                    if($tmp_seg[$cont_exist]=="1"){
                        ${'dias_'.intval($array[$j])}[]=1;
                    }else if($tmp_seg[$cont_exist]=="0"){
                        ${'dias_'.intval($array[$j])}[]=1;
                    }else if($tmp_seg[$cont_exist]=="2"){ //N/A
                        ${'dias_'.intval($array[$j])}[]=1;
                    }
                    if($tmp_seg[$cont_exist]=="1" || $tmp_seg[$cont_exist]=="2"){
                        $cont++;
                        ${'cont_'.intval($array[$j])}++;
                    }
                }else{
                    ${'dias_'.intval($array[$j])}[]=0;
                }
                $cont_exist++;
            }else{
              ${'dias_'.intval($array[$j])}[]=0;
            }
        }
        for($j=0; $j<count($array); $j++){   
            if(isset(${'cont_'.intval($array[$j])}) && ${'cont_'.intval($array[$j])}>0){ //son dias del mes
                $porcentaje=(${'cont_'.intval($array[$j])}/array_sum(${'dias_'.intval($array[$j])}))*100;
            }else{
                $porcentaje=0;
            }
            ${'porc_'.$j.'_'.$a->id_limpieza}=$porcentaje;
        }
        /*log_message('error', 'cont : '.$cont);
        log_message('error', 'tot_act : '.$a->tot_act);
        log_message('error', 'id_limpieza : '.$a->id_limpieza);
        log_message('error', 'id_limpieza_ant : '.$id_limpieza_ant);*/
        if($a->id_limpieza==$id_limpieza_ant){
          $cont_activ++;
          $val_activ = $val_activ+(round(($cont/$a->tot_act*100),2));
        }else{
          $cont_activ=1;
          $val_activ = (round(($cont/$a->tot_act*100),2));
        }
        $prom1=round($val_activ/$cont_activ,2);
        $prom2=round(($cont/$a->tot_act*100),2);
        
        if($a->id_limpieza==$id_limpieza_ant){
            ${'prom_'.$a->id_limpieza}=${'prom_'.$a->id_limpieza}+$prom2;
            //${'prom_'.$a->id_limpieza}=${'prom_'.$a->id_limpieza}/$cont_activ;
        }else{
            ${'prom_'.$a->id_limpieza}=$prom1;
        }
        ${'cont_activ_'.$a->id_limpieza}=$cont_activ;
        /*log_message('error', 'cont_activ : '.$cont_activ);
        log_message('error', 'actividad : '.$a->nombre);
        log_message('error', 'prom1 : '.$prom1);
        log_message('error', 'prom2 : '.$prom2);
        log_message('error', 'prom_ activ : '.${'prom_'.$a->id_limpieza});*/
        //log_message('error', 'id_limpieza : '.$a->id_limpieza);
        $id_limpieza_ant=$a->id_limpieza;
    }

    $i=0;
    $act=$this->ModelReportes->get_result_monitoreo2($params,0);
    foreach ($act as $a){
        $i++;
        ${'activ'.$a->id_limpieza.'_'.$i}=$a->nombre;
    }

?>
<link rel="stylesheet" href="<?php echo base_url(); ?>plugins/alert/sweetalert.css">
<script src="<?php echo base_url(); ?>plugins/alert/sweetalert.js"></script>
<link href="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
<script src="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.js"></script>
<script type="text/javascript">
    $(document).ready(function($) { 
        new Chart(document.getElementById("graph_act_mes"), {
            type: "bar",
            data: {
                labels: [<?php $i=0; foreach ($act as $a){ $i++; echo "'${'activ'.$a->id_limpieza.'_'.$i}',"; }?>],
                datasets: [
                {
                    type: 'line',
                    label: "Objetivo",
                    borderColor: "#00CC00",
                    data: [100],
                    align: 'top',
                    datalabels:{
                        anchor:'end',
                        align:'end',
                        offset:'0',
                        color:"transparent"
                    }
                },
                {
                    type: "bar",
                    label: ["Actividades realizadas"],
                    <?php $i=0; foreach ($act as $a){
                        //for($j=0; $j<count($array); $j++){
                            $i++;
                            
                            if(${'cont_activ_'.$a->id_limpieza}==1){
                                $val_dia=round(${'prom_'.$a->id_limpieza},2);  
                            }else{
                                $val_dia=round(${'prom_'.$a->id_limpieza}/${'cont_activ_'.$a->id_limpieza},2);
                            }
                            if($val_dia>=90) {
                                $color="'rgba(0, 204, 0, 0.7)',";
                                $borde="'rgba(0,80,0,1)',";
                            }
                            else if($val_dia>=80 && $val_dia<90) {
                                $color="'rgba(255, 255, 0, 0.7)',";
                                $borde="'rgba(255,255,0,1)',"; 
                            }
                            else if($val_dia<80) {
                                $color="'rgba(255, 0, 0, 0.7)',";
                                $borde="'rgba(255,0,0,1)',";
                            }
                            ${'color_'.$a->id_limpieza}=$color;
                            ${'borde_'.$a->id_limpieza}=$borde;
                        //}
                    } 
                    ?>
                    backgroundColor: [<?php $i=0; foreach($act as $a){ 
                        //for($j=0; $j<count($array); $j++){
                            //$i++;
                            echo ${'color_'.$a->id_limpieza}.'';
                        //}
                    } ?> ],
                    borderColor: [<?php $i=0; foreach($act as $a){ 
                        //for($j=0; $j<count($array); $j++){
                            //$i++;
                            echo ${'borde_'.$a->id_limpieza}.'';
                        //}
                    } ?> ],
                    borderWidth: 2,
                    data: [<?php $i=0; foreach($act as $a){
                        if(${'cont_activ_'.$a->id_limpieza}==1){
                            echo round(${'prom_'.$a->id_limpieza},2).',';  
                        }else{
                            echo round(${'prom_'.$a->id_limpieza}/${'cont_activ_'.$a->id_limpieza},2).',';
                        }
                    } ?> ],
                    fill: false,
                },
              ]
            },
            options: {
                animation: {
                    duration:0
                },
                lineaHorizontal: 100, 
                legend: { 
                    display: true 
                },
                title: {
                    display: true,
                    text: "MONITOREO DE ÁREAS CRÍTICAS <?php echo $name_mes; ?>",
                    fontSize: 15,
                },
                scales: {
                    xAxes: [{
                        barPercentage: 1, //ancho de barra
                        categoryPercentage: 0.6,
                        ticks: {
                          autoSkip: false,
                          rotation: -90,
                          barPercentage: 0.5
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            stepSize: 10,
                            suggestedMax: 100,
                        }
                    }]
                },
                plugins: {
                    datalabels: {
                        formatter: (value) => {
                            return value;
                        },
                        anchor: "top",
                        color: "white",
                        font: {
                          family: '"Times New Roman"',
                          size: "22",
                          weight: "bold",
                        },
                    },
                },
            }
        });    

        $("#save_all").on("click",function(){
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: '¡Atención!',
                content: '¿Desea guardar todos los cambios? Se recargara la página y sus resultados',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        swal("Éxito", "Guardado correctamente", "success");
                        setTimeout(function(){
                            location.reload();
                        }, 1500);
                    },
                    cancelar: function () 
                    {
                        
                    }
                }
            });
        });
    });
</script>
