<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/fontawesome.css">

<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatables.css">
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">

<script src="<?php echo base_url(); ?>assets/js/config.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chart/chartjs/chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chart/chartjs/chart.custom.js"></script>

<script src="<?php echo base_url(); ?>assets/js/chart/google/google-chart-loader.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chart/google/google-chart.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tooltip-init.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatable/datatables/datatable.custom.js"></script>


<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.3.2/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>-->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/b-print-1.6.1/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/b-print-1.6.1/datatables.min.js"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js"></script> 
<script src="<?php echo base_url();?>public/js/reportes/resultados_orden.js?v=<?php echo date('YmdGis') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>