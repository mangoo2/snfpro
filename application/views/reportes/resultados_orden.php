<style type="text/css">
    .table_thead{
        background-color:#00B0F0;
        color: #FFFFFF !important;
        text-align: center;
        vertical-align: middle;
    }
    #canvastg2,#canvastg2_2,#canvastg2_3,#canvas_gt_2,#canvas_gs_2,#canvastg3,#canvasgc,#canvasfe{
        display: none;
    }
    .chartWithMarkerOverlay {
       position: relative;
       /*width: auto;*/
    }
    .overlay-marker {

       position: absolute;
       top: 32%;  
       left: 570px; 
       /*top: 60px;
       right: 110px;*/
    }
</style>
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3>Resultados de cumplimiento</h3>
          </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <input type="hidden" id="id_proy" value="<?php echo $id_proy; ?>">
        <input type="hidden" id="mes" value="<?php echo $mes; ?>">
        <input type="hidden" id="anio" value="<?php echo $anio; ?>">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
 
                                </div>
                                <div class="col-md-4">
                                    <table class="table" border="1" width="100%">
                                        <tr>
                                            <th width="30%" style="background-color:#00CC00;"></th>
                                            <th width="70%">Porcentaje optimo 90 a 100%</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FFFF00;"></th>
                                            <th>Porcentaje aceptable del 80 al 89 %</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FF0000;"></th>
                                            <th>79 % o menos NO ACEPTABLE</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <th width="15%"><img class="img-fluid2" height="90px" src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                            <th width="65%"><h3>CUMPLIMIENTO DE ORDENES DE TRABAJO DE LIMPIEZA DIARIO</h3></th>
                                            <th width="20%"><img align="right" class="img-fluid2" height="90px" id="logo_cli" src="<?php echo base_url();?>uploads/clientes/<?php echo $logo_cli; ?>"></th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table" border="1" width="100%" id="result_cumpli">
                                    <thead class="table_thead">
                                        <tr>
                                          <!--<th>id</th>-->
                                          <th>SEMANA</th>
                                          <th>DÍA</th>
                                          <th>OTS</th>
                                          <th>ENCARGADO</th>
                                          <th>TURNO</th>
                                          <th>REALIZADAS</th>
                                          <th>NO REALIZADAS</th>
                                          <th>INCOMPLETAS</th>
                                          <!--<th>CAUSAS</th>-->
                                          <th>CUMPLIMIENTO</th>
                                          <th>% SEMANAL</th>
                                          <th>% TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $html1=""; $html2=""; $html3=""; $semana_ant=0; $supert=0; $supert2=0; $supert3=0;
                                        $cont_realiza=0; $cont_no_realiza=0; $cont_incom=0;
                                        $rows=0; $rows2=0; $rows3=0; $cont=0; $id_ant=0;
                                        $turno_proy=0; $turno_proy2=0; $turno_proy3=0;
                                        $total=0; $total2=0; $total3=0;
                                        $cont_realiza=0; $cont_no_realiza=0; $cont_incom=0;
                                        $cont_realiza2=0; $cont_no_realiza2=0; $cont_incom2=0;
                                        $cont_realiza3=0; $cont_no_realiza3=0; $cont_incom3=0;
                                        $cont_turnos=0; $items=0;
                                        $cont_sem=0; $semana_ant2=0;
                                        foreach ($res as $r) {
                                            $turno_proy=$r->turno_proy;
                                            $turno_proy2=$r->turno_proy2;
                                            $turno_proy3=$r->turno_proy3;

                                            $fecha = strtotime($r->fecha); //y-m-d
                                            $semana = date('W', $fecha);
                                            if(/*$r->turno==1 && $turno_proy=="1" &&*/ $semana_ant2==$semana || $semana_ant2==0){
                                                ${'cont_sem_'.$semana}++;
                                            }else if(/*$r->turno==1 && $turno_proy=="1" &&*/ $semana_ant2!=$semana){
                                                ${'cont_sem_'.$semana}=1;
                                            }
                                            $semana_ant2=$semana;
                                            if($r->turno==1 && $turno_proy=="1"){
                                                //echo "<br>semana: ".$semana;
                                                //echo "<br>cont_sem_: ".${'cont_sem_'.$semana};
                                            }
                                        }
                                        if($turno_proy=="1") $cont_turnos++;
                                        if($turno_proy2=="1") $cont_turnos++;
                                        if($turno_proy3=="1") $cont_turnos++;
                                        //echo "cont_turnos: ".$cont_turnos;
                                        foreach ($res as $r) {
                                            
                                            $turno_proy=$r->turno_proy;
                                            $turno_proy2=$r->turno_proy2;
                                            $turno_proy3=$r->turno_proy3;

                                            $ots=intval($r->days);
                                            /*if($r->turno==1 || $r->turno==2)
                                                $ots=6;
                                            else
                                                $ots=8;*/
                                            if($r->turno==1){
                                                $turno="1ero";
                                            }
                                            else if($r->turno==2){
                                                $turno="2do";
                                            }
                                            else if($r->turno==3){
                                                $turno="3ero";
                                            }
                                            $fecha = strtotime($r->fecha); //y-m-d
                                            $semana = date('W', $fecha);
                                            $rfecha = date("Y-m-d", strtotime($r->fecha));
              
                                            //echo "semana: ".$semana."<br>";
                                            //echo "semana_ant: ".$semana_ant."<br>";
                                            if($r->turno==1 && $turno_proy=="1" && $semana_ant!=$semana){
                                                $cont++;
                                            }
                                            if($r->turno==2 && $turno_proy2=="1" && $semana_ant!=$semana){
                                                $cont++;
                                            }
                                            if($r->turno==3 && $turno_proy3=="1" && $semana_ant!=$semana){
                                                $cont++;
                                            }

                                            if($r->turno==1 && $turno_proy=="1"){
                                                $rows++;
                                                $items++;
                                                //$cumpli=0;
                                                //$semanal=0;
                                                //$total=0;
                                                if($id_ant!=$r->id){
                                                    $count_tot=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,1);
                                                    $count4=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,1);//cancelado
                                                    $count=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,1);
                                                    $count2=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,1);
                                                    $count3=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,1);
                                                    //totales
                                                    $cont_total=$count_tot->tot_estatus;
                                                    //realizada
                                                    $cont_realiza=$count->tot_estatus;
                                                    //incompletas
                                                    $cont_incom=$count2->tot_estatus;
                                                    //no realizadas
                                                    $cont_no_realiza=$count3->tot_estatus;
                                                    //canceladas
                                                    $cont_cancela=$count4->tot_estatus;
                                                }

                                                $cumpli = $cont_realiza*100/$cont_total ;
                                                //echo "<br>cumpli: ".$cumpli;
                                                //$semanal = $semanal + $cumpli;
                                                //$semanal=$cumpli/7; //comentado despues de productivo
                                                //echo "<br>cont_sem_: ".${'cont_sem_'.$semana};

                                                $semanal=$cumpli/${'cont_sem_'.$semana};
                                                //echo "<br>cont_sem: ".$cont_sem;
                                                //$semanal=$cumpli/1;
                                                //$total = $total + $semanal;
                                                //$total=$semanal/3;//comentado despues de productivo
                                                $total=$semanal;
                                                $html1.= "<tr>
                                                    <!--<td>".$r->id."</td>-->
                                                    <td><input type='hidden' data-sem_ini='".$semana."' id='cont' value='".$cont."'>".$semana."</td>
                                                    <td>".saber_dia($rfecha)."</td>
                                                    <td><input readonly class='form-control' type='number' id='ots' value='".$cont_total."'></td>
                                                    <td>SNF PRO</td>
                                                    <td>".$turno."</td>
                                                    <td>".$cont_realiza."</td>
                                                    <td>".($cont_no_realiza+$cont_cancela)."</td>
                                                    <td>".$cont_incom."</td>
                                                    <td><input class='cumpli1 cumpli1_".$cont."' id='cumpli1_".$r->num_semana."' type='hidden' value='".round($cumpli,2)."'>".round($cumpli,2)."</td>
                                                    <!--<td rowspan='".$rows."'>".round($semanal,2)."</td>-->
                                                    <td><input class='semanal1 semanal1_".$cont."' id='semanal1_".$r->num_semana."' type='hidden' value='".round($semanal,2)."'>".round($semanal,2)."</td>";
                                                /*if($rows==1){
                                                    $html1.= "<td rowspan='6'><input class='semanal1' id='semanal1_".$r->num_semana."' type='hidden' value='".$semanal."'>".$semanal."</td>";
                                                }*/
                                                $html1.= "<td>".round($total,2)."</td>
                                                </tr>";
                                                $supert=round($supert+$total,2);
                                            }
                                            if($r->turno==2 && $turno_proy2=="1"){
                                                $rows2++;
                                                $items++;
                                                //$cumpli2=0;
                                                //$semanal2=0;
                                                //$total2=0;
                                                if($id_ant!=$r->id){
                                                    $count_tot2=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,2);
                                                    $count4_2=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,2);//cancelado
                                                    $count_2=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,2);
                                                    $count2_2=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,2);
                                                    $count3_2=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,2);
                                                    //totales
                                                    $cont_total2=$count_tot2->tot_estatus;
                                                    //realizada
                                                    $cont_realiza2=$count_2->tot_estatus;
                                                    //incompletas
                                                    $cont_incom2=$count2_2->tot_estatus;
                                                    //no realizadas
                                                    $cont_no_realiza2=$count3_2->tot_estatus;
                                                    //canceladas
                                                    $cont_cancela2=$count4_2->tot_estatus;
                                                }

                                                $cumpli2 = $cont_realiza2*100/$cont_total2;
                                                //$semanal2 = $semanal2 + $cumpli2;
                                                //$semanal2=$cumpli2/7; //comentado dps de productivo
                                                $semanal2=$cumpli2/${'cont_sem_'.$semana};
                                                //$semanal2=$cumpli2/1;
                                                //$total2 = $total2 + $semanal2;
                                                //$total2=$semanal2/3; //comentado dps de productivo
                                                $total2=$semanal2;
                                                $html2.= "<tr>
                                                    <!--<td>".$r->id."</td>-->
                                                    <td><input data-sem_ini='".$semana."' type='hidden' id='cont' value='".$cont."'>".$semana."</td>
                                                    <td>".saber_dia($rfecha)."</td>
                                                    <td><input readonly class='form-control' type='number' id='ots' value='".$cont_total."'></td>
                                                    <td>SNF PRO</td>
                                                    <td>".$turno."</td>
                                                    <td>".$cont_realiza2."</td>
                                                    <td>".($cont_no_realiza2+$cont_cancela2)."</td>
                                                    <td>".$cont_incom2."</td>
                                                    <!--<td><input class='form-control' type='text' id='causas' value='".$r->causas."'></td>-->
                                                    <td><input class='cumpli2 cumpli2_".$cont."' id='cumpli2_".$r->num_semana."' type='hidden' value='".round($cumpli2,2)."'>".round($cumpli2,2)."</td>
                                                    <td><input class='semanal2 semanal2_".$cont."' id='semanal2_".$r->num_semana."' type='hidden' value='".round($semanal2,2)."'>".round($semanal2,2)."</td>";
                                                /*if($rows2==1){
                                                    $html2.= "<td rowspan='6'><input class='semanal2' id='semanal2_".$r->num_semana."' type='hidden' value='".$semanal2."'>".$semanal2."</td>";
                                                }*/
                                                $html2.= "<td>".round($total2,2)."</td>
                                                </tr>";
                                                $supert2=round($supert2+$total2,2);
                                            }
                                            if($r->turno==3 && $turno_proy3=="1"){
                                                $rows3++;
                                                $items++;
                                                //$cumpli3=0;
                                                //$semanal3=0;
                                                //$total3=0;
                                                //echo "id: ".$r->id."<br>";
                                                //echo "id_ant: ".$id_ant."<br>";
                                                if($id_ant!=$r->id){
                                                    $count_tot3=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,3);
                                                    $count4_3=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,3);//cancelado
                                                    $count_3=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,3);
                                                    $count2_3=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,3);
                                                    $count3_3=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,3);
                                                    //totales
                                                    $cont_total3=$count_tot3->tot_estatus;
                                                    //realizada
                                                    $cont_realiza3=$count_3->tot_estatus;
                                                    //incompletas
                                                    $cont_incom3=$count2_3->tot_estatus;
                                                    //no realizadas
                                                    $cont_no_realiza3=$count3_3->tot_estatus;
                                                    //canceladas
                                                    $cont_cancela3=$count4_3->tot_estatus;
                                                }
                                                //echo "cont_realiza3: ".$cont_realiza3."<br>";
                                                //echo "cont_incom3: ".$cont_incom3."<br>";
                                                //echo "cont_no_realiza3: ".$cont_no_realiza3."<br>";

                                                $cumpli3 = $cont_realiza3*100/$cont_total3;
                                                //$semanal3 = $semanal3 + $cumpli3;
                                                //$semanal3=$cumpli3/7; //comentado dps de productivo
                                                $semanal3=$cumpli3/${'cont_sem_'.$semana};
                                                //$semanal3=$cumpli3/1; 
                                                //$total3 = $total3 + $semanal3;
                                                $total3=$semanal3; //comentado dps de productivo
                                                //$total3=$semanal3/1;
                                                $html3.= "<tr>
                                                    <!--<td>".$r->id."</td>-->
                                                    <td><input data-sem_ini='".$semana."' type='hidden' id='cont' value='".$cont."'>".$semana."</td>
                                                    <td>".saber_dia($r->fecha)."</td>
                                                    <td><input readonly class='form-control' type='number' id='ots' value='".$cont_total."'></td>
                                                    <td>SNF PRO</td>
                                                    <td>".$turno."</td>
                                                    <td>".$cont_realiza3."</td>
                                                    <td>".($cont_no_realiza3+$cont_cancela3)."</td>
                                                    <td>".$cont_incom3."</td>
                                                    <!--<td><input class='form-control' type='text' id='causas' value='".$r->causas."'></td>-->
                                                    <td><input class='cumpli3 cumpli3_".$cont."' id='cumpli3_".$r->num_semana."' type='hidden' value='".round($cumpli3,2)."'>".round($cumpli3,2)."</td>
                                                    <td><input class='semanal3 semanal3_".$cont."' id='semanal3_".$r->num_semana."' type='hidden' value='".round($semanal3,2)."'>".round($semanal3,2)."</td>";
                                                /*if($rows3==1){
                                                   $html3.= "<td rowspan='6'><input class='semanal3' id='semanal3_".$r->num_semana."' type='hidden' value='".$semanal3."'>".$semanal3."</td>";
                                                }*/
                                                $html3.= "<td>".round($total3,2)."</td>
                                                </tr>";
                                                $supert3=round($supert3+$total3,2);
                                            }
                                            //$supert=$supert+$total+$total2+$total3;
                                            $semana_ant=$semana;
                                            $fecha_ant=$r->fecha;
                                            $id_ant=$r->id;
                                            
                                            //echo "fecha_ant: ".$fecha_ant."<br>";
                                        } 
                                        //$supert=$total+$total2+$total3;
                                        echo $html1.$html2.$html3;
                                        //echo "items: ".$items
                                        ?>
                                    </tbody>
                                    <tfoot>
                                      <tr>
                                        <td colspan="10"></td>
                                        <td><input id='super_tot' type='hidden' value="<?php echo round(($supert+$supert2+$supert3)/($cont),2); ?>"><?php echo round(($supert+$supert2+$supert3)/($cont),2); ?></td>
                                      </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4" id="cont_table_turnos">
                            <br>
                            <input type="hidden" id="1ero" value="<?php echo $turno_proy; ?>">
                            <input type="hidden" id="2do" value="<?php echo $turno_proy2; ?>">
                            <input type="hidden" id="3er" value="<?php echo $turno_proy3; ?>">
                            <table style="text-align: center;" class="table" border="1" width="100%" id="table_turnos">
                                <thead class="table_thead">
                                    <tr>
                                        <?php if($turno_proy=="1") echo "<th>1RO</th>"; ?>
                                        <?php if($turno_proy2=="1") echo "<th>2DO</th>"; ?>
                                        <?php if($turno_proy3=="1") echo "<th>3RO</th>"; ?>
                               
                                    </tr>
                                </thead>
                                <tbody id="body_res_sem">

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4" id="cont_table_semanas">
                            <br>
                            <table style="text-align: center;" class="table" border="1" width="100%" id="table_semanas">
                                <thead class="table_thead">
                                    <tr>
                                        <th>SEMANA</th>
                                        <th>CUMPLIMIENTO</th>
                                        <th>OBJETIVO</th>
                                    </tr>
                                </thead>
                                <tbody id="body_total_sem">

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <!--<center><span><b>CUMPLIMIENTO OTS POR TURNO</b> </span></center>
                            <div class="card-body chart-block">
                                <canvas id="graph_turnos"></canvas>
                            </div>-->
                            <div class="col-sm-12 box-col-12" id="cont_graph_turnos_2">
                                <div class="card-body chart-block">
                                    <div class="chart-overflow" id="graph_turnos_2"></div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-1"></div>-->
                        <div class="col-md-6">
                            <!--<center><span><b>CUMPLIMIENTO OTS <?php echo mb_strtoupper($name_mes,"UTF-8"); ?></b> </span></center>
                            <div class="card-body chart-block">
                                <canvas id="graph_semana"></canvas>
                            </div>-->
                            <div class="col-sm-12 box-col-12" id="cont_graph_semana_2">
                                <div class="card-body chart-block">
                                    <div class="chart-overflow" id="graph_semana_2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"><br><br></div>
                    </div>
                    <input type="hidden" id="mes_sel" value="<?php echo mb_strtoupper($name_mes,"UTF-8"); ?>">
                    <div class="row">
                        <div class="col-md-7" id="cont_table_anual">
                            <table style="text-align: center;" class="table" border="1" width="100%" id="table_anual">
                                <?php 
                                $get_proy = $this->ModeloGeneral->getselectwhere2('proyectos',array('id'=>$id_proy));
                                $get_proy=$get_proy->row();
                                $datetime1 = date_create($get_proy->fecha_ini);
                                $datetime2 = date_create($get_proy->fecha_fin);
                                $contador = date_diff($datetime1, $datetime2);
                                $dif_meses=$contador->format("%m");
                                $dif_anios=$contador->format("%y");
                                //log_message('error', 'dif_meses: '.$dif_meses);
                                //log_message('error', 'dif_anios: '.$dif_anios);
                                $params['id_proy']=$id_proy; $html_ah=""; $html_anual=""; $superta=0; $superta2=0; $superta3=0;
                                $total=0; $total2=0; $total3=0; $cont_total=0; /*$cont_turnos=0;*/
                                $cumpli=0; $cumpli2=0; $cumpli3=0;
                                $cont_realiza=0; $cont_no_realiza=0; $cont_incom=0;
                                $cont_realiza2=0; $cont_no_realiza2=0; $cont_incom2=0;
                                $cont_realiza3=0; $cont_no_realiza3=0; $cont_incom3=0; $cc_aux=0;
                                $mes_ant=0; $cont=1; $array_sem=array();
                                if($dif_anios==0){
                                    $html_ah.='<thead class="table_thead">';
                                    for($c=0; $c<=$dif_meses; $c++){
                                        $total=0; $total2=0; $total3=0; $con_turn=0; $count_turno1=0; $count_turno2=0; $count_turno3=0;
                                        $superta=0; $superta2=0; $superta3=0; $cont=1; $items=0;
                                        $super_sem=0;
                                        //echo "<br>c: ".$c;
                                        $params['mes']=date('m', strtotime($get_proy->fecha_ini.'+ '.$c.' month'));
                                        $params['anio']=date('Y', strtotime($get_proy->fecha_ini));
                                        //echo "<br>params mes: ".$params['mes'];
                                        //echo "<br>params anio: ".$params['anio'];
                                        $result=$this->ModelReportes->get_data_ordenes($params,0);
                                        $cont_sem=0; $semana_ant2=0;
                                        $array_sum=array();
                                        foreach ($result as $r) {
                                            
                                            $fecha = strtotime($r->fecha); //y-m-d
                                            $semana = date('W', $fecha);
                                            $mes = $params['mes'];
                                            ${'cumpli_'.$semana}=array();

                                            //echo "mes: ".$mes;
                                            //echo "<br>semana_ant2 for 1: ".$semana_ant2;
                                            //echo "<br>semana for 1: ".$semana;
                                            if(/*$r->turno==1 && $turno_proy=="1" &&*/ $semana_ant2==$semana || $semana_ant2==0){
                                                ${'cont_sem_'.$semana."_".$mes}++;
                                                ${'cont_mes_'.$mes}++;
                                                ${'cont_sem2_'.$semana}++;
                                            }else if(/*$r->turno==1 && $turno_proy=="1" &&*/ $semana_ant2!=$semana){
                                                ${'cont_sem_'.$semana."_".$mes}=1;
                                                ${'cont_mes_'.$mes}=1;
                                                ${'cont_sem2_'.$semana}=1;
                                            }
                                            //echo "<br>cont_sem_ for 1: ".${'cont_sem_'.$semana."_".$mes};
                                            $semana_ant2=$semana;
                                            //array_push($array_sem, $semana);
                                        }
                                        foreach ($result as $r) {
                                            
                                            $fecha = strtotime($r->fecha); //y-m-d
                                            $semana = date('W', $fecha);
                                            $mes = date('m', $fecha);
                                            $rfecha = date("Y-m-d", strtotime($r->fecha));
                                            //echo "semana: ".$semana."<br>";
                                            //echo "semana_ant: ".$semana_ant."<br>";
                                            //var_dump($array_sem);
                                            //array_unique($array_sem);
                                            //var_dump($array_sem);

                                            if($r->turno==1 && $turno_proy=="1" && $semana_ant!=$semana){
                                                $cont++;
                                            }
                                            if($r->turno==2 && $turno_proy2=="1" && $semana_ant!=$semana){
                                                $cont++;
                                            }
                                            if($r->turno==3 && $turno_proy3=="1" && $semana_ant!=$semana){
                                                $cont++;
                                            }
                                            //echo "<br> mes_ant :".$mes_ant;
                                            //echo "<br>fecha :".$r->fecha;
                                            if($mes_ant!=$r->fecha){
                                                
                                                //echo "num_sems: "+$r->num_sems;
                                                //echo "<br>entro a res con mes_ant :".$r->fecha;
                                                if($r->turno==1 && $turno_proy=="1"){
                                                    if($id_ant!=$r->id){
                                                        $items++;
                                                        $count_tot=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,1);
                                                        $count_turno1=$this->ModelReportes->count_turnos($r->fecha,$id_proy,1);
                                                        $count4=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,1);//cancelado
                                                        $count=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,1); //ok
                                                        $count2=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,1);
                                                        $count3=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,1);
                                                        //totales turno
                                                        $count_turno1=$count_turno1->tot_turnos;
                                                        //totales
                                                        $cont_total=$count_tot->tot_estatus;
                                                        //realizada
                                                        $cont_realiza=$count->tot_estatus;
                                                        //incompletas
                                                        $cont_incom=$count2->tot_estatus;
                                                        //no realizadas
                                                        $cont_no_realiza=$count3->tot_estatus;
                                                        //canceladas
                                                        $cont_cancela=$count4->tot_estatus;
                                                    }
                                                    $cumpli = $cont_realiza*100/$cont_total;
                                                    //$semanal=$cumpli/7;//comentado despues de productivo
                                                    $semanal=$cumpli/${'cont_sem_'.$semana."_".$mes}; //comentado despues de productivo
                                                    $super_sem=$super_sem+$cumpli;
                                                    $total=$semanal;
                                                    array_push($array_sum, $cumpli);
                                                    /*${'cumpli_'.$semana[$r]}[]=$cumpli;
                                                    array_sum(${'cumpli_'.$semana[$r]});*/
                                                    if($mes==11){
                                                        /*echo "<br>cumpli: ".$cumpli;
                                                        echo "<br>cont_sem_: ".${'cont_sem_'.$semana."_".$mes};
                                                        echo "<br>semanal: ".$semanal;      
                                                        //echo "<br>cumpli_: ".var_dump(${'cumpli_'.$semana[$r]});
                                                        echo "<br>cumpli_: ".var_dump($array_sum); */
                                                    }
                                                    $superta=round($superta+$total,2);
                                                    /*echo "<br><br>cont_realiza: ".$cont_realiza;
                                                    echo "<br>cont_total: ".$cont_total;
                                                    echo "<br>cumpli: ".$cumpli;
                                                    echo "<br>cont_sem_: ".${'cont_sem_'.$semana."_".$mes};
                                                    echo "<br>semanal: ".$semanal;*/
                                                }
                                                if($r->turno==2 && $turno_proy2=="1"){
                                                    if($id_ant!=$r->id){
                                                        $items++;
                                                        $count_tot2=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,2);
                                                        $count_turno2=$this->ModelReportes->count_turnos($r->fecha,$id_proy,2);
                                                        $count4_2=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,2);//cancelado
                                                        $count_2=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,2);
                                                        $count2_2=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,2);
                                                        $count3_2=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,2);
                                                        //totales turno
                                                        $count_turno2=$count_turno2->tot_turnos;
                                                        //totales
                                                        $cont_total2=$count_tot2->tot_estatus;
                                                        //realizada
                                                        $cont_realiza2=$count_2->tot_estatus;
                                                        //incompletas
                                                        $cont_incom2=$count2_2->tot_estatus;
                                                        //no realizadas
                                                        $cont_no_realiza2=$count3_2->tot_estatus;
                                                        //canceladas
                                                        $cont_cancela2=$count4_2->tot_estatus;
                                                    }
                                                    $cumpli2 = $cont_realiza2*100/$cont_total2;
                                                    //$semanal2=$cumpli2/7; //comentado dps de productivo
                                                    $semanal2=$cumpli2/${'cont_sem_'.$semana."_".$mes}; //comentado despues de productivo
                                                    $total2=$semanal2;
                                                    array_push($array_sum, $cumpli2);
                                                    //$superta2=round($superta2+$total2,2);
                                                }
                                                if($r->turno==3 && $turno_proy3=="1"){
                                                    if($id_ant!=$r->id){
                                                        $items++;
                                                        $count_tot3=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,3);
                                                        $count_turno3=$this->ModelReportes->count_turnos($r->fecha,$id_proy,3);
                                                        $count4_3=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,3);//cancelado
                                                        $count_3=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,3);
                                                        $count2_3=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,3);
                                                        $count3_3=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,3);
                                                        //totales turno
                                                        $count_turno3=$count_turno3->tot_turnos;
                                                        //totales
                                                        $cont_total3=$count_tot3->tot_estatus;
                                                        //realizada
                                                        $cont_realiza3=$count_3->tot_estatus;
                                                        //incompletas
                                                        $cont_incom3=$count2_3->tot_estatus;
                                                        //no realizadas
                                                        $cont_no_realiza3=$count3_3->tot_estatus;
                                                        //canceladas
                                                        $cont_cancela3=$count4_3->tot_estatus;
                                                    }
                                                    $cumpli3 = $cont_realiza3*100/$cont_total3;
                                                    //$semanal3=$cumpli3/7; //comentado dps de productivo
                                                    $semanal3=$cumpli3/${'cont_sem_'.$semana."_".$mes}; //comentado despues de productivo
                                                    $total3=$semanal3; //comentado dps de productivo
                                                    //$superta3=round($superta3+$total3,2);
                                                    array_push($array_sum, $cumpli3);
                                                }
                                                $semana_ant=$semana;
                                                $fecha_ant=$r->fecha;
                                                $id_ant=$r->id;
                                            }
                                            $superta=round($superta+$total,2);
                                            $superta2=round($superta2+$total2,2);
                                            $superta3=round($superta3+$total3,2);
                                            $mes_ant=$r->fecha;
                                        }
                                        $supersup=0;
                                        if($count_turno1>0){
                                            $count_turno1=1;
                                        }if($count_turno2>0){
                                            $count_turno2=1;
                                        }if($count_turno3>0){
                                            $count_turno3=1;
                                        }
                                        if($cont>0){
                                            //$supersup=round(($superta+$superta2+$superta3)/($cont),2); 
                                            $cont_turn_tot=$count_turno1+$count_turno2+$count_turno3;
                                            if($cont_turn_tot>0){
                                                $supersup=round(($superta+$superta2+$superta3)/$cont_turn_tot,2); 
                                            }else{
                                                $supersup=round(($superta+$superta2+$superta3),2); 
                                            }
                                            $supersup=round(array_sum($array_sum)/count($array_sum),2);
                                            /*echo "<br>cont_turn_tot: ".$cont_turn_tot."<br>";
                                            echo "cont: ".$cont."<br>";
                                            echo "super_sem: ".$super_sem."<br>";
                                            echo "tot array_sum: ".array_sum($array_sum)."<br>";
                                            echo "superta: ".$superta."<br>";
                                            echo "superta2: ".$superta2."<br>";
                                            echo "superta3: ".$superta3."<br>";*/
                                        }
                                        /*echo "cont: ".$cont."<br>";
                                        echo "superta: ".$superta."<br>";
                                        echo "superta2: ".$superta2."<br>";
                                        echo "superta3: ".$superta3."<br>";*/

                                        $html_ah.='<th>'.nameMes($params['mes']).'</th>';
                                        $html_anual.='<td><input type="hidden" id="total_anual" value="'.$supersup.'">
                                                      <input type="hidden" id="mes_anual" value="'.nameMes($params['mes']).'"> '.$supersup.'%</td>';
                                        
                                    }
                                    echo "<tr>".$html_ah."</tr></thead><tbody><tr>".$html_anual."</tr></tbody>";
                                }else{
                                    $nva_dif=$dif_anios*12;
                                    $c2=0; $c_aux=0; $html_ah.='<thead class="table_thead">';
                                    for($c=0; $c<=$nva_dif; $c++){
                                        $items=0;
                                        //echo "<br>c: ".$c;
                                        $c_aux++;
                                        /*if($c%12==0 && $c>=12){
                                            $c2++;
                                            $c_aux=0;
                                        }*/
                                        $total=0; $total2=0; $total3=0;
                                        $superta=0; $superta2=0; $superta3=0; $cont=1;
                                        $count_turno1=0; $count_turno2=0; $count_turno3=0;
                                        //echo "<br>c_aux: ".$c_aux;
                                        $mes_mod = date('m', strtotime($get_proy->fecha_ini.'+ '.$c.' month'));
                                        $anio_mod = date('Y', strtotime($get_proy->fecha_ini.'+ '.$c2.' year'));
                                        $date_comp = date('Y-m', strtotime($anio_mod.'-'.$mes_mod));
                                        //echo "<br>date_comp: ".$date_comp;
                                        //echo "<br>date: ".date("Y-m");
                                        //echo "<br>c: ".$c;
                                        if($mes_mod%12==0){
                                            //echo "<br>mes_mod % 12: ".$mes_mod;
                                            $c2++;
                                        }
                                        $array_sum=array();
                                        if($date_comp <= date("Y-m")){
                                            $params['mes']=date('m', strtotime($get_proy->fecha_ini.'+ '.$c.' month'));
                                            $params['anio']=date('Y', strtotime($get_proy->fecha_ini.'+ '.$c2.' year'));
                                            $result=$this->ModelReportes->get_data_ordenes($params,0);
                                            $semana_ant2=0;
                                            foreach ($result as $r) {
                                                $fecha = strtotime($r->fecha); //y-m-d
                                                $semana = date('W', $fecha);
                                                $mes = $params['mes'];
                                                //echo "mes: ".$mes;
                                                //echo "<br>semana_ant2 for 1: ".$semana_ant2;
                                                //echo "<br>semana for 1: ".$semana;
                                                if(/*$r->turno==1 && $turno_proy=="1" &&*/ $semana_ant2==$semana || $semana_ant2==0){
                                                    ${'cont_sem_'.$semana."_".$mes}++;
                                                }else if(/*$r->turno==1 && $turno_proy=="1" &&*/ $semana_ant2!=$semana){
                                                    ${'cont_sem_'.$semana."_".$mes}=1;
                                                }
                                                //echo "<br>cont_sem_ for 1: ".${'cont_sem_'.$semana."_".$mes};
                                                $semana_ant2=$semana;
                                            }
                                            foreach ($result as $r) {
                                                $fecha = strtotime($r->fecha); //y-m-d
                                                $semana = date('W', $fecha);
                                                $rfecha = date("Y-m-d", strtotime($r->fecha));
                                                if($r->turno==1 && $turno_proy=="1" && $semana_ant!=$semana){
                                                    $cont++;
                                                }
                                                if($r->turno==2 && $turno_proy2=="1" && $semana_ant!=$semana){
                                                    $cont++;
                                                }
                                                if($r->turno==3 && $turno_proy3=="1" && $semana_ant!=$semana){
                                                    $cont++;
                                                }
                                                //echo "<br>semana_ant: ".$semana_ant;
                                                //echo "<br>semana: ".$semana;
                                                //echo "<br>cont_sem_: ".${'cont_sem_'.$semana."_".$mes};
                                                if($mes_ant!=$r->fecha){
                                                    if($r->turno==1 && $turno_proy=="1"){
                                                        if($id_ant!=$r->id){
                                                            $items++;
                                                            $count_tot=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,1);
                                                            $count_turno1=$this->ModelReportes->count_turnos($r->fecha,$id_proy,1);
                                                            $count4=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,1);//cancelado
                                                            $count=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,1);
                                                            $count2=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,1);
                                                            $count3=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,1);
                                                            //totales turno
                                                            $count_turno1=$count_turno1->tot_turnos;
                                                            //totales
                                                            $cont_total=$count_tot->tot_estatus;
                                                            //realizada
                                                            $cont_realiza=$count->tot_estatus;
                                                            //incompletas
                                                            $cont_incom=$count2->tot_estatus;
                                                            //no realizadas
                                                            $cont_no_realiza=$count3->tot_estatus;
                                                            //canceladas
                                                            $cont_cancela=$count4->tot_estatus;
                                                        }
                                                        $cumpli = $cont_realiza*100/$cont_total ;
                                                        //$semanal=$cumpli/7;//comentado despues de productivo
                                                        $semanal=$cumpli/${'cont_sem_'.$semana."_".$mes}; 
                                                        $total=$semanal;
                                                        array_push($array_sum, $cumpli);
                                                        //$superta=round($superta+$total,3);
                                                        /*echo "<br><br>semana: ".$semana;
                                                        echo "<br><br>cont_realiza: ".$cont_realiza;
                                                        echo "<br>cont_total: ".$cont_total;
                                                        echo "<br>cumpli: ".$cumpli;
                                                        echo "<br>cont_sem_: ".${'cont_sem_'.$semana."_".$mes};
                                                        echo "<br>semanal: ".$semanal;*/
                                                    }
                                                    if($r->turno==2 && $turno_proy2=="1"){
                                                        if($id_ant!=$r->id){
                                                            $items++;
                                                            $count_tot2=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,2);
                                                            $count_turno2=$this->ModelReportes->count_turnos($r->fecha,$id_proy,2);
                                                            $count4_2=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,2);//cancelado
                                                            $count_2=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,2);
                                                            $count2_2=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,2);
                                                            $count3_2=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,2);
                                                            //totales turno
                                                            $count_turno2=$count_turno2->tot_turnos;
                                                            //totales
                                                            $cont_total2=$count_tot2->tot_estatus;
                                                            //realizada
                                                            $cont_realiza2=$count_2->tot_estatus;
                                                            //incompletas
                                                            $cont_incom2=$count2_2->tot_estatus;
                                                            //no realizadas
                                                            $cont_no_realiza2=$count3_2->tot_estatus;
                                                            //canceladas
                                                            $cont_cancela2=$count4_2->tot_estatus;
                                                        }
                                                        $cumpli2 = $cont_realiza2*100/$cont_total2;
                                                        //$semanal2=$cumpli2/7; //comentado dps de productivo
                                                        $semanal2=$cumpli2/${'cont_sem_'.$semana."_".$mes}; //comentado despues de productivo
                                                        $total2=$semanal2;
                                                        array_push($array_sum, $cumpli2);
                                                        //$superta2=round($superta2+$total2,3);
                                                    }
                                                    if($r->turno==3 && $turno_proy3=="1"){
                                                        if($id_ant!=$r->id){
                                                            $items++;
                                                            $count_tot3=$this->ModelReportes->count_estatus($r->id,0,$r->fecha,$id_proy,3);
                                                            $count_turno3=$this->ModelReportes->count_turnos($r->fecha,$id_proy,3);
                                                            $count4_3=$this->ModelReportes->count_estatus($r->id,1,$r->fecha,$id_proy,3);//cancelado
                                                            $count_3=$this->ModelReportes->count_estatus($r->id,3,$r->fecha,$id_proy,3);
                                                            $count2_3=$this->ModelReportes->count_estatus($r->id,4,$r->fecha,$id_proy,3);
                                                            $count3_3=$this->ModelReportes->count_estatus($r->id,5,$r->fecha,$id_proy,3);
                                                            //totales turno
                                                            $count_turno3=$count_turno3->tot_turnos;
                                                            //totales
                                                            $cont_total3=$count_tot3->tot_estatus;
                                                            //realizada
                                                            $cont_realiza3=$count_3->tot_estatus;
                                                            //incompletas
                                                            $cont_incom3=$count2_3->tot_estatus;
                                                            //no realizadas
                                                            $cont_no_realiza3=$count3_3->tot_estatus;
                                                            //canceladas
                                                            $cont_cancela3=$count4_3->tot_estatus;
                                                        }
                                                        $cumpli3 = $cont_realiza3*100/$cont_total;
                                                        //$semanal3=$cumpli3/7; //comentado dps de productivo
                                                        $semanal3=$cumpli3/${'cont_sem_'.$semana."_".$mes}; //comentado despues de productivo
                                                        $total3=$semanal3; //comentado dps de productivo
                                                        //$superta3=round($superta3+$total3,3);
                                                        array_push($array_sum, $cumpli3);
                                                    }
                                                    $semana_ant=$semana;
                                                    $fecha_ant=$r->fecha;
                                                    $id_ant=$r->id;

                                                    $superta=round($superta+$total,2);
                                                    $superta2=round($superta2+$total2,2);
                                                    $superta3=round($superta3+$total3,2);
                                                }
                                                $mes_ant=$r->fecha;
                                            }
                                            $supersup=0;
                                            $supersup=0;
                                            if($count_turno1>0){
                                                $count_turno1=1;
                                            }if($count_turno2>0){
                                                $count_turno2=1;
                                            }if($count_turno3>0){
                                                $count_turno3=1;
                                            }
                                            
                                            if($cont>0){
                                                //$supersup=round(($superta+$superta2+$superta3)/($cont),2); 
                                                $cont_turn_tot=$count_turno1+$count_turno2+$count_turno3;
                                                if($cont_turn_tot>0){
                                                    $supersup=round(($superta+$superta2+$superta3)/$cont_turn_tot,2); 
                                                }else{
                                                    $supersup=round(($superta+$superta2+$superta3),2); 
                                                }
                                                if(count($array_sum)>0){
                                                    $supersup=round(array_sum($array_sum)/count($array_sum),2);
                                                }else{
                                                    $supersup=0;
                                                }
                                            }
                                            //$superta=round($superta+$total,2);
                                            //$superta2=round($superta2+$total2,2);
                                            //$superta3=round($superta3+$total3,2);
                                            
                                            //echo "<br>cont: ".$cont;
                                            $html_ah.='<th>'.nameMes($params['mes']).'</th>';
                                            $html_anual.='<td><input type="hidden" id="total_anual" value="'.$supersup.'">
                                                      <input type="hidden" id="mes_anual" value="'.nameMes($params['mes']).'"> '.$supersup.'%</td>';
                                            
                                        }
                                    } 
                                    echo "<tr>".$html_ah."</tr></thead><tbody><tr>".$html_anual."</tr></tbody>";
                                }
                                ?>
                            </table>
                        </div>
                        <div class="col-md-12"></div>
                        <div class="col-md-6 calc">
                            <center><span><b>CUMPLIMIENTO OTS ANUAL</b> </span></center>
                            <div class="card-body chart-block">
                                <canvas id="graph_anual"></canvas>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <!--<div class="col-4">
                            <center><span><b>EFICACIA DE LIMPIEZAS REALIZADAS</b> </span></center>
                            <div class="card-body chart-block">
                                <canvas id="graph_sem_cumple"></canvas>
                            </div>
                        </div>-->
                    </div>
                    <div class="row">
                        <div class="col-md-12"><br><hr></div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                              <div class="form-group">
                                <label class="control-label">Semana: </label>
                                <select class="form-control round" id="semana_act">
                                </select>
                              </div>
                            </div>

                            <div class="col-md-3">
                              <br>
                              <button id="expot_list" class="btn btn-success">Exportar a Excel <i class="fa fa-file-excel-o"></i></button>
                            </div>
                            <div class="col-md-12 table-responsive" id="cont_table_cumpli">
                                <table style="text-align: center; vertical-align: middle; font-size: 11px;" class="table dataTable" border="1" width="100%" id="table_rep_sem">
                                    <thead>
                                        <tr>
                                            <th colspan="3" rowspan="4">
                                                <table>
                                                    <tr>
                                                        <td width="50%"><img class="img-fluid" height="90px" src="<?php echo base_url();?>public/img/logofinalsys2.png"></td>
                                                        <td width="50%"><b>TIME LINE</b>
                                                            <p>SNF PRO</p>
                                                            <b>PLANEACIÓN GERENCIAL</b>
                                                            <p id="name_cli"> CLIENTE: </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%"></td>
                                                        <td width="50%"><img id="logo_cli" class="img-fluid" height="90px" src="<?php echo base_url(); ?>uploads/clientes/<?php echo $logo_cli; ?>"></td>
                                                    </tr>
                                                </table>   
                                            </th>
                                            <th rowspan="2">%PORCENTAJE A LA EFICACIA (CALIDAD EN EL TRABAJO REALIZADO):</th>
                                            <th rowspan="2">%PORCENTAJE DE CUMPLIMIENTO EFICIENCIA:</th>
                                            <th colspan="9">TITULO/ PROYECTO: Plan de trabajo semana </th>
                                            <th>Nivel de cambios: </th>
                                        </tr>
                                        <tr>
                                            <th>Fecha inicial:</th>
                                            <th id="fecha_ini"></th>
                                            <th id="mes_anio" colspan="7" rowspan="2"> / ¿Cuándo?</th>
                                            <th>Codigo doc: SNF-CAL-10</th>
                                        </tr>
                                        <tr>
                                            <th style="font-size:35px" id="head_fin_eficacia">%</th>
                                            <th style="font-size:35px" id="head_fin_eficiencia">%</th>
                                            <th>Fecha final programada:</th>
                                            <th id="fecha_final"></th>
                                            <th rowspan="3">MINUTA / NOTA:</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2"><span class='btn btn-danger'> X</span> CANCEL <span class='btn btn-info'> P</span> PROG  <span class='btn btn-success'>1</span> OK <span class='btn btn-warning'>-</span> RE-PROG <span class='btn btn-warning'>N</span> INCUMP.</th>
                                            <th>Proceso:</th>
                                            <th>Limpieza Industrial</th>
                                            <th id="num_semana" colspan="7">SEMANA </th>
                                        </tr>
                                        <tr>
                                            <th>OT</th>
                                            <th>ITEM</th>
                                            <th>¿QUE?</th>
                                            <th>ACTIVIDADES / ¿COMO?</th>
                                            <th>¿Porque?</th>
                                            <th>¿Quien?</th>
                                            <th>¿Donde?</th>
                                            <th id="day1">26 </th>
                                            <th id="day2">27 </th>
                                            <th id="day3">28 </th> 
                                            <th id="day4">29 </th>
                                            <th id="day5">30 </th>
                                            <th id="day6">1 </th>
                                            <th id="day7">2 </th>
                                        </tr>
                                    </thead>
                                    <tbody id="body_table_rep_sem">
                                    </tbody>
                                    <tfoot id="foot_table_rep_sem"></tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-sm-12 box-col-12" id="cont_graph_cumplimiento">
                                <div class="card-body chart-block">
                                    <div class="chart-overflow" id="graph_cumplimiento"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 chartWithMarkerOverlay">
                            <div class="col-sm-12 box-col-12" id="cont_graph_eficiencia">
                                <div class="card-body chart-block">
                                    <div class="chart-overflow" id="graph_eficiencia"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1" style="margin-top:5%;">
                            <div class="">
                                <img src="<?php echo base_url(); ?>public/img/ok_ng.png" height="110">
                            </div>
                        </div>
                    </div>
                    <!-- agregar tabla de comentarios cuando sea reprogramado -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-12 box-col-12" id="cont_table_coment">
                                <table style="text-align: center;" class="table" border="1" width="100%" id="table_semanas">
                                    <thead class="table_thead">
                                        <tr>
                                            <th>FECHA</th>
                                            <th>ACTIVIDAD</th>
                                            <th>OT</th>
                                            <th>COMENTARIOS</th>
                                            <th>ESTATUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $table_com; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

function nameMes($m){
    $mes="Enero";
    switch ($m) {
      case 2: $mes="Febrero"; break;
      case 3: $mes="Marzo"; break;
      case 4: $mes="Abril"; break;
      case 5: $mes="Mayo"; break;
      case 6: $mes="Junio"; break;
      case 7: $mes="Julio"; break;
      case 8: $mes="Agosto"; break;
      case 9: $mes="Septiembre"; break;
      case 10: $mes="Octubre"; break;
      case 11: $mes="Noviembre"; break;
      case 12: $mes="Diciembre"; break;
    }
    return $mes;
  }

function saber_dia($name) {
    //log_message('error', 'name: '.$name);
    $dias = array('','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');
    $fecha = $dias[date('N', strtotime($name))];
    return $fecha;
}
?>