<?php 
  header("Content-Type: text/html;charset=UTF-8");
  header("Pragma: public");
  header("Expires:0");
  header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
  header("Content-Type: application/force-download");
  header("Content-Type: application/octet-stream");
  header("Content-Type: application/download");
  header("Content-Type: application/vnd.ms-excel;");
  header("Content-Disposition: attachment; filename=check_list_diario".$id_proy."_".$mes.".xls");

  function redimensionar($src,$name){
    $imagen = $src; //Imagen original
    $imagenNueva = FCPATH."uploads/monitoreo/2_".$name; //Nueva imagen
    $nAncho = 105; //Nuevo ancho
    $nAlto = 75;  //Nuevo alto
    
    //Creamos una nueva imagen a partir del fichero inicial
    if(pathinfo($name,PATHINFO_EXTENSION)=="jpg" || pathinfo($name,PATHINFO_EXTENSION)=="jpeg" || pathinfo($name,PATHINFO_EXTENSION)=="JPG" || pathinfo($name,PATHINFO_EXTENSION)=="JPEG")
        $imagen = imagecreatefromjpeg($imagen); 
    else if(pathinfo($name,PATHINFO_EXTENSION)=="png" || pathinfo($name,PATHINFO_EXTENSION)=="PNG")
        $imagen = imagecreatefrompng($imagen); 

    //Obtenemos el tamaño 
    $x = imagesx($imagen);
    $y = imagesy($imagen);
    
    // Crear una nueva imagen, copia y cambia el tamaño de la imagen
    $img = imagecreatetruecolor($nAncho, $nAlto);
    imagecopyresized($img, $imagen, 0, 0, 0, 0, $nAncho, $nAlto, $x, $y);
    
    //Creamos el archivo jpg
    imagejpeg($img, $imagenNueva);
    return "2_".$name;
  }

    $html_head=""; $html=""; $html_foot=""; $i=0; $array=array();
    $fecha_f="";

    $params["id_proy"] = $id_proy;
    $params["mes"] = $mes;
    $params["anio"] = $anio;
    $params["id_act"] = $id_act;

    foreach ($act as $a) {
      $fecha_i = ''.$a->mes.'/01/'.$a->anio.'';
      $fecha_f = $a->ultimo;
    }
    if($fecha_f==""){
      $fecha_i = ''.$mes.'/01/'.date("Y").'';
      $fecha_f = ''.$mes.'/31/'.date("Y").'';
    }

    $begin = new DateTime($fecha_i);
    $end = new DateTime($fecha_f);
    $end = $end->modify( '+1 day' );
    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);
    $html_head="<tr>
                  <th>#</th>
                  <th>ZONA</th>
                  <th>INSPECCIÓN</th>
                  <th colspan='2'>FOTO</th>
                  <th>DESCRIPCIÓN DE LA ACTIVIDAD</th>
                  <th>COMENTARIOS</th>
                  <th>FRECUENCIA</th>";

    $html_foot="<tr>
              <td colspan='3'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>";

    foreach($daterange as $date){
      if(date('l', strtotime($date->format("d-m-Y"))) != 'Sunday'){
        //log_message('error', 'dia del fin : '.$date->format("d"));
        $html_head.="<th><input id='num_dia' type='hidden' value='".$date->format("d")."'>".$date->format("d")."</th>";
        ${'dias_'.intval($date->format("d"))}=array();
        array_push($array,$date->format("d"));
        $html_foot.="<td id='ft_".intval($date->format("d"))."'></td>";
      }
    }
    $html_foot.="<td></td>
            </tr>";

    $html_head.="<th>ÁREA DE OPORTUNIDAD</th>
              </tr>";
    $cont=0; $cont2=0; $cont3=0; $cont4=0; $cont5=0; 

    $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].')';
    if(isset($array[4])){
      $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].' or DATE_FORMAT(ma.fecha, "%d")='.$array[4].')';
    }

    $html_foot2=""; $i=0;
    $cont_area=0; $cont_area1=0; $cont_area2=0; $cont_area3=0; $cont_area4=0;
    //$act=$this->ModelReportes->get_result_monitoreo($params,0,$where);
    $act=$this->ModelReportes->get_result_monitoreoNvo($params,0,$where,$id_act);
    foreach ($act as $a) {
      $cont=0;
      $cont_exist=0;
      $fecha_camb = strtotime($a->fecha);
      $semana = date('W', $fecha_camb);
      $i++;
      $logo=$a->foto;
      $anio=$a->anio;
      $tmp=explode(",",$a->dias_act);
      $tmp_seg=explode(",",$a->seguimiento_act);
      //log_message('error', 'tmp : '.count($tmp));
      if($i->foto!=""){
        $imagen = base_url()."uploads/monitoreo/".$a->foto;
        $img_med = redimensionar($imagen, $a->foto);
        $img='<img src="'.base_url()."uploads/monitoreo/".$img_med.'" />';
      }else{
        $img='';
      }

      $html.="<tr>
              <td>".$i."</td>
              <td>".$a->zona."</td>
              <td>".$a->inspeccion."</td>
              <td colspan='2' style='align-items: center; justify-content: center'><p class='pspaces'></p>
                <table>
                  <tr> <td colspan='3' width='100%''></td></tr>
                  <tr>
                    <td width='10%'></td>
                    <td width='80%'>".$img."</td>
                    <td width='10%'></td>
                  </tr>
                </table><br><br><br><br>
              </td>
              <td>".$a->nombre."</td>
              <td>".$a->observaciones."</td>
              <td>".$a->frecuencia."</td>";
              for($j=0; $j<count($array); $j++){
                //for($k=0; $k<count($tmp); $k++){
                //log_message('error', 'array : '.$array[$j]);
                //log_message('error', 'tmp : '.$tmp[$j]);
                //log_message('error', 'tmp_seg : '.$tmp_seg[$j]);

                if(in_array($array[$j], $tmp)){ //son dias del mes
                  //log_message('error', 'el dia existe : '.$array[$j]);
                  //log_message('error', 'cont_exist : '.$cont_exist);
                  //log_message('error', 'j : '.$j);
                  if(isset($tmp_seg[$cont_exist])){
                    //log_message('error', 'tmp_seg existe : '.$tmp_seg[$cont_exist]);
                    if($tmp_seg[$cont_exist]=="1"){
                      ${'dias_'.intval($array[$j])}[]=1;
                    }
                    else if($tmp_seg[$cont_exist]=="0"){
                      ${'dias_'.intval($array[$j])}[]=1;
                    }
                    else if($tmp_seg[$cont_exist]=="2"){ //N/A
                      ${'dias_'.intval($array[$j])}[]=1;
                    }
                
                    $html.="<td><input data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='".$tmp_seg[$cont_exist]."'>".$tmp_seg[$cont_exist]."</td>";

                    if($tmp_seg[$cont_exist]=="1" || $tmp_seg[$cont_exist]=="2"){
                      $cont++;
                    }
                  }else{
                    ${'dias_'.intval($array[$j])}[]=0;
                    $html.="<td><input data-id='".$a->id."' data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='0'>0</td>";
                  }
                  $cont_exist++;
                }else{
                  ${'dias_'.intval($array[$j])}[]=0;
                  $html.="<td><input data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='0'></td>";
                }
                $html_foot="<tr>
                <td colspan='5'></td>
                  <td colspan='2'>% CUMPLIMIENTO</td>
                  ".$html_foot2."
                  <td></td>
                </tr>";
              }

              $html_foot2x='';
              for($j=0; $j<count($array); $j++){
                //$html_foot2x.='<td>'.array_sum(${'dias_'.intval($array[$j])}).'</td>';
                $porcentaje=(array_sum(${'dias_'.intval($array[$j])})/count(${'dias_'.intval($array[$j])}))*100;
                $html_foot2x.='<td>'.round($porcentaje,2).'</td>';
              }
              $html_foot="<tr>
                <td colspan='5'></td>
                  <td colspan='2'>% CUMPLIMIENTO</td>
                  ".$html_foot2x."
                  <td></td>
                </tr>";



        $html.="<td><input id='val_area' type='hidden' value='".round(($cont/$a->tot_act*100),2)."'>".round(($cont/$a->tot_act*100),2)."</td>
              </tr>";
    }

    echo '<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <table style="text-align:center;" class="table" border="1" width="100%" id="getMonitoreoFin">
            <thead class="table_thead" id="tble_head">
             '.$html_head.'
            </thead>
            <tbody id="body_table">
              '.$html.'
            </tbody>
            <tfoot id="tfoot">
              '.$html_foot.'
              <tr>
                <td colspan="'.(count($array)+2).'"></td>
                <td colspan="5">Firma de cliente</td>
                <td height="70px"></td>
              </tr>
            </tfoot>
          </table>';