<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<input type="hidden" id="base_url" value="<?php echo base_url();?>">

<script src="<?php echo base_url(); ?>assets/js/config.js"></script>

<script src="<?php echo base_url(); ?>assets/js/chart/google/google-chart-loader.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/chart/google/google-chart.js"></script>-->
<script src="<?php echo base_url();?>public/js/reportes/resultados_matriz_accion.js?v=<?php echo date('YmdGis') ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/tooltip-init.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/datatable/datatables/datatable.custom.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
