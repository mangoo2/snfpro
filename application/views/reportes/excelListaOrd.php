<?php
	header("Content-Type: text/html;charset=UTF-8");
	header("Pragma: public");
	header("Expires:0");
	header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Type: application/vnd.ms-excel;");
	header("Content-Disposition: attachment; filename=salida_cumplimiento".date("Ymd").".xls");

  function redimensionar($src,$name){
    $imagen = $src; //Imagen original
    $imagenNueva = FCPATH."uploads/clientes/2_".$name; //Nueva imagen
    $nAncho = 50; //Nuevo ancho
    $nAlto = 50;  //Nuevo alto
    
    //Creamos una nueva imagen a partir del fichero inicial
    if(pathinfo($name,PATHINFO_EXTENSION)=="jpg" || pathinfo($name,PATHINFO_EXTENSION)=="jpeg" || pathinfo($name,PATHINFO_EXTENSION)=="JPG" || pathinfo($name,PATHINFO_EXTENSION)=="JPEG")
        $imagen = imagecreatefromjpeg($imagen); 
    else if(pathinfo($name,PATHINFO_EXTENSION)=="png" || pathinfo($name,PATHINFO_EXTENSION)=="PNG")
        $imagen = imagecreatefrompng($imagen); 

    //Obtenemos el tamaño 
    $x = imagesx($imagen);
    $y = imagesy($imagen);
    
    // Crear una nueva imagen, copia y cambia el tamaño de la imagen
    $img = imagecreatetruecolor($nAncho, $nAlto);
    imagecopyresized($img, $imagen, 0, 0, 0, 0, $nAncho, $nAlto, $x, $y);
    
    //Creamos el archivo jpg
    imagejpeg($img, $imagenNueva);
    return "2_".$name;
  }

	$html_head=array(); $html=""; $html_foot=""; $i=0; $turno=""; $logo=""; $empresa="";
    $anio=0; $fecha_ini=""; $fecha_final="";
    $fin_cumple=0;
    $fin_progra=0;
    $fin_efi=0;
    $fin_ok=0;
    $fin_efica=0;
    $cont_can=0; 
    $cont_pro=0;
    $cont_ok=0; 
    $cont_re=0; 
    $html_foot_can="";
    $html_foot_pro="";
    $html_foot_ok="";
    $html_foot_re="";
    $cont_ok2=0; 
    $cont_ok3=0;
    $cont_ok4=0;
    $cont_ok5=0;
    $cont_ok6=0;
    $cont_ok7=0;
    $cont_pro2=0; 
    $cont_pro3=0;
    $cont_pro4=0;
    $cont_pro5=0;
    $cont_pro6=0;
    $cont_pro7=0;

    foreach ($act as $a) {
    	for($day=0; $day<7; $day++){ 
			if(date('d', strtotime($a->anio."W".$semana.$day))==$a->dia){
				if($a->seguimiento==1){//Cancelado
					$cont_can++;
				}
				if($a->seguimiento==2){//Programado			
					if($day==1){
					  $cont_pro++;
					}else if($day==2){
					  $cont_pro2++;
					}else if($day==3){
					  $cont_pro3++;
					}else if($day==4){
					  $cont_pro4++;
					}else if($day==5){
					  $cont_pro5++;
					}else if($day==6){
					  $cont_pro6++;
					}else if($day==7){
					  $cont_pro7++;
					}
				}
				if($a->seguimiento==3){//Ok
					if($day==1){
					  $cont_ok++;
					}else if($day==2){
					  $cont_ok2++;
					}else if($day==3){
					  $cont_ok3++;
					}else if($day==4){
					  $cont_ok4++;
					}else if($day==5){
					  $cont_ok5++;
					}else if($day==6){
					  $cont_ok6++;
					}else if($day==7){
					  $cont_ok7++;
					}
				}
				if($a->seguimiento==4){//Reprogramado
					$cont_re++;	
				}
			}
		}
	}

	$fin_ok=$cont_ok+$cont_ok2+$cont_ok3+$cont_ok4+$cont_ok5+$cont_ok6+$cont_ok7;
    $fin_progra=$cont_pro+$cont_pro2+$cont_pro3+$cont_pro4+$cont_pro5+$cont_pro6+$cont_pro7;
    /*$efi=$cont_ok/$cont_pro*100;
    $efi2=$cont_ok2/$cont_pro2*100;
    $efi3=$cont_ok3/$cont_pro3*100;
    $efi4=$cont_ok4/$cont_pro4*100;
    $efi5=$cont_ok5/$cont_pro5*100;
    $efi6=$cont_ok6/$cont_pro6*100;
    $efi7=$cont_ok7/$cont_pro7*100;*/
    if($cont_ok>0)
      $efi=$cont_ok/$cont_ok*100;
    else
      $efi=0;
    if($cont_ok2>0)
      $efi2=$cont_ok2/$cont_ok2*100;
    else
      $efi2=0;
    if($cont_ok3>0)
      $efi3=$cont_ok3/$cont_ok3*100;
    else
      $efi3=0;
    if($cont_ok4>0)
      $efi4=$cont_ok4/$cont_ok4*100;
    else
      $efi4=0; 
    if($cont_ok5>0)
      $efi5=$cont_ok5/$cont_ok5*100;
    else
      $efi5=0; 
    if($cont_ok6>0)
      $efi6=$cont_ok6/$cont_ok6*100;
    else
      $efi6=0; 
    if($cont_ok7>0)
      $efi7=$cont_ok7/$cont_ok7*100;
    else
      $efi7=0;

   	foreach ($act as $a) {
		$i++;
    if($a->foto!=""){
      $imagen = base_url()."uploads/clientes/".$a->foto;
      $img_med = redimensionar($imagen, $a->foto);
      $img='<img src="'.base_url()."uploads/clientes/".$img_med.'" width="55px" height="55px" />';
    }else{
      $img='';
    }

		//$logo=$a->foto;
		if($a->turno==1)
		  $turno="1ER";
		else if($a->turno==2)
		  $turno="2DO";
		else if($a->turno==3)
      $turno="3ER";

    	if($i==1){
    		//log_message('error', 'fin_ok : '.$fin_ok);
    		$html.='<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    		<table style="text-align: center; vertical-align: middle;" class="table" border="1" width="100%" id="table_rep_sem">
            <thead>
                <tr>
                    <th colspan="3" rowspan="4">
                        <table>
                            <tr>
                                <td width="50%"><img class="img-fluid" height="90px" src="'.base_url().'public/img/logo_mail2.png"></td>
                                <td width="50%" align="right"><b>TIME LINE</b>
                                    <p align="right">SNF PRO</p>
                                    <b align="right">PLANEACIÓN GERENCIAL</b>
                                    <p align="right" id="name_cli"> CLIENTE: '.$a->empresa.'</p>
                                </td>
                                <td width="50%">'.$img.'</td>
                            </tr>
                            <tr>
                                <td width="50%"></td>
                                <td width="50%"></td>
                            </tr>
                        </table>   
                    </th>
                    <th rowspan="2">%PORCENTAJE A LA EFICACIA (CALIDAD EN EL TRABAJO REALIZADO):</th>
                    <th rowspan="2">%PORCENTAJE DE CUMPLIMIENTO EFICIENCIA:</th>
                    <th colspan="9">TITULO/ PROYECTO: Plan de trabajo semana </th>
                    <th>Nivel de cambios: </th>
                </tr>
                <tr>
                    <th>Fecha inicial:</th>
                    <th>'.$a->fecha_ini.'</th>
                    <th colspan="7" rowspan="2">'.$name_mes.' '.$a->anio.' / ¿Cuándo?</th>
                    <th>Codigo doc: SNF-CAL-10</th>
                </tr>
                <tr>
                    <th>'.($efi+$efi2+$efi3+$efi4/4).'%</th>
                    <th>'.($fin_ok/180*100).'%</th>
                    <th>Fecha final programada:</th>
                    <th>'.$a->fecha_final.'</th>
                    <th rowspan="3">MINUTA / NOTA:</th>
                </tr>
                <tr>
                    <th colspan="2">X = CANCEL  P = PROG   1 = OK  - = RE-PROG</th>
                    <th>Proceso:</th>
                    <th>Limpieza Industrial</th>
                    <th colspan="7">SEMANA '.$semana.'</th>
                </tr>
                <tr>
                    <th>OT</th>
                    <th>ITEM</th>
                    <th>¿QUE?</th>
                    <th>ACTIVIDADES / ¿COMO?</th>
                    <th>¿Porque?</th>
                    <th>¿Quien?</th>
                    <th>¿Donde?</th>';
            for($day=1; $day<=7; $day++)
				    {
				      	$html.="<td>".date('d', strtotime($a->anio."W".$semana.$day))."</td>";
				      	//log_message('error', 'dias de la semana : '.date('d', strtotime($anio."W".$semana.$day)));
				     	//$html_head[]=array(date('d', strtotime($anio."W".$semana.$day)));
				    }

            $html.='</tr>
            </thead>
            <tbody id="body_table_rep_sem">';
    	}

     	$logo=$a->foto; $empresa=$a->empresa; $fecha_ini=$a->fecha_ini; $fecha_final=$a->fecha_final;
      	$html.="<tr>
              <td>".$a->ot."</td>
              <td>".$i."</td>
              <td>".$a->nombre."</td>
              <td>EN BASE A TASK SEGÚN ORDEN DE TRABAJO</td>
              <td>ASEGURAR LIMPIEZA Y PROGRAMA SEMANAL DE LIMPIEZA CUMPLIMIENTO AL 100%</td>
              <td>".$turno." TURNO</td>
              <td>PAINT CSAP</td>";
              for($day=1; $day<=7; $day++)
              { 
                //log_message('error', 'day : '.date('d', strtotime($anio."W".$semana.$day)));
                if(date('d', strtotime($a->anio."W".$semana.$day))==$a->dia){
                  //log_message('error', 'dias iguales : '.$a->dia);
                  //$html.="<td>".$a->dia."</td>";
                  if($a->seguimiento==1){//Cancelado
                    //$cont_can++;
                    $html.="<td><span class='btn btn-danger'>X</span></td>";
                  }
                  if($a->seguimiento==2){//Programado
                    $html.="<td><span class='btn btn-info'>P</span></td>";
                    /*if($day==1){
                      $cont_pro++;
                    }else if($day==2){
                      $cont_pro2++;
                    }else if($day==3){
                      $cont_pro3++;
                    }else if($day==4){
                      $cont_pro4++;
                    }else if($day==5){
                      $cont_pro5++;
                    }else if($day==6){
                      $cont_pro6++;
                    }else if($day==7){
                      $cont_pro7++;
                    }*/
                  }
                  if($a->seguimiento==3){//OK
                    $html.="<td><span class='btn btn-success'>1</span></td>";
                    /*if($day==1){
                      $cont_ok++;
                    }else if($day==2){
                      $cont_ok2++;
                    }else if($day==3){
                      $cont_ok3++;
                    }else if($day==4){
                      $cont_ok4++;
                    }else if($day==5){
                      $cont_ok5++;
                    }else if($day==6){
                      $cont_ok6++;
                    }else if($day==7){
                      $cont_ok7++;
                    }*/
                  }
                  if($a->seguimiento==4){//Reprogramado
                    //$cont_re++;
                    $html.="<td><span class='btn btn-warning'>-</span></td>";
                  }
                }else{
                  $html.="<td></td>";
                }
              }
        $html.="<td>".$a->observaciones."</td>
          </tr>";

    }
    $html.="</tbody>";

    $html.="<tfoot>
            <tr>
              <td colspan='6'></td>
              <td>CUMPLIMIENTO</td>
              <td id='cont_ok'>".$cont_ok."</td>
              <td id='cont_ok2'>".$cont_ok2."</td>
              <td id='cont_ok3'>".$cont_ok3."</td>
              <td id='cont_ok4'>".$cont_ok4."</td>
              <td id='cont_ok5'>".$cont_ok5."</td>
              <td id='cont_ok6'>".$cont_ok6."</td>
              <td id='cont_ok7'>".$cont_ok7."</td>
              <td id='fin_ok'>".$fin_ok."</td>
            </tr>
          <tr>
            <td colspan='6'></td>
            <td>PROGRAMADO</td>
            <td id='cont_pro'>".$cont_ok."</td>
            <td id='cont_pro2'>".$cont_ok2."</td>
            <td id='cont_pro3'>".$cont_ok3."</td>
            <td id='cont_pro4'>".$cont_ok4."</td>
            <td id='cont_pro5'>".$cont_ok5."</td>
            <td id='cont_pro6'>".$cont_ok6."</td>
            <td id='cont_pro7'>".$cont_ok7."</td>
            <td>".$fin_ok."</td>
          </tr>
          <tr>
            <td colspan='5'></td>
            <td>CUMPLIMIENTO AL OBJETIVO</td>
            <td>EFICIENCIA</td>
            <td id='efi'>".$efi."%</td>
            <td id='efi2'>".$efi2."%</td>
            <td id='efi3'>".$efi3."%</td>
            <td id='efi4'>".$efi4."%</td>
            <td id='efi5'>".$efi5."%</td>
            <td id='efi6'>".$efi6."%</td>
            <td id='efi7'>".$efi7."%</td>
            <td>".($efi+$efi2+$efi3+$efi4/4)."%</td>
          </tr>
          <tr>
            <td colspan='6'></td>
            <td>LIMPIEZAS OK</td>
            <td>".$cont_ok."</td>
            <td>".$cont_ok2."</td>
            <td>".$cont_ok3."</td>
            <td>".$cont_ok4."</td>
            <td>".$cont_ok5."</td>
            <td>".$cont_ok6."</td>
            <td>".$cont_ok7."</td>
            <td>".$fin_ok."</td>
          </tr>
          <tr>
            <td colspan='5'></td>
            <td>CALIDAD EN LA LIMPIEZA</td>
            <td>EFICACIA</td>
            <td>".$efi."%</td>
            <td>".$efi2."%</td>
            <td>".$efi3."%</td>
            <td>".$efi4."%</td>
            <td>".$efi5."%</td>
            <td>".$efi6."%</td>
            <td>".$efi7."%</td>
            <td id='fin_eficacia'>".($efi+$efi2+$efi3+$efi4/4)."%</td>
          </tr>";

    $html.="</tfoot>
          </table>";
    echo $html;

?>
