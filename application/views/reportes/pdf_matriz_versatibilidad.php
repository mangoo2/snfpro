<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    //$GLOBALS['servicio']=$servicio;

    $GLOBALS['logo_cli']=$logo_cli;


//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logofinalsys2.png';
        if($GLOBALS['logo_cli']!="")
            $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
        else
            $logo_cliente ="#";
        //$this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);

        $html = '<table width="100%">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="15%"><img src="'.$logos.'" height="90px"></td>
                <td width="65%" style="font-weight:bold; color:rgba(68, 114, 196); text-align:center; font-size:32px">SERVICIOS NUEVOS DE FILTRACIÓN HISPANOMEXICANOS S.A DE C.V.</td>
                <td width="20%" align="center"><img src="'.$logo_cliente.'" height="90px"></td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $pie = base_url().'public/img/footer.png';
        $html = '
        <table width="100%" cellpadding="2">
        <tr>
            <td width="100%"><img src="'.$pie.'"></td>
        </tr>
      </table>';
    
        //$this->writeHTMLCell(188, '', 12, 100, $html, 0, 0, 0, true, 'C', true);
        $this->Image($pie, 0, 165, 310, 50, 'PNG', '', '', true, 310, '', false, false, 0);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('SALIDA - Matriz Versatilidad');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10,36,10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 25);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setPrintHeader(false);
// add a page
$pdf->AddPage('L', 'A4'); 

$img_file = base_url().'public/img/portada_mat_versa.png';

                            //ancho alto
$pdf->Image($img_file, 0, 0, 400, 190, 'PNG', '', '', true, 310, '', false, false, 0);

$logos = base_url().'public/img/logofinalsys2.png';
$pdf->Image($logos, 16, 13, 39,25, 'PNG', '', '', true, 150, '', false, false, false);

if($GLOBALS['logo_cli']!=""){
    $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
    $pdf->Image($logo_cliente, 239, 13, 39,25, 'JPG', '', '', true, 150, '', false, false, 0);
}
/*$html="";
    $html.='<style>
                .titulo{
                    font-size:28px;
                }
                .subtitulo{
                    font-size:54px;   
                }
                .subtitulo2{
                    font-size:23px;   
                }
                .subtitulo3{
                    font-size:20px;   
                }
            </style>
            <table border="0" align="center">
                <tr>
                    <td class="titulo">SERVICIOS NUEVOS DE FILTRACION HISPANOMEXICANOS S.A. DE C.V.</td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
            <table border="0" align="center">
                
                <tr>
                    <td width="20%"></td>
                    <td width="60%"class="subtitulo">MATRIZ DE VERSATILIDAD</td>
                    <td width="20%"></td>
                </tr>
                <tr>
                    <td colspan="3" ></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="subtitulo2"><b></b></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3" ></td>
                </tr>
                
            </table>
            
            ';

$pdf->writeHTML($html, true, false, true, false, '');*/

$pdf->setPrintHeader(true);

// add a page

$pdf->setPrintFooter(true);

//===============================================================
/*
foreach ($result->result() as $item) {
    //===============================================================
    $pdf->AddPage('L', 'A4'); 
    $pdf->setPrintFooter(false);
    $img_base64_encodet=$item->tabla;
    if($item->tabla!=""){
        $img='<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $img_base64_encodet) . '">';
    }

    $html='<table border="0" align="center">
                <tr><td><br></td></tr>
                <tr>
                    <td>'.$img.'</td>
                </tr>
            </table>';
    $pdf->writeHTML($html, true, false, true, false, '');    
    //===============================================================

    //===============================================================
    $pdf->AddPage('L', 'A4'); 
    $pdf->setPrintFooter(false);
    $img_base64_encoded=$item->charbar;
    if($item->charbar!=""){
        $img2='<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $img_base64_encoded) . '">';
    }
    $html='<table border="0" align="center">
                <tr>
                    <td>'.$img2.'</td>
                </tr>
            </table>';
    $pdf->writeHTML($html, true, false, true, false, '');    
    //===============================================================



    //===============================================================
    $pdf->AddPage('L', 'A4'); 
    $pdf->setPrintFooter(false);
    $img_base64_encoded_pie=$item->charpie;
    if($item->charpie!=""){
        $img3='<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $img_base64_encoded_pie) . '">';
    }
    $html='<table border="0" align="center">
                <tr><td><br></td></tr>
                <tr>
                    <td width="85%">'.$img3.'</td>
                </tr>
            </table>';
    $pdf->writeHTML($html, true, false, true, false, '');    
    //===============================================================
}
*/

$html="";
/*$html='<style type="text/css">
            .table{
                font-size:7.5px;
            }
        </style>
        <!--<table class="table" cellpadding="1">
            <tr>
                <td width="2%">
                    <img width="9px" src="'.base_url().'public/img/capacitado.png" >
                </td>
                <td width="6%">
                    Capacitado
                </td>
                <td width="2%">
                    <img width="9px" src="'.base_url().'public/img/capacitacion.png" >
                </td>
                <td width="6%">
                    Capacitación
                </td>
            </tr>
        </table>-->
        <table class="table" cellpadding="1">
            <tr>
                <td width="2%">
                    <img width="9px" src="'.base_url().'public/img/capacitado.png" >
                </td>
                <td width="6%">
                    Capacitado
                </td>
                <td width="2%">
                    <img width="9px" src="'.base_url().'public/img/capacitacion.png" >
                </td>
                <td width="6%">
                    Capacitación
                </td>
                <td width="29%"></td>
                <td>
                    <b><u>Criterios de Certificación</u></b>
                </td>
            </tr>
            <tr>
                <td width="2%">
                    <img width="10px" src="'.base_url().'public/img/cuadro1.png" >
                </td>
                <td width="30%">
                    <b>N. 1: Aprende a llevar a cabo tareas básicas y a desempeñar la operación.</b>
                </td>

                <td width="2%">
                    <img width="10px" src="'.base_url().'public/img/cuadro2.png" >
                </td>
                <td width="20%">
                    <b>N. 2: Habilitado para desempeñar la operación</b>
                </td>
      
                <td width="2%">
                    <img width="10px" src="'.base_url().'public/img/cuadro3.png" >
                </td>
                <td width="20%">
                    <b>N. 3: Certificado para desempeñar la operación</b>
                </td>
           
                <td width="2%">
                    <img width="10px" src="'.base_url().'public/img/cuadro4.png" >
                </td>
                <td width="20%">
                    <b>N. 4: Certificado para enseñar la operación</b>
                </td>
            </tr>
        </table>';*/

//log_message('error', 'html : '.$html);
/*$cont_row = $result2->row();
$num_row=$count->num_rows();*/
foreach ($result2 as $item) {
    //===============================================================
    $pdf->AddPage('L', 'A4'); //$pdf->writeHTML($html, true, false, true, false, ''); 
    $pdf->setPrintFooter(false);
    $img_base64_encodet=$item->tabla;
    if($item->tabla!=""){
        //$img='<img src="'.base_url().'public/graficas/'.$item->tabla . '">';
                                                          //ajustar   an - alto
        //$pdf->Image(base_url().'public/graficas/'.$item->tabla, 12, 47, 290,'', '', '', '', false, 300, '', false, false, 1, false, false, false);
        $pdf->Image(base_url().'public/graficas/'.$item->tabla, '', '', '', '', '', '', 'M', true, 150, 'C', false, false, false, true, false, true);

        /*$html1='<table width="100%" align="center"><tr><td align="center"><img src="'.base_url().'public/graficas/'.$item->tabla.'" width="920"></td></tr></table>'.$html;
        $pdf->writeHTML($html1, true, false, true, false, '');*/
    }

    /*
    $html='<table border="1" align="center">
                <tr><td><br></td></tr>
                <tr>
                    <td>'.$img.'</td>
                </tr>
            </table>';
    $pdf->writeHTML($html, true, false, true, false, '');    
    */
    //===============================================================

    //===============================================================
    $pdf->AddPage('L', 'A4'); 
    $pdf->setPrintFooter(false);
    $img_base64_encoded=$item->charbar;
    if($item->charbar!=""){
        $img2='<img src="'.base_url().'public/graficas/'.$item->charbar . '">';
        $pdf->Image(base_url().'public/graficas/'.$item->charbar, '', '', '', '', '', '', 'M', true, 150, 'C', false, false, false, true, false, true);
    }
    /*
    $html='<table border="0" align="center">
                <tr>
                    <td>'.$img2.'</td>
                </tr>
            </table>';
    $pdf->writeHTML($html, true, false, true, false, '');    
    */
    //===============================================================



    //===============================================================
    if($item->charpie!="" && $item->row==1){
        $charpie=$item->charpie;
        $img_base64_encoded_pie=$item->charpie;
    }
    /*
    $html='<table border="0" align="center">
                <tr><td><br></td></tr>
                <tr>
                    <td width="85%">'.$img3.'</td>
                </tr>
            </table>';
    $pdf->writeHTML($html, true, false, true, false, '');   
    */ 
    //===============================================================
}

    $pdf->AddPage('L', 'A4'); 
    $pdf->setPrintFooter(false);
    //log_message('error', 'charpie : '.$charpie);
    if($charpie!=""){
        $img3='<img src="'.base_url().'public/graficas/'.$charpie. '">';
        $pdf->Image(base_url().'public/graficas/'.$charpie, '', '', '', '', '', '', 'M', true, 150, 'C', false, false, false, true, false, true);
    }

    /*$pdf->AddPage('L', 'A4');
    $pdf->writeHTML($html, true, false, true, false, '');*/

$ruta=$_SERVER['DOCUMENT_ROOT'];
//$val_ruta=1; //local
$val_ruta=2; //server

if ($val_ruta == 1) //local
{   
    $url = $ruta.'snfpro/pdf_matriz_versa/matriz_'.$id_proy.'.pdf'; //local
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'snfpro/pdf_matriz_versa/matriz_'.$id_proy.'.pdf','F'); //local
    }else{
       $pdf_str = $pdf->Output($ruta.'snfpro/pdf_matriz_versa/matriz_'.$id_proy.'.pdf','F'); //local
    }
}else{
    $url = $ruta.'pdf_matriz_versa/matriz_'.$id_proy.'.pdf'; //server
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'pdf_matriz_versa/matriz_'.$id_proy.'.pdf','F'); //server
    }else{
        $pdf_str = $pdf->Output($ruta.'pdf_matriz_versa/matriz_'.$id_proy.'.pdf','F'); //server
    }
}

$pdf->endTOCPage();
$pdf->Output('EntregableLMatrizV.pdf', 'I');

?>