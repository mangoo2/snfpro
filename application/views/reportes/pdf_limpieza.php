<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $GLOBALS['logo_cli']=$logo_cli;
    foreach ($gra as $i) {
        $GLOBALS['mes']=$i->mes;
        if($i->tipo==1) //turno
            $GLOBALS['grafica1']=$i->grafica;
        if($i->tipo==2) //mes
            $GLOBALS['grafica2']=$i->grafica;
        if($i->tipo==3) //anual
            $GLOBALS['grafica3']=$i->grafica;
        /*if($i->tipo==7) //cumplidas al 100
            $GLOBALS['grafica4']=$i->grafica;*/

        if($i->tipo==4) //turno
            $GLOBALS['tabla1']=$i->grafica;
        if($i->tipo==5) //mes
            $GLOBALS['tabla2']=$i->grafica;
        if($i->tipo==6) //anual
            $GLOBALS['tabla3']=$i->grafica;

        if($i->tipo==9) //cumplimiento
            $GLOBALS['grafica5']=$i->grafica;
        if($i->tipo==10) //eficacia
            $GLOBALS['grafica6']=$i->grafica;
    }

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logofinalsys2.png';
        if($GLOBALS['logo_cli']!="")
            $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
        else
            $logo_cliente ="#";
        //$this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);

        $html = '<table width="100%">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="15%"><img src="'.$logos.'" height="90px"></td>
                <td width="65%" style="font-weight:bold; color:rgba(68, 114, 196); text-align:center; font-size:32px">SERVICIOS NUEVOS DE FILTRACIÓN HISPANOMEXICANOS S.A DE C.V.</td>
                <td width="20%" align="left"><img src="'.$logo_cliente.'" height="90px"></td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $pie = base_url().'public/img/footer.png';
        $html = '
        <table width="100%" cellpadding="2">
        <tr>
            <td width="100%"><img src="'.$pie.'"></td>
        </tr>
      </table>';
    
        //$this->writeHTMLCell(188, '', 12, 100, $html, 0, 0, 0, true, 'C', true);
        $this->Image($pie, 0, 165, 310, 50, 'PNG', '', '', true, 310, '', false, false, 0);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('SALIDA - LIMPIEZA');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10,40,10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 21);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setPrintHeader(false);
// add a page
$pdf->AddPage('L', 'A4'); 

$img_file = base_url().'public/img/portada_limp2_2.png';

                            //ancho alto
//$pdf->Image($img_file, 0, 0, 452, 530, '', '', 10, false, 310, '', false, false, 0);
$pdf->Image($img_file, 0, 0, 400, 190, 'PNG', '', '', true, 310, '', false, false, 0);

$logos = base_url().'public/img/logofinalsys2.png';
$pdf->Image($logos, 16, 13, 39,25, 'PNG', '', '', true, 150, '', false, false, false);

if($GLOBALS['logo_cli']!=""){
    $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
    $pdf->Image($logo_cliente, 239, 13, 39,25, 'JPG', '', '', true, 150, '', false, false, 0);
}

$pdf->setPrintHeader(true);

// add a page
$pdf->AddPage('L', 'A4'); 
$pdf->setPrintFooter(false);
$html="";

$img=""; $img2=""; $img3=""; $img4="";

$tabla1=$GLOBALS['tabla1'];
if($tabla1!=""){
    $img = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $tabla1) . '">';
}
$tabla2=$GLOBALS['tabla2'];
if($tabla2!=""){
    $img2 = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $tabla2) . '">';
}

$grafica1=$GLOBALS['grafica1'];
if($grafica1!=""){
    $img3 = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica1) . '">';
}
$grafica2=$GLOBALS['grafica2'];
if($grafica2!=""){
    $img4 = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica2) . '">';
}

$html.='<table border="0">
            <tr><th height="10px"></th></tr>
            <tr>
                <th width="40%">
                    '.$img.'
                </th>
                <th width="20%"></th>
                <th width="40%">
                    '.$img2.'
                </th>
            </tr>

            <tr>
                <th width="50%">
                    '.$img3.'
                </th>
                <!--<th width="20%"></th>-->
                <th width="50%">
                    '.$img4.'
                </th>
            </tr>
        </table>';
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->AddPage('L', 'A4'); 

$img5=""; $img6="";
$tabla3=$GLOBALS['tabla3'];
if($tabla3!=""){
    $img5 = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $tabla3) . '">';
}
$grafica3=$GLOBALS['grafica3'];
if($grafica3!=""){
    $img6 = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica3) . '">';
}
/*$grafica4=$GLOBALS['grafica4'];
$img7 = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica4) . '">';*/

$html="";
$html.='<table border="0">
            <tr><th height="10px"></th></tr>
            <tr>
                <th width="10%"></th>
                <th width="90%">
                    '.$img5.'
                </th>
            </tr>
            <tr>
                <th width="20%"></th>
                <th align="center" width="60%">
                    <span><b>CUMPLIMIENTO OTS ANUAL</b> </span>
                </th>
                <th width="20%"></th>
            </tr>
            <tr>
                <th width="20%"></th>
                <th width="60%">
                    '.$img6.'
                </th>
                <th width="20%">
                </th>
            </tr>';

        /*foreach ($gra as $i) {
            if($i->tipo==7){
                $img7 = '<img src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $i->grafica) . '">';
                $html.='<tr>
                    <th width="10%"> 
                    </th>
                    <th width="90%">
                        '.$img7.'
                    </th>
                </tr>';
            }
        }*/
            
    $html.='</table>';
$pdf->writeHTML($html, true, false, true, false, '');

/*$pdf->AddPage('L', 'A4'); 
$html="";
$grafica5=$GLOBALS['grafica5'];
$graf5 = '<img height="480px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica5) . '">';
$grafica6=$GLOBALS['grafica6'];
$graf6 = '<img height="500px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica6) . '">';

$html.='<table border="0">
            <!--<tr><th height="10px"></th></tr>-->
            <tr>
                <th width="54%">
                    '.$graf5.'
                </th>
                <th width="46%">
                    '.$graf6.'
                </th>
            </tr>
        </table>';
$pdf->writeHTML($html, true, false, true, false, ''); */

//construir la tabla de comentarios de actividades reprogramadas o canceladas
$html="";
$pdf->AddPage('L', 'A4'); 
$html=$table_com;
$pdf->writeHTML($html, true, false, true, false, '');


$ruta=$_SERVER['DOCUMENT_ROOT'];
//$pdf_str = $pdf->Output($ruta.'snfpro/pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'.pdf','F'); //local
//$pdf_str = $pdf->Output($ruta.'pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'.pdf','F'); //server

//falta poner la tabla de comentarios cuando sea reprogramado

//$val_ruta=1; //local
$val_ruta=2; //server

if ($val_ruta == 1) //local
{   
    $url = $ruta.'snfpro/pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //local
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'snfpro/pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }else{
        $pdf_str = $pdf->Output($ruta.'snfpro/pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }
}else{
    $url = $ruta.'pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //server
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }else{
        $pdf_str = $pdf->Output($ruta.'pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }
}
$pdf->endTOCPage();
$pdf->Output('EntregableLimpieza.pdf', 'I');
?>