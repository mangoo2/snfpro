<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    //$GLOBALS['servicio']=$servicio;
    $GLOBALS['logo_cli']=$logo_cli;

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logofinalsys2.png';
        if($GLOBALS['logo_cli']!="")
            $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
        else
            $logo_cliente ="#";
        //$this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);

        $html = '<table width="100%">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="15%"><img src="'.$logos.'" height="90px"></td>
                <td width="65%" style="font-weight:bold; color:rgba(68, 114, 196); text-align:center; font-size:32px">SERVICIOS NUEVOS DE FILTRACIÓN HISPANOMEXICANOS S.A DE C.V.</td>
                <td width="20%" align="center"><img src="'.$logo_cliente.'" height="90px"></td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $pie = base_url().'public/img/footer.png';
        $html = '
        <table width="100%" cellpadding="2">
        <tr>
            <td width="100%"><img src="'.$pie.'"></td>
        </tr>
      </table>';
    
        //$this->writeHTMLCell(188, '', 12, 100, $html, 0, 0, 0, true, 'C', true);
        $this->Image($pie, 0, 165, 310, 50, 'PNG', '', '', true, 310, '', false, false, 0);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('SALIDA - LIMPIEZA');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10,37,10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 21);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setPrintHeader(false);
// add a page
$pdf->AddPage('L', 'A4'); 

$img_file = base_url().'public/img/portada_general2_2.png';
$logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
                            //ancho alto
$pdf->Image($img_file, 0, 0, 400, 190, 'PNG', '', '', true, 310, '', false, false, 0);

$logos = base_url().'public/img/logofinalsys2.png';
$pdf->Image($logos, 16, 13, 39,25, 'PNG', '', '', true, 150, '', false, false, false);

if($GLOBALS['logo_cli']!=""){
    $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
    $pdf->Image($logo_cliente, 239, 13, 39,25, 'JPG', '', '', true, 150, '', false, false, 0);
}
$html="";
    $html.='<style>
                .titulo{
                    font-size:28px;
                }
                .subtitulo{
                    font-size:24px;   
                }
                .subtitulo2{
                    font-size:23px;   
                }
                .subtitulo3{
                    font-size:20px;   
                }
            </style>

            <table border="0" align="center">
                <tr><td></td></tr>
                <tr>
                    <td width="10%"></td>
                    <td width="80%"class="subtitulo">'.$dg_titulo.'</td>
                    <td width="10%"></td>
                </tr>
                <!--<tr>
                    <td colspan="3" ></td>
                </tr>-->
                <tr>
                    <td></td>
                    <td class="subtitulo2"><b>'.$de_dep.'</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3" ></td>
                </tr>
                <tr>
                    <td colspan="3" align="left" class="subtitulo3"><i>
                    '.$dg_des.'<br>
                    <table>
                        <tr><td>Periodo Inicial: <b>'.$fechai.'</b></td><td>Periodo Final: <b>'.$fechaf.'</b></td></tr>
                    </table>
                    Contenido:<br>
                    <ol>';
                        foreach ($resultitem->result() as $item) {
                            $html.='<li>'.$item->descripcion.'</li>';
                        }
            $html.='</ol>
                    </i>
                    </td>
                </tr>
            </table>
            
            ';

$pdf->writeHTML($html, true, false, true, false, '');

$pdf->setPrintHeader(true);

// add a page

$pdf->setPrintFooter(true);

    
foreach ($resultitem->result() as $item) {
    $pdf->AddPage('L', 'A4'); 
    $pdf->setPrintFooter(false);
    $html="";
        $img1='';
        $img2='';
        $img3='';
        if($item->file1!=''){
            $filename1 = FCPATH.'/uploads/files_ar/'.$item->file1;
            $filename1b = base_url().'/uploads/files_ar/'.$item->file1;
            if(file_exists($filename1)){
                $img1='<img src="'.$filename1b.'" >';
            }else{
                $img1=$filename1;
            }
        }
        if($item->file2!=''){
            $filename2 = FCPATH.'/uploads/files_ar/'.$item->file2;
            $filename2b = base_url().'/uploads/files_ar/'.$item->file2;
            if(file_exists($filename2)){
                $img2='<img src="'.$filename2b.'" >';
            }
        }
        if($item->file3!=''){
            $filename3 = FCPATH.'/uploads/files_ar/'.$item->file3;
            $filename3b = base_url().'/uploads/files_ar/'.$item->file3;
            if(file_exists($filename3)){
                $img3='<img src="'.$filename3b.'" >';
            }
        }
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        $pdf->Image($filename1b, '', 85, 87, 84, '', '', '', true, 150, 'L', false, false, 0, false, false, false);
        $pdf->Image($filename2b, '', 85, 87, 84, '', '', '', true, 150, 'C', false, false, 0, false, false, false);
        $pdf->Image($filename3b, '', 85, 87, 84, '', '', '', true, 150, 'R', false, false, 0, false, false, false);

        $filename1b = '';
        $filename2b = '';
        $filename3b = '';

        //$column1a=$item->titulo1;
        
        
        
    $html.='<table border="0" align="center">
                <tr>
                    <td colspan="5"></td>
                </tr>
                <tr>
                    <td colspan="5" style="font-size:18px; text-align:justify;height:110px;">'.$item->descripcion_general.'</td>
                </tr>
                <!--<tr>
                    <td colspan="5"></td>
                </tr>-->
                <tr>
                    <td width="31%" style="height:50px;">'.$item->titulo1.'</td>
                    <td width="3%"></td>
                    <td width="31%">'.$item->titulo2.'</td>
                    <td width="3%"></td>
                    <td width="31%">'.$item->titulo3.'</td>
                </tr>
                <tr>
                    <td style="vertical-align: middle;height:300px;"></td>
                    <td ></td>
                    <td style="vertical-align: middle"></td>
                    <td ></td>
                    <td style="vertical-align: middle"></td>
                </tr>
                <tr>
                    <td >'.$item->descripcion1.'</td>
                    <td ></td>
                    <td >'.$item->descripcion2.'</td>
                    <td ></td>
                    <td >'.$item->descripcion3.'</td>
                </tr>
                <tr>
                ';

                if($item->file1 != ''){
                    $html.='<td style="font-size:13px; color: #808080;">'.$item->reg_file1.'</td>
                            <td ></td>';
                }else{
                    $html.='<td ></td>
                            <td ></td>';
                }

                if($item->file2 != ''){
                    $html.='<td style="font-size:13px; color: #808080;">'.$item->reg_file2.'</td>
                            <td ></td>';
                }else{
                    $html.='<td ></td>
                            <td ></td>';
                }
                
                if($item->file3 != ''){
                    $html.='
                            <td style="font-size:13px; color: #808080;">'.$item->reg_file3.'</td>
                            <td ></td>';
                }else{
                    $html.='<td ></td>
                            <td ></td>';
                }

                $html.='</tr>
                </table>';
                
    $pdf->writeHTML($html, true, false, true, false, '');
    // writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)
    //$pdf->writeHTMLCell(86, 10, 10, 75, $column1a, 1, 0, 0, true, 'C', true);
    //$pdf->writeHTMLCell(86, 80, 10, 75, '<img src="'.$filename3b.'" height="400px">', 1, 0, 0, true, 'C', true);
    
}

$ruta=$_SERVER['DOCUMENT_ROOT'];
//$pdf_str = $pdf->Output($ruta.'snfpro/pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'.pdf','F'); //local
//$pdf_str = $pdf->Output($ruta.'pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'.pdf','F'); //server

//$val_ruta=1; //local
$val_ruta=2; //server

if ($val_ruta == 1) //local
{   
    $url = $ruta.'snfpro/pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'_'.$anio.'_'.$dpto.'.pdf'; //local
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'snfpro/pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'_'.$anio.'_'.$dpto.'.pdf','F'); //local
    }else{
       $pdf_str = $pdf->Output($ruta.'snfpro/pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'_'.$anio.'_'.$dpto.'.pdf','F'); //local
    }
}else{
    $url = $ruta.'pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'_'.$anio.'_'.$dpto.'.pdf'; //server
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'_'.$anio.'_'.$dpto.'.pdf','F'); //server
    }else{
        $pdf_str = $pdf->Output($ruta.'pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'_'.$anio.'_'.$dpto.'.pdf','F'); //server
    }
}

$pdf->endTOCPage();
$pdf->Output('EntregableLimpieza.pdf', 'I');

?>