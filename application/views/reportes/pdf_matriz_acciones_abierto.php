<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    $GLOBALS['logo_cli']=$logo_cli;
//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logofinalsys2.png';
        if($GLOBALS['logo_cli']!="")
            $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
        else
            $logo_cliente ="#";
        //$this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);

        $html = '<table width="100%">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="15%"><img src="'.$logos.'" height="90px"></td>
                <td width="65%" style="font-weight:bold; color:rgba(68, 114, 196); text-align:center; font-size:32px">SERVICIOS NUEVOS DE FILTRACIÓN HISPANOMEXICANOS S.A DE C.V.</td>
                <td width="20%" align="center"><img src="'.$logo_cliente.'" height="90px"></td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $pie = base_url().'public/img/footer.png';
        $html = '
        <table width="100%" cellpadding="2">
        <tr>
            <td width="100%"><img src="'.$pie.'"></td>
        </tr>
      </table>';
    
        //$this->writeHTMLCell(188, '', 12, 100, $html, 0, 0, 0, true, 'C', true);
        $this->Image($pie, 0, 165, 310, 50, 'PNG', '', '', true, 310, '', false, false, 0);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('SALIDA - MATRIZ DE HALLAZGOS ABIERTOS');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10,37,10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 21);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setPrintHeader(false);
// add a page
$pdf->AddPage('L', 'A4'); 

$img_file = base_url().'public/img/portada_mat_acc.png';

                            //ancho alto
//$pdf->Image($img_file, 0, 0, 452, 530, '', '', 10, false, 310, '', false, false, 0);
$pdf->Image($img_file, 0, 0, 400, 190, 'PNG', '', '', true, 310, '', false, false, 0);

$logos = base_url().'public/img/logofinalsys2.png';
$pdf->Image($logos, 16, 13, 39,25, 'PNG', '', '', true, 150, '', false, false, false);

if($GLOBALS['logo_cli']!=""){
    $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
    $pdf->Image($logo_cliente, 239, 13, 39,25, 'JPG', '', '', true, 150, '', false, false, 0);
}
$pdf->setPrintHeader(true);

// add a page
$pdf->AddPage('L', 'A4'); 
$pdf->setPrintFooter(false);
$html="";
$html_abierto=""; $cols=6; $td_durante="";
foreach ($res as $r) { //poner tamaño mas pequeño a las fotos de evidencia
    $img_antes=""; $img_durante=""; $img_desps="";
    if($r->avance==0){
        
        if($r->antes!=""){
            $img_antes='<img src="'.base_url().'uploads/evidencias/'.$r->antes.'" class="rounded mr-3 img-fluid" height="100" width="100">';
        }
        if($r->durante!=""){
            $cols=7;
            $img_durante='<img src="'.base_url().'uploads/evidencias/'.$r->durante.'" class="rounded mr-3 img-fluid" height="100" width="100">';
            $td_durante='<td><p class="pspaces"></p>'.$img_durante.'</td>';
        }
        if($r->despues!=""){
            $img_desps='<img src="'.base_url().'uploads/evidencias/'.$r->despues.'" class="rounded mr-3 img-fluid" height="100" width="100">';
        }
        $html_abierto.='<tr>
            <td><p class="pspaces"></p>'.$r->area.'</td>
            <td><p class="pspaces"></p>'.$r->hallazgo.'</td>
            <td><p class="pspaces"></p>'.$img_antes.'</td>
            <td><p class="pspaces"></p>'.$r->acciones.'</td>
            '.$td_durante.'
            <td><p class="pspaces"></p>'.$r->fecha_cierre.'</td>
            <td><p class="pspaces"></p>'.$img_desps.'</td>
        </tr>';
    }
} 

$html.='<style type="text/css">
    .table{
        text-align: center;
        vertical-align: middle;
        font-size:11px;
    }
    .table_th{
        background-color:#00B0F0;
        color: #FFFFFF;
        text-align: center;
        vertical-align: middle;
        font-weight: bold;
    }
    .pspaces{ font-size: 0.1px; }
</style>

    <table>
        <tr>
            <th><br></th>
        </tr>
    </table>
    <table class="table" border="1">
        <thead>
            <tr class="table_th"><th colspan="'.$cols.'">MATRIZ DE ACCIONES ABIERTAS</th></tr>
            <tr class="table_th">
                <th>ÁREA</th>
                <th>HALLAZGO</th>
                <th>EVIDENCIA ANTES</th>
                <th>ACCIONES REQUERIDAS</th>';
                if($cols>6){
                    $html.='<th>EVIDENCIA DURANTE</th>';
                }
                $html.='<th>FECHA DE CIERRE</th>
                <th>EVIDENCIA DESPUÉS</th>
            </tr>
        </thead>
        <tbody>
            '.$html_abierto.'
        </tbody>
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');

$ruta=$_SERVER['DOCUMENT_ROOT'];

//$val_ruta=1; //local
$val_ruta=2; //server

if ($val_ruta == 1) //local
{   
    $url = $ruta.'snfpro/pdf_matriz_acciones/matriz_abierto_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //local
    if(file_exists($url)){
        unlink($url);
        //log_message('error', 'elimina a  : '.$url);
        $pdf_str = $pdf->Output($ruta.'snfpro/pdf_matriz_acciones/matriz_abierto_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }else{
       $pdf_str = $pdf->Output($ruta.'snfpro/pdf_matriz_acciones/matriz_abierto_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local 
    }
}else{
    $url = $ruta.'pdf_matriz_acciones/matriz_abierto_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //server
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'pdf_matriz_acciones/matriz_abierto_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }else{
        $pdf_str = $pdf->Output($ruta.'pdf_matriz_acciones/matriz_abierto_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }
}

?>