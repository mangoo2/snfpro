<style type="text/css">
  .btn_background_naranja {
    background-color: #cf3117;
  }
  .btn_color {
      cursor: pointer;
      border-radius: 24px;
      padding: 22px;
      padding-bottom: 3px;
      margin: 5px;
  }
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>carrusel/style.css">
<input type="hidden" id="idproyecto" value="<?php echo $idproyecto ?>">
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Servicio de Limpieza</h3>
      </div>
      <div class="col-sm-6" align="right">
        <a onclick="javascript:history.back()" class="btn btn-light">Regresar</a>
      </div>  
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <label class="col-form-label">Actividades realizadas</label>
              <select class="form-control" id="iddepartamento" onchange="get_img()">
                <option selected disabled>Seleccionar departamento</option>
                <?php foreach ($resultdepa->result() as $d) { ?>
                  <option value="<?php echo $d->id ?>"><?php echo $d->departamento ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <!-- -->
              <br><br><br><br><br>
              <div class="carrusel_text">
              </div>
              <!-- -->
            </div>
          </div>
           
            

        </div>
      </div>
    </div>
  </div>
</div>



<style type="text/css">
  .slider {
  position: relative;
  width: 100%;
  max-width: 100%;
}
.slider .slider__inner {
  display: flex;
  position: relative;
  align-items: center;
  /*max-width: 750px;*/
  max-width: 950px;
  height: 400px;
  margin: 0 auto;
}
.slider .slider__inner .slider__item {
  position: absolute;
  height: 150px;
  width: 150px;
  opacity: 0;
  transition: all 0.3s ease-in-out;
  z-index: -1;
  pointer-events: none;
  user-select: none;
}
.slider .slider__inner .slider__item-trigger-next {
  left: 15%;
  transform: translateX(-50%);
}
.slider .slider__inner .slider__item-trigger-previous {
  left: 85%;
  transform: translateX(-50%);
}
.slider .slider__inner .slider__item-selected {
  box-shadow: 0 0 30px rgba(255, 255, 255, 0.6), 0 0 60px rgba(255, 255, 255, 0.45), 0 0 110px rgba(255, 255, 255, 0.25), 0 0 100px rgba(255, 255, 255, 0.1);
  height: 300px;
  opacity: 1;
  left: 50%;
  transform: translateX(-50%);
  width: 300px;
  z-index: 2;
}
.slider .slider__inner .slider__item-previous, .slider .slider__inner .slider__item-next {
  height: 200px;
  opacity: 1;
  width: 200px;
  z-index: 1;
}
.slider .slider__inner .slider__item-last, .slider .slider__inner .slider__item-first {
  opacity: .4;
  z-index: 0;
}
.slider .slider__inner .slider__item-previous {
  left: 30%;
  transform: translateX(-50%);
}
.slider .slider__inner .slider__item-next {
  left: 70%;
  transform: translateX(-50%);
}
.slider .slider__inner .slider__item-first {
  left: 15%;
  transform: translateX(-50%);
}
.slider .slider__inner .slider__item-last {
  left: 85%;
  transform: translateX(-50%);
}
.slider .slider__inner .slider__item-container {
  position: relative;
  width: 100%;
  height: 100%;
}
.slider .slider__inner .slider__item-img {
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: center;
}
.slider .slider__inner .slider__item-datas {
  position: absolute;
  /*bottom: 0;*/
  top:-75px;
  width: 100%;
  padding: .5rem 0;
  background-color: rgba(255, 255, 255);
  text-align: center;
  font-family: 'Arial';
  /*white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;*/
  color: #0231cf;
  font-weight: bold;
  font-size: 20px;
}
.slider .slider__controls {
  display: flex;
  position: absolute;
  left: 0;
  right: 0;
  width: 100%;
  justify-content: space-between;
  align-items: center;
}
.slider .slider__controls-previous, .slider .slider__controls-next {
  border: 0;
  cursor: pointer;
  font-size: 50px;
  background-color: transparent;
  color: rgba(0, 0, 0, 0.75);
  padding: 0;
}
.slider .slider__controls-previous:focus, .slider .slider__controls-next:focus {
  outline: none;
}
</style>