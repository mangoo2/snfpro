
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<style type="text/css">
    .table_thead{
        background-color:#00B0F0;
        color: #FFFFFF !important;
        text-align: center;
        vertical-align: middle;
    }
    #canvastg2,#canvastg2_2,#canvastg3{
        display: none;
    }

</style>

<div class="container-fluid">
    <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3>Resultados de monitoreo de actividades</h3>
          </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <input type="hidden" id="id_proy" value="<?php echo $id_proy; ?>">
        <input type="hidden" id="cant_dia" value="0">
        <input type="hidden" id="mes" value="<?php echo $mes; ?>">
        <input type="hidden" id="anio" value="<?php echo $anio; ?>">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <table>
                                        <tr>
                                            <th width="20%"><img class="img-fluid2" height="120px" src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                            <th width="40%"><h3>CHECK LIST FIN DE SEMANA</h3></th>
                                            <th width="20%"><img align="right" class="img-fluid2" height="120px" id="logo_cli" src="<?php echo base_url();?>uploads/clientes/<?php echo $logo; ?>"></th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <table class="table" border="1" width="100%">
                                        <tr>
                                            <th width="30%" style="background-color:#00CC00;"></th>
                                            <th width="70%">Porcentaje optimo 90 a 100%</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FFFF00;"></th>
                                            <th>Porcentaje aceptable del 80 al 89 %</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FF0000;"></th>
                                            <th>79 % o menos NO ACEPTABLE</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Mes: </label>
                                    <select class="form-control round" id="mes_table" disabled>
                                        <option <?php if($mes==1) echo "selected"; ?> value="01">Enero</option>
                                        <option <?php if($mes==2) echo "selected"; ?> value="02">Febrero</option>
                                        <option <?php if($mes==3) echo "selected"; ?> value="03">Marzo</option>
                                        <option <?php if($mes==4) echo "selected"; ?> value="04">Abril</option>
                                        <option <?php if($mes==5) echo "selected"; ?> value="05">Mayo</option>
                                        <option <?php if($mes==6) echo "selected"; ?> value="06">Junio</option>
                                        <option <?php if($mes==7) echo "selected"; ?> value="07">Julio</option>
                                        <option <?php if($mes==8) echo "selected"; ?> value="08">Agosto</option>
                                        <option <?php if($mes==9) echo "selected"; ?> value="09">Septiembre</option>
                                        <option <?php if($mes==10) echo "selected"; ?> value="10">Octubre</option>
                                        <option <?php if($mes==11) echo "selected"; ?> value="11">Noviembre</option>
                                        <option <?php if($mes==12) echo "selected"; ?> value="12">Diciembre</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <button id="export_excel1" type="button" class="btn btn-success">Exportar a Excel <i class="fa fa-file-excel-o"></i></button>
                                <table style="text-align:center;" class="table" border="1" width="100%" id="getMonitoreoFin">
                                  <thead class="table_thead" id="tble_head">
                                    <tr>
                                        <th>ZONA</th>
                                        <th>INSPECCIÓN</th>
                                        <th>DESCRIPCIÓN DE LA ACTIVIDAD</th>
                                        <th>FRECUENCIA</th>
                                        <th>D</th>
                                        <th>D</th>
                                        <th>D</th>
                                        <th>D</th>
                                        <th>ÁREA DE OPORTUNIDAD </th>
                                    </tr>
                                  </thead>
                                  <tbody id="body_table">

                                  </tbody>
                                  <tfoot id="tfoot">

                                  </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12"><br><br></div>
                    </div>
                    <div class="row">
                        <div class="col-12"></div>
                        <div class="col-md-6">
                            <center><span><b>ÁREAS DE OPORTUNIDAD</b> </span></center>
                            <div class="card-body chart-block">
                                <canvas id="graph"></canvas>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-sm-12 box-col-12" id="cont_graph2">
                                <div class="card-body chart-block">
                                    <div class="chart-overflow" id="graph2"></div>
                                    <!--<canvas id="graph2"></canvas>-->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"><br></div>
                        
                        <div class="col-md-10">
                            <div class="card-body chart-block">
                                <canvas id="graph_inspecc"></canvas>
                            </div>
                        </div>
                        
                    </div>

                    <div class="col-md-12">
                        <hr>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <table>
                                        <tr>
                                            <th width="20%"><img class="img-fluid2" height="120px" src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                            <th width="40%"><h3>CHECK LIST DIARIO</h3></th>
                                            <th width="20%"><img align="right" class="img-fluid2" height="120px" id="logo_cli" src="<?php echo base_url();?>uploads/clientes/<?php echo $logo; ?>"></th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <table class="table" border="1" width="100%">
                                        <tr>
                                            <th width="30%" style="background-color:#00CC00;"></th>
                                            <th width="70%">Porcentaje optimo 90 a 100%</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FFFF00;"></th>
                                            <th>Porcentaje aceptable del 80 al 89 %</th>
                                        </tr>
                                        <tr>
                                            <th style="background-color:#FF0000;"></th>
                                            <th>79 % o menos NO ACEPTABLE</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <button id="export_excel2" type="button" class="btn btn-success">Exportar a Excel <i class="fa fa-file-excel-o"></i></button><br>
                        <div class="table-responsive">
                            <br>
                            <table style="text-align:center;" class="table" border="1" width="100%" id="getMonitoreoDiario">
                              <thead class="table_thead" id="tble_head_dia">
                                <tr>
                                    <th>ZONA</th>
                                    <th>INSPECCIÓN</th>
                                    <th>DESCRIPCIÓN DE LA ACTIVIDAD</th>
                                    <th>FRECUENCIA</th>
                                    <th>D</th>
                                    <th>D</th>
                                    <th>D</th>
                                    <th>D</th>
                                    <th>ÁREA DE OPORTUNIDAD </th>
                                </tr>
                              </thead>
                              <tbody id="body_table_dia">

                              </tbody>
                              <tfoot id="tfoot_dia">

                              </tfoot>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br><br></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"></div>
                            <div class="col-md-12">
                                <center><span><b>CUMPLIMIENTO</b> </span></center>
                                <div class="card-body chart-block">
                                    <canvas id="graph_diario"></canvas>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive" style="display: none;">
                            <input type="hidden" id="total_dias" value="0">
                            <table style="text-align:center;" class="table" border="1" width="100%" id="getMonitoreoCompleta">
                              <thead class="table_thead" id="tble_head_diacomp">
                                <tr>
                                    <th>ZONA</th>
                                    <th>INSPECCIÓN</th>
                                    <th>DESCRIPCIÓN DE LA ACTIVIDAD</th>
                                    <th>FRECUENCIA</th>
                                    <th>D</th>
                                    <th>D</th>
                                    <th>D</th>
                                    <th>D</th>
                                    <th>ÁREA DE OPORTUNIDAD </th>
                                </tr>
                              </thead>
                              <tbody id="body_table_diacomp">

                              </tbody>
                              <tfoot id="tfoot_diacomp">

                              </tfoot>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br><br></div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-12"></div>
                            <div class="col-md-12">
                                <center><span><b>PORCENTAJE DE ACTIVIDAD POR DÍA</b> </span></center>
                                <div class="card-body chart-block" id="cont_completa">
                                    <canvas id="graph_completa"></canvas>
                                </div>
                            </div>
                            <!--<div class="col-sm-12 box-col-12" id="cont_graph3">
                                <div class="card-body chart-block">
                                    <div class="chart-overflow" id="graph_completa"></div>
                                </div>
                            </div>-->
                        </div>

                        <!--<div class="row">
                            <div class="col-12"></div>
                            <div class="col-12">
                                <center><span><b>TOTAL DE ACTIVIDADES POR MES</b> </span></center>
                                <canvas id="graph_act_mes"></canvas>
                                
                            </div>
                        </div>-->

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    foreach ($act as $a) {
      ${'tot_act_'.$a->id_limpieza}=$a->tot_act;
      ${'nombre_'.$a->id_limpieza}=$a->nombre;
      //echo "tot_act_ : ".${'tot_act_'.$a->id_limpieza};
      //echo "nombre_ : ".${'nombre_'.$a->id_limpieza};
    }

?>
<script type="text/javascript">
    $(document).ready(function() {
        /*setTimeout(function(){
            new Chart2(document.getElementById("graph_act_mes"), {
                type: 'bar',
                data: {
                    labels: [<?php foreach ($act as $h) {echo "'${'nombre_'.$h->id_limpieza}',";} ?>],
                    datasets: [
                    {
                        type: 'bar',
                        label: ["Total "],
                        backgroundColor: "rgba(0, 176, 240, 0.7)",
                        borderColor: 'rgba(0, 176, 240, 1)',
                        borderWidth: 2,
                        data: [<?php foreach ($act as $h) {echo ${'tot_act_'.$h->id_limpieza}.',';} ?>],
                        fill: false,
                    }
                  ]
                },
                options: {
                    legend: { 
                        display: true 
                    },
                    title: {
                        display: true,
                        text: 'HEAD COUNT'
                    },
                    scales: {
                        xAxes: [{
                            barPercentage: 1,
                            categoryPercentage: 0.6,
                            ticks: {
                              autoSkip: false,
                              rotation: -90,
                              barPercentage: 1
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stepSize: 1
                            }
                        }],
                    },
                    //Enabling 3D Chart
                    enable3D: true, 
                    enableRotation: true,     
                    depth: 100,
                    wallSize: 2,
                    tilt: 0,
                    rotation: 14,
                    perspectiveAngle: 20,
                    sideBySideSeriesPlacement: true,
                    load: "loadTheme",
                    title: { text: '' },            
                    isResponsive: true,
                    size: { height: '450'},
                    legend: { visible: true  },
                    plugins: {
                        datalabels: {
                            formatter: (value) => {
                                return value;
                            },
                            color: '#fff',
                            font: {
                              family: '"Times New Roman"',
                              size: "18",
                              weight: "bold",
                            },
                        },
                    },

                }
            });
        }, 2500);*/
            
    });
</script>