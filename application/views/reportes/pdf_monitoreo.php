<?php
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

    $GLOBALS['grafica']="";
    $GLOBALS['grafica2']="";
    $GLOBALS['grafica3']="";
    $GLOBALS['logo_cli']=$logo_cli;
    /*foreach ($gra as $i) {
        if($i->tipo==1)
            $GLOBALS['grafica']=$i->grafica;
        if($i->tipo==2)
            $GLOBALS['grafica2']=$i->grafica;
        if($i->tipo==3)
            $GLOBALS['grafica3']=$i->grafica;
    }*/

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
    public function Header() {
        $logos = base_url().'public/img/logofinalsys2.png';
        if($GLOBALS['logo_cli']!="")
            $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
        else
            $logo_cliente ="#";
        //$this->Image($logos, 10, 6, 50, '', '', '', '', true, 150, '', false, false, 0, false, false, false);

        $html = '<table width="100%">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td width="15%"><img src="'.$logos.'" height="90px"></td>
                <td width="65%" style="font-weight:bold; color:rgba(68, 114, 196); text-align:center; font-size:32px">SERVICIOS NUEVOS DE FILTRACIÓN HISPANOMEXICANOS S.A DE C.V.</td>
                <td width="20%" align="center"><img src="'.$logo_cliente.'" height="90px"></td>
            </tr>
            </table>';
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $pie = base_url().'public/img/footer.png';
        $html = '
        <table width="100%" cellpadding="2">
        <tr>
            <td width="100%"><img src="'.$pie.'"></td>
        </tr>
      </table>';
    
        //$this->writeHTMLCell(188, '', 12, 100, $html, 0, 0, 0, true, 'C', true);
        $this->Image($pie, 0, 165, 310, 50, 'PNG', '', '', true, 310, '', false, false, 0);
    }
} 

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(311,396), true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Entregable');
$pdf->SetTitle('Entregable');
$pdf->SetSubject('Entregable');
$pdf->SetKeywords('SALIDA - MONITOREO DE ÁREAS');
$pdf->setPrintFooter(true);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10,37,10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(30);
$pdf->SetFooterMargin(26);

// set auto page breaks
$pdf->SetAutoPageBreak(true, 21);
//$pdf->SetAutoPageBreak(true, 25);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setPrintHeader(false);
// add a page
$pdf->AddPage('L', 'A4'); 

$img_file = base_url().'public/img/portada_monitor.png';

                            //ancho alto
//$pdf->Image($img_file, 0, 0, 452, 530, '', '', 10, false, 310, '', false, false, 0);
$pdf->Image($img_file, 0, 0, 400, 190, 'PNG', '', '', true, 310, '', false, false, 0);

$logos = base_url().'public/img/logofinalsys2.png';
$pdf->Image($logos, 16, 13, 39,25, 'PNG', '', '', true, 150, '', false, false, false);

if($GLOBALS['logo_cli']!=""){
    $logo_cliente = base_url().'uploads/clientes/'.$GLOBALS['logo_cli'];
    $pdf->Image($logo_cliente, 239, 13, 39,25, 'JPG', '', '', true, 150, '', false, false, 0);
}
$pdf->setPrintHeader(true);


$img=""; $img2=""; $img3="";
/*$grafica=$GLOBALS['grafica'];
if($grafica!=""){
    //$img = '<img width="740px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica) . '">';
    $img='<img align="center" width="560px" src="'.base_url()."pdf_monitoreo/".$grafica.'">';
}

$grafica2=$GLOBALS['grafica2'];
if($grafica2!=""){
    //$img2 = '<img width="740px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica2) . '">';
    $img2='<img align="center" width="820px" src="'.base_url()."pdf_monitoreo/".$grafica2.'">';
}

$grafica3=$GLOBALS['grafica3'];
if($grafica3!=""){
    //$img2 = '<img width="740px" src="@' . preg_replace('#^data:image/[^;]+;base64,#', '', $grafica2) . '">';
    $img3='<img align="center" width="820px" src="'.base_url()."pdf_monitoreo/".$grafica3.'">';
}*/

$html="";
foreach ($gra as $i) {
    if($i->tipo==1 || $i->tipo==2){
        // add a page
        $pdf->AddPage('L', 'A4'); 
        $pdf->setPrintFooter(false);
        $img="";
        if($i->grafica!="" && file_exists(FCPATH."/pdf_monitoreo/".$i->grafica)){
            $img='<img align="center" width="640px" src="'.base_url()."pdf_monitoreo/".$i->grafica.'">';
        }
        $html='
            <table>
                <tr>
                    <th><br></th>
                </tr>
            </table>
            <table>
                <tr>
                    <th align="center" width="100%"><b>EFICIENCIA DE LIMPIEZA '.mb_strtoupper($name_mes,"UTF-8").' (FIN DE SEMANA)</b></th>
                </tr>
                <tr>
                    <th width="20%"></th>
                    <th style="font-size:9px; color:#B8BCB6;" width="80%">% Cumplimiento</th>
                </tr>
                <tr>
                    <th align="center" width="100%">'.$img.'</th>
                </tr>
            </table>';
        $pdf->writeHTML($html, true, false, true, false, '');
    }   
}

$html="";
foreach ($gra as $i) {
    if($i->tipo==3){
        $wdh='width="640px"';
    }if($i->tipo==4){
        $wdh='width="770px"';
    }
    if($i->tipo==3 || $i->tipo==4){
        // add a page
        $pdf->AddPage('L', 'A4'); 
        $pdf->setPrintFooter(false);
        $img2="";
        if($i->grafica!="" && file_exists(FCPATH."/pdf_monitoreo/".$i->grafica)){
            $img2='<img align="center" '.$wdh.' src="'.base_url()."pdf_monitoreo/".$i->grafica.'">';
        }
        $html='
            <table>
                <tr>
                    <th><br></th>
                </tr>
            </table>
            <table>
                <tr>
                    <th align="center" width="100%"><b>EFICIENCIA DE LIMPIEZA '.mb_strtoupper($name_mes,"UTF-8").' (DIARIO)</b></th>
                </tr>
                <tr>
                    <th width="20%"></th>
                    <th style="font-size:9px; color:#B8BCB6;" width="80%">% Cumplimiento</th>
                </tr>
                <tr>
                    <th align="center" width="100%">'.$img2.'</th>
                </tr>
            </table>';
        $pdf->writeHTML($html, true, false, true, false, '');
    }   
}

$html="";
foreach ($gra_mes as $i) {
    $img5=""; 
    if($i->tipo==5){
        if($i->grafica!="" && file_exists(FCPATH."/pdf_monitoreo/".$i->grafica)){
            $img5='<img align="center" width="810px" src="'.base_url()."pdf_monitoreo/".$i->grafica.'">';
        }
    }     
}

$pdf->AddPage('L', 'A4'); 
$pdf->setPrintFooter(false);
$html="";
$html.='
    <table>
        <tr>
            <th><br></th>
        </tr>
    </table>
    <table>
        <tr>
            <th align="center" width="100%"><b>TOTAL DE ACTIVIDADES POR MES '.mb_strtoupper($name_mes,"UTF-8").'</b></th>
        </tr>
        <tr>
            <th width="20%"></th>
            <th style="font-size:9px; color:#B8BCB6;" width="80%">% Cumplimiento</th>
        </tr>
        <tr>
            <th align="center" width="100%">'.$img5.'</th>
        </tr>
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');

//log_message('error', 'html : '.$html);
$ruta=$_SERVER['DOCUMENT_ROOT'];

//$val_ruta=1; //local
$val_ruta=2; //server

if ($val_ruta == 1) //local
{   
    $url = $ruta.'snfpro/pdf_monitoreo/monitoreo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //local
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'snfpro/pdf_monitoreo/monitoreo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }else{
       $pdf_str = $pdf->Output($ruta.'snfpro/pdf_monitoreo/monitoreo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //local
    }
}else{
    $url = $ruta.'pdf_monitoreo/monitoreo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf'; //server
    if(file_exists($url)){
        unlink($url);
        $pdf_str = $pdf->Output($ruta.'pdf_monitoreo/monitoreo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }else{
        $pdf_str = $pdf->Output($ruta.'pdf_monitoreo/monitoreo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf','F'); //server
    }
}

$pdf->endTOCPage();
$pdf->Output('EntregableMonitoreo.pdf', 'I');

?>