<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--<script src="<?php echo base_url(); ?>assets/js/chart/Chart.bundle.min.js"></script>-->
<script src="<?php echo base_url(); ?>plugins/chartjs-plugin/2.7.2/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/chartjs-plugin/chartjs-plugin-datalabels.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<style type="text/css">
    .table_thead{background-color:#00B0F0;color: #FFFFFF !important;text-align: center;vertical-align: middle;}
    .table{text-align: center;}
    .ft8{ font-size:10px;}
    #canvastg2,#canvastg2_2,#canvastg2_3,#canvastg3,#canvastg4{display: none;}
    .img-fluid2{
            height: 120px;
        }
    @media(max-width: 1164px){
        .tablec4 td,.tablec4 th{
            padding: 4px !important;
        }
    }
    @media(max-width: 760px){
        #result_gral td,#result_gral th,
        #result_bajas td,#result_bajas th,
        #result_head td, #result_head th,
        .table_puestos_div td, .table_puestos_div th,
        #table_presentismo_data td,#table_presentismo_data th,
        .table_generar_fz td, .table_generar_fz th{
            font-size: 7.5px;
        }
        .img-fluid2{
            height: 100px;
        }
        .h_titulo{
            font-size: 17px;
        }
        .graph_head_div{
            padding: 0px;
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .container-fluid{
            padding-left: 1px !important;
            padding-right: 1px !important;
        }
        .graph_g_div{
            padding-left: 1px !important;
            padding-right: 1px !important;
        }
        .cont_flui{
            padding-right: 0px !important;
        }
        .carbod{
            padding-left: 5px !important;
            padding-right: 5px !important;
        }
    }
    @media(max-width: 550px){
        .img-fluid2{
            height: 80px;
        }
        .graph_div{
            width: 650px;
        }
    }
    @media(max-width: 550px){
        .img-fluid2{
            height: 80px;
        }
    }
    
    
</style>
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3>Resultados de presentismo y rotación</h3>
          </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <input type="hidden" id="id_proy" value="<?php echo $id_proy; ?>">
        <input type="hidden" id="mes" value="<?php echo $mes; ?>">
        <input type="hidden" id="anio" value="<?php echo $anio; ?>">
        <div class="col-sm-12 cont_flui">
            <div class="card">
                <div class="card-body carbod">
                    <div class="row">
                        <div class="col-md-4">
                            <table>
                                <tr>
                                    <th width="30%"><br><br></th>
                                    <th width="70%"><br><br></th>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <table>
                                <tr>
                                    <th width="20%"><img class="img-fluid2"  src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                    <th width="40%"><h3 class="h_titulo">LISTADO DE REGISTRO DE INCIDENCIAS</h3></th>
                                    <th width="10%"></th>
                                    <th width="30%"><img align="right" class="img-fluid2"  src="<?php echo base_url();?>uploads/clientes/<?php echo $foto; ?>"></th>
                                </tr>
                            </table>
                            <div class="table-responsive">
                                <table class="table" border="1" width="100%" id="result_gral">
                                    <thead class="table_thead">
                                        <tr>
                                            <th>ITEM</th>
                                            <th>NOMBRE</th>
                                            <th>FALTAS</th>
                                            <th>PERMISOS</th>
                                            <th>RETARDOS</th>
                                            <th>INCAPACIDAD</th>
                                            <th>VACACIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            //echo $html;
                                            $i=0; //$anio="";
                                            foreach ($res as $r) {
                                                $i++;
                                                //$anio=$r->anio;
                                                if($r->tot_faltas>0 || $r->tot_perm>0 || $r->tot_ret>0 || $r->tot_inca>0 || $r->tot_vaca>0){
                                                    ${'tot_faltas'.$r->id}=$r->tot_faltas;
                                                    ${'tot_perm'.$r->id}=$r->tot_perm;
                                                    ${'tot_ret'.$r->id}=$r->tot_ret;
                                                    ${'tot_inca'.$r->id}=$r->tot_inca;
                                                    ${'tot_vaca'.$r->id}=$r->tot_vaca;
                                                    echo '<tr>
                                                      <td>'.$i.'</td>
                                                      <td>'.$r->nombre.'</td>
                                                      <td>'.$r->tot_faltas.'</td>
                                                      <td>'.$r->tot_perm.'</td>
                                                      <td>'.$r->tot_ret.'</td>
                                                      <td>'.$r->tot_inca.'</td>
                                                      <td>'.$r->tot_vaca.'</td>
                                                  </tr>';
                                                }
                                            }

                                        ?>
                                    </tbody>
                                </table>
                             </div>
                        </div>

                        <div class="col-md-12" style="overflow: auto;">
                            <div class="graph_div">
                                <canvas id="graph" width="600" height="450"></canvas>
                                <div></div>
                            <!--<div class="chart-overflow" id="graph"></div>-->
                            </div>
                        </div>
                        <!--<div class="col-md-1"></div>-->
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <table>
                                <tr>
                                    <th width="30%"><br><br></th>
                                    <th width="70%"><br><br></th>
                                </tr>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <th width="30%"><img class="img-fluid2"  src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                        <th width="70%"><h3 class="h_titulo">LISTADO DE BAJAS</h3></th>
                                    </tr>
                                </table>
                                <div class="table-responsive">
                                    <table class="table" border="1" width="100%" id="result_bajas">
                                        <thead class="table_thead">
                                            <tr>
                                                <th>ITEM</th>
                                                <th>SEMANA</th>
                                                <th>NOMBRE</th>
                                                <th>CAUSA</th>
                                                <th>JEFE INMEDIATO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $i=0; $causa="";
                                                foreach ($bajas as $b) {
                                                    if($b->causa=="1")
                                                        $causa="FALTA DE DICIPLINA";
                                                    else if($b->causa=="2")
                                                        $causa="OTRO EMPLEO";
                                                    else if($b->causa=="3")
                                                        $causa="PROBLEMAS DE SALUD";
                                                    else if($b->causa=="4")
                                                        $causa="PROBLEMAS FAMILIARES";
                                                    else if($b->causa=="5")
                                                        $causa="OTRO";
                                                    $i++;
                                                    echo '<tr>
                                                      <td>'.$i.'</td>
                                                      <td>'.$b->semana.'</td>
                                                      <td>'.$b->nombre.'</td>
                                                      <td>'.$causa.'</td>
                                                      <td></td>
                                                  </tr>';
                                                }

                                            ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <th width="30%"><img class="img-fluid2"  src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                        <th width="70%"><h3 class="h_titulo">% DE CAUSA DE BAJAS</h3></th>
                                    </tr>
                                </table>
                                <div class="table-responsive">
                                    <table class="table" border="1" width="100%" id="result_bajas">
                                        <thead class="table_thead">
                                            <tr>
                                                <th>CAUSAS</th>
                                                <th>% DE PERSONAL</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $j=0; $causa=""; $causa2=""; $causa3=""; $causa4=""; $causa5=""; 
                                                $porcentaje=0; $porcentaje2=0; $porcentaje3=0; $porcentaje4=0; $porcentaje5=0;
                                                foreach ($bajas as $b) {
                                                    $j++;
                                                    if($b->causa=="1"){
                                                        $porcentaje++;
                                                        ${'causa_'.$b->causa}="FALTA DE DICIPLINA";
                                                        $causa="<td>FALTA DE DICIPLINA</td>";
                                                        //${'porcentaje_'.$b->id} = intval($porcentaje)*100/$j;
                                                    }
                                                    else if($b->causa=="2"){
                                                        $porcentaje2++;
                                                        ${'causa_'.$b->causa}="OTRO EMPLEO";
                                                        $causa2="<td>OTRO EMPLEO</td>";
                                                        //${'porcentaje_'.$b->id} = intval($porcentaje2)*100/$j;
                                                    }
                                                    else if($b->causa=="3"){
                                                        $porcentaje3++;
                                                        ${'causa_'.$b->causa}="PROBLEMAS DE SALUD";
                                                        $causa3="<td>PROBLEMAS DE SALUD</td>";
                                                        //${'porcentaje_'.$b->id} = intval($porcentaje3)*100/$j;
                                                    }
                                                    else if($b->causa=="4"){
                                                        $porcentaje4++;
                                                        ${'causa_'.$b->causa}="PROBLEMAS FAMILIARES";
                                                        $causa4="<td>PROBLEMAS FAMILIARES</td>";
                                                        //${'porcentaje_'.$b->id} = intval($porcentaje4)*100/$j;
                                                    }
                                                    else if($b->causa=="5"){
                                                        $porcentaje5++;
                                                        ${'causa_'.$b->causa}="OTRO";
                                                        $causa5="<td>OTRO</td>";
                                                        //${'porcentaje_'.$b->id} = intval($porcentaje5)*100/$j;
                                                    }
                                                } 
                                                foreach ($bajas as $b) {
                                                    if($b->causa=="1"){
                                                        ${'porcentaje_'.$b->causa} = round(intval($porcentaje)*100/$j,2);
                                                    }
                                                    else if($b->causa=="2"){
                                                        ${'porcentaje_'.$b->causa} = round(intval($porcentaje2)*100/$j,2);
                                                    }
                                                    else if($b->causa=="3"){
                                                        ${'porcentaje_'.$b->causa} = round(intval($porcentaje3)*100/$j,2);
                                                    }
                                                    else if($b->causa=="4"){
                                                        ${'porcentaje_'.$b->causa} = round(intval($porcentaje4)*100/$j,2);
                                                    }
                                                    else if($b->causa=="5"){
                                                        ${'porcentaje_'.$b->causa} = round(intval($porcentaje5)*100/$j,2);
                                                    }
                                                }
                                                if($causa!=""){
                                                    echo '<tr>
                                                      '.$causa.'
                                                      <td>'.round(intval($porcentaje)*100/$j,2).'%</td>
                                                    </tr>';
                                                }
                                                if($causa2!=""){
                                                    echo '<tr>
                                                      '.$causa2.'
                                                      <td>'.round(intval($porcentaje2)*100/$j,2).'%</td>
                                                    </tr>';
                                                }
                                                if($causa3!=""){
                                                    echo '<tr>
                                                      '.$causa3.'
                                                      <td>'.round(intval($porcentaje3)*100/$j,2).'%</td>
                                                    </tr>';
                                                }
                                                if($causa4!=""){
                                                    echo '<tr>
                                                      '.$causa4.'
                                                      <td>'.round(intval($porcentaje4)*100/$j,2).'%</td>
                                                    </tr>';
                                                }
                                                if($causa5!=""){
                                                    echo '<tr>
                                                      '.$causa5.'
                                                      <td>'.round(intval($porcentaje5)*100/$j,2).'%</td>
                                                    </tr>';
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-6"></div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <canvas id="graph_pie" width="600" height="450"></canvas>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table>
                                <tr>
                                    <th><br><br></th>
                                </tr>
                            </table>
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <table>
                                <tr>
                                    <th width="30%"><img class="img-fluid2"  src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                    <th width="70%"><h3 class="h_titulo">HEAD COUNT <?php echo mb_strtoupper($namemes,"UTF-8"); ?></h3></th>
                                </tr>
                            </table>
                            <div class="table-responsive">
                                <table class="table" border="1" width="100%" id="result_head">
                                    <thead class="table_thead">
                                        <tr>
                                            <th>PUESTO</th>
                                            <th>OBJETIVO</th>
                                            <th>ACTIVOS</th>
                                            <th>INCAPACIDAD</th>
                                            <th>FALTANTES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $puesto="";
                                            foreach ($head as $h) {
                                                $puesto=$h->puesto;
                                                ${'puesto_'.$h->id}=$puesto;
                                                ${'objetivo_'.$h->id."_".$h->id_puesto}=$h->objetivo;

                                                $get_act=$this->ModelReportes->count_personal_activo($id_proy,$h->id_puesto,$mes,$anio);
                                                //${'activos_'.$h->id."_".$h->id_puesto}=$get_act->tot_activos;
                                                ${'activos_'.$h->id."_".$h->id_puesto}=$get_act->tot_activos-$h->tot_inca_pers;
                                                //${'activos_'.$h->id."_".$h->id_puesto}=intval($h->objetivo)-intval($h->tot_inca)-intval($h->tot_falta);

                                                //log_message('error', "objetivo: ".${'objetivo_'.$h->id."_".$h->puesto});
                                                //log_message('error', "activos_: ".${'activos_'.$h->id."_".$h->id_puesto});
                                                //$activos=intval($h->objetivo)-intval($h->tot_inca)-intval($h->tot_falta);
                                                $activos=$get_act->tot_activos;
                                                ${'tot_activos_'.$h->id_puesto}=$activos;
                                                $faltantes=0;
                                                //log_message('error', "tot_activos_: ".${'tot_activos_'.$h->id_puesto});
                                                echo '<tr>
                                                  <td>'.$puesto.'</td>
                                                  <td>'.$h->objetivo.'</td>
                                                  <td>'.$activos.'</td>
                                                  <td>'.$h->tot_inca_pers.'</td>
                                                  <td>'.($h->objetivo-$activos+$h->tot_inca_pers).'</td>
                                              </tr>';
                                            }

                                        ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 graph_head_div">
                            <canvas id="graph_head" width="480"></canvas>
                        </div>
                    </div>
                    <div class="row">
                        <table>
                            <tr>
                                <th><br><br></th>
                            </tr>
                        </table>
                        <hr>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                            <table>
                                <tr>
                                    <th><br><br></th>
                                </tr>
                            </table>
                            <table class="table table_puestos_div" border="1" width="100%">
                                <thead class="table_thead">
                                    <tr>
                                        <th>PUESTO</th>
                                        <th>OBJETIVO</th>
                                        <th>DÍAS LABORABLES</th>
                                        <th>ASISTENCIA SEMANAL</th>
                                    </tr>
                                </thead>
                                <?php $cont_pues=0; $ips=0; $dlab=0; $asis_sem=0;
                                    foreach ($det_proy as $d) {
                                        //echo "<br> puesto: ".$d->puesto;
                                        $ips++; $dlab=$d->dias_laborables; 
                                        $puesto=$d->name_puesto;
                                        ${'dias_laborables'.$d->puesto}=$d->dias_laborables;
                                        //echo "<br> dias_laborables: ".${'dias_laborables'.$d->puesto};
                                        echo '<tr>
                                          <td>'.$puesto.'</td>
                                          <td>'.$d->objetivo.'</td>
                                          <td>'.$d->dias_laborables.'</td>
                                          <td>'.$d->objetivo*$d->dias_laborables.'</td>
                                        </tr>';
                                        $cont_pues=$cont_pues+$d->objetivo;
                                        $asis_sem=$asis_sem+($d->objetivo*$d->dias_laborables);
                                    }
                                ?>
                                <tfoot>
                                    <tr>
                                        <td>TOTAL</td>
                                        <td><?php echo $cont_pues; ?> </td>
                                        <td><?php echo $ips*$dlab; ?> </td>
                                        <td><?php echo $asis_sem; ?> </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="">
                            <table>
                                <tr>
                                    <th width="20%"><img class="img-fluid2"  src="<?php echo base_url();?>public/img/logofinalsys2.png"></th>
                                    <th width="50%"><h3 class="h_titulo">LISTADO DE PRESENTISMO Y ROTACIÓN</h3></th>
                                    <th width="10%"></th>
                                    <th width="20%"><img align="right" class="img-fluid2"  src="<?php echo base_url();?>uploads/clientes/<?php echo $foto; ?>"></th>
                                </tr>
                            </table>
                            <div class="row table-responsive">

                                <?php 
                                $coltables_grid='col-md-4';
                                $kkk=0;
                                $sem1=0; $sem2=0; $sem3=0; $sem4=0; $sem5=0;
                                $htm_head=""; $htm_body=""; $htm_head2=""; $htm_body2=""; $htm_body_obj=""; $htm_head3=""; $htm_body3=""; $htm_body_obj3=""; $tabla_prese=""; $tabla_turn=""; $tabla_inca=""; $class_pf=""; $td_tot=""; $cds=0;
                                $tot_obj=0; $tot_asis=0; $tot_falt=0; $tot_baj=0; $tot_inca=0;
                                $arr_sem=array();
                                $htm_head.="<div class='".$coltables_grid."'><table cellpadding='1' class='table ft8 tablec4' border='1'>";
                                //log_message('error', "primeraSemana de view: ".$primeraSemana);
                                if($primeraSemana==intval(1)){
                                    $sem_ant=0; $puesto_ant=0; $k_ant=0; $cont_aux=0;
                                    $suma_bajas=0;
                                    $s_aux=$primeraSemana; $th_aux="<th>EMPLEADO</th>"; $td_aux=""; $val_div="col-md-4";
                                    if($s_aux==intval(52) || $s_aux==intval(53)){ array_push($arr_sem,intval($s_aux)); $s_aux=1; $th_aux="";} 
                                    $cds=0; $td_tot="";  $sem_ant_aux=0; $cnt_sem_pto=0;
                                    for($k=$s_aux; $k<=$ultimaSemana; $k++){
                                        $cds++; $p_aux=0;
                                        $htm_head2.="<td><input id='nums_sems' type='hidden' value='".intval($k)."'>SEM ".intval($k)."</td>";
                                        $htm_head3.="<td>SEM ".intval($k)."</td>";
                                        array_push($arr_sem,intval($k));

                                        $tot_obj=0; $tot_asis=0; $tot_falt=0; $tot_baj=0; $tot_inca=0;
                                        $cont_aux++; if($primeraSemana!=intval(52) && $primeraSemana!=intval(53)) $val_div="col-md-12 col-sm-12 col-md-6 col-lg-4";
                                        echo "<div class='".$val_div."'><table class='table ft8 tablec4' border='1'>
                                            <thead class='table_thead'>
                                            <tr>";
                                            if ($cont_aux==1) echo $th_aux;
                                            echo "<th>SEM".intval($k)."</th>
                                                <th>ASISTENCIA</th>
                                                <th>FALTAS</th>
                                                <th>BAJAS</th>
                                                <th>INCAP.</th>
                                            </thead></tr>";
                                            //log_message('error', "semana consultada: ".$k);
                                            $data_sems=$this->ModelReportes->get_data_salida_semana($id_proy,$k,$s_aux,$mes/*,$h->puesto*/,$anio);
                                            $htm_body=""; 
                                            foreach ($data_sems as $s) {
                                                $td_aux="<td>".$s->puesto."</td>";
                                                $sem_ant=$s->semana;
                                                //$asist=intval($s->objetivo)-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                                //log_message('error', "tot_activos_: ".${'tot_activos_'.$s->id_puesto});
                                                //$asist=${'tot_activos_'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                                
                                                $tot_obj=$tot_obj+$s->objetivo;
                                                //$tot_asis=$tot_asis+$asist;
                                                $tot_falt=$tot_falt+$s->tot_falta;
                                                $tot_baj=$tot_baj+$s->tot_bajas;
                                                //if($sem_ant_aux!=$s->semana){
                                                    if($s->tot_bajas>0){
                                                        $p_aux++;
                                                        $suma_bajas=$suma_bajas+$s->tot_bajas;
                                                        ${'suma_bajas_'.$s->id_puesto}=$suma_bajas;

                                                    }
                                                //}
                                                $tot_inca=$tot_inca+$s->tot_inca;
                                                //log_message('error', "tot_activos_ desde listados se asis: ".${'tot_activos_'.$s->id_puesto});
                                                /*log_message('error', "semana desde listados se asis: ".$s->semana);
                                                log_message('error', "tot_activos desde listados se asis: ".$s->tot_activos);
                                                log_message('error', "tot_falta desde listados se asis: ".$s->tot_falta);
                                                log_message('error', "tot_bajas desde listados se asis: ".$s->tot_bajas);
                                                log_message('error', "tot_inca desde listados se asis: ".$s->tot_inca);*/

                                                if(isset(${'suma_bajas_'.$s->id_puesto}) && ${'suma_bajas_'.$s->id_puesto}>0 && $cnt_sem_pto>0){
                                                    $asist = $s->tot_activos*${'dias_laborables'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_inca)-intval(${'suma_bajas_'.$s->id_puesto});
                                                    //$asist = $s->tot_activos-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                                }else{
                                                    $asist = $s->tot_activos*${'dias_laborables'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                                }
                                                $tot_asis=$tot_asis+$asist;
                                                if($asist<0){
                                                    $asist=0;
                                                }
                                                //log_message('error', "asist desde listados se asis: ".$asist);
                                                //$tot_asis=$tot_asis+($asist*${'dias_laborables'.$s->id_puesto});
                                                
                                                /*log_message('error', "tot_activos: ".$s->tot_activos);
                                                log_message('error', "id_puesto: ".$s->id_puesto);
                                                log_message('error', "dias_laborables: ".${'dias_laborables'.$s->id_puesto});
                                                log_message('error', "asist: ".$asist);
                                                log_message('error', "tot_falta: ".$s->tot_falta);
                                                log_message('error', "tot_inca: ".$s->tot_inca);
                                                log_message('error', "tot_inca: ".$s->tot_inca);

                                                log_message('error', "suma_bajas_: ".${'suma_bajas_'.$s->id_puesto});
                                                log_message('error', "id puesto: ".$s->id_puesto);
                                                log_message('error', "k: ".$k);
                                                log_message('error', "cnt_sem_pto: ".$cnt_sem_pto);*/
                                                $htm_body.= '<tr>';
                                                //log_message('error', "cont_aux de view: ".$cont_aux);
                                                //log_message('error', "primeraSemana: ".$primeraSemana);
                                                if ($primeraSemana!=intval(52) && $cont_aux==1 && $primeraSemana!=intval(53)) $htm_body.=$td_aux;
                                                $htm_body.='<td>'.$s->objetivo.'</td>
                                                      <td>'.$asist.'</td>
                                                      <td>'.$s->tot_falta.'</td>
                                                      <td>'.$s->tot_bajas.'</td>
                                                      <td>'.$s->tot_inca.'</td>';
                                                $htm_body.= '</tr>';
                                                $sem_ant_aux=$s->semana;
                                                if(isset(${'suma_bajas_'.$s->id_puesto}) && $p_aux>0){
                                                    $cnt_sem_pto=$k;
                                                }
                                                
                                            }
                                            //log_message('error', "asis_sem: ".$asis_sem);
                                            $nvo_tot_asis=round(($tot_asis*100/$asis_sem),2);
                                            //log_message('error', "nvo_tot_asis: ".$nvo_tot_asis);
                                            if($nvo_tot_asis>=90)
                                                $class_p="btn-success";
                                            else if($nvo_tot_asis>=80 && $nvo_tot_asis<=89)
                                                $class_p="btn-warning";
                                            else if($nvo_tot_asis<=79)
                                                $class_p="btn-danger";

                                            $nvo_tot_falt=round(($tot_falt*100/$asis_sem),2);
                                            if($nvo_tot_falt<=1.9)
                                                $class_pf="btn-success";
                                            else if($nvo_tot_falt>=2 && $nvo_tot_falt<=4.9)
                                                $class_pf="btn-warning";
                                            else if($nvo_tot_falt>=5)
                                                $class_pf="btn-danger";

                                            $nvo_tot_baj=round(($tot_baj*100/$asis_sem),2);
                                            if($nvo_tot_baj<=1.9)
                                                $class_pb="btn-success";
                                            else if($nvo_tot_baj>=2 && $nvo_tot_baj<=4.9)
                                                $class_pb="btn-warning";
                                            else if($nvo_tot_baj>=5)
                                                $class_pb="btn-danger";

                                            $nvo_tot_inca=round(($tot_inca*100/$asis_sem),2);
                                            if($nvo_tot_inca<=1.9)
                                                $class_pi="btn-success";
                                            else if($nvo_tot_inca>=2 && $nvo_tot_inca<=4.9)
                                                $class_pi="btn-warning";
                                            else if($nvo_tot_inca>=5)
                                                $class_pi="btn-danger";

                                            echo $htm_body.'</tr><tfoot>';
                                            if($cds==1 && $primeraSemana!=intval(52) && $primeraSemana!=intval(53)) { $td_tot="<td>TOTAL</td>"; $colspan=2; } else { $td_tot=""; $colspan=1; }
                                            echo '<tr>'.$td_tot.'<td>'.$tot_obj.'</td><td>'.$tot_asis.'</td><td>'.$tot_falt.'</td><td>'.$tot_baj.'</td><td>'.$tot_inca.'</td></tr>
                                                <tr><td colspan="'.$colspan.'"></td>
                                                <td><p style="font-size:10px" class="btn '.$class_p.'">'.$nvo_tot_asis.'</p>
                                                </td>
                                                <td><p style="font-size:10px" class="btn '.$class_pf.'">'.$nvo_tot_falt.'</p>
                                                </td>
                                                <td><p style="font-size:10px" class="btn '.$class_pb.'">'.$nvo_tot_baj.'</p></td>
                                                <td><p style="font-size:10px" class="btn '.$class_pi.'">'.$nvo_tot_inca.'</p></td>
                                                </tr></tfoot>
                                            </table></div>'; 
                                            $htm_body2.="<td>".$nvo_tot_asis."%</td>";
                                            $htm_body_obj.="<td>100%</td>";
                                            $htm_body3.="<td>".$nvo_tot_baj."%</td>";
                                            $htm_body_obj3.="<td>0%</td>";
                                            $tabla_prese.="<tr><td><input id='nums_sems' type='hidden' value='".$k."'><input id='val_sems' type='hidden' value='".$nvo_tot_asis."'></td></tr>";
                                            $tabla_turn.="<tr><td><input id='nums_sems' type='hidden' value='".$k."'><input id='val_sems' type='hidden' value='".$nvo_tot_baj."'></td></tr>";

                                            
                                    }  
                                }else{
                                    $cont_td=0; $htm_body="";
                                    $cds=0; $td_tot="";
                                    $tot_obj=0; $tot_asis=0; $tot_falt=0; $tot_baj=0; $tot_inca=0;
                                    if($primeraSemana==intval(52) || $primeraSemana==intval(53)){
                                        $sem_anio=$primeraSemana;
                                        $p_aux=0; $cnt_sem_pto=0;
                                        echo "<div class='".$coltables_grid."'><table cellspacing='1' class='table ft8 tablec4' border='1'>
                                            <thead class='table_thead'>
                                            <tr>
                                                <th>EMPLEADO</th>
                                                <th>SEM".$sem_anio."</th>
                                                <th>ASISTENCIA</th>
                                                <th>FALTAS</th>
                                                <th>BAJAS</th>
                                                <th>INCAP.</th>
                                            </thead></tr>";
                                        $htm_body.= '<tr>';
                                        $data_sems=$this->ModelReportes->get_data_salida_semana($id_proy,$sem_anio,$primeraSemana,$mes/*,$h->puesto*/,$anio);
                                        foreach ($data_sems as $s) {
                                            $puesto="<td>".$s->puesto."</td>";
                                            //$asist=intval($s->objetivo)-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                            //log_message('error', "tot_activos_: ".${'tot_activos_'.$s->id_puesto});
                                            //$asist=${'tot_activos_'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                            $tot_obj=$tot_obj+$s->objetivo;
                                            //$tot_asis=$tot_asis+$asist;
                                      
                                            $tot_falt=$tot_falt+$s->tot_falta;
                                            $tot_baj=$tot_baj+$s->tot_bajas;
                                            $tot_inca=$tot_inca+$s->tot_inca;
                                            if($s->tot_bajas>0){
                                                $p_aux++;
                                                $suma_bajas=$suma_bajas+$s->tot_bajas;
                                                ${'suma_bajas_'.$s->id_puesto}=$suma_bajas;

                                            }
                                            if(isset(${'suma_bajas_'.$s->id_puesto}) && ${'suma_bajas_'.$s->id_puesto}>0 && $cnt_sem_pto>0){
                                                $asist = $s->tot_activos*${'dias_laborables'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_inca)-intval(${'suma_bajas_'.$s->id_puesto});
                                                //$asist = $s->tot_activos-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                            }else{
                                                $asist = $s->tot_activos*${'dias_laborables'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                            }
                                            $tot_asis=$tot_asis+$asist;

                                            if($asist<0){
                                                $asist=0;
                                            }
                                            //$tot_asis=$tot_asis+($asist*${'dias_laborables'.$s->id_puesto});
                                            //$tot_asis=$tot_asis+$asist;
                                            $htm_body.= '<tr>';
                                            $htm_body.= $puesto;
                                            $htm_body.='<td>'.$s->objetivo.'</td>
                                                  <td>'.$asist.'</td>
                                                  <td>'.$s->tot_falta.'</td>
                                                  <td>'.$s->tot_bajas.'</td>
                                                  <td>'.$s->tot_inca.'</td>';
                                            $htm_body.= '</tr>';

                                            $htm_head2="<td><input id='nums_sems' type='hidden' value='".$primeraSemana."'>SEM ".$primeraSemana."</td>";
                                            if(isset(${'suma_bajas_'.$s->id_puesto}) && $p_aux>0){
                                                $cnt_sem_pto=$k;
                                            }
                                        }
                                    
                                        $nvo_tot_asis=round(($tot_asis*100/$asis_sem),2);
                                        //log_message('error', "nvo_tot_asis: ".$nvo_tot_asis);
                                        if($nvo_tot_asis>=90)
                                            $class_p="btn-success";
                                        else if($nvo_tot_asis>=80 && $nvo_tot_asis<=89)
                                            $class_p="btn-warning";
                                        else if($nvo_tot_asis<=79)
                                            $class_p="btn-danger";

                                        $nvo_tot_falt=round(($tot_falt*100/$asis_sem),2);
                                        if($nvo_tot_falt<=1.9)
                                            $class_pf="btn-success";
                                        else if($nvo_tot_falt>=2 && $nvo_tot_falt<=4.9)
                                            $class_pf="btn-warning";
                                        else if($nvo_tot_falt>=5)
                                            $class_pf="btn-danger";

                                        $nvo_tot_baj=round(($tot_baj*100/$asis_sem),2);
                                        if($nvo_tot_baj<=1.9)
                                            $class_pb="btn-success";
                                        else if($nvo_tot_baj>=2 && $nvo_tot_baj<=4.9)
                                            $class_pb="btn-warning";
                                        else if($nvo_tot_baj>=5)
                                            $class_pb="btn-danger";

                                        $nvo_tot_inca=round(($tot_inca*100/$asis_sem),2);
                                        if($nvo_tot_inca<=1.9)
                                            $class_pi="btn-success";
                                        else if($nvo_tot_inca>=2 && $nvo_tot_inca<=4.9)
                                            $class_pi="btn-warning";
                                        else if($nvo_tot_inca>=5)
                                            $class_pi="btn-danger";

                                        if($cds==1 && $primeraSemana==intval(52) || $cds==1 && $primeraSemana==intval(53)) { $td_tot="<td>TOTAL</td>"; $colspan=2; } else if($cds==1 && $primeraSemana==intval(52) || $cds==1 && $primeraSemana==intval(53)) { $td_tot=""; $colspan=2; }
                                        echo $htm_body.'</tr><tfoot>
                                            <tr><td>TOTAL</td><td>'.$tot_obj.'</td><td>'.$tot_asis.'</td><td>'.$tot_falt.'</td><td>'.$tot_baj.'</td><td>'.$tot_inca.'</td></tr>
                                            <tr>
                                                <td colspan="2"></td><td><p style="font-size:10px" class="btn '.$class_p.'">'.$nvo_tot_asis.'</p>
                                                </td>
                                                <td><p style="font-size:10px" class="btn '.$class_pf.'">'.$nvo_tot_falt.'</p>
                                                </td>
                                                <td><p style="font-size:10px" class="btn '.$class_pb.'">'.$nvo_tot_baj.'</p>
                                                </td>
                                                <td><p style="font-size:10px" class="btn '.$class_pi.'">'.$nvo_tot_inca.'</p></td>
                                            </tr></tfoot></table></div>';
                                        $htm_body2.="<td>".$nvo_tot_asis."%</td>";
                                        $htm_body_obj.="<td>100%</td>";
                                        $htm_body3.="<td>".$nvo_tot_baj."%</td>";
                                        $htm_body_obj3.="<td>0%</td>";

                                        $tabla_prese.="<tr><td><input id='nums_sems' type='hidden' value='".$primeraSemana."'><input id='val_sems' type='hidden' value='".$nvo_tot_asis."'></td></tr>";
                                        $tabla_turn.="<tr><td><input id='nums_sems' type='hidden' value='".$primeraSemana."'><input id='val_sems' type='hidden' value='".$nvo_tot_baj."'></td></tr>";
                                    }
                                    $sem_ant=0; $puesto_ant=0; $k_ant=0; $cont_aux=0;
                                    $suma_bajas=0;
                                    $s_aux=$primeraSemana; $th_aux="<th>EMPLEADO</th>"; $td_aux=""; $val_div="col-md-4";
                                    if($s_aux==intval(52) || $s_aux==intval(53)){ array_push($arr_sem,intval($s_aux)); $s_aux=1; $th_aux="";} 
                                    $cds=0; $td_tot="";  $sem_ant_aux=0; $cnt_sem_pto=0;
                                    for($k=$s_aux; $k<=$ultimaSemana; $k++){
                                        $cds++; $p_aux=0;
                                        $htm_head2.="<td><input id='nums_sems' type='hidden' value='".intval($k)."'>SEM ".intval($k)."</td>";
                                        $htm_head3.="<td>SEM ".intval($k)."</td>";
                                        array_push($arr_sem,intval($k));

                                        $tot_obj=0; $tot_asis=0; $tot_falt=0; $tot_baj=0; $tot_inca=0;
                                        $cont_aux++; if($primeraSemana!=intval(52) && $primeraSemana!=intval(53)) $val_div="col-md-12 col-sm-12 col-md-6 col-lg-4";
                                        echo "<div class='".$val_div."'><table class='table ft8 tablec4' border='1'>
                                            <thead class='table_thead'>
                                            <tr>";
                                            if ($cont_aux==1) echo $th_aux;
                                            echo "<th>SEM".intval($k)."</th>
                                                <th>ASISTENCIA</th>
                                                <th>FALTAS</th>
                                                <th>BAJAS</th>
                                                <th>INCAP.</th>
                                            </thead></tr>";
                                            //log_message('error', "semana consultada: ".$k);
                                            $data_sems=$this->ModelReportes->get_data_salida_semana($id_proy,$k,$s_aux,$mes/*,$h->puesto*/,$anio);
                                            $htm_body=""; 
                                            foreach ($data_sems as $s) {
                                                $td_aux="<td>".$s->puesto."</td>";
                                                $sem_ant=$s->semana;
                                                //$asist=intval($s->objetivo)-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                                //log_message('error', "tot_activos_: ".${'tot_activos_'.$s->id_puesto});
                                                //$asist=${'tot_activos_'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                                
                                                $tot_obj=$tot_obj+$s->objetivo;
                                                //$tot_asis=$tot_asis+$asist;
                                                $tot_falt=$tot_falt+$s->tot_falta;
                                                $tot_baj=$tot_baj+$s->tot_bajas;
                                                //if($sem_ant_aux!=$s->semana){
                                                    if($s->tot_bajas>0){
                                                        $p_aux++;
                                                        $suma_bajas=$suma_bajas+$s->tot_bajas;
                                                        ${'suma_bajas_'.$s->id_puesto}=$suma_bajas;

                                                    }
                                                //}
                                                $tot_inca=$tot_inca+$s->tot_inca;
                                                //log_message('error', "tot_activos_ desde listados se asis: ".${'tot_activos_'.$s->id_puesto});
                                                /*log_message('error', "semana desde listados se asis: ".$s->semana);
                                                log_message('error', "tot_activos desde listados se asis: ".$s->tot_activos);
                                                log_message('error', "tot_falta desde listados se asis: ".$s->tot_falta);
                                                log_message('error', "tot_bajas desde listados se asis: ".$s->tot_bajas);
                                                log_message('error', "tot_inca desde listados se asis: ".$s->tot_inca);*/

                                                if(isset(${'suma_bajas_'.$s->id_puesto}) && ${'suma_bajas_'.$s->id_puesto}>0 && $cnt_sem_pto>0){
                                                    $asist = $s->tot_activos*${'dias_laborables'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_inca)-intval(${'suma_bajas_'.$s->id_puesto});
                                                    //$asist = $s->tot_activos-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                                }else{
                                                    $asist = $s->tot_activos*${'dias_laborables'.$s->id_puesto}-intval($s->tot_falta)-intval($s->tot_bajas)-intval($s->tot_inca);
                                                }
                                                $tot_asis=$tot_asis+$asist;
                                                if($asist<0){
                                                    $asist=0;
                                                }
                                                //log_message('error', "asist desde listados se asis: ".$asist);
                                                //$tot_asis=$tot_asis+($asist*${'dias_laborables'.$s->id_puesto});
                                                
                                                /*log_message('error', "tot_activos: ".$s->tot_activos);
                                                log_message('error', "id_puesto: ".$s->id_puesto);
                                                log_message('error', "dias_laborables: ".${'dias_laborables'.$s->id_puesto});
                                                log_message('error', "asist: ".$asist);
                                                log_message('error', "tot_falta: ".$s->tot_falta);
                                                log_message('error', "tot_inca: ".$s->tot_inca);
                                                log_message('error', "tot_inca: ".$s->tot_inca);

                                                log_message('error', "suma_bajas_: ".${'suma_bajas_'.$s->id_puesto});
                                                log_message('error', "id puesto: ".$s->id_puesto);
                                                log_message('error', "k: ".$k);
                                                log_message('error', "cnt_sem_pto: ".$cnt_sem_pto);*/
                                                $htm_body.= '<tr>';
                                                //log_message('error', "cont_aux de view: ".$cont_aux);
                                                //log_message('error', "primeraSemana: ".$primeraSemana);
                                                if ($primeraSemana!=intval(52) && $cont_aux==1 && $primeraSemana!=intval(53)) $htm_body.=$td_aux;
                                                $htm_body.='<td>'.$s->objetivo.'</td>
                                                      <td>'.$asist.'</td>
                                                      <td>'.$s->tot_falta.'</td>
                                                      <td>'.$s->tot_bajas.'</td>
                                                      <td>'.$s->tot_inca.'</td>';
                                                $htm_body.= '</tr>';
                                                $sem_ant_aux=$s->semana;
                                                if(isset(${'suma_bajas_'.$s->id_puesto}) && $p_aux>0){
                                                    $cnt_sem_pto=$k;
                                                }
                                                
                                            }
                                            //log_message('error', "asis_sem: ".$asis_sem);
                                            $nvo_tot_asis=round(($tot_asis*100/$asis_sem),2);
                                            //log_message('error', "nvo_tot_asis: ".$nvo_tot_asis);
                                            if($nvo_tot_asis>=90)
                                                $class_p="btn-success";
                                            else if($nvo_tot_asis>=80 && $nvo_tot_asis<=89)
                                                $class_p="btn-warning";
                                            else if($nvo_tot_asis<=79)
                                                $class_p="btn-danger";

                                            $nvo_tot_falt=round(($tot_falt*100/$asis_sem),2);
                                            if($nvo_tot_falt<=1.9)
                                                $class_pf="btn-success";
                                            else if($nvo_tot_falt>=2 && $nvo_tot_falt<=4.9)
                                                $class_pf="btn-warning";
                                            else if($nvo_tot_falt>=5)
                                                $class_pf="btn-danger";

                                            $nvo_tot_baj=round(($tot_baj*100/$asis_sem),2);
                                            if($nvo_tot_baj<=1.9)
                                                $class_pb="btn-success";
                                            else if($nvo_tot_baj>=2 && $nvo_tot_baj<=4.9)
                                                $class_pb="btn-warning";
                                            else if($nvo_tot_baj>=5)
                                                $class_pb="btn-danger";

                                            $nvo_tot_inca=round(($tot_inca*100/$asis_sem),2);
                                            if($nvo_tot_inca<=1.9)
                                                $class_pi="btn-success";
                                            else if($nvo_tot_inca>=2 && $nvo_tot_inca<=4.9)
                                                $class_pi="btn-warning";
                                            else if($nvo_tot_inca>=5)
                                                $class_pi="btn-danger";

                                            echo $htm_body.'</tr><tfoot>';
                                            if($cds==1 && $primeraSemana!=intval(52) && $primeraSemana!=intval(53)) { $td_tot="<td>TOTAL</td>"; $colspan=2; } else { $td_tot=""; $colspan=1; }
                                            echo '<tr>'.$td_tot.'<td>'.$tot_obj.'</td><td>'.$tot_asis.'</td><td>'.$tot_falt.'</td><td>'.$tot_baj.'</td><td>'.$tot_inca.'</td></tr>
                                                <tr><td colspan="'.$colspan.'"></td>
                                                <td><p style="font-size:10px" class="btn '.$class_p.'">'.$nvo_tot_asis.'</p>
                                                </td>
                                                <td><p style="font-size:10px" class="btn '.$class_pf.'">'.$nvo_tot_falt.'</p>
                                                </td>
                                                <td><p style="font-size:10px" class="btn '.$class_pb.'">'.$nvo_tot_baj.'</p></td>
                                                <td><p style="font-size:10px" class="btn '.$class_pi.'">'.$nvo_tot_inca.'</p></td>
                                                </tr></tfoot>
                                            </table></div>'; 
                                            $htm_body2.="<td>".$nvo_tot_asis."%</td>";
                                            $htm_body_obj.="<td>100%</td>";
                                            $htm_body3.="<td>".$nvo_tot_baj."%</td>";
                                            $htm_body_obj3.="<td>0%</td>";
                                            $tabla_prese.="<tr><td><input id='nums_sems' type='hidden' value='".$k."'><input id='val_sems' type='hidden' value='".$nvo_tot_asis."'></td></tr>";
                                            $tabla_turn.="<tr><td><input id='nums_sems' type='hidden' value='".$k."'><input id='val_sems' type='hidden' value='".$nvo_tot_baj."'></td></tr>";

                                            
                                    }  
                                } //else
                                ?>
                             </div>
                             <div class="col-md-6">
                                <table>
                                    <tr>
                                        <th><br><br></th>
                                    </tr>
                                </table>
                                <table style="display: none;" border="0" id="table_presentismo">
                                    <tbody id="thead_present">
                                        <?php echo $tabla_prese; ?>
                                    </tbody>
                                </table>
                                <table style="display: none;" border="0" id="table_turn">
                                    <tbody id="thead_turn">
                                        <?php echo $tabla_turn; ?>
                                    </tbody>
                                </table>

                                <table class="table" border="1" width="100%" id="table_presentismo_data">
                                    <thead class="table_thead">
                                        <tr>
                                            <td></td>
                                            <?php echo $htm_head2; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color:#00B0F0; color: #FFFFFF !important; text-align: center;">PRESENTISMO</td>
                                            <?php echo $htm_body2; ?>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#00B0F0; color: #FFFFFF !important; text-align: center;">OBJETIVO</td>
                                            <?php echo $htm_body_obj; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                          
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <th><br><br></th>
                                    </tr>
                                </table>
                                <table class="table table_generar_fz" border="1" width="100%" id="table_turn_data">
                                    <thead class="table_thead">
                                        <tr>
                                            <td></td>
                                            <?php echo $htm_head2; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color:#00B0F0; color: #FFFFFF !important; text-align: center;">TURN OVER</td>
                                            <?php echo $htm_body3; ?>
                                        </tr>
                                        <tr>
                                            <td style="background-color:#00B0F0; color: #FFFFFF !important; text-align: center;">OBJETIVO</td>
                                            <?php echo $htm_body_obj3; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="col-md-6 col-sm-12 box-col-12">
                                <div class="card" id="cont_graph_line">
                                    <div class="card-header pb-0">
                                        <h5>PRESENTISMO <?php echo mb_strtoupper($namemes,"UTF-8"); ?> <?php echo $anio; ?></h5>
                                    </div>
                                    <div class="card-body graph_g_div"><br>
                                        <div class="col-md-2 pb-0">
                                            <label style="font-size:12px; font-weight: bold;">% Presentismo</label>
                                        </div>
                                        <div class="flot-chart-container" id="graph_line"></div>
                                        <canvas id="graph_line_" width="480"></canvas>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6 col-sm-12 box-col-12">
                                <div class="card" id="cont_graph_line2">
                                    <div class="card-header pb-0">
                                        <h5>TURN OVER <?php echo mb_strtoupper($namemes,"UTF-8"); ?> <?php echo $anio; ?></h5>
                                    </div>
                                    <div class="card-body graph_g_div"><br>
                                        <div class="col-md-2 pb-0">
                                            <label style="font-size:12px; font-weight:bold;">% Turn Over</label>
                                        </div>
                                    
                                        <div class="card-body graph_g_div">
                                            <div class="ct-6 flot-chart-container" id="graph_line2"></div>
                                            <canvas id="graph_line2_" width="480"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- agregado vacaciones -->
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <table>
                                    <tr>
                                        <th><br><br></th>
                                    </tr>
                                </table>
                                <table class="table table_generar_fz" border="1" width="100%" id="table_holly">
                                    <thead class="table_thead">
                                        <tr>
                                            <td>NOMBRE</td>
                                            <?php echo $htm_head2; ?>
                                            <td>TOTAL</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if($primeraSemana==intval(1)){
                                            $aux=0;
                                            $where='(WEEKOFYEAR(fecha)='.$arr_sem[0].' or WEEKOFYEAR(fecha)='.$arr_sem[1].' or WEEKOFYEAR(fecha)='.$arr_sem[2].' or WEEKOFYEAR(fecha)='.$arr_sem[3].' or WEEKOFYEAR(fecha)='.$arr_sem[4].')';
                                            if($arr_sem[5]){
                                                $where='(WEEKOFYEAR(fecha)='.$arr_sem[0].' or WEEKOFYEAR(fecha)='.$arr_sem[1].' or WEEKOFYEAR(fecha)='.$arr_sem[2].' or WEEKOFYEAR(fecha)='.$arr_sem[3].' or WEEKOFYEAR(fecha)='.$arr_sem[4].' or WEEKOFYEAR(fecha)='.$arr_sem[5].')';
                                            }

                                            $aux2=0;
                                            $td_holly1=0; $td_holly2=0; $td_holly3=0; $td_holly4=0; $td_holly5=0; $td_holly6=0;
                                            $total_vacas=0;
                                            $data_holly=$this->ModelReportes->get_data_vacaciones2($id_proy,$mes,$anio,$where);
                                            foreach ($data_holly as $s) {
                                                $aux++;
                                                //echo "<br>id_personal: ".$s->id_personal;
                                                $id_personal=$s->id_personal;
                                                $td_holly1=0; $td_holly2=0; $td_holly3=0; $td_holly4=0; $td_holly5=0; $td_holly6=0;
                                                for($r=0; $r<=count($arr_sem); $r++){
                                                    //echo "semana 2: ".$arr_sem[1];
                                                    $tmp=explode(",",$s->semana2); 
                                                    //echo "<br>array_sem : ".$arr_sem[$aux];
                                                    if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[0]) $td_holly1++;
                                                    else if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[1]) $td_holly2++;
                                                    else if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[2]) $td_holly3++; 
                                                    else if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[3]) $td_holly4++; 
                                                    else if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[4]) $td_holly5++;
                                                    else if(isset($arr_sem[5]) && $tmp[$r]==$arr_sem[5]) $td_holly6++; 
                                                }   
                                                $total_vacas=$td_holly1+$td_holly2+$td_holly3+$td_holly4+$td_holly5+$td_holly6;
                                                ${'tot_sem1_'.$s->id_personal.'_'.$arr_sem[0]}=$td_holly1;
                                                ${'tot_sem2_'.$s->id_personal.'_'.$arr_sem[1]}=$td_holly2;
                                                ${'tot_sem3_'.$s->id_personal.'_'.$arr_sem[2]}=$td_holly3;
                                                ${'tot_sem4_'.$s->id_personal.'_'.$arr_sem[3]}=$td_holly4;
                                                ${'tot_sem5_'.$s->id_personal.'_'.$arr_sem[4]}=$td_holly5;
                                                ${'tot_sem6_'.$s->id_personal.'_'.$arr_sem[5]}=$td_holly6;
                                                ${'tot_sems_'.$s->id_personal}=$total_vacas;
                                                echo '<tr>
                                                    <td>'.$s->nombre.'</td>
                                                    <td>'.$td_holly1.'</td>
                                                    <td>'.$td_holly2.'</td>
                                                    <td>'.$td_holly3.'</td>
                                                    <td>'.$td_holly4.'</td>
                                                    <td>'.$td_holly5.'</td>';
                                                if(isset($arr_sem[5])){
                                                    echo '<td>'.$td_holly6.'</td>';
                                                }
                                                echo '<td>'.$total_vacas.'</td>
                                                </tr>';
                                            } 
                                            
                                        } else{
                                            $td_holly1=0; $td_holly2=0; $td_holly3=0; $td_holly4=0; $td_holly5=0; $td_holly6=0;
                                            $total_vacas=0;
                                            if($primeraSemana==intval(52) || $primeraSemana==intval(53)){
                                                $data_holly=$this->ModelReportes->get_data_vacaciones($id_proy,$mes,$anio,$primeraSemana);
                                                foreach ($data_holly as $s) {
                                                    if($s->semana==52 || $s->semana==53) $td_holly1++;
                                                } 
                                            } 
                                            $aux=0;
                                            //var_dump($arr_sem);
                                            $where='(WEEKOFYEAR(fecha)='.$arr_sem[0].' or WEEKOFYEAR(fecha)='.$arr_sem[1].' or WEEKOFYEAR(fecha)='.$arr_sem[2].' or WEEKOFYEAR(fecha)='.$arr_sem[3].' or WEEKOFYEAR(fecha)='.$arr_sem[4].')';
                                            if(isset($arr_sem[5])){
                                                $where='(WEEKOFYEAR(fecha)='.$arr_sem[0].' or WEEKOFYEAR(fecha)='.$arr_sem[1].' or WEEKOFYEAR(fecha)='.$arr_sem[2].' or WEEKOFYEAR(fecha)='.$arr_sem[3].' or WEEKOFYEAR(fecha)='.$arr_sem[4].' or WEEKOFYEAR(fecha)='.$arr_sem[5].')';
                                            }
                                            //for($k=$primeraSemana; $k<=$ultimaSemana; $k++){
                                                $aux2=0;
                                                /*$td_holly1=0; $td_holly2=0; $td_holly3=0; $td_holly4=0; $td_holly5=0;*/
                                                $data_holly=$this->ModelReportes->get_data_vacaciones2($id_proy,$mes,$anio,$where);
                                                foreach ($data_holly as $s) {
                                                    $aux++;
                                                    //echo "<br>id_personal: ".$s->id_personal;
                                                    $id_personal=$s->id_personal;
                                                    $td_holly1=0; $td_holly2=0; $td_holly3=0; $td_holly4=0; $td_holly5=0; $td_holly6=0;
                                                    for($r=0; $r<=count($arr_sem); $r++){
                                                        //echo "semana 2: ".$arr_sem[1];
                                                        $tmp=explode(",",$s->semana2);
                                                        //echo "<br>tmp[r] : ".$tmp[$r];
                                                        //echo "<br>array_sem : ".$arr_sem[$aux];
                                                        if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[0]) $td_holly1++;
                                                        else if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[1]) $td_holly2++;
                                                        else if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[2]) $td_holly3++; 
                                                        else if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[3]) $td_holly4++; 
                                                        else if(isset($tmp[$r]) && $tmp[$r]==$arr_sem[4]) $td_holly5++;
                                                        else if(isset($arr_sem[5]) && $tmp[$r]==$arr_sem[5]) $td_holly6++; 
                                                    }   
                                                    $total_vacas=$td_holly1+$td_holly2+$td_holly3+$td_holly4+$td_holly5+$td_holly6;
                                                    ${'tot_sem1_'.$s->id_personal.'_'.$arr_sem[0]}=$td_holly1;
                                                    ${'tot_sem2_'.$s->id_personal.'_'.$arr_sem[1]}=$td_holly2;
                                                    ${'tot_sem3_'.$s->id_personal.'_'.$arr_sem[2]}=$td_holly3;
                                                    ${'tot_sem4_'.$s->id_personal.'_'.$arr_sem[3]}=$td_holly4;
                                                    ${'tot_sem5_'.$s->id_personal.'_'.$arr_sem[4]}=$td_holly5;
                                                    ${'tot_sem6_'.$s->id_personal.'_'.$arr_sem[5]}=$td_holly6;
                                                    ${'tot_sems_'.$s->id_personal}=$total_vacas;
                                                    echo '<tr>
                                                        <td>'.$s->nombre.'</td>
                                                        <td id="sem_'.$aux.'">'.$td_holly1.'</td>
                                                        <td id="sem_'.$aux.'">'.$td_holly2.'</td>
                                                        <td id="sem_'.$aux.'">'.$td_holly3.'</td>
                                                        <td id="sem_'.$aux.'">'.$td_holly4.'</td>
                                                        <td id="sem_'.$aux.'">'.$td_holly5.'</td>';
                                                    if(isset($arr_sem[5])){
                                                        echo'<td>'.$td_holly6.'</td>';
                                                    }
                                                    echo '<td id="total">'.$total_vacas.'</td>
                                                    </tr>';
                                                } 
                                            //}
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                
                            </div><!-- termina semanas indicadoras de vacaciones -->
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <canvas id="graph_holly" width="700" height="450"></canvas>
                            </div>
                            <div class="col-1"></div>
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                                <table>
                                    <tr>
                                        <th><br><br></th>
                                    </tr>
                                </table>
                                <table class="table table_generar_fz" border="1" width="100%" id="table_holly_days">
                                    <thead class="table_thead">
                                        <tr>
                                            <td>NOMBRE</td>
                                            <td>DÍAS TOMADOS</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $aux=0;
                                            //agregar validacion si existe semana 6 y tambien en graficas
                                            $where='(WEEKOFYEAR(fecha)='.$arr_sem[0].' or WEEKOFYEAR(fecha)='.$arr_sem[1].' or WEEKOFYEAR(fecha)='.$arr_sem[2].' or WEEKOFYEAR(fecha)='.$arr_sem[3].' or WEEKOFYEAR(fecha)='.$arr_sem[4].')';
                                            if($arr_sem[5]){
                                                $where='(WEEKOFYEAR(fecha)='.$arr_sem[0].' or WEEKOFYEAR(fecha)='.$arr_sem[1].' or WEEKOFYEAR(fecha)='.$arr_sem[2].' or WEEKOFYEAR(fecha)='.$arr_sem[3].' or WEEKOFYEAR(fecha)='.$arr_sem[4].' or WEEKOFYEAR(fecha)='.$arr_sem[5].')';
                                            }
                                            $aux2=0;
                                            $data_holly=$this->ModelReportes->get_data_vacaciones2($id_proy,$mes,$anio,$where);
                                            foreach ($data_holly as $s) {
                                                $aux++;   
                                                echo '<tr>
                                                    <td>'.$s->nombre.'</td>
                                                    <td>'.$s->fechas.'</td>
                                                </tr>';
                                            } 
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <canvas id="graph_holly_year" width="600" height="450"></canvas>
                            </div><!-- termina semanas indicadoras de vacaciones -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $bajas=$this->ModelReportes->get_bajas_proy($id_proy,$anio,$mes,1); ?>
<script type="text/javascript">
$(document).ready(function() {
    $.blockUI({ 
        message: '<div class="fa-3x spinner-grow" style="justify-content: center; align-items:center;" role="status" >\
           \
          </div><br>Generando resultados..... Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff'
        } 
    });
    new Chart(document.getElementById("graph"), {
        type: 'bar',
        data: {
            labels: [<?php foreach ($res as $r) { if($r->tot_faltas>0 || $r->tot_perm>0 || $r->tot_ret>0 || $r->tot_inca>0 || $r->tot_vaca>0){ echo "'$r->nombre',";} }?>],
            datasets: [
            {
                type: 'bar',
                label: "FALTAS",
                backgroundColor: "rgba(255, 0, 0, 0.7)",
                borderColor: 'rgba(255, 0, 0, 1)',
                borderWidth: 2,
                data: [<?php foreach ($res as $r2) { if($r->tot_faltas>0 || $r->tot_perm>0 || $r->tot_ret>0 || $r->tot_inca>0 || $r->tot_vaca>0){ echo ${'tot_faltas'.$r2->id}.',';} } ?>],
                fill: false,
            },
            {
                type: 'bar',
                label: ["PERMISOS"],
                backgroundColor: "rgba(0, 255, 0, 0.7)",
                borderColor: 'rgba(0, 255, 0, 1)',
                borderWidth: 2,
                data: [<?php foreach ($res as $r3) { if($r3->tot_faltas>0 || $r3->tot_perm>0 || $r3->tot_ret>0 || $r3->tot_inca>0 || $r3->tot_vaca>0){ echo ${'tot_perm'.$r3->id}.',';} }?>],
                fill: false,
            },
            {
                type: 'bar',
                label: "RETARDOS",
                backgroundColor: "rgba(0,0,153, 0.7)",
                borderColor: 'rgba(0,0,153, 1)',
                borderWidth: 2,
                data: [<?php foreach ($res as $r2) { if($r2->tot_faltas>0 || $r2->tot_perm>0 || $r2->tot_ret>0 || $r2->tot_inca>0 || $r2->tot_vaca>0){ echo ${'tot_ret'.$r2->id}.',';} }?>],
                fill: false,
            },
            {
                type: 'bar',
                label: ["INCAPACIDAD"],
                backgroundColor: "rgba(255,192,0, 0.7)",
                borderColor: 'rgba(255,192,0, 1)',
                borderWidth: 2,
                data: [<?php foreach ($res as $r3) { if($r3->tot_faltas>0 || $r3->tot_perm>0 || $r3->tot_ret>0 || $r3->tot_inca>0 || $r3->tot_vaca>0){ echo ${'tot_inca'.$r3->id}.',';} }?>],
                fill: false,
            },
            {
                type: 'bar',
                label: ["VACACIONES"],
                backgroundColor: "rgba(0,176,240, 0.7)",
                borderColor: 'rgba(0,176,240, 1)',
                borderWidth: 2,
                data: [<?php foreach ($res as $r3) { if($r3->tot_faltas>0 || $r3->tot_perm>0 || $r3->tot_ret>0 || $r3->tot_inca>0 || $r3->tot_vaca>0){ echo ${'tot_vaca'.$r3->id}.',';} }?>],
                fill: false,
            }

          ]
        },
        options: {
            legend: { 
                display: true 
            },
            title: {
                display: true,
                text: 'INCIDENCIAS DE <?php echo mb_strtoupper($namemes,"UTF-8"); ?> <?php echo $anio; ?>'
            },
            scales: {
                xAxes: [{
                    barPercentage: 1, //ancho de barra
                    categoryPercentage: 0.9,
                    ticks: {
                      autoSkip: false,
                      //rotation: -90,
                      //fontSize:9,
                      fontSize:11,
                      fontColor : '#000',
                      fontWeight: "bold",
                      //barPercentage: 0.9
                    }
                }],
                yAxes: [{

                    ticks: {
                        //suggestedMax: 4,
                        //suggestedMin:0.5,
                        beginAtZero: true,
                        callback: function(value, index, values) {
                            if (Math.floor(value) === value) {
                                return value;
                            }
                        },
                        gridLines: {
                        drawBorder: true,
                        display: false
                    }
                        //stepSize: 2
                    }
                }]
            },
            responsive: true,
            maintainAspectRatio: false,
            //Enabling 3D Chart
            enable3D: true, 
            enableRotation: true,     
            depth: 100,
            wallSize: 2,
            tilt: 0,
            rotation: 14,
            perspectiveAngle: 20,
            sideBySideSeriesPlacement: true,
            load: "loadTheme",
            title: { text: '' },            
            isResponsive: true,
            size: { height: '450'},
            legend: { visible: true  },
            plugins: {
                datalabels: {
                    formatter: (value) => {
                        return value;
                    },
                    anchor: 'end',
                    //align: 'end',
                    color: 'black',
                    font: {
                      family: '"Times New Roman"',
                      size: "16",
                      weight: "bold",
                    },
                },
            },

        }
    });
    
    new Chart(document.getElementById("graph_pie"), {
        type: 'pie',
        data: {
            labels: [<?php foreach ($bajas as $r) {echo "'${'causa_'.$r->causa}',";} ?>],
            datasets: [
            {
                type: 'pie',
                label: [<?php foreach ($bajas as $r2) {echo "'${'causa_'.$r2->causa}',";} ?>],
                backgroundColor: ["rgba(255, 0, 0, 0.5)", "rgba(0, 255, 0, 0.7)", "rgba(0,0,153, 0.7)", "rgba(255,192,0, 0.7)"],
                borderColor: 'rgba(238,238,238, 1)',
                borderWidth: 2,
                data: [<?php foreach ($bajas as $r2) {echo ${'porcentaje_'.$r2->causa}.',';} ?>],
                fill: false,
            },
          ]
        },
        options: {
            legend: { 
                display: true 
            },
            title: {
                display: true,
                text: 'INCIDENCIAS DE <?php echo mb_strtoupper($namemes,"UTF-8"); ?> <?php echo $anio; ?>'
            },
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6,
                    ticks: {
                      autoSkip: false,
                      rotation: -90,
                      barPercentage: 0.5
                    }
                }]
            },
            //Enabling 3D Chart
            enable3D: true, 
            enableRotation: true,     
            depth: 100,
            wallSize: 2,
            tilt: 0,
            rotation: 10,
            perspectiveAngle: 20,
            sideBySideSeriesPlacement: true,
            load: "loadTheme",
            title: { text: '' },            
            isResponsive: true,
            size: { height: '450'},
            legend: { visible: true  },
            plugins: {
                datalabels: {
                    formatter: (value) => {
                        return value +"%";
                    },
                    color: '#fff',
                    font: {
                      family: '"Times New Roman"',
                      size: "18",
                      weight: "bold",
                    },
                },
            }
        }
    });

    new Chart(document.getElementById("graph_head"), {
        /*renderTo: 'container',
        type: 'bar',
        margin: 75,
        options3d: {
         enabled: true,
         alpha: 15,
         beta: 15,
         depth: 50,
         viewDistance: 25
        },*/
        type: 'bar',
        data: {
            labels: [<?php foreach ($head as $h) {echo "'${'puesto_'.$h->id}',";} ?>],
            datasets: [
            {
                type: 'bar',
                label: [ "OBJETIVO" ],
                backgroundColor: "rgba(0, 204, 0, 0.7)",
                borderColor: 'rgba(0, 204, 0, 1)',
                borderWidth: 2,
                data: [<?php foreach ($head as $h) {echo ${'objetivo_'.$h->id."_".$h->id_puesto}.',';} ?>],
                fill: false,
            },
            {
                type: 'bar',
                label: ["ACTIVOS"],
                backgroundColor: "rgba(0, 176, 240, 0.7)",
                borderColor: 'rgba(0, 176, 240, 1)',
                borderWidth: 2,
                data: [<?php foreach ($head as $h) {echo ${'activos_'.$h->id."_".$h->id_puesto}.',';} ?>],
                fill: false,
            }
          ]
        },
        options: {
            legend: { 
                display: true,
                fontColor: 'black',
                fontWeight: "bold",
            },
            title: {
                display: true,
                text: 'HEAD COUNT',
                padding: 40,
                fontColor: 'black',
                fontWeight: "bold",
            },
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6,
                    ticks: {
                      autoSkip: false,
                      rotation: -90,
                      barPercentage: 1,
                      fontColor: 'black',
                      fontWeight: "bold",
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }],
            },
            //Enabling 3D Chart
            enable3D: true, 
            enableRotation: true,     
            depth: 100,
            wallSize: 2,
            tilt: 0,
            rotation: 14,
            perspectiveAngle: 20,
            sideBySideSeriesPlacement: true,
            load: "loadTheme",
            title: { text: '' },            
            isResponsive: true,
            size: { height: '450'},
            legend: { visible: true  },
            plugins: {
                datalabels: {
                    formatter: (value) => {
                        return value;
                    },
                    color: '#fff',
                    font: {
                      family: '"Times New Roman"',
                      size: "18",
                      weight: "bold",
                    },
                },
            },
            maintainAspectRatio: false

        }
    });
    setTimeout(function(){ 
        //saveGraph("graph_head",1); //se comenta a peticion el dia 17 d enero de 2023
    }, 1500);

    new Chart(document.getElementById("graph_holly"), {
        type: 'bar',
        data: {
            labels: [<?php foreach ($data_holly as $s) {echo "'$s->nombre',";} ?>],
            datasets: [
            {
                type: 'bar',
                label: ["SEMANA <?php echo $arr_sem[0]; ?>"],
                //label: [<?php for($c=0; $c<5; $c++) { echo $arr_sem[$c].','; } ?>],
                backgroundColor: "rgba(68,114,196, 0.7)",
                borderColor: 'rgba(68,114,196, 1)',
                borderWidth: 2,
                data: [<?php foreach ($data_holly as $s) { echo ${'tot_sem1_'.$s->id_personal.'_'.$arr_sem[0]}.','; }  ?>],
                fill: false,
            },
            {
                type: 'bar',
                label: ["SEMANA <?php echo $arr_sem[1]; ?>"],
                backgroundColor: "rgba(121,189,28, 0.7)",
                borderColor: 'rgba(97,148,57, 1)',
                borderWidth: 2,
                data: [<?php foreach ($data_holly as $s) { echo ${'tot_sem2_'.$s->id_personal.'_'.$arr_sem[1]}.','; }  ?>],
                fill: false,
            },
            {
                type: 'bar',
                label: ["SEMANA <?php echo $arr_sem[2]; ?>"],
                //label: [<?php for($c=0; $c<5; $c++) { echo $arr_sem[$c].','; } ?>],
                backgroundColor: "rgba(237,125,49, 0.7)",
                borderColor: 'rgba(237,125,49, 1)',
                borderWidth: 2,
                data: [<?php foreach ($data_holly as $s) { echo ${'tot_sem3_'.$s->id_personal.'_'.$arr_sem[2]}.','; }  ?>],
                fill: false,
            },
            {
                type: 'bar',
                label: ["SEMANA <?php echo $arr_sem[3]; ?>"],
                backgroundColor: "rgba(255,192,0, 0.7)",
                borderColor: 'rgba(255,192,0, 1)',
                borderWidth: 2,
                data: [<?php foreach ($data_holly as $s) { echo ${'tot_sem4_'.$s->id_personal.'_'.$arr_sem[3]}.','; }  ?>],
                fill: false,
            },
            {
                type: 'bar',
                label: ["SEMANA <?php echo $arr_sem[4]; ?>"],
                backgroundColor: "rgba(165,165,165, 0.7)",
                borderColor: 'rgba(165,165,165, 1)',
                borderWidth: 2,
                data: [<?php foreach ($data_holly as $s) { echo ${'tot_sem5_'.$s->id_personal.'_'.$arr_sem[4]}.','; }  ?>],
                fill: false,
            }
            <?php if(isset($arr_sem[5])){ ?>
                ,{
                    type: 'bar',
                    label: ["SEMANA <?php echo $arr_sem[5]; ?>"],
                    backgroundColor: "rgba(2,50,207, 0.7)",
                    borderColor: 'rgba(2,50,207, 1)',
                    borderWidth: 2,
                    data: [<?php foreach ($data_holly as $s) { echo ${'tot_sem6_'.$s->id_personal.'_'.$arr_sem[5]}.','; }  ?>],
                    fill: false,
                }
            <?php } ?>
          ]
        },
        options: {
            legend: { 
                display: true,
                fontColor: 'black',
                fontWeight: "bold",
            },
            title: {
                display: true,
                text: 'VACACIONES <?php echo $anio; ?>',
                fontColor: 'black',
                fontWeight: "bold",
            },
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6,
                    ticks: {
                      autoSkip: false,
                      rotation: -90,
                      barPercentage: 0.5,
                      //fontSize:9,
                      fontSize:12,
                      fontColor : '#000',
                      fontWeight: "bold",
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                        fontSize:13,
                        fontColor: 'black',
                        fontWeight: "bold",
                    }
                }],
            },
            plugins: {
                datalabels: {
                    formatter: (value) => {
                        return value;
                    },
                    color: '#fff',
                    font: {
                      family: '"Times New Roman"',
                      size: "18",
                      weight: "bold",
                    },
                },
            },
            maintainAspectRatio: false

        }
    }); 
    setTimeout(function(){ 
        saveGraph("graph_holly",8);
    }, 2000);

    new Chart(document.getElementById("graph_holly_year"), {
        type: 'bar',
        data: {
            labels: [<?php foreach ($data_holly as $s) {echo "'$s->nombre',";} ?>],
            datasets: [
            {
                type: 'bar',
                label: ["TOTAL <?php echo $anio; ?>"],
                backgroundColor: "rgba(68,114,196, 0.7)",
                borderColor: 'rgba(68,114,196, 1)',
                borderWidth: 2,
                data: [<?php foreach ($data_holly as $s) { echo ${'tot_sems_'.$s->id_personal}.','; }  ?>],
                fill: false,
            }
          ]
        },
        options: {
            legend: { 
                display: true,
                fontColor: 'black',
                fontWeight: "bold",
            },
            title: {
                display: true,
                text: 'VACACIONES <?php echo $anio; ?>',
                fontColor: 'black',
                fontWeight: "bold",
            },
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6,
                    ticks: {
                      autoSkip: false,
                      rotation: -90,
                      barPercentage: 0.5,
                      //fontSize:9,
                      fontSize:12,
                      fontColor : '#000',
                      fontWeight: "bold",
                    }  
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                        //fontColor: 'blue',
                        fontSize:13,
                        fontColor: 'black',
                        fontWeight: "bold",
                    }
                }],
            },
            plugins: {
                datalabels: {
                    formatter: (value) => {
                        return value;
                    },
                    color: '#fff',
                    font: {
                      family: '"Times New Roman"',
                      size: "18",
                      weight: "bold",
                    },
                },
            },
            maintainAspectRatio: false

        }
    }); 
    setTimeout(function(){ 
        saveGraph("graph_holly_year",9);
    }, 2500);

    var label_sem=[]; var vals_sem=[]; var objetivo=[];
    var TABLA = $("#table_presentismo tbody > tr");
    TABLA.each(function(){  
        //console.log("nums_sems: "+$(this).find("input[id*='nums_sems']").val());
        //console.log("val_sems: "+$(this).find("input[id*='nums_sems']").val());
        label_sem.push(String("SEM "+$(this).find("input[id*='nums_sems']").val()));
        vals_sem.push(parseFloat($(this).find("input[id*='val_sems']").val()));
        objetivo.push(100);
    });
    new Chart(document.getElementById("graph_line_"), {
        type: 'line',
        data: {
            labels: label_sem,
            datasets: [
            {
                type: 'line',
                label: [ "OBJETIVO" ],
                backgroundColor: "rgba(0, 204, 0, 0.7)",
                borderColor: 'rgba(0, 204, 0, 1)',
                borderWidth: 5,
                data: objetivo,
                fill: false,
                align: 'top',
                datalabels:{
                    anchor:'end',
                    align:'end',
                    offset:'0'
                }
            },
            {
                type: 'line',
                label: ["PRESENTISMO"],
                backgroundColor: "rgba(0, 176, 240, 0.7)",
                borderColor: 'rgba(0, 176, 240, 1)',
                borderWidth: 2,
                data: vals_sem,
                fill: false,
                align: 'bottom',
                datalabels:{
                    anchor:'start',
                    align:'start',
                    offset:'0'
                }
            }
          ]
        },
        options: {
            legend: { 
                display: true,
                position: 'bottom',
                padding: 5,
                fontColor: 'black',
                fontWeight: "bold",   
            },
            title: {
                display: true,
                text: 'HEAD COUNT',
                padding: 40,
                fontColor: 'black',
                fontWeight: "bold",
            },
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6,
                    ticks: {
                      autoSkip: false,
                      rotation: -90,
                      barPercentage: 0.5,
                      padding:20
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 25,
                        padding:20,
                        fontSize:13,
                        fontColor: 'black',
                        fontWeight: "bold",
                    }
                }],
            },
            plugins: {
                datalabels: {
                    formatter: (value) => {
                        return "\n"+value+"%";
                    },
                    //anchor: 'center',
                    //align: 'bottom',
                    color: '#6c757d',
                    font: {
                      family: '"Arial"',
                      size: "13",
                      weight: "bold",
                    },
                },
                legend: {
                    labels: {
                        padding: 100,
                    }
                },
            },

        }
    });
    setTimeout(function(){ 
        saveGraph("graph_line_",2);
    }, 3000);


    //tabla de turn over
    let label_sem2=[]; let vals_sem2=[]; let objetivo2=[];
    let TABLA2 = $("#table_turn tbody > tr");
    TABLA2.each(function(){  
        //console.log("nums_sems: "+$(this).find("input[id*='nums_sems']").val());
        //console.log("val_sems: "+$(this).find("input[id*='nums_sems']").val());
        label_sem2.push(String("SEM "+$(this).find("input[id*='nums_sems']").val()));
        vals_sem2.push(Number($(this).find("input[id*='val_sems']").val()));
        objetivo2.push(0);
    });

    new Chart(document.getElementById("graph_line2_"), {
        type: 'line',
        data: {
            labels: label_sem2,
            datasets: [
            {
                type: 'line',
                label: [ "OBJETIVO" ],
                backgroundColor: "rgba(0, 204, 0, 0.7)",
                borderColor: 'rgba(0, 204, 0, 1)',
                borderWidth: 4,
                data: objetivo2,
                fill: false,
                datalabels:{
                    anchor:'end',
                    align:'end',
                    offset:'0'
                }
            },
            {
                type: 'line',
                label: ["TURN OVER"],
                backgroundColor: "rgba(0, 176, 240, 0.7)",
                borderColor: 'rgba(0, 176, 240, 1)',
                borderWidth: 2,
                data: vals_sem2,
                fill: false,
                datalabels:{
                    anchor:'start',
                    align:'start',
                    offset:'0'
                }
            }
          ]
        },
        options: {
            legend: { 
                display: true,
                position: 'bottom',
                fontColor: 'black',
                fontWeight: "bold",
            },
            title: {
                display: true,
                text: 'TURN OVER',
                padding: 40,
                fontColor: 'black',
                fontWeight: "bold",
            },
            scales: {
                xAxes: [{
                    padding:30,
                    barPercentage: 1,
                    categoryPercentage: 0.6,
                    ticks: {
                      autoSkip: false,
                      rotation: -90,
                      barPercentage: 0.5,
                      padding:20
                    },
                    label:"holly"
                }],
                yAxes: [{
                    
                    ticks: {
                        beginAtZero: true,
                        stepSize: 0.5,
                        padding:20,
                        fontSize:13,
                        fontColor: 'black',
                        fontWeight: "bold",
                    }
                }],
            },
            plugins: {
                datalabels: {
                    formatter: (value) => {
                        return value+"%";
                    },
                    //anchor: 'end',
                    anchor: 'center',
                    align: 'top',
                    color: '#6c757d',
                    font: {
                      family: '"Arial"',
                      size: "15",
                      weight: "bold",
                    },
                },
            },

        }
    });
    setTimeout(function(){ 
        saveGraph("graph_line2_",3);
    }, 3500);

    setTimeout(function(){
        /*html2canvas(document.querySelector("#result_head")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2');
            saveGraph("canvastg2",0); //TABLA DE HEAD COUNT
        }); */ //se comenta a peticion en 17 de enero de 2024
        html2canvas(document.querySelector("#table_presentismo_data")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2_2');
            saveGraph("canvastg2_2",4); //TABLA DE PRESENTISMO
        });
        html2canvas(document.querySelector("#table_turn_data")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg2_3');
            saveGraph("canvastg2_3",5); //TABLA DE TURN OVER
        });

        html2canvas(document.querySelector("#table_holly")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg3');
            saveGraph("canvastg3",6); //TABLA DE VACACIONES SEMANAS
        });
        html2canvas(document.querySelector("#table_holly_days")).then(canvas => {
        document.body.appendChild(canvas).setAttribute('id','canvastg4');
            saveGraph("canvastg4",7); //TABLA DE VACACIONES AÑO
        });
    }, 1500);
});

function saveGraph(idname,tipo){
    var canvasx= document.getElementById(idname);
    var dataURL = canvasx.toDataURL('image/png', 1);
    //console.log("tipo :"+tipo);
    $.ajax({
        type:'POST',
        url: base_url+'Reportes/insertGraphPresentismo',
        data: {
            id:0,
            id_proyecto:$("#id_proy").val(),
            grafica:dataURL,
            mes:$("#mes").val(),
            anio:$("#anio").val(),
            tipo:tipo
        },
        success:function(data){
            //console.log("hecho");
            setTimeout(function(){
                $.unblockUI();
            }, 4500);
        }
    });
}

</script>
