<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Nuevo clientes</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id" id="idcliente" value="<?php echo $id ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Nombre de servicio</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="text" name="servicio" value="<?php echo $servicio ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Descripción de servicio</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="text" name="descripcion" value="<?php echo $descripcion ?>">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>  
          <div class="card-footer">
            <div class="col-sm-9">
              <button class="btn btn-primary btn_registro" type="button" onclick="add_form()">Guardar datos</button>
              <a href="<?php echo base_url()?>Servicios" class="btn btn-light">Regresar</a>
            </div>
          </div>
        <!---->
      </div>
    </div>
  </div>
</div>