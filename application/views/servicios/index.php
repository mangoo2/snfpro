<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de servicios</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <a class="btn btn-primary" href="<?php echo base_url() ?>Servicios/registro">Nuevo Servicios</a>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Servicio</th>
                      <th scope="col">Usuario de captura</th>
                      <th scope="col">Fecha y hora</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>