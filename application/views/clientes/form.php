<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Nuevo clientes</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data" autocomplete="off">
          <input type="hidden" name="id" id="idcliente" value="<?php echo $id ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Empresa</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="text" name="empresa" value="<?php echo $empresa ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Contacto</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="text" name="contacto" value="<?php echo $contacto ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Teléfono</label>
                  <div class="col-sm-3">
                    <input class="form-control" type="text" name="telefono" value="<?php echo $telefono ?>">
                  </div>
                  <label class="col-sm-1 col-form-label">Email</label>
                  <div class="col-sm-5">
                    <input class="form-control" type="email" name="correo" id="correo" value="<?php echo $correo ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Dirección</label>
                  <div class="col-sm-9">
                    <input class="form-control m-input" type="text" name="direccion" value="<?php echo $direccion ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Número del cliente</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="number" name="num_cliente" value="<?php echo $num_cliente ?>">
                  </div>
                </div>

<!-------------------------------------------------------------------------->
  
                <div class="col-md-12 mt-5">
                  <h4 class="form-section"> Contactos</h4>
                  <table id="table_contactos_container" width="100%">
                    <tbody>
                      <tr id="tr_contactos">
                        <td width="30%">
                          <input type="hidden" id="id_prod" value="0">
                          <input type="hidden" id="estatus" value="1">
                          <div class="form-group">
                            <label class="col-form-label">Contacto</label>
                            <div class="controls">
                              <input type="text" id="contacto" class="form-control form-control-smr" value="">
                            </div>
                          </div>
                        </td>
                        
                        <td width="30%">
                          <div class="form-group">
                            <label class="col-form-label">Teléfono</label>
                            <div class="controls">
                              <input type="text" id="telefono" class="form-control form-control-sm" value="">
                            </div>
                          </div>
                        </td>

                        <td width="30%">
                          <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <div class="controls">
                              <input type="email" id="correo" class="form-control form-control-sm" value="">
                            </div>
                          </div>
                        </td>

                        <td width="30%">
                          <span style="color: transparent;">add</span>
                          <button type="button" class="btn btn-primary" title="Agregar nuevo contacto" onclick="CloneContact()"><i id="btn_plus" class="fa fa-plus" aria-hidden="true"></i></button>
                        </td>
                      </tr>
                      
                      <?php if (isset($id)) {
                        foreach ($contactos->result() as $i => $key) { ?>
                        <tr id="<?php echo 'tr_each_'.$i ?>">
                        <td width="30%">
                          <input type="hidden" id="id_prod" value="<?php echo $key->id; ?>">
                          <input type="hidden" id="estatus" value="1">
                          <div class="form-group">
                            <label class="col-form-label">Contacto</label>
                            <div class="controls">
                              <input type="text" id="contacto" class="form-control form-control-smr" value="<?php echo $key->contacto; ?>">
                            </div>
                          </div>
                        </td>
                        
                        <td width="30%">
                          <div class="form-group">
                            <label class="col-form-label">Teléfono</label>
                            <div class="controls">
                              <input type="text" id="telefono" class="form-control form-control-sm" value="<?php echo $key->telefono; ?>">
                            </div>
                          </div>
                        </td>
                        <td width="30%">
                          <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <div class="controls">
                              <input type="email" id="correo" class="form-control form-control-sm" value="<?php echo $key->correo; ?>">
                            </div>
                          </div>
                        </td>
                        <td width="30%">
                          <span class="required" style="color: transparent;">dele</span>
                          <a class="btn btn-danger" title="Eliminar contacto" onclick="eliminarContact(<?php echo $i ?>)"><i class="fa fa-minus" aria-hidden="true"></i></a>
                        </td>
                      </tr>
                      <?php }
                    } ?>
                    </tbody>
                  </table>
                </div>

                <hr>
                <div class="mb-3 row">
                  <div class="col-md-3">
                    <div class="item">
                      <label>Cargar logo de cliente</label>
                      <div class="card" align="center">
                        <span class="img_cliente">
                          <?php if($foto!=''){ ?>
                              <img id="img_avatar" src="<?php echo base_url(); ?>uploads/clientes/<?php echo $foto ?>" class="rounded mr-3" height="142" width="142">
                          <?php }else{ ?>
                              <img id="img_avatar" src="<?php echo base_url(); ?>public/img/avatar.png" class="rounded mr-3" height="142" width="142">
                          <?php } ?>
                        </span>
                      </div>
                    </div>
                  </div>  
                  <div class="col-md-3">
                    <label class="btn btn-light" for="foto_avatar" style="width: 100%">Cargar logo</label>
                    <input type="file" id="foto_avatar" hidden>
                    <br><br>
                    <a class="btn btn-light" style="width: 100%" onclick="reset_img()">Eliminar logo</a>
                    <br><br>
                    <a class="btn btn-primary" style="width: 100%">Generar contraseña</a>
                  </div>  
                  <div class="col-md-3">
                    <br><br><br>
                    <label class="col-form-label">Contraseña</label>
                    <input class="form-control" type="password" autocomplete="new-password" name="contrasena" id="contrasena" aria-autocomplete="list" value="<?php echo $contrasena ?>" autocomplete="nope">
                  </div>
                  <div class="col-md-3">
                    <br><br><br>
                    <label class="col-form-label">Confirmar contraseña</label>
                    <input class="form-control" type="password" d="contrasena2" name="contrasena2" value="<?php echo $contrasena ?>" autocomplete="nope">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>  
          <div class="card-footer">
            <div class="col-sm-9">
              <button class="btn btn-primary" type="button" onclick="add_form()">Guardar datos</button>
              <a href="<?php echo base_url()?>Clientes" class="btn btn-light">Regresar</a>
            </div>
          </div>
        <!---->
      </div>
    </div>
  </div>
</div>