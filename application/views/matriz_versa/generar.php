<meta name=viewport content="width=device-width,initial-scale=1,user-scalable=0">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
<!--<script src="<?php echo base_url(); ?>assets/js/chart/Chart.bundle.min.js"></script>-->
<script src="<?php echo base_url(); ?>plugins/chartjs-plugin/2.7.2/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/chartjs-plugin/chartjs-plugin-datalabels.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas141.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/dom-to-image.js"></script> 
<input type="hidden" name="id_proy" id="id_proy" value="<?php echo $id_proy;?>">
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url()?>">
<input type="hidden" id="reg" value="<?php echo date('YmdGis')?>">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
<style type="text/css">
	.th_actividad{
		writing-mode: vertical-rl;
		transform: rotate(180deg);
		height: 190px;

		-webkit-writing-mode: vertical-rl;
		-epub-writing-mode: vertical-rl;
    -webkit-text-combine: vertical-rl;
    -webkit-transform: rotate(180deg);

  	-moz-writing-mode: vertical-rl;
  	-ms-writing-mode: vertical-rl;

  	-moz-transform: rotate(180deg);
    -moz-text-orientation: vertical-rl;
    -ms-transform: rotate(180deg);
    -ms-text-orientation: vertical-rl;
    -o-transform: rotate(180deg);
    -o-text-orientation: vertical-rl;

		/*font-size: 12px;*/
				/*
				white-space:nowrap;
		    -webkit-transform: rotate(180deg);
		    -moz-transform: rotate(180deg);
		    -ms-transform: rotate(180deg);
		    -o-transform: rotate(180deg);
		    transform: rotate(180deg);
		    writing-mode: vertical-lr;
		   */
	}
	.th_actividad2{
		-webkit-height: 190px;
		-webkit-writing-mode: vertical-rl;
		-epub-writing-mode: vertical-rl;
    -webkit-text-combine: vertical-rl;
    /*-webkit-transform: rotate(0deg);*/
    text-align: left;
    /*padding-top: 1px;*/
    font-size: 11px;
	}
	.td_nombre{width: 164px !important;}
	.td_foto{
		width: 40px !important;
		max-width: 45px !important;
	}
	.img_emp{max-width: 250px;}
	@media(max-width: 650px){
		.th_actividad2{
			height: 240px;
			width: 70px;
			-webkit-writing-mode: vertical-rl;
			-epub-writing-mode: vertical-rl;
	    -webkit-text-combine: vertical-rl;
	    /*-webkit-transform: rotate(0deg);*/
	    text-align: left;
	    font-size: 8.5px;
	    font-weight: normal;
		}
		.img_emp{max-width: 70px;}
	}

	@media(max-width: 850px){
		.th_actividad2{
			height: 240px;
			width: 70px;
			-webkit-writing-mode: vertical-rl;
			-epub-writing-mode: vertical-rl;
	    -webkit-text-combine: vertical-rl;
	    /*-webkit-transform: rotate(0deg);*/
	    text-align: left;
	    font-size: 8.5px;
		}
		.img_emp{max-width: 90px;}
		.table_actividades td{min-width: 60px;}
		.td_nombre{min-width: 160px !important;}
		.td_row{min-width: 30px !important;}
		.td_row_rest{min-width: 33px !important;}
	}

	.vertical-text {
    /*writing-mode: vertical-rl;
    -moz-transform: scale(-1, -1);
    -webkit-transform: scale(-1, -1);
    -o-transform: scale(-1, -1);
    -ms-transform: scale(-1, -1);
    transform: scale(-1, -1);
    text-orientation: mixed;*/

    -epub-writing-mode: vertical-rl;
    -webkit-text-combine: vertical-rl;
	}

	.th_actividad.act{
		vertical-align:-webkit-baseline-middle;
		width: 70px;
	}
	.aling_td{
		text-align: -webkit-center;
	}
	.cuadritos{
		width: 12px;
		height: 12px;
		border: 1px solid black;
		float: left;
	}
	.cuadro{
		width: 30px;
		align-items: center;
		margin-left: auto;
    margin-right: auto;
	}
	.cuadritos.activo{
		background-color: green;
	}
	.cuadritos.activo2{
		background: yellow;
	}
	.table_actividades{
		width:auto;
		font-size: 11px;
	}
	.col_des_1,.col_des_2{
		padding-left: 30px;
		padding-right: 30px;
		font-size: 12px;
	}
	.actividadg{
    background-color: #e9dd79 !important;
  }
  .actividadh{
    background-color: #cadeef !important;
  }
  #canvasmt,#canvasmt1,#canvasmt2,#canvasmt3,#canvasmt4,#canvasmt5,#canvasmt6{display: none;}
  .td_foto{padding: 0px !important;}
  tfoot td{
  	padding: 2px 0.5rem !important;
  }
  body{
  	-webkit-print-color-adjust:exact !important;
  	print-color-adjust:exact !important;
	}
	@media print {
  	.col-md-4 {
		    -webkit-box-flex: 0;
		    -ms-flex: 0 0 auto;
		    flex: 0 0 auto;
		    width: 33.33333%;
		}
		.col-md-5 {
		    -webkit-box-flex: 0;
		    -ms-flex: 0 0 auto;
		    flex: 0 0 auto;
		    width: 41.66667%;
		}
	}
	.classview{display: none;}
	.table_info{font-size:8px;}
  .table > :not(caption) > * > * {padding: 8px;}
</style>
<?php
	$row_actividad=0; $row_actividad2=0;
	$row_actividad_group=0;
	$actividadesrow=array();
	foreach ($list_act->result() as $itemac) { 
		$row_actividad++; $row_actividad2++;
		if($row_actividad>15){
			$row_actividad=1;
			$row_actividad_group++;
		}
		$actividadesrow[$row_actividad_group]['pro'][]=$itemac;
	}

	$row_pers=0; $cont_pers=0; 
	$row_pers_group=0;
	$persrow=array();
	$r_p_row=0;
	foreach ($list_per->result() as $itemp) { 
		$row_pers++;
		//echo "<br>for lista de personas: ".$row_pers;
		//echo "<br>foto de lista de personas: ".$itemp->foto;
		if($row_pers>10){
			$row_pers=1;
			$row_pers_group++;
		}
		$persrow[$row_pers_group]['pro2'][]=$itemp;
	}
	//echo count($persrow);
	//var_dump($persrow);
	//var_dump($actividadesrow);
	$r_a_group=0;
	$actividadg=0;
	$actividadh=0;
	$aux_cont_act=0;
foreach ($persrow as $itemgp) {
	foreach ($itemgp['pro2'] as $itemp) { 
		${'totalcumplimientoemple_'.$itemp->personalId}=0;
	}
}
foreach ($persrow as $itemgp) {
	//var_dump($itemgp['pro2']);
	//$r_a_group=0;
	//$actividadg=0;
	//$actividadh=0;
	
	foreach ($actividadesrow as $itemg) {
		$r_a_group++; $aux_cont_act++;
		if(count($actividadesrow)==$r_a_group || (count($actividadesrow)*2)==$r_a_group){
			$classview='classviewt';
		}else{
			$classview='classview';
		}
		//var_dump($itemg);
		//==================================================================================================
			?>
				<table cellpadding="5" border="1" class="table-bordered table_actividades" id="table_actividades1_<?php echo $r_a_group;?>">
					<thead>
						<tr>
							<th colspan="3"><img class="img_emp" src="<?php echo base_url().'public/img/tipo_act.png';?>"></th>
							<?php 
								$totalobjetivom=0; $totalobjetivoh=0;
								
								//$totalh=0; $totalm=0;
								$cont_matriz=0;
								$html_falta="";
								foreach ($itemg['pro'] as $itemac) { 
									$cont_matriz++;
									${'totalobjetivom'.$itemac->id}=0;
									${'totalobjetivoh'.$itemac->id}=0;
									//echo "<br>cont_matriz: ".$cont_matriz;

									${'actividad_'.$itemac->id}=array();
									if($itemac->tipo==0){
					          $titulo_tipo='actividadg';
					          $actividadg++;
					          ${'totalobjetivom'.$itemac->id}++;
										${'totalobjetivoh'.$itemac->id}++;
					        }else{
					          $titulo_tipo='actividadh';
					          $actividadh++;
					          ${'totalobjetivoh'.$itemac->id}++;
					        }
									${'genero_act'.$itemac->id}=$itemac->tipo;
									//echo "<br>totalh: ".$totalh;
									//echo "<br>totalm: ".$totalm;
									//echo "<br>totalobjetivom : ".${'totalobjetivom'.$itemac->id}.", actividad ".$itemac->nombre;
									//echo "<br>totalobjetivoh : ".${'totalobjetivoh'.$itemac->id}.", actividad ".$itemac->nombre;
					        ${'total_operacion_'.$r_a_group.'_'.$itemac->id}=0;
								?>
								<th class="th_actividad act <?php echo $titulo_tipo;?>"><span class="th_actividad2"><?php echo $itemac->nombre;?></span></th>
							<?php } ?>
							<th class="th_actividad <?php echo $classview; ?>"><span class="th_actividad2">CUMPLIMIENTO</span></th>
							<th class="th_actividad <?php echo $classview; ?>"><span class="th_actividad2">OBJETIVO</span></th>
							<th class="th_actividad <?php echo $classview; ?>"><span class="th_actividad2">FALTA</span></th>
						</tr>
						<tr>
							<th>#</th>
							<th>FOTO</th>
							<th>NOMBRE</th>
							<th style="text-align: center;" colspan="<?php echo count($itemg['pro']);?>">HABILIDAD </th>
							<th class="<?php echo $classview; ?>"></th>
							<th class="<?php echo $classview; ?>"></th>
							<th class="<?php echo $classview; ?>"></th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$row=1; $fecha_update=""; $row_pers_group=0; $aux=0; $row_pers_group_ant=0;
							//foreach ($list_per->result() as $itemp) { 
							$totalobjetivom=0; $totalobjetivoh=0;
							$totalh=0; $totalm=0;
							foreach ($itemgp['pro2'] as $itemp) { 
								//echo "<br>genero: ".$itemp->genero;
								if($itemac->tipo==1){ //general
									$totalobjetivom++;
									$totalobjetivoh++;
								}else{ //unica de hombre
									$totalobjetivoh++;
								}
								if($itemp->genero==1){ //hombre
									$totalh++;
								}else if($itemp->genero==2){ //mujer
									$totalm++;
								}
								//echo "<br>totalh: ".$totalh;
								//echo "<br>totalm: ".$totalm;
								//${'totalcumplimientoemple_'.$itemp->personalId}=0;
								$r_p_row++;
								if($r_p_row>10){
									$r_p_row=0;
									$row_pers_group++;
								}
								//echo "<br>for lista de personas: ".$cont_pers;
							//}
								//echo "<br>row_pers_group: ".$row_pers_group;
								//echo "<br>row_pers_group_ant: ".$row_pers_group_ant;
							//if($row_pers_group>$row_pers_group_ant){
								//foreach ($persrow as $itemp) { 
									//echo "entra a lista de personas: ".$itemp['pro2']->foto;
									$html_falta="";
									/*$r_p_row++;
									if($r_p_row>10){
										$r_p_row=0;
									}*/					
									if($aux>9){
										$aux=0;
									}
									$aux++;
									$fecha_update=$itemp->fecha_update;
									if($itemp->foto!=''){ 
					        	$personal_foto=FCPATH.'uploads/empleados/'.$itemp->foto;
					        	if(file_exists($personal_foto)){
					        		$personal_foto=base_url().'uploads/empleados/'.$itemp->foto;
					        	}else{
					        		$personal_foto=base_url().'public/img/avatar.png';
					        	}
					      	}else{ 
					        	$personal_foto=base_url().'public/img/avatar.png';
					      	}
					      	//echo "tipo_incidencia: ".$itemp->tipo_incidencia;
					      	if($itemp->tipo_incidencia!=5){ ?>
										<tr>
											<td class="td_row"><?php echo $row;?></td>
											<td class="td_foto"><img id="img_avatar" src="<?php echo $personal_foto; ?>" class="rounded mr-3" width="37px"></td>
											<td class="td_nombre"><?php echo $itemp->nombre; ?></td>
											<?php
												$valor_actividad_td=0; 
												$valor_actividad=$this->Modelo_matriz_versatibilidad->list_actividades_empleado2($itemp->idproyecto,$itemp->personalId);
												foreach ($valor_actividad->result() as $itempv) {
													$itempv_id=$itempv->id;
													foreach ($itemg['pro'] as $itemac) {
														if($itemac->id==$itempv->id){
															$pintaact=1;
															${'actividad_'.$itempv->id}[]=array($itempv->valor,$itempv->estatus);
															if($itempv->valor>=3){
																${'total_operacion_'.$r_a_group.'_'.$itemac->id}++;
															}
															?>
															<td style="" class="">
																<div class="cuadro">
																	<div class="cuadritos <?php if($itempv->valor>=1 && $itempv->estatus==1 || $itempv->valor>1 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=1 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
																	<div class="cuadritos <?php if($itempv->valor>=2 && $itempv->estatus==1 || $itempv->valor>2 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=2 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
																	<div class="cuadritos <?php if($itempv->valor>=3 && $itempv->estatus==1 || $itempv->valor>=4 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=3 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
																	<div class="cuadritos <?php if($itempv->valor>=4 && $itempv->estatus==1){ echo 'activo';} else if($itempv->valor>=4 && $itempv->estatus==2){ echo 'activo2';}?>"></div>

																</div>
																<?php  //'total_operacion_'.$r_a_group.'_'.$itemac->id.' ('.$itempv->valor.')'?>
															</td>
														<?php 
															if($itempv->valor>=3){
																${'totalcumplimientoemple_'.$itemp->personalId}++;
															}
															$valor_actividad_td++;
														}
														// code...
													}	
													
																
												}

											if($cont_matriz>$valor_actividad_td){ 
												for($j=$valor_actividad_td; $j<$cont_matriz; $j++){
													/*log_message('error', 'valor_actividad_td : '.$valor_actividad_td);
													log_message('error', 'row_actividad : '.$row_actividad);
													log_message('error', 'j : '.$j);*/
													//if($valor_actividad_td<=$row_actividad){
														$html_falta.='<td><div class="cuadro"><div class="cuadritos"></div><div class="cuadritos"></div><div class="cuadritos"></div><div class="cuadritos"></div></div></td>';
													//}
												} 
												echo $html_falta;
											} //echo "faltante: ".$itemp->faltante; 
											if($itemac->cumplimiento==0) $faltante=$itemp->objetivo; else $faltante=$itemp->faltante;
											$faltante=$itemp->objetivo-${'totalcumplimientoemple_'.$itemp->personalId};
											if($faltante<0){
												$faltante=0;
											}
											?>
											<td class="<?php echo $classview; ?> td_row_rest"><?php echo ${'totalcumplimientoemple_'.$itemp->personalId}; ?></td>
											<td class="<?php echo $classview; ?> td_row_rest"><?php echo $itemp->objetivo; ?></td>
											<td class="<?php echo $classview; ?> td_row_rest"><?php echo $faltante; ?></td>
										</tr>
								<?php } //if de incidencia
								$row_pers_group_ant=$row_pers_group;
								//}//for de persrow
							//}//if de r_p_row
							$row_ant=$r_p_row; 
							$row++; 
						}
						?>
					</tbody>
					<tfoot>
						<?php foreach ($list_act->result() as $itemac) { 
								$totaloperacion=0;
								//$totalobjetivo=0;
								$totalfalta=0;
								foreach (${'actividad_'.$itemac->id} as $itemvg) {
									//echo "<br>totalobjetivom: ".${'totalobjetivom'.$itemvg->id};
									//echo "<br>totalobjetivoh: ".${'totalobjetivoh'.$itemvg->id};
									if($itemvg[0]>=3){
										if($itemvg[0]==3 && $itemvg[1]==1){ //1=completado, 2 = capacitacion
											$totaloperacion++;
										}
										if($itemvg[0]==4){
											$totaloperacion++;
										}
									}
									/*if($itemvg[0]>0){
										$totalobjetivo++;
									}*/
									//echo "<br>totalh: ".$totalh;
									//echo "<br>totalm: ".$totalm;
									if(${'genero_act'.$itemac->id}==0){ //general
										//echo "<br>col de actividad general";
										$totalobjetivo=$totalh+$totalm;
									}else{
										//echo "<br>col de actividad unica de hombres";
										$totalobjetivo=$totalh;
									}
								}
								${'totaloperacion_'.$itemac->id}=$totaloperacion;
								${'totalobjetivo_'.$itemac->id}=$totalobjetivo;
								${'totalfalta_'.$itemac->id}=$totalobjetivo-$totaloperacion;
							} ?>
						<tr>
							<td colspan="3" >Empleados calificados por operación</td>
							<?php foreach ($list_act->result() as $itemac) { 
													foreach ($itemg['pro'] as $itemac_a) {
															if($itemac->id==$itemac_a->id){
																echo '<td>'.${'totaloperacion_'.$itemac->id}.'</td>';
															}
													}
							 			} ?>
						</tr>
						<tr>
							<td colspan="3" >Objetivo</td>
							<?php foreach ($list_act->result() as $itemac) { 
													foreach ($itemg['pro'] as $itemac_a) {
															if($itemac->id==$itemac_a->id){
																echo '<td>'.${'totalobjetivo_'.$itemac->id}.'</td>';
															}
													}
							 	} ?>
						</tr>
						<tr>
							<td colspan="3">Falta</td>
							<?php foreach ($list_act->result() as $itemac) { 
											foreach ($itemg['pro'] as $itemac_a) {
															if($itemac->id==$itemac_a->id){
																	echo '<td>'.${'totalfalta_'.$itemac->id}.'</td>';
															}
											}
							 	} ?>
						</tr>
						<tr>
							<td colspan="<?php if($classview=="classview") echo $cont_matriz+3; else echo $cont_matriz+6; ?>">
								<table border="1" class="table_info" cellpadding="1" id="sub_table_info<?php echo $r_a_group;?>" style="display:none">
			            <tr>
			                <td width="2%"><img width="9px" src="<?php echo base_url();?>public/img/capacitado.png"></td>
			                <td width="6%">Capacitado</td>
			                <td width="2%"><img width="9px" src="<?php echo base_url();?>public/img/capacitacion.png"></td>
			                <td width="6%">Capacitación</td>
			                <td width="5%"></td>
			                <td><b><u>Criterios de Certificación</u></b></td>
			            </tr>
			            <tr>
			                <td width="2%"><img width="10px" src="<?php echo base_url();?>public/img/cuadro1.png" ></td>
			                <td width="30%"><b>N. 1: Aprende a llevar a cabo tareas básicas y a desempeñar la operación.</b></td>
			                <td width="2%"><img width="10px" src="<?php echo base_url();?>public/img/cuadro2.png" ></td>
			                <td width="20%"><b>N. 2: Habilitado para desempeñar la operación</b></td>
			                <td width="2%"><img width="10px" src="<?php echo base_url();?>public/img/cuadro3.png" ></td>
			                <td width="20%"><b>N. 3: Certificado para desempeñar la operación</b></td>
			                <td width="2%"><img width="10px" src="<?php echo base_url();?>public/img/cuadro4.png" ></td>
			                <td width="20%"><b>N. 4: Certificado para enseñar la operación</b></td>
			            </tr>
			        	</table>
							</td>
						</tr>
					</tfoot>
				</table>
				<div class="row col_des_1">
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-1">
								<div class="cuadro">
									<div class="cuadritos activo"></div>
								</div>
							</div>
							<div class="col-md-9">
								<ul>
									<li>Capacitado</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-md-1">
								<div class="cuadro">
									<div class="cuadritos activo2"></div>
								</div>
							</div>
							<div class="col-md-9">
								<ul>
									<li>Capacitación</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<p><b><u>Fecha de ultima actualización: </u><?php echo $fecha_update; ?></b></p>
					</div>
					<div class="col-md-4 col_des_1" >
						<p><b><u>Matriz de Versatilidad</u></b></p>
						<p>La matriz de versatilidad es la herramienta usada para documentar a los miembros de equipo que son entrenados o certificados para cada operación o maquina en el área de trabajo o actividad especifica. La matriz establece la flexibilidad de la fuerza de trabajo, la rotación del equipo, y la planeación para <span style="color:red;">el entrenamiento en los procesos</span>.</p>
						<p>El líder del equipo revisa y actualiza la matriz de versatilidad con ayuda del supervisor.</p>
						<p><b>Como completar la matriz</b></p>
						<ol>
						  <li>Enlistar todos los miembros del equipo en la columna NOMBRE.</li>
						  <li>Enlistar todas las operaciones o nombres de máquinas en el renglón OPERACION/ACTIVIDAD. Agrega cualquier otra tarea que sea responsable el departamento/equipo.</li>
						  <li>Identifica todas las características críticas y significativas, u operaciones de la actividad.</li>
						  <li>Documentar los estatus de certificaciones de  los miembros del equipo de acuerdo al criterio de certificación. Después que el miembro del equipo se certificó al nivel 3 o 4, añadir la fecha de la certificación bajo las casillas sombreadas en la plantilla.</li>
						  <li>El total abajo de las columnas determina cuantos miembros del equipo están certificados para cada operación o máquina.</li>
						  <li>El total de los renglones determina cuantas operaciones o maquinas está certificado para desempeñar el miembro del equipo. Publicar la matriz de versatilidad en el tablero.</li>
						</ol> 
						<p>La matriz de versatilidad debe estar actualizada. Sera revisada cada vez que haya un cambio en cualquier operación, maquina o en una hoja de proceso de calidad; que exista un nuevo miembro en el equipo; o que existan miembros del equipo que necesiten ser certificados o ser recertificados. Mínimo la matriz debe ser revisada una vez al mes.</p>
					</div>
					<div class="col-md-5 col_des_2">
						<p><b><u>Criterios de Certificación</u></b></p>
						<p><b>Nivel 1: Aprende a llevar a cabo tareas básicas y a desempeñar la operación.</b></p>
						<div class="row">
							<div class="col-md-2">
								<div class="cuadro"><div class="cuadritos activo"></div><div class="cuadritos"></div><div class="cuadritos"></div><div class="cuadritos"></div></div>
							</div>
							<div class="col-md-9">
								<ul>
									<li>Lee copia de los instructivos de trabajo y AST y puede repetir los puntos claves</li>
									<li>Aprende la operación y las medidas de seguridad requeridas.</li>
									<li>Aprende las responsabilidades básicas del rol.</li>
									<li>Requiere asistencia al 100%</li>
								</ul>
							</div>
						</div>

						<p><b>Nivel 2: Habilitado para desempeñar la operación</b></p>
						<div class="row">
							<div class="col-md-2">
								<div class="cuadro"><div class="cuadritos activo"></div><div class="cuadritos activo"></div><div class="cuadritos "></div><div class="cuadritos "></div></div>
							</div>
							<div class="col-md-9">
								<ul>
									<li>Comienza a desarrollar un mayor conocimiento de la operación, mejor entendimiento y la importancia 
				                    de seguir los estándares en procesos de la seguridad, calidad y el tiempo de las actividades</li>
									<li>Desempeña la operación por un turno completo.
										<ul>
											<li>Sin leer copia de IT y AST</li>
											<li>Sin ayuda</li>
											<li>Dentro del tiempo de ciclo</li>
											<li>Bajo los estándares</li>
										</ul>
									</li>
									<li>Maneja las anomalías con la asistencia</li>
								</ul>
							</div>
						</div>
						<p>*Debido a la variación que pueden presentar algunos elementos de la operación, velocidad de la actividad, y la complejidad de la producción, el líder y el supervisor pueden ajustar el numero de repeticiones requeridas para la certificación.</p>
						<p><b>Nivel 3: Certificado para desempeñar la operación</b></p>
						<div class="row">
							<div class="col-md-2">
								<div class="cuadro"><div class="cuadritos activo"></div><div class="cuadritos activo"></div><div class="cuadritos activo"></div><div class="cuadritos "></div></div>
							</div>
							<div class="col-md-9">
								<ul>
									<li>Continua desarrollando mas conocimiento de la operación, frecuentemente las áreas de oportunidad son identificadas y comentadas</li>
									<li>Desempeña la operación bajo estándar y tiempo</li>
									<li>Maneja anomalías independientemente</li>
								</ul>
							</div>
						</div>
						<p>El líder y el supervisor certifican que el miembro del equipo es capaz de llevar a cabo la operación. </p>
						<p><b>Nivel 4: Certificado para enseñar la operación</b></p>
						<div class="row">
							<div class="col-md-2">
								<div class="cuadro"><div class="cuadritos activo"></div><div class="cuadritos activo"></div><div class="cuadritos activo"></div><div class="cuadritos activo"></div></div>
							</div>
							<div class="col-md-9">
								<ul>
									<li>Desempeña la operación bajo estándar y tiempo</li>
									<li>Maneja anomalías independientemente</li>
									<li>Tiene un conocimiento profundo acerca de la operación</li>
									<li>Puede enseñar todo el conocimiento de la operación, conoce el procedimiento de Entrenamiento.</li>
								</ul>
							</div>
						</div>
						<p>Los líderes de los equipos deben estar certificados para enseñar todas las operaciones que se lleven a cabo en su área.</p>
					</div>
				</div>
				<div style="min-width: 900px;">
					<canvas id="bar-chart<?php echo $r_a_group;?>" width="800" height="450"></canvas>
				</div>
				<script type="text/javascript">
					var base_url = $('#base_url').val();
					var proy = $('#id_proy').val();
					$(document).ready(function($) {
						new Chart(document.getElementById("bar-chart<?php echo $r_a_group;?>"), {
					    type: 'bar',
					    data: {
					      labels: [<?php foreach ($itemg['pro'] as $itemac) {echo "'$itemac->nombre',";} ?>],
					      datasets: [
					        {
					        	type: 'line',
					          label: "Objetivo",
					          borderColor: "green",
					          data: [<?php foreach ($itemg['pro'] as $itemac) {echo ${'totalobjetivo_'.$itemac->id}.',';} ?>]
					        },
					        {
					        	type: 'bar',
					          label: "CALIFICADOS",
					          backgroundColor: "#3e95cd",
					          data: [<?php foreach ($itemg['pro'] as $itemac) {echo ${'totaloperacion_'.$itemac->id}.',';} ?>],
					          fill: false,
					        }
					        
					      ]
					    },
					    options: {
					      legend: { 
					      		display: true 
					      },
					      title: {
					        display: true,
					        text: 'EMPLEADOS CALIFICADOS'
					      },
					      scales: {
						      xAxes: [{
						        ticks: {
						          autoSkip: false,
						          rotation: -90
						        }
						      }],
						      yAxes: [{
				            display: true,
				            ticks: {
				                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
				                // OR //
				                beginAtZero: true   // minimum value will be 0.
				            }
				        }]
						    },
						    plugins: {
					        datalabels: {
					          formatter: (value) => {
					             return value;
					          },
					          //color: '#fff',
					          anchor: 'center',
                    align: 'top',
					          font: {
					            family: '"Times New Roman"',
					            size: "18",
					            weight: "bold",
					            color:"black"
					          },
					        },
					      },
					    }
						});

						/*setTimeout(function(){
							savechart1(<?php echo $r_a_group;?>); 
							savetable(<?php echo $r_a_group;?>); 
						}, 2500);*/
						setTimeout(function(){
							//savechart1(<?php echo $r_a_group;?>); 
							savetable(<?php echo $r_a_group;?>); 
						}, 3500);
						setTimeout(function(){
							savechart1(<?php echo $r_a_group;?>); 
							//savetable(<?php echo $r_a_group;?>); 
						}, 4000);

					});

				</script>
				<?php
		//==================================================================================================
	}
}

	$actividadg=$actividadg/count($persrow);
	$actividadh=$actividadh/count($persrow);


?>			
			<div class="row" id="pie-chart_div" style="width:900px">
				<div class="col-md-12">
					<table class="table table-bordered" style="width:600px">
						<tr>
							<td>Actividades de hombres y mujeres</td>
							<td><?php echo $actividadg;?></td>
						</tr>
						<tr>
							<td>Actividades exclusivas de hombres</td>
							<td><?php echo $actividadh;?></td>
						</tr>
					</table>
					<div style="width:900px" id="pie-chart_div2">
						<canvas id="pie-chart"></canvas>
					</div>
					
				</div>
			</div>
		<script type="text/javascript">
			$(document).ready(function($) {

				var total = <?php echo intval($actividadg)+intval($actividadh);?>;
				var actividadg = <?php echo $actividadg;?> * 100/total;
				var actividadh = <?php echo $actividadh;?> * 100/total;
				/*console.log("total: "+total);
				console.log("actividadg: "+actividadg);
				console.log("actividadh: "+actividadh);*/
				new Chart(document.getElementById("pie-chart"), {
				    type: 'pie',
				    data: {
				      labels: ["Actividades de hombres y mujeres", "Actividades exclusivas de hombres"],
				      datasets: [{
				        label: "Population (millions)",
				        backgroundColor: ["#e9dd79", "#5caae9"],
				        data: [parseFloat(actividadg).toFixed(2),parseFloat(actividadh).toFixed(2)]
				      }]
				    },
				    options: {
				    	legend: { 
				      		//position: 'right' ,
				      		//align: "center",
				      },
				      title: {
				        display: true,
				        text: 'ACTIVIDADES'
				      },
				      plugins: {
				        datalabels: {
				            formatter: (value) => {
				                return value+"%";
				            },
				            color: '#fff',
				            font: {
				              family: '"Times New Roman"',
				              size: "22",
				              weight: "bold",
				            },
				        },
				      },
				    }
				});
					setTimeout(function(){      
				  	//savechart1(<?php echo $r_a_group;?>);
				  	html2canvas(document.querySelector("#pie-chart_div")).then(canvas => {
				  		//if (documentClone.fonts && documentClone.fonts.ready) {
								var canvaspie= document.getElementById("pie-chart_div2");
					    	canvaspie.appendChild(canvas).setAttribute('id','canvasmt');
					    	savechart2();
							//}
				  	});
				  	/*html2canvas(document.getElementById('pie-chart_div2'),{
			        allowTaint: true,
			        useCORS : true,
				    }).then(function(canvas) {
				      console.log("canvas: " + canvas);
				      document.getElementById('canvasmt').appendChild(canvas);
				    });*/
				}, 3000);
				$.blockUI({ 
		      message: '<div class="fa-3x spinner-grow" style="justify-content: center; align-items:center;" role="status" >\
		          \
		        </div><br>Generando resultados..... Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
		      css: { 
		          border: 'none', 
		          padding: '15px', 
		          backgroundColor: '#000', 
		          '-webkit-border-radius': '10px', 
		          '-moz-border-radius': '10px', 
		          opacity: .5, 
		          color: '#fff'
		      } 
		    });
			});
				//================================================================
					

				//===================================================================		
		</script>
				<div id="table_actividades0" style="display: none;">
				<table border="1" class="table table-bordered table_actividades" id="table_actividades2">
					<thead>
						<tr>
							<th colspan="3"><img width="275px" src="<?php echo base_url().'public/img/tipo_act.png';?>"></th>
							<?php 
								foreach ($list_act->result() as $itemac) { 
								${'actividad_'.$itemac->id}=array();
								if($itemac->tipo==0){
				          $titulo_tipo='actividadg';
				        }else{
				          $titulo_tipo='actividadh';
				        }
							?>
								<th class="th_actividad <?php echo $titulo_tipo;?>"><span class="th_actividad2"><?php echo $itemac->nombre;?></span></th>
							<?php } ?>
							
						</tr>
						<tr>
							<th>#</th>
							<th>FOTO</th>
							<th>NOMBRE</th>
							<th colspan="<?php echo $list_act->num_rows()?>">HABILIDAD</th>
							
						</tr>
					</thead>
					<tbody>
						<?php 
							$row=1;
							foreach ($list_per->result() as $itemp) { 
								if($itemp->foto!=''){ 
                	$personal_foto=base_url().'uploads/empleados/'.$itemp->foto;
              	}else{ 
                	$personal_foto=base_url().'public/img/avatar.png';
              	}
						?>
							<tr>
								<td class="td_row"><?php echo $row;?></td>
								<td class="td_foto"><img id="img_avatar" src="<?php echo $personal_foto; ?>" class="rounded mr-3" width="50px"></td>
								<td class="td_nombre"><?php echo $itemp->nombre; ?></td>
								<?php 
									$valor_actividad=$this->Modelo_matriz_versatibilidad->list_actividades_empleado($itemp->idproyecto,$itemp->personalId);
									foreach ($valor_actividad->result() as $itempv) { 
									?>
									<td>
										<div class="cuadro">
											<!--<div class="cuadritos <?php if($itempv->valor>=1){ echo 'activo';}?>"></div>
											<div class="cuadritos <?php if($itempv->valor>=2){ echo 'activo';}?>"></div>
											<div class="cuadritos <?php if($itempv->valor>=3){ echo 'activo';}?>"></div>
											<div class="cuadritos <?php if($itempv->valor>=4){ echo 'activo';}?>"></div>-->

											<div class="cuadritos <?php if($itempv->valor>=1 && $itempv->estatus==1 || $itempv->valor>1 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=1 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
											<div class="cuadritos <?php if($itempv->valor>=2 && $itempv->estatus==1 || $itempv->valor>2 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=2 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
											<div class="cuadritos <?php if($itempv->valor>=3 && $itempv->estatus==1 || $itempv->valor>=4 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=3 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
											<div class="cuadritos <?php if($itempv->valor>=4 && $itempv->estatus==1){ echo 'activo';} else if($itempv->valor>=4 && $itempv->estatus==2){ echo 'activo2';}?>"></div>

										</div>
									</td>
								<?php } 
								if($cont_matriz>$valor_actividad->num_rows()){ 
									echo $html_falta;
								} 
								?>
								
							</tr>
						<?php
							$row++; 
							} ?>
					</tbody>
				</table>
				</div>

<script type="text/javascript">
	var proy = $('#id_proy').val();
	function savetable(idgroup){
		//console.log("savetable --- idgroup: "+idgroup);

		var node = document.getElementById('table_actividades1_'+idgroup);
		$("#sub_table_info"+idgroup+"").show();
		domtoimage.toPng(node)
		    .then (function (dataUrl) {
		        //var img = new Image();
		        //console.log(dataUrl);
		        $.ajax({
		          type:'POST',
		          url: base_url+'Matriz_versatibilidad/savegrafo2_2',
		          async:false,
		          data: {
		                  id:proy,
		                  name:'tabla',
		                  grafica:dataUrl,
		                  row:idgroup,
		                  codigo:'<?php echo date('YmdGis')?>'
		              },
		          statusCode:{
		              404: function(data){
		                  
		              },
		              500: function(){
		                  
		              }
		          },
		          success:function(data){
		             //console.log("hecho savetable");
		          	$("#sub_table_info"+idgroup+"").hide();
		          }
		      });
		    })
		    .catch(function (error) {
		       	console.error('oops, something went wrong!', error);
		    });
	}

function savechart2(){
	setTimeout(function(){  
		var base_url = $('#base_url').val();
		var proy = $('#id_proy').val();
	      var canvasx= document.getElementById("canvasmt");
	      var dataURL = canvasx.toDataURL('image/png', 1);
	      $.ajax({
	          type:'POST',
	          url: base_url+'Matriz_versatibilidad/savegrafo2',
	          data: {
	                  id:proy,
	                  name:'charpie',
	                  grafica:dataURL,
	                  row:1,
		                codigo:'<?php echo date('YmdGis')?>'
	              },
	          statusCode:{
	              404: function(data){
	                  
	              },
	              500: function(){
	                  
	              }
	          },
	          success:function(data){
	             //console.log("hecho");
	          }
	      });
  }, 2000);
}
function savechart1(idgroup){
		var base_url = $('#base_url').val();
		var proy = $('#id_proy').val();
    var canvasx= document.getElementById("bar-chart"+idgroup);
    var dataURL = canvasx.toDataURL('image/png', 1);
    $.ajax({
        type:'POST',
        url: base_url+'Matriz_versatibilidad/savegrafo2',
        data: {
                id:proy,
                name:'charbar',
                grafica:dataURL,
                row:idgroup,
	              codigo:'<?php echo date('YmdGis')?>'
            },
        statusCode:{
            404: function(data){
                
            },
            500: function(){
                
            }
        },
        success:function(data){
          setTimeout(function(){
            $.unblockUI();
          }, 12500);
        }
    });
	}
</script>