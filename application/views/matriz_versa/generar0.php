<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
<!--<script src="<?php echo base_url(); ?>assets/js/chart/Chart.bundle.min.js"></script>-->
<script src="<?php echo base_url(); ?>plugins/chartjs-plugin/2.7.2/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/chartjs-plugin/chartjs-plugin-datalabels.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/html2canvas141.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/dom-to-image.js"></script> 
<input type="hidden" name="id_proy" id="id_proy" value="<?php echo $id_proy;?>">
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url()?>">
<style type="text/css">
	.th_actividad{
		writing-mode: vertical-rl;
		transform: rotate(180deg);
		height: 156px;
		/*font-size: 12px;*/

				/*
				white-space:nowrap;
		    -webkit-transform: rotate(180deg);
		    -moz-transform: rotate(180deg);
		    -ms-transform: rotate(180deg);
		    -o-transform: rotate(180deg);
		    transform: rotate(180deg);
		    writing-mode: vertical-lr;
		   */

	}
	.cuadritos{
		width: 15px;
		height: 15px;
		border: 1px solid black;
		float: left;
	}
	.cuadro{
		width: 30px;
	}
	.cuadritos.activo{
		background-color: green;
	}
	.cuadritos.activo2{
		background: yellow;
	}
	.table_actividades{
		width:auto;
		font-size: 11px;
	}
	.col_des_1,.col_des_2{
		padding-left: 30px;
		padding-right: 30px;
		font-size: 12px;
	}
	.actividadg{
    background-color: #e9dd79 !important;
  }
  .actividadh{
    background-color: #5caae9 !important;
  }
  #canvasmt{
  	display: none;
  }
  .td_foto{
  	padding: 0px !important;
  }
  body{
  	-webkit-print-color-adjust:exact !important;
  	print-color-adjust:exact !important;
	}
</style>

<table border="1" class="table table-bordered table_actividades" id="table_actividades1">
	<thead>
		<tr>
			<th colspan="3">
					<img width="275px" src="<?php echo base_url().'public/img/tipo_act.png';?>">
			</th>
			<?php 
				$actividadg=0;
				$actividadh=0;
				$cont_matriz=0;
				$html_falta="";
				foreach ($list_act->result() as $itemac) { 
					$cont_matriz++;
					${'actividad_'.$itemac->id}=array();
					if($itemac->tipo==0){
	          $titulo_tipo='actividadg';
	          $actividadg++;
	        }else{
	          $titulo_tipo='actividadh';
	          $actividadh++;
	        }
				?>
				<th class="th_actividad <?php echo $titulo_tipo;?>"><?php echo $itemac->nombre;?></th>
			<?php } ?>
			<th class="th_actividad">CUMPLIMIENTO</th>
			<th class="th_actividad">OBJETIVO</th>
			<th class="th_actividad">FALTA</th>
		</tr>
		<tr>
			<th>#</th>
			<th>FOTO</th>
			<th>NOMBRE</th>
			<th colspan="<?php echo $list_act->num_rows()?>">HABILIDAD</th>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$row=1;
			foreach ($list_per->result() as $itemp) { 
				$html_falta="";
				if($itemp->foto!=''){ 
        	$personal_foto=base_url().'uploads/empleados/'.$itemp->foto;
      	}else{ 
        	$personal_foto=base_url().'public/img/avatar.png';
      	}
      	//echo "tipo_incidencia: ".$itemp->tipo_incidencia;
      	if($itemp->tipo_incidencia!=5){
		?>
			<tr>
				<td><?php echo $row;?></td>
				<td class="td_foto"><img id="img_avatar" src="<?php echo $personal_foto; ?>" class="rounded mr-3" width="50px"></td>
				<td><?php echo $itemp->nombre; ?></td>
				<?php 
					$valor_actividad=$this->Modelo_matriz_versatibilidad->list_actividades_empleado($itemp->idproyecto,$itemp->personalId);
					foreach ($valor_actividad->result() as $itempv) {
						${'actividad_'.$itempv->id}[]=array($itempv->valor,$itempv->estatus);
					?>
					<td>
						<div class="cuadro">
							<div class="cuadritos <?php if($itempv->valor>=1 && $itempv->estatus==1 || $itempv->valor>1 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=1 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=2 && $itempv->estatus==1 || $itempv->valor>2 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=2 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=3 && $itempv->estatus==1 || $itempv->valor>=4 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=3 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=4 && $itempv->estatus==1){ echo 'activo';} else if($itempv->valor>=4 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
						</div>
					</td>
				<?php }

				if($cont_matriz>$valor_actividad->num_rows()){ 
					for($j=$valor_actividad->num_rows(); $j<$cont_matriz; $j++){
						$html_falta.='<td>
							<div class="cuadro">
								<div class="cuadritos"></div>
								<div class="cuadritos"></div>
								<div class="cuadritos"></div>
								<div class="cuadritos"></div>
							</div>
						</td>';
					} 
					echo $html_falta;
				} ?>
				<td><?php echo $itemp->cumplimiento; ?></td>
				<td><?php echo $itemp->objetivo; ?></td>
				<td><?php echo $itemp->faltante; ?></td>
			</tr>
		<?php
				$row++; 
				} 
			}
		?>
	</tbody>
	<tfoot>
		<?php foreach ($list_act->result() as $itemac) { 
				$totaloperacion=0;
				$totalobjetivo=0;
				$totalfalta=0;
				foreach (${'actividad_'.$itemac->id} as $itemvg) {
					if($itemvg[0]>=3){
						if($itemvg[0]==3 && $itemvg[1]==1){ //1=completado, 2 = capacitacion
							$totaloperacion++;
						}
						if($itemvg[0]==4){
							$totaloperacion++;
						}
					}
					if($itemvg[0]>0){
						$totalobjetivo++;
					}
					// code...
				}
				${'totaloperacion_'.$itemac->id}=$totaloperacion;
				${'totalobjetivo_'.$itemac->id}=$totalobjetivo;
				${'totalfalta_'.$itemac->id}=$totalobjetivo-$totaloperacion;
			} ?>
		<tr>
			<td colspan="3">Empleados calificados por operación</td>
			<?php foreach ($list_act->result() as $itemac) { 
				echo '<td>'.${'totaloperacion_'.$itemac->id}.'</td>';
			 	} ?>
		</tr>
		<tr>
			<td colspan="3">Objetivo</td>
			<?php foreach ($list_act->result() as $itemac) { 
				echo '<td>'.${'totalobjetivo_'.$itemac->id}.'</td>';
			 	} ?>
		</tr>
		<tr>
			<td colspan="3">Falta</td>
			<?php foreach ($list_act->result() as $itemac) { 
				echo '<td>'.${'totalfalta_'.$itemac->id}.'</td>';
			 	} ?>
		</tr>
	</tfoot>
	
</table>


<div class="row">
	<div class="col-md-4 col_des_1" >
		<p><b><u>Matriz de Versatilidad</u></b></p>
		<p>La matriz de versatilidad es la herramienta usada para documentar a los miembros de equipo que son entrenados o certificados para cada operación o maquina en el área de trabajo o actividad especifica. La matriz establece la flexibilidad de la fuerza de trabajo, la rotación del equipo, y la planeación para <span style="color:red;">el entrenamiento en los procesos</span>.</p>
		<p>El líder del equipo revisa y actualiza la matriz de versatilidad con ayuda del supervisor.</p>
		<p><b>Como completar la matriz</b></p>
		<ol>
		  <li>Enlistar todos los miembros del equipo en la columna NOMBRE.</li>
		  <li>Enlistar todas las operaciones o nombres de máquinas en el renglón OPERACION/ACTIVIDAD. Agrega cualquier otra tarea que sea responsable el departamento/equipo.</li>
		  <li>Identifica todas las características críticas y significativas, u operaciones de la actividad.</li>
		  <li>Documentar los estatus de certificaciones de  los miembros del equipo de acuerdo al criterio de certificación. Después que el miembro del equipo se certificó al nivel 3 o 4, añadir la fecha de la certificación bajo las casillas sombreadas en la plantilla.</li>
		  <li>El total abajo de las columnas determina cuantos miembros del equipo están certificados para cada operación o máquina.</li>
		  <li>El total de los renglones determina cuantas operaciones o maquinas está certificado para desempeñar el miembro del equipo. Publicar la matriz de versatilidad en el tablero.</li>
		  
		</ol> 
		<p>La matriz de versatilidad debe estar actualizada. Sera revisada cada vez que haya un cambio en cualquier operación, maquina o en una hoja de proceso de calidad; que exista un nuevo miembro en el equipo; o que existan miembros del equipo que necesiten ser certificados o ser recertificados. Mínimo la matriz debe ser revisada una vez al mes.</p>
	</div>
	<div class="col-md-5 col_des_2">
		<p><b><u>Criterios de Certificación</u></b></p>
		<p><b>Nivel 1: Aprende a llevar a cabo tareas básicas y a desempeñar la operación.</b></p>
		<div class="row">
			<div class="col-md-2">
				<div class="cuadro">
					<div class="cuadritos activo"></div>
					<div class="cuadritos "></div>
					<div class="cuadritos "></div>
					<div class="cuadritos "></div>
				</div>
			</div>
			<div class="col-md-9">
				<ul>
					<li>Lee copia de los instructivos de trabajo y AST y puede repetir los puntos claves</li>
					<li>Aprende la operación y las medidas de seguridad requeridas.</li>
					<li>Aprende las responsabilidades básicas del rol.</li>
					<li>Requiere asistencia al 100%</li>
				</ul>
			</div>
		</div>

		<p><b>Nivel 2: Habilitado para desempeñar la operación</b></p>
		<div class="row">
			<div class="col-md-2">
				<div class="cuadro">
					<div class="cuadritos activo"></div>
					<div class="cuadritos activo"></div>
					<div class="cuadritos "></div>
					<div class="cuadritos "></div>
				</div>
			</div>
			<div class="col-md-9">
				<ul>
					<li>Comienza a desarrollar un mayor conocimiento de la operación, mejor entendimiento y la importancia 
                    de seguir los estándares en procesos de la seguridad, calidad y el tiempo de las actividades</li>
					<li>Desempeña la operación por un turno completo.
						<ul>
							<li>Sin leer copia de IT y AST</li>
							<li>Sin ayuda</li>
							<li>Dentro del tiempo de ciclo</li>
							<li>Bajo los estándares</li>
						</ul>
					</li>
					<li>Maneja las anomalías con la asistencia</li>
				</ul>
			</div>
		</div>
		<p>*Debido a la variación que pueden presentar algunos elementos de la operación, velocidad de la actividad, y la complejidad de la producción, el líder y el supervisor pueden ajustar el numero de repeticiones requeridas para la certificación.</p>


		<p><b>Nivel 3: Certificado para desempeñar la operación</b></p>
		<div class="row">
			<div class="col-md-2">
				<div class="cuadro">
					<div class="cuadritos activo"></div>
					<div class="cuadritos activo"></div>
					<div class="cuadritos activo"></div>
					<div class="cuadritos "></div>
				</div>
			</div>
			<div class="col-md-9">
				<ul>
					<li>Continua desarrollando mas conocimiento de la operación, frecuentemente las áreas de oportunidad 
                    son identificadas y comentadas</li>
					<li>Desempeña la operación bajo estándar y tiempo</li>
					<li>Maneja anomalías independientemente</li>
				</ul>
			</div>
		</div>
		<p>El líder y el supervisor certifican que el miembro del equipo es capaz de llevar a cabo la operación. </p>



		<p><b>Nivel 4: Certificado para enseñar la operación</b></p>
		<div class="row">
			<div class="col-md-2">
				<div class="cuadro">
					<div class="cuadritos activo"></div>
					<div class="cuadritos activo"></div>
					<div class="cuadritos activo"></div>
					<div class="cuadritos activo"></div>
				</div>
			</div>
			<div class="col-md-9">
				<ul>
					<li>Desempeña la operación bajo estándar y tiempo</li>
					<li>Maneja anomalías independientemente</li>
					<li>Tiene un conocimiento profundo acerca de la operación</li>
					<li>Puede enseñar todo el conocimiento de la operación, conoce el procedimiento de Entrenamiento.</li>
				</ul>
			</div>
		</div>
		<p>Los líderes de los equipos deben estar certificados para enseñar todas las operaciones que se lleven a cabo en su área.</p>
	</div>
</div>
<canvas id="bar-chart" width="800" height="450"></canvas>
<div class="row" id="pie-chart_div" style="width:900px">
	<div class="col-md-12">
		<table class="table table-bordered" style="width:600px">
			<tr>
				<td>Actividades de hombres y mujeres</td>
				<td><?php echo $actividadg;?></td>
			</tr>
			<tr>
				<td>Actividades exclusivas de hombres</td>
				<td><?php echo $actividadh;?></td>
			</tr>
		</table>
		<div style="width:900px" id="pie-chart_div2">
			<canvas id="pie-chart"></canvas>
		</div>
		
	</div>
</div>

<script type="text/javascript">
	var base_url = $('#base_url').val();
	var proy = $('#id_proy').val();
	$(document).ready(function($) {
		setTimeout(function(){
			savetable(); 
		}, 2000);
		$.blockUI({ 
      message: '<div class="fa-3x spinner-grow" style="justify-content: center; align-items:center;" role="status" >\
          \
        </div><br>Generando resultados..... Espere porfavor <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>',
      css: { 
          border: 'none', 
          padding: '15px', 
          backgroundColor: '#000', 
          '-webkit-border-radius': '10px', 
          '-moz-border-radius': '10px', 
          opacity: .5, 
          color: '#fff'
      } 
    });
	});

	new Chart(document.getElementById("bar-chart"), {
    type: 'bar',
    data: {
      labels: [<?php foreach ($list_act->result() as $itemac) {echo "'$itemac->nombre',";} ?>],
      datasets: [
        {
        	type: 'line',
          label: "Objetivo",
          borderColor: "green",
          data: [<?php foreach ($list_act->result() as $itemac) {echo ${'totalobjetivo_'.$itemac->id}.',';} ?>]
        },
        {
        	type: 'bar',
          label: "CALIFICADOS",
          backgroundColor: "#3e95cd",
          data: [<?php foreach ($list_act->result() as $itemac) {echo ${'totaloperacion_'.$itemac->id}.',';} ?>],
          fill: false,
        }
        
      ]
    },
    options: {
      legend: { 
      		display: true 
      },
      title: {
        display: true,
        text: 'EMPLEADOS CALIFICADOS'
      },
      scales: {
	      xAxes: [{
	        ticks: {
	          autoSkip: false,
	          rotation: -90
	        }
	      }]
	    },
	    plugins: {
        datalabels: {
          formatter: (value) => {
             return value;
          },
          color: '#fff',
          font: {
            family: '"Times New Roman"',
            size: "18",
            weight: "bold",
          },
        },
      },
    }
});

var total = <?php echo intval($actividadg)+intval($actividadh);?>;
var actividadg = <?php echo $actividadg;?> * 100/total;
var actividadh = <?php echo $actividadh;?> * 100/total;
/*console.log("total: "+total);
console.log("actividadg: "+actividadg);
console.log("actividadh: "+actividadh);*/
new Chart(document.getElementById("pie-chart"), {
    type: 'pie',
    data: {
      labels: ["Actividades de hombres y mujeres", "Actividades exclusivas de hombres"],
      datasets: [{
        label: "Population (millions)",
        backgroundColor: ["#e9dd79", "#5caae9"],
        data: [parseFloat(actividadg).toFixed(3),parseFloat(actividadh).toFixed(3)]
      }]
    },
    options: {
    	legend: { 
      		//position: 'right' ,
      		//align: "center",
      },
      title: {
        display: true,
        text: 'ACTIVIDADES'
      },
      plugins: {
        datalabels: {
            formatter: (value) => {
                return value+"%";
            },
            color: '#fff',
            font: {
              family: '"Times New Roman"',
              size: "22",
              weight: "bold",
            },
        },
      },
    }
});
	setTimeout(function(){      
  	savechart1();
  	html2canvas(document.querySelector("#pie-chart_div")).then(canvas => {
  		var canvaspie= document.getElementById("pie-chart_div2");
    	canvaspie.appendChild(canvas).setAttribute('id','canvasmt');
    	savechart2();
  	});
}, 3000);
//================================================================
	function savetable(){
		var node = document.getElementById('table_actividades1');

		domtoimage.toPng(node)
		    .then (function (dataUrl) {
		        //var img = new Image();
		        //console.log(dataUrl);
		        $.ajax({
		          type:'POST',
		          url: base_url+'Matriz_versatibilidad/savegrafo',
		          data: {
		                  id:proy,
		                  name:'tabla',
		                  grafica:dataUrl
		              },
		          statusCode:{
		              404: function(data){
		                  
		              },
		              500: function(){
		                  
		              }
		          },
		          success:function(data){
		             //console.log("hecho savetable");
		          }
		      });
		    })
		    .catch(function (error) {
		       	console.error('oops, something went wrong!', error);
		    });
	}

//===================================================================
function savechart1(){
	var base_url = $('#base_url').val();
	var proy = $('#id_proy').val();
      var canvasx= document.getElementById("bar-chart");
      var dataURL = canvasx.toDataURL('image/png', 1);
      $.ajax({
          type:'POST',
          url: base_url+'Matriz_versatibilidad/savegrafo',
          data: {
                  id:proy,
                  name:'charbar',
                  grafica:dataURL
              },
          statusCode:{
              404: function(data){
                  
              },
              500: function(){
                  
              }
          },
          success:function(data){
             //console.log("hecho");
          }
      });
}
function savechart2(){
	setTimeout(function(){  
		var base_url = $('#base_url').val();
		var proy = $('#id_proy').val();
    var canvasx= document.getElementById("canvasmt");
    var dataURL = canvasx.toDataURL('image/png', 1);
    $.ajax({
        type:'POST',
        url: base_url+'Matriz_versatibilidad/savegrafo',
        data: {
                id:proy,
                name:'charpie',
                grafica:dataURL
            },
        statusCode:{
            404: function(data){
                
            },
            500: function(){
                
            }
        },
        success:function(data){
          setTimeout(function(){
            $.unblockUI();
          }, 1500);
        }
    });
  }, 2000);
}
</script>
<div id="table_actividades0" style="display: none;">
<table border="1" class="table table-bordered table_actividades" id="table_actividades2">
	<thead>
		<tr>
			<th colspan="3">
				<img width="275px" src="<?php echo base_url().'public/img/tipo_act.png';?>">
			</th>
			<?php 
				foreach ($list_act->result() as $itemac) { 
				${'actividad_'.$itemac->id}=array();
				if($itemac->tipo==0){
          $titulo_tipo='actividadg';
        }else{
          $titulo_tipo='actividadh';
        }
			?>
				<th class="th_actividad <?php echo $titulo_tipo;?>"><?php echo $itemac->nombre;?></th>
			<?php } ?>
			
		</tr>
		<tr>
			<th>#</th>
			<th>FOTO</th>
			<th>NOMBRE</th>
			<th colspan="<?php echo $list_act->num_rows()?>">HABILIDAD</th>
			
		</tr>
	</thead>
	<tbody>
		<?php 
			$row=1;
			foreach ($list_per->result() as $itemp) { 
				if($itemp->foto!=''){ 
                	$personal_foto=base_url().'uploads/empleados/'.$itemp->foto;
              	}else{ 
                	$personal_foto=base_url().'public/img/avatar.png';
              	}
		?>
			<tr>
				<td><?php echo $row;?></td>
				<td class="td_foto"><img id="img_avatar" src="<?php echo $personal_foto; ?>" class="rounded mr-3" width="50px"></td>
				<td><?php echo $itemp->nombre; ?></td>
				<?php 
					$valor_actividad=$this->Modelo_matriz_versatibilidad->list_actividades_empleado($itemp->idproyecto,$itemp->personalId);
					foreach ($valor_actividad->result() as $itempv) { 
					?>
					<td>
						<div class="cuadro">
							<!--<div class="cuadritos <?php if($itempv->valor>=1){ echo 'activo';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=2){ echo 'activo';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=3){ echo 'activo';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=4){ echo 'activo';}?>"></div>-->

							<div class="cuadritos <?php if($itempv->valor>=1 && $itempv->estatus==1 || $itempv->valor>1 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=1 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=2 && $itempv->estatus==1 || $itempv->valor>2 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=2 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=3 && $itempv->estatus==1 || $itempv->valor>=4 && $itempv->estatus==2){ echo 'activo';} else if($itempv->valor>=3 && $itempv->estatus==2){ echo 'activo2';}?>"></div>
							<div class="cuadritos <?php if($itempv->valor>=4 && $itempv->estatus==1){ echo 'activo';} else if($itempv->valor>=4 && $itempv->estatus==2){ echo 'activo2';}?>"></div>

						</div>
					</td>
				<?php } 
				if($cont_matriz>$valor_actividad->num_rows()){ 
					echo $html_falta;
				} 
				?>
				
			</tr>
		<?php
			$row++; 
			} ?>
	</tbody>
	
	
</table>
</div>