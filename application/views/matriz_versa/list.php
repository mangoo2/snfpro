<style type="text/css">
  #table_data .img-thumbnail{
    max-width: 50px;
  }
  @media only screen and (max-width: 600px) {
    #table_data th,#table_data td{
          font-size: 11px;
    }

  }
</style>
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de matriz</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
              <a class="btn btn-primary" href="<?php echo base_url().'Matriz_versatibilidad/add/'.$id_proy; ?>">Nuevo</a>
              <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">

            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <a class="btn btn-primary" href="<?php echo base_url().'Matriz_versatibilidad/generar/'.$id_proy; ?>" target="_blank">Generar</a>
              <a class="btn btn-primary" href="<?php echo base_url().'Matriz_versatibilidad/documento/'.$id_proy; ?>" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 20px;" ></i> Documento</a>
              <!--<a class="btn btn-primary" href="<?php echo base_url().'Matriz_versatibilidad/exportarExcel/'.$id_proy; ?>" target="_blank"><i class="fa fa-file-excel-o" style="font-size: 20px;" ></i> Documento en Excel</a>-->
            </div>
            <div class="col-md-1"><br></div>
            <div class="col-12">
              <div class="col-md-1"><br></div>
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Cumplimiento</th>
                      <th>Objetivo</th>
                      <th>Faltante</th>
                      <th>Fecha creación</th>
                      <th>Fecha actualización</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
