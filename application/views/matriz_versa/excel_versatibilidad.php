<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=excel_versatibilidad.xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style type="text/css">
  .th_actividad{
    writing-mode: vertical-rl;
    transform: rotate(180deg);
    height: 156px;
  }
  .table_actividades{
    width:auto;
    font-size: 11px;
  }
  .col_des_1,.col_des_2{
    padding-left: 30px;
    padding-right: 30px;
    font-size: 12px;
  }
  .actividadg{
    background-color: #e9dd79 !important;
  }
  .actividadh{
    background-color: #5caae9 !important;
  }
  #canvasmt{
    display: none;
  }
  .td_foto{
    padding: 0px !important;
  }
</style>

<table cellpadding="5" border="1" class="table table-bordered table_actividades" id="table_actividades1">
  <thead>
    <tr border="1">
      <th colspan="3">
        <img src="<?php echo base_url().'public/img/tipo_act2.png';?>">
      </th>
      <?php 
        $actividadg=0;
        $actividadh=0;
        $cont_matriz=0;
        $html_falta="";
        foreach ($list_act->result() as $itemac) { 
          $cont_matriz++;
          ${'actividad_'.$itemac->id}=array();
          if($itemac->tipo==0){
            $titulo_tipo='actividadg';
            $actividadg++;
          }else{
            $titulo_tipo='actividadh';
            $actividadh++;
          }
        ?>
        <th class="th_actividad <?php echo $titulo_tipo;?>"><?php echo $itemac->nombre;?></th>
      <?php } ?>
      <th class="th_actividad">CUMPLIMIENTO</th>
      <th class="th_actividad">OBJETIVO</th>
      <th class="th_actividad">FALTA</th>
    </tr>
    <tr>
      <th>#</th>
      <th>FOTO</th>
      <th>NOMBRE</th>
      <th colspan="<?php echo $list_act->num_rows()?>">HABILIDAD</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <?php 

    /*function redimensionar($src, $ancho_forzado){
      if (file_exists($src)) {
        list($width, $height, $type, $attr)= getimagesize($src);
        if ($ancho_forzado > $width) {
           $max_width = $width;
        } else {
           $max_width = $ancho_forzado;
        }
        $proporcion = $width / $max_width;
        if ($proporcion == 0) {
           return -1;
        }
        $height_dyn = $height / $proporcion;
      } else {
        return -1;
      }
      return array($max_width, $height_dyn);
    }*/
    
      $row=1;
      foreach ($list_per->result() as $itemp) { 
        $html_falta="";
        if($itemp->foto!=''){ 
          $personal_foto=base_url().'uploads/empleados/'.$itemp->foto;
        }else{ 
          $personal_foto=base_url().'public/img/avatar2.png';
        }
        //echo "tipo_incidencia: ".$itemp->tipo_incidencia;
        if($itemp->tipo_incidencia!=5){
    ?>
      <tr>
        <td><?php echo $row;?></td>
        <td class="td_foto"><img id="img_avatar" src="<?php echo $personal_foto; ?>" class="rounded mr-3" width="30px"></td>
        <td style="padding-top: 20px"><?php echo $itemp->nombre; echo "\n"; ?></td>
        <?php 
          $valor_actividad=$this->Modelo_matriz_versatibilidad->list_actividades_empleado($itemp->idproyecto,$itemp->personalId);
          foreach ($valor_actividad->result() as $itempv) {
            ${'actividad_'.$itempv->id}[]=array($itempv->valor,$itempv->estatus);
          ?>
          <td>
            <table cellpadding="5">
              <tr>
                <?php 
                  if($itempv->valor==1 && $itempv->estatus==1){
                    echo '<img src="'.base_url().'public/img/cuadro1.png">';
                  }else if($itempv->valor==2 && $itempv->estatus==1){
                    echo '<img src="'.base_url().'public/img/cuadro2.png">';
                  }else if($itempv->valor==3 && $itempv->estatus==1){
                    echo '<img src="'.base_url().'public/img/cuadro3.png">';
                  }else if($itempv->valor==4 && $itempv->estatus==1){
                    echo '<img src="'.base_url().'public/img/cuadro4.png">';
                  }
                  else if($itempv->valor==1 && $itempv->estatus==2){ //empiezan los amarillos
                    echo '<img src="'.base_url().'public/img/cuadro1a.png">';
                  }else if($itempv->valor==2 && $itempv->estatus==2){ 
                    echo '<img src="'.base_url().'public/img/cuadro2a.png">';
                  }else if($itempv->valor==3 && $itempv->estatus==2){ 
                    echo '<img src="'.base_url().'public/img/cuadro3a.png">';
                  }else if($itempv->valor==4 && $itempv->estatus==2){ 
                    echo '<img src="'.base_url().'public/img/cuadro4a.png">';
                  }else if($itempv->valor==0){
                    echo '<img src="'.base_url().'public/img/cuadro0.png">';
                  }
                  echo "<br><br>";  ?>
              </tr>
            </table>
          </td>
        <?php }

        if($cont_matriz>$valor_actividad->num_rows()){ 
          for($j=$valor_actividad->num_rows(); $j<$cont_matriz; $j++){
            $html_falta.='<td>
              <div class="cuadro">
                <img src="'.base_url().'public/img/cuadro0.png">
              </div>
            </td>';
          } 
          echo $html_falta;
        } ?>
        <td><?php echo $itemp->cumplimiento; ?></td>
        <td><?php echo $itemp->objetivo; ?></td>
        <td><?php echo $itemp->faltante; ?></td>
      </tr>
    <?php
        $row++; 
        } 
      }
    ?>
  </tbody>
  <tfoot>
    <?php foreach ($list_act->result() as $itemac) { 
        $totaloperacion=0;
        $totalobjetivo=0;
        $totalfalta=0;
        foreach (${'actividad_'.$itemac->id} as $itemvg) {
          if($itemvg[0]>=3){
            if($itemvg[0]==3 && $itemvg[1]==1){ //1=completado, 2 = capacitacion
              $totaloperacion++;
            }
            if($itemvg[0]==4){
              $totaloperacion++;
            }
          }
          if($itemvg[0]>0){
            $totalobjetivo++;
          }
          // code...
        }
        ${'totaloperacion_'.$itemac->id}=$totaloperacion;
        ${'totalobjetivo_'.$itemac->id}=$totalobjetivo;
        ${'totalfalta_'.$itemac->id}=$totalobjetivo-$totaloperacion;
      } ?>
    <tr>
      <td colspan="3">Empleados calificados por operación</td>
      <?php foreach ($list_act->result() as $itemac) { 
        echo '<td>'.${'totaloperacion_'.$itemac->id}.'</td>';
        } ?>
    </tr>
    <tr>
      <td colspan="3">Objetivo</td>
      <?php foreach ($list_act->result() as $itemac) { 
        echo '<td>'.${'totalobjetivo_'.$itemac->id}.'</td>';
        } ?>
    </tr>
    <tr>
      <td colspan="3">Falta</td>
      <?php foreach ($list_act->result() as $itemac) { 
        echo '<td>'.${'totalfalta_'.$itemac->id}.'</td>';
        } ?>
    </tr>
  </tfoot>  
</table>

<table class="table">
  <tr class="col_des_1">
    <th width="50%" colspan="3">
      <p><b><u>Matriz de Versatilidad</u></b></p>
      <p>La matriz de versatilidad es la herramienta usada para documentar a los miembros de equipo que son entrenados o certificados para cada operación o maquina en el área de trabajo o actividad especifica. La matriz establece la flexibilidad de la fuerza de trabajo, la rotación del equipo, y la planeación para <span style="color:red;">el entrenamiento en los procesos</span>.</p>
      <p>El líder del equipo revisa y actualiza la matriz de versatilidad con ayuda del supervisor.</p>
      <p><b>Como completar la matriz</b></p>
      <ol>
        <li>Enlistar todos los miembros del equipo en la columna NOMBRE.</li>
        <li>Enlistar todas las operaciones o nombres de máquinas en el renglón OPERACION/ACTIVIDAD. Agrega cualquier otra tarea que sea responsable el departamento/equipo.</li>
        <li>Identifica todas las características críticas y significativas, u operaciones de la actividad.</li>
        <li>Documentar los estatus de certificaciones de  los miembros del equipo de acuerdo al criterio de certificación. Después que el miembro del equipo se certificó al nivel 3 o 4, añadir la fecha de la certificación bajo las casillas sombreadas en la plantilla.</li>
        <li>El total abajo de las columnas determina cuantos miembros del equipo están certificados para cada operación o máquina.</li>
        <li>El total de los renglones determina cuantas operaciones o maquinas está certificado para desempeñar el miembro del equipo. Publicar la matriz de versatilidad en el tablero.</li>
        
      </ol> 
      <p>La matriz de versatilidad debe estar actualizada. Sera revisada cada vez que haya un cambio en cualquier operación, maquina o en una hoja de proceso de calidad; que exista un nuevo miembro en el equipo; o que existan miembros del equipo que necesiten ser certificados o ser recertificados. Mínimo la matriz debe ser revisada una vez al mes.</p>
    </th>

    <th width="50%" colspan="3">
      <p><b><u>Criterios de Certificación</u></b></p>
      <p><img src="<?php echo base_url() ?>public/img/cuadro1.png"> <b>Nivel 1: Aprende a llevar a cabo tareas básicas y a desempeñar la operación.</b></p>
      <div class="row">
        <div class="col-md-9">
          <ul>
            <li>Lee copia de los instructivos de trabajo y AST y puede repetir los puntos claves</li>
            <li>Aprende la operación y las medidas de seguridad requeridas.</li>
            <li>Aprende las responsabilidades básicas del rol.</li>
            <li>Requiere asistencia al 100%</li>
          </ul>
        </div>
      </div>
      <p><img src="<?php echo base_url() ?>public/img/cuadro2.png"> <b>Nivel 2: Habilitado para desempeñar la operación</b></p>
      <div class="row">
        <div class="col-md-9">
          <ul>
            <li>Comienza a desarrollar un mayor conocimiento de la operación, mejor entendimiento y la importancia 
                      de seguir los estándares en procesos de la seguridad, calidad y el tiempo de las actividades</li>
            <li>Desempeña la operación por un turno completo.
              <ul>
                <li>Sin leer copia de IT y AST</li>
                <li>Sin ayuda</li>
                <li>Dentro del tiempo de ciclo</li>
                <li>Bajo los estándares</li>
              </ul>
            </li>
            <li>Maneja las anomalías con la asistencia</li>
          </ul>
        </div>
      </div>
      <p>*Debido a la variación que pueden presentar algunos elementos de la operación, velocidad de la actividad, y la complejidad de la producción, el líder y el supervisor pueden ajustar el numero de repeticiones requeridas para la certificación.</p>


      <p><img src="<?php echo base_url() ?>public/img/cuadro3.png"> <b>Nivel 3: Certificado para desempeñar la operación</b></p>
      <div class="row">
        <div class="col-md-9">
          <ul>
            <li>Continua desarrollando mas conocimiento de la operación, frecuentemente las áreas de oportunidad 
                      son identificadas y comentadas</li>
            <li>Desempeña la operación bajo estándar y tiempo</li>
            <li>Maneja anomalías independientemente</li>
          </ul>
        </div>
      </div>
      <p>El líder y el supervisor certifican que el miembro del equipo es capaz de llevar a cabo la operación. </p>

      <p><img src="<?php echo base_url() ?>public/img/cuadro4.png"> <b>Nivel 4: Certificado para enseñar la operación</b></p>
      <div class="row">
        <div class="col-md-9">
          <ul>
            <li>Desempeña la operación bajo estándar y tiempo</li>
            <li>Maneja anomalías independientemente</li>
            <li>Tiene un conocimiento profundo acerca de la operación</li>
            <li>Puede enseñar todo el conocimiento de la operación, conoce el procedimiento de Entrenamiento.</li>
          </ul>
        </div>
      </div>
      <p>Los líderes de los equipos deben estar certificados para enseñar todas las operaciones que se lleven a cabo en su área.</p>
    </th>
  </tr>
</table>