<style type="text/css">
  .error{
    margin: 0px;
  }
  label.error{
    color: red;
  }
  input.error,select.error{
    color: red;
    border: 1px solid red;
  }
  .numempleados{
    color: #0232cf;
    font-weight: bold;
  }
  #form_data{
    margin-top: 15px;
  }
  .actividadg{
    background: #e9dd7999;
  }
  .actividadh{
    background: #5caae994;
  }

  input:checked + .sliderNCom {background-color: #09E743;}
  input:focus + .sliderNCom {box-shadow: 0 0 1px #09E743;}

  input:checked + .sliderNCap {background-color: #FFFF00;}
  input:focus + .sliderNCap {box-shadow: 0 0 1px #FFFF00;}

</style>
<input type="hidden"id="idproyecto0" value="<?php echo $id_proy;?>">
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3> Proyecto:<?php echo $empresa; ?></h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>Matriz_versatibilidad/listado/<?php echo $id_proy; ?>">Lista de actividades</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        
        <!---->
        
          <div class="card-body">
            <div class="row">
              <h3>Capacitación</h3>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label>Total de empleados participantes: <span class="numempleados"><?php echo $empleados->num_rows(); ?><span></label>
                  <select class="form-control" id="search_empleado" >
                    <option value="0"></option>
                    <?php foreach ($empleados->result() as $item) { ?>
                      <option value="<?php echo $item->personalId; ?>"><?php echo $item->nombre; ?></option>
                    <?php } ?>
                  </select>
              </div>
            </div>
            <?php if($idempleado>0){ 
                $personal_result=$this->ModeloGeneral->getselectwhere2('personal',array('personalId'=>$idempleado));
                foreach ($personal_result->result() as $item) {
                  $personal_nombre=$item->nombre;

                  if($item->foto!=''){ 
                    $personal_foto=base_url().'uploads/empleados/'.$item->foto;
                  }else{ 
                    $personal_foto=base_url().'public/img/avatar.png';
                  }

                  // code...
                }
            ?>
              <form class="form" method="post" role="form" id="form_data">
                <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo $id_proy;?>">
                <input type="hidden" name="idempleado" id="idempleado" value="<?php echo $idempleado; ?>">
                <input type="hidden" name="idmatriz" id="idmatriz" value="0">
                <div class="row">
                  <div class="col-md-12">
                    <b><?php echo $personal_nombre;?></b>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <img id="img_avatar" src="<?php echo $personal_foto; ?>" class="rounded mr-3" width="142">
                  </div>
                  <div class="col-md-4">
                    <a class="btn btn-primary" style="margin-top: 10px;">Datos Generales</a>
                    <a class="btn btn-primary" style="margin-top: 10px;">Listado de actividades</a>
                  </div>
                  <div class="col-md-4">
                    <label>Cumplimiento:</label>
                    <input type="number" name="cumplimiento" id="cumplimiento" class="form-control" readonly>
                    <label>Objetivo:</label>
                    <input type="number" name="objetivo" id="objetivo" class="form-control" onchange="calcularfaltante()">
                    <label>Falta:</label>
                    <input type="number" name="faltante" id="faltante" class="form-control" readonly>
                  </div>
                  <div class="mb-3 row">
                    <div class="col-md-3">
                      <label class="col-form-label m-r-10">Capacitado</label>
                      <div class="col-md-2">
                        <label class="switch">
                          <input checked class="tipo" id="capacitado" name="tipo" type="radio">
                          <span class="sliderN sliderNCom round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <label class="col-form-label m-r-10">En capacitación</label>
                      <div class="col-md-2">
                        <label class="switch">
                          <input class="tipo" id="encapacitacion" name="tipo" type="radio">
                          <span class="sliderN sliderNCap round"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <br>
                  </div>
                </div>
                <div class="row actividad_padre">
                  <div class="col-md-12 actividad_hijos">
                    <?php 
                        $actividades=$this->ModeloGeneral->getselectwhere2('limpieza_actividades',array('id_proyecto'=>$id_proy,'estatus'=>1));
                        foreach ($actividades->result() as $item) { 
                            if($item->tipo==0){
                              $titulo_tipo='actividadg';
                            }else{
                              $titulo_tipo='actividadh';
                            }

                          ?>
                          <div class="actividad actividad_gral_<?php echo $item->id;?>">
                            <div class="titulo <?php echo $titulo_tipo;?>"><?php echo $item->nombre;?><input type="hidden" id="titulo" value="<?php echo $item->nombre;?>"></div>
                            <div class="input select rating-c">
                                <select id="rating" class="select_rating rating_actividad_<?php echo $item->id;?>" data-actividad="<?php echo $item->id;?>" data-id="0" data-capacitado="<?php echo $item->estatus;?>">
                                    <option></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>

                            </div>
                          </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <br>
                    <a class="btn btn-primary" onclick="saveform()" id="buttonsave">Guardar registros</a>
                  </div>
                </div>


              </form>
            <?php } ?>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <a href="<?php echo base_url()?>Matriz_versatibilidad/listado/<?php echo $id_proy; ?>" class="btn btn-light">Regresar</a>
            </div>
          </div>  
          
        <!---->
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .actividad{
    width: 183px;
    text-align: center;
    float: left;
    border: 1px solid #59a6d64a;
  }
  .actividad .titulo{
    min-height: 63px;
    padding-left: 1px;
    padding-right: 1px;
    font-size: 11px;
  }
  .actividad .rating-c{
    width: 83px;
    margin-left: auto;
    margin-right: auto;
  }
  .rating-c .bar-rating {
    height: 80px;
}

.rating-c .bar-rating a {
    display: block;
    width: 35px;
    height: 35px;
    float: left;
    background-color: #e3e3e3;
    margin: 2px;
    text-decoration: none;
    font-size: 16px;
    font-weight: 400;
    line-height: 2.2;
    text-align: center;
    color: #b6b6b6;
}

.rating-c .bar-rating a.active,
.rating-c .bar-rating a.selected {
    background-color: #59a6d6;
    color: white;
}

.current.capacitacion.active, .current.capacitacion.selected{
    background-color: yellow !important;
    color: white;
}
</style>