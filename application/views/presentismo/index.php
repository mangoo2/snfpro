<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Incidencias</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
            <input type="hidden" id="id_cli" value="<?php echo $id_cli;?>">

            <div class="col-12">
              <div class="mb-3 row">
              <br>
              <div class="col-sm-4">
                <label>Tipo de incidencia</label>
                <select class="form-control" id="tipo">
                  <option value="0">Todas</option>
                  <option value="1">Falta</option>
                  <option value="2">Permiso</option>
                  <option value="3">Retardo</option>
                  <option value="4">Incapacidad</option>
                  <option value="5">Baja</option>
                  <option value="6">Vacaciones</option>
                </select>
              </div>
            </div>
              <a href="<?php echo base_url()?>Proyectos/ejecucion/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <a class="btn btn-primary" href="<?php echo base_url() ?>Presentismo/registro/0/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Nueva incidencia</a>
              <button class="btn btn-light" onclick="excel_exportar()"><i class="fa fa-file-excel-o" style="font-size: 20px;" ></i> Excel</button>
            </div>

            <div class="col-12">
              <br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_incidencias">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Empleado</th>
                      <th scope="col">Tipo</th>
                      <th scope="col">Fecha</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>