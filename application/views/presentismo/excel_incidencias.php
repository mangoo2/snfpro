<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=incidencias.xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style>
  .table-striped tbody tr:nth-of-type(odd) {
    background-color: rgb(255 18 18 / 5%);
    }
    .style_head{
        color:black; font-size: 10px; text-align: center; background-color: #BFBFBF;
    }
</style>

  <h3>Incidencias</h3> 
    <table width="100%" align="center" border="1" RULES="rows" style="padding: 5px;" class="table table-striped"> 
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Empleado</th>
          <th scope="col">Tipo</th>
          <th scope="col">Fecha</th>
        </tr>
      </thead>
      <?php 
      foreach ($get_result as $i){
        $tipo='';
        if($i->tipo_incidencia=="1")
            $tipo="Falta";
        else if($i->tipo_incidencia=="2")
            $tipo="Permiso";
        else if($i->tipo_incidencia=="3")
            $tipo="Retardo";
        else if($i->tipo_incidencia=="4")
            $tipo="Incapacidad";
        else if($i->tipo_incidencia=="5")
            $tipo="Baja";
        else if($i->tipo_incidencia=="6")
            $tipo="Vacaciones";

        echo '<tr> 
          <td style="text-align: left;">'.$i->id.'</td> 
          <td style="text-align: left;">'.$i->nombre.'</td>
          <td style="text-align: left;">'.$tipo.'</td>
          <td style="text-align: left;">'.$i->fecha.'</td>  
        </tr>';

      }  ?>
    </table>