<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> incidencia</h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>Presentismo/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Lista de incidencias</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id_proyecto" id="id_proy" value="<?php echo $id_proy;?>">
          <input type="hidden" id="id_cli" value="<?php echo $id_cli;?>">
          <input type="hidden" name="id" id="id" value="<?php if(isset($a)) echo $a->id; else echo '0' ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3">
                  <div class="col-md-3">
                    <center><label class="col-form-label m-r-10">Tipo de incidencia</label></center>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">FALTA</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input class="tipo_incidencia" id="tipo_1" name="tipo" type="radio" <?php if(isset($a->tipo_incidencia) && $a->tipo_incidencia=="1") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">PERMISO</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input class="tipo_incidencia" id="tipo_2" name="tipo" type="radio" <?php if(isset($a->tipo_incidencia) && $a->tipo_incidencia=="2") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">RETARDO</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input class="tipo_incidencia" id="tipo_3" name="tipo" type="radio" <?php if(isset($a->tipo_incidencia) && $a->tipo_incidencia=="3") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">INCAPACIDAD</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input class="tipo_incidencia" id="tipo_4" name="tipo" type="radio" <?php if(isset($a->tipo_incidencia) && $a->tipo_incidencia=="4") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">BAJA</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input class="tipo_incidencia" id="tipo_5" name="tipo" type="radio" <?php if(isset($a->tipo_incidencia) && $a->tipo_incidencia=="5") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">VACACIONES</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input class="tipo_incidencia" id="tipo_6" name="tipo" type="radio" <?php if(isset($a->tipo_incidencia) && $a->tipo_incidencia=="6") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha Inicio</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" name="fecha" value="<?php if(isset($a)) echo $a->fecha; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Fecha Fin</label>
                  <div class="col-sm-4">
                    <input <?php if(isset($a)) echo "disabled"; ?> class="form-control" type="date" name="fecha_fin" value="<?php if(isset($a) && $a->fecha_fin!="0000-00-00") echo $a->fecha_fin; else if(isset($a) && $a->fecha_fin=="0000-00-00") echo $a->fecha; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Empleado</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="id_personal" id="id_personal">
                      <?php foreach ($emp as $e){ ?>
                        <option value="<?php echo $e->id_empleado; ?>" <?php if(isset($a) && $e->id_empleado==$a->id_personal) echo 'selected' ?>><?php echo $e->nombre ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="mb-3 row" id="cont_causas" style="display: none;">
                  <label class="col-sm-2 col-form-label">Causas</label>
                  <div class="col-sm-3">
                    <select class="form-control" name="causa" id="causa">
                      <option value=""></option>
                      <option <?php if(isset($a) && $a->causa=="1") echo "selected"; ?> value="1">FALTA DE DICIPLINA</option>
                      <option <?php if(isset($a) && $a->causa=="2") echo "selected"; ?> value="2">OTRO EMPLEO</option>
                      <option <?php if(isset($a) && $a->causa=="3") echo "selected"; ?> value="3">PROBLEMAS DE SALUD</option>
                      <option <?php if(isset($a) && $a->causa=="4") echo "selected"; ?> value="4">PROBLEMAS FAMILIARES</option>
                      <option <?php if(isset($a) && $a->causa=="5") echo "selected"; ?> value="5">OTRO</option>
                    </select>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <a href="<?php echo base_url()?>Presentismo/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <button id="btn_sub" class="btn btn-primary" type="button">Guardar incidencia</button>
            </div>
          </div>  
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>