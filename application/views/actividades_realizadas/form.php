<style type="text/css">
  .error{
    margin: 0px;
  }
  label.error{
    color: red;
  }
  input.error,select.error{
    color: red;
    border: 1px solid red;
  }
  .kv-file-upload,.kv-file-remove,.kv-file-zoom,.fileinput-remove-button{
    display: none;
  }
</style>
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3> Proyecto:<?php echo $empresa; ?></h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>Actividades_realizadas/listado/<?php echo $id_proy; ?>">Lista de actividades</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo $id_proy;?>">
          <input type="hidden" name="id" id="id" value="<?php echo $id_acti; ?>">
          <div class="card-body">
            <div class="row">
              <h3>Actividades realizadas</h3>
            </div>
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Descripción de la actividad</label>
                  <div class="col-sm-9">
                    <input placeholder="Descripción" class="form-control" type="text" name="descripcion" id="descripcion" required>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Descripción general</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" rows="3" id="descripcion_general" name="descripcion_general"></textarea>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha de creación</label>
                  <div class="col-sm-9">
                    <input placeholder="Fecha / hora" class="form-control" type="datetime-local" name="fecha" id="fecha" <?php if($fecha_ini!=''){ echo 'min="'.$fecha_ini.'"';}?> <?php if($fecha_fin!=''){ echo 'max="'.$fecha_fin.'"';}?> required>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Periodo Inicial</label>
                  <div class="col-sm-3">
                    <input placeholder="Fecha" class="form-control" type="date" name="fechai" id="fechai" <?php if($fecha_ini!=''){ echo 'min="'.$fecha_ini.'"';}?> <?php if($fecha_fin!=''){ echo 'max="'.$fecha_fin.'"';}?> required>
                  </div>
                  <label class="col-sm-3 col-form-label">Periodo Final</label>
                  <div class="col-sm-3">
                    <input placeholder="Fecha" class="form-control" type="date" name="fechaf" id="fechaf" <?php if($fecha_ini!=''){ echo 'min="'.$fecha_ini.'"';}?> <?php if($fecha_fin!=''){ echo 'max="'.$fecha_fin.'"';}?> required>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Horas hombre</label>
                  <div class="col-sm-9">
                    <input placeholder="Horas hombre" class="form-control" type="text" name="horas" id="horas" required>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Departamentos</label>
                  <div class="col-sm-8">
                    <select class="form-control" id="departamento" name="departamento" required>
                      <?php foreach ($det_dep->result() as $item) { ?>
                        <option value="<?php echo $item->id;?>"><?php echo $item->departamento;?></option>
                      <?php }?>
                    </select>
                  </div>
                  <div class="col-sm-1">
                    <button id="add_dep" class="btn btn-primary" type="button" onclick="adddepartamento()"><i class="fa fa-plus" aria-hidden="true"></i></button>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-4 div_file1">
                    <div class="row">
                      <div class="col-sm-12">
                        <label>Titulo</label>
                        <input class="form-control" type="text" name="titulo1" id="titulo1">
                      </div>
                    </div>
                    <div class="row">
                      <input class="form-control" type="file" name="file1" id="file1">
                    </div>
                    <div class="row">
                      
                      <div class="col-sm-12">
                        <label>Descripción</label>
                        <input class="form-control" type="text" name="descripcion1" id="descripcion1">
                      </div>
                      <!--<div class="col-sm-12">
                        <label>Periodo inicial</label>
                        <input class="form-control" type="date" name="periodo_inicio1" id="periodo_inicio1">
                      </div>
                      <div class="col-sm-12">
                        <label>Periodo fin</label>
                        <input class="form-control" type="date" name="periodo_fin1" id="periodo_fin1">
                      </div>-->
                    </div>
                  </div>
                  <div class="col-sm-4 div_file2">
                    <div class="row">
                      <div class="col-sm-12">
                        <label>Titulo</label>
                        <input class="form-control" type="text" name="titulo2" id="titulo2">
                      </div>
                    </div>
                    <div class="row">
                      <input class="form-control" type="file" name="file2" id="file2">
                    </div>
                    
                    <div class="row">
                      
                      <div class="col-sm-12">
                        <label>Descripción</label>
                        <input class="form-control" type="text" name="descripcion2" id="descripcion2">
                      </div>
                      <!--<div class="col-sm-12">
                        <label>Periodo inicial</label>
                        <input class="form-control" type="date" name="periodo_inicio2" id="periodo_inicio2">
                      </div>
                      <div class="col-sm-12">
                        <label>Periodo fin</label>
                        <input class="form-control" type="date" name="periodo_fin2" id="periodo_fin2">
                      </div>-->
                    </div>
                  </div>
                  <div class="col-sm-4 div_file3">
                    <div class="row">
                      <div class="col-sm-12">
                        <label>Titulo</label>
                        <input class="form-control" type="text" name="titulo3" id="titulo3">
                      </div>
                    </div>
                    <div class="row">
                      <input class="form-control" type="file" name="file3" id="file3">
                    </div>
                    
                    <div class="row">
                      
                      <div class="col-sm-12">
                        <label>Descripción</label>
                        <input class="form-control" type="text" name="descripcion3" id="descripcion3">
                      </div>
                      <!--<div class="col-sm-12">
                        <label>Periodo inicial</label>
                        <input class="form-control" type="date" name="periodo_inicio3" id="periodo_inicio3">
                      </div>
                      <div class="col-sm-12">
                        <label>Periodo fin</label>
                        <input class="form-control" type="date" name="periodo_fin3" id="periodo_fin3">
                      </div>-->
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-3">
                    <button style="margin-top: 35px;" id="btn_sub" class="btn btn-primary" type="submit">Guardar actividad</button>
                  </div>
                </div>
                

              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <a href="<?php echo base_url()?>Actividades_realizadas/listado/<?php echo $id_proy; ?>" class="btn btn-light">Regresar</a>
            </div>
          </div>  
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modaldepartamento" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Departamentos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <label class="col-md-2">Departamento</label>
          <div class="col-md-10">
            <input type="hidden" name="iddep" id="iddep" value="0">
            <input type="text" name="departamentonew" id="departamentonew" class="form-control">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-bordernone" id="tabledep">
              <thead>
                <tr>
                <th>Nombre</th>
                <th>Accion</th>
              </tr>  
              </thead>
              <tbody>
                
              </tbody>
              
              
              
            </table>
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button"  onclick="savedep()" >Guardar</button>
        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="Cerrar" title="">Cerrar</button>
      </div>
    </div>
  </div>
</div>