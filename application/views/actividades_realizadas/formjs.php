<script src="<?php echo base_url();?>plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>plugins/fileinput/fileinput.min.css">

<script src="<?php echo base_url();?>plugins/toastr/toastr.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>plugins/toastr/toastr.css">


<script type="text/javascript" src="<?php echo base_url();?>public/js/actividades_r_form.js?v=<?php echo date('YmdGis')?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		<?php if($id_acti>0){ ?>
			obtenerdatosform(<?php echo $id_acti;?>);
		<?php } ?>
	});
</script>
