<style type="text/css">
  #table_data .img-thumbnail{
    max-width: 50px;
  }
  @media only screen and (max-width: 600px) {
    #table_data th,#table_data td{
          font-size: 11px;
    }

  }
</style>
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de actividades</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <a class="btn btn-primary" href="<?php echo base_url().'Actividades_realizadas/add/'.$id_proy; ?>">Nueva actividad</a>
              <a class="btn btn-primary" onclick="adddescripcion()">Descripción Mensual</a>
              <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
            </div>
            <div class="col-12">
              <div class="table-responsive">
                
                
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Descripción</th>
                      <th>Fecha de Creación</th>
                      <th>Horas hombre</th>
                      <th>Departamento</th>
                      <th>Evidencias</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modalimages" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Evidencia</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
      </div>
      <div class="modal-body imagemodalbody">
        
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modaldescripcion" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Descripciones</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3">
            <input type="hidden" class="form-control" id="dg_id" value="0">
            <label>MES</label>
            <select class="form-control" id="dg_mes">
              <option value="1">Enero</option>
              <option value="2">Febrebro</option>
              <option value="3">Marzo</option>
              <option value="4">Abril</option>
              <option value="5">Mayo</option>
              <option value="6">Junio</option>
              <option value="7">Julio</option>
              <option value="8">Agosto</option>
              <option value="9">Septiembre</option>
              <option value="10">Octubre</option>
              <option value="11">Noviembre</option>
              <option value="12">Diciembre</option>
              <?php 
                /*foreach ($meses as $item) { ?>
                  <!--<option value="<?php echo $item['mes']?>"><?php echo $item['mesl']?></option>-->
              <?php /* }*/ ?>
            </select>
          </div>
          <div class="col-md-3">
            <label>AÑO</label>
            <select class="form-control" id="dg_anio">
              <?php $anio_ant=date("Y")-2; $anio_fin=date("Y")+5;
                for($a=$anio_ant; $a<$anio_fin; $a++) { ?>
                  <option value="<?php echo $a; ?>"><?php echo $a; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-9">
            <br>
            <label>Titulo</label>
            <input type="text" class="form-control" id="dg_titulo">
          </div>
          
          <div class="col-md-5">
            <!--<label>Departamento</label>
            <input type="text" class="form-control" id="dg_dep">-->
            <label class="col-sm-2 col-form-label">Departamento</label>
            <div class="col-sm-8">
              <select class="form-control" id="dg_dep" required>
                <?php foreach ($det_dep->result() as $item) { ?>
                  <option value="<?php echo $item->id;?>"><?php echo $item->departamento;?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="col-md-7">
            <label>Descripción</label>
            <input type="text" class="form-control" id="dg_des">
          </div>

        </div>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-bordernone" id="tabledes">
              <thead>
                <tr>
                  <th>Mes</th>
                  <th>Año</th>
                  <th>Titulo</th>
                  <th>Departamento</th>
                  <th>Descripción</th>
                  <th></th>

                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" onclick="savedescripcion()" >Guardar</button>
        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="" title="">Cerrar</button>
      </div>
    </div>
  </div>
</div>