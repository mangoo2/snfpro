<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Cliente: <?php echo $cliente_text->empresa ?></h3>
        <h3>Ejecución de Proyecto</h3>
      </div>
      <div class="col-sm-6" align="right">
        <a href="<?php echo base_url() ?>Proyectos" class="btn btn-light">Regresar</a>
      </div>  
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Actividades/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">
                <div class="row texto_centro " style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Programa de Limpieza </span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-calendar fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Ordenes/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Órdenes de Trabajo </span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-briefcase fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>MatrizAcciones/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Matríz de Acciones </span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-tv fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Actividades_realizadas/listado/<?php echo $id_proy; ?>">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Actividades Realizadas </span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-line-chart fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Matriz_versatibilidad/listado/<?php echo $id_proy; ?>">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Matríz de Versatilidad </span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-table fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Monitoreo/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Monitoreo Áreas Criticas </span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-search fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>
            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Presentismo/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Presentismo Rotación</span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-users fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>