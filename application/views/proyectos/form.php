<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> Proyecto</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id" id="id" value="<?php echo $id;?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Cliente</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="id_cliente" id="id_cliente" style="width: 100%">
                      <?php if(isset($p) && $p->id_cliente>0) echo "<option value='".$p->id_cliente."'>".$p->empresa."</option>"; ?>
                    </select>
                  </div>
                  <label class="col-sm-2 col-form-label">Servicio</label>
                  <div class="col-sm-4">
                    <select class="form-control" name="id_servicio" id="id_servicio">
                      <?php if(isset($p) && $p->id_cliente>0) echo "<option value='".$p->id_servicio."'>".$p->servicio."/".$p->descripcion."</option>"; ?>
                    </select>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Fecha inicio</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" name="fecha_ini" value="<?php if(!isset($p)) echo date("Y-m-d"); else echo $p->fecha_ini; ?>">
                  </div>
                  <label class="col-sm-2 col-form-label">Fecha fin</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="date" name="fecha_fin" value="<?php if(!isset($p)) echo date("Y-m-d"); else echo $p->fecha_fin; ?>">
                  </div>
                </div>
                <div class="mb-3 row">

                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">1er turno</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input id="turno1" type="checkbox" <?php if(isset($p) && $p->turno>0) echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">2do turno</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input id="turno2" type="checkbox" <?php if(isset($p) && $p->turno2>0) echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">3er turno</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input id="turno3" type="checkbox" <?php if(isset($p) && $p->turno3>0) echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <!--<div class="col-md-3">
                    <label class="col-sm-12 col-form-label">Días laborables por semana</label>
                    <div class="col-sm-12">
                      <input class="form-control" type="number" name="dias_laborables" value="<?php if(isset($p)) echo $p->dias_laborables; ?>">
                    </div>
                  </div>-->
                </div>
              </div>
            </div>

            <div>  
              <div class="row"><br></div>
              <h5>Empleados participantes </h5> 
              <div class="col-md-4">
                <button id="add_all_emp" class="btn btn-primary" type="button" ><i class="fa fa-plus" aria-hidden="true"></i> Agregar todos los asignados</button>
              </div> <br>   
              <div class="row"> 
                <div class="col-md-6 div_proyect">
                  <div class="mb-3 row">
                    <div class="col-sm-9">
                      <label class="">Empleado</label>
                      <select class="form-control" id="id_empleado"></select>
                    </div>
                    <div class="col-md-1">
                      <label style="color: transparent;">agregar</label>
                      <button id="add_emp" class="btn btn-primary" type="button" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                  </div>  
                  <table class="table" id="tabla_emp">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Empleado</th>
                        <th>Fecha asignación</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody class="body_emp">
                      <?php
                        echo $html;
                      ?>
                    </tbody>
                  </table>  
                </div>
               
                <div class="col-md-6 div_proyect">
                  <div class="mb-3 row">
                    <div class="col-sm-9">
                      <label class="">Supervisor</label>
                      <select class="form-control" id="id_super"></select>
                    </div>
                    <div class="col-md-1">
                      <label style="color: transparent;">agregar</label>
                      <button id="add_empsup" class="btn btn-primary" type="button" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                  </div>
                  <table class="table" id="tabla_emp_sup">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Empleado</th>
                        <th>Fecha asignación</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody class="body_emp_sup">
                      <?php 
                        echo $html2;
                      ?>
                    </tbody>
                  </table>  
                </div>
              </div>
              <div class="row"> 
                <div class="mb-3 row">

                </div>
                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Comentarios adicionales</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="text" name="comentarios" value="<?php if(isset($p)) echo $p->comentarios; ?>">
                  </div>
                </div>

                <div class="col-md-3">
                  <label class="col-form-label m-r-10">Presentismo</label>
                  <div class="col-md-2">
                    <label class="switch">
                      <input id="tipo" type="checkbox" <?php if($cont_pr>0) echo "checked"; ?>>
                      <span class="sliderN round"></span>
                    </label>
                  </div>
                </div>
                <div class="col-md-12" id="cont_rotacion" style="display: none;">
                  <div class="mb-3 row">
                    <label class="col-sm-1 col-form-label">Puesto</label>
                    <div class="col-sm-3">
                      <!--<input class="form-control puesto_pri" type="text" id="puesto" value="">-->
                      <select class="form-control puesto_pri" id="puesto">
                        <?php foreach ($puestos as $p){ ?>
                          <option value="<?php echo $p->id; ?>"><?php echo $p->puesto; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <label class="col-sm-2 col-form-label">Objetivo</label>
                    <div class="col-sm-1">
                      <input class="form-control objetivo_pri" type="text" id="objetivo" value="">
                    </div>
                    <label class="col-sm-2 col-form-label">Días que Labora</label>
                    <div class="col-sm-1">
                      <input class="form-control dias_laborables_pri" type="text" id="dias_laborables" value="">
                    </div>
                    <div class="col-md-1">
                      <button title="Agregar Puesto de rotación" id="add_puesto" class="btn btn-primary" type="button" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                  </div>
                  
                  <table class="table" id="tabla_puestos" width="100%">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Puesto</th>
                        <th>Objetivo</th>
                        <th>Días laborables</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody class="body_puestos">
                      <?php 
                        echo $html3;
                      ?>
                    </tbody>
                  </table>

                </div>

              </div>
            </div>


          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <button id="btn_sub" class="btn btn-primary" type="submit">Guardar datos</button>
              <a href="<?php echo base_url()?>Proyectos" class="btn btn-light">Regresar</a>
            </div>
          </div>  
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>