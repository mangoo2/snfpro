<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Proyectos</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <a class="btn btn-primary" href="<?php echo base_url() ?>Proyectos/registro">Nuevo proyecto</a>
            </div>
            <div class="col-12">
              <br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_proyectos">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Cliente</th>
                      <th scope="col">Tipo de servicio</th>
                      <th scope="col">Fecha de inicio</th>
                      <th scope="col">Fecha fin</th>
                      <th scope="col">Usuario de captura</th>
                      <th scope="col">Fecha y hora</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>