<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Cliente: <?php echo $cliente_text->empresa ?></h3>
        <h3><u><i>Programa de limpieza</i></u></h3>
      </div>
      <div class="col-sm-6" align="right">
        <a href="<?php echo base_url() ?>Proyectos/ejecucion/<?php echo $id_proy ?>/<?php echo $id_cli ?>" class="btn btn-light">Regresar</a>
      </div>  
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">   
            <div class="col-md-4 mb-3 row" align="center">

            </div>       
            <div class="col-md-4 mb-4 row" align="center">
              <a href="<?php echo base_url() ?>Actividades/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>"><button style="width: 290px;" class="btn btn-primary">
                <div class="row texto_centro">
                  <div class="col-md-9" align="left">
                    <span class="align-middle" style="font-size: 18px; font-weight: bold;">Actividades </span>
                    <span style="font-size: 18px; font-weight: bold; color: transparent;">Act</span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-calendar fa-3x"></i>
                  </div>
                </div>  
              </button></a>
            </div>

            <!--<div class="col-md-4 mb-4 row" align="center">
              <a href="<?php echo base_url() ?>Actividades/listadoCriticas/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>"><button style="width: 290px;" class="btn btn-primary">
                <div class="row texto_centro">
                  <div class="col-md-10" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Actividades Críticas </span>
                  </div>
                  <div class="col-md-2" align="right">
                    <i class="fa fa-warning fa-3x"></i>
                  </div>
                </div>  
              </button></a>
            </div>-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>