<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Nuevo usuario</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="clienteId" id="idclientePadre" value="<?php echo $idPadre ?>">
          <input type="hidden" name="id" id="idcliente" value="<?php echo $id ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Contacto</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="text" name="contacto" value="<?php echo $contacto ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Email</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="email" name="correo" id="correo" value="<?php echo $correo ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Contraseña</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="password" autocomplete="new-password" name="contrasena" id="contrasena" aria-autocomplete="list" value="<?php echo $contrasena ?>">
                  </div>
                </div>

                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Confirmar contraseña</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="password" d="contrasena2" name="contrasena2" value="<?php echo $contrasena ?>">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>  
          <div class="card-footer">
            <div class="col-sm-9">
              <button class="btn btn-primary" type="button" onclick="add_form()">Guardar datos</button>
              <a href="<?php echo base_url()?>Mis_clientes" class="btn btn-light">Regresar</a>
            </div>
          </div>
        <!---->
      </div>
    </div>
  </div>
</div>