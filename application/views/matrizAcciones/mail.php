
<style type="text/css">
    .div1{
        min-height: 450px;
        max-width: 30px;
        background-color: #083dd3;
    }
    .div2{
        min-height: 450px;
        max-width: 30px;
        background-color: #2c4c97;
        margin-left: 5px;
    }
    .div3{
        min-height: 450px;
        max-width: 30px;
        background-color: #1b3367;
        margin-left: 5px;
    }
    p {
        font-size: 20px;
        line-height: 1.7;
        letter-spacing: inherit;
        font-family: "Montserrat", sans-serif;
        color: #0232cf;
        font-weight: 700;
    }
    span{}
</style>

<table width="100%" border="0">
    <tr>
        <!--<td width="1%"><div class="div1"></div></td>
        <td width="1%"><div class="div2"></div></td>
        <td width="1%"><div class="div3"></div></td>-->
        <td width="3%">
            <img width="70px" src="<?php echo base_url();?>public/img/img_mail.png" alt="">
        </td>
        <td width="97%">
            <img width="150px" src="<?php echo base_url();?>public/img/logo_mail.png" alt="">
            <div class="media-body">
              <p>Hallazgo: </p><span><i><?php echo $hallazgo; ?></i></span>
              <p>Fecha compromiso: </p><span><i><?php echo $fecha_compromiso; ?></i></span>
            </div>
        </td>
    </tr>
</table>

<table width="100%" border="0">
    <tr>
        <td width="100%">
            <p><i><u>Favor de no responder a este mensaje, es un mail de envío automático.<u></i></p>
        </td>
    </tr>
</table>


<!--<div class="col-sm-3">
    <img width="150px" class="img-fluid" src="<?php echo base_url();?>public/img/logofinalsys.png" alt="">
    <div class="media-body">
      <p>Hallazgo: </p><span><i><?php echo $hallazgo; ?></i></span>
      <p>Fecha compromiso: </p><span><i><?php echo $fecha_compromiso; ?></i></span>
    </div>
</div>-->
          

