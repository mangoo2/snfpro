<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Matriz de acciones</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
            <input type="hidden" id="id_cli" value="<?php echo $id_cli;?>">
            <div class="col-12">
              <a href="<?php echo base_url()?>Proyectos/ejecucion/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <a class="btn btn-primary" href="<?php echo base_url() ?>MatrizAcciones/registro/0/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Nuevo hallazgo</a>
            </div>
            <div class="col-12">
              <br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_matriz">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Área</th>
                      <th scope="col">Hallazgo</th>
                      <th scope="col">Tipo</th>
                      <th scope="col">Evidencia</th>
                      <th scope="col">Fecha de detección</th>
                      <th scope="col">Falla</th>
                      <th scope="col">Acciones requeridas</th>
                      <th scope="col">Afectación</th>
                      <th scope="col">Criticidad</th>
                      <th scope="col">Responsable</th>
                      <th scope="col">Avance</th>
                      <th scope="col">Fecha compromiso</th>
                      <th scope="col">Fecha de cierre</th>
                      <th scope="col">Observaciones</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>