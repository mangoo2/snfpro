<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> Hallazgo</h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>MatrizAcciones/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Lista de hallazgos</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id_proyecto" id="id_proy" value="<?php echo $id_proy;?>">
          <input type="hidden" name="id_cliente" id="id_cli" value="<?php echo $id_cli;?>">
          <input type="hidden" name="id" id="id" value="<?php if(isset($a)) echo $a->id; else echo '0' ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3">
                  <div class="col-md-3">
                    <center><label class="col-form-label m-r-10">Tipo de hallazgo</label></center>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">SNF</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input class="tipo_hallazgo" id="tipo_hallazgo1" name="tipo_hallazgo" type="radio" <?php if(isset($a->tipo_hallazgo) && $a->tipo_hallazgo=="1") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">Cliente</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input class="tipo_hallazgo" id="tipo_hallazgo2" name="tipo_hallazgo" type="radio" <?php if(isset($a->tipo_hallazgo) && $a->tipo_hallazgo=="2") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Área</label>
                  <div class="col-sm-9">
                    <input placeholder="Área" class="form-control" type="text" name="area" value="<?php if(isset($a)) echo $a->area; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Hallazgo</label>
                  <div class="col-sm-9">
                    <input placeholder="Hallazgos" class="form-control" type="text" name="hallazgo" value="<?php if(isset($a)) echo $a->hallazgo; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-3 col-form-label">Fecha de detección</label>
                  <div class="col-sm-3">
                    <input class="form-control" type="date" name="fecha_deteccion" value="<?php if(isset($a)) echo $a->fecha_deteccion; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Falla</label>
                  <div class="col-sm-9">
                    <input placeholder="Falla" class="form-control" type="text" name="falla" value="<?php if(isset($a)) echo $a->falla; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Acciones requeridas</label>
                  <div class="col-sm-9">
                    <input placeholder="Acciones requeridas" class="form-control" type="text" name="acciones" value="<?php if(isset($a)) echo $a->acciones; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Afectación</label>
                  <div class="col-sm-9">
                    <input placeholder="Afectación" class="form-control" type="text" name="afectacion" value="<?php if(isset($a)) echo $a->afectacion; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-3">
                    <label>Criticidad</label>
                    <select class="form-control" name="criticidad" id="criticidad">
                      <option value="A">A</option>
                      <option value="B">B</option>
                      <option value="C">C</option>
                    </select>
                  </div>
                  <div class="col-sm-5" id="cont_resp">
                    <label>Responsable</label>
                    <select class="form-control" name="id_resp" id="id_resp">
                      <?php foreach ($emp as $e){ $sel=""; if(isset($a) && $e->id_empleado==$a->id_resp) $sel='selected'; else $sel=""; ?>
                        <option value="<?php echo $e->id_empleado; ?>" <?php echo $sel; ?>><?php echo $e->nombre ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-5" id="cont_resp_cli" style="display: none;">
                    <label>Responsable</label>
                    <input placeholder="Indique el nombre del responsable del hallazgo" class="form-control" type="text" name="responsable_cli" value="<?php echo $c->empresa; ?>" readonly>
                    <label>Email</label>
                    <input class="form-control" type="text" id="correo_cli" value="<?php echo $c->correo; ?>" readonly>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">Avance 25%</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input id="avance" name="avance" type="radio" <?php if(isset($a->avance) && $a->avance=="1") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">Avance 50%</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input id="avance2" name="avance" type="radio" <?php if(isset($a->avance) && $a->avance=="2") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">Avance 75%</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input id="avance3" name="avance" type="radio" <?php if(isset($a->avance) && $a->avance=="3") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">Avance 100%</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input id="avance4" name="avance" type="radio" <?php if(isset($a->avance) && $a->avance=="4") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-5">
                    <label>Fecha compromiso</label>
                    <input class="form-control" type="date" name="fecha_compromiso" value="<?php if(isset($a)) echo $a->fecha_compromiso; ?>">
                  </div>
                  <div class="col-sm-5">
                    <label>Fecha de cierre</label>
                    <input class="form-control" type="date" name="fecha_cierre" value="<?php if(isset($a)) echo $a->fecha_cierre; ?>">
                  </div>
                </div>
                <div class="mb-3">
                  <label>Observaciones</label>
                  <textarea class="form-control" name="observaciones" value="<?php if(isset($a)) echo $a->observaciones; ?>"><?php if(isset($a)) echo $a->observaciones; ?></textarea>
                </div>
                <div class="mb-3 row">
                  <div class="col-md-4">
                    <div class="item">
                      <center><label>Evidencia Antes</label></center>
                      <div class="card" align="center" style="height:150px">
                        <span class="img_evidencia">
                          <?php if(isset($a) && $a->antes!=''){ ?>
                            <img id="img_antes" src="<?php echo base_url(); ?>uploads/evidencias/<?php echo $a->antes; ?>" class="rounded mr-3 img-fluid" height="142" width="142">
                          <?php }else{ ?>
                            <img id="img_antes" src="<?php echo base_url(); ?>public/img/no-image.png" class="rounded mr-3" height="142" width="142">
                          <?php } ?>
                        </span>
                      </div>
                      <label class="btn btn-light" for="antes" style="width: 100%">Cargar foto</label>
                      <input type="file" id="antes" hidden>
                      <br>
                      <a class="btn btn-light" style="width: 100%" onclick="reset_img(1)">Eliminar</a>
                      <br>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item">
                      <center><label>Evidencia Durante</label></center>
                      <div class="card" align="center" style="height:150px">
                        <span class="img_evidencia_durante">
                          <?php if(isset($a) && $a->durante!=''){ ?>
                            <img id="img_durante" src="<?php echo base_url(); ?>uploads/evidencias/<?php echo $a->durante; ?>" class="rounded mr-3 img-fluid" height="142" width="142">
                          <?php }else{ ?>
                            <img id="img_durante" src="<?php echo base_url(); ?>public/img/no-image.png" class="rounded mr-3" height="142" width="142">
                          <?php } ?>
                        </span>
                      </div>
                      <label class="btn btn-light" for="durante" style="width: 100%">Cargar foto</label>
                      <input type="file" id="durante" hidden>
                      <br>
                      <a class="btn btn-light" style="width: 100%" onclick="reset_img(2)">Eliminar</a>
                      <br>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item">
                      <center><label align="center">Evidencia Después</label></center>
                      <div class="card" align="center" style="height:150px">
                        <span class="img_evidencia_despues">
                          <?php if(isset($a) && $a->despues!=''){ ?>
                            <img id="img_despues" src="<?php echo base_url(); ?>uploads/evidencias/<?php echo $a->despues; ?>" class="rounded mr-3 img-fluid" height="142" width="142">
                          <?php }else{ ?>
                            <img id="img_despues" src="<?php echo base_url(); ?>public/img/no-image.png" class="rounded mr-3" height="142" width="142">
                          <?php } ?>
                        </span>
                      </div>
                      <label class="btn btn-light" for="despues" style="width: 100%">Cargar foto</label>
                      <input type="file" id="despues" hidden>
                      <br>
                      <a class="btn btn-light" style="width: 100%" onclick="reset_img(3)">Eliminar</a>
                      <br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <a href="<?php echo base_url()?>MatrizAcciones/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <button id="btn_sub" class="btn btn-primary" type="button">Guardar actividad</button>
            </div>
          </div>  
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>