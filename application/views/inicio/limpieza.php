<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Servicio de limpieza</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Inicio/orden_trabajo_from/<?php echo $id_proy; ?>/1">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Órdenes de trabajo</span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-briefcase fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Inicio/orden_trabajo_from/<?php echo $id_proy; ?>/2">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Matriz de Acciones</span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-tv fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Inicio/orden_trabajo_from/<?php echo $id_proy; ?>/3">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Monitoreo Áreas Críticas</span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-search fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Inicio/orden_trabajo_from/<?php echo $id_proy; ?>/4">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Actividades Realizadas </span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-line-chart fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Inicio/orden_trabajo_from/<?php echo $id_proy; ?>/5">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Matríz de Versatilidad</span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-table fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>

            <div class="col-md-4 mb-3 row mx-auto" align="center">
              <button style="width: 260px;" class="btn btn-primary"><a href="<?php echo base_url() ?>Inicio/orden_trabajo_from/<?php echo $id_proy; ?>/6">
                <div class="row texto_centro" style="color:white;">
                  <div class="col-md-9" align="left">
                    <span style="font-size: 18px; font-weight: bold;">Presentismo Rotación </span>
                  </div>
                  <div class="col-md-3" align="right">
                    <i class="fa fa-users fa-3x"></i>
                  </div>
                </div>  
              </a></button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>