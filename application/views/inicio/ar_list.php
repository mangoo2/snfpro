<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Servicio de Limpieza</h3>
        <h4>Actividades Realizadas</h4>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
                <label class="control-label">Departamento</label>
                <select class="form-control round" id="departamento" onchange="loadtable()">
                  <option value="0"></option>
                </select>
              </div>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_ordenes">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Reporte</th>
                      <th scope="col">Supervisor</th>
                      <th scope="col">Mes Consultado</th>
                      <th scope="col">Mes</th>
                      <th scope="col">Descarga</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>