<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Servicio de Limpieza</h3>
        <h4><?php echo $tipo_txt; ?></h4>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="id_cli" value="<?php echo $this->session->userdata("tipo") == 3 ? $this->session->userdata("clienteid") : $this->session->userdata("usuarioid_tz"); ?>">
<input type="hidden" id="tipo" value="<?php echo $tipo; ?>">
<input type="hidden" id="id_proy" value="<?php echo $id_proy; ?>">
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label class="control-label">Proyecto</label>
                <select class="form-control round" id="id_proyecto" disabled>
                  <option value="0"></option>
                </select>
              </div>
            </div>
            <div class="col-sm-3">
              <label>Mes</label>
              <select class="form-control" id="mes">
                <option value="0"></option>
              </select>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_ordenes">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Reporte</th>
                      <th scope="col">Supervisor</th>
                      <th scope="col">Mes Consultado</th>
                      <th scope="col">Descarga</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modaldepartamento" tabindex="-1" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel2">Departamentos</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close" data-bs-original-title="" title=""></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <input type="hidden" id="id_pd" value="0">
          <input type="hidden" id="anio_pd" value="0">
          <input type="hidden" id="tipo_sol" value="0">
          <input type="hidden" id="num_tel" value="0">
          <div class="mb-3 row">
            <label class="col-sm-2 col-form-label">Departamentos</label>
            <div class="col-sm-8">
              <select class="form-control" id="departamento" required>
                <?php foreach ($det_dep->result() as $item) { ?>
                  <option value="<?php echo $item->id;?>"><?php echo $item->departamento;?></option>
                <?php }?>
              </select>
            </div>
          </div>
        </div>        
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button"  onclick="generarPDFDpto()" >Aceptar</button>
        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal" data-bs-original-title="Cerrar" title="">Cerrar</button>
      </div>
    </div>
  </div>
</div>