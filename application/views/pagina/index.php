<!DOCTYPE html>
<html lang="es-MX">


<!-- Mirrored from www.grupoisn.com.mx/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Nov 2022 21:29:58 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
		
			<style>
			.fuse_social_icons_links {
			    display: block;
			}
			.facebook-awesome-social::before {
			    content: "\f09a" !important;
			}
			
			.awesome-social-img img {
			    position: absolute;
			    top: 50%;
			    left: 50%;
			    transform: translate(-50%,-50%);
			}

			.awesome-social-img {
			    position: relative;
			}			
			.icon_wrapper .awesome-social {
			    font-family: 'FontAwesome' !important;
			}
			#icon_wrapper .fuse_social_icons_links .awesome-social {
			    font-family: "FontAwesome" !important;
			    ext-rendering: auto !important;
			    -webkit-font-smoothing: antialiased !important;
			    -moz-osx-font-smoothing: grayscale !important;
			}
									
			
				#icon_wrapper{
					position: fixed;
					top: 25%;
					right: 0px;
					z-index: 99999;
				}

			
			.awesome-social

			{

            margin-top:2px;

			color: #fff !important;

			text-align: center !important;

			display: block;

			
			line-height: 51px !important;

			width: 48px !important;

			height: 48px !important;

			font-size: 28px !important;

			
				border-radius:50% !important;

				


			}

			
			.fuse_social_icons_links

			{

			outline:0 !important;



			}

			.fuse_social_icons_links:hover{

			text-decoration:none !important;

			}

			
			.fb-awesome-social

			{

			background: #3b5998;
			border-color: #3b5998;
			
			}
			.facebook-awesome-social

			{

			background: #3b5998;
			border-color: #3b5998;
						}


			.tw-awesome-social

			{

			background:#00aced;
			border-color: #00aced;
			
			}
			.twitter-awesome-social

			{

			background:#00aced;
			border-color: #00aced;
			
			}
			.rss-awesome-social

			{

			background:#FA9B39;
			border-color: #FA9B39;
			
			}

			.linkedin-awesome-social

			{

			background:#007bb6;
			border-color: #007bb6;
						}

			.youtube-awesome-social

			{

			background:#bb0000;
			border-color: #bb0000;
						}

			.flickr-awesome-social

			{

			background: #ff0084;
			border-color: #ff0084;
						}

			.pinterest-awesome-social

			{

			background:#cb2027;
			border-color: #cb2027;
						}

			.stumbleupon-awesome-social

			{

			background:#f74425 ;
			border-color: #f74425;
						}

			.google-plus-awesome-social

			{

			background:#f74425 ;
			border-color: #f74425;
						}

			.instagram-awesome-social

			{

			    background: -moz-linear-gradient(45deg, #f09433 0%, #e6683c 25%, #dc2743 50%, #cc2366 75%, #bc1888 100%);
			    background: -webkit-linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
			    background: linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
			    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f09433', endColorstr='#bc1888',GradientType=1 );
			    border-color: #f09433;
					    

			}

			.tumblr-awesome-social

			{

			background: #32506d ;
			border-color: #32506d;
						}

			.vine-awesome-social

			{

			background: #00bf8f ;
			border-color: #00bf8f;
						}

            .vk-awesome-social {



            background: #45668e ;
            border-color: #45668e;
            
            }

            .soundcloud-awesome-social

                {

            background: #ff3300 ;
            border-color: #ff3300;
            
                }

                .reddit-awesome-social{



            background: #ff4500 ;
            border-color: #ff4500;

                            }

                .stack-awesome-social{



            background: #fe7a15 ;
            border-color: #fe7a15;
            
                }

                .behance-awesome-social{

            background: #1769ff ;
            border-color: #1769ff;
            
                }

                .github-awesome-social{

            background: #999999 ;
            border-color: #999999;
            


                }

                .envelope-awesome-social{

                  background: #ccc ;
 				  border-color: #ccc;                 
 				                  }

/*  Mobile */






/* Custom Background */


             




			</style>

<meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
<link rel="alternate" href="index.html" hreflang="es" />
<link rel="alternate" href="en/home/index.html" hreflang="en" />

	<!-- This site is optimized with the Yoast SEO plugin v17.8 - https://yoast.com/wordpress/plugins/seo/ -->
	<title>Página Principal - Servicios Nuevos de Filtración</title>
	<meta name="description" content="Servicios Nuevos de Filtración Puebla, Filtros, Mexico Página Principal Todo acerca de filtros de aire, colectores de polvo, equipo de seguridad, filtros de puebla, presencia en Puebla, Mexico, Aguascalientes, Sonora, Hermosillo, Sur, Centro, Norte, de la Republica Mexicana" />
	<link rel="canonical" href="index.html" />
	<meta property="og:locale" content="es_MX" />
	<meta property="og:locale:alternate" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Página Principal - Servicios Nuevos de Filtración" />
	<meta property="og:description" content="Servicios Nuevos de Filtración Puebla, Filtros, Mexico Página Principal Todo acerca de filtros de aire, colectores de polvo, equipo de seguridad, filtros de puebla, presencia en Puebla, Mexico, Aguascalientes, Sonora, Hermosillo, Sur, Centro, Norte, de la Republica Mexicana" />
	<meta property="og:url" content="https://www.grupoisn.com.mx/" />
	<meta property="og:site_name" content="Servicios Nuevos de Filtración" />
	<meta property="article:publisher" content="https://www.facebook.com/snf.grupoisn" />
	<meta property="article:modified_time" content="2021-11-30T21:30:33+00:00" />
	<meta property="og:image" content="https://www.grupoisn.com.mx/wp-content/uploads/2021/08/REPSE.png" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@SNFiltracion" />
	<meta name="twitter:label1" content="Tiempo de lectura" />
	<meta name="twitter:data1" content="2 minutos" />
	<script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://www.grupoisn.com.mx/#website","url":"https://www.grupoisn.com.mx/","name":"Servicios Nuevos de Filtraci\u00f3n","description":"Servicios Nuevos de Filtraci\u00f3n Hispano mexicanos Puebla","potentialAction":[{"@type":"SearchAction","target":{"@type":"EntryPoint","urlTemplate":"https://www.grupoisn.com.mx/?s={search_term_string}"},"query-input":"required name=search_term_string"}],"inLanguage":"es-MX"},{"@type":"ImageObject","@id":"https://www.grupoisn.com.mx/#primaryimage","inLanguage":"es-MX","url":"https://www.grupoisn.com.mx/wp-content/uploads/2021/08/REPSE.png","contentUrl":"https://www.grupoisn.com.mx/wp-content/uploads/2021/08/REPSE.png","width":348,"height":145},{"@type":"WebPage","@id":"https://www.grupoisn.com.mx/#webpage","url":"https://www.grupoisn.com.mx/","name":"P\u00e1gina Principal - Servicios Nuevos de Filtraci\u00f3n","isPartOf":{"@id":"https://www.grupoisn.com.mx/#website"},"primaryImageOfPage":{"@id":"https://www.grupoisn.com.mx/#primaryimage"},"datePublished":"2020-02-12T12:07:32+00:00","dateModified":"2021-11-30T21:30:33+00:00","description":"Servicios Nuevos de Filtraci\u00f3n Puebla, Filtros, Mexico P\u00e1gina Principal Todo acerca de filtros de aire, colectores de polvo, equipo de seguridad, filtros de puebla, presencia en Puebla, Mexico, Aguascalientes, Sonora, Hermosillo, Sur, Centro, Norte, de la Republica Mexicana","breadcrumb":{"@id":"https://www.grupoisn.com.mx/#breadcrumb"},"inLanguage":"es-MX","potentialAction":[{"@type":"ReadAction","target":["https://www.grupoisn.com.mx/"]}]},{"@type":"BreadcrumbList","@id":"https://www.grupoisn.com.mx/#breadcrumb","itemListElement":[{"@type":"ListItem","position":1,"name":"Inicio"}]}]}</script>
	<!-- / Yoast SEO plugin. -->


<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link rel="alternate" type="application/rss+xml" title="Servicios Nuevos de Filtración &raquo; Feed" href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Servicios Nuevos de Filtración &raquo; RSS de los comentarios" href="comments/feed/index.html" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.grupoisn.com.mx\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.8.6"}};
			!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([10084,65039,8205,55357,56613],[10084,65039,8203,55357,56613])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>

<style type="text/css">
/* [Object] Modal
 * =============================== */
.modal {
  opacity: 0;
  visibility: hidden;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  text-align: left;
  background: rgb(0 0 0 / 0%);
  transition: opacity .25s ease;
}

.modal-state {
  display: none;
}

.modal-state:checked + .modal {
  opacity: 1;
  visibility: visible;
}

.modal__inner {
  float: right;
  bottom: 0;
  left: 0;
  width: 41%;
  margin: auto;
  overflow: auto;
  background: #fff;
  border-radius: 5px;
  padding: 1em 3.8em;
  height: 100%;
}

.modal__close {
  position: absolute;
  right: 1em;
  top: 1em;
  width: 1.9em;
  height: 1.9em;
  cursor: pointer;
  background-color: #0132cf;
  border-radius: 19px;
}

.modal__close:after,
.modal__close:before {
  content: '';
  position: absolute;
  width: 2px;
  height: 1.5em;
  background: #fff;
  display: block;
  transform: rotate(45deg);
  left: 50%;
  margin: 2px 0 0 -1px;
  top: 0;
}

.modal__close:hover:after,
.modal__close:hover:before {
  background: #aaa;
}

.modal__close:before {
  transform: rotate(-45deg);
}

@media screen and (max-width: 768px) {
	
  .modal__inner {
    width: 90%;
    height: 90%;
    box-sizing: border-box;
  }
}

.vd_red{
    color: red;
} 
/* Other
 * =============================== */
body {
  padding: 1%;
  font: 1/1.5em sans-serif;
  text-align: center;
}

.btn {
  cursor: pointer;
  background: #27ae60;
  display: inline-block;
  padding: .5em 1em;
  color: #fff;
  border-radius: 3px;
}

.btn:hover,
.btn:focus {
  background: #2ecc71;
}

.btn:active {
  background: #27ae60;
  box-shadow: 0 1px 2px rgba(0,0,0, .2) inset;
}

.btn--blue {
  background: #2980b9;
}

.btn--blue:hover,
.btn--blue:focus {
  background: #3498db;
}

.btn--blue:active {
  background: #2980b9;
}

p img {
  max-width: 200px;
  height: auto;
  float: left;
  margin: 0 1em 1em 0;
}

.input-group{
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-align: stretch;
    -ms-flex-align: stretch;
    align-items: stretch;
    width: 100%;
}

.input-group-text{
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: center;
    white-space: nowrap;
    background-color: #e9ecef;
    border: 1px solid #ced4da;
    border-radius: 0.25rem;
}

</style>
	<link rel='stylesheet' id='pt-cv-public-style-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/content-views-query-and-display-post-page/public/assets/css/cv749c.css?ver=2.4.0.2' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='<?php echo base_url(); ?>pagina_css/wp-includes/css/dist/block-library/style.min6281.css?ver=5.8.6' type='text/css' media='all' />
<link rel='stylesheet' id='cliengo-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/cliengo/public/css/cliengo-public4c56.css?ver=2.0.2' type='text/css' media='all' />
<link rel='stylesheet' id='dae-download-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/download-after-email/css/downloadf88a.css?ver=1638339176' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='<?php echo base_url(); ?>pagina_css/wp-includes/css/dashicons.min6281.css?ver=5.8.6' type='text/css' media='all' />
<link rel='stylesheet' id='dae-fa-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/download-after-email/css/all6281.css?ver=5.8.6' type='text/css' media='all' />
<link rel='stylesheet' id='fontawesome-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/fuse-social-floating-sidebar/inc/font-awesome/css/font-awesome.min6281.css?ver=5.8.6' type='text/css' media='all' />
<link rel='stylesheet' id='hfe-style-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/header-footer-elementor/assets/css/header-footer-elementor7514.css?ver=1.6.6' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min05c8.css?ver=5.13.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-legacy-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/css/frontend-legacy.min1aae.css?ver=3.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/css/frontend.min1aae.css?ver=3.5.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-329-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/uploads/elementor/css/post-3291cd3.css?ver=1664935675' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-38-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/uploads/elementor/css/post-38a826.css?ver=1664981351' type='text/css' media='all' />
<link rel='stylesheet' id='hfe-widgets-style-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/header-footer-elementor/inc/widgets-css/frontend7514.css?ver=1.6.6' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-455-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/uploads/elementor/css/post-4551cd3.css?ver=1664935675' type='text/css' media='all' />
<link rel='stylesheet' id='wordpress-popular-posts-css-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/wordpress-popular-posts/assets/css/wppdeba.css?ver=5.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='neve-style-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/themes/neve/style.minc141.css?ver=2.6.1' type='text/css' media='all' />
<style id='neve-style-inline-css' type='text/css'>
body, .site-title{font-family:"Poppins";}h1:not(.site-title), .single h1.entry-title{font-size:35px;}h1:not(.site-title), .single h1.entry-title{font-weight:600;}h1:not(.site-title), .single h1.entry-title{text-transform:none;}h1:not(.site-title), .single h1.entry-title{letter-spacing:0px;}h1:not(.site-title), .single h1.entry-title{line-height:1.2;}h2{font-size:30px;}h2{font-weight:600;}h2{text-transform:none;}h2{letter-spacing:0px;}h2{line-height:1.6;}h3{font-size:20px;}h3{font-weight:600;}h3{text-transform:none;}h3{letter-spacing:0px;}h3{line-height:1.6;}h4{font-size:18px;}h4{font-weight:600;}h4{text-transform:none;}h4{letter-spacing:0px;}h4{line-height:1.6;}h5{font-size:16px;}h5{font-weight:600;}h5{text-transform:none;}h5{letter-spacing:0px;}h5{line-height:1.6;}h6{font-size:14em;}h6{font-weight:600;}h6{text-transform:none;}h6{letter-spacing:0px;}h6{line-height:1.6;}h1, .single h1.entry-title, h2, h3, h4, h5, h6{font-family:Arial, Helvetica, sans-serif;}a{color:#1e303f;}.nv-loader{border-color:#1e303f;}a:hover, a:focus{color:#1e303f;}body, .entry-title a, .entry-title a:hover, .entry-title a:focus{color:#ffffff;}.has-white-background-color{background-color:#ffffff!important;}.has-white-color{color:#ffffff!important;}.has-black-background-color{background-color:#000000!important;}.has-black-color{color:#000000!important;}.has-neve-button-color-background-color{background-color:#0366d6!important;}.has-neve-button-color-color{color:#0366d6!important;}.has-neve-link-color-background-color{background-color:#1e303f!important;}.has-neve-link-color-color{color:#1e303f!important;}.has-neve-link-hover-color-background-color{background-color:#1e303f!important;}.has-neve-link-hover-color-color{color:#1e303f!important;}.has-neve-text-color-background-color{background-color:#ffffff!important;}.has-neve-text-color-color{color:#ffffff!important;}.nv-tags-list a{color:#fcaf3b;}.nv-tags-list a{border-color:#fcaf3b;} .button.button-primary, button, input[type=button], .btn, input[type="submit"], /* Buttons in navigation */ ul[id^="nv-primary-navigation"] li.button.button-primary > a, .menu li.button.button-primary > a{background-color:#fcaf3b;} .button.button-primary, button, input[type=button], .btn, input[type="submit"], /* Buttons in navigation */ ul[id^="nv-primary-navigation"] li.button.button-primary > a, .menu li.button.button-primary > a{color:#000000;}.nv-tags-list a:hover{border-color:#0366d6;} .button.button-primary:hover, .nv-tags-list a:hover, ul[id^="nv-primary-navigation"] li.button.button-primary > a:hover, .menu li.button.button-primary > a:hover{background-color:#0366d6;} .button.button-primary:hover, .nv-tags-list a:hover, ul[id^="nv-primary-navigation"] li.button.button-primary > a:hover, .menu li.button.button-primary > a:hover{color:#ffffff;}.button.button-primary, button, input[type=button], .btn, input[type="submit"]:not(.search-submit), /* Buttons in navigation */ ul[id^="nv-primary-navigation"] li.button.button-primary > a, .menu li.button.button-primary > a{border-radius:0px;}.button.button-secondary, #comments input[type="submit"]{background-color:#2b2b2b;} .button.button-secondary, #comments input[type="submit"]{color:#ffffff;}.button.button-secondary, #comments input[type="submit"]{border-color:#ffffff;} .button.button-secondary:hover, #comments input[type="submit"]:hover{color:#676767;}.button.button-secondary:hover, #comments input[type="submit"]:hover{border-color:#676767;}.button.button-secondary, #comments input[type="submit"]{border-radius:0px;}.button.button-secondary, #comments input[type="submit"]{border:none;}@media(min-width: 576px){h1:not(.site-title), .single h1.entry-title{font-size:55px;}h1:not(.site-title), .single h1.entry-title{letter-spacing:0px;}h1:not(.site-title), .single h1.entry-title{line-height:1.6;}h2{font-size:35px;}h2{letter-spacing:0px;}h2{line-height:1.6;}h3{font-size:25px;}h3{letter-spacing:0px;}h3{line-height:1.6;}h4{font-size:18px;}h4{letter-spacing:0px;}h4{line-height:1.6;}h5{font-size:16px;}h5{letter-spacing:0px;}h5{line-height:1.6;}h6{font-size:14em;}h6{letter-spacing:0px;}h6{line-height:1.6;}}@media(min-width: 960px){h1:not(.site-title), .single h1.entry-title{font-size:70px;}h1:not(.site-title), .single h1.entry-title{letter-spacing:0px;}h1:not(.site-title), .single h1.entry-title{line-height:1.2;}h2{font-size:50px;}h2{letter-spacing:0px;}h2{line-height:1.2;}h3{font-size:25px;}h3{letter-spacing:0px;}h3{line-height:1.2;}h4{font-size:18px;}h4{letter-spacing:0px;}h4{line-height:1.6;}h5{font-size:16px;}h5{letter-spacing:0px;}h5{line-height:1.6;}h6{font-size:14px;}h6{letter-spacing:0px;}h6{line-height:1.6;}.neve-main > .archive-container .nv-index-posts.col{max-width:100%;}.neve-main > .archive-container .nv-sidebar-wrap{max-width:0%;}.caret-wrap{border-color:transparent;}}.hfg_header .header-top-inner,.hfg_header .header-top-inner.dark-mode,.hfg_header .header-top-inner.light-mode { } @media (max-width: 576px) { .header-main-inner { height: auto; } .builder-item--logo .site-logo img { max-width: 120px; } .builder-item--logo .site-logo { padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px; } .builder-item--logo { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--nav-icon .navbar-toggle { padding-top: 10px; padding-right: 15px; padding-bottom: 10px; padding-left: 15px; } .builder-item--nav-icon { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--primary-menu li > a { font-size: 1em; line-height: 1.6; letter-spacing: 0px; } .builder-item--primary-menu li > a svg { height: 1em; width: 1em; } .builder-item--primary-menu li > a.builder-item--primary-menu li > a { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; } .builder-item--primary-menu { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--button_base > .component-wrap > .button { padding-top: 8px; padding-right: 12px; padding-bottom: 8px; padding-left: 12px; } .builder-item--button_base { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--custom_html { font-size: 1em; line-height: 1.6; letter-spacing: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--custom_html svg { height: 1em; width: 1em; } .builder-item--header_search { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--header_search_responsive { padding-top: 0px; padding-right: 10px; padding-bottom: 0px; padding-left: 10px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--secondary-menu #secondary-menu li > a { font-size: 1em; line-height: 1.6; letter-spacing: 0px; } .builder-item--secondary-menu #secondary-menu li > a svg { height: 1em; width: 1em; } .builder-item--secondary-menu { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-one-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-two-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-three-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-four-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-menu li > a { font-size: 1em; line-height: 1.6; letter-spacing: 0px; } .builder-item--footer-menu li > a svg { height: 1em; width: 1em; } .builder-item--footer-menu { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer_copyright { font-size: 1em; line-height: 1.6; letter-spacing: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer_copyright svg { height: 1em; width: 1em; } } @media (min-width: 576px) { .header-main-inner { height: auto; } .builder-item--logo .site-logo img { max-width: 120px; } .builder-item--logo .site-logo { padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px; } .builder-item--logo { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--nav-icon .navbar-toggle { padding-top: 10px; padding-right: 15px; padding-bottom: 10px; padding-left: 15px; } .builder-item--nav-icon { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--primary-menu li > a { font-size: 1em; line-height: 1.6; letter-spacing: 0px; } .builder-item--primary-menu li > a svg { height: 1em; width: 1em; } .builder-item--primary-menu li > a.builder-item--primary-menu li > a { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; } .builder-item--primary-menu { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--button_base > .component-wrap > .button { padding-top: 8px; padding-right: 12px; padding-bottom: 8px; padding-left: 12px; } .builder-item--button_base { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--custom_html { font-size: 1em; line-height: 1.6; letter-spacing: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--custom_html svg { height: 1em; width: 1em; } .builder-item--header_search { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--header_search_responsive { padding-top: 0px; padding-right: 10px; padding-bottom: 0px; padding-left: 10px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--secondary-menu #secondary-menu li > a { font-size: 1em; line-height: 1.6; letter-spacing: 0px; } .builder-item--secondary-menu #secondary-menu li > a svg { height: 1em; width: 1em; } .builder-item--secondary-menu { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-one-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-two-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-three-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-four-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-menu li > a { font-size: 1em; line-height: 1.6; letter-spacing: 0px; } .builder-item--footer-menu li > a svg { height: 1em; width: 1em; } .builder-item--footer-menu { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer_copyright { font-size: 1em; line-height: 1.6; letter-spacing: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer_copyright svg { height: 1em; width: 1em; } } @media (min-width: 961px) { .header-main-inner { height: auto; } .builder-item--logo .site-logo img { max-width: 120px; } .builder-item--logo .site-logo { padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px; } .builder-item--logo { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--nav-icon .navbar-toggle { padding-top: 10px; padding-right: 15px; padding-bottom: 10px; padding-left: 15px; } .builder-item--nav-icon { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--primary-menu li > a { font-size: 13px; line-height: 1.6; letter-spacing: 2px; } .builder-item--primary-menu li > a svg { height: 13px; width: 13px; } .builder-item--primary-menu li > a.builder-item--primary-menu li > a { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; } .builder-item--primary-menu { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--button_base > .component-wrap > .button { padding-top: 8px; padding-right: 12px; padding-bottom: 8px; padding-left: 12px; } .builder-item--button_base { margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--custom_html { font-size: 1em; line-height: 1.6; letter-spacing: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--custom_html svg { height: 1em; width: 1em; } .builder-item--header_search { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--header_search_responsive { padding-top: 0px; padding-right: 10px; padding-bottom: 0px; padding-left: 10px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--secondary-menu #secondary-menu li > a { font-size: 1em; line-height: 1.6; letter-spacing: 0px; } .builder-item--secondary-menu #secondary-menu li > a svg { height: 1em; width: 1em; } .builder-item--secondary-menu { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-one-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-two-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-three-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-four-widgets { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer-menu li > a { font-size: 1em; line-height: 1.6; letter-spacing: 0px; } .builder-item--footer-menu li > a svg { height: 1em; width: 1em; } .builder-item--footer-menu { padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer_copyright { font-size: 0.8em; line-height: 1.6; letter-spacing: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; } .builder-item--footer_copyright svg { height: 0.8em; width: 0.8em; } } .hfg_header .header-main-inner,.hfg_header .header-main-inner.dark-mode,.hfg_header .header-main-inner.light-mode { } .hfg_header .header-bottom-inner,.hfg_header .header-bottom-inner.dark-mode,.hfg_header .header-bottom-inner.light-mode { } .hfg_header .header-menu-sidebar .header-menu-sidebar-bg,.hfg_header .header-menu-sidebar .header-menu-sidebar-bg.dark-mode,.hfg_header .header-menu-sidebar .header-menu-sidebar-bg.light-mode { } .builder-item--nav-icon .navbar-toggle { border-radius: 3px; border: 1px solid; } .header-menu-sidebar .close-sidebar-panel .navbar-toggle { border-radius: 3px; border: 1px solid; } .builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li:not(.woocommerce-mini-cart-item) > a { color: #18191d; } .builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li > a .caret-wrap svg,.builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li > .amp-caret-wrap svg { fill: #18191d; } .builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li:not(.woocommerce-mini-cart-item) > a:after { background-color: #18191d; } .builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li:not(.woocommerce-mini-cart-item):hover > a { color: #18191d; } .builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li:hover > a .caret-wrap svg,.builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li:hover > .amp-caret-wrap svg { fill: #18191d; } .builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li.current-menu-item > a { color: #18191d; } .builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li.current-menu-item > a .caret-wrap svg,.builder-item--primary-menu .nav-menu-primary > .primary-menu-ul li.current-menu-item > .amp-caret-wrap svg { fill: #18191d; } @media (min-width: 961px) { .header--row .hfg-item-right .builder-item--primary-menu .primary-menu-ul > li:not(:first-child) { margin-left: 20px; } .header--row .hfg-item-center .builder-item--primary-menu .primary-menu-ul > li:not(:last-child), .header--row .hfg-item-left .builder-item--primary-menu .primary-menu-ul > li:not(:last-child) { margin-right: 20px; } .builder-item--primary-menu .style-full-height .primary-menu-ul > li:not(.menu-item-nav-search):not(.menu-item-nav-cart) > a:after { left: -10px; right: -10px; } .builder-item--primary-menu .style-full-height .primary-menu-ul:not(#nv-primary-navigation-sidebar) > li:not(.menu-item-nav-search):not(.menu-item-nav-cart):hover > a:after { width: calc(100% + 20px) !important;; } } .builder-item--primary-menu .primary-menu-ul > li > a { height: 25px; } .builder-item--primary-menu li > a { font-weight: 600; text-transform: uppercase; } .builder-item--button_base > .component-wrap > .button { border-radius: 3px; } .builder-item--custom_html { font-weight: 500; text-transform: none; } .builder-item--header_search_responsive .nv-search > svg { width: 15px; height: 15px; } .hfg-item-right .builder-item--secondary-menu #secondary-menu > li:not(:first-child) { margin-left: 20px; } .hfg-item-center .builder-item--secondary-menu #secondary-menu li:not(:last-child), .hfg-item-left .builder-item--secondary-menu #secondary-menu > li:not(:last-child) { margin-right: 20px; } .builder-item--secondary-menu .style-full-height #secondary-menu > li > a:after { left: -10px; right: -10px; } .builder-item--secondary-menu .style-full-height #secondary-menu > li:hover > a:after { width: calc(100% + 20px) !important;; } .builder-item--secondary-menu #secondary-menu > li > a { height: 25px; } .builder-item--secondary-menu #secondary-menu li > a { font-weight: 500; text-transform: none; } .footer-top-inner,.footer-top-inner.dark-mode,.footer-top-inner.light-mode { } .footer-bottom-inner,.footer-bottom-inner.dark-mode,.footer-bottom-inner.light-mode { background-color: #1e303f; } .hfg-item-right .builder-item--footer-menu .footer-menu > li:not(:first-child) { margin-left: 20px; } .hfg-item-center .builder-item--footer-menu .footer-menu li:not(:last-child), .hfg-item-left .builder-item--footer-menu .footer-menu > li:not(:last-child) { margin-right: 20px; } .builder-item--footer-menu .style-full-height .footer-menu > li > a:after { left: -10px !important; right: -10px !important; } .builder-item--footer-menu .style-full-height .footer-menu > li:hover > a:after { width: calc(100% + 20px) !important;; } .builder-item--footer-menu .footer-menu > li > a { height: 25px; } .builder-item--footer-menu li > a { font-weight: 500; text-transform: none; } .builder-item--footer_copyright { font-weight: 300; text-transform: none; } 
</style>
<link rel='stylesheet' id='neve-google-font-poppins-css'  href='http://fonts.googleapis.com/css?family=Poppins%3A400&amp;ver=5.8.6' type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CKarla%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPrata%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CHind%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLato%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=5.8.6' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-shared-0-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min52d5.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-solid-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min52d5.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-brands-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min52d5.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-regular-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min52d5.css?ver=5.15.3' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-includes/js/jquery/jquery.minaf6c.js?ver=3.6.0' id='jquery-core-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-includes/js/jquery/jquery-migrate.mind617.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/cliengo/public/js/cliengo-public4c56.js?ver=2.0.2' id='cliengo-js'></script>
<script type='text/javascript' id='fuse-social-script-js-extra'>
/* <![CDATA[ */
var fuse_social = {"ajax_url":"https:\/\/www.grupoisn.com.mx\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/fuse-social-floating-sidebar/inc/js/fuse_scriptdb1a.js?ver=1735719766' id='fuse-social-script-js'></script>
<script type='application/json' id='wpp-json'>
{"sampling_active":0,"sampling_rate":100,"ajax_url":"https:\/\/www.grupoisn.com.mx\/wp-json\/wordpress-popular-posts\/v1\/popular-posts","api_url":"https:\/\/www.grupoisn.com.mx\/wp-json\/wordpress-popular-posts","ID":0,"token":"6bec034765","lang":"es","debug":0}
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/wordpress-popular-posts/assets/js/wpp.mindeba.js?ver=5.5.0' id='wpp-js-js'></script>
<link rel="https://api.w.org/" href="<?php echo base_url(); ?>pagina_css/wp-json/index.html" /><link rel="alternate" type="application/json" href="<?php echo base_url(); ?>pagina_css/wp-json/wp/v2/pages/38.json" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?php echo base_url(); ?>pagina_css/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.8.6" />
<link rel='shortlink' href='index.html' />
<link rel="alternate" type="application/json+oembed" href="<?php echo base_url(); ?>pagina_css/wp-json/oembed/1.0/embed27ac.json?url=https%3A%2F%2Fwww.grupoisn.com.mx%2F" />
<link rel="alternate" type="text/xml+oembed" href="<?php echo base_url(); ?>pagina_css/wp-json/oembed/1.0/embed1166?url=https%3A%2F%2Fwww.grupoisn.com.mx%2F&amp;format=xml" />
<meta name="framework" content="Redux 4.1.29" /><style id="mystickymenu" type="text/css">#mysticky-nav { width:100%; position: static; }#mysticky-nav.wrapfixed { position:fixed; left: 0px; margin-top:0px;  z-index: 99990; -webkit-transition: 0.3s; -moz-transition: 0.3s; -o-transition: 0.3s; transition: 0.3s; -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)"; filter: alpha(opacity=100); opacity:1; background-color: #1e73be;}#mysticky-nav.wrapfixed .myfixed{ background-color: #1e73be; position: relative;top: auto;left: auto;right: auto;}#mysticky-nav .myfixed { margin:0 auto; float:none; border:0px; background:none; max-width:100%; }</style>			<style type="text/css">
																															</style>
			<!-- Introduce aquí tus scripts -->        <style>
            @-webkit-keyframes bgslide {
                from {
                    background-position-x: 0;
                }
                to {
                    background-position-x: -200%;
                }
            }

            @keyframes bgslide {
                    from {
                        background-position-x: 0;
                    }
                    to {
                        background-position-x: -200%;
                    }
            }

            .wpp-widget-placeholder {
                margin: 0 auto;
                width: 60px;
                height: 3px;
                background: #dd3737;
                background: -webkit-gradient(linear, left top, right top, from(#dd3737), color-stop(10%, #571313), to(#dd3737));
                background: linear-gradient(90deg, #dd3737 0%, #571313 10%, #dd3737 100%);
                background-size: 200% auto;
                border-radius: 3px;
                -webkit-animation: bgslide 1s infinite linear;
                animation: bgslide 1s infinite linear;
            }
        </style>
        <style type="text/css" id="custom-background-css">
body.custom-background { background-color: #1e73be; }
</style>
			<style type="text/css" id="wp-custom-css">
			label.control-label, span.cf2-file-name.file-name, div.caldera-form-page, div.caldera-forms-summary-field{
    color: #1E303F;
}

progress.cf2-file-progress-bar{
	margin-top: 6px;
}

.intro-bg{
	background: rgba(30, 48, 63, 0.5)
}

.hightlig-text{
	color: #1E303F;	
}
.elementor-widget-container {
    border-color: #1E303F !important;
}

.caldera-grid .help-block {
    color: #a94442 !important;
}

.pt-cv-readmore {
    background-color: #1E303F !important;
    border: none !important;
}

.pt-cv-view .carousel-control {
    color: #1E303F;
}

.b__news{
	color: #1E303F;
}

/** Start Envato Elements CSS: Blocks (69-3-4f8cfb8a1a68ec007f2be7a02bdeadd9) **/

::placeholder {
    color: #fff;
}


::placeholder:hover {
    color: #fff;
}

.search-form input[type=search], .woocommerce-product-search input[type=search]{
	color: #fff;
}


.envato-kit-66-menu .e--pointer-framed .elementor-item:before{
	border-radius:1px;
}

.envato-kit-66-subscription-form .elementor-form-fields-wrapper{
	position:relative;
}

.envato-kit-66-subscription-form .elementor-form-fields-wrapper .elementor-field-type-submit{
	position:static;
}

.envato-kit-66-subscription-form .elementor-form-fields-wrapper .elementor-field-type-submit button{
	position: absolute;
    top: 50%;
    right: 6px;
    transform: translate(0, -50%);
		-moz-transform: translate(0, -50%);
		-webmit-transform: translate(0, -50%);
}

.envato-kit-66-testi-slider .elementor-testimonial__footer{
	margin-top: -60px !important;
	z-index: 99;
  position: relative;
}

.envato-kit-66-featured-slider .elementor-slides .slick-prev{
	width:50px;
	height:50px;
	background-color:#ffffff !important;
	transform:rotate(45deg);
	-moz-transform:rotate(45deg);
	-webkit-transform:rotate(45deg);
	left:-25px !important;
	-webkit-box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.32);
	-moz-box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.32);
	box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.32);
}

.envato-kit-66-featured-slider .elementor-slides .slick-prev:before{
	display:block;
	margin-top:0px;
	margin-left:0px;
	transform:rotate(-45deg);
	-moz-transform:rotate(-45deg);
	-webkit-transform:rotate(-45deg);
}

.envato-kit-66-featured-slider .elementor-slides .slick-next{
	width:50px;
	height:50px;
	background-color:#ffffff !important;
	transform:rotate(45deg);
	-moz-transform:rotate(45deg);
	-webkit-transform:rotate(45deg);
	right:-25px !important;
	-webkit-box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.32);
	-moz-box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.32);
	box-shadow: 0px 1px 2px 1px rgba(0,0,0,0.32);
}

.envato-kit-66-featured-slider .elementor-slides .slick-next:before{
	display:block;
	margin-top:-5px;
	margin-right:-5px;
	transform:rotate(-45deg);
	-moz-transform:rotate(-45deg);
	-webkit-transform:rotate(-45deg);
}

.envato-kit-66-orangetext{
	color:#f4511e;
}

.envato-kit-66-countdown .elementor-countdown-label{
	display:inline-block !important;
	border:2px solid rgba(255,255,255,0.2);
	padding:9px 20px;
}

/** End Envato Elements CSS: Blocks (69-3-4f8cfb8a1a68ec007f2be7a02bdeadd9) **/



/** Start Envato Elements CSS: Blocks (144-3-3a7d335f39a8579c20cdf02f8d462582) **/

.envato-block__preview{overflow: visible;}

/* Envato Kit 141 Custom Styles - Applied to the element under Advanced */

.elementor-headline-animation-type-drop-in .elementor-headline-dynamic-wrapper{
	text-align: center;
}
.envato-kit-141-top-0 h1,
.envato-kit-141-top-0 h2,
.envato-kit-141-top-0 h3,
.envato-kit-141-top-0 h4,
.envato-kit-141-top-0 h5,
.envato-kit-141-top-0 h6,
.envato-kit-141-top-0 p {
	margin-top: 0;
}

.envato-kit-141-newsletter-inline .elementor-field-textual.elementor-size-md {
	padding-left: 1.5rem;
	padding-right: 1.5rem;
}

.envato-kit-141-bottom-0 p {
	margin-bottom: 0;
}

.envato-kit-141-bottom-8 .elementor-price-list .elementor-price-list-item .elementor-price-list-header {
	margin-bottom: .5rem;
}

.envato-kit-141.elementor-widget-testimonial-carousel.elementor-pagination-type-bullets .swiper-container {
	padding-bottom: 52px;
}

.envato-kit-141-display-inline {
	display: inline-block;
}

.envato-kit-141 .elementor-slick-slider ul.slick-dots {
	bottom: -40px;
}

/** End Envato Elements CSS: Blocks (144-3-3a7d335f39a8579c20cdf02f8d462582) **/



/** Start Envato Elements CSS: Blocks (105-3-0fb64e69c49a8e10692d28840c54ef95) **/

.envato-kit-102-phone-overlay {
	position: absolute !important;
	display: block !important;
	top: 0%;
	left: 0%;
	right: 0%;
	margin: auto;
	z-index: 1;
}

/** End Envato Elements CSS: Blocks (105-3-0fb64e69c49a8e10692d28840c54ef95) **/

		</style>
		</head>

<body id="body_home" class="home page-template page-template-page-templates page-template-template-pagebuilder-full-width page-template-page-templatestemplate-pagebuilder-full-width-php page page-id-38 custom-background wp-custom-logo ehf-footer ehf-template-neve ehf-stylesheet-neve nv-sidebar-right menu_sidebar_slide_left elementor-default elementor-kit-329 elementor-page elementor-page-38" >
<div class="wrapper">
	<header class="header" role="banner">
		<a class="neve-skip-link show-on-focus" href="#content" tabindex="0">
			Saltar al contenido		</a>
		<div id="header-grid"  class="hfg_header site-header">
	
<nav class="header--row header-main hide-on-mobile hide-on-tablet layout-full-contained nv-navbar header--row"
	data-row-id="main" data-show-on="desktop">

	<div
		class="header--row-inner header-main-inner light-mode">
		<div class="container">
			<div
				class="row row--wrapper"
				data-section="hfg_header_layout_main" >
				<div class="builder-item hfg-item-first col-1 col-md-1 col-sm-1 hfg-item-left"><div class="item--inner builder-item--logo"
		data-section="title_tagline"
		data-item-id="logo">
	<div class="site-logo">
	<a class="brand" href="index.html" title="Servicios Nuevos de Filtración"
			aria-label="Servicios Nuevos de Filtración">
		<img src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2022/02/SNFPRO.jpg" alt="">	</a>
</div>

	</div>

</div><div class="builder-item col-8 col-md-8 col-sm-8 hfg-item-right"><div class="item--inner builder-item--primary-menu has_menu"
		data-section="header_menu_primary"
		data-item-id="primary-menu">
	<div class="nv-nav-wrap">
	<div role="navigation" class="style-plain nav-menu-primary"
			aria-label="Menú principal">

		<ul id="nv-primary-navigation-main" class="primary-menu-ul"><li id="menu-item-22" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-22"><a href="index.html" aria-current="page">&nbsp;Inicio</a></li>
<li id="menu-item-113" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113"><a href="conocenos/index.html">Conócenos</a></li>
<li id="menu-item-622" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-622"><a href="#"><span class="menu-item-title-wrap">Portafolio</span><div class="caret-wrap 3" tabindex="0"><span class="caret"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"/></svg></span></div></a>
<ul class="sub-menu">
	<li id="menu-item-506" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-506"><a href="productos/index.html">Productos</a></li>
	<li id="menu-item-1220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1220"><a href="servicios/index.html">Servicios</a></li>
</ul>
</li>
<li id="menu-item-115" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115"><a href="casos-de-exito/index.html">Clientes</a></li>
<li id="menu-item-1568" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1568"><a href="#"><span class="menu-item-title-wrap">Proveedores</span><div class="caret-wrap 7" tabindex="0"><span class="caret"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"/></svg></span></div></a>
<ul class="sub-menu">
	<li id="menu-item-1569" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1569"><a href="sostenibilidad/index.html">Sostenibilidad</a></li>
	<li id="menu-item-1570" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1570"><a href="carta-compromiso/index.html">Carta Compromiso</a></li>
</ul>
</li>
<li id="menu-item-166" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166"><a href="contacto/index.html">Contacto</a></li>
<li id="menu-item-1485" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1485"><a href="integracion-servicios-mexicanos/index.html">Ismex</a></li>
<li id="menu-item-510" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-510"><a href="bolsa-de-trabajo/index.html">Bolsa de Trabajo</a></li>
<li id="menu-item-1267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1267"><a href="noticias/index.html">Noticias</a></li>
<li id="menu-item-1073" class="pll-parent-menu-item menu-item menu-item-type-custom menu-item-object-custom current-menu-parent menu-item-has-children menu-item-1073"><a href="#pll_switcher"><span class="menu-item-title-wrap"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAflBMVEUAXgnp6ejj49zOAADGAACBvZl+u5Z2t4/9/f36+vrnZ2tmrYNdqXv29vbjR03fQkjjWFy4AABVpHba0LzfOD5PoXHg1sXHsIyDXjzSxq8AUQCrtaONfWn08fHbLTPgTVBInWudrZk7lmHYIymvAAAAQwAANwAAJQDU1NTKxMQZpDQjAAAAaklEQVR4ATXItUEEUAAFsLxvuDc4+2+Fu0ODnFSXMgGBRIguWQS/0YdMZqJFkiIyWFttkk4jqs1kfHcohzG059YetlndUhh1zYF+ZnUVhbpW3A4X6O97be1jDevP1nRoPGUAz9ByFAJZgjmA3Q34KhcIQAAAAABJRU5ErkJggg==" alt="Español" width="16" height="11" style="width: 16px; height: 11px;" /></span><div class="caret-wrap 14" tabindex="0"><span class="caret"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"/></svg></span></div></a>
<ul class="sub-menu">
	<li id="menu-item-1073-es" class="lang-item lang-item-12 lang-item-es current-lang lang-item-first menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-1073-es"><a href="index.html" hreflang="es-MX" lang="es-MX"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAflBMVEUAXgnp6ejj49zOAADGAACBvZl+u5Z2t4/9/f36+vrnZ2tmrYNdqXv29vbjR03fQkjjWFy4AABVpHba0LzfOD5PoXHg1sXHsIyDXjzSxq8AUQCrtaONfWn08fHbLTPgTVBInWudrZk7lmHYIymvAAAAQwAANwAAJQDU1NTKxMQZpDQjAAAAaklEQVR4ATXItUEEUAAFsLxvuDc4+2+Fu0ODnFSXMgGBRIguWQS/0YdMZqJFkiIyWFttkk4jqs1kfHcohzG059YetlndUhh1zYF+ZnUVhbpW3A4X6O97be1jDevP1nRoPGUAz9ByFAJZgjmA3Q34KhcIQAAAAABJRU5ErkJggg==" alt="Español" width="16" height="11" style="width: 16px; height: 11px;" /></a></li>
	<li id="menu-item-1073-en" class="lang-item lang-item-15 lang-item-en menu-item menu-item-type-custom menu-item-object-custom menu-item-1073-en"><a href="en/home/index.html" hreflang="en-US" lang="en-US"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAmVBMVEViZsViZMJiYrf9gnL8eWrlYkjgYkjZYkj8/PujwPybvPz4+PetraBEgfo+fvo3efkydfkqcvj8Y2T8UlL8Q0P8MzP9k4Hz8/Lu7u4DdPj9/VrKysI9fPoDc/EAZ7z7IiLHYkjp6ekCcOTk5OIASbfY/v21takAJrT5Dg6sYkjc3Nn94t2RkYD+y8KeYkjs/v7l5fz0dF22YkjWvcOLAAAAgElEQVR4AR2KNULFQBgGZ5J13KGGKvc/Cw1uPe62eb9+Jr1EUBFHSgxxjP2Eca6AfUSfVlUfBvm1Ui1bqafctqMndNkXpb01h5TLx4b6TIXgwOCHfjv+/Pz+5vPRw7txGWT2h6yO0/GaYltIp5PT1dEpLNPL/SdWjYjAAZtvRPgHJX4Xio+DSrkAAAAASUVORK5CYII=" alt="English" width="16" height="11" style="width: 16px; height: 11px;" /></a></li>
</ul>
</li>
</ul>	</div>
</div>

	</div>

</div><div class="builder-item hfg-item-last col-3 col-md-3 col-sm-3 hfg-item-left"><div class="item--inner builder-item--custom_html"
		data-section="custom_html"
		data-item-id="custom_html">
	<div class="nv-html-content">
	<div class="site-logo" style="margin-top: -30px">
    <a class="brand" href="integracion-servicios-mexicanos/index.html" title="Servicios Nuevos de Filtración" aria-label="Servicios Nuevos de Filtración"><br />
        <img src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2021/01/ISMEX.png" alt="Servicios Nuevos de Filtración" /> </a>&nbsp;&nbsp;<br>
    <label class="btn" style="background-color: #0232d0;color: white; border-radius: 9px;  width: 146px; font-size: 10px; position: relative; top: 14px; font-family: 'Poppins' !important;" for="modal-1">Acceso clientes</label>

</div>
</div>
	</div>

</div>							</div>
		</div>
	</div>
</nav>


<nav class="header--row header-main hide-on-desktop layout-full-contained nv-navbar header--row"
	data-row-id="main" data-show-on="mobile">

	<div
		class="header--row-inner header-main-inner light-mode">
		<div class="container">
			<div
				class="row row--wrapper"
				data-section="hfg_header_layout_main" >
				<div class="builder-item hfg-item-first col-8 col-md-8 col-sm-8 hfg-item-left"><div class="item--inner builder-item--logo"
		data-section="title_tagline"
		data-item-id="logo">
	<div class="site-logo">
	<a class="brand" href="index.html" title="Servicios Nuevos de Filtración"
			aria-label="Servicios Nuevos de Filtración">
		<img src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2022/02/SNFPRO.jpg" alt="">	</a>
</div>

	</div>

</div><div class="builder-item hfg-item-last col-4 col-md-4 col-sm-4 hfg-item-right"><div class="item--inner builder-item--nav-icon"
		data-section="header_menu_icon"
		data-item-id="nav-icon">
	<div class="menu-mobile-toggle item-button navbar-toggle-wrapper">
	<button class="navbar-toggle"
					aria-label="
			Menú de navegación			">
				<div class="bars">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</div>
		<span class="screen-reader-text">Cambiar modo de navegación</span>
	</button>
</div> <!--.navbar-toggle-wrapper-->


	</div>

</div>							</div>
		</div>
	</div>
</nav>

<div id="header-menu-sidebar" class="header-menu-sidebar menu-sidebar-panel light-mode slide_left">
	<div id="header-menu-sidebar-bg" class="header-menu-sidebar-bg">
		<div class="close-sidebar-panel navbar-toggle-wrapper">
			<button class="navbar-toggle active" 					aria-label="
				Menú de navegación				">
				<div class="bars">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</div>
				<span class="screen-reader-text">
				Cambiar modo de navegación					</span>
			</button>
		</div>
		<div id="header-menu-sidebar-inner" class="header-menu-sidebar-inner">
			<div class="builder-item hfg-item-last hfg-item-first col-12 col-md-12 col-sm-12 hfg-item-right"><div class="item--inner builder-item--primary-menu has_menu"
		data-section="header_menu_primary"
		data-item-id="primary-menu">
	<div class="nv-nav-wrap">
	<div role="navigation" class="style-plain nav-menu-primary"
			aria-label="Menú principal">
        
		<ul id="nv-primary-navigation-sidebar" class="primary-menu-ul"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-22" style="text-align: left;"><label class="btn navbar-toggle" style="background-color: #0232d0;color: white; border-radius: 9px;  width: 146px; font-size: 10px; font-family: 'Poppins' !important;" for="modal-1" onclick="ocultar()">Acceso clientes</label></li><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-22"><a href="index.html" aria-current="page">Inicioss</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113"><a href="conocenos/index.html">Conócenos</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-622"><a href="#"><span class="menu-item-title-wrap">Portafolio</span><div class="caret-wrap 3" tabindex="0"><span class="caret"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"/></svg></span></div></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-506"><a href="productos/index.html">Productos</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1220"><a href="servicios/index.html">Servicios</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115"><a href="casos-de-exito/index.html">Clientes</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1568"><a href="#"><span class="menu-item-title-wrap">Proveedores</span><div class="caret-wrap 7" tabindex="0"><span class="caret"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"/></svg></span></div></a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1569"><a href="sostenibilidad/index.html">Sostenibilidad</a></li>
	<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1570"><a href="carta-compromiso/index.html">Carta Compromiso</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-166"><a href="contacto/index.html">Contacto</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1485"><a href="integracion-servicios-mexicanos/index.html">Ismex</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-510"><a href="bolsa-de-trabajo/index.html">Bolsa de Trabajo</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1267"><a href="noticias/index.html">Noticias</a></li>
<li class="pll-parent-menu-item menu-item menu-item-type-custom menu-item-object-custom current-menu-parent menu-item-has-children menu-item-1073"><a href="#pll_switcher"><span class="menu-item-title-wrap"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAflBMVEUAXgnp6ejj49zOAADGAACBvZl+u5Z2t4/9/f36+vrnZ2tmrYNdqXv29vbjR03fQkjjWFy4AABVpHba0LzfOD5PoXHg1sXHsIyDXjzSxq8AUQCrtaONfWn08fHbLTPgTVBInWudrZk7lmHYIymvAAAAQwAANwAAJQDU1NTKxMQZpDQjAAAAaklEQVR4ATXItUEEUAAFsLxvuDc4+2+Fu0ODnFSXMgGBRIguWQS/0YdMZqJFkiIyWFttkk4jqs1kfHcohzG059YetlndUhh1zYF+ZnUVhbpW3A4X6O97be1jDevP1nRoPGUAz9ByFAJZgjmA3Q34KhcIQAAAAABJRU5ErkJggg==" alt="Español" width="16" height="11" style="width: 16px; height: 11px;" /></span><div class="caret-wrap 14" tabindex="0"><span class="caret"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"/></svg></span></div></a>
<ul class="sub-menu">
	<li class="lang-item lang-item-12 lang-item-es current-lang lang-item-first menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-1073-es"><a href="index.html" hreflang="es-MX" lang="es-MX"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAflBMVEUAXgnp6ejj49zOAADGAACBvZl+u5Z2t4/9/f36+vrnZ2tmrYNdqXv29vbjR03fQkjjWFy4AABVpHba0LzfOD5PoXHg1sXHsIyDXjzSxq8AUQCrtaONfWn08fHbLTPgTVBInWudrZk7lmHYIymvAAAAQwAANwAAJQDU1NTKxMQZpDQjAAAAaklEQVR4ATXItUEEUAAFsLxvuDc4+2+Fu0ODnFSXMgGBRIguWQS/0YdMZqJFkiIyWFttkk4jqs1kfHcohzG059YetlndUhh1zYF+ZnUVhbpW3A4X6O97be1jDevP1nRoPGUAz9ByFAJZgjmA3Q34KhcIQAAAAABJRU5ErkJggg==" alt="Español" width="16" height="11" style="width: 16px; height: 11px;" /></a></li>
	<li class="lang-item lang-item-15 lang-item-en menu-item menu-item-type-custom menu-item-object-custom menu-item-1073-en"><a href="en/home/index.html" hreflang="en-US" lang="en-US"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAmVBMVEViZsViZMJiYrf9gnL8eWrlYkjgYkjZYkj8/PujwPybvPz4+PetraBEgfo+fvo3efkydfkqcvj8Y2T8UlL8Q0P8MzP9k4Hz8/Lu7u4DdPj9/VrKysI9fPoDc/EAZ7z7IiLHYkjp6ekCcOTk5OIASbfY/v21takAJrT5Dg6sYkjc3Nn94t2RkYD+y8KeYkjs/v7l5fz0dF22YkjWvcOLAAAAgElEQVR4AR2KNULFQBgGZ5J13KGGKvc/Cw1uPe62eb9+Jr1EUBFHSgxxjP2Eca6AfUSfVlUfBvm1Ui1bqafctqMndNkXpb01h5TLx4b6TIXgwOCHfjv+/Pz+5vPRw7txGWT2h6yO0/GaYltIp5PT1dEpLNPL/SdWjYjAAZtvRPgHJX4Xio+DSrkAAAAASUVORK5CYII=" alt="English" width="16" height="11" style="width: 16px; height: 11px;" /></a></li>
</ul>
</li>
</ul>	</div>
</div>

	</div>

</div>		</div>
	</div>
</div>
<div class="header-menu-sidebar-overlay"></div>

</div>
	</header>
	
	<main id="content" class="neve-main" role="main">

		<div data-elementor-type="wp-page" data-elementor-id="38" class="elementor elementor-38" data-elementor-settings="[]">
						<div class="elementor-inner">
							<div class="elementor-section-wrap">
							<section class="elementor-section elementor-top-section elementor-element elementor-element-1336e649 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default" data-id="1336e649" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:609,&quot;url&quot;:&quot;https:\/\/www.grupoisn.com.mx\/wp-content\/uploads\/2020\/03\/Service-and-maintenance-3.jpg&quot;},{&quot;id&quot;:611,&quot;url&quot;:&quot;https:\/\/www.grupoisn.com.mx\/wp-content\/uploads\/2020\/03\/GFS_BoothMaint.jpg&quot;},{&quot;id&quot;:338,&quot;url&quot;:&quot;https:\/\/www.grupoisn.com.mx\/wp-content\/uploads\/2020\/02\/Page-1-Image-5.png&quot;},{&quot;id&quot;:587,&quot;url&quot;:&quot;https:\/\/www.grupoisn.com.mx\/wp-content\/uploads\/2020\/03\/main-products_1-scaled.jpg&quot;},{&quot;id&quot;:610,&quot;url&quot;:&quot;https:\/\/www.grupoisn.com.mx\/wp-content\/uploads\/2020\/03\/Service-Filters6x4.jpg&quot;},{&quot;id&quot;:345,&quot;url&quot;:&quot;https:\/\/www.grupoisn.com.mx\/wp-content\/uploads\/2020\/02\/Page-11-Image-183.png&quot;}],&quot;background_slideshow_ken_burns&quot;:&quot;yes&quot;,&quot;background_slideshow_ken_burns_zoom_direction&quot;:&quot;out&quot;,&quot;background_slideshow_slide_duration&quot;:3500,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;,&quot;background_slideshow_transition_duration&quot;:500}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-34640e81 intro-bg" data-id="34640e81" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;slideshow&quot;,&quot;background_slideshow_gallery&quot;:[],&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_duration&quot;:5000,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;,&quot;background_slideshow_transition_duration&quot;:500}">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-60970747 intro-text elementor-invisible elementor-widget elementor-widget-heading" data-id="60970747" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-default">LIDERES EN RECURSOS GLOBALES Y SOLUCIONES LOCALES</h1>		</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-17786dfd elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="17786dfd" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-no">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-549de1ca" data-id="549de1ca" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-2caed2c0 elementor-widget elementor-widget-heading" data-id="2caed2c0" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h4 class="elementor-heading-title elementor-size-default">PROVEEDOR DE SERVICIOS ESPECIALIZADOS,<br> AVALADOS POR LA SECRETARIA DEL TRABAJO. </h4>		</div>
				</div>
						</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-7d756d4a" data-id="7d756d4a" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-3e70a12 elementor-widget elementor-widget-image" data-id="3e70a12" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
								<div class="elementor-image">
													<a href="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2021/08/FORMATO-17-Aviso-de-Registro.pdf" target="_blank">
							<img width="348" height="145" src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2021/08/REPSE.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://www.grupoisn.com.mx/wp-content/uploads/2021/08/REPSE.png 348w, https://www.grupoisn.com.mx/wp-content/uploads/2021/08/REPSE-300x125.png 300w" sizes="(max-width: 348px) 100vw, 348px" />								</a>
														</div>
						</div>
				</div>
				<div class="elementor-element elementor-element-aab06d1 elementor-align-justify elementor-widget elementor-widget-button" data-id="aab06d1" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2021/08/FORMATO-17-Aviso-de-Registro.pdf" class="elementor-button-link elementor-button elementor-size-md" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-right">
				<i aria-hidden="true" class="fas fa-download"></i>			</span>
						<span class="elementor-button-text">Descarga aquí</span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-39206072 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible" data-id="39206072" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;pulse&quot;}">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3abf3117" data-id="3abf3117" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<section class="elementor-section elementor-inner-section elementor-element elementor-element-6efcfea9 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="6efcfea9" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4ac965b3" data-id="4ac965b3" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-257ca053 elementor-widget elementor-widget-heading" data-id="257ca053" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h5 class="elementor-heading-title elementor-size-default">BIENVENIDOS A </h5>		</div>
				</div>
				<div class="elementor-element elementor-element-5fc2c660 elementor-widget elementor-widget-heading" data-id="5fc2c660" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-small">Servicios Nuevos De Filtración Hispanomexicános​</p>		</div>
				</div>
				<div class="elementor-element elementor-element-60dd63 elementor-widget-divider--view-line elementor-widget elementor-widget-divider" data-id="60dd63" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-25857365 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="25857365" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-75680209" data-id="75680209" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-1346036d elementor-widget elementor-widget-image" data-id="1346036d" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
								<div class="elementor-image">
												<img width="800" height="505" src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2020/02/paint.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://www.grupoisn.com.mx/wp-content/uploads/2020/02/paint.jpg 800w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/paint-300x189.jpg 300w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/paint-768x485.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" />														</div>
						</div>
				</div>
						</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4e40761c" data-id="4e40761c" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-41235290 elementor-widget elementor-widget-text-editor" data-id="41235290" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
								<div class="elementor-text-editor elementor-clearfix">
				<p>SNF es una empresa líder enfocada en la solución de problemas en cabinas de pintura.</p>
<ul>
<li>Brindando suministros en filtración</li>
<li>Servicio de aire limpio</li>
<li>Servicio de balanceo</li>
<li>Análisis de defectos en las diferentes capas de pintura</li>
</ul>					</div>
						</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-3efc3abc elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3efc3abc" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3583d1b7" data-id="3583d1b7" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-0cb908c elementor-widget elementor-widget-text-editor" data-id="0cb908c" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
								<div class="elementor-text-editor elementor-clearfix">
				<blockquote><p style="text-align: center;">&#8220;Comprometida en atender sus necesidades y brindar soluciones.&#8221;</p></blockquote>					</div>
						</div>
				</div>
				<div class="elementor-element elementor-element-733f31a7 elementor-widget elementor-widget-text-editor" data-id="733f31a7" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
								<div class="elementor-text-editor elementor-clearfix">
				<p>Contamos con 10 años de experiencia dentro de la industria automotriz dando soporte a procesos productivos., amplio conocimiento de sistemas de filtración y conocimiento para “la mejor aplicación para cada situación”.</p><h3>¿Por qué elegirnos?</h3><p>SNF la mejor opción para ti, ya que nuestros sistemas de trabajo se encuentran certificados en calidad ISO 9001-2015; capacidad comprobada para cualquier tipo de trabajo, experiencia de grupo, fuerza corporativa y entusiasmo para el logro de los objetivos planeados.</p>					</div>
						</div>
				</div>
				<div class="elementor-element elementor-element-2007faa1 elementor-align-center elementor-widget elementor-widget-button" data-id="2007faa1" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="conocenos/index.html" class="elementor-button-link elementor-button elementor-size-md elementor-animation-float" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Mas acerca de nosotros</span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-3b768b98 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="3b768b98" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-26eb110d" data-id="26eb110d" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<section class="elementor-section elementor-inner-section elementor-element elementor-element-616a45f7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="616a45f7" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-27e4739b" data-id="27e4739b" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-63f413c9 elementor-widget elementor-widget-heading" data-id="63f413c9" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">MENSAJE DE NUESTRO DIRECTOR</h2>		</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-4d9c1222 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="4d9c1222" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2c7823c7" data-id="2c7823c7" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-177f3835 elementor-invisible elementor-widget elementor-widget-text-editor" data-id="177f3835" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
								<div class="elementor-text-editor elementor-clearfix">
				<div><p class="x_MsoNormal"><span lang="ES-MX">Hace veinte años, inició un sueño de un Joven que buscaba aportar algo de la inquietud que guardaba para generar valor dentro del área de ingeniería en la cual baso su formación. En esta búsqueda, iniciamos en una empresa automotriz de clase mundial dentro de plantas de pintura, alcanzando en seis años la jefatura del departamento de pintura, teniendo -Cómo sucede aún hoy día- muchas necesidades para alcanzar los objetivos de calidad y entrega. Producto de esta necesidad y de la inquietud por trascender, se tomó la decisión de explorar con personas o empresas que tuvieran esa misma pasión e inquietud por facilitar la vida de los responsables de las plantas de pintura automotrices . Es así, como hace diez años, nace SERVICIOS NUEVOS DE FILTRACIÓN HISPANOMEXICANOS, como parte de grupoISN en donde encuentro que, su presidente Fermín Elízalde, con una estructura y experiencia internacional probada y una completa afinidad con los ideales de servicio a los que aspiraba, seria el mejor soporte de recursos técnicos y capacidad necesaria, para iniciar el lanzamiento de esta nueva empresa de clase mundial en México.</span></p></div><div> </div>					</div>
						</div>
				</div>
						</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-3c15b28" data-id="3c15b28" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-377de88 elementor-invisible elementor-widget elementor-widget-text-editor" data-id="377de88" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
								<div class="elementor-text-editor elementor-clearfix">
				<p>Hoy, a diez años de haber iniciado esta aventura, no tengo más que agradecimiento a Dios, mi familia, socios de negocios, pero sobre todo a esta gran familia que SNF a formado en todo México, agradecerles su PASIÓN y entrega en cada uno de los retos que hemos alcanzado, y decirles, que estos son solamente los primeros diez años de éxito y que seguiremos INNOVANDO, con la ÉTICA Y HONESTIDAD que hasta hoy nos ha caracterizado, para continuar con CONFIANZA el proceso de crecimiento, como la mejor opción en las soluciones a los retos de cada día, para nuestros socios, clientes, proveedores y equipo de trabajo.</p>					</div>
						</div>
				</div>
				<div class="elementor-element elementor-element-6a2ab27 elementor-widget elementor-widget-text-editor" data-id="6a2ab27" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
								<div class="elementor-text-editor elementor-clearfix">
				<blockquote><h5 style="text-align: right;"><em>Mtro. José Felipe Camacho Rogel</em></h5></blockquote>					</div>
						</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-aeeb5ac elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible" data-id="aeeb5ac" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;zoomIn&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2030ab4" data-id="2030ab4" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-9d4c702 elementor-widget elementor-widget-heading" data-id="9d4c702" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Presencia Internacional</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c4cc511 elementor-widget elementor-widget-image" data-id="c4cc511" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
								<div class="elementor-image">
												<img width="742" height="445" src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2020/02/Imagen1.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://www.grupoisn.com.mx/wp-content/uploads/2020/02/Imagen1.png 742w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/Imagen1-300x180.png 300w" sizes="(max-width: 742px) 100vw, 742px" />														</div>
						</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
						</div>
						</div>
					</div>
		</main><!--/.neve-main-->


		<footer itemtype="https://schema.org/WPFooter" itemscope="itemscope" id="colophon" role="contentinfo">
			<div class='footer-width-fixer'>		<div data-elementor-type="wp-post" data-elementor-id="455" class="elementor elementor-455" data-elementor-settings="[]">
						<div class="elementor-inner">
							<div class="elementor-section-wrap">
							<section class="elementor-section elementor-top-section elementor-element elementor-element-5e07429 envato-kit-141-top-0 elementor-reverse-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="5e07429" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-wider">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-675eaf18 elementor-invisible" data-id="675eaf18" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:&quot;100&quot;}">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-aece2d8 elementor-widget elementor-widget-heading" data-id="aece2d8" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">DIRECCIÓN</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-1630ba81 elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="1630ba81" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-map-marker-alt"></i>						</span>
										<span class="elementor-icon-list-text">Laterial de la autopista Mexico-Publa #7534<br>Rancho Moratilla<br>Int 1 CP. 72104<br>Puebla Pue.</span>
									</li>
								<li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-phone"></i>						</span>
										<span class="elementor-icon-list-text">+52 1 2222248068</span>
									</li>
								<li class="elementor-icon-list-item">
											<a href="https://api.whatsapp.com/send?phone=522226726116">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fab fa-whatsapp"></i>						</span>
										<span class="elementor-icon-list-text">+52 1 2226726116</span>
											</a>
									</li>
								<li class="elementor-icon-list-item">
											<a href="mailto:ventasnf@grupoisn.com.mx">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="far fa-envelope-open"></i>						</span>
										<span class="elementor-icon-list-text">ventasnf@grupoisn.com.mx</span>
											</a>
									</li>
						</ul>
				</div>
				</div>
						</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-3ca501d9 elementor-invisible" data-id="3ca501d9" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:&quot;200&quot;}">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-23a444e2 elementor-widget elementor-widget-heading" data-id="23a444e2" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">búscanos en redes sociales</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-1ab5852a elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="1ab5852a" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<a href="https://www.facebook.com/snf.grupoisn" target="_blank">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fab fa-facebook-f"></i>						</span>
										<span class="elementor-icon-list-text">Facebook</span>
											</a>
									</li>
								<li class="elementor-icon-list-item">
											<a href="https://twitter.com/SNFiltracion" target="_blank">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fab fa-twitter"></i>						</span>
										<span class="elementor-icon-list-text">Twitter</span>
											</a>
									</li>
								<li class="elementor-icon-list-item">
											<a href="https://www.linkedin.com/company/servicios-nuevos-de-filtracion-hispanomexicanos" target="_blank">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fab fa-linkedin-in"></i>						</span>
										<span class="elementor-icon-list-text">LinkedIn</span>
											</a>
									</li>
						</ul>
				</div>
				</div>
				<div class="elementor-element elementor-element-71f776c7 elementor-widget elementor-widget-heading" data-id="71f776c7" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">ENLACES</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-4c0f8aba elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="4c0f8aba" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item">
											<a href="http://www.grupoisn.com/">

											<span class="elementor-icon-list-text">Grupo ISN Global</span>
											</a>
									</li>
								<li class="elementor-icon-list-item">
											<a href="contacto/index.html">

											<span class="elementor-icon-list-text">Contacto</span>
											</a>
									</li>
								<li class="elementor-icon-list-item">
											<a href="https://rvazquez7.wixsite.com/grupoisnmexico">

											<span class="elementor-icon-list-text">ISMEX</span>
											</a>
									</li>
						</ul>
				</div>
				</div>
						</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-3b34343d elementor-invisible" data-id="3b34343d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:&quot;300&quot;}">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-ff4db95 elementor-widget elementor-widget-heading" data-id="ff4db95" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">certificaciones</h2>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-6437995 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="6437995" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-38e99f0" data-id="38e99f0" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-bbd7dbc elementor-widget elementor-widget-image" data-id="bbd7dbc" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
								<div class="elementor-image">
												<img width="300" height="188" src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2020/02/iaff-300x188.png" class="attachment-medium size-medium" alt="" loading="lazy" srcset="https://www.grupoisn.com.mx/wp-content/uploads/2020/02/iaff-300x188.png 300w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/iaff.png 303w" sizes="(max-width: 300px) 100vw, 300px" />														</div>
						</div>
				</div>
						</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-27c7ab4" data-id="27c7ab4" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-4f7b77a elementor-widget elementor-widget-image" data-id="4f7b77a" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
								<div class="elementor-image">
												<img width="300" height="164" src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2020/02/enac-300x164.jpg" class="attachment-medium size-medium" alt="" loading="lazy" srcset="https://www.grupoisn.com.mx/wp-content/uploads/2020/02/enac-300x164.jpg 300w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/enac.jpg 630w" sizes="(max-width: 300px) 100vw, 300px" />														</div>
						</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-9d0cc9e elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9d0cc9e" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f31c724" data-id="f31c724" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-8f7ea1c elementor-widget elementor-widget-image" data-id="8f7ea1c" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
								<div class="elementor-image">
												<img width="219" height="310" src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-20-at-13.12.01.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://www.grupoisn.com.mx/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-20-at-13.12.01.jpeg 219w, https://www.grupoisn.com.mx/wp-content/uploads/2020/03/WhatsApp-Image-2020-03-20-at-13.12.01-212x300.jpeg 212w" sizes="(max-width: 219px) 100vw, 219px" />														</div>
						</div>
				</div>
						</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-db41954" data-id="db41954" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-2b0d50f elementor-widget elementor-widget-image" data-id="2b0d50f" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
								<div class="elementor-image">
												<img width="512" height="512" src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2020/02/WhatsApp-Image-2020-03-10-at-11.31.33.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://www.grupoisn.com.mx/wp-content/uploads/2020/02/WhatsApp-Image-2020-03-10-at-11.31.33.jpeg 512w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/WhatsApp-Image-2020-03-10-at-11.31.33-300x300.jpeg 300w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/WhatsApp-Image-2020-03-10-at-11.31.33-150x150.jpeg 150w" sizes="(max-width: 512px) 100vw, 512px" />														</div>
						</div>
				</div>
				<div class="elementor-element elementor-element-e5a9062 elementor-widget elementor-widget-image" data-id="e5a9062" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
								<div class="elementor-image">
												<img width="300" height="169" src="<?php echo base_url(); ?>pagina_css/wp-content/uploads/2020/02/emma-300x169.png" class="attachment-medium size-medium" alt="" loading="lazy" srcset="https://www.grupoisn.com.mx/wp-content/uploads/2020/02/emma-300x169.png 300w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/emma-768x432.png 768w, https://www.grupoisn.com.mx/wp-content/uploads/2020/02/emma.png 800w" sizes="(max-width: 300px) 100vw, 300px" />														</div>
						</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
						</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-7647d23c elementor-invisible" data-id="7647d23c" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;pulse&quot;,&quot;animation_delay&quot;:&quot;400&quot;}">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
						<div class="elementor-element elementor-element-4131ed72 elementor-widget elementor-widget-heading" data-id="4131ed72" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default"><h3>Servicios Nuevos de Filtración Hispanomexicános<h3></h3>		</div>
				</div>
				<div class="elementor-element elementor-element-2302f95 elementor-invisible elementor-widget elementor-widget-text-editor" data-id="2302f95" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:&quot;250&quot;}" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
								<div class="elementor-text-editor elementor-clearfix">
				<p>Lideres en recursos globales y soluciones locales.</p>					</div>
						</div>
				</div>
						</div>
					</div>
		</div>
								</div>
					</div>
		</section>
						</div>
						</div>
					</div>
		</div>		</footer>
	
</div>
<style type="text/css">
	.input-group {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-align: stretch;
    -ms-flex-align: stretch;
    align-items: stretch;
    width: 100%;
}

.input-group:not(.has-validation) > :not(:last-child):not(.dropdown-toggle):not(.dropdown-menu), .input-group:not(.has-validation) > .dropdown-toggle:nth-last-child(n + 3) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}

.login-form .form-group .input-group-text {
    background-color: rgba(36, 105, 92, 0.1);
    border: none;
    color: #0232cf !important;
}


.input-group-text {
    border-color: #e6edef;
    font-weight: 500;
}

.input-group-text {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    padding: 0.375rem 0.75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: center;
    white-space: nowrap;
    background-color: #e9f0ee;
    border: 1px solid #ced4da;
    border-radius: 0.25rem;
}

.input-group > :not(:first-child):not(.dropdown-menu):not(.valid-tooltip):not(.valid-feedback):not(.invalid-tooltip):not(.invalid-feedback) {
    margin-left: -1px;
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
}

.theme-form .form-group input[type=text], .theme-form .form-group input[type=email], .theme-form .form-group input[type=search], .theme-form .form-group input[type=password], .theme-form .form-group input[type=number], .theme-form .form-group input[type=tel], .theme-form .form-group input[type=date], .theme-form .form-group input[type=datetime-local], .theme-form .form-group input[type=time], .theme-form .form-group input[type=datetime-local], .theme-form .form-group input[type=month], .theme-form .form-group input[type=week], .theme-form .form-group input[type=url], .theme-form .form-group input[type=file], .theme-form .form-group select {
    border-color: #e6edef;
    font-size: 14px;
    color: #898989;
}

.input-group > .form-control, .input-group > .form-select {
    position: relative;
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    width: 1%;
    min-width: 0;
}

</style>
<input class="modal-state" id="modal-1" type="checkbox" />
<div class="modal">
    <label class="modal__bg" for="modal-1"></label>
    <div class="modal__inner" style="background-color: #e9f0ee;">
        <label class="modal__close" for="modal-1"></label>
        <div class="">
        	<div style="margin-bottom: 75px;"></div>
            <div align="center" style="margin-bottom: 15px;">
                <img style="width: 150px;" src="<?php echo base_url();?>public/img/logofinalsys.png">
            </div>
            <div style="background: white; padding: 28px;">
              <form id="login-form" action="#" role="form">
              <h4 style="color: black; text-transform: capitalize;
				    font-weight: 600;
				    margin-bottom: 5px;
				    font-size: 22px; font-family: 'Montserrat', sans-serif;">Inicio De Sesión</h4>
              <h6 style="color: black; font-size: 14px;
				    margin-bottom: 25px;
				    color: #999; font-family: 'Montserrat', sans-serif;">Bienvenido de nuevo. Por favor inicia sesión</h6>
              <div class="form-group">
                <label style="color: black; margin-bottom: 5px;font-weight: 600;display: inline-block; font-weight: 600;
                    text-transform: capitalize; margin-bottom: 5px; font-size: 14px; font-family: 'Montserrat', sans-serif;">Correo electrónico</label>
                <div class="input-group"><span class="input-group-text"><i class="far fa-envelope" style="color: #0232d0;"></i></span>
                  <input class="form-control" type="text" name="txtUsuario" id="txtUsuario" required="" autofocus="" aria-required="true">
                </div>
              </div>
              <div class="form-group">
                <label style="color: black; margin-bottom: 5px;font-weight: 600;display: inline-block; font-weight: 600;
                    text-transform: capitalize; margin-bottom: 5px; font-size: 14px; font-family: 'Montserrat', sans-serif;">Contraseña</label>
                <div class="input-group"><span class="input-group-text"><i class="fas fa-lock" style="color: #0232d0;"></i></span>
                      <input class="form-control" type="password" name="" id="txtPass" required="" aria-required="true" aria-describedby="txtPass-error" aria-invalid="false"><div id="txtPass-error" class="vd_red help-inline hidden valid"></div>
                    </div>
              </div>
              <br>
              <div class="form-group" style="text-align: right;">
                <button class="btn" style="text-transform: uppercase;
				    font-weight: 700;
				    display: block;    background-color: #0232cf !important;
				    border-color: #0231d0 !important;
				    color: white !important;    font-size: 14px;
				    padding: 0.375rem 1.75rem; width: 100%; border-radius: 6px;" type="submit" id="login-submit">Iniciar</button>
              </div>
             </form>
             <br>
             <div style="background-color: #0231d1; border-radius: 13px; font-size: 15px !important; text-align: center; display: none;" id="success">
                <b>Acceso Correcto!</b> Será redirigido al sistema 
              </div>

              <div style="background-color: red; border-radius: 13px; font-size: 15px !important; text-align: center; display: none;" id="error" align="center">
                <b>Error!</b> El nombre de usuario y/o contraseña son incorrectos 
              </div>
            </div>
        </div>  
    </div>
</div>

<!--/.wrapper-->
<!-- Introduce aquí tus scripts --><link rel='stylesheet' id='e-animations-css'  href='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/animations/animations.min1aae.css?ver=3.5.3' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/cliengo/public/js/script_install_cliengo4c56.js?ver=2.0.2' id='script-install-cliengo-js'></script>
<script type='text/javascript' id='pt-cv-content-views-script-js-extra'>
/* <![CDATA[ */
var PT_CV_PUBLIC = {"_prefix":"pt-cv-","page_to_show":"5","_nonce":"4000aadcbb","is_admin":"","is_mobile":"","ajaxurl":"https:\/\/www.grupoisn.com.mx\/wp-admin\/admin-ajax.php","lang":"es","loading_image_src":"data:image\/gif;base64,R0lGODlhDwAPALMPAMrKygwMDJOTkz09PZWVla+vr3p6euTk5M7OzuXl5TMzMwAAAJmZmWZmZszMzP\/\/\/yH\/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgAPACwAAAAADwAPAAAEQvDJaZaZOIcV8iQK8VRX4iTYoAwZ4iCYoAjZ4RxejhVNoT+mRGP4cyF4Pp0N98sBGIBMEMOotl6YZ3S61Bmbkm4mAgAh+QQFCgAPACwAAAAADQANAAAENPDJSRSZeA418itN8QiK8BiLITVsFiyBBIoYqnoewAD4xPw9iY4XLGYSjkQR4UAUD45DLwIAIfkEBQoADwAsAAAAAA8ACQAABC\/wyVlamTi3nSdgwFNdhEJgTJoNyoB9ISYoQmdjiZPcj7EYCAeCF1gEDo4Dz2eIAAAh+QQFCgAPACwCAAAADQANAAAEM\/DJBxiYeLKdX3IJZT1FU0iIg2RNKx3OkZVnZ98ToRD4MyiDnkAh6BkNC0MvsAj0kMpHBAAh+QQFCgAPACwGAAAACQAPAAAEMDC59KpFDll73HkAA2wVY5KgiK5b0RRoI6MuzG6EQqCDMlSGheEhUAgqgUUAFRySIgAh+QQFCgAPACwCAAIADQANAAAEM\/DJKZNLND\/kkKaHc3xk+QAMYDKsiaqmZCxGVjSFFCxB1vwy2oOgIDxuucxAMTAJFAJNBAAh+QQFCgAPACwAAAYADwAJAAAEMNAs86q1yaWwwv2Ig0jUZx3OYa4XoRAfwADXoAwfo1+CIjyFRuEho60aSNYlOPxEAAAh+QQFCgAPACwAAAIADQANAAAENPA9s4y8+IUVcqaWJ4qEQozSoAzoIyhCK2NFU2SJk0hNnyEOhKR2AzAAj4Pj4GE4W0bkJQIAOw=="};
var PT_CV_PAGINATION = {"first":"\u00ab","prev":"\u2039","next":"\u203a","last":"\u00bb","goto_first":"Go to first page","goto_prev":"Go to previous page","goto_next":"Go to next page","goto_last":"Go to last page","current_page":"Current page is","goto_page":"Go to page"};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/content-views-query-and-display-post-page/public/assets/js/cv749c.js?ver=2.4.0.2' id='pt-cv-content-views-script-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/download-after-email/js/media-queryf88a.js?ver=1638339176' id='dae-media-query-js'></script>
<script type='text/javascript' id='dae-download-js-extra'>
/* <![CDATA[ */
var objDaeDownload = {"ajaxUrl":"https:\/\/www.grupoisn.com.mx\/wp-admin\/admin-ajax.php","nonce":"699d151f1e"};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/download-after-email/js/downloadf88a.js?ver=1638339176' id='dae-download-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/mystickymenu/js/detectmobilebrowser2a47.js?ver=2.5.6' id='detectmobilebrowser-js'></script>
<script type='text/javascript' id='mystickymenu-js-extra'>
/* <![CDATA[ */
var option = {"mystickyClass":"header.header","activationHeight":"0","disableWidth":"0","disableLargeWidth":"0","adminBar":"false","device_desktop":"1","device_mobile":"1","mystickyTransition":"slide","mysticky_disable_down":"false"};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/mystickymenu/js/mystickymenu.min2a47.js?ver=2.5.6' id='mystickymenu-js'></script>
<script type='text/javascript' id='neve-script-js-extra'>
/* <![CDATA[ */
var NeveProperties = {"ajaxurl":"https:\/\/www.grupoisn.com.mx\/wp-admin\/admin-ajax.php","nonce":"6bec034765"};
/* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/themes/neve/assets/js/build/modern/frontendc141.js?ver=2.6.1' id='neve-script-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-includes/js/comment-reply.min6281.js?ver=5.8.6' id='comment-reply-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-includes/js/wp-embed.min6281.js?ver=5.8.6' id='wp-embed-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/js/webpack.runtime.min1aae.js?ver=3.5.3' id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/js/frontend-modules.min1aae.js?ver=3.5.3' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min05da.js?ver=4.0.2' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-includes/js/jquery/ui/core.min35d0.js?ver=1.12.1' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/swiper/swiper.min48f5.js?ver=5.3.6' id='swiper-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/share-link/share-link.min1aae.js?ver=3.5.3' id='share-link-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/lib/dialog/dialog.mind227.js?ver=4.9.0' id='elementor-dialog-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Compartir en Facebook","shareOnTwitter":"Compartir en Twitter","pinIt":"Fijarlo","download":"Descargar","downloadImage":"Descargar imagen","fullscreen":"Pantalla completa","zoom":"Zoom","share":"Compartir","playVideo":"Reproducir video","previous":"Previo","next":"Siguiente","close":"Cerrar"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"M\u00f3vil","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"M\u00f3vil grande","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tableta","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tableta grande","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Pantalla grande","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.5.3","is_static":false,"experimentalFeatures":{"e_import_export":true,"e_hidden_wordpress_widgets":true,"landing-pages":true,"elements-color-picker":true,"favorite-widgets":true,"admin-top-bar":true},"urls":{"assets":"https:\/\/www.grupoisn.com.mx\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":38,"title":"Servicios%20Nuevos%20de%20Filtraci%C3%B3n%20%E2%80%93%20Servicios%20Nuevos%20de%20Filtraci%C3%B3n%20Hispano%20mexicanos%20Puebla","excerpt":"","featuredImage":false}};
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/js/frontend.min1aae.js?ver=3.5.3' id='elementor-frontend-js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>pagina_css/wp-content/plugins/elementor/assets/js/preloaded-modules.min1aae.js?ver=3.5.3' id='preloaded-modules-js'></script>
<script type='text/javascript'>
(function() {
				var expirationDate = new Date();
				expirationDate.setTime( expirationDate.getTime() + 31536000 * 1000 );
				document.cookie = "pll_language=es; expires=" + expirationDate.toUTCString() + "; path=/; secure; SameSite=Lax";
			}());
	function ocultar(){
		$('#body_home').removeClass('home page-template page-template-page-templates page-template-template-pagebuilder-full-width page-template-page-templatestemplate-pagebuilder-full-width-php page page-id-38 custom-background wp-custom-logo ehf-footer ehf-template-neve ehf-stylesheet-neve nv-sidebar-right menu_sidebar_slide_left elementor-default elementor-kit-329 elementor-page elementor-page-38 e--ua-appleWebkit e--ua-safari e--ua-webkit is-menu-sidebar');
		$('#body_home').addClass('home page-template page-template-page-templates page-template-template-pagebuilder-full-width page-template-page-templatestemplate-pagebuilder-full-width-php page page-id-38 custom-background wp-custom-logo ehf-footer ehf-template-neve ehf-stylesheet-neve nv-sidebar-right menu_sidebar_slide_left elementor-default elementor-kit-329 elementor-page elementor-page-38 e--ua-appleWebkit e--ua-safari e--ua-webkit hiding-header-menu-sidebar');
		setTimeout(function(){ 
            $('#body_home').removeClass('home page-template page-template-page-templates page-template-template-pagebuilder-full-width page-template-page-templatestemplate-pagebuilder-full-width-php page page-id-38 custom-background wp-custom-logo ehf-footer ehf-template-neve ehf-stylesheet-neve nv-sidebar-right menu_sidebar_slide_left elementor-default elementor-kit-329 elementor-page elementor-page-38 e--ua-appleWebkit e--ua-safari e--ua-webkit hiding-header-menu-sidebar');
		    $('#body_home').addClass('home page-template page-template-page-templates page-template-template-pagebuilder-full-width page-template-page-templatestemplate-pagebuilder-full-width-php page page-id-38 custom-background wp-custom-logo ehf-footer ehf-template-neve ehf-stylesheet-neve nv-sidebar-right menu_sidebar_slide_left elementor-default elementor-kit-329 elementor-page elementor-page-38 e--ua-appleWebkit e--ua-safari e--ua-webkit');
        }, 500);
		
    } 
</script>
<div id='icon_wrapper'><a target="_blank" class='fuse_social_icons_links' data-nonce='3763f0c34f' data-title='facebook' href='https://www.facebook.com/snf.grupoisn'>	<i class='fa fa-facebook facebook-awesome-social awesome-social'></i></a><a target="_blank" class='fuse_social_icons_links' data-nonce='3763f0c34f' data-title='twitter' href='https://twitter.com/SNFiltracion'>	<i class='fa fa-twitter twitter-awesome-social awesome-social'></i></a><a target="_blank" class='fuse_social_icons_links' data-nonce='3763f0c34f' data-title='linkedin' href='https://www.linkedin.com/authwall?trk=gf&amp;trkInfo=AQGtBK-uTeESpwAAAX4FvFZA4zf5p-Dp98CnlUJz1zrJBpLQI9g58HdBxVBlQgI_kzYlwjNboircTS0gox1OFUt9x2LZyiIY_ZxCSdBSifBBvz-PkPmRGuVlQoEZWY3WfjQWD9I=&amp;originalReferer=https://www.grupoisn.com.mx/&amp;sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Fservicios-nuevos-de-filtracion-hispanomexicanos'>	<i class='fa fa-linkedin linkedin-awesome-social awesome-social'></i></a><a target="_blank" class='fuse_social_icons_links' data-nonce='3763f0c34f' data-title='envelope' href='mailto:ventasnf@grupoisn.com.mx'>	<i class='fa fa-envelope envelope-awesome-social awesome-social'></i></a></div>
</body>


<!-- Mirrored from www.grupoisn.com.mx/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Nov 2022 21:30:18 GMT -->
</html>
