<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sys ERP SNF PRO">
    <meta name="keywords" content="Sys ERP SNF PRO - BY Mangoo Software">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url();?>public/img/logofinalsys.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url();?>public/img/logofinalsys.png" type="image/x-icon">
    <title>SNF</title>
    <!-- Google font-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/calendar.css">
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/chartist.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/date-picker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/prism.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/vector-map.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <link id="color" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/alert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatables.css">
    <link href="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">

    <link href="<?php echo base_url();?>public/css/propios.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/fileinput/fileinput.min.css">

  </head>
  <style type="text/css">
    .vd_red{
      color: red;
    }
    .vd_green{
      color: #4caf50;
    }
    .head_tbl_prog{
      background-color: #0232cf !important;
      color:white !important;
    }
    .img-fluid{
      max-width: 33% !important;
    }
    @media only screen and (max-width: 600px) {
      .logo-wrapper{
        width: 90px;
      }
      .img-fluid{
        max-width: 88px !important;
        height: 63px !important;
      }
      .left-menu-header{
        display: none;
      }
      .page-main-header .right-menu{
        height: 51px;
      }
      .nav-menus{
        top: 0px !important;
        width: 100% !important;
        position: absolute !important;
        z-index: 1 !important;
        opacity: 1 !important;
        -webkit-transform: translateY(0px) scaleY(1) !important;
        transform: translateY(0px) scaleY(1) !important;
        visibility: visible !important;
        border-top: 1px solid #fff !important;
        -webkit-box-shadow: 0 2px 2px 2px rgb(255 255 255 / 8%) !important;
        box-shadow: 0 2px 2px 2px rgb(255 255 255 / 8%) !important;
      }
      .mobile-toggle{
        display: none !important;
      }
      .page-wrapper.compact-wrapper .page-body-wrapper header .sidebar-user img{
        float: unset;
      }
      .page-wrapper.compact-wrapper .page-body-wrapper header .sidebar-user p{
        max-width: 100%;
        text-align: center;
      }
      .page-wrapper.compact-wrapper .page-body-wrapper header .sidebar-user h6{
        padding-left: 0px;
        text-align: center;
      }
      .cw{
        color: white;
      }
    }
  </style>
  <input type="hidden" id="base_url" value="<?php echo base_url() ?>">
  <?php 
      $tipo=0;
      $foto_cli='';
    if (!isset($_SESSION['perfilid_tz'])) {
        $tipo=0;
        $foto_cli=$_SESSION['foto'];
    }else{
        $tipo=$_SESSION['tipo'];  
        $foto_cli=$_SESSION['foto'];
        $foto_cli=$_SESSION['foto'];
    }
  ?>
  <body>
    <div class="loader-wrapper">
      <div class="theme-loader">    
        <div class="loader-p"></div>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start       -->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
      <div class="page-main-header">
        <div class="main-header-right row m-0">
          <div class="main-header-left" >
            <div class="logo-wrapper">
              <a href="<?php echo base_url() ?>Inicio">
                <img class="img-fluid" style=" position: absolute; top: -1px; !important;" src="<?php echo base_url();?>public/img/logofinalsys.png" alt="">
              </a>
            </div>
            <div class="dark-logo-wrapper">
              <a href="<?php echo base_url() ?>Inicio">
                <img class="img-fluid" src="<?php echo base_url();?>public/img/logofinalsys.png" alt="">
              </a>
            </div>
            <?php if($tipo!=2){ ?>
              <div class="toggle-sidebar"><i class="status_toggle middle" data-feather="align-center" id="sidebar-toggle"></i></div>
            <?php } ?>      
          </div>
          <div class="left-menu-header col">
            <?php if($tipo==2){ 
              if($foto_cli!=''){ 
              ?>
              <ul>
                <li style="width: 20%;">
                  <img style="max-width: 5% !important; position: fixed; top: 5px !important;" src="<?php echo base_url();?>uploads/clientes/<?php echo $foto_cli ?>" alt="">
                </li>
                <li>
                  <span style="color: #0233d1;"><b>Bienvenido de nuevo</b></span><br><em style="font-weight: 500;"><?php echo $_SESSION['usuario_tz'] ?></em>
                </li>
              </ul>
            <?php } } ?>
          </div>
          <div class="nav-right col pull-right right-menu p-0">
            <ul class="nav-menus">

            <?php if($tipo == 2){ ?>
              <li>
                <a class="btn btn-primary" href="<?php echo base_url(); ?>index.php/Mis_clientes" style="width:auto"> Mis Usuarios</a>
              </li>
            <?php } ?>     

              <li class="onhover-dropdown">
                <div class="notification-box"><i data-feather="bell"></i><span class="dot-animated"></span></div>
                <ul class="notification-dropdown onhover-show-div">
                  <?php if($tipo==2){  
                    $this->load->model('ModeloGeneral');
                    $id_cli=$this->session->userdata("usuarioid_tz");
                    $get_ha=$this->ModeloGeneral->getHallazgoCliente(array("ma.id_cliente"=>$id_cli),2);
                    $cont=0; $html=""; $htmlh="";
                    foreach ($get_ha as $h) {
                      $cont++;
                      $htmlh='<li>
                          <p class="f-w-700 mb-0">Tienes '.$cont.' Notificaciones<span class="pull-right badge badge-primary badge-pill">'.$cont.'</span></p>
                        </li>';
                      $html.='
                        <li class="noti-primary">
                          <div class="media"><span class="notification-bg bg-light-primary"><i data-feather="file-text"> </i></span>
                            <div class="media-body">
                              <p>Hallazgo: </p><span>'.$h->hallazgo.'</span>
                              <p>Fecha compromiso: </p><span>'.$h->fecha_compromiso.'</span>
                            </div>
                          </div>
                        </li>';
                    }
                    echo $htmlh.$html;
                  } ?>
                  
                  
                  <!--<li class="noti-primary">
                    <div class="media"><span class="notification-bg bg-light-primary"><i data-feather="activity"> </i></span>
                      <div class="media-body">
                        <p>Delivery processing </p><span>10 minutes ago</span>
                      </div>
                    </div>
                  </li>
                  <li class="noti-secondary">
                    <div class="media"><span class="notification-bg bg-light-secondary"><i data-feather="check-circle"> </i></span>
                      <div class="media-body">
                        <p>Order Complete</p><span>1 hour ago</span>
                      </div>
                    </div>
                  </li>
                  <li class="noti-success">
                    <div class="media"><span class="notification-bg bg-light-success"><i data-feather="file-text"> </i></span>
                      <div class="media-body">
                        <p>Tickets Generated</p><span>3 hour ago</span>
                      </div>
                    </div>
                  </li>
                  <li class="noti-danger">
                    <div class="media"><span class="notification-bg bg-light-danger"><i data-feather="user-check"> </i></span>
                      <div class="media-body">
                        <p>Delivery Complete</p><span>6 hour ago</span>
                      </div>
                    </div>
                  </li>-->
                </ul>
              </li>
              <li class="onhover-dropdown p-0">
                <button class="btn btn-primary-light" type="button"><a href="<?php echo base_url(); ?>index.php/Login/exitlogin"><i data-feather="log-out"></i>Cerrar sesión</a></button>
              </li>
            </ul>
          </div>
          <div class="d-lg-none mobile-toggle pull-right w-auto"><i data-feather="more-horizontal"></i></div>
        </div>
      </div>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
