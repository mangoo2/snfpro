<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
$tipo=0;
if (!isset($_SESSION['perfilid_tz'])) {
    $perfil=0;
    $tipo=0;
}else{
    $perfil=$_SESSION['perfilid_tz'];  
    $tipo=$_SESSION['tipo'];  
}

if(!isset($_SESSION['perfilid_tz'])){
    ?>
    <script>
        document.location="<?php echo base_url(); ?>index.php/Login"
    </script>
    <?php
}

$menu=$this->ModeloSession->menus($perfil); 
  if($tipo!=2){ ?>
        <!-- Page Sidebar Start-->
        <header class="main-nav">
          <div class="sidebar-user text-center">
            <?php
              if($_SESSION['tipo']==1){ 
                if($_SESSION['foto']!=''){ ?> 
                <img class="img-90 rounded-circle" src="<?php echo base_url(); ?>uploads/empleados/<?php echo $_SESSION['foto'] ?>" alt="">
                <?php }else{ ?>
                  <img class="img-90 rounded-circle" src="<?php echo base_url(); ?>assets/images/dashboard/1.png" alt="">
                <?php }
              }else{
                if($_SESSION['foto']!=''){ ?> 
                <img class="img-90 rounded-circle" src="<?php echo base_url(); ?>uploads/clientes/<?php echo $_SESSION['foto'] ?>" alt="">
                <?php }else{ ?>
                  <img class="img-90 rounded-circle" src="<?php echo base_url(); ?>assets/images/dashboard/1.png" alt="">
                <?php }
              } ?> 
            <div class="badge-bottom"></div><a href="user-profile.html">
              <h6 class="mt-3 f-14 f-w-600"><?php echo $_SESSION['usuario_tz'] ?></h6></a>
            <p class="mb-0 font-roboto"><?php echo $_SESSION['puesto'] ?></p><p class="mb-0 font-roboto"><?php echo $_SESSION['perfil_nombre'] ?></p>
          </div>
        <nav>
            <div class="main-navbar">
                <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
                    <div id="mainnav">           
                        <ul class="nav-menu custom-scrollbar">
                            <li class="back-btn">
                                <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                            </li>
                            <?php if($this->session->userdata("usuarioid_tz")=="1"){ 
                                foreach ($menu->result() as $item){ ?>
                                    <li class="sidebar-main-title">
                                      <div>
                                        <h6><?php echo $item->Nombre; ?></h6>
                                      </div>
                                    </li>
                                    <?php 
                                    $menu =  $item->MenuId;
                                    $menusub = $this->ModeloSession->submenus($perfil,$menu,1);
                                    foreach ($menusub->result() as $datos) { ?>
                                        <li><a class="nav-link menu-title link-nav" href="<?php echo base_url(); ?><?php echo $datos->Pagina; ?>"><i data-feather="<?php echo $datos->Icon; ?>"></i><span><?php echo $datos->Nombre; ?></span></a></li>
                                    <?php }
                                } 
                            }else {
                                if($this->session->userdata("perfilid_tz")=="1") { //administrador ?>
                                    <li class="sidebar-main-title">
                                      <div>
                                        <h6>Catálogos</h6>
                                      </div>
                                    </li>
                                    <li><a class="nav-link menu-title link-nav" href="<?php echo base_url(); ?>Empleados"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg><span>Empleados</span></a>
                                    </li>

                                    <li class="sidebar-main-title">
                                      <div>
                                        <h6>Operaciones</h6>
                                      </div>
                                    </li>
                                    <li><a class="nav-link menu-title link-nav" href="<?php echo base_url(); ?>Proyectos"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders"><line x1="4" y1="21" x2="4" y2="14"></line><line x1="4" y1="10" x2="4" y2="3"></line><line x1="12" y1="21" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="3"></line><line x1="20" y1="21" x2="20" y2="16"></line><line x1="20" y1="12" x2="20" y2="3"></line><line x1="1" y1="14" x2="7" y2="14"></line><line x1="9" y1="8" x2="15" y2="8"></line><line x1="17" y1="16" x2="23" y2="16"></line></svg><span>Proyectos</span></a>
                                    </li>

                                    <li class="sidebar-main-title">
                                      <div>
                                        <h6>Reportes</h6>
                                      </div>
                                    </li>
                                    <li><a class="nav-link menu-title link-nav" href="<?php echo base_url(); ?>Reportes/Salida"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders"><line x1="4" y1="21" x2="4" y2="14"></line><line x1="4" y1="10" x2="4" y2="3"></line><line x1="12" y1="21" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="3"></line><line x1="20" y1="21" x2="20" y2="16"></line><line x1="20" y1="12" x2="20" y2="3"></line><line x1="1" y1="14" x2="7" y2="14"></line><line x1="9" y1="8" x2="15" y2="8"></line><line x1="17" y1="16" x2="23" y2="16"></line></svg><span>Reportes de Salida</span></a></li>
                                <?php } 
                                else if($this->session->userdata("perfilid_tz")=="2") { //operador ?>
                                    <li class="sidebar-main-title">
                                      <div>
                                        <h6>Operaciones</h6>
                                      </div>
                                    </li>
                                    <li><a class="nav-link menu-title link-nav" href="<?php echo base_url(); ?>Proyectos"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders"><line x1="4" y1="21" x2="4" y2="14"></line><line x1="4" y1="10" x2="4" y2="3"></line><line x1="12" y1="21" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="3"></line><line x1="20" y1="21" x2="20" y2="16"></line><line x1="20" y1="12" x2="20" y2="3"></line><line x1="1" y1="14" x2="7" y2="14"></line><line x1="9" y1="8" x2="15" y2="8"></line><line x1="17" y1="16" x2="23" y2="16"></line></svg><span>Proyectos</span></a>
                                    </li>
                                <?php } 
                                else if($this->session->userdata("perfilid_tz")=="3"){ //Reportes ?>
                                    <li class="sidebar-main-title">
                                      <div>
                                        <h6>Reportes</h6>
                                      </div>
                                    </li>
                                    <li><a class="nav-link menu-title link-nav" href="<?php echo base_url(); ?>Reportes/Salida"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-sliders"><line x1="4" y1="21" x2="4" y2="14"></line><line x1="4" y1="10" x2="4" y2="3"></line><line x1="12" y1="21" x2="12" y2="12"></line><line x1="12" y1="8" x2="12" y2="3"></line><line x1="20" y1="21" x2="20" y2="16"></line><line x1="20" y1="12" x2="20" y2="3"></line><line x1="1" y1="14" x2="7" y2="14"></line><line x1="9" y1="8" x2="15" y2="8"></line><line x1="17" y1="16" x2="23" y2="16"></line></svg><span>Reportes de Salida</span></a></li>
                                <?php } 
                            }?> 

                        </ul>
                    </div>
                <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </div>
          </nav>
        </header>
  <?php }else{ ?>
        <header class="main-nav close_icon">
        </header>  
  <?php } ?>               
        <!-- Page Sidebar Ends-->
        <div class="page-body">
          <!-- Container-fluid starts-->
          
<?php
/*
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
if (!isset($_SESSION['perfilid_tz'])) {
    $perfil=0;
}else{
    $perfil=$_SESSION['perfilid_tz'];  
}
if(!isset($_SESSION['perfilid_tz'])){
    ?>
    <script>
        document.location="<?php echo base_url(); ?>index.php/Login"
    </script>
    <?php

$menu=$this->ModeloSession->menus($perfil);

?>

<nav class="navbar navbar-expand-lg navbar-light header-navbar navbar-fixed">
        <div class="container-fluid navbar-wrapper">
            <div class="navbar-header d-flex">
                <div class="navbar-toggle menu-toggle d-xl-none d-block float-left align-items-center justify-content-center" data-toggle="collapse"><i class="ft-menu font-medium-3"></i></div>
                <ul class="navbar-nav">
                    <li class="nav-item mr-2 d-none d-lg-block"><a class="nav-link apptogglefullscreen" id="navbar-fullscreen" href="javascript:;"><i class="ft-maximize font-medium-3"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container">
                <div class="collapse navbar-collapse d-block" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>index.php/Login/exitlogin" class="btn btn-sistema mr-sm-2 mb-1">Cerrar sesión</a>
                        </li>  
      
                        <li class="dropdown nav-item mr-1"><a class="nav-link dropdown-toggle user-dropdown d-flex align-items-end" id="dropdownBasic2" href="javascript:;" data-toggle="dropdown">
                                <div class="user d-md-flex d-none mr-2"><span class="text-right"><?php echo $_SESSION['usuario_tz'];?></span><span class="text-right text-muted font-small-3"><?php echo $_SESSION['perfil_nombre'];?></span></div><img class="avatar" src="<?php echo base_url(); ?>public/img/FAV.png" alt="avatar" height="35" width="35">
                            </a>
                            <div class="dropdown-menu text-left dropdown-menu-right m-0 pb-0" aria-labelledby="dropdownBasic2">
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url(); ?>index.php/Login/exitlogin">
                                    <div class="d-flex align-items-center"><i class="ft-power mr-2"></i><span>Cerrar</span></div>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <div class="wrapper">


        <!-- main menu-->
        <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
        <div class="app-sidebar menu-fixed" data-background-color="man-of-steel" data-image="<?php echo base_url(); ?>public/img/fondo_menu.jpg" data-scroll-to-active="true">
            <!-- main menu header-->
            <!-- Sidebar Header starts-->
            <div class="sidebar-header">
                <div class="logo clearfix logo_tam" style="background: white; height: 114px;">
                    <a class="logo-text float-left" href="<?php echo base_url()?>Inicio">
                        <div class="logo-img">
                            <span class="img_logo">
                                <img src="<?php echo base_url(); ?>public/img/logo_inicio.png" alt="Logo" />
                            </span>
                        </div>
                    </a>
                    <a class="nav-toggle d-none d-lg-none d-xl-block" id="sidebarToggle" href="javascript:;">
                        <i class="toggle-icon ft-toggle-right" data-toggle="expanded"></i></a>
                    <a class="nav-close d-block d-lg-block d-xl-none" id="sidebarClose" href="javascript:;"><i class="ft-x"></i></a>
                </div>
            </div>
            <!-- Sidebar Header Ends-->
            <!-- / main menu header-->
            <!-- main menu content-->
            <div class="sidebar-content main-menu-content">
                <div class="nav-container">
                    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                      <?php foreach ($menu->result() as $item){ ?>
                        <li class="has-sub nav-item"><a href="#"><i class="<?php echo $item->Icon; ?>"></i><span data-i18n="" class="menu-title"><?php echo $item->Nombre; ?></span></a>
                          <ul class="menu-content">
                              <?php 
                                  $menu =  $item->MenuId;
                                  $menusub = $this->ModeloSession->submenus($perfil,$menu,1);
                                  foreach ($menusub->result() as $datos) { ?>
                                      <li>
                                          <a href="<?php echo base_url(); ?><?php echo $datos->Pagina; ?>" class="menu-item">
                                              <i class="<?php echo $datos->Icon; ?>"></i>
                                              <?php echo $datos->Nombre; ?>
                                          </a>
                                      </li>
                              <?php } ?>
                            
                          </ul>
                        </li>
                      <?php } ?>
                      <?php 
                          $menusub = $this->ModeloSession->submenus($perfil,0,0);
                          foreach ($menusub->result() as $datos) { ?>
                            <li class="nav-item">
                              <a href="<?php echo base_url(); echo $datos->Pagina; ?>">
                                <i class="<?php echo $datos->Icon; ?>"></i>
                                <span data-i18n="" class="menu-title"><?php echo $datos->Nombre; ?></span>
                              </a>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
            <!-- main menu content-->
            <div class="sidebar-background"></div>
            <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
            <!-- / main menu-->
        </div>

        <div class="main-panel">
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
*/?>