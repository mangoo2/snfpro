        </div>
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0"><span>Copyright &copy; <?php echo date('Y');?> &nbsp;</span><a href="http://www.mangoo.mx" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2" style="color: orange !important;"> <img style="width: 110px;" src="<?php echo base_url() ?>public/img/mangoo.png"> </a><span class="d-none d-sm-inline-block">, All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Hand crafted & made with <i class="fa fa-heart font-secondary"></i></p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url(); ?>assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="<?php echo base_url(); ?>assets/js/sidebar-menu.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/config.js"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- Plugins JS start-->
    <script src="<?php echo base_url(); ?>assets/js/chart/chartist/chartist.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/knob/knob.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/knob/knob-chart.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/apex-chart/apex-chart.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chart/apex-chart/stock-prices.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/prism/prism.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/clipboard/clipboard.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/counter/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/counter/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/counter/counter-custom.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom-card/custom-card.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/notify/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-us-aea-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-uk-mill-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-au-mill.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-chicago-mill-en.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-in-mill.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vector-map/map/jquery-jvectormap-asia-mill.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dashboard/default.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/notify/index.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datepicker/date-picker/datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datepicker/date-picker/datepicker.es.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datepicker/date-picker/datepicker.custom.js"></script>
    <script src="<?php echo base_url(); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/alert/sweetalert.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatable/datatables/datatable.custom.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/datatable/datatable-extension/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatable-extension.css">
    <script src="<?php echo base_url();?>plugins/confirm/jquery-confirm.min.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?php echo base_url(); ?>assets/js/script.js"></script>
    <!--<script src="<?php echo base_url(); ?>assets/js/theme-customizer/customizer.js"></script>-->
    <!-- login js-->
    <!-- Plugin used-->
    
    <!-- Plugins JS start-->
    <!--<script src="<?php echo base_url(); ?>assets/js/calendar/tui-code-snippet.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar/tui-time-picker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar/tui-date-picker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar/chance.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar/tui-calendar.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar/calendars.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar/schedules.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/calendar/app.js"></script>-->
    <!-- Plugins JS Ends-->

    <!-- Plugins JS start-->
    <!--<script src="<?php echo base_url(); ?>assets/js/jsgrid/jsgrid.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jsgrid/griddata.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jsgrid/jsgrid.js"></script>-->
    <!-- Plugins JS Ends-->
    <script src="<?php echo base_url(); ?>/public/js/select2.full.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.2.4/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>public/fileinput/fileinput.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>public/js/spanish_dt.js"></script>
    <script src="<?php echo base_url(); ?>public/js/correos_hallazgo.js?v=<?php echo date('YmdGis') ?>"></script>
    <?php if($_SERVER["REQUEST_URI"]=='/snfpro/Inicio' or $_SERVER["REQUEST_URI"]=='/Inicio'){ ?>
      <script type="text/javascript">
        $(document).ready(function($) {
          if(screen.width<600){
            $('.toggle-sidebar').click();
          }
        });
      </script>    
    <?php } ?>
  </body>
</html>
