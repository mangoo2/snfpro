<?php
	header("Content-Type: text/html;charset=UTF-8");
	header("Pragma: public");
	header("Expires:0");
	header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Type: application/vnd.ms-excel;");
	header("Content-Disposition: attachment; filename=plantilla_excelOrdenes".date("Ymd").".xls");

?>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<table border="1">
		<thead>
			<tr>
				<th scope="col">Semana</th>
				<th scope="col">Turno (1=1RO, 2=2DO, 3=3RO)</th>
				<th scope="col">ID Supervisor</th>
				<th scope="col">OT</th>
				<th scope="col">ID Actividad</th>
				<th scope="col">Fecha (AAAA-MM-DD)</th>
				<th scope="col">Nombre quien realiza</th>
				<th scope="col">Seguimiento (1=Cancelado,2=Programado,3=OK,4=Reprogramado,5=No realizado)</th>
				<th scope="col">Fecha final programada (AAAA-MM-DD)</th>
				<th scope="col">Observaciones</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
			</tr>
		</tbody>
	</table>