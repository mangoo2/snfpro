<style type="text/css">
  .head_tbl_prog {
    background-color: #0232cf !important;
    color:white !important;
  }
</style>
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Órdenes de Trabajo diario</h3>
      </div>

    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
            <input type="hidden" id="id_cli" value="<?php echo $id_cli;?>">
            <input type="hidden" id="fecha_act" value="<?php echo date("Y-m-d");?>">
            <input type="hidden" id="dia_mes" value="0">
            <div class="col">
              <div class="mb-3 row">
                <div class="col-sm-5">
                  <div class="mb-3 row">
                    <div class="col-sm-12">
                      <div class="datepicker-here" id="fecha" data-language="es"></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-7">
                  <div class="col-12">
                    <div class="mb-3 row">
                      <div class="col-sm-4">
                        <span id="fecha_select">Fecha: </span>  
                      </div>
                      <div class="col-md-4">
                        <span>Ver mes completo</span>  
                      </div>
                      <div class="col-sm-4 col-sm-4 col-6" style="text-align: end;">
                        <div class="col-md-3">
                          <label class="switch">
                            <input id="mon_compl" type="checkbox">
                            <span class="sliderN round"></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="table" id="cont_table">
                      <table class="table" id="table_actividades">
                        <thead class="head_tbl_prog">
                          <tr>
                            <th scope="col">Actividad</th>
                            <th scope="col">Turno</th>
                            <th scope="col">Estatus</th>
                            <th scope="col">Causas</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12 col-lg-12 col-xl-12" id="cont_table_mes">
                  <div class="table-responsive">
                    <table class="table" id="table_actividades_mes">
                      <thead class="head_tbl_prog" id="thead_mes">

                      </thead>
                      <tbody id="body_mes">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <a href="<?php echo base_url()?>Proyectos/ordenes/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
            </div>
          </div>  
        </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" id="id_ord">
        <h5 class="modal-title" id="exampleModalLabel">Editar Estatus</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="mb-3 row">
          <div class="col-sm-12">
            <label>Estatus</label>
            <select class="form-control" id="estatus">
              <option value="1">Cancelado</option>
              <!--<option value="2">Programado</option>-->
              <option value="3">OK</option>
              <option value="4">Reprogramado</option>
              <option value="5">No realizado</option>
            </select>
          </div>
        </div>
        <div class="mb-3 row">
          <label class="">Causas</label>
          <div class="col-sm-12">
            <input class="form-control" type="text" id="causas" name="causas">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-secondary" type="button" id="edit_status">Aceptar</button>
      </div>
    </div>
  </div>
</div>