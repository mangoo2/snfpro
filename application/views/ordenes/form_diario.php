<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> Órdenes de Trabajo</h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>Ordenes/listadoDiario/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Lista de órdenes de trabajo diario</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id_proyecto" id="id_proy" value="<?php echo $id_proy;?>">
          <input type="hidden" name="id_cliente" id="id_cli" value="<?php echo $id_cli;?>">
          <input type="hidden" id="fecha_act" value="<?php echo date("Y-m-d");?>">
          <input type="hidden" name="id" id="id" value="<?php if(isset($a)) echo $a->id; else echo '0' ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <div class="col-sm-6">
                    <!-- Container-fluid starts-->
                    <!--<div class="container-fluid calendar-basic">
                      <div class="row">
                        <div class="col-md-12">
                          <div id="menu">
                            <div id="menu-navi">
                              <div class="menu-navi-left">
                                <button class="btn btn-primary move-today" type="button" data-action="move-today">Today</button>
                              </div>
                              <div class="render-range menu-navi-center" id="renderRange"></div>
                              <div class="menu-navi-right">
                                <button class="btn btn-primary" id="dropdownMenu-calendarType" type="button" data-bs-toggle="dropdown"><i id="calendarTypeIcon"></i><span id="calendarTypeName">Dropdown</span><i class="fa fa-angle-down"></i></button>
                                <ul class="dropdown-menu" role="menu">
                                  <li role="presentation"><a class="dropdown-menu-title" role="menuitem" data-action="toggle-daily"><i class="fa fa-bars"></i>Daily</a></li>
                                  <li role="presentation"><a class="dropdown-menu-title" role="menuitem" data-action="toggle-weekly"><i class="fa fa-th-large"></i>Weekly</a></li>
                                  <li role="presentation"><a class="dropdown-menu-title" role="menuitem" data-action="toggle-monthly"><i class="fa fa-th"></i>Month</a></li>
                                  <li role="presentation"><a class="dropdown-menu-title" role="menuitem" data-action="toggle-weeks2"><i class="fa fa-th-large"></i>2 weeks</a></li>
                                  <li role="presentation"><a class="dropdown-menu-title" role="menuitem" data-action="toggle-weeks3"><i class="fa fa-th-large"></i>3 weeks</a></li>
                                  <li class="dropdown-divider" role="presentation"></li>
                                  <li role="presentation"><a role="menuitem" data-action="toggle-workweek">
                                      <input class="tui-full-calendar-checkbox-square" type="checkbox" value="toggle-workweek" checked=""><span class="checkbox-title"></span>Show weekends</a></li>
                                  <li role="presentation"><a role="menuitem" data-action="toggle-start-day-1">
                                      <input class="tui-full-calendar-checkbox-square" type="checkbox" value="toggle-start-day-1"><span class="checkbox-title"></span>Start Week on Monday</a></li>
                                  <li role="presentation"><a role="menuitem" data-action="toggle-narrow-weekend">
                                      <input class="tui-full-calendar-checkbox-square" type="checkbox" value="toggle-narrow-weekend"><span class="checkbox-title"></span>Narrower than weekdays</a></li>
                                </ul>
                                <div class="move-btn">
                                  <button class="btn btn-primary move-day" type="button" data-action="move-prev"><i class="fa fa-angle-left" data-action="move-prev"></i></button>
                                  <button class="btn btn-primary move-day" type="button" data-action="move-next"><i class="fa fa-angle-right" data-action="move-next"></i></button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div id="lnb">
                            <div class="lnb-new-schedule">
                              <button class="btn lnb-new-schedule-btn btn-primary" id="btn-new-schedule" type="button" data-bs-toggle="modal">New schedule</button>
                            </div>
                            <div class="lnb-calendars" id="lnb-calendars">
                              <div class="lnb-calendars-d1" id="calendarList"></div>
                              <div class="lnb-calendars-item m-0 p-0">
                                <label>
                                  <input class="tui-full-calendar-checkbox-square" type="checkbox" value="all" checked=""><span></span><strong>View all</strong>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div id="right">
                            <div id="calendar"></div>
                          </div>
                        </div>
                      </div>
                    </div>-->
                    <!-- Container-fluid Ends-->
                    <div class="mb-3 row">
                      <div class="col-sm-12">
                        <div class="datepicker-here" id="fecha" data-language="es"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="mb-3 row">
                      <div class="col-sm-12">
                        <label>Actividad</label>
                        <select class="form-control" name="id_actividad" id="id_actividad">
                          <?php foreach ($act as $ac){ ?>
                            <option data-tipo="<?php echo $ac->tipo; ?>" value="<?php echo $ac->id ?>" <?php if(isset($a) && $ac->id==$a->id_actividad && $ac->tipo==$a->tipo_act) echo 'selected' ?>><?php echo $ac->nombre ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="mb-3 row">
                      <div class="col-sm-12">
                        <label>Turno</label>
                        <select class="form-control" name="turno" id="turno">
                          <option value="1">1ero</option>
                          <option value="2">2do</option>
                          <option value="3">3ero</option>
                        </select>
                      </div>
                    </div>
                    <div class="mb-3 row">
                      <div class="col-sm-12">
                        <label>Estatus</label>
                        <select class="form-control" name="estatus" id="estatus">
                          <option value="1">Cancelado</option>
                          <!--<option value="2">Programado</option>-->
                          <option value="3">OK</option>
                          <option value="4">Reprogramado</option>
                          <option value="5">No realizado</option>
                        </select>
                      </div>
                    </div>
                    <div class="mb-3 row">
                      <label class="col-sm-6 col-form-label">Causa / observación</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="text" name="causas" value="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="col-sm-9">
                <button id="btn_sub" class="btn btn-primary" type="button">Guardar registro</button>
                <a href="<?php echo base_url()?>Ordenes/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              </div>
            </div>  
          </div>
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>