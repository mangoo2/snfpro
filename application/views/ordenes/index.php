<style type="text/css">
  .head_tbl_prog {
    background-color: #0232cf !important;
    color:white !important;
  }
</style>
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Órdenes de Trabajo Semanal</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
            <input type="hidden" id="id_cli" value="<?php echo $id_cli;?>">
            <div class="col-12">
              <a href="<?php echo base_url()?>Proyectos/ejecucion/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <a class="btn btn-primary" href="<?php echo base_url() ?>Ordenes/registro/0/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Nueva orden</a>
              <!--<a class="btn btn-primary" href="<?php echo base_url() ?>Ordenes/nvaPlantilla"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar Plantilla</a>
              <button id="upload_pre" class="btn btn-primary" type="button"><i class="fa fa-upload" aria-hidden="true"></i> Cargar Plantilla</button>-->
            </div>
            <div class="col-12">
              <br>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="control-label">Semana</label>
                <select class="form-control round" id="semana">
                  <option value="0">Todas</option>
                  <?php $sel=""; for($i=1; $i<=53; $i++){ 
                    if($semana==$i && $this->session->userdata("semana_orden")==0 || $this->session->userdata("semana_orden")>0 && $this->session->userdata("semana_orden")==$i) $sel="selected"; else $sel="";
                    echo '<option '.$sel.' value="'.$i.'">'.$i.'</option>';
                  } ?>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label class="control-label">Año</label>
                <select class="form-control round" id="anio">
                  <?php $sel=""; $anio=date("Y"); $aa=$anio-5; for($j=$aa; $j<=$anio; $j++){ 
                    if($anio==$j) $sel="selected"; else $sel="";
                    echo '<option '.$sel.' value="'.$j.'">'.$j.'</option>';
                  } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-1">
              <label>Turno</label>
              <select class="form-control" id="turno">
                <option value="1">1ero</option>
                <option value="2">2do</option>
                <option value="3">3ero</option>
              </select>
            </div>
            <div class="col-sm-2">
              <label>Tipo</label>
              <select class="form-control" id="tipo">
                <option value="0">Todos</option>
                <option value="1">Semanal</option>
                <option value="2">Diario</option>
              </select>
            </div>
            <div class="col-sm-2">
              <label>Frecuencia</label>
              <select class="form-control" id="frecuencia">
                <option value="0">Todos</option>
                <option value="1">Semanal</option>
                <option value="2">Quincenal</option>
                <option value="3">Mensual</option>
                <option value="4">Bimestral</option>
                <option value="5">Anual</option>
              </select>
            </div>
            <!--<div class="col-sm-2">
              <label>Tipo Act.</label>
              <select class="form-control" id="tipo_act">
                <option value="0">Normal</option>
                <option value="1">Crítica</option>
              </select>
            </div>-->
            <div class="col-sm-3">
              <label>Actividad</label>
              <select class="form-control" id="id_act">
                <option value="0">Todas</option>
                <?php foreach ($act as $ac){ ?>
                  <option value="<?php echo $ac->id ?>" <?php if($this->session->userdata("activ_orden")==$ac->id) echo 'selected' ?>><?php echo $ac->nombre ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="col-sm-3">
              <label style="color: transparent;">delete</label>
              <div class="col-md-8">
                <button class="btn btn-danger" id="elimina_selec"><i class="fa fa-trash"></i> Eliminar selección</button><br>
              </div>
            </div>
            <div class="col-12" id="cont_table_ordenes">
              <div class="col-3">
                <br>
              </div>
              <div class="table-responsive">
                <table class="table" id="table_ordenes">
                  <thead>
                    <tr>
                      <th scope="col"></th>
                      <th scope="col">ID</th>
                      <th scope="col">OT</th>
                      <th scope="col">Actividad</th>
                      <th scope="col">Fecha</th>
                      <th scope="col">Semana</th>
                      <th scope="col">Nombre quien registró</th>
                      <!--<th scope="col">Firma</th>-->
                      <th scope="col">Nombre de cliente</th>
                      <!--<th scope="col">Firma</th>-->
                      <th scope="col">Seguimiento</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="col-12" id="cont_table_ordenes_diario">
              <div class="table-responsive">
                <table class="table" id="table_ordenes_diario">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Actividad</th>
                      <th scope="col">Turno</th>
                      <th scope="col">Fecha</th>
                      <th scope="col">Seguimiento</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" id="id_ord">
        <h5 class="modal-title" id="exampleModalLabel">Dar Seguimiento</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="mb-3 row">
          <div class="col-sm-12">
            <label>Estatus</label>
            <select class="form-control" id="estatus">
              <option value="1">Cancelado</option>
              <!--<option value="2">Programado</option>-->
              <option value="3">OK</option>
              <option value="4">Reprogramado</option>
              <option value="5">No realizado</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-secondary" type="button" id="edit_status">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEdit2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" id="id_ord2">
        <h5 class="modal-title" id="exampleModalLabel">Editar Estatus</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="mb-3 row">
          <div class="col-sm-12">
            <label>Estatus</label>
            <select class="form-control" id="estatus2">
              <option value="1">Cancelado</option>
              <!--<option value="2">Programado</option>-->
              <option value="3">OK</option>
              <option value="4">Reprogramado</option>
              <option value="5">No realizado</option>
            </select>
          </div>
        </div>
        <div class="mb-3 row">
          <label class="">Causas</label>
          <div class="col-sm-12">
            <input class="form-control" type="text" id="causas" name="causas">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-secondary" type="button" id="edit_status2">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalCarga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Cargar malla de datos</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" >
        <div class="row carga_malla">
          <div class="col-md-12">
            <form id="form_carga_malla" method="POST" enctype="multipart/form-data">
              <div class="col-md-8">
                <div class="form-group">
                  <label>Seleccione un csv</label>
                  <input type="file" name="inputFile" id="inputFile" class="form-control" accept=".csv"> 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <button class="btn btn-secondary" type="button" id="aceptar_carga"><i class="fa fa-upload"></i> Cargar Malla</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>