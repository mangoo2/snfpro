<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> Órdenes de Trabajo Semanal</h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>Ordenes/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Lista de órdenes de trabajo semanal</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id_proyecto" id="id_proy" value="<?php echo $id_proy;?>">
          <input type="hidden" name="id_cliente" id="id_cli" value="<?php echo $id_cli;?>">
          <input type="hidden" name="firma" id="firma" value="<?php if(isset($a)) echo $a->firma;?>">
          <input type="hidden" name="firma2" id="firma2" value="<?php if(isset($a)) echo $a->firma;?>">
          <input type="hidden" name="firma_cliente" id="firma_cliente" value="<?php if(isset($a)) echo $a->firma_cliente;?>">
          <input type="hidden" name="firma_cliente2" id="firma_cliente2" value="<?php if(isset($a)) echo $a->firma_cliente2;?>">
          <input type="hidden" name="id" id="id" value="<?php if(isset($a)) echo $a->id; else echo '0' ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <div class="col-sm-3">
                    <label>Semana</label>
                    <input readonly class="form-control" type="text" name="semana" value="<?php if(isset($a)) echo $a->semana; else echo $semana; ?>">
                  </div>
                  <div class="col-sm-3">
                    <label>Turno</label>
                    <select class="form-control" name="turno" id="turno">
                      <option <?php if(isset($a) && $a->turno=="1") echo "selected"; ?> value="1">1ero</option>
                      <option <?php if(isset($a) && $a->turno=="2") echo "selected"; ?> value="2">2do</option>
                      <option <?php if(isset($a) && $a->turno=="3") echo "selected"; ?> value="3">3ero</option>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <label>Supervisor</label>
                    <select class="form-control" name="id_supervisor" id="id_supervisor">
                      <option value="" disabled selected>Elige un supervisor</option>
                      <?php foreach ($sup as $s){ ?>
                        <option value="<?php echo $s->id_empleado ?>" <?php if(isset($a) && $s->id_empleado==$a->id_supervisor) echo 'selected' ?>><?php echo $s->nombre ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <label>OT</label>
                    <input class="form-control" type="text" name="ot" value="<?php if(isset($a)) echo $a->ot; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-3">
                    <label>Actividad</label>
                    <select class="form-control" name="id_limpieza" id="id_limpieza">
                      <?php foreach ($act as $ac){ ?>
                        <option data-tipo="<?php echo $ac->tipo; ?>" value="<?php echo $ac->id ?>" <?php if(isset($a) && $ac->id==$a->id_limpieza) echo 'selected' ?>><?php echo $ac->nombre ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <label>Fecha en que realiza</label>
                    <input class="form-control" type="date" name="fecha" value="<?php if(!isset($a)) echo date("Y-m-d"); else echo $a->fecha; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <h5 class="pull-right">Firmas SNF</h5>  
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-6">
                    <div class="col-lg-12 col-md-12" id="cont_otra" style="text-align:center">           
                      <div class="col-md-6">
                        <h5 class="pull-right">Capturar otra firma</h5>  
                      </div>
                      <div class="col-md-3">
                        <label class="switch">
                          <input id="otra_firma" type="checkbox" <?php if(isset($a->firma) && $a->firma=="" || !isset($a->firma)) echo "checked"; ?>>
                          <span class="sliderN round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="row col-md-12" id="cont_firma" <?php if(isset($a->firma) && $a->firma=="") echo'style="display: none;"'; ?> style="display: none;">
                      <div id="aceptance">
                        <div id="signature" class="signature col-md-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                            <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma con su mouse o su dedo</label><br>
                            <canvas id="patientSignature" class="patientSignature sighiddeable hidden-xs hidden-sm img-fluid" style="border: 2px dashed rgb(29, 175, 147); cursor: crosshair;">
                            </canvas>
                            <div class="col-md-12" style="text-align:center;margin-bottom:1px;border-radius:4px;">
                                <a class="btn waves-effect cyan btn-bmz sighiddeable hidden-xs hidden-sm clearSignature" 
                                  data-signature="patientSignature" id="clearSignature">Limpiar</a>
                                <a class="btn btn-success waves-effect waves-green green btn-flat firma">Aceptar</a>
                            </div>
                        </div>        
                      </div>
                    </div>

                    <div class="col-md-6" id="cont_firma_actual" <?php if(isset($a) && $a->firma=="" || !isset($a)) echo 'style="display: none;"'; ?>>
                      <div id="signature" class="signature col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                        <?php 
                        if(isset($a->firma) && $a->firma!=""){
                          $fh = fopen(FCPATH."uploads/firma/".$a->firma, 'r') or die("Se produjo un error al abrir el archivo");
                          $linea = fgets($fh);
                          fclose($fh);    
                        }else{
                            $linea='#';
                        }?>
                        <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Firma capturada</label><br>
                        <img class="img-fluid" src="<?php echo $linea;?>" width="355" height="160" style="border:dotted 1px black;">
                      </div>
                    </div>

                  </div>
                  <div class="col-sm-6 row">
                    <div class="col-sm-10">
                      <label>Nombre quien realiza</label>
                      <!-- <input class="form-control" type="text" name="realiza" value="<?php //if(isset($a)) echo $a->realiza; ?>"> -->
                      <select class="form-control" name="realiza" id="realiza">
                        <?php foreach ($emp as $e){ $sel=""; if(isset($a) && $e->id==$a->realiza) $sel='selected'; ?>
                          <option value="<?php echo $e->id_empleado ?>" <?php echo $sel; ?>><?php echo $e->nombre ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-md-1">
                      <label style="color: transparent;">agregar</label>
                      <button id="add_emp" class="btn btn-primary" type="button" ><i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div> 
                    <table class="table" id="tabla_emp">
                      <thead>
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="body_emp">
                        <?php
                          echo $html;
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- firma 2 de cliente -->
                <div class="mb-3 row">
                  <div class="col-sm-6">
                    <div class="col-lg-12 col-md-12" id="cont_otra2" style="text-align:center">           
                      <div class="col-md-6">
                        <h5 class="pull-right">Capturar otra firma</h5>  
                      </div>
                      <div class="col-md-3">
                        <label class="switch">
                          <input id="otra_firma2" type="checkbox" <?php if(isset($a->firma2) && $a->firma2=="" || !isset($a->firma2)) echo "checked"; ?>>
                          <span class="sliderN round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="row col-md-12" id="cont_firma2" <?php if(isset($a->firma2) && $a->firma2=="") echo'style="display: none;"'; ?> style="display: none;">
                      <div id="aceptance">
                        <div id="signature" class="signature col-md-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                            <label for="patientSignatureSNF2" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma con su mouse o su dedo</label><br>
                            <canvas id="patientSignatureSNF2" class="patientSignatureSNF2 sighiddeable hidden-xs hidden-sm img-fluid" style="border: 2px dashed rgb(29, 175, 147); cursor: crosshair;">
                            </canvas>
                            <div class="col-md-12" style="text-align:center;margin-bottom:1px;border-radius:4px;">
                                <a class="btn waves-effect cyan btn-bmz sighiddeable hidden-xs hidden-sm clearSignatureSNF2" 
                                  data-signature="patientSignatureSNF2" id="clearSignatureSNF2">Limpiar</a>
                                <a class="btn btn-success waves-effect waves-green green btn-flat firma2">Aceptar</a>
                            </div>
                        </div>        
                      </div>
                    </div>

                    <div class="col-md-6" id="cont_firma_actual2" <?php if(isset($a) && $a->firma2=="" || !isset($a)) echo 'style="display: none;"'; ?>>
                      <div id="signature" class="signature col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                        <?php 
                        if(isset($a->firma2) && $a->firma2!=""){
                          $fh = fopen(FCPATH."uploads/firma/".$a->firma2, 'r') or die("Se produjo un error al abrir el archivo");
                          $linea2 = fgets($fh);
                          fclose($fh);    
                        }else{
                            $linea2='#';
                        }?>
                        <label for="patientSignatureSNF2" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Firma capturada</label><br>
                        <img class="img-fluid" src="<?php echo $linea2;?>" width="355" height="160" style="border:dotted 1px black;">
                      </div>
                    </div>
                  </div>

                </div>
                <?php if(isset($a->firma) && $a->firma!="" && isset($a->firma2) && $a->firma2!=""){ ?>
                  <hr>
                  <!-- firma del cliente -->
                  <div class="mb-3 row">
                    <h5 class="pull-right">Firmas Cliente</h5>  
                  </div>
                  <div class="mb-3 row">
                    <div class="col-sm-6">
                      <div class="col-lg-12 col-md-12" id="cont_otra_cli" style="text-align:center">           
                        <div class="col-md-6">
                          <h5 class="pull-right">Capturar otra firma</h5>  
                        </div>
                        <div class="col-md-3">
                            <label class="switch">
                                <input id="otra_firma_cli" type="checkbox" <?php if(isset($a->firma_cliente) && $a->firma_cliente=="" || !isset($a->firma_cliente)) echo "checked"; ?>>
                                <span class="sliderN round"></span>
                            </label>
                        </div>
                      </div>
                      <div class="row col-md-12" id="cont_firma_cli" <?php if(isset($a->firma_cliente) && $a->firma_cliente=="") echo'style="display: none;"'; ?> style="display: none;">
                        <div id="aceptance">
                          <div id="signature" class="signature col-md-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                              <label for="patientSignature2" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma con su mouse o su dedo</label><br>
                              <canvas id="patientSignature2" class="patientSignature2 sighiddeable hidden-xs hidden-sm img-fluid" style="border: 2px dashed rgb(29, 175, 147); cursor: crosshair;">
                              </canvas>
                              <div class="col-md-12" style="text-align:center;margin-bottom:1px;border-radius:4px;">
                                  <a class="btn waves-effect cyan btn-bmz sighiddeable hidden-xs hidden-sm clearSignature2" 
                                    data-signature="patientSignature2" id="clearSignature2">Limpiar</a>
                                  <a class="btn btn-success waves-effect waves-green green btn-flat firma_cli">Aceptar</a>
                              </div>
                          </div>        
                        </div>
                      </div>

                      <div class="col-md-6" id="cont_firma_actual_cli" <?php if(isset($a) && $a->firma_cliente=="" || !isset($a)) echo 'style="display: none;"'; ?>>
                        <div id="signature" class="signature col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                          <?php 
                          if(isset($a->firma_cliente) && $a->firma_cliente!=""){
                            $fh = fopen(FCPATH."uploads/firma/".$a->firma_cliente, 'r') or die("Se produjo un error al abrir el archivo");
                            $firma_cli = fgets($fh);
                            fclose($fh);    
                          }else{
                              $firma_cli='#';
                          }?>
                          <label for="patientSignature2" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Firma capturada</label><br>
                          <img class="img-fluid" src="<?php echo $firma_cli;?>" width="355" height="160" style="border:dotted 1px black;">
                        </div>
                      </div>

                    </div>

                    <div class="col-sm-6">
                      <div class="col-lg-12 col-md-12" id="cont_otra_cli2" style="text-align:center">           
                        <div class="col-md-6">
                          <h5 class="pull-right">Capturar otra firma</h5>  
                        </div>
                        <div class="col-md-3">
                            <label class="switch">
                                <input id="otra_firma_cli2" type="checkbox" <?php if(isset($a->firma_cliente2) && $a->firma_cliente2=="" || !isset($a->firma_cliente2)) echo "checked"; ?>>
                                <span class="sliderN round"></span>
                            </label>
                        </div>
                      </div>
                      <div class="row col-md-12" id="cont_firma_cli2" <?php if(isset($a->firma_cliente2) && $a->firma_cliente2=="") echo'style="display: none;"'; ?> style="display: none;">
                        <div id="aceptance">
                          <div id="signature" class="signature col-md-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                              <label for="patientSignatureC2" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma con su mouse o su dedo</label><br>
                              <canvas id="patientSignatureC2" class="patientSignatureC2 sighiddeable hidden-xs hidden-sm img-fluid" style="border: 2px dashed rgb(29, 175, 147); cursor: crosshair;">
                              </canvas>
                              <div class="col-md-12" style="text-align:center;margin-bottom:1px;border-radius:4px;">
                                  <a class="btn waves-effect cyan btn-bmz sighiddeable hidden-xs hidden-sm clearSignatureC2" 
                                    data-signature="patientSignatureC2" id="clearSignatureC2">Limpiar</a>
                                  <a class="btn btn-success waves-effect waves-green green btn-flat firma_cli2">Aceptar</a>
                              </div>
                          </div>        
                        </div>
                      </div>

                      <div class="col-md-6" id="cont_firma_actual_cli2" <?php if(isset($a) && $a->firma_cliente2=="" || !isset($a)) echo 'style="display: none;"'; ?>>
                        <div id="signature" class="signature col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
                          <?php 
                          if(isset($a->firma_cliente2) && $a->firma_cliente2!=""){
                            $fh = fopen(FCPATH."uploads/firma/".$a->firma_cliente2, 'r') or die("Se produjo un error al abrir el archivo");
                            $firma_cli2 = fgets($fh);
                            fclose($fh);    
                          }else{
                              $firma_cli2='#';
                          }?>
                          <label for="patientSignatureC2" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Firma capturada</label><br>
                          <img class="img-fluid" src="<?php echo $firma_cli2;?>" width="355" height="160" style="border:dotted 1px black;">
                        </div>
                      </div>

                    </div>
                  </div>
                <?php } ?>
                <div class="mb-3">
                  <div class="col-sm-6">
                    <label>Cliente</label>
                    <input readonly class="form-control" type="text" id="cliente" value="<?php if(isset($cli)) echo $cli->empresa; ?>">
                  </div>
                  <div class="mb-3 row">
                    <div class="col-sm-6">
                      <br>
                      <label>Estatus de seguimiento</label>
                      <select class="form-control" name="seguimiento" id="seguimiento">
                        <option value="0"></option>
                        <option <?php if(isset($a) && $a->seguimiento==1) echo 'selected' ?> style="color:red; font-weight: bold;" value="1">Cancelado</option>
                        <!--<option <?php if(isset($a) && $a->seguimiento==2) echo 'selected' ?> style="color:blue;" value="2">Programado</option>-->
                        <option <?php if(isset($a) && $a->seguimiento==3) echo 'selected' ?> style="color:green; font-weight: bold;" value="3">OK</option>
                        <option <?php if(isset($a) && $a->seguimiento==4) echo 'selected' ?> style="color:yellow; font-weight: bold;" value="4">Reprogramado</option>
                        <option <?php if(isset($a) && $a->seguimiento==5) echo 'selected' ?> style="color:yellow; font-weight: bold;" value="5">No realizado</option>
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <br>
                      <label>Fecha final programada</label>
                      <input class="form-control" type="date" name="fecha_final" value="<?php if(!isset($a)) echo date("Y-m-d"); else echo $a->fecha_final; ?>">
                    </div>
                  </div>
                  <label class="form-label" for="observaciones">Observaciones</label>
                  <textarea class="form-control" name="observaciones" value="<?php if(isset($a)) echo $a->observaciones; ?>"><?php if(isset($a)) echo $a->observaciones; ?></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <button id="btn_sub" class="btn btn-primary" type="submit">Guardar registro</button>
              <a href="<?php echo base_url()?>Ordenes/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
            </div>
          </div>  
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/firma.js?v=<?php echo date('YmdGis'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/firma2.js?v=<?php echo date('YmdGis'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/firma_snf2.js?v=<?php echo date('YmdGis'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/firma_cli2.js?v=<?php echo date('YmdGis'); ?>"></script>