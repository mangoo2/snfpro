<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> Actividades</h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>Actividades/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Lista de actividades</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id_proyecto" id="id_proy" value="<?php echo $id_proy;?>">
          <input type="hidden" name="id_cliente" id="id_cli" value="<?php echo $id_cli;?>">
          <input type="hidden" name="id" id="id" value="<?php if(isset($a)) echo $a->id; else echo '0' ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3">
                  <div class="col-md-3">
                    <center><label class="col-form-label m-r-10">Tipo de actividad</label></center>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-md-5">
                    <label class="col-form-label m-r-10">Actividades de hombres y mujeres</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input class="tipo" id="tipo1" name="tipo" type="radio" <?php if(isset($a->tipo) && $a->tipo=="0") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <label class="col-form-label m-r-10">Actividades exclusivas de hombres</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input class="tipo" id="tipo2" name="tipo" type="radio" <?php if(isset($a->tipo) && $a->tipo=="1") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Nombre</label>
                  <div class="col-sm-9">
                    <input placeholder="Nombre de la actividad" class="form-control" type="text" name="nombre" value="<?php if(isset($a)) echo $a->nombre; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Zona</label>
                  <div class="col-sm-9">
                    <input placeholder="Zona de la actividad" class="form-control" type="text" name="zona" value="<?php if(isset($a)) echo $a->zona; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-3">
                    <label># de orden de trabajo</label>
                    <input class="form-control hgt_inp" type="text" name="mp" value="<?php if(isset($a)) echo $a->mp; ?>">
                  </div>
                  <!--<div class="col-sm-3">
                    <label>Frecuencia</label>
                    <input class="form-control hgt_inp" type="text" name="frecuencia_cotiza" value="<?php if(isset($a)) echo $a->frecuencia_cotiza; ?>">
                  </div>-->
                  <div class="col-sm-3">
                    <label>Frecuencia</label>
                    <select class="form-control" name="frecuencia_cotiza">
                      <option selected disabled>Selecciona una opción</option>
                      <option value="1" <?php if(isset($a) && $a->frecuencia_cotiza==1) echo 'selected' ?>>Semanal</option>
                      <option value="2" <?php if(isset($a) && $a->frecuencia_cotiza==2) echo 'selected' ?>>Quincenal</option>
                      <option value="3" <?php if(isset($a) && $a->frecuencia_cotiza==3) echo 'selected' ?>>Mensual</option>
                      <option value="4" <?php if(isset($a) && $a->frecuencia_cotiza==4) echo 'selected' ?>>Bimestral</option>
                      <option value="5" <?php if(isset($a) && $a->frecuencia_cotiza==5) echo 'selected' ?>>Anual</option>
                    </select>
                  </div>
                  <!--<div class="col-sm-3">
                    <label>Frecuencia modificada 1</label>
                    <input class="form-control hgt_inp" type="text" name="frecuencia_modi1" value="<?php if(isset($a)) echo $a->frecuencia_modi1; ?>">
                  </div>
                  <div class="col-sm-3">
                    <label>Frecuencia modificada 2</label>
                    <input class="form-control hgt_inp" type="text" name="frecuencia_modi2" value="<?php if(isset($a)) echo $a->frecuencia_modi2; ?>">
                  </div>-->
                  <div class="col-sm-3">
                    <label>Recursos necesarios</label>
                    <input class="form-control hgt_inp" type="text" name="recursos" value="<?php if(isset($a)) echo $a->recursos; ?>">
                  </div>
                  <div class="col-sm-3">
                    <label>Tiempo</label>
                    <input class="form-control hgt_inp" type="text" name="tiempo" value="<?php if(isset($a)) echo $a->tiempo; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <!--<div class="col-sm-3">
                    <label>Propuesta cambio de frecuencia</label>
                    <input class="form-control hgt_inp" type="text" name="propuesta" value="<?php if(isset($a)) echo $a->propuesta; ?>">
                  </div>-->
                  
                  <div class="col-sm-3">
                    <label>Horas hombre</label>
                    <input class="form-control hgt_inp" type="text" name="horas_hombre" value="<?php if(isset($a)) echo $a->horas_hombre; ?>">
                  </div>
                  <div class="col-sm-3">
                    <label>Unidades de Frecuencia</label>
                    <input class="form-control hgt_inp" type="number" name="days" value="<?php if(isset($a)) echo $a->days; ?>">
                  </div>
                  <div class="col-sm-3">
                    <label>Grupo propietario</label>
                    <input class="form-control hgt_inp" type="text" name="grupo" value="<?php if(isset($a)) echo $a->grupo; ?>">
                  </div>
                  <div class="col-sm-3">
                    <label>Fecha inicio de actividad</label>
                    <input onkeydown="return false" min="<?php echo $dp->fecha_ini; ?>" max="<?php echo $dp->fecha_fin; ?>" class="form-control hgt_inp" type="date" name="fecha_ini" value="<?php if(isset($a)) echo $a->fecha_ini; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <!--<div class="col-sm-3">
                    <label>Fecha realiza actividad</label>
                    <input class="form-control hgt_inp" type="date" name="fecha_realiza" value="<?php if(isset($a)) echo $a->fecha_realiza; ?>">
                  </div>-->
                  <div class="col-sm-3">
                    <label>Fecha fin de actividad</label>
                    <input onkeydown="return false" min="<?php echo $dp->fecha_ini; ?>" max="<?php echo $dp->fecha_fin; ?>" class="form-control hgt_inp" type="date" name="fecha_fin" value="<?php if(isset($a)) echo $a->fecha_fin; ?>">
                  </div>
                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">Crítica</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input class="critica" id="critica" name="critica" type="checkbox" <?php if(isset($a->critica) && $a->critica=="1") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <button style="margin-top: 35px;" id="btn_sub" class="btn btn-primary" type="button">Guardar actividad</button>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <a href="<?php echo base_url()?>Actividades/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
            </div>
          </div>  
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>
