<style type="text/css">
  #table_activ th, #table_activ td{
    font-size: 13px;
  }
</style>
<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Actividades</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
            <input type="hidden" id="id_cli" value="<?php echo $id_cli;?>">
            <div class="col-12">
              <a href="<?php echo base_url()?>Proyectos/ejecucion/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <a class="btn btn-primary" href="<?php echo base_url() ?>Actividades/registro/0/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Nueva actividad</a>

              <!--<a class="btn btn-primary" href="<?php echo base_url() ?>Actividades/nvaPlantilla"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar Plantilla</a>
              <button id="upload_pre" class="btn btn-primary" type="button"><i class="fa fa-upload" aria-hidden="true"></i> Cargar Plantilla</button>-->
            </div>
            <div class="col-sm-4"><br></div>
            <div class="mb-3 row">
              <div class="col-sm-3">
                <label>Tipo de actividad</label>
                <select class="form-control" id="critica">
                  <option value="0">Normal</option>
                  <option value="1">Crítica</option>
                </select>
              </div>
            </div>
            <div class="col-12">
              <br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_activ">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Zona</th>
                      <th scope="col">Actividad</th>
                      <th scope="col"># de orden</th>
                      <th scope="col">Frecuencia</th>
                      <!--<th scope="col">Frecuencia modificada 1</th>
                      <th scope="col">Frecuencia modificada 2</th>
                      <th scope="col">Propuesta cambio de frecuencia</th>-->
                      <th scope="col">Recursos necesarios</th>
                      <th scope="col">Tiempo</th>
                      <th scope="col">Horas hombre</th>
                      <th scope="col">Unidades de frecuencia</th>
                      <th scope="col">Grupo propietario</th>
                      <th scope="col">Fecha de inicio</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" id="id_ord">
        <h5 class="modal-title" id="exampleModalLabel">Dar Seguimiento</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_activ">
        <div class="mb-3 row">
          <div class="col-sm-12">
            <label>Estatus</label>
            <select class="form-control" id="estatus">
              <option value="1">Cancelado</option>
              <!--<option value="2">Programado</option>-->
              <option value="3">OK</option>
              <option value="4">Reprogramado</option>
              <option value="4">No realizado</option>
            </select>
          </div>
        </div>
        <div class="col-sm-12">
          <label>Fecha que realiza actividad</label>
          <input class="form-control" type="date" id="fecha_realiza">
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-secondary" type="button" id="edit_status">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalCarga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Cargar malla de datos</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" >
        <div class="mb-3 row">
          <div class="col-sm-12">
            <label>Generar Orden por actividad</label>
            <select class="form-control" id="genera_orden">
              <option value="1">Si</option>
              <option value="2">No</option>
            </select>
          </div>
        </div>
        <div class="row carga_malla">
          <div class="col-md-12">
            <form id="form_carga_malla" method="POST" enctype="multipart/form-data">
              <div class="col-md-8">
                  <div class="form-group">
                      <label>Seleccione un csv</label>
                      <input type="file" name="inputFile" id="inputFile" class="form-control" accept=".csv"> 
                  </div>
               </div>
              <div class="col-md-6">
                <div class="form-group">
                  <button class="btn btn-secondary" type="button" id="aceptar_carga"><i class="fa fa-upload"></i> Cargar Malla</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalfiles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Cargar De archivos</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" >
        <div class="row">
          <!--<input type="hidden" id="actividad">-->
          <input type="file" id="files" name="files[]" multiple>

        </div>
        <div class="row">
          <div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <td>Archivo</td>
                  <td>Fecha</td>
                  <td></td>
                </tr>
              </thead>
              <tbody class="tbody_view_files">
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>