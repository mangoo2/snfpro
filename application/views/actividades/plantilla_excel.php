<?php
	header("Content-Type: text/html;charset=UTF-8");
	header("Pragma: public");
	header("Expires:0");
	header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Type: application/vnd.ms-excel;");
	header("Content-Disposition: attachment; filename=plantilla_excel".date("Ymd").".xls");

?>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<table border="1">
		<thead>
			<tr>
				<th scope="col">Nombre de actividad</th>
				<th scope="col">Tipo de actividad (0=Actividades de hombres y mujeres, 1=Actividades exclusivas de hombres)</th>
				<th scope="col">Zona</th>
				<th scope="col"># de orden de trabajo</th>
				<th scope="col">Frecuencia (1=Semanal,2=Quincenal,3=Mensual,4=Bimestral5=Anual)</th>
				<th scope="col">Recursos necesarios</th>
				<th scope="col">Tiempo</th>
				<th scope="col">Horas hombre</th>
				<th scope="col">Unidades de frecuencia</th>
				<th scope="col">Grupo propietario</th>
				<th scope="col">Fecha de inicio (AAAA-MM-DD)</th>
				<th scope="col">Fecha fin (AAAA-MM-DD)</th>
				<!--<th scope="col">Fecha que realiza actividad (AAAA-MM-DD)</th>-->
				<th scope="col">Estatus de Seguimiento (1=Cancelado,2=Programado,3=OK,4=Reprogramado,5=no realizado)</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
			</tr>
		</tbody>
	</table>