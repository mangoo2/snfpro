<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Actividades Críticas</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
            <input type="hidden" id="id_cli" value="<?php echo $id_cli;?>">
            <div class="col-12">
              <a href="<?php echo base_url()?>Proyectos/limpieza/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <a class="btn btn-primary" href="<?php echo base_url() ?>Actividades/registroCritica/0/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Nueva actividad</a>
            </div>
            <div class="col-12">
              <br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <div id="table_activ_crit2"></div>
                <table class="table" id="table_activ_crit">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Zona</th>
                      <th scope="col">Actividad</th>
                      <th scope="col">Gente</th>
                      <th scope="col">Horas</th>
                      <th scope="col">Frecuencia</th>
                      <th scope="col">Turno</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
