<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> Actividades Críticas</h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>Actividades/listadoCriticas/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Lista de actividades</a>
      </div>
      
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="id_proyecto" id="id_proy" value="<?php echo $id_proy;?>">
          <input type="hidden" name="id_cliente" id="id_cli" value="<?php echo $id_cli;?>">
          <input type="hidden" name="id" id="id" value="<?php if(isset($a)) echo $a->id; else echo '0' ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Nombre</label>
                  <div class="col-sm-9">
                    <input placeholder="Nombre de la actividad" class="form-control" type="text" name="nombre" value="<?php if(isset($a)) echo $a->nombre; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <label class="col-sm-2 col-form-label">Zona</label>
                  <div class="col-sm-9">
                    <input placeholder="Zona de la actividad" class="form-control" type="text" name="zona" value="<?php if(isset($a)) echo $a->zona; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-2">
                    <label>Gente</label>
                    <input class="form-control hgt_inp" type="text" name="gente" value="<?php if(isset($a)) echo $a->gente; ?>">
                  </div>
                  <div class="col-sm-2">
                    <label>Horas</label>
                    <input class="form-control hgt_inp" type="text" name="horas" value="<?php if(isset($a)) echo $a->horas; ?>">
                  </div>
                  <div class="col-sm-2">
                    <label>Frecuencia</label>
                    <input class="form-control hgt_inp" type="text" name="frecuencia" value="<?php if(isset($a)) echo $a->frecuencia; ?>">
                  </div>
                  <div class="col-sm-2">
                    <label>Turno</label>
                    <?php 
                      $turno_num=0;
                      if(isset($a)){
                        $turno_num=$a->turno;
                      }
                    ?>
                    <select class="form-control" name="turno">
                      <option selected disabled>Selecciona una opción</option>
                      <option value="1" <?php if($turno_num==1) echo 'selected' ?>>1°</option>
                      <option value="2" <?php if($turno_num==2) echo 'selected' ?>>2°</option>
                      <option value="3" <?php if($turno_num==3) echo 'selected' ?>>3°</option>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <button style="margin-top: 35px;" id="btn_sub" class="btn btn-primary" type="submit">Guardar actividad</button>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <a href="<?php echo base_url()?>Actividades/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
            </div>
          </div>  
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>