<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=monitoreo.xls");

function redimensionar($src,$name){
    $imagen = $src; //Imagen original
    $imagenNueva = FCPATH."uploads/monitoreo/2_".$name; //Nueva imagen
    $nAncho = 105; //Nuevo ancho
    $nAlto = 75;  //Nuevo alto
    
    //Creamos una nueva imagen a partir del fichero inicial
    if(pathinfo($name,PATHINFO_EXTENSION)=="jpg" || pathinfo($name,PATHINFO_EXTENSION)=="jpeg" || pathinfo($name,PATHINFO_EXTENSION)=="JPG" || pathinfo($name,PATHINFO_EXTENSION)=="JPEG")
        $imagen = imagecreatefromjpeg($imagen); 
    else if(pathinfo($name,PATHINFO_EXTENSION)=="png" || pathinfo($name,PATHINFO_EXTENSION)=="PNG")
        $imagen = imagecreatefrompng($imagen); 

    //Obtenemos el tamaño 
    $x = imagesx($imagen);
    $y = imagesy($imagen);
    
    // Crear una nueva imagen, copia y cambia el tamaño de la imagen
    $img = imagecreatetruecolor($nAncho, $nAlto);
    imagecopyresized($img, $imagen, 0, 0, 0, 0, $nAncho, $nAlto, $x, $y);
    
    //Creamos el archivo jpg
    imagejpeg($img, $imagenNueva);
    return "2_".$name;
}

?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<style>
    .table-striped tbody tr:nth-of-type(odd) {
        background-color: rgb(255 18 18 / 5%);
    }
    .style_head{
        color:black; font-size: 10px; text-align: center; background-color: #BFBFBF;
    }
    .pspaces{ font-size: 0.2px; }
</style>

    <h3>Listado de Monitoreo</h3> 
    <table width="100%" border="1" RULES="rows" style="padding: 5px; text-align:center;" class="table table-striped"> 
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Zona</th>
          <th scope="col">Inspección</th>
          <th scope="col">Actividad</th>
          <th scope="col">Frecuencia</th>
          <th scope="col">Mes</th>
          <th scope="col">Fecha</th>
          <th scope="col">Seguimiento</th>
          <th scope="col">Observaciones</th>
          <th colspan="2">Foto</th>
        </tr>
      </thead>
    <?php 
    foreach ($get_result as $i){
        $seguimiento='';
        if($i->seguimiento=="0")
            $seguimiento="No completado";
        else if($i->seguimiento=="1")
            $seguimiento="completado";
        
        $mesl='';
        if($i->mes=='01'){
            $mesl='Enero';
        }else if($i->mes=='02'){
            $mesl='Febrero';
        }else if($i->mes=='03'){
            $mesl='Marzo';
        }else if($i->mes=='04'){
            $mesl='Abril';
        }else if($i->mes=='05'){
            $mesl='Mayo';
        }else if($i->mes=='06'){
            $mesl='Junio';
        }else if($i->mes=='07'){
            $mesl='Julio';
        }else if($i->mes=='08'){
            $mesl='Agosto';
        }else if($i->mes=='09'){
            $mesl='Septiembre';
        }else if($i->mes=='10'){
            $mesl='Octubre';
        }else if($i->mes=='11'){
            $mesl='Noviembre';
        }else if($i->mes=='12'){
            $mesl='Diciembre';
        }
        if($i->foto!=""){
            $imagen = base_url()."uploads/monitoreo/".$i->foto;
            $img_med = redimensionar($imagen, $i->foto);
            $img='<img src="'.base_url()."uploads/monitoreo/".$img_med.'" />';
        }else{
            $img='';
        }
        echo '<tr> 
            <td style="text-align: left;">'.$i->id.'</td> 
            <td style="text-align: left;">'.$i->zona.'</td>
            <td style="text-align: left;">'.$i->inspeccion.'</td>
            <td style="text-align: left;">'.$i->nombre.'</td>  
            <td style="text-align: left;">'.$i->frecuencia.'</td>
            <td style="text-align: left;">'.$mesl.'</td>
            <td style="text-align: left;">'.$i->fecha.'</td>
            <td style="text-align: left;">'.$seguimiento.'</td>
            <td style="text-align: left;">'.$i->observaciones.'</td>
            <td colspan="2" style="align-items: center; justify-content: center"><p class="pspaces"></p>
                <table>
                    <tr> <td colspan="3" width="100%"></td></tr>
                    <tr>
                        <td width="10%"></td>
                        <td width="80%">'.$img.'</td>
                        <td width="10%"></td>
                    </tr>
                </table><br><br><br><br>
            </td>
        </tr>';
    }  
    
    ?>
    </table>