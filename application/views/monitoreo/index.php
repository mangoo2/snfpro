<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de Monitoreo</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <input type="hidden" id="id_proy" value="<?php echo $id_proy;?>">
            <input type="hidden" id="id_cli" value="<?php echo $id_cli;?>">
            <div class="col-12">
              <a href="<?php echo base_url()?>Proyectos/ejecucion/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <a class="btn btn-primary" href="<?php echo base_url() ?>Monitoreo/registro/0/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Nuevo registro</a>
              <button class="btn btn-light" onclick="excel_exportar()"><i class="fa fa-file-excel-o" style="font-size: 20px;" ></i> Excel</button>
              <div class="col-sm-4"><br></div>
              
              <div class="mb-3 row">
                <div class="col-sm-4">
                  <label>Tipo de registro</label>
                  <select class="form-control" id="tipo">
                    <option <?php if($this->session->userdata("tipo_reg_monitor")==2) echo "selected"; ?> value="2">Todo</option>
                    <option <?php if($this->session->userdata("tipo_reg_monitor")==0) echo "selected"; ?> value="0">Diario (L-S)</option>
                    <option <?php if($this->session->userdata("tipo_reg_monitor")==1) echo "selected"; ?> value="1">Semanal (D)</option>
                  </select>
                </div>
                <div class="col-sm-4">
                  <label>Actividad</label>
                  <select class="form-control" name="id_limpieza" id="id_limpieza">
                    <?php foreach ($act as $ac){ ?>
                      <option value="<?php echo $ac->id ?>" <?php if($this->session->userdata("activ_monitor")==$ac->id) echo 'selected' ?>><?php echo $ac->nombre ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-sm-4">
                  <label>Semana</label>
                  <select class="form-control" id="semana">
                    <?php function getIsoWeeksInYear($año) { $date = new DateTime; $date->setISODate($año, 53); return ($date->format("W") === "53" ? 53 : 52); }
                      for($i=1;$i<=getIsoWeeksInYear(date("Y"));$i++){ ?>
                        <option value="<?php echo $i; ?>" <?php if(date('W')==$i) echo "selected"; ?>><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
                </div>

              </div>
            </div>
            <div class="col-12">
              <br>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_activ">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Zona</th>
                      <th scope="col">Inspección</th>
                      <th scope="col">Actividad</th>
                      <th scope="col">Frecuencia</th>
                      <th scope="col">Mes</th>
                      <th scope="col">Fecha</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" id="id_ord">
        <h5 class="modal-title" id="exampleModalLabel">Dar Seguimiento</h5>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_mon">
        <div class="mb-3 row">
          <div class="col-sm-12">
            <label>Estatus</label>
            <select class="form-control" id="estatus">
              <option value="" disabled selected></option>
              <option value="1">1 - Completo</option>
              <option value="0">0 - No Completo</option>
              <option value="2">2 -  N/A</option>
            </select>
          </div>
        </div>
        <div class="mb-3 row">
          <div class="col-sm-12">
            <label>Observaciones</label>
            <input class="form-control" type="text" id="observaciones" name="observaciones">
          </div>
        </div>
        <div class="mb-3 row">
          <div class="col-sm-12">
            <label>Inspección</label>
            <input class="form-control" type="text" id="inspeccion" name="inspeccion">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-bs-dismiss="modal">Close</button>
        <button class="btn btn-secondary" type="button" id="edit_status">Aceptar</button>
      </div>
    </div>
  </div>
</div>