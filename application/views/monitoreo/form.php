<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3><?php echo $tittle; ?> monitoreo</h3>
      </div>
      <div class="col-sm-6" style="text-align: end;">
        <a class="btn btn-primary" href="<?php echo base_url() ?>Monitoreo/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>">Lista de monitoreo</a>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" id="fecha_ini" value="<?php echo date("Y");?>-01-01">
          <input type="hidden" id="fecha_fin" value="<?php echo date("Y");?>-12-31">
          <input type="hidden" id="fecha_aux" value="<?php if(isset($a)) echo $a->fecha; ?>">
          <input type="hidden" name="id_proyecto" id="id_proy" value="<?php echo $id_proy;?>">
          <input type="hidden" name="id_cliente" id="id_cli" value="<?php echo $id_cli;?>">
          <input type="hidden" name="id" id="id" value="<?php if(isset($a)) echo $a->id; else echo '0' ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3" style="display: none;">
                  <div class="col-md-3">
                    <center><label class="col-form-label m-r-10">Tipo de registro</label></center>
                  </div>
                </div>
                <div class="mb-3 row" style="display: none;">
                  <div class="col-md-3">
                    <label class="col-form-label m-r-10">Diario (L-S)</label>
                    <div class="col-md-2">
                      <label class="switch">
                        <input class="tipo" id="tipo1" name="tipo" type="radio" <?php if(isset($a->tipo) && $a->tipo=="0") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <label class="col-form-label m-r-10">Semanal (D)</label>
                    <div class="col-md-3">
                      <label class="switch">
                        <input class="tipo" id="tipo2" name="tipo" type="radio" <?php if(isset($a->tipo) && $a->tipo=="1") echo "checked"; ?>>
                        <span class="sliderN round"></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-4">
                    <label>Actividad</label>
                    <select class="form-control" name="id_limpieza" id="id_limpieza">
                      <?php foreach ($act as $ac){ ?>
                        <option data-zona="<?php echo $ac->zona; ?>" data-frecuencia="<?php echo $ac->frecuencia_cotiza; ?>" data-mes_ini="<?php echo $ac->mes; ?>" value="<?php echo $ac->id ?>" <?php if(isset($a) && $ac->id==$a->id_limpieza) echo 'selected' ?>><?php echo $ac->nombre ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-4">
                    <label>Zona</label>
                    <input class="form-control" readonly type="text" id="zona" name="zona" value="<?php if(isset($a)) echo $a->zona; ?>">
                  </div>
                  <div class="col-sm-4">
                    <label>Inspección</label>
                    <input class="form-control" type="text" id="inspeccion" name="inspeccion" value="<?php if(isset($a)) echo $a->inspeccion; ?>">
                  </div>
                  <div class="col-sm-4">
                    <label>Frecuencia (# de veces en el día)</label>
                    <!--<input class="form-control" type="text" id="frecuencia" name="frecuencia" value="<?php if(isset($a)) echo $a->frecuencia; ?>">-->
                    <select class="form-control" name="frecuencia">
                      <option selected disabled>Selecciona una opción</option>
                      <option class="ds" value="1" <?php if(isset($a) && $a->frecuencia==1) echo 'selected' ?>>1</option>
                      <option class="ds" value="2" <?php if(isset($a) && $a->frecuencia==2) echo 'selected' ?>>2</option>
                      <option class="ds" value="3" <?php if(isset($a) && $a->frecuencia==3) echo 'selected' ?>>3</option>
                      <option class="ds" value="4" <?php if(isset($a) && $a->frecuencia==4) echo 'selected' ?>>4</option>
                      <option class="d" value="5" <?php if(isset($a) && $a->frecuencia==5) echo 'selected' ?>>5</option>
                      <option class="d" value="6" <?php if(isset($a) && $a->frecuencia==6) echo 'selected' ?>>6</option>
                    </select>
                  </div>
                </div>

                <div class="mb-3 row">
                  <div class="col-sm-4">
                    <label>Tipo Frecuencia</label>
                    <select class="form-control" name="frecuencia_tipo">
                      <option selected disabled>Selecciona una opción</option>
                      <option value="1" <?php if(isset($a) && $a->frecuencia_tipo==1) echo 'selected' ?>>Semanal</option>
                      <!--<option value="2" <?php if(isset($a) && $a->frecuencia_tipo==2) echo 'selected' ?>>Quincenal</option>
                      <option value="3" <?php if(isset($a) && $a->frecuencia_tipo==3) echo 'selected' ?>>Mensual</option>-->
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <label># de punto(s) de inspección(es)</label>
                    <input class="form-control" type="number" min="1" id="num_inspeccion" name="num_inspeccion" value="<?php if(isset($a)) echo $a->num_inspeccion; ?>">
                  </div>
                </div>
                <div class="mb-3 row">
                  <div class="col-sm-4">
                    <label>Fecha Inicio</label>
                    <input class="form-control" type="date" id="fecha" name="fecha" value="<?php if(isset($a)) echo $a->fecha; ?>">
                  </div>
                  <div class="col-sm-4">
                    <label>Fecha Fin</label>
                    <input class="form-control" <?php if(isset($a)) echo "readonly"; ?> type="date" id="fecha_fin" name="fecha_fin" value="<?php if(isset($a)) echo $a->fecha; ?>">
                  </div>
                  <div class="col-sm-4">
                    <label>Día(s)</label>
                    <select <?php if(isset($a)) echo "readonly"; ?> size="6" style="height: 100%;" class="form-control" name="dia_activ[]" id="dia_activ" multiple="multiple">
                      <!--<option class="di" value="Monday">Lunes</option>
                      <option class="di" value="Tuesday">Martes</option>
                      <option class="di" value="Wednesday">Miércoles</option>
                      <option class="di" value="Thursday">Jueves</option>
                      <option class="di" value="Friday">Viernes</option>
                      <option class="di" value="Saturday">Sabado</option>
                      <option class="fin" value="Sunday">Domingo</option>-->
                      <option class="di" <?php if(isset($a) && $dia_activ_name=="Monday") echo "selected"; ?> value="1">Lunes</option>
                      <option class="di" <?php if(isset($a) && $dia_activ_name=="Tuesday") echo "selected"; ?> value="2">Martes</option>
                      <option class="di" <?php if(isset($a) && $dia_activ_name=="Wednesday") echo "selected"; ?> value="3">Miércoles</option>
                      <option class="di" <?php if(isset($a) && $dia_activ_name=="Thursday") echo "selected"; ?> value="4">Jueves</option>
                      <option class="di" <?php if(isset($a) && $dia_activ_name=="Friday") echo "selected"; ?> value="5">Viernes</option>
                      <option class="di" <?php if(isset($a) && $dia_activ_name=="Saturday") echo "selected"; ?> value="6">Sábado</option>
                      <option class="fin" <?php if(isset($a) && $dia_activ_name=="Sunday") echo "selected"; ?> value="7">Domingo</option>
                    </select>
                  </div>
                </div>
                <div class="mb-3 row">
                  
                </div>

                <div class="mb-3 row">
                  <div class="col-md-4">
                    <div class="item">
                      <center><label>Foto</label></center>
                      <div class="card" align="center">
                        <span class="img_foto_cont">
                          <?php if(isset($a) && $a->foto!=''){ ?>
                            <img id="img_foto" src="<?php echo base_url(); ?>uploads/monitoreo/<?php echo $a->foto; ?>" class="rounded mr-3 img-fluid" height="142" width="142">
                            <input type="hidden" id="name_foto" value="<?php echo $a->foto; ?>">
                          <?php }else{ ?>
                            <img id="img_foto" src="<?php echo base_url(); ?>public/img/no-image.png" class="rounded mr-3 img-fluid" height="142" width="142">
                          <?php } ?>
                        </span>
                      </div>
                      <label class="btn btn-light" for="foto" style="width: 100%">Cargar foto</label>
                      <input type="file" id="foto" hidden>
                      <br>
                      <a class="btn btn-light" style="width: 100%" onclick="reset_img()">Eliminar</a>
                      <br>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-sm-9">
              <a href="<?php echo base_url()?>Monitoreo/listado/<?php echo $id_proy; ?>/<?php echo $id_cli; ?>" class="btn btn-light">Regresar</a>
              <button id="btn_sub" class="btn btn-primary" type="button">Guardar actividad</button>
            </div>
          </div>  
        </form>  
        <!---->
      </div>
    </div>
  </div>
</div>
