<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-7"><img class="bg-img-cover bg-center" src="<?php echo base_url();?>public/img/logoin2.jpeg" alt="looginpage"></div>
      <div class="col-xl-5 p-0">
        <div class="login-card">
            <div class="theme-form">
                <div align="center">
                    <img style="width: 150px;" src="<?php echo base_url();?>public/img/logofinalsys.png">
                </div><br>
                <div class="theme-form login-form">
                  <form id="login-form" action="#" role="form">
                  <h4>Inicio de sesión</h4>
                  <h6>Bienvenido de nuevo. Por favor inicia sesión</h6>
                  <div class="form-group">
                    <label>Correo electrónico</label>
                    <div class="input-group"><span class="input-group-text"><i class="icon-email"></i></span>
                      <input class="form-control" type="text" name="txtUsuario" id="txtUsuario" required autofocus>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Contraseña</label>
                    <div class="input-group"><span class="input-group-text"><i class="icon-lock"></i></span>
                      <input class="form-control" type="password" name="" name="txtPass" id="txtPass" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <button class="btn btn-blue btn-block" type="submit" id="login-submit">Iniciar</button>
                  </div>
                 </form>
                 <div class="rounded-pill pill-badge-primary" id="success" style="font-size: 15px !important; text-align: center; display: none">
                    <b>Acceso Correcto!</b> Será redirigido al sistema 
                  </div>

                  <div class="rounded-pill pill-badge-danger" id="error" style="font-size: 15px !important; text-align: center; display: none">
                    <b>Error!</b> El nombre de usuario y/o contraseña son incorrectos 
                  </div>
                </div>
            </div>   
        </div>
      </div>
    </div>
  </div>
</section>
