    <!-- page-wrapper end-->
    <!-- latest jquery-->
    <script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-3.5.1.min.js"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url();?>assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->

    <script src="<?php echo base_url();?>assets/js/config.js"></script>
    <!-- Bootstrap js-->
    
    <script src="<?php echo base_url();?>assets/js/bootstrap/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap/bootstrap.min.js"></script>
    <!-- Plugins JS start-->
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?php echo base_url();?>assets/js/script.js"></script>
    <script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>
    <!-- login js-->
    <!-- Plugin used-->
  </body>
</html>