<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Nuevo empleado</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <!---->
        <form class="form" method="post" role="form" id="form_data">
          <input type="hidden" name="personalId" id="personalId" value="<?php echo $personalId ?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="mb-3 row">
                  <div class="col-md-3">
                    <div class="item">
                      <label>Fotografía</label>
                      <div class="card" align="center">
                        <span class="img_empleado">
                          <?php if($foto!=''){ ?>
                              <img id="img_avatar" src="<?php echo base_url(); ?>uploads/empleados/<?php echo $foto ?>" class="rounded mr-3" height="142" width="142">
                          <?php }else{ ?>
                              <img id="img_avatar" src="<?php echo base_url(); ?>public/img/avatar.png" class="rounded mr-3" height="142" width="142">
                          <?php } ?>
                        </span>
                      </div>
                    <label class="btn btn-light" for="foto_avatar" style="width:auto">Cargar foto</label>
                    <input type="file" id="foto_avatar" hidden>
                    <br>
                    <a class="btn btn-light" style="width: auto" onclick="reset_img()">Eliminar</a>
                    <br>
                    </div>
                  </div>   
                  <div class="col-md-9">
                  <!-- -->
                    <div class="mb-3 row">
                      <label class="col-sm-3 col-form-label">Nombre completo</label>
                      <div class="col-sm-9">
                        <input class="form-control" type="text" name="nombre" value="<?php echo $nombre ?>">
                      </div>
                    </div>
                    <div class="mb-3 row">
                      <label class="col-sm-3 col-form-label">Teléfono</label>
                      <div class="col-sm-9">
                        <input class="form-control" type="text" name="celular" value="<?php echo $celular ?>">
                      </div>
                    </div>
                    <div class="mb-3 row">
                      <label class="col-sm-3 col-form-label">Email</label>
                      <div class="col-sm-9">
                        <input class="form-control" type="text" name="correo" value="<?php echo $correo ?>">
                      </div>
                    </div>
                    <div class="mb-3 row">
                      <div class="col-sm-4">
                        <div class="media">
                          <label class="col-form-label m-r-10">Hombre</label>
                          <div class="media-body text-end">
                            <label class="switch">
                              <input type="radio" id="hombre" name="genero" <?php if($genero=="1") echo "checked"; ?>><span class="switch-state"></span>
                            </label>
                          </div>
                        </div>
                      </div>  
                      <div class="col-sm-2"></div>
                      <div class="col-sm-4">
                        <div class="media">
                          <label class="col-form-label m-r-10">Mujer</label>
                          <div class="media-body text-center">
                            <label class="switch">
                              <input type="radio" id="mujer" name="genero" <?php if($genero=="2") echo "checked"; ?>><span class="switch-state"></span>
                            </label>
                          </div>
                        </div>
                      </div>  
                    </div> 
                    <div class="mb-3 row">
                      <label class="col-sm-3 col-form-label">Seleccione perfil</label>
                      <div class="col-sm-9">
                        <select class="form-control" name="perfilId" id="perfilId">
                          <option value="0" disabled="" selected=""></option>
                          <?php foreach ($resultperfil as $x){ ?>
                            <option value="<?php echo $x->perfilId ?>" <?php if($x->perfilId==$perfilId) echo 'selected' ?>><?php echo $x->nombre ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="mb-3 row">
                      <label class="col-sm-3 col-form-label">Puesto</label>
                      <div class="col-md-7">
                        <!--<input class="form-control" type="text" name="puesto" value="<?php echo $puesto ?>">-->
                        <select class="form-control" name="puesto" id="puesto">
                          <?php foreach ($puestos as $p){ ?>
                            <option <?php if($p->id==$puesto) echo "selected"; ?> value="<?php echo $p->id; ?>"><?php echo $p->puesto; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-md-2">
                        <button class="btn btn-primary" title="Agregar nuevo puesto" type="button" id="addPuesto"><i class="fa fa-plus" aria-hidden="true"></i></button>
                      </div>
                    </div>
                    <div class="mb-3 row">
                      <label class="col-sm-3 col-form-label">Empresa/Cuenta</label>
                      <div class="col-sm-9">
                        <select class="form-control" name="empresa" id="empresa">
                          <?php 
                            foreach ($cli as $c) {
                              if($this->session->userdata("usuarioid_tz")==1){ //super administrador
                                if($empresa==$c->id) $sel="selected"; else $sel="";
                                echo "<option ".$sel." value='".$c->id."'>".$c->empresa."</option>";
                              }else{
                                if($c->id==$this->session->userdata("empresa")){
                                  echo "<option value='".$c->id."'>".$c->empresa."</option>";
                                }
                              }
                          } ?>     
                        </select>
                      </div>
                    </div>
                  <!-- -->  
                  </div>
                </div> 
                <?php
                  $style_user='';
                  $checked_user=''; 
                  if($check_acceso=='on'){
                    $style_user=''; 
                    $checked_user='checked';
                  }else{
                    $style_user='style="display: none"'; 
                    $checked_user='';
                  } 
                ?>
                <div class="mb-3 row">
                  <div class="col-3">
                    <div class="media">
                        <label class="col-form-label m-r-10">Acceso a plataforma</label>
                        <div class="media-body text-end">
                          <label class="switch">
                            <input type="checkbox" id="check_acceso" name="check_acceso" onclick="btn_check_acceso()" <?php echo $checked_user ?>><span class="switch-state"></span>
                          </label>
                        </div>
                      </div>
                  </div>  
                </div> 
                <div class="text_usuario" <?php echo $style_user ?>>
                  <div class="mb-3 row">
                    <div class="col-9">
                      <div class="mb-3 row">
                        <input type="hidden" id="UsuarioID" value="<?php echo $UsuarioID ?>">
                        <label class="col-sm-3 col-form-label">Correo electrónico</label>
                        <div class="col-sm-5">
                          <input class="form-control" type="text" id="Usuario" name="Usuario" value="<?php echo $Usuario ?>">
                        </div>
                        <div class="col-4">
                          <a class="btn btn-primary" style="width: 100%">Generar contraseña</a>
                        </div>
                      </div>
                      <div class="mb-3 row">
                        <div class="col-3">
                          <label class="col-form-label">Contraseña</label>
                          <input class="form-control" type="password" autocomplete="new-password" name="contrasena" id="contrasena" aria-autocomplete="list" value="<?php echo $contrasena ?>">
                        </div>
                        <div class="col-3">
                          <label class="col-form-label">Confirmar contraseña</label>
                          <input class="form-control" type="password" name="contrasena2" id="contrasena2" value="<?php echo $contrasena ?>">
                        </div>   
                      </div>
                    </div>
                    <div class="col-3">
                      <div class="item">
                        <label>Firma</label>
                        <div class="card" align="center">
                          <span class="img_firma">
                            <?php if($firma!=''){ ?>
                              <img id="img_firma_empleado" src="<?php echo base_url(); ?>uploads/firma/<?php echo $firma ?>" class="rounded mr-3" height="142" width="142">
                            <?php }else{ ?>
                              <img id="img_firma_empleado" class="rounded mr-3" height="142" width="142">
                            <?php } ?>
                          </span>
                        </div>
                        <label class="btn btn-light" for="foto_firma" style="width: 100%">Cargar firma</label>
                      <input type="file" id="foto_firma" hidden>
                      <br>
                      <a class="btn btn-light" style="width: 100%" onclick="reset_img_firma()">Eliminar</a>
                      <br>
                      </div> 
                    </div>  
                  </div>      
                </div>
              </div>
            </div>
          </div>
        </form>  
          <div class="card-footer">
            <div class="col-sm-9">
              <button class="btn btn-primary" type="button" onclick="add_form()">Guardar datos</button>
              <a href="<?php echo base_url()?>Empleados" class="btn btn-light">Regresar</a>
            </div>
          </div>
        <!---->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalPuestos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Agregar puesto</h4>
        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_puesto" value="0">
        <div class="col-12">
          <label class="col-form-label">Puesto</label>
          <input class="form-control" type="text" id="puesto_modal" value="">
        </div>
        <div class="col-md-12">
          <table class="table" id="table_puesto">
            <thead>
              <tr>
                <td>Puesto</td>
                <td>Acciones</td>
              </tr>
            </thead>
            <tbody id="body_tablep">
              
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="savePuesto">Guardar</button>
        <button type="button" class="btn grey btn-outline-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>