<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-sm-6">
        <h3>Listado de empleados</h3>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              
            </div>
            <div class="mb-3 row">
              <div class="col-sm-3">
                <a class="btn btn-primary" href="<?php echo base_url() ?>Empleados/registro">Nuevo empleado</a>
              </div>    
              <?php if($this->session->userdata("usuarioid_tz")==1){ //super administrador ?>
                <div class="col-sm-5">
                  <label class="col-sm-6 col-form-label">Empresa / Cuenta</label>
                  <select class="form-control" id="empresa">
                    <option selected value="0">Todos</option>
                    <?php foreach ($cli as $c) {
                      if($s==$c->id) $sel="selected"; else $sel="";
                      echo "<option ".$sel." value='".$c->id."'>".$c->empresa."</option>";
                    } ?>
                  </select>
                </div>
              <?php } ?>
            </div>
            <div class="col-12">
              <div class="table-responsive">
                <table class="table" id="table_data">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Logo</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Puesto</th>
                      <th scope="col">Perfil</th>
                      <th scope="col">Email</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

