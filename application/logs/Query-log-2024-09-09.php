SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-8-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 8
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.28781604766846

SELECT DISTINCT(os.id), `la`.`nombre` as `actividad`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, WEEKOFYEAR(os.fecha) AS num_semana, `os`.`ot`, `os`.`observaciones`, `os`.`seguimiento`
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 8
AND DATE_FORMAT(os.fecha, '%Y') = 2024
AND (`os`.`seguimiento` = 1 or `os`.`seguimiento` = 4 or `os`.`seguimiento` = 5)
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.37276601791382

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.24302196502686

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 1 
 Execution Time:0.26500606536865

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 3 
 Execution Time:0.22712302207947

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 4 
 Execution Time:0.27607393264771

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 5 
 Execution Time:0.1492760181427

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.16148209571838

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 1 
 Execution Time:0.14024186134338

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 3 
 Execution Time:0.31752586364746

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 4 
 Execution Time:0.17621278762817

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 5 
 Execution Time:0.14478087425232

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12' 
 Execution Time:0.14028096199036

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 1 
 Execution Time:0.20287084579468

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 3 
 Execution Time:0.18141794204712

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 4 
 Execution Time:0.15531396865845

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 5 
 Execution Time:0.15365600585938

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13' 
 Execution Time:0.14825797080994

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 1 
 Execution Time:0.15078401565552

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 3 
 Execution Time:0.3782331943512

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 4 
 Execution Time:0.14429593086243

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 5 
 Execution Time:0.166659116745

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19' 
 Execution Time:0.14805293083191

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 1 
 Execution Time:0.17318797111511

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 3 
 Execution Time:0.16609001159668

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 4 
 Execution Time:0.17167592048645

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 5 
 Execution Time:0.15545916557312

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20' 
 Execution Time:0.15250205993652

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 1 
 Execution Time:0.16169881820679

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 3 
 Execution Time:0.20262217521667

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 4 
 Execution Time:0.1668848991394

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 5 
 Execution Time:0.14512300491333

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26' 
 Execution Time:0.14006686210632

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 1 
 Execution Time:0.16377806663513

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 3 
 Execution Time:0.14274907112122

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 4 
 Execution Time:0.15081810951233

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 5 
 Execution Time:0.15100193023682

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27' 
 Execution Time:0.15813589096069

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 1 
 Execution Time:0.14729404449463

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 3 
 Execution Time:0.16339182853699

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 4 
 Execution Time:0.15960907936096

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 5 
 Execution Time:0.14985489845276

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28' 
 Execution Time:0.28437900543213

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 1 
 Execution Time:0.15446496009827

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 3 
 Execution Time:0.16276812553406

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 4 
 Execution Time:0.14197087287903

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 5 
 Execution Time:0.16002893447876

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29' 
 Execution Time:0.17443609237671

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 1 
 Execution Time:0.15950989723206

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 3 
 Execution Time:0.15895509719849

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 4 
 Execution Time:0.1542489528656

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 5 
 Execution Time:0.15467000007629

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30' 
 Execution Time:0.16170716285706

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 1 
 Execution Time:0.29207897186279

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 3 
 Execution Time:0.17381405830383

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 4 
 Execution Time:0.19327306747437

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 5 
 Execution Time:0.1554741859436

SELECT *
FROM `proyectos`
WHERE `id` = '36' 
 Execution Time:0.10916519165039

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-08-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 08
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.1827700138092

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.19685482978821

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.16706800460815

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 1 
 Execution Time:0.17901301383972

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 3 
 Execution Time:0.17449688911438

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 4 
 Execution Time:0.14969491958618

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 5 
 Execution Time:0.16237092018127

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.22719192504883

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.17545104026794

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 1 
 Execution Time:0.15314602851868

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 3 
 Execution Time:0.21398115158081

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 4 
 Execution Time:0.20850419998169

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 5 
 Execution Time:0.16131806373596

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12' 
 Execution Time:0.17270398139954

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.14643001556396

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 1 
 Execution Time:0.13929414749146

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 3 
 Execution Time:0.14427304267883

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 4 
 Execution Time:0.16618514060974

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 5 
 Execution Time:0.17698097229004

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13' 
 Execution Time:0.1964430809021

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.15252280235291

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 1 
 Execution Time:0.16372609138489

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 3 
 Execution Time:0.14500188827515

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 4 
 Execution Time:0.2575569152832

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 5 
 Execution Time:0.21756100654602

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19' 
 Execution Time:0.14942812919617

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.15503287315369

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 1 
 Execution Time:0.14969205856323

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 3 
 Execution Time:0.15304112434387

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 4 
 Execution Time:0.1664571762085

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 5 
 Execution Time:0.27236080169678

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20' 
 Execution Time:0.19535112380981

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.22127294540405

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 1 
 Execution Time:0.15384721755981

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 3 
 Execution Time:0.16224503517151

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 4 
 Execution Time:0.186842918396

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 5 
 Execution Time:0.20449304580688

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26' 
 Execution Time:0.20024800300598

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.18290519714355

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 1 
 Execution Time:0.24843406677246

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 3 
 Execution Time:0.1648280620575

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 4 
 Execution Time:0.28647184371948

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 5 
 Execution Time:0.31169390678406

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27' 
 Execution Time:0.22343397140503

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.22583293914795

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 1 
 Execution Time:0.14482998847961

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 3 
 Execution Time:0.21026206016541

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 4 
 Execution Time:0.16072106361389

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 5 
 Execution Time:0.16854310035706

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28' 
 Execution Time:0.29674005508423

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.17900204658508

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 1 
 Execution Time:0.15545606613159

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 3 
 Execution Time:0.1469669342041

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 4 
 Execution Time:0.1413369178772

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 5 
 Execution Time:0.2053279876709

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29' 
 Execution Time:0.14784002304077

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.16038513183594

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 1 
 Execution Time:0.19365906715393

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 3 
 Execution Time:0.14689493179321

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 4 
 Execution Time:0.16613698005676

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 5 
 Execution Time:0.1427788734436

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30' 
 Execution Time:0.1520721912384

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.15274500846863

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 1 
 Execution Time:0.14770913124084

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 3 
 Execution Time:0.19520282745361

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 4 
 Execution Time:0.14758491516113

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 5 
 Execution Time:0.17436695098877

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-09-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 09
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.23192596435547

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01' 
 Execution Time:0.17342400550842

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.15623903274536

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 1 
 Execution Time:0.1429808139801

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 3 
 Execution Time:0.14785718917847

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 4 
 Execution Time:0.13885188102722

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 5 
 Execution Time:0.14131999015808

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02' 
 Execution Time:0.15111899375916

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.16725897789001

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 1 
 Execution Time:0.16624784469604

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 3 
 Execution Time:0.15977001190186

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 4 
 Execution Time:0.14375519752502

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 5 
 Execution Time:0.17558598518372

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03' 
 Execution Time:0.16481494903564

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.17280697822571

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 1 
 Execution Time:0.15069103240967

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 3 
 Execution Time:0.15944790840149

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 4 
 Execution Time:0.14657998085022

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 5 
 Execution Time:0.15336894989014

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09' 
 Execution Time:0.14160394668579

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.15863084793091

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 1 
 Execution Time:0.1419849395752

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 3 
 Execution Time:0.1622109413147

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 4 
 Execution Time:0.19066119194031

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 5 
 Execution Time:0.15354895591736

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10' 
 Execution Time:0.44416499137878

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.15903520584106

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 1 
 Execution Time:0.21219801902771

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 3 
 Execution Time:0.30487608909607

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 4 
 Execution Time:0.34258699417114

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 5 
 Execution Time:0.23403882980347

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16' 
 Execution Time:0.20325803756714

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.170578956604

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 1 
 Execution Time:0.16245198249817

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 3 
 Execution Time:0.17169809341431

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 4 
 Execution Time:0.22425484657288

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 5 
 Execution Time:0.20743608474731

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17' 
 Execution Time:0.15160894393921

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.1479549407959

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 1 
 Execution Time:0.13746094703674

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 3 
 Execution Time:0.13714098930359

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 4 
 Execution Time:0.14094018936157

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 5 
 Execution Time:0.14845895767212

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23' 
 Execution Time:0.14695310592651

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.1766049861908

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 1 
 Execution Time:0.34115409851074

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 3 
 Execution Time:0.17254519462585

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 4 
 Execution Time:0.18291115760803

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 5 
 Execution Time:0.15701985359192

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24' 
 Execution Time:0.14088892936707

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.30272698402405

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 1 
 Execution Time:0.28751277923584

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 3 
 Execution Time:0.19040703773499

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 4 
 Execution Time:0.19979882240295

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 5 
 Execution Time:0.215008020401

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30' 
 Execution Time:0.17960405349731

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.18428206443787

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 1 
 Execution Time:0.21948504447937

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 3 
 Execution Time:0.20811009407043

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 4 
 Execution Time:0.44987201690674

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 5 
 Execution Time:0.27384996414185

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-01-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 01
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.42774486541748

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-02-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 02
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.51372098922729

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-03-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 03
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.39889311790466

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-04-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 04
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.38974499702454

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-05-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 05
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.35775208473206

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-06-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 06
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.18818402290344

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-07-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 07
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.17305016517639

SELECT DISTINCT(semana)
FROM `ordenes_semanal`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%Y') = '2024'
AND DATE_FORMAT(fecha,'%m') = '08'
ORDER BY `semana` ASC 
 Execution Time:0.2068920135498

SELECT `la`.`nombre`, `la`.`fecha_ini`, `os`.`id`, `os`.`seguimiento`, `os`.`semana`, DATE_FORMAT(os.fecha, '%Y') as anio, DATE_FORMAT(os.fecha, '%d') as dia, `os`.`fecha_final`, `os`.`fecha`, `os`.`ot`, `os`.`turno`, `os`.`observaciones`, `c`.`foto`, `c`.`empresa`, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=1 and os2.fecha=os.fecha) as cont_can, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=2 and os2.fecha=os.fecha) as cont_pro, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=3 and os2.fecha=os.fecha) as cont_ok, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=4 and os2.fecha=os.fecha) as cont_re, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=5 and os2.fecha=os.fecha) as cont_no_rea
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id_proyecto`=`os`.`id_proyecto` and `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `clientes` `c` ON `c`.`id`=`la`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%Y') = 2024
AND `os`.`semana` = 32
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC 
 Execution Time:0.19910502433777

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.14557099342346

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.17162799835205

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-07' 
 Execution Time:0.14222693443298

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-08' 
 Execution Time:0.14310884475708

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-09' 
 Execution Time:0.1734471321106

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-10' 
 Execution Time:0.18143105506897

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-11' 
 Execution Time:0.16828298568726

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '1'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.25225186347961

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:,', `mes` = '8', `anio` = '2024', `tipo` = '1', `semana` = '0'
WHERE `id` = '276' 
 Execution Time:0.092483043670654

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '2'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.2291100025177

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:,', `mes` = '8', `anio` = '2024', `tipo` = '2', `semana` = '0'
WHERE `id` = '278' 
 Execution Time:0.079514026641846

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '4'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.11270689964294

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAl0AAAEFCAYAAADZt1IsAAAAAXNSR0IArs4c6QAAHelJREFUeF7t3VuoXXV+B/D/ydjkHGbSeJoWNKE0XgqlBU1ovdWH4uhTUR/EmKZ1KmIImqAP5nI8aVCU1GNuUpRECRFxagkx4oNGH1od2wdrEhlQmSmFekkZosLMcNDMTKLj5JT/nq7t2su19z4r7rPW3nt98iLZe63/5fNbxC//9V9rj8zMzMwEfwgQIECAAAECBOZUYETomlNfjRMgQIAAAQIEGgJClwuBAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFBA4MyZM2HevHkFzvjtoWcVun4+/Vn4x/d/Hc6cmSncoRMIECBAgAABAoMqMG/eSPiHi84Ji8cXFZ7CWYWu6U9Pht/79zOFO3MCAQIECBAgQGCQBc4ZCeGnfzUvnLtoYeFpCF2FyZxAgAABAgQI1FXgWyMh/Ezoqmv5zZsAAQIECBAoS0DoKktaPwQIECBAgECtBdxerHX5TZ4AAQIECBAoS0DoKktaPwQIECBAgECtBYSuWpff5AkQIECAAIGyBISusqT1Q4AAAQIECNRaQOiqdflNngABAgQIEChLQOgqS1o/BAgQIECAQK0FhK5al9/kCRAgQIAAgbIEhK6ypPVDgAABAgQI1FpA6Kp1+U2eAAECBAgQKEtA6CpLWj8ECBAgQIBArQWErlqX3+QJECBAgACBsgSErrKk9UOAAAECBAjUWkDoqnX5TZ7AYAs89CejYeKPFzQmsf1/Pg/3//fp5oRe/ctvh2v/4JzcCZ44fSb8/Q9PhR/87Muvff/d3z8nfP/Px8LS0Xkt37320y/Ddf/5y8EGM3oCBCoVELoq5dc5AQJnI/C9P/ydsOeSsbAw/gsWQvjiTLHQFc85+eVMWP/uqfDPP/l1cwj7l4+FO/5oftsh5Z1zNuN3DgEC9RQQuupZd7MmMLACecGoW+h66n+/CGvePhWyq1jp1at0u9n2fvzdheFPF/525avTKtnAoho4AQKlCAhdpTDrhACBXgkk4SgGpvNH5zXC0GxDVxxD+rZjErrSYSyvrWxYS0Jcr+akHQIE6iEgdNWjzmZJYCgFkhWobxq6kr1h8+e1X8lKr4T918kz4c9+cHIoTU2KAIG5ExC65s5WywQIzLFA0dCVDlfp/VnpQNVuw/xsgtkcT1fzBAgMuIDQNeAFNHwCdRaYbejKGmU3xOfdcsyeI3TV+UozdwK9ERC6euOoFQIEKhA429CV3ZMldFVQPF0SqKGA0FXDopsygWERmG3oiiHrP37+ZfM1E9k9YOnbi+2eTrSna1iuGvMgUJ2A0FWdvZ4JEPiGAkVCV3xlRLtwlX7vV96m/DjM9GsjPL34DQvndAI1FRC6alp40yYwDAJFQ1c2PJ3Ne7o8uTgMV445EKhGQOiqxl2vBAj0QOBsQld6Q3x2VavTTwfF4Xoxag+KpgkCNRYQumpcfFMnMOgCZxO64pzT4SobpLI/MZQYuaU46FeL8ROoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXqCS0PXYJ/Orn7kRECBAgAABAgRKFJgXQrj7vC/CuYsWFu51ZGZmZqboWdOfngw/PD0/nPOtbxU91fEECBAgQIAAgYEV+PI3vwl/MVpy6Ipa42eR8gZW2cAJECBAgACB2gtMf/aLMDIzU+5Kl9BV++sOAAECBAgQqJ2A0FW7kpswAQIECBAgUIWA0FWFuj4JECBAgACB2gkIXbUruQkTIECAAAECVQgIXVWo65MAAQIECBConYDQVbuSmzABAgQIECBQhYDQVYW6PgkQIECAAIHaCQhdtSu5CRMgQIAAAQJVCAhdVajrkwABAgQIEKidgNBVu5KbMAECBAgQIFCFgNBVhbo+CRAgQIAAgdoJCF21K7kJEyBAgAABAlUICF1VqOuTAAECBAgQqJ2A0FW7kpswAQIECBAgUIWA0FWFuj4JECBAgACB2gkIXbUruQkTGByBQ8+/ECa2bG0O+MCzz4QrLr+s6wR27Ho0PLlvf+O4pUuXhKf37wsXX3Rhy3nT09PhjrV3hbffebfx+Y03XB+mtj0UxsZGm8el+8/7PmljYtOGWY2r68AdQIDAUAsIXUNdXpMjMLgCMfAcOPhceGrfE2F8fDwcPfZWWH3rbaFb8IqB66OPP2kGqNjOY3v2tgSvJCytXnVLWHnzTeHUqdNhcuv9DawkeMX+tu/c3ew/thv/bN54bxM1fnbBsmWNNvwhQIBANwGhq5uQ7wkQKF2g3QpSNlBlB/be+x+ETROTYef2qebKVhKolpx/XjMwxSD2xptHWla24rm3r1kbdm2faqxaZQNV/P7hR3aE3TummiEwHcpKR9IhAQIDJyB0DVzJDJjA8AtkV5mSGcfPN05M5t4ujMfkhank82TVbHR0rLGqdfVVV7asUKXD2d3r1zWOWb1qZfO2YTp0xTbjrUm3FYf/WjRDAr0UELp6qaktAgR6ItBuRSu7GpXuLG9FKy+sxc/SK1rpNtL9Pr5nb8utw3ToevW118OHx4+33GrsycQ1QoDAUAsIXUNdXpMjMJgCefun4kw6bVxPQld2BSuel77tGP+evQWZKKVXyg6//ErLnrLkduOKFctbbjMOprBREyBQhYDQVYW6PgkQ6CjQLXQlG+DzVrraha5kdWvx4sUdQ1d68376Kcg7164JyW3H2Md1117T8vRjtw3+Sk6AAAGhyzVAgEDfCXQLXXl7qXq90pV+dUR2JeyBrZPhwW1TzX1heRv4+w7VgAgQqFxA6Kq8BAZAgEBWoB/2dGVDV7y1uWHzZNhy3+bGcNNPMsa/e32E65gAgW4CQlc3Id8TIFC6QPYdXckAuj292C6spduLbcUnD7O3KDttxM+GqjiOAwcPtbxyQugq/TLRIYGBExC6Bq5kBkxg+AXaPaXY7T1deaEsL0zltdPpycjsKyyy7+yy0jX816QZEuiFgNDVC0VtECDQc4EYjI4cPdbyRvrsO7qyb5vPe7N83qpZErDuWb+u5Y306ReoJhPKe2Iyu3/Mnq6el1+DBIZSQOgayrKaFIHhEOj2G4p5P/GTBKIXXzrcQFh+6SXN4JZWSYLXiRMfNT6OTyemf+InObbbpv74243tft9xOKpgFgQI9EpA6OqVpHYIECBAgAABAh0EhC6XBwECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBIFKQtfpz78IowvmlzA9XRAgQIAAAQIE+kMg5p+xBfPDuYsWFh7QyMzMzEzRs6Y/PRmErqJqjidAoFcCIyMjIf7Tlfw33W72u7k8Jum303gc81V1+sUpjiP50+5/gWUeE8eS9Nfuuu7nYzpZph3jcf3g/U1rW0noinjjZ5HyevWPrnYIECBAgAABAmULVHJ7Uegqu8z6I0CAAAECBKoWELqqroD+CRAgQIAAgVoICF21KLNJEiBAgAABAlULCF1VV0D/BAgQIECAQC0EhK5alNkkCRAgQIAAgaoFhK6qK6B/AgQIECBAoBYCQlctymySBAgQIECAQNUCQlfVFdA/AQIECBAgUAsBoasWZTZJAgQIECBAoGoBoavqCuifAAECBAgQqIWA0FWLMpskAQIECBAgULWA0FV1BfRPgAABAgQI1EJA6KpFmU2SAAECBAgQqFpA6Kq6AvonQIAAAQIEaiEgdNWizCZJgAABAgQIVC0gdFVdAf0TINBW4NDzL4SJLVub3x949plwxeWXdRXbsevR8OS+/Y3jli5dEp7evy9cfNGFLedNT0+HO9beFd5+593G5zfecH2Y2vZQGBsbbR6X7j/v+6SNiU0bZjWurgN3AAECQy0gdA11eU2OwOAKxMBz4OBz4al9T4Tx8fFw9NhbYfWtt4VuwSsGro8+/qQZoGI7j+3Z2xK8krC0etUtYeXNN4VTp06Hya33N7CS4BX7275zd7P/2G78s3njvU3U+NkFy5Y12vCHAAEC3QSErm5CvidAoHSBditI2UCVHdh7738QNk1Mhp3bp5orW0mgWnL+ec3AFIPYG28eaVnZiufevmZt2LV9qrFqlQ1U8fuHH9kRdu+YaobAdCgrHUmHBAgMnIDQNXAlM2ACwy+QXWVKZhw/3zgxmXu7MB6TF6aSz5NVs9HRscaq1tVXXdmyQpUOZ3evX9c4ZvWqlc3bhunQFduMtybdVhz+a9EMCfRSQOjqpaa2CBDoiUC7Fa3salS6s7wVrbywFj9Lr2il20j3+/ievS23DtOh69XXXg8fHj/ecquxJxPXCAECQy0gdA11eU2OwGAK5O2fijPptHE9CV3ZFax4Xvq2Y/x79hZkopReKTv88iste8qS240rVixvuc04mMJGTYBAFQJCVxXq+iRAoKNAt9CVbIDPW+lqF7qS1a3Fixd3DF3pzfvppyDvXLsmJLcdYx/XXXtNy9OP3Tb4KzkBAgSELtcAAQJ9J9AtdOXtper1Slf61RHZlbAHtk6GB7dNNfeF5W3g7ztUAyJAoHIBoavyEhgAAQJZgX7Y05UNXfHW5obNk2HLfZsbw00/yRj/7vURrmMCBLoJCF3dhHxPgEDpAtl3dCUD6Pb0Yruwlm4vthWfPMzeouy0ET8bquI4Dhw81PLKCaGr9MtEhwQGTkDoGriSGTCB4Rdo95Rit/d05YWyvDCV106nJyOzr7DIvrPLStfwX5NmSKAXAkJXLxS1QYBAzwViMDpy9FjLG+mz7+jKvm0+783yeatmScC6Z/26ljfSp1+gmkwo74nJ7P4xe7p6Xn4NEhhKAaFrKMtqUgSGQ6Dbbyjm/cRPEohefOlwA2H5pZc0g1taJQleJ0581Pg4Pp2Y/omf5Nhum/rjbze2+33H4aiCWRAg0CsBoatXktohQIAAAQIECHQQELpcHgQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgQqCV2nP/8ijC6YX8L0dEGAAAECBAgQ6A+BmH/GFswP5y5aWHhAIzMzMzNFz5r+9GQQuoqqOZ4AgV4JjIyMhPhPV/LfdLvZ7+bymKTfTuNxzFfV6RenOI7kT7v/BZZ5TBxL0l+767qfj+lkmXaMx/WD9zetbSWhK+KNn0XK69U/utohQIAAAQIECJQtUMntRaGr7DLrjwABAgQIEKhaQOiqugL6J0CAAAECBGohIHTVoswmSYAAAQIECFQtIHRVXQH9EyBAgAABArUQELpqUWaTJECAAAECBKoWELqqroD+CRAgQIAAgVoICF21KLNJEiBAgAABAlULCF1VV0D/BAgQIECAQC0EhK5alNkkCRAgQIAAgaoFhK6qK6B/AgQIECBAoBYCQlctymySBAgQIECAQNUCQlfVFdA/AQIECBAgUAsBoasWZTZJAgQIECBAoGoBoavqCuifAAECBAgQqIWA0FWLMpskAQIECBAgULWA0FV1BfRPgEBbgUPPvxAmtmxtfn/g2WfCFZdf1lVsx65Hw5P79jeOW7p0SXh6/75w8UUXtpw3PT0d7lh7V3j7nXcbn994w/VhattDYWxstHlcuv+875M2JjZtmNW4ug7cAQQIDLWA0DXU5TU5AoMrEAPPgYPPhaf2PRHGx8fD0WNvhdW33ha6Ba8YuD76+JNmgIrtPLZnb0vwSsLS6lW3hJU33xROnTodJrfe38BKglfsb/vO3c3+Y7vxz+aN9zZR42cXLFvWaMMfAgQIdBMQuroJ+Z4AgdIF2q0gZQNVdmDvvf9B2DQxGXZun2qubCWBasn55zUDUwxib7x5pGVlK557+5q1Ydf2qcaqVTZQxe8ffmRH2L1jqhkC06GsdCQdEiAwcAJC18CVzIAJDL9AdpUpmXH8fOPEZO7twnhMXphKPk9WzUZHxxqrWldfdWXLClU6nN29fl3jmNWrVjZvG6ZDV2wz3pp0W3H4r0UzJNBLAaGrl5raIkCgJwLtVrSyq1HpzvJWtPLCWvwsvaKVbiPd7+N79rbcOkyHrldfez18ePx4y63GnkxcIwQIDLWA0DXU5TU5AoMpkLd/Ks6k08b1JHRlV7DieenbjvHv2VuQiVJ6pezwy6+07ClLbjeuWLG85TbjYAobNQECVQgIXVWo65MAgY4C3UJXsgE+b6WrXehKVrcWL17cMXSlN++nn4K8c+2akNx2jH1cd+01LU8/dtvgr+QECBAQulwDBAj0nUC30JW3l6rXK13pV0dkV8Ie2DoZHtw21dwXlreBv+9QDYgAgcoFhK7KS2AABAhkBfphT1c2dMVbmxs2T4Yt921uDDf9JGP8u9dHuI4JEOgmIHR1E/I9AQKlC2Tf0ZUMoNvTi+3CWrq92FZ88jB7i7LTRvxsqIrjOHDwUMsrJ4Su0i8THRIYOAGha+BKZsAEhl+g3VOK3d7TlRfK8sJUXjudnozMvsIi+84uK13Df02aIYFeCAhdvVDUBgECPReIwejI0WMtb6TPvqMr+7b5vDfL562aJQHrnvXrWt5In36BajKhvCcms/vH7Onqefk1SGAoBYSuoSyrSREYDoFuv6GY9xM/SSB68aXDDYTll17SDG5plSR4nTjxUePj+HRi+id+kmO7beqPv93Y7vcdh6MKZkGAQK8EhK5eSWqHAAECBAgQINBBQOhyeRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShCoJHSd/vyLMLpgfgnT0wUBAgQIECBAoD8EYv4ZWzA/nLtoYeEBjczMzMwUPWv605NB6Cqq5ngCBHolMDIyEuI/Xcl/0+1mv5vLY5J+O43HMV9Vp1+c4jiSP+3+F1jmMXEsSX/trut+PqaTZdoxHtcP3t+0tpWErog3fhYpr1f/6GqHAAECBAgQIFC2QCW3F4WussusPwIECBAgQKBqAaGr6gronwABAgQIEKiFgNBVizKbJAECBAgQIFC1gNBVdQX0T4AAAQIECNRCQOiqRZlNkgABAgQIEKhaQOiqugL6J0CAAAECBGohIHTVoswmSYAAAQIECFQtIHRVXQH9EyBAgAABArUQELpqUWaTJECAAAECBKoWELqqroD+CRAgQIAAgVoICF21KLNJEiBAgAABAlULCF1VV0D/BAgQIECAQC0EhK5alNkkCRAgQIAAgaoFhK6qK6B/AgQIECBAoBYCQlctymySBAgQIECAQNUCQlfVFdA/AQINgaPH3gqrb72tqbH94W1h5c03tegcev6FMLFl69fE8o5NHzQ9PR3uWHtXePuddxsf37l2Tdi88d6WdrL9xy8PPPtMuOLyy9qO4cYbrg9T2x4KY2OjzWOSviY2bfjauUpNgEC9BYSuetff7An0hUAMU4/t2Rue3r8vXHzRhSEJLqtX3dISvOJxb7x55GtBp9MkkrauvOLyRtA6dep0mNx6f+OUdGB65p//Jdx4/V+H8fHxxndJwEsHrxjMtu/cHZ7a90TjuB27Hm0cmw5w8bMLli37WmDsC2iDIECgUgGhq1J+nRMg0G5lKBtwkiBUNHTF8HTg4HPNoBTbee/9D8Lta9aGXdun2q5GJeFsyfnnNUNVNlDFdh5+ZEfYvWOqEcLyxqzCBAgQSASELtcCAQKVCsSgsnFisrnK1fzH6f9vCaZv0+WtLHUbfDzno48/aVnVmu0twHR/SQhbvWplM6ilQ1ccR7yF6bZit4r4nkB9BYSu+tbezAn0hUC30JW+xXg2oavdStemicmwc/tU43Zm3p8kZF191ZXNW4WdVrpefe318OHx41/bK9YXyAZBgEBfCAhdfVEGgyBQX4F2t/qSz+9Zv64RepIQ9OJLh5tYS5cu+doKWVay3Z6u9G3DPP28FbJsgEtC2IoVy1tuM9a3mmZOgEAnAaHL9UGAQOUCMby89PIrzQAVA9bD23eEH/3ox+Fv/2ZV203p8bwn9+3PfcowPanZPL2YDXXtnohM+oztx6cg716/rrExP66IXXftNS1PSeY9/Vg5tgEQIFCZgNBVGb2OCRBIC6TDTFzB2vv4P4Wnnv5+I8xkXx2RPS+7Zyv9fd7ty+zTknmVSJ5e7PY6iuSJyge2ToYHt001xxtX6rrdwnQFECBQLwGhq171NlsCAyMw283ueXu2kkm2e/VE3pOJ7YJX9snH7Arahs2TYct9mxsfp59kjH/3+oiBudwMlEApAkJXKcw6IUCgqMBsX7/QKXR1ejXEbN751W6TfzKXdKiKxx44eKjlKUmhq2jVHU9guAWEruGur9kRGEiB2a5EdTtuLle6sqEw+84uK10DeekZNIE5FRC65pRX4wQIzEYg+zb4uEJ05OixlheaxgD14uFXwm3f+7tmk9kN+PGL7H6tvP1b2bfNx8D0r//2alh359pm28nPAuVths+79Zl9xYQ9XbOpvGMI1EtA6KpXvc2WQF8KpDfRxwHm/TZi9gnEeFzebx/mhazs7youv/SSlkBX9HUU7d4Xlh7jbF5n0ZfFMCgCBOZMQOiaM1oNEyBAgAABAgS+EhC6XA0ECBAgQIAAgRIEhK4SkHVBgAABAgQIEBC6XAMECBAgQIAAgRIEhK4SkHVBgAABAgQIEBC6XAMECBAgQIAAgRIEhK4SkHVBgAABAgQIEBC6XAMECBAgQIAAgRIEhK4SkHVBgAABAgQIEBC6XAMECBAgQIAAgRIEhK4SkHVBgAABAgQIEPjwJx+H8d/9Tjh30cLCGCMzMzMzRc+a/vRk45Txs+iwaF+OJ0CAAAECBAj0i0Aloev051+E0QXz+8XAOAgQIECAAAECcy7w2clfhkULv13eStfPpz8Lv/jVr8KZM4UXyeYcQwcECBAgQIAAgbkSmDcyEr7z7bGweHxR4S7O6vZi4V6cQIAAAQIECBAYEoEzZ86EefPmFZ6N0FWYzAkECBAgQIAAgeICQldxM2cQIECAAAECBAoLCF2FyZxAgAABAgQIECguIHQVN3MGAQIECBAgQKCwgNBVmMwJBAgQIECAAIHiAkJXcTNnECBAgAABAgQKCwhdhcmcQIAAAQIECBAoLiB0FTdzBgECBAgQIECgsIDQVZjMCQQIECBAgACB4gJCV3EzZxAgQIAAAQIECgsIXYXJnECAAAECBAgQKC4gdBU3cwYBAgQIECBAoLCA0FWYzAkECBAgQIAAgeICQldxM2cQIECAAAECBAoLCF2FyZxAgAABAgQIECguIHQVN3MGAQIECBAgQKCwgNBVmMwJBAgQIECAAIHiAv8HWNiqS7hnJasAAAAASUVORK5CYII=', `mes` = '8', `anio` = '2024', `tipo` = '4', `semana` = '0'
WHERE `id` = '277' 
 Execution Time:0.089149951934814

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '5'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.16498613357544

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAl0AAAEFCAYAAADZt1IsAAAAAXNSR0IArs4c6QAAGKZJREFUeF7t3c+LJvldB/Dqnt9Zx9nMrKC7LkbjSlAIojktSCT6HygehOhBb5KD5KAYiCh48CCCIl70oAERr141xIPrJYoElMAmuLJZCSQ762bczE7PTLd8e6nm2zXf+vH59NM1T8/z6tPu9OdT9dSrvlX1ru9Tz9N7R0dHR50fAgQIECBAgACBcxXYE7rO1dfCCRAgQIAAAQLHAkKXgUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQCAgcHh52+/v7gY4PSlOh6+13vtP9wdcfdoeHR+EVaiBAgAABAgQIXFSB/f297nMfvdzd+fCt8CakQtc7797rbn/pMLwyDQQIECBAgACBiyxwea/rvvXJ/e75WzfDmyF0hck0ECBAgAABArsqIHTt6p633QQIECBAgMCqAkLXqtxWRoAAAQIECOyqgNC1q3vedhMgQIAAAQKrCghdq3JbGQECBAgQILCrAkLXru55202AAAECBAisKiB0rcptZQQIECBAgMCuCghdu7rnbTcBAgQIECCwqoDQtSq3lREgQIAAAQK7KiB07eqet90ECBAgQIDAqgJC16rcVkaAAAECBAjsqoDQtat73nYTIECAAAECqwoIXatyWxkBAgQIECCwqwJC167uedtNgAABAgQIrCogdK3KbWUECBAgQIDArgoIXRN7/h9efa77ue+7/ETFP37rUffzr73XffrlK92fffxGd7MoDn4ODrvuD19/0H3+q+93w+W89f5h9yv/er/74rcfnepaWvf7H7ve/dYr17qr+x+0/+e9w+4nvnjviddQL+/eo6PuN75yv/vCmw9P6j71wuXur3/6RvfS9f2u9ftSuHRdu3oAbet2j43dv/zvg+7X//1+9x+futn9+M39rh6n/bbU+7weW0vH51TdcDzVfsMxOPY66p76GKxf63A9re1sje+6bumyx7ahNy6/bx3z9fL73//px28c75e5n34/lrr6OK77+vPU3LL8fjsE6vHSv6Kxc3urdq5n7Hy/5Jj87VeuLx6X//T2o5PrYv/6/+Inb3S/9kNXR69X9WtrHat1f7+dY9es7dib469C6GrYjJ3E+tKzhq7WoGqtc2k4Gxt8w4vf8CS8JHQNl3FRB/q2H4iben1TJ9Cyjk2Grsw4nnt99WvcZOgqy22FkOH4Pq/QVW9Xv683EbpaF6OpILupcWY5mxOYOyZax9lU6BoL+U8zdNVjvXUNqQ3q697ctXjsuN7c3tn8koSuhml9Ih4Gn3KS+8iH9p+Y6RoLSGXx/fLKYLu2v3c8QzW8APSDrtQ/ODw6nj1rLXNsENZ3vv0mTV1QhnfIczNhNVNrXZsfmpYYFRievIdjrIyHN757eOaZrrOM4yWzaP24/9kXLp/M6I7d8c/NRk0dT/Wx9PbBUXfn6t6pmb+5ZZfjeOx1lfUOL4zDY6wVuurZ77nQWQeu4YV5bpYtOrbUn4/A8Jitz61T71S0Zqrr8TR3Q1SPxblxNtzyufqx42Zqdr3e1v68NbzWTc1kX6RrktDVOJbqE9bUzpw7aQ7DTzkQxgJVK5i1Qlc94F+7+7j7xPOXjkNc6+Tfeotp7C5i7u5jbl3nc0qy1KUCwxPU3EnoLG8vnmUcj52wW3fCrzx36cyhqxwbrUBVXFs3OtGZrkjoKuus6+fOH1MXt7m3Y6LjYek4U7dZgblrTf37+iZq7PidOq6XzHRNjed+y7Ohq75JGN4Qtl733FuS9e+nJj02u8fOvjShayZ0Te3MuZPmMHSVYPP6e4fdT926dOqOuj4Y/u3dx90rz+2PznT1QapcHP78jQfdL754ZfSZrLr2y//7uHv19qXjl9S6k2iFrsi6zj4ULeEsAmPT82PLPEvoOss4fhqhq75hqE/2/fgux/iDx133I8+dfsZtUzNdtVfZH30gnjt/TF3cluzvuYvWWcab3rMLzL3lVt8YlJuH+lrUOn7nwtDTDl1j433s3+cC6dyNx9n30PksQehquLbeY597EHa4mDrE1LNYf/fWw+7TL1899RZjfcf9hTcPul966UozdNWDrH89v/Nj104e9h/ObsyFprfuH40+SB9d1/kMT0tdKhC9wJ41dGXH8ZK3F/u77bmLSLFZEoz+9q2H3c/cuXR8c9IfN6W3/xBJCWI/cH3/iQ8WLFn2cP+0LozlXPDHXz/ofvOjV08d1y/d2Dt54HhuVns4AzE1a9CakbhIMwFLx/xFr4uO7/qaMvVM19gs95LQNTWezzrTVfrnZrSWTgj0r2UumG3jGBG6RvbK2AOqY28PLA1dv/vVB91nf/TqqQtAH5zKifGPvnbQ/d7HrjVDV32Q9oNz6sCt314sB+L/vH906u2az3zl/mjoiq5rGwf3Lr2m1jMRU9t/1tCVHcdzDw1HnzVZEozK2C/PYZZPIvdvHxab8gng8lM+ZfwLL145t9DVr/PF63snn+Aqx+9fvXmQDl1L9veS2bBdOka2bVuX3CiNzYbNPUjfeptwG0JXvc19OKwnB/pP/C+ZBaxDXPnvuUcqtmX/C10ze6L1XFTr04tLH6QvX9vwqy9fPbkA1G8Rzp2IWxfKqU8gDkNX+aqA+t/qGYDh24vRdW3LgN7V17HkIlzbnDV0ZcfxVOgaXiiiMwFjD9oObzjKcVZ+SggbflXDeTzTVS+zdp+a1S6vb+kN1dhXQwhd23022EToqsfW8Dm+pZ9WX3Kc1ZJz9WM3QmUZw9/VN/71NVToao/dvaOjo6PosH7n3Xvd7S8dRtueen3rkyTlRfXf0xUJXfUDwn//zYdd+ZRW+VRjSfmvv/e4ucyp7wSrccY+/dL/e31gfvvgg933wtW9U9/TlVnXU99BO/4Cog+VbiJ0Zcbx3Ak7cnJvncT776ur11PG/t984+HJrG4JZ+WnfB9Wf2Ga+zTYWKBb8iD9WJAr54zvvbw3+vzmlNWS/b3kor7jh81T3fx6/y75nsS5Z7rKxkzt8yUzXef5IH2P3R9rZZtbjymUuiUfBPFM18Lhe1FDVyt5Z0NX/SxVz9YfUGPPecx9H0+/nLEvs6zDWGu2oT7oM+tauPuVnZPAMCjPTbW3ZkH7lzb2vFD9bGKZ6cqM46cVuoazvGVbx2agWm9xbCp0DS+Mw+N/6VdGzH09QFnuRXze5ZwOj61c7JJgMTaDPXbTNBW6xh5YjxyTBXKufmqmqx7/5fj7xv3DJz7A0u+settbYfCizuR6e7FxOJYB/S93Hx1/n1FrAMwFpOEihxer8q3wY19cmvkkx9hU7NSFdeo7hKZO1kunfbfyLPeMv6jhmGp9sKL/nq6xu+yp/buJcTx3wj6vma5yLA9vNuZmDpY8Lxad6WrdxZd/iz5IPwxvU9/TtWT24hk/NLZ284ZjcuydiuH4mHv8o2xwmcX957uPu6+99/j4L5GMBbjIMbmJ0LX0g2pLv6dr7K9NbOtOF7pGQtfUn+JofeS7tYP7ty5aF6t64E29/VD+XFA9+zU2Dd0KSlOhazgz0i+3bEf/lmlkXds6wHftdc09YDt2Up8av8Mbj7GH3efGcZnFiZzg5x66L9vS+pMjwwtD6631/qJU/pxXPSsUfaar5Rb91v9s6Cp9rWdO69fkU4vbfwaYe2ehdR6eO877nk/euXzy4Y1eYurPbU2N5/53c8fw3ExX6wu+x55LXPKoy9ys/raNAKGrsUfGDoLhCWxuQEyFrtZXMpSLUmum65d/8Mrk360a3vX2d7ZToWvY0zpIx+6QPSuybYfx6dczNi7nvgB37KQ8Fboi4/hph646pIzNDK0VuoaBKTPT1e+Xsf190S5G231Une+rG/tLI2NhZCp01eft4bVsbpytEbqGY3/JTNXcF32f797Z7NKFrs16WhoBAgQIECBAoCkgdBkYBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBJ5K6PqTb15dYdOevVUcHR11e3t7pzas9W/P3pbHtqg3+ciH9rs3vnsYa1b9zAgMj43y/+WnPobG/m3uOMsei5vsG+6ouXPB3O/L8jb5+lr+S1xb+yjS9+rtS91rdx8/M+PYhjxbAvtd133m+w+652/dDG/Y3lF/xgq0vv3Od7ov379yfHD7IXCeArevXeruPnDyPU9jyyawbQI//D1Xuv/6v4fb9rK8HgInN32fuHHQ3fnwrbBIKnSF16KBAAECBAgQIPCMCBweHnb7+2XOK/YjdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAv8PfnAtHrj438kAAAAASUVORK5CYII=', `mes` = '8', `anio` = '2024', `tipo` = '5', `semana` = '0'
WHERE `id` = '279' 
 Execution Time:0.081795930862427

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '6'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.14864897727966

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABCMAAABoCAYAAADLnzp0AAAAAXNSR0IArs4c6QAAIABJREFUeF7tnXuwZUdVh/s+MnMnYYyTCUISSCIhFEWKGEBIAvIQ+ItXWSpiQKCoAAoUlggkJIWoVCSEZxUIYghSCMVDKKSSEEsLMFjIswIxlIqESEicCcIM42QgM5nHPdY6w7qzbt/Vvffuc84++5z9nb/uPWfv7t7f6l69+tePvTAYDAah4Wf3nrvDX9x2KKyuNr61YU5cDgEIQAACEIAABCAAAQhAAAIQgEAXCSwvL4UrzjkhrGze1Lh4CyVixJ69+8JJN642zowbIAABCEAAAhCAAAQgAAEIQAACEJgPAo/beiR87nH3QYyYD3PyFBCAAAQgAAEIQAACEIAABCAAge4TQIzovo0oIQQgAAEIQAACEIAABCAAAQhAYK4IIEbMlTl5GAhAAAIQgAAEIAABCEAAAhCAQPcJIEZ030aUEAIQgAAEIAABCEAAAhCAAAQgMFcEECPmypw8DAQgAAEIQAACEIAABCAAAQhAoPsEECO6byNKCAEIQAACEIAABCAAAQhAAAIQmCsCiBFzZU4eBgIQgAAEIAABCEAAAhCAAAQg0H0CiBHdtxElhAAEIAABCEAAAhCAAAQgAAEIzBUBxIi5MicPAwEIQAACEIAABCAAAQhAAAIQ6D4BxIju24gSQgACEIAABCAAAQhAAAIQgAAE5ooAYsRcmZOHgQAEIAABCEAAAhCAAAQgAAEIdJ8AYkT3bUQJIQABCEAAAhCAAAQgAAEIQAACc0UAMWKuzMnDQAACEIAABCAAAQhAAAIQgAAEuk8AMaL7NqKEEIAABCAAAQhAAAIQgAAEIACBuSKAGDFX5uRhukzg35+8NTxs62LYcWA1vOCm/eELuw63VtzPPfaE8JT7Lg/z+8APDoYX37y/tbzJCALzSuD5DzwuvOfcLWHr8sJU2vW8cuW5IFBF4MknL4e/fdSWcNrKYth3eBBeccv+8OE7D1Xd5v7+xoeuhEvP3hw2LR7rH685b0u4+IxNw+s//+PD4alf/llR2twEgXkjYNvef+xbDed8Yd+8PWKnnyfl+6Y5xhgVWC/FCDWYwDu4GsJVt94b3vCdA6Oy5P4OEbCDBFusUYOW0ke05ZlGnUOMKLVc8/tSdQ9/05zlpO6wfUCcR5PgCjFiUhYqS9dre574Wve6slJw16gEYvt4Av44xQjbP2r7R4wIwYo0uf6r7nWj1gvuTxOwNmjSh5UwtXlNK6YuKXdX7hnVVp7vk2fTiZFpjDFGZds7McILQrqoekuw/MAtCyOp/aNWjlm93wYR8TO04Ti1jt25f7BOMZ6maokY0V5tRoxoj3VpTogRpeS6fZ/X9rzA3OsjWDHWHdvG9vGC63GKEayM8G0fiwxylRcv2/gC0X067WjUAW6TUrMyogmtjdeOaitWRqxnujAYDAZNTbJn775w0o2rTW8b2/VeEDKNZfOpBxpnBzs2aDOUUByM2gBT2L773C3hzbceKF7OWYViVCdTlX7p74gRpeSa38dseXNmbd+hYsSoMwjYum3L5fOz9th9cBC2b1pwl/Cr/fUaSRUxoju29MTCeBA86ViJlRHHVkZIzbh3deBuR7N20PY0ql/tTk2cnZJ0NfacHYLtlXRUW03a97VH4lhOvVsZYWen7z0SwoNOWOzUVo15rGRtVmzbyKchMo3qZCbFCjFiUmQ3pssAtT3WpTkhRpSS6/Z9nhgRCw11run2U8536ax9vrn3SDj7hMXKQfAkVjwiRqzfppESGjTmsYIFYkT7bbSrsWf7JLqf46i2msdxYq/ECNvJydLNr/zkcPaAIm/f4g/uGYTHnrQ0rO3xTEq8VE2uqbOkTa+7/Z7VtfLY5hR3tN6sAbM6R4nFywqruFTtHY4Hlp/aeSi87MyjB13Jxy4BTi39lmteecv+5GFbo5ZB65jNP64zsRhx4UnLw8M05RMHDjYI+/JPjoQzjl8YHhJm63Jc16ch/HS1y6kjRsSdyTtvOxheddamYdAtH49n03qivsqm5S277aPt6ooRTZnn/ENs8xt3HQ7PvP9x62bum+bn2dhLYxKDtS62v9RA1vpp9W/C5NafrYZHnrixP/d8ecww5ydTfbkys/1S3bihi7wnUSbLVThpXxX3U019qOV83Q8PhSedvDz0t5KHfOLDKhEj1sdTEgv86i8uDWMfLxaQfsSb4EttW7Rt0trGpp0adNGPbWx53gA3FYt430uK9kDYXEySi3H63P/U9YfjtpUc3rtj/yB7oG/Xx429EiPiTu6Luw8nT0L3nF1c0TSgsA7Tq4w22E8NWMUBV4kRkrYeUOLlM+lDa+o2tGlfV7fR5c6W0A4xt/9fn1O5l4gR4yiDBGm6hNKyT3X2nn1soJcqkzB503fvXXN4cTp9GfBU1e9cnVFGtuNIpWeDslHqifqfyx+yee2NKnGefZvJqiNGjMLca4epfkLrxBO3L7titKRV5Y/Uxs99wHHJNCSdKnG2qm53/Xfb9oTZKSuLQ9HV+ia1vfjHuw6sbnjLUK4/t+nk/GROjNC2JmKUBv9VcUPXuY+zfGofr12kBqqp/G3s5Yk+2iYQI3yCNg7++I5D4fHbl4YTE8rVDmBte7P9SS6W1hgltaLVfq+2T9lRytK3fsxabZwD3KqYJCVG5PrMPvQ/df3gOG3lxZS2n6oaw3Rl3NgrMSLu5OQ1UKmg1A4sU4GgBnbeScxSKWO190N3HnTFD6mY5524GJ71tXtCbvmNVybJx34/78FmncaeCiZtRxWvktFXE6nNtDHHApBXF1Idr23kVaffesJBqgxqY28lhC2vdUi2LqYCNO8U8bhz13RSLLt4GGydOjPOa0rECOVmO3O102lbFtb8RpN6Ym2ROv162tuaxsm9SVop4VDr9a0/O1LEXNumrQOpwae15aj+SJ7dy1PeEmW/n3fBMBYjrCggtrETEPL/mccv1nrlcTyRIa9Gtt9VDYK8vrtu3NCnV0p67SA18In7efV38fdenJZb5eL54r72a/HBntpetL6L35FXospH3kr3W6ceNxT/6rYHTyS098bxhuYnqzPsvX3txyYpRtSNSbw4JRUfznv/UzcGaVOMmJVx4+NPHIR/uvD4sLL56OuUm3xm6gDL1B4dbylebglSvNz9o/9zqNbye2mw7/zewfCqB28aKsv6icWDlBiRK9Oo+4+aGH2Wrs0tF8vNQlqF3Q5K4uXs3jkMKVt4dh21DKllpJ7oljozwqtXdnY1Jaik6kFXVNZp1tOSbRqyzE7E0XHVk6ptOjKY0k+dVQLT5DmJvKvEiFNXFrIrDDzRoso/2L4itzXKe14vvzoDKk2rL2fGxGKEnQCwWzOV3QsfuCkpRqREbS9Iz/k966f1utykQx3/MYk20YU0U/XU81E5hl7fmNoKIM/tXc82jfXbNCRW3XlgMBQfdKuGsHvKfZfXVkrIAeEpMcJbIZFakRmvwtR2k/NjfezHJiVG2L6laiLNW5UXi3d96X/q+tC2xAgpj66mj+OTro0beyNG5JZ2aQXyFL4mAWYcHHpBhexTVGduK6635SO11Kbrlapug2zruliUyG2JiW2S28ozaTFCyhLP5lnbV4kR3gyDpqmD0SoxYpRlsW3Zt2v51BlMpALpEjGiqp7I7wRx62tJVeBatdy0hHlOuB5HfrnBU1+CwViMkFUF9tBqPWMjN7ipWtbaZOY8NWOLGLHRa1dtd9U7vBUQdYS5XBtAjPB70XhlhPVh0obkI+JDfG5VnW2fcq+9ztpf4hx7/o63uiWeyKvy6V2LE8ZdnkkMcFMTJN4g105iIUbkrTsJW3lnRiBGZOwwjVd71u3kRp3tqiNGfGHX4SGdOOBpsu8HMaK5G49nRezy3dwSzNzA0uv8SldGlJRhHGKEV95UuvN4gm/zmlR9xyTFiJJ6ghix0WZVgWvdWdEm/sGeEVBn8OTVtFx+iBHr+1VPNFCmucGNN4Pu7V2vqiPWX1Ydvqgro+LYoE+Hy9Y5p0v4eOcV1FkJhhhR3XfFV8RihExixBN7tm7HftX6vNz2C9lOZvspsad3wC+ietqGTVZ5eyKppGwPsMyt1kSMaN6W7B2TsBViREObTEOMqNpPFgcf8SF9GrjEnaUX0NjBgndmxOvOXhm+WUFFibrOWzCnzobgzIhjlVBs9LwHHBdecvMxxrHwE682iANFe4aHd690yE334FctdatbhnGsjEgdqFpnxi+1z1ks8KXH3yf89e33Drcb9PkzbjHCdvwl9UTur3NAWJ8GPlViROr8BWWpZ/w08Q+5065L8ovtlTobos9nRsjKiJTwL37KG9xUbXOr4ydz/bX6xtS2gdx2gnn2q7nn9oSd+ADQqvOcECOa1x5PjIjjYC+eyJ29kxPpvFVJqbOPUm+J6lM/JoNaiUdTTFPfe2eIjSpG2LOtUqu6+3xmxKRtlYovZmXc2IttGlWduxeoV51KLg236ds0bGONu4XcGzdSBxnGabBff+OrPXOcc1t3qk6vt+na5YJeZ5p7tWfTMoxDjPBCkjrpxrN2cTp97mgsizrLvK3gWbU/Mx40xdy9uuoFZLm6VnXgWPMwttt3VIkRdpbOe5IS/1C1smgUX6BlrNruMe8HHHvbNGJhIPWWIW9yIWf7uitRvDQkL7vkvcond7s1jV663AA1Jd7k3uyk96Qmh+J2wDYN34aeGJE6ONS2s7pvjPH6HTt48n6nHztmK+/so3gcULVFvWqlUdVEWip2TPk9e17V6J5jdlKYtK1SYkRVPNqVcePcixF1OrnUNXFgl3oVmNdZep1hqlLES69jZx8P8rxKPe9BZl2Xk9uS4zHylobmzurQPcdanjpp5sQISadpGV5w09FVH6XbNOw71iX/eOBatfw4Du69ul7XXvN4XZXzjw/nqiNGjFJPLGOvrvVpJklZ1BEjSpjn/EOVGFGSn/oCa+Pcwb3zvmopJUZ4b8MQZlUHJqp/tPvXq1ZG1NkWavsNb7DQt7c3VK1ejdtGfCC4Xdaf6o9YGdG8t/XECNtuYrHA86vemV2Shhx86YkNTeuCPlWf+7GqGCx+89o7bzsYXnXWprB1eaFy21MTMULK0ef+J9fC4nFbyseX2iq38jIVs3dp3Dj3YkRz95u/o24QO+58SW86BOosuZ9OycgVAhCAAAQgAAEIzA+BlHA4P0/Ik0AAAjEBxIhEnRB19un3Xw6PuvGna1d4+6z07Aeq1nwSQIyYT7vyVBCAAAQgAAEIdIdAndVj3SktJYEABMZFADEiI0Z4r+CUy9kbP67q1/10ECO6byNKCAEIQAACEIDAbBLwtg92aQn5bFKl1BCYHQKIEQ3FiL7t5ZydqjyZkiJGTIYrqUIAAhCAAAQgAIHUm+ogAwEI9IMAYkQ/7MxTQgACEIAABCAAAQhAAAIQgAAEOkMAMaIzpqAgEIAABCAAAQhAAAIQgAAEIACBfhBAjOiHnXlKCEAAAhCAAAQgAAEIQAACEIBAZwggRnTGFBQEAhCAAAQgAAEIQAACEIAABCDQDwKIEf2wM08JAQhAAAIQgAAEIAABCEAAAhDoDAHEiM6YgoJAAAIQgAAEIAABCEAAAhCAAAT6QQAxoh925ikhAAEIQAACEIAABCAAAQhAAAKdIYAY0RlTUBAIQAACEIAABCAAAQhAAAIQgEA/CExFjHjXDzf1g24PnvLXTloKX/rJkR48aXuPOBgMhpktLCwE/bsqd7lWPnWu12urrrfXeddW/W7LXDdPvafkebxnf+jWpfBfP12twsfvUyJw7i8shVvu7p//sHVV27nX3uPfbLvwfot9R+oa+7227ZL0rJ9KPcfiQgiDcNQ/8ekugeOXQrinf02xuwaJSra8EMLho6EBn44REB+3im06ZpVjxVlZDOEAYWBn7SMFO33zIFx0ymJY2dxcH1gY1Bn5RI+/Z+++cNOBTWF5aanTYChcPQKnH78Y7riHVl6PFle1TeDkTQth10GihLa5183vfpsXwv/ei33q8prF60SGSFm4jhCjz+wJMfJbaRqWZZM0bJ5VaZSma4WpttI4bnEhHPr5iMrLs0k5UmJykzSacLZ1JA5L64h8nh2rvivNs266cVtXUa+t52vbVl6dSU3IpMpWVe/qsE+VQ+5Nsbc+LjXx0rRsWtZYuM6Vo8ofaZuK06gqW64Nef55UuWI063LQoS8I2Hj5N44beXVjSacU8/SJA1vSN6kPns+t6puxH6q5Hop98FDh8MTTmpZjJDCbztx6yzGVZQZAhCAAAQgAAEIQAACEIAABCAAgREJ3PWj3UNdoNWVEYgRI1qN2yEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIoNdixJ49e8LFL31ZOP3008OVV7wxbNmyMrTh92777/CiF7807Nixc82mV73pivDs3/7NrI0/+alPh0svf/2Ga1L3vuVt7wjvu/oaNw8t283/dsvw94995EPh/Mc8el3akt+/fuWr68o+i5VwnsssdelDH/5IuPzSS9bql31eWwdOO+3U8MFrrg4PPutBa5dQD2andnzt698IF/3eC8OznvmMZJsUe++864fZ38UneO1dSJTmYX2TVz6tZ5e+9tUb/MzsWGD0kiqnP3jpi8Mlr/njDQnu338gXPb6N4RTT7l/9vebvvnNDW1ZEyvNw/oKr3zia1576WXhrVdduc6HjE5ltlIobSP6lGrjnA1L86AdhqB8r73u+qSf0xjsbVdd6foj5V/VTpvmQX8bgjL48a5dSR8m/F9z6WXZ36UvxD7j951dto8dO3nxrNCQfkw+Xv86flrtpzgO/6YcH/XIR7qxYmkeXfZvvRYjNDCIg/Mb/uEfw0MecvZaQKcdX5UgUVcc0IokzcSKIHEw9LgLLxgKIF6QKd+96c1vCW9/y5Vh27Zt7bc4cqwkkKpfemM8MJXr3/We96518FpPqAeVqDtxgfoJKUxOgEyJEdJRvPqSy4bB4IUXnO921iV5yD1XvfXt4QNX/9XQV3jBgHz3y2eeWSm4dgL0BAthB4ueIFQlRqivliI+93ef4/IsycP2LZK2CCLqF+R/LddFz3l2r8UkK9iVtsM6NqQdljdCG0if9yvnrvklm2KVGCH+6j+/852wd+/d7v0ledDfHrWAHbCkhPUqMQL7lLePqju7ap94QiOOO9Q3f+wTn5zrCdQS3xPbXPr7j378E8OvvcmFkjy67t96K0bUadC2glTNaMq1dcWIqrQ8ocEOFuJKVeW8+L1dAjZQlZy9Dt0TmOKBDvWgXbuNmpsGaE964hPCjV/8F3fWKNf25X7pqH/jWc8I7/rL97pBdkkesdAQ1ysvaBiVxazeLz78Y5/4u+FquTvuuGODDarECLn/+7ffPhR2UqvWmubhCQ1aV1TMrtv3zKpdmpS7pI3Y9OvYsCQP2uFRynYyRtrYBec/ZoPwmhMjVLT9/ZdcPBRZvdVcJXnQ364XI84552HDfuwPX/HyDaJqTozAPk28VfNrdezSNfvEfVLcb/Vl9WWJ77G1QO+XOPAz116/btJBryvJo+v+rbdihAYcYtzcsmk1fp1gr87yozoNMm7UUgYbyNQpS3MXxx3jImDtc/1nb3AHJSkb6kBFZrG/e+v3hoNTu3qGejAuK40/HQ3QZGmxBMlekJ0SI6zA+NSn/Ppw+5gXZDfNQ55SZtHtjLntlOT3VF7jJ9T9FLX9CXtZhhwH4jkxwvr27du3J7dMNM1Dg/vLX3fJutV66ht27NzJ9gxTtZq2Ed2eKUnUtWHTPGiHxwxk25CIdnY1oF6VEyNs3/nu97x3eEu85LskD+Ku9WLERc/5naGw+tWvfX2DKJsTI7DPZPs59VFds4+OqbQtxmJEnfHRZMm1k3qJ77Els5NDn/v8P7vjh5I8uu7feilG2FnpT//9Z2qJEXUaUp1r6sxC5hQsGajIUm4bmLbTxMilhIAnOuQGNLaTl/zirTgqRlAPSqwx2Xus7Xbv3j08PyJe6p8SI+KVMqnrSvLIzchKZycB57zu32xqcSsGCpt4oFTVdnU7zMrKluTZEk3zqFoZIQMyttgcs3RJG9G7bf+cs2FJHrTDo5RtG3rlK14+bCfy8c7tis+MiFeFpgbFJXkQd20UI1QYj4X1Ku66hazqOjl7p24dwD7dtk9uZYSI833ZVl7ie2ycYseRKVG2JI+ut5/v33lXOOWXtoeVzZuahm1hYTAYDJretWfvvuEt207c2vTWsVwfd2ZVWyYkUxs8ps5nsHt4tKDeAS46OL34RS8IL3/lH60dkmkP+rHKp54ZoQ35/R/4IIHnWGpCO4nkxAi751tLYwel20/aNpy1FgWcetCOvUbJJQ68xLfEs0opfxPXk1QQV5JH7L90UPSIR5zXmwChrl0tKx2M2oFSToyIxehUv1GSh603Uh5d7SJ/z/se3Lq2s4KCPVyvSTusa0PaYVOrHLve244oB4bbVUipIDwWbeNYSXMpyYO4a+NgV+IOqeuxsJ7qn7BPebuoe2dcT7tin7jNqjjxp6+/LPz5FVeuW51Z91ln8boS36PPGa+cT8UbJXl03b/1ToyIg/7U4EC+1zddpE4ErmoomoadHdXv7DkC2oif+fSnrc1QeqfSymyrzrzJrJ2+uSN3en9VGfl9sgRKxAgJzHRGiHowWfuMM/U4QFPnb2eVPH8TC6RSplSQXZpH7M90NkoEMZ39yr25Z5ycupxWLCBo+9OBUio48LbfpQZUJXnEYrcckGq388jMk74BKnWKeZe5j7NspW2kiQ1L86Adrl8ZoSuypE3YVUi5thOfxZLzqfatN3XyoL/1+55Y0EuJEV68g33G6d26bR8VRuSJ9XBau9VAVvGNOq4aL83xp+bFCHV8j5TEWznvTWqU5tFl/9YrMcJzoHVWRmgDKxEl4vTl/+s+e8OGw+3iyho3ERsoyW9Vp+OPv4mRYgmBEjEi93o+6kGJFdq5x/Mvcbv2/E0qsKt7bZ08YgJaL3XWIvfGlnbodSMXr+O3gXhq6X7dgEGesjSPmJDO4r/k4hetO/ejzlbAbtCeTClK22ETG5bmQTv0xYhYuPXEiLoCrTD2gvU6eRB3+YPdWJT16j/2mYw/S9VJXTErv3fVPnalzLe+dfPa+QdS5viNUO3Qm3wupb4nNdHh+cLSPLrs33ojRqQcZR0xQhWr3HuVU1XcWyLtHZiZO7BJA1jd253be8prPifvbJrkMMqZEQ8+60EbsrKHBFEPmlhi8td6AZp2GpK77ImWmYG4/dvZUq+UdmVVaR42XXsgonyfOpdEluj27eMNSO0gRleU2BlXb4ue5RavVCjJI7aD3f8ZH3TrnTHRJzuWtJGmNizJwwsE9fynPrXDVNBtl5vrSh97ZoSddfXqs50sKs3DE4v6FnelYmUressq3Tgexj7teNlZso/GqM94+tM2HKQdnzHRDr3J51Lqe+yqBa+UdgV8aR5d9m+9ESPEkeq2hlR1lKWvqQC8SiyoK0ak9hHHe+1sejbw1Jm51On4iBGTdzZNcvDECLk/d3aAvFpQ3qYR25J60IR8+9fm9tHqnmgJbK0YkQospPTeNo+SPGISVsTyAoJY5Gqf5PRyTPlnDbT/5v3vG75uy4oRub4hnrGSJyvJwxKJxQYrUMp1iBHf2DBQEi7WFnE7bGpD2mF5G606d0XO2dG32VgxIjdxFG8jKM2DuCu9RdAK6/LawT/5szeuW+GLfcrbRJM7q85JkbS6YB8bWxw4sH/Dwft9EyM07k/5t1RcoDGD3cY2j/6tN2JEqrFPcmWEV2Hq7iPW8nr7yZkRb+K6p3ttSozIzazZgQ71YLr2a5J7aoBiO5OTt28PZ5xxxtrJ8bl7vIFrSR72GeIl/LkTllkZsW2d+XWLndjwwgvOXzvfJxdEeMJj7vpUHrYgsU+JgzrECF+MyLXDpjakHTbxjOuvzQXSOtCSs9F37d69dnZSTrS1QpOKFyV5eIKfPWS6L3FXjrXGr7JqU/7+4DVXD183jH3K20PTO2fBPqmDGO0kah/FiJR/y/krqR+xzefRvyFGvO0dG2Yq5Y0VshxX3z/uzW7Fs5by/7XX3xBe+PznrfmW3PkQdvbbS18T8Qaz8YBC9w7zer6mbn3y16fEiHj5vtS1KmU0PriLejB5+zXJITdAscvAdbmdpO291s7m6Z1Qndou5uWhPsx2aDLreP5jHj3MJhY7cyu0mrCY1WtzbVB9vhz0qUvCvdUr8bPH9aJpHnF9iM+UiQM/zoxIixFeG5FZO3lrUfz6wljEs+2uaVunHR6jWRV42+X+ukWt6kytOM2SPHKCn/zWl/62SljQVcZ2+xn2aa/HmwX7eGMSGwtr7OO9Ua49kpPJqcT32C1qGpvFpbMT58rPm7hUXyVvwJFP/Hp5+a6L40rEiEiMEEN5e7hjg3pihAQ0eiK9pJN7y0W8bcSrMLmBgb2/5GDNyTRDUo0JpMQIOxC89rrrh7fp6cPe9ozUoZbUg+7UuapVDtrhqF/YsXPn8A0I9pV28dPEotUt3/6edCcXAAAEU0lEQVS2uwRd74vzsIOglGhpB9l9fxND1Qy5tjf1uXWCiLivaJqH2tZbJae/2f2mKT/SnZYy2ZI0bYfSpuJXF8YljG3YNA/aYX0xwgpGEhed+/CHV4q2GmDrUubTTj11eE8qWI/zsAOAvsddVYNd/f3Hu3YNV0Yoa7GBnItk63os8GCf0X1f1+2TE8Pt2Cq3LX50StNLoUqM8HxPvM3MK72NNdQnzpN/670YMb0qS84QgAAEIAABCEAAAhCAAAQgAIF+EkCM6KfdeWoIQAACEIAABCAAAQhAAAIQgMDUCCBGTA09GUMAAhCAAAQgAAEIQAACEIAABPpJADGin3bnqSEAAQhAAAIQgAAEIAABCEAAAlMjgBgxNfRkDAEIQAACEIAABCAAAQhAAAIQ6CcBxIh+2p2nhgAEIAABCEAAAhCAAAQgAAEITI0AYsTU0JMxBCAAAQhAAAIQgAAEIAABCECgnwQQI/ppd54aAhCAAAQgAAEIQAACEIAABCAwNQKIEVNDT8YQgAAEIAABCEAAAhCAAAQgAIF+EkCM6KfdeWoIQAACEIAABCAAAQhAAAIQgMDUCCBGTA09GUMAAhCAAAQgAAEIQAACEIAABPpJADGin3bnqSEAAQhAAAIQgAAEIAABCEAAAlMjgBgxNfRkDAEIQAACEIAABCAAAQhAAAIQ6CeBqYgR/3f3T/tJew6fenl5KRw+fGQOn4xHmgcCS0tL4cgR6mdXbbm8tBQOY5+ummcs5VoIIQzGkhKJTJLA4sJCWB1gqUkyHi1tWtJo/CZ3N5aZHNtxpLywsBAG+LZxoJxYGouLi+F+J28LK5s3Nc5jYVBg3bt+tLsos8al4wYIQAACEIAABCAAAQhAAAIQgAAEOktABIkTt57QuHxFYkTjXLgBAhCAAAQgAAEIQAACEIAABCAAAQj8nABiBFUBAhCAAAQgAAEIQAACEIAABCAAgVYJIEa0ipvMIAABCEAAAhCAAAQgAAEIQAACEECMoA5AAAIQgAAEIAABCEAAAhCAAAQg0CoBxIhWcZMZBCAAAQhAAAIQgAAEIAABCEAAAogR1AEIQAACEIAABCAAAQhAAAIQgAAEWiWAGNEqbjKDAAQgAAEIQAACEIAABCAAAQhAADGCOgABCEAAAhCAAAQgAAEIQAACEIBAqwQQI1rFTWYQgAAEIAABCEAAAhCAAAQgAAEIIEZQByAAAQhAAAIQgAAEIAABCEAAAhBolQBiRKu4yQwCEIAABCAAAQhAAAIQgAAEIAABxAjqAAQgAAEIQAACEIAABCAAAQhAAAKtEkCMaBU3mUEAAhCAAAQgAAEIQAACEIAABCCAGEEdgAAEIAABCEAAAhCAAAQgAAEIQKBVAogRreImMwhAAAIQgAAEIAABCEAAAhCAAAQQI6gDEIAABCAAAQhAAAIQgAAEIAABCLRKADGiVdxkBgEIQAACEIAABCAAAQhAAAIQgMD/A+JoRMZPf0KvAAAAAElFTkSuQmCC', `mes` = '8', `anio` = '2024', `tipo` = '6', `semana` = '0'
WHERE `id` = '280' 
 Execution Time:0.087257862091064

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '9'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.18901109695435

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA3AAAAJWCAYAAAAZaGVIAAAAAXNSR0IArs4c6QAAIABJREFUeF7s3Q2cVFXh//EvPoSroK0JiiiWrk/5wJroFj5mpabL+sNcRTStldxE0RTYlv2l/QRdNwwNTRJdNrNcUVJyITPTnjRrFQ0r/flAZBChkqHyywVL+f/PxbPevdyZOTNzZ3bu3M+8Xr2S2XPPPed9zu7Od++95wzYuHHjRvFCAAEEEEAAAQQQQAABBBAoeYEBBLiSHyMaiAACCCCAAAIIIIAAAgh4AgQ4JgICCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45kDZCdxxxx06++yz+/Tr0Ucf1RFHHFFSfZ0xY4auuOIKTZ8+XZdffrmef/55nXHGGV4b77rrLu277745tzdYd84VpTjQGJvXWWedFXXVfep77bXXvHP89Kc/VSmOYUE73w+VRzFv7PdfY2Ojrr/+eq1YsSKyee1CEuX3kcv5KJNawM4nWyLsezhY5gc/+EGfnys9PT269NJLvSrMfKqoqJDrMXPnzvWOs3PRHGtf/p8t5r2RI0em/Llr22DqC7aP8UcAAQT6Q4AA1x/qnLMgAv5fsmEnsEGpICfPodK4Bjjb7mJ8kCHA5TCx8jiEAJcHHof2EQiGrGCIS/fz2v+zxQbyqVOn6tRTT/XCnA1m/hPaY1LV6w9xts6nn366T5tThbjf/OY3OvLII72yxfi5x1RCAAEEMgkQ4DIJ8fXYCNgPDCeccILMVYAPfehDXtv9HyRK6SpOFB+W+2NwCHD9oV6cc0YxJ4NX4PxXPYrTC87S3wJhf3hx+YOVnTv+n+E2PJmf3TvttNNmV3ODxzz33HNe2LJ1/OMf/0h5jA11xssGw+Af+oJX6ghw/T27OD8CCBgBAhzzoCwE0t02ZX8Bm9sUzW2Uqa7qpLr1a5dddvFucbzwwgtl/mIb/GDgf8+ERn9brrzySn3961/3jjMvf4DM9IHGfFgxtw++/PLLuummm7wgam4ltH8ltl/3v2dvuwz7IB78IBK8rch/zJ577tl7G6rt77bbbrvZX7/9f7EO+8u3S2AO/qXe3y6XK3DZepv6X3rpJc/S/2Et021ZwT8GmGOtU9jtgscff7yuvfba3vEaMWLEZn7+89v556/XnNOW8d8aHPyQGXZFIZN9cLxMneblv63X/DtYd6Yr2S63UPr/CLB8+XLvnOZlPxz7x8L/gdk/R21bzf+HXV0x7/tvRU7Xj1RXZPzfs2HzO5NF8Hsu+DPA/vD19zd4FaiQ8yLT97z9I1gufQ/+Ygn+4Scs6NtxMD9z7R/hzP+b7yMzlkuWLPF+LoWNtz1mzpw5m81h/7n9V/H8c8vvbH7eh42Nf46WxS9OOoEAArEVIMDFduhouF/A/pU27FmHoFS2AS54m42pz3w4f+WVV3qDmf9DpH3mJ+w4/wc41wBngkbwFXZ+/1+tU9UdbFPYMWEzy7i2traqpaWlz+1LYWEyeHy6v1inus3KfjDOJsC5evvbZ9qW7rassICVysf/vJe/LcZ43rx5XgBPd+tX2LOb9lzm2UjzATbYdhPws70dzNTherux/9Yx/7nTfZ9lE+CClmY+7bzzznrwwQf7fMmG0VTzJez7z7xnA1ymfqT7njXn/tjHPpbx1r10P2eCX7P9STcOtkwh54WLp2l7ptsW0/028s/PTD+fg3PHntv8v33+LXiu4DHf/OY3vQCXKvj7w5mtK9Uzbrbt+++/v1d0/vz53ELJRw8EECgJAQJcSQwDjchXIJvbtnIJcPbDgP/DVPC9sCtzNgD4PyDY97IJcPYY/wfR4Hv+v9wH6w677TH4nv23vx7b37C6wz4ghd36lOq5EjsOZuz9f233/4X9rbfeyriIif8Doot3qisc/vf9dZoP0vvtt19vO2y//fMoeAXOBDh/+LP1ffzjH99sIYbgFTbbDv8Vu7D3gseFtSvVVbhg/8yV6aDjlClTej+4hwWOVHVnE+DsfDFzwC5WE/Ze8Mqcf6zs94R9z9TlXwzI7+jSD/+4Br+n041f8GdY2NWk4Pdl2B+egu/de++93lWnQswLl+9565lN3/0W/p9Z6RYK8c+/4Fw28zMseAWPyfbqmm1n2M85+zP7d7/7nW677TbdfPPN3h9guIUy39/WHI8AAlEIEOCiUKSOfhcoZIAznbN/yQ+7VTP4QS3smQtTh/0gYz8UBm/1Cdbtv0XSfvAMC5/2PXOrpW2n/4PixIkTez8chw1UqkBpyob1Nxj8UgXibFZuC15FsoEomwDnHycXb1MmXRv9hqeffroXCvy3dtng6Q+cLisuBq8GBYOY/ypF2LxOdauXaU/wiopryLLPqYX1OdWVzVQfZLMJcGG3sIa9Fwxw/jLBMRw1alSfAGcDiEs//HW5LGhh6k51G2Xw9sngs7lh4+X//rTlH3jggc1uG4xqXoTdap3udvRUc9flF0C6n9GprtL5FzAJrngbdoxph71a6HJ7pP972Px32DHBq/QEOJfRpgwCCBRagABXaGHqL4pAIW+h9H9oD/ureqoAF/ywHyznGuD8wSwsrOUb4GxgsLcehV058oejVAHO30476GFX/uzX0t0+lkuAy9Y7GOCCYccflI477rg+CyPYZ4OC8y5VgAt7Fso6BANcqufi7BWIYIBLdXugqT/bAOev24bWVMEnVXDJJsCFXcVN916q+eR/P9sAl+o2Wb+dy/iF/aBLNTZhz/oFj7cB0j73VYh54RLg/H9ICrYx0zOA/vKpgqE/iAVDrv/5N/+2KqmOSfUHmbB+BsObvy/BuwP8z/8S4IryK52TIIBABgECHFOkLATS/dXY/lI/6qijvCtR2d5CmWuA84ce89+5XoGLMsClW9jC5cOc+RAV1RU4/4fb4PM+uQS4bL2DAS74wczvkSrAuYQV/wfFdLe5hl1Zy/Se/zbHbMxS/cEj7Apc0DXTDwwXE5dbes15Ut3mm8sVuEz9CLs92fY13a3EriEmLHSELbgR9M00B8KCveu8cPmetwEy3dwNtjn4sy64uJO9UyDsNmT/qqWmfaYue4u1OY/LMcGFeMLmW7qtAdL9UcS0IdOzfJm+R/g6AgggkK8AAS5fQY4vGQH7SzrTNgLZPs+Ua4DzPweVzzNw+QY48wHP2oQtm53u9rR0t1CG3eqWzTNwqRYsMM+ZZBNGsn0GLviBO+zDeb7PwAXDQvCDsv9DaD5X4Py3x9oQHBaMg9+k2T4Dl82iMsUIcLk+A5eqH3Z5evM9G/bh3GX8cgkx5nZrs+R92HOnth32GbhsrsC5zguXAHf33Xf3WdUxbO66zC87L8JWtQ27vdT+zBw+fHjv82/+n6Nhx4T9ocy8Z1fzDd4KH3xW1faDAFcyv9ZpCAIIpBAgwDE1ykYg3W1OppNhgSOs88EFKfIJcMH60/0VO9UzcFEEuOAzZrZd6VauNGXSBThbh39/prDb7VLdcpTuQ1KuAS4bb1vW9fa4sNUA7eqQYdsI+JewT7eSYD4Bzn+lJWwuu1xxDTsubNEcf7moVqHM9RbKdG0Om7OZVqG0ISlVvf5tNYJlUl2Bc7lF2NSVaoXH4FXpbAKc67xwCXD2Clw687CvBZ/H9P+8MIuSpPv+Nz+XzBYsZp6ZDbzt82+ZjjHfZ2HbnZhz++dsqrYFf0/YNmfzPG/Z/EKlIwggUNICBLiSHh4al4tA2Ifl4AfZ4Icr88vdLBVu/j+qAGfa7t8HLrgoQvDDUyEDnD+M2ZCV6kplpmfggoEn3ZLomfYi84+Vcf/a176mCRMmeHvfmQAUtpBLcE743bLxDtYT/FAXFjz9ZczXzct1ERP/scY4uDCKXawimw/q5upq2HiYfe5MuzLd3hdsk+lP8Paz4IfmTLePFeMKnOmXbWvwQ3eq26nT9cMlYGcaP/tcpH9eueyflmn/xFxuoXSdFy4Bzn/btLVOtahP8Hsq6Bq2D2bYz3jzs+niiy/WySefHLp3Zqpj7K2WYT/f7TYE2fyhjwCXy29gjkEAgWIIEOCKocw5EiWQ7nm8REEUqbPF8E5121iqxRGK1PXEnQbvxA05HUYAAQQQCBEgwDEtEIhYoBiBIuImx7q6Ynmnu+0q05XGWAOXUOMJcCU0GDQFAQQQQKDfBAhw/UbPictVoFiBolz9su1XMb3DQhzhLdsRy708AS53O45EAAEEECgfAQJc+YwlPUEAAQQQQAABBBBAAIEyFyDAlfkA0z0EEEAAAQQQQAABBBAoHwECXPmMJT1BAAEEEEAAAQQQQACBMhcgwJX5ANM9BBBAAAEEEEAAAQQQKB8BAlz5jCU9QQABBBBAAAEEEEAAgTIXIMCV+QDTPQQQQAABBBBAAAEEECgfAQJc+YwlPUEAAQQQQAABBBBAAIEyFyDAlfkA0z0EEEAAAQQQQAABBBAoHwECXPmMJT1BAAEEEEAAAQQQQACBMhcgwJX5ANM9BBBAAAEEEEAAAQQQKB8BAlz5jCU9QQABBBBAAAEEEEAAgTIXIMCV+QDTPQQQQAABBBBAAAEEECgfAQJc+YwlPUEAAQQQQAABBBBAAIEyFyDAlfkA0z0EEEAAAQQQQAABBBAoHwECXPmMJT1BAAEEEEAAAQQQQACBMhcgwJX5ANM9BBBAAAEEEEAAAQQQKB8BAlz5jCU9QQABBBBAAAEEEEAAgTIXIMCV+QDTPQQQQAABBBBAAAEEECgfAQJc+YwlPUEAAQQQQAABBBBAAIEyFyDAlfkA90f3li9frqamJjU3N2vUqFG9TVi7dq0mT56slStXeu/V1tbqkksu6Y8mck4EEEAAAQQQQAABBGIpQICL5bCVbqPXr1+vGTNm6PHHH9c111zTG+BseBszZozGjh2r4L9Lt0e0DAEEEEAAAQQQQACB0hEgwJXOWJRFSxYuXKg5c+Z4ffEHOPP+okWLNGvWLFVWVnpfX7JkiVfW/15ZINAJBBBAAAEEEEAAAQQKJECAKxBsEqs1t05eddVVGjdunG655ZY+t1DOnj3bI/HfMmmvwk2cOLHPrZZJtKPPCCCAAAIIIIAAAgi4CBDgXJQok1HA3jo5dOhQmdsk/c/A2a+Z5+HM7ZP2xW2UGVkpgAACCCCAAAIIIIBAHwECHBMiEgH/7ZAmmBHgImGlEgQQQAABBBBAAAEECHDMgWgFgrdCBlehLPQVuHXr1umFF16ItlPUhgACCCCAAAKxEzjwwAM1cODAnNq9dsNGfeP3b+d0bBQH1e+1lQ4dsmUUVVFHmQtwBa7MB7gY3Qs+31bsALdhw4ZidJNzIIAAAggggEAMBMo5wJk/mk+YMEFLly7tHYm2tjbV19fHYGRoYlQCBLioJBNaT3BvtyCD3euNRUwSOkHoNgIIIIAAAjERKPUrcAsWLPAWiOvs7FRNTY2nagPdiBEj1NraqoqKipho08x8BAhw+ehxbKhA2EbebCPAZEEAAQQQQACBUhYo5QC3bNkyNTQ06Nprr+0Nb9bSfm3SpElciSvlCRZh2whwEWJS1SaBsABnr9SNHDnS20qAFSiZLQgggAACCCBQSgKlHOBmzpyp1atXh15l6+npkQlxBx10kMdprtQ99thjfcp2d3dr6tSp6ujoUFVVlUx99jV37lzvP6urq9Xe3q5bb71V9r3GxkZvYTrzCjumrq6OK3/9MIkJcP2AXu6nDAtwps/B2y3t7ZXl7kH/EEAAAQQQQKD0BUo1wJmA1tLSotGjRztdYXMNcCak2efn7FW8VatWbfaevbJnApw5xoY62y4zsty+Wdz5TYArrjdnQwABBBBAAAEEEChBgVINcPY5t3HjxkUa4MxVOXPFrbKyUmFhzL43bNgw7yqcCXD+Y8wQpru1swSHuGyaRIArm6GkIwgggAACCCCAAAK5CiQtwAVvybS3SNpbJsMCnLG1Xzf/ne3VwVzHhuP6ChDgmBEIIIAAAggggAACiRco1QCXbUhyvYWSABffKU+Ai+/Y0XIEEEAAAQQQQACBiARKNcCZ7qVbxCT49cWLFzstYhJFgMv29s6Ihirx1RDgEj8FAEAAAQQQQAABBBAo5QCXzTYC5grc/Pnze59vMyNr3rvxxhv7rEKZS4ALewbOPh9nVrfkVRwBAlxxnDkLAggggAACCCCAQAkLlHKAsyHMZSNvE7LGjx/fu+G3DX+mDv82ArkEuLBVKO0iJyU8tGXXNAJc2Q0pHUIAAQQQQAABBBDIVqDUA5zpj3+5f9s/uxWAv7/mipsJe+Zl9nczV8m+8Y1veLdi2n3gcglw5pjBgwfrjjvu8Or27xNnQ2ZwD7psx4HymQUIcJmNKIEAAggggAACCCBQ5gJxCHD9OQSZnsPrz7Yl7dwEuKSNOP1FAAEEEEAAAQQQ2EyAAJd+UhDgSuebhgBXOmNBSxBAAAEEEEAAAQT6ScAEuAV//k8/nV06dMgWOnTIlv12/kwnJsBlEire1wlwxbPmTAgggAACCCCAAAIIIIBAXgIEuLz4OBgBBBBAAAEEEEAAAQQQKJ4AAa541pwJAQQQQAABBBBAAAEEEMhLgACXFx8HI4AAAggggAACCCCAAALFEyDAFc+aMyGAAAIIIIAAAggggAACeQkQ4PLi42AEEEAAAQQQQAABBBBAoHgCBLjiWXMmBBBAAAEEEEAAAQQQQCAvAQJcXnwcjAACCCCAAAIIIIAAAggUT4AA52i9du1aTZgwQU1NTaqpqXE8imIIIIAAAggggAACCCCAQHQCBDhHy7AAZ98bN26c6uvrHWuiGAIIIIAAAggggAACCCCQmwABztEtmwDX3d2tmTNnqr29XZWVlY5noBgCCCCAAAIIIIAAAgggkF6AAOc4QwhwjlAUQwABBBBAAAEEECiIgP08unTp0t76GxsbvUd8/C9zMWH8+PGhbejs7Ez5OFDw7rKenh61tLSoq6srZX9MfeaV6nz2wLq6OrW2turGG2/03po0aZJX94oVK9Je9DAXRUx//BdGli1bpoaGBq1ataq3XW1tbYm5I44A5/jtRYBzhKIYAggggAACCCCAQOQCNpT5g4oNWMEQZMpOnTpVHR0dqqqq6m2LrSNViEsV4IYNG7ZZSEzXwQULFmj+/PmhwcwEMvMyoTOsT/56bVCrra3tPb+pu7m5Wf4+2HaPGDHCC4kVFRWR+5dShQQ4x9EgwDlCUQwBBBBAAAEEEEAgUgEbZK699trQq2cmFK1evbo3vKQKcDbwjR49OvRqVbEDXKbgZcKauWJng2iqfhls27dsw2akA1WkyghwjtAEOEcoiiGAAAIIIIAAAghEKhAMaMHKgwEvLgHO9CMY0mzfbCAz/zZX1czL3HKZLqClC3iRDkg/V0aAcxwAApwjFMUQQAABBBBAAAEEIhPIdNXMnChYJt0tlObWxlS3GRb7Cpxpuw2f5pk4/6ruwVDqsqWXS5nIBqYfKyLAOeKHPTRqDw0+NMkqlI6oFEMAAQQQQAABBEpE4HX9XS/pyX5tTbXGbHZ+l22rgrcPplvEpLq6OuWiIdkuYpKqLtdn4Gxnw64wBuvIdBupqcvFql8HOKKTE+BygEwX5mx16b45cjglhyCAAAIIIIAAAggUUMAEuNmqK+AZ0ld9ir6uKANc2CImYYuC+FvVH1fgzPmDi6vYdtTU1PQuXkKAe3+kCHCO36ZmIt16663ekqepVrbxBzsCnCMsxRBAAAEEEEAAgRIQMAHue/pyv7XkGH0pNMBFeQul6Vy6q2P9FeDCriAGA6jL7ZEuZfptgCM8MQHOETMpE8KRg2IIIIAAAggggAACRRKIahGTYIAzFyf8q1f2V4DL1C7zdZdVJlnEpEgTMi6nIcDFZaRoJwIIIIAAAgggUF4CmW4fdN1GwAalxx57zFvIZPHixX32awt+3nUJTWHS2T4DZ+qw5z7vvPM0b94879ZJcwul/8U2Aps0uALn+P1NgHOEohgCCCCAAAIIIIBA5AJRbOQdtt3A+PHjezfFDgakYgY4A2aC6Ny5c5XuUSQ28ibAOX9z2QC3Zs2azXa1d66EgggggAACCCCAAAII5CgQtpBeY2Nj70Ifttp0q1B2dnb2ubJlA5E5dvjw4X0+59oA19XVlbLFYefP5QqcOUGqLQWCJ7flVq1a1ful4KrwORLH4jCuwDkOU7qVJ4OT3bFKiiGAAAIIIIAAAggggAACWQkQ4By5wm6h9P/FIlgNq1A6wlIMAQQQQAABBBBAAAEEnAUIcI5Urs/A2VBHgHOEpRgCCCCAAAIIIIAAAgg4CxDgHKlcA5xjdRRDAAEEEEAAAQQQQAABBLIWIMA5khHgHKEohgACCCCAAAIIIIAAAgUTIMAVjJaKEUAAAQQQQAABBBBAAIFoBQhwjp7mCpzZrX7SpEmqqKhwPIpiCCCAAAIIIIAAAggggEB0AgQ4R8t02wjU1dV5u9kT7BwxKYYAAggggAACCCCAAAI5CRDgcmJ7f6NB/waC/qpYhTJHWA5DAAEEEEAAAQQQQACBlAIEuIgnh92xfsWKFWpvb1dlZWXEZ6A6BBBAAAEEEEAAAQQQSKoAAS6ike/u7tb48eO92tra2lRfXx9RzVSDAAIIIIAAAggggAACCGwSIMDlORMIbnkCcjgCCCCAAAIIIIAAAgg4CxDgnKn6Fly2bJkaGhpknoHjiluOiByGAAIIIIAAAggggAACWQkQ4LLi6rt4CcEtSzyKI4AAAggggAACCCCAQF4CBDhHvlTbCDQ2NqqpqcmxFoohgAACCCCAAAIIIIAAArkLEOBysEu3J5ytjr3hcoDlEAQQQAABBBBAAAEEEEh9GHnKAAAgAElEQVQrQICLcIL4gx37wEUIS1UIIIAAAggggAACCCDgCRDgHCeCDWfmdsmamhrHoyiGAAIIIIAAAggggAACCEQnQIBztAwLcPa9cePGse+boyPFEEAAAQQQQAABBBBAIHcBApyjXTYBzuwNN3PmTLW3t6uystLxDBRDAAEEEEAAAQQQQAABBNILEOAcZwgBzhGKYggggAACCCCAAAIIIFAwAQKcIy0BzhGKYr0CP9Y1+ogO10f1KVQQQAABBBBAAAEEEIhEgADnyEiAc4SimCfwlt7QHNXrX/qnPqxDdbBOVrXGaIAGIIQAAggggAACCCCAQM4CBDhHOgKcIxTFPIHVek6/1316Sj/SO/q3994w7aeROlkjVattNBgpBBBAAAEEEEAAAQSyFiDAOZIR4ByhKNZH4J9a4YU4E+bMVTnz2lG7vxfkTtYOGoYYAggggAACCCCAAALOAgQ4Ryr/Jt3BQ9ra2vpsI8AqlI6oCSr2f/pHb5B7Xau9nm+nHXuD3FBVJUiDriKAAAIIIIAAAgjkKkCAy0EuXZiz1VVXV7ONQA625X7I23rLC3Lmf2u03Ovu1hroBbmDdLJGaGS5E9A/BBBAAAEEEEAAgTwECHB54AUP9Qc7AlyEsGValb218m/6Y28PD9QJXpir0ugy7TXdQgABBBBAAAEEEMhHgADnqOcPZ8OHD1dHR4eqqrjtzZGPYmkE/qQH9Xv9SMv1eG+pvXWkF+QO0GewQwABBBBAAAEEEECgV4AA5zgZbIBbs2aNmpubZZ57W7VqVe/RwefgHKulGAK9Ai/qUe/Wyuf0y973RugQL8h9TP+FFAIIIIAAAggggAACIsBlOQnMAiXjx4/3Alx9fb0WLFjgBbrgi1sos4SleK/AX/WUd0Xuad3f+94u2sfbfqBG4zRAW6CFAAIIIIAAAgggkFABAlyOA2+DW/DK27Jly9TQ0KAhQ4awiEmOthy2ScDsJWcXPHlX//Heq9Rw74rc4TpDFdoBKgQQQAABBBBAAIGECRDg8hzwmTNnau7cuers7FRNTU2etXE4ApsLrNXftET3elflevSmV2BbfdALcqNUrx21G2wIIIAAAggggAACCREgwEUw0D09PWppadGTTz7J4iYReFJFuMBbWqsluse7KveGXvYKbaUP6GCdpEP1Oe2q/aFDAAEEEEAAAQQQKHMBAlyEA2yD3IoVK7h9MkJXquor8I7e1hP6oXdF7tX39pIzJQ7Q8TpUY/URHQYZAggggAACCCCAQJkKEOAKMLBmxcrp06friiuuUGVlZQHOQJUIbBJ4Sgv1lO7TKv2pl8TsIWdWrdxfx8GEAAIIIIAAAgggUGYCBLgIBtQ+B+evir3iIoClCmeBZ/SgntSP9BffXnK7a6QO0Sk6RHXO9VAQAQQQQAABBBBAoLQFCHBZjI9/M2//YXV1dWptbVVFRUUWtVEUgegFlukxPal7++wlt7P29rYg+ITOiv6E1IgAAggggAACCCBQVAECnCO3P7wR2BzRKNZvAiv1tLfgyR98e8l9ULt6K1cepQZtqa37rW2cGAEEEEAAAQQQQCB3AQJcFnZht0r6D2fz7iwwKVoUgVe1TE9ogX6v+/TOe3vJVWh774rckfqCttOORWkHJ0EAAQQQQAABBBCIRoAAl6ejXXmyq6urT02EuTxhOTxSgde12gtyZguC9e/tJbeltvK2IDBBbkeNiPR8VIYAAggggAACCCBQGAECXGFcqRWBkhQwG4E/obv1pBbqTb3S28aP6tM6QudoV320JNtNoxBAAAEEEEAAAQQ2CRDgIp4J9jbLpF2BW758uZqamvTGG294orW1tbrkkkv66JrnCCdPnqyVK1emLBPxcFBdCoGNeleP6y4vyK3x7SW3lz6h0Tpbe6oGOwQQQAABBBBAAIESFCDA5TEo6Z6JS9JCJzaYjRkzRmPHjpX998iRI3tDXKoy9pg8hoFD8xRYqi5vwZNVeqa3pt10sD6h8TJX5nghgAACCCCAAAIIlI4AAc5xLFJtIWAON2Ft8ODBGjRokHcVKmmvhQsXatGiRZo1a1bvxuVLlizRnDlzet9zKZM0t1Lr77P6uZZogf6iJ3qbNlR76XCN06EaW2rNpT0IIIAAAggggEAiBQhwjsNuAtyUKVM0bdo0VVVVbXaUuRpnXkkMcGGEwcA2e/Zsr5j/tkp7VW7ixIkaNWqU40hQrNACy/W4d3vl8/pV76l20C46XGdotD5f6NNTPwIIIIAAAggggEAaAQJcFtPDf8tkZ2enamref06IAPc+ZPB2yfXr12vGjBleSDO3WNpXsFwWQ0HRIgiYWypNkPPvJbeNBusw1esonaetNbAIreAUCCCAAAIIIIAAAn4BAlwO88Ef5Nra2lRfXy8CnGSD2uOPP67dd9+99/bJQge4devWacOGDTmMJIe4CKzdcqX+WNGlZ7a5X+/qHe+QLbSFDlhfq1Fvnalt3610qYYyCCCAAAIIFFzAPNIycCB/YCw4NCfoVwECXB78wUVMGhsbuYXyPU/zDJwJt8Zo1113LegVOBPgVq9encdIcqiLQM9Wa/XCB3+qZR98WG9v8a/eQ/Z48wgd+M+x2v7tXV2qoQwCCCCAAAIFE9hjjz0IcAXTpeJSESDARTASYVfkIqg21lXYq25Dhw6VCbbcQhnr4ezT+Lf1lh7X3d6CJ2/49pLbR0fpKDVoNx1UPp2lJwgggAACCCCAQIkJEOAiHBCC3PuY/gBnFi5hEZMIJ1oJVfWEFnhh7h/6S2+rRqjaC3JVGl1CLaUpCCCAAAIIIIBAeQgQ4AowjibIdXd3q729vXdZ/QKcpmSqNOHs1Vdf1eWXX65tttnGa1dwgRK2ESiZ4SpIQ8xCJ2bBE/9ecmYLgqN1ng7Q8QU5J5UigAACCCCAAAJJFCDAJXHUI+7z8uXLvWf/zjrrrN5VJk2oe/rpp3sXMglu7s0KlBEPQolU97x+rcc1X2YrAvvaXkN1tCboUJ1aIq2kGQgggAACCCCAQHwFCHCOY5dqI+/q6urEXGlLR2VD3BtvvOEVO/zww/tckTPv2dC2cuVKr0xtbW2ffeEch4JiMRB4SU/qCd0lszm4fQ3Udt6tlUfo3Bj0gCYigAACCCCAAAKlKUCAy2FcUoU5f1UEuxxgOaTsBFbrOe8ZuaXq6u3bAA3wQpy5vXJrVZRdn+kQAggggAACCCBQSAECXIS6/mBHgIsQlqpiL7BWf1O37vKek9uod3v7c5hO867KDdbQ2PeRDiCAAAIIIIAAAsUQIMBlodzT06OWlhZ1dW26mlBXV6fW1lZVVHAVIQtGiiZY4C2t9YLcE7pbPXrTk/iQ9tBFuifBKnQdAQQQQAABBBBwFyDAuVt5m1KbDaNtaAtbbdKsPjl+/HhxBS4LWIomTuAdve0FuZ9pttf3L2qeRmhk4hzoMAIIIIAAAgggkK0AAc5RzN4eaVZbrKmp8Y7yv2f+bYKbfXV2dvaWczwFxRBInMC3VKs39LI+qQu8Z+J4IYAAAggggAACCKQXIMA5zpCwABe8pbKxsdFbTp8XAgi4Cdyvmd7tlB/RYTpH33E7iFIIIIAAAggggECCBQhwjoMfFuDMoeY2yrlz54orbo6QFEPAJ/Bn/VY/0CSZlSkv1n36oHbFBwEEEEAAAQQQQCCNAAHOcXqkC3D+5+Icq6MYAgi8J3ClRnn/dYquULXqcEEAAQQQQAABBBAgwOU/B9IFOFM7t07mb0wNyRTo1Ff0oh7VwTpJYzU9mQj0GgEEEEAAAQQQcBTgCpwjFAHOEYpiCGQp8KTu1WK1envBfUVd2kJbZVkDxRFAAAEEEEAAgeQIEOAcx9q/SXfwEBYvcUSkGAIhAuu0Rtfps95XztaN2kufwAkBBBBAAAEEEEAghQABLoepkS7M2erYBy4HWA5JrMAcna41Wq4jdK4+rUmJdaDjCCCAAAIIIIBAJgECXCahLL7uD3YEuCzgKJp4gYd0o36j72m4DtAEfS/xHgAggAACCCCAAAKpBAhwzA0EEOh3gZV6Wh3vbeR9ge7SUO3V722iAQgggAACCCCAQCkKEOAiHhW7LxxX4CKGpbqyF2jVkfq31utETVGNxpV9f+kgAggggAACCCCQiwABLhe1946xYS2sirq6OrW2tqqioiKPM3AoAskR+KFa9Iwe1H46Vmfom8npOD1FAAEEEEAAAQSyECDAOWKlW7jEhLXBgwdr0KBB7Afn6EkxBIICf9RPdK8u1zYapIvVpQptDxICCCCAAAIIIIBAQIAA5zglTICbMmWKpk2bpqqqqs2OMlfjzIsNvR1BKYZAQGCD/qU2HeO9e7pman8dhxECCCCAAAIIIIAAAS73OeC/ZbKzs1M1NTW9lRHgcnflSASsQLvO1So9o8N0uk5SEzAIIIAAAggggAACBLj854A/yLW1tam+vl4EuPxdqQGBX2uefqHvaCd9RBdqASAIIIAAAggggAACBLjo5kBwEZPGxkZuoYyOl5oSKPCyXtBcjfd6fp6+q910UAIV6DICCCCAAAIIIJBagGfgIpgdYVfkIqiWKhBIpMA3dbz+pX/qOE3UUWpIpAGdRgABBBBAAAEEUgkQ4CKcGwS5CDGpKrECXZqh3+s+7anD9XnNSawDHUcAAQQQQAABBMIECHAFmBcmyHV3d6u9vV2VlZUFOANVIlC+As/rV5qvydpCW3jbCeygXcq3s/QMAQQQQAABBBDIUoAAlyUYxRFAoPACV2qUd5L/0v9opGoLf0LOgAACCCCAAAIIxESAAOc4UKk28q6uruZKm6MhxRBwFbhdF+gvekLVqtUp+h/XwyiHAAIIIIAAAgiUvQABLochThXm/FUR7HKA5RAE3hP4nTr1U12n7bWzLtViSQOwQQABBBBAAAEEEDCfijZu3LgRiWgE/MGOABeNKbUkU+Cf+ptu/P83UJrX2bpJe6kmmRD0GgEEEEAAAQQQCAgQ4HKYEsH930wVdXV1am1tVUVFRQ41cggCCAQFbtApWqtVOlJf1Kd0IUAIIIAAAggggAACXIHLbg4sW7ZMDQ0Nqq2t3WzD7gULFqi5uVltbW2qr6/PrmJKI4DAZgIPaJa6dae3mbfZ1JsXAggggAACCCCAALdQZjUHzJU382pqago9rqenRy0tLRo2bFjKMlmdkMIIJFjgL3pct2uiJzBRd2uI9kywBl1HAAEEEEAAAQQ2CXALpeNMsM+3jRs3Lu0VNlvOhLyaGp7bceSlGAKhAtN1uDbqXZ2kJh2m01FCAAEEEEAAAQQSL0CAc5wC2QSzTFfqHE9JMQQSLzBfl+l5/Vr76zidrk1XwHkhgAACCCCAAAJJFiDAOY5+NgHOPA83f/589odztKUYAqkEntKPtEhXaRttr69okQZqO7AQQAABBBBAAIFECxDgHIc/mwDX3d0tcxWuvb1dlZWVjmegGAIIBAX+pbX6pj7jvX2GrtV++iRICCCAAAIIIIBAogUIcI7DH9y8e/jw4ero6FBVVdVmNRDgHFEphoCDwM06U6/oRR2uM/RZTXU4giIIIIAAAggggED5ChDgshzbYJCzh/sDHQEuS1SKI5BG4Oeao0fUoaHaSxfoLqwQQAABBBBAAIFECxDg8hz+VIGuurqaWyjztOVwBIzAKv1J7fqChzFB39NwHQAMAggggAACCCCQWAECXMRDbwOdqZZn4CLGpbrECrTpGG3Qv/RpTdIROjexDnQcAQQQQAABBBAgwDEHEECg5AXu1df0Rz2gvfRxna1vl3x7aSACCCCAAAIIIFAoAQJcoWSpFwEEIhP4kx7UPWrRltpKF6tL22toZHVTEQIIIIAAAgggECcBAlycRou2IpBQgXf0H12lj3u9H6vpOlgnJVSCbiOAAAIIIIBA0gUIcEmfAfQfgZgIdKhBK/UHVatOp+iKmLSaZiKAAAIIIIAAAtEKEOCi9aQ2BBAokMCjuk0P69v6oHbVJeoq0FmoFgEEEEAAAQQQKG0BAlyW47Ns2TI1NDRoyJAhrDKZpR3FEchHYI2Wa45O96o4R9/RR3RYPtVxLAIIIIAAAgggEEsBAlwWw2bD26RJk1RfX997ZE9Pj1paWtTV1SX/ht5ZVE1RBBBwELhOn9U6rdFRatBxmuhwBEUQQAABBBBAAIHyEiDAZTGeM2fO1OrVq9Xa2qqKigrvSBvehg0bpqamJnV3d2vq1Knq6OhQVVVVFrVTFAEEMgn8WK1aonu1uw5WgzoyFefrCCCAAAIIIIBA2QkQ4ByH1G7QPW7cuD5X38xVORPcTLizgc38t3mZ93khgEB0Ai/oEd2pS70KL9I9+pD2iK5yakIAAQQQQAABBGIgQIBzHCQb4Ewoq6mp6T0qLKwtWLBA8+fP5xk5R1uKIZCNwJUa5RU/Wc0apdOyOZSyCCCAAAIIIIBA7AUIcI5DGBbgUoU6cxulCXbt7e2qrKx0PAPFEEDAReAHukh/1u90gD6t09TmcghlEEAAAQQQQACBshEgwDkOZfBZN3OYudL22GOP9Xkmzr7PFThHWIohkKXA47pbP9FMVWgHXaofa2ttk2UNFEcAAQQQQAABBOIrQIDLYuz8V9bMYRMmTFDwmTjzPs/AZYFKUQSyFFinV3WdTvKOOkOztJ+OybIGiiOAAAIIIIAAAvEVIMBlOXbmqltzc7N3VFtbW58FTcx7JuSNHz9enZ2dfZ6Vy/I0FEcAgTQC39apek0rVKMzdaImY4UAAggggAACCCRGgAAX0VDb5+GWLl1KeIvIlGoQSCXwoL6l3+oH2ll768u6EygEEEAAAQQQQCAxAgS4xAw1HUWgfARe0pP6nhq9Dn1Jt2tXfbR8OkdPEEAAAQQQQACBNAIEOKYHAgjEUmCGPq539R99RhdrtM6JZR9oNAIIIIAAAgggkK0AAS5bsQzlzQImc+fOVXV1NdsIRGxLdQj4Be5Wk/5XP1eVRuss3QAOAggggAACCCCQCAECXB7DbMNaWBV1dXWbbS+Qx6k4FAEEAgJLtUj36UptqQ/oUi3WdtoRIwQQQAABBBBAoOwFCHCOQ+xfpCR4iAlrgwcP1qBBg9TU1ORYI8UQQCAfgX+rR606yqviVM3QQfpsPtVxLAIIIIAAAgggEAsBApzjMJkAN2XKFE2bNk1VVVWbHcXeb46QFEMgQoFbdLZW6zkdolNUp8sjrJmqEEAAAQQQQACB0hQgwGUxLv5bJoP7vBHgsoCkKAIRCfxSt+hXukWVGq6LdV9EtVINAggggAACCCBQugIEuBzGxh/k7GbeBLgcIDkEgTwFzNU3cxXOvM7VXH1Yh+ZZI4cjgAACCCCAAAKlLUCAy2N8gouYNDY28gxcHp4cikAuAt/QcVqvN3WMvqRj39sbLpd6OAYBBBBAAAEEEIiDAAEuglEKuyIXQbVUgQACDgI/0tf1tH6sEarWF9XucARFEEAAAQQQQACB+AoQ4CIcO4JchJhUhYCjwLN6WAv0Va/0xfqRKrWb45EUQwABBBBAAAEE4idAgMthzML2f/Pv+2a+3t3dzUbeOdhyCAK5CFypUd5htWrRoTo1lyo4BgEEEEAAAQQQiIUAAS6LYVq2bJkaGhpUW1u72bNuCxYsUHNzs+yiJllUS1EEEMhT4Dadr7/qKR2g43WaWvOsjcMRQAABBBBAAIHSFSDAZTE2mVaa7OnpUUtLi4YNG8ZiJlm4UhSBfAUe0/f1M83WdtpRl+rH2lJb51slxyOAAAIIIIAAAiUpQIBzHBazkfeECRM0btw41dfXpzzKlmtqalJNTY1j7RRDAIF8BNZqlW7QKV4VZ+p67aOj8qmOYxFAAAEEEEAAgZIVIMA5Dk02wSzTlTrHU1IMAQSyELheJ+tNvaKPa7xO0GVZHElRBBBAAAEEEEAgPgIEOMexyibAmefh5s+fzyImjrYUQyAKgfv1DT2hBdpF+6hRnVFUSR0IIIAAAggggEDJCRDgHIckmwBnVqA0V+Ha29tVWVnpeAaKIYBAPgLL9Jju0MVeFSbAmSDHCwEEEEAAAQQQKDcBApzjiNoAt3TpUu+I4cOHq6OjQ1VVVZvVQIBzRKUYAhEL2O0EjtdX9AmdHXHtVIcAAggggAACCPS/AAEuyzEIBjl7uD/QEeCyRKU4AhEJdOoSvajfaB8dqTP1rYhqpRoEEEAAAQQQQKB0BAhweY5FqkBXXV2dqFsolyxZomnTpvVqHn744br88su1zTbb9L5nrCZPnqyVK1d675n99C655JI8R4DDEXhfYInu0Y91jbbSQF2mn6hC28ODAAIIIIAAAgiUlQABLuLhtIHOVJuUZ+BseLvmmms0atQorV+/XjNmzPBkbYiz4W3MmDEaO3asgv+OeBioLqEC6/WmvqHjvN6fqqt1kE5IqATdRgABBBBAAIFyFSDAlevIFrFfs2fP9s7mv5q2fPlybzPz5uZmL9QtXLhQixYt0qxZs3oXdjHBb86cOX3eK2KzOVWZCtykev1Df9HHNFZj9N9l2ku6hQACCCCAAAJJFSDARTzyZvXJuXPnKmm3UAYZ7RW2iRMnegEuLOQFy0Q8FFSXUIGHdIN+o9u1o3bXJC1MqALdRgABBBBAAIFyFSDA5TGyNqyFVVFXV6fW1lZVVFTkcYb4HmqurrW1tXnbKey6667eLZUmyJnbJ+2L2yjjO76l3PK/6Y+apy96Tfyi2jVC1aXcXNqGAAIIIIAAAghkJUCAc+RKtViJOdyEtcGDB2vQoEHebYNJfwWfgTMeBLikz4ri9r9VR+jf2qBjdb6O0fnFPTlnQwABBBBAAAEECihAgHPENQFuypQp3kqLYXu/mStN5kWAk3e75COPPOJdfdtzzz17FzUp1BW4devWafXq1Y4jSbEkCPxm2A1aMfh32rlnfx238vIkdJk+IoAAAghI2mOPPTRw4EAsEChrAQJcFsPrv2Wys7NTNTU1vUcT4DZRBMObec9ekStkgNuwYUMWI0nRchd4fuDDemjwtV43z/nn9zT43Z3Lvcv0DwEEEEBA8u6IIsAxFcpdgACXwwj7g5x5zqu+vt672mReSb4CFxbeLC+LmOQw0TgkD4GNulKHeceblSjNipS8EEAAAQQQQACBchAgwOUxisFFTBobGxMb4NKFN0PMNgJ5TDQOzUngVp2jv+tZHagT9DldnVMdHIQAAggggAACCJSaAAEughEJuyIXQbWxqcKEszvuuKP3mbewhtsVJ0eOHOntF8cKlLEZ3tg29Ndq1y90swbpQ5qsByQNiG1faDgCCCCAAAIIIGAFCHARzoUkBjkbxFauXBkqafaBs1sHBMvW1tb22fw7wqGgKgT0D72km3SaJ3GWblCVRqOCAAIIIIAAAgjEXoAAV4AhNEGuu7tb7e3tqqysLMAZqBIBBFwErtVn9JbWarQ+r8/oEpdDKIMAAggggAACCJS0AAHOcXjsPnAjRoxI9AbdjlwUQ6AkBLo0Xb9Xl4ZpP52vH5REm2gEAggggAACCCCQjwABzlHPv5F3dXV12qtrCxYs0Pz587kC52hLMQQKJfC8fqX5muxVf4Hu0lDtVahTUS8CCCCAAAIIIFAUAQKcI7MNcOedd57mzZunNWvWqKOjI3RTbwKcIyrFECiCwJUa5Z3lBF2mj2t8Ec7IKRBAAAEEEEAAgcIJEOAcbW2AM/u8HXzwwWppaVFXV5eCG3qb6ghwjqgUQ6AIAt/Tl/WSlmgfHa0zdV0RzsgpEEAAAQQQQACBwgkQ4Bxt/QGupqbGO8quOhnc/40A54hKMQSKIPA7deqnuk4f0La6TD/RQG1XhLNyCgQQQAABBBBAoDACBDhH17AAZw41Ya25uVl1dXW9i5sQ4BxRKYZAEQT+T69plk7wznSartEB+kwRzsopEEAAAQQQQACBwggQ4BxdUwU4c/iyZcvU0NCgIUOGeAuXPPTQQyxi4uhKMQSKITBbdXpdf9ehOlW1ainGKTkHAggggAACCCBQEAECnCNrugBnqrBfN4ubHHvssXrmmWdYhdLRlmIIFFrgAX1T3ZqvD2mELtK9hT4d9SOAAAIIIIAAAgUTIMBFSNvT09O7uEmmrQYiPC1VIYBABoGX9JS+p/O9Uufpu9pNB2GGAAIIIIAAAgjEUoAAV4BhM4ubdHd3cwWuALZUiUCuAtN1mDZqoz6pL+toTci1Go5DAAEEEEAAAQT6VYAA58jv38jbfwhX2hwBKYZAPwvcqcv0gn6tj2iUztHN/dwaTo8AAggggAACCOQmQIDLwS1VmCPY5YDJIQgUSeApLdQiXe2d7VLdr+01tEhn5jQIIIAAAggggEB0AgS46Cx7FzJZunSpuDIXISxVIRCBwH/0tq7WaK+mOl2hQ1QXQa1UgQACCCCAAAIIFFeAAFdcb86GAAL9KPAdjdOrWqaD9Fmdqhn92BJOjQACCCCAAAII5CZAgMvNLe1RZgETs5CJ2ROusrKyAGegSgQQyEXg55qjR9ShwRqiy/STXKrgGAQQQAABBBBAoF8FCHAF4CfAFQCVKhGIQOAVvaibdaZX0+d1k/ZUTQS1UgUCCCCAAAIIIFA8AQJcAawJcAVApUoEIhJo09HaoLd0hM7Rp3VxRLVSDQIIIIAAAgggUBwBAlwBnAlwBUClSgQiErhH/60/6acaro9qgm6PqFaqQQABBBBAAAEEiiNAgHN07unp0fr1652eaSPAOaJSDIF+EHhWD2mBmr0zX6R79CHt0Q+t4JQIIIAAAggggEBuAgQ4R7dUe78NHz5cHR0dqqqq6q2JAOeISjEE+kngSo3yznyiJqvmvWfi+qkpnBYBBBBAAAEEEMhKgACXFdemwmzknQMahyBQQgLz1KC/6Q/aV8donGaVUMtoCgIIIIAAAgggkF6AABfhDPEHOzbyjhCWqhCIWOBRfVcP6yYN1HaarJ9qa20T8RmoDgEEEEAAAQQQKIwAAa4wrtSKAAIlLNXkOHQAACAASURBVPCGXta3VOu18HTN1P46roRbS9MQQAABBBBAAIH3BQhwzAYEEEikwHX//wm4dfqHDlO9TtJXE2lApxFAAAEEEEAgfgIEuIjHbObMmZo7d664hTJiWKpDIGKBRbpaT2mhdtKHdaF+GHHtVIcAAggggAACCBRGgACXh6sNa2FV1NXVqbW1VRUVFXmcgUMRQKBQAsvVre/rQq/6L+l27aqPFupU1IsAAggggAACCEQmQIBzpEy38qQJa4MHD9agQYPU1NTkWCPFEECgvwXsdgLHaaKOUkN/N4fzI4AAAggggAACGQUIcBmJNhUwAW7KlCmaNm1anz3f7OHmapx5EeAcQSmGQAkImCtw5krcR3S4ztGcEmgRTUAAAQQQQAABBNILEOCymCH+WyY7OztVU1PTezQBLgtIiiJQIgKP6279RDM1QFvoMj2gQdqxRFpGMxBAAAEEEEAAgXABAlwOM8Mf5Nra2lRfXy8CXA6QHIJAPwts0L/UpmO8VvyXrtRIndzPLeL0CCCAAAIIIIBAegECXB4zJLiISWNjI7dQ5uHJoQj0h8C3dape0wodrJM0VtP7owmcEwEEEEAAAQQQcBYgwDlTpS4YdkUugmqpAgEEiiDwoK7Xb3WHttdQXar7i3BGToEAAggggAACCOQuQIBztLOrUJpFSvzPvvkPJ8g5YlIMgRIS+Lue1a06x2vRubpZH9aoEmodTUEAAQQQQAABBPoKEOAcZ0RYgLPvjRs3znsOzr5MkOvu7lZ7e7sqKysdz0AxBBDoL4EZqtG7ekdH6gv6lC7qr2ZwXgQQQAABBBBAIKMAAS4j0aYC2QQ4E95MiCPAOeJSDIF+FrhLU/WcfqHddKDO02393BpOjwACCCCAAAIIpBYgwDnODgKcIxTFEIihwB90vxbqCq/lF+tHqtRuMewFTUYAAQQQQACBJAgQ4BxHmQDnCEUxBGIqcOV7z76dpCYdptNj2guajQACCCCAAALlLkCAcxxhApwjFMUQiKnALTpLq/W89tMndYaujWkvaDYCCCCAAAIIlLsAAc5xhAlwjlAUQyCmAr/UXP1Kt2obDdYUPagttXVMe0KzEUAAAQQQQKCcBQhwjqNLgHOEohgCMRUwm3mbTb3N60xdp310dEx7QrMRQAABBBBAoJwFCHCOo2sD3NKlSzc7oq2trc82AqxC6YhKMQRKTOAb+qTWa50O1+n6rJpKrHU0BwEEEEAAAQQQkAhwOcyCdGHOVlddXc02AjnYcggC/SlgVqI0K1IO1Z66QHf3Z1M4NwIIIIAAAgggECpAgItwYviDHQEuQliqQqBIAi/oEd2pS72zNapTu2ifIp2Z0yCAAAIIIIAAAm4CBDg3J0ohgEBCBOx2Ap/SRTpSX0hIr+kmAggggAACCMRFgACXxUj19PSopaVFXV1d3lF1dXVqbW1VRUVFFrVQFAEESlngNn1Jf9Xvtadq9HndVMpNpW0IIIAAAgggkEABAlwWgz5z5kytXr26N7SZf5sFS9rb21VZWenVZP49fvx4cQtlFrAURaCEBH6rO/SgrtcW2lJT9DNVaPsSah1NQQABBBBAAIGkCxDgHGdAum0Empo2rVZngpt9dXZ2qqamxrF2iiGAQKkIvKXXda0+7TXnVF2lg3RiqTSNdiCAAAIIIIAAAqxC6ToHwgJc8JbKxsZG2TDnWi/lEECg9ASu10l6U6+qWmN0ir5eeg2kRQgggAACCCCQWAGuwDkOfViAM4ea2yjnzp0rrrg5QlIMgRgI/FhtWqIfagftoq9ocQxaTBMRQAABBBBAICkCBDjHkU4X4PzPxTlWRzEEEChhgZV6Wh06z2vhF3Sr9tAhJdxamoYAAggggAACSRIgwDmOdroAZ6rg1klHSIohEBMBu53AUWrQcZoYk1bTTAQQQAABBBAodwECnOMIE+AcoSiGQJkIdOpivajHtJsO1nnqKJNe0Q0EEEAAAQQQiLsAAc5xBG2AW7p06WZHsHiJIyLFEIiRwO/VpS5N91r8FS3SDhoWo9bTVAQQQAABBBAoVwECXA4jmy7M2erYBy4HWA5BoIQE3tU7mqFNW4HUqkWH6tQSah1NQQABBBBAAIGkChDgIhx5f7AjwEUIS1UI9JPATTpN/9BL+qg+pXp9o59awWkRQAABBBBAAIH3BQhwzAYEEEAghcBDukG/0e2q0Paaqoc0QFtghQACCCCAAAII9KsAAa5f+Tk5AgiUssAaLdccne418SzdoCqNLuXm0jYEEEAAAQQQSIAAAS4Bg0wXEUAgd4GrNVr/0duq0TidqCm5V8SRCCCAAAIIIIBABAIEuAgQqQIBBMpXYIGa9awe0s6q0pc1v3w7Ss8QQAABBBBAIBYCBLhYDBONRACB/hL4X/1cd6vJO/0FuktDtVd/NYXzIoAAAggggAACIsBlOQmWLVumhoYGDRkyRO3t7aqsrMyyBoojgEDcBK7UKK/Jx+sr+oTOjlvzaS8CCCCAAAIIlJEAAS6LwbThbdKkSaqvr+89sqenRy0tLerq6tLw4cPV0dGhqqqqLGqmKAIIlLLArTpHf9ez2suLbzeWclNpGwIIIIAAAgiUuQABLosBnjlzplavXq3W1lZVVFR4R9rwNmzYMDU1Nam7u1tTp04lxGXhSlEESl3gUX1XD+smbamtve0EBmq7Um8y7UMAAQQQQACBMhUgwDkOrN2ke9y4cX2uvpmrcia4mXBnr7qZ/zYv8z4vBBCIv8CbelXX6ySvI6fpGh2gz8S/U/QAAQQQQAABBGIpQIBzHDYb4Ewoq6mp6T0qLKwtWLBA8+fP5xk5R1uKIRAHgWv1ab2l13WI6lSnK+LQZNqIAAIIIIAAAmUoQIBzHNSwAJcq1JnbKE2wY5ETR1yKIRADgfs0XUvVpUrtqovVFYMW00QEEEAAAQQQKEcBApzjqAafdTOHmSttjz32WJ9n4uz7XIFzhKUYAjER+Iue0O26wGvtefqudtNBMWk5zUQAAQQQQACBchIgwGUxmv4ra+awCRMmKPhMnHmfZ+CyQKUoAjESsNsJHKPzdazOj1HLaSoCCCCAAAIIlIsAAS7LkTRX3Zqbm72j2tra+ixoYt4zIW/8+PHq7Ozs86xclqehOAIIlKDA99Sol/SkdtdINWheCbaQJiGAAAIIIIBAuQsQ4CIaYfs83NKlSwlvEZlSDQKlJvCEfqj71eY16zLdr8EaWmpNpD0IIIAAAgggUOYCBLgyH2C6hwAC0Qn8W+vVqiO9Cut0uQ7RKdFVTk0IIIAAAggggICDAAHOASmbIub5t7lz56q6uppVKLOBoywCMRGYrTq9rr97e8GZPeF4IYAAAggggAACxRQgwOWhbcNaWBV1dXWbrU6Zx6k4FAEESkTgAc1St+7UtvqgpuqhEmkVzUAAAQQQQACBpAgQ4BxH2v+MW/AQE9YGDx6sQYMGyWz0zQsBBMpX4GU9r7k6y+vg53WT9lRN+XaWniGAAAIIIIBAyQkQ4ByHxAS4KVOmaNq0aaqqqtrsKLYOeJ9k9uzZGjFihMaOHdvHyRhOnjxZK1eu9N6vra3VJZdc4jgCFEOgdATsdgKf0Fk6XpeWTsNoCQIIIIAAAgiUvQABLosh9t8yGdwmgAC3CXLhwoWaM2eOJk6c2CfA2fA2ZswY7/3gv7MYBooi0O8Cd+pSvaBHtLP20ZfV2e/toQEIIIAAAgggkBwBAlwOY+0PcnYvuKQHuPXr12vGjBl6/PHHPdFggDPBbtGiRZo1a5YqKyu9MkuWLPHCnv+9HIaDQxAousCf9KDuUYt33gv1Q+2kDxe9DZwQAQQQQAABBJIpQIDLY9yDi5g0NjYm8hk4G95Wr16tr33ta7rqqqtkr7RZXnNbpXn5b5m0V+FM2Bs1alQeI8GhCBRfwN5GeaImq0ZnFr8BnBEBBBBAAAEEEilAgItg2MOuyEVQbSyrCLs10gY8E9L8z8VxG2Ush5hGvyfwHZ2hV/Vn7a0jNF6b/kDBCwEEEEAAAQQQKLQAAS5CYYKcQp9tK3SA27BhQ4SjSFUIuAk8uuU8PbpVh7bSB3TJhp9oa23jdiClEEAAAQQKKjBw4MCC1k/lCPS3AAGuACNgglx3d3ciN/Lujytw69at0wsvvFCAkaRKBFIL9Ax8TQ8fuOk5uEP/3Khhr38MLgQQQACBfhY48MADRYDr50Hg9AUXIMA5Ett94Mzy+K2traqoqHA8MlnF+iPAJUuY3paSwDU6Wm/rLR2qU1X73qImpdQ+2oIAAggggAAC5SdAgHMcU/9G3tXV1Wmvri1YsEDz58/nCpxvHzgWMXGcaBSLlcA9+m/9ST/VjtpNk/SjWLWdxiKAAAIIIIBAPAUIcI7jZgPceeedp3nz5mnNmjXq6OgI3dSbADd5s1Uo2UbAcaJRLFYCy/SY7tDFXpu/pNu1qz4aq/bTWAQQQAABBBCInwABznHMbIBramrSwQcfrJaWFnV1dSm4obepjgC3eYCzt1aOHDnS20qAFSgdJx7FSl7AbifwSV2go3VeybeXBiKAAAIIIIBAvAUIcI7j5w9wNTU13lF21cng/m8EuM0DnPGyoW3lypWeX21tbZ994RyHgmIIlJTAPH1Rf9MfNUKH6Iu6taTaRmMQQAABBBBAoPwECHCOYxoW4MyhJqw1Nzerrq6ud3GTJAc4R06KIVA2At26Uw9oltefyXpQg7Rj2fSNjiCAAAIIIIBA6QkQ4BzHJFWAM4cvW7ZMDQ0NGjJkiLdwyUMPPZTYRUwcOSmGQNkI9OhNzdRxXn9O0f+oWrVl0zc6ggACCCCAAAKlJ0CAcxyTdAHOVGG/bhY3OfbYY/XMM88kchVKR06KIVBWArN0gv5Pr+lAnaDP6eqy6hudQQABBBBAAIHSEiDAOY5HpgBnqunp6eld3CTTVgOOp6UYAgjEQODHatMS/dC7fdLcRskLAQQQQAABBBAolAABzlE2LMDZ98aNG6f6+vremsziJt3d3VyBc7SlGAJxFzCLmJjFTMzrXN2sD2tU3LtE+xFAAAEEEECgRAUIcI4Dk02AM+HNhDjzPFxlZaXjGSiGAAJxFrDbCRyhc/Tp9/aGi3N/aDsCCCCAAAIIlKYAAc5xXAhwjlAUQyChAt/XRC3X49pF+6pRdyRUgW4jgAACCCCAQKEFCHCOwgQ4RyiKIZBQgae1WD/S/3i9n6SF2lG7J1SCbiOAAAIIIIBAIQUIcI66BDhHKIohkGABexvlSWrSYTo9wRJ0HQEEEEAAAQQKJUCAc5QlwDlCUQyBBAvcqLH6p1ZqHx2lM3V9giXoOgIIIIAAAggUSoAA5yhLgHOEohgCCRZ4WDfpUX1XH1CFmvRzbamtE6xB1xFAAAEEEECgEAIEOEdVG+CWLl262RFtbW19thFgFUpHVIohUGYCr+mv+rY+5/VqnK7Tvjq6zHpIdxBAAAEEEECgvwUIcDmMQLowZ6tjI+8cYDkEgTIQmK7DtVHv6jDV6yR9tQx6RBcQQAABBBBAoJQECHARjoY/2BHgIoSlKgRiJHC3mvS/+rl21AhN0r0xajlNRQABBBBAAIE4CBDg4jBKtBEBBGIj8Lx+rfm6zGtvozq1i/aJTdtpKAIIIIAAAgiUvgABrvTHiBYigEDMBOx2Ap/SRTpSX4hZ62kuAggggAACCJSyAAHOcXRSPffGrZKOgBRDIEECczVeL+sFfViH6lzNTVDP6SoCCCCAAAIIFFqAAJeDMIuY5IDGIQgkSOAx3a6f6Qavx1P1sLbVDgnqPV1FAAEEEEAAgUIKEOAi1GURkwgxqQqBGAv8S//UN3W814NTdZUO0okx7g1NRwABBBBAAIFSEiDAZTEawStv3D6ZBR5FEUiYQJuO1Qb9nw7WSRqr6QnrPd1FAAEEEEAAgUIJEOAcZXt6etTS0qJhw4apqanJO2rBggVqbm5WZ2enampqHGuiGAIIJEGgSzP0e92nwdpJl+mBJHSZPiKAAAIIIIBAEQQIcI7I9uqbCW/+sDZz5kytXr1ara2tqqiocKyNYgggUO4Cf9VTuk3ne938om7VCB1S7l2mfwgggAACCCBQBAECnCNyqgDX3d0tE+La29tVWVnpWBvFEEAgCQJ2O4Gj1KDjNDEJXaaPCCCAAAIIIFBgAQKcI3C6ADd16lR1dHSoqqrKsTaKIYBAEgS+qwlaoaUapv11vr6fhC7TRwQQQAABBBAosAABzhGYAOcIRTEEEOgVeEoLtUhXe//+ihZpBw1DBwEEEEAAAQQQyEuAAOfIF1yBsq6uznvu7Q9/+IO4AueISDEEEibwH72tqzXa63WtWnSoTk2YAN1FAAEEEEAAgagFCHBZitqVJ4OHsaVAlpAURyAhAtfpJK3Tq9pPx+oMfTMhvaabCCCAAAIIIFAoAQJcnrJmEZPx48dvVguBLk9YDkegTAQe1Lf0W/1AA7WtvqpfaoC2KJOe0Q0EEEAAAQQQ6A8BAlzE6suWLVNDQ4OGDBnCypQR21IdAnEUeEUv6mad6TV9vGZrbx0Rx27QZgQQQAABBBAoEQECnONA+J+BGz58OKtOOrpRDAEEJLudQI3G6URNgQQBBBBAAAEEEMhZgADnSGcD3Jo1a9Tc3Ky2tjatWrWq92jz7/r6esfaKIYAAkkSuFOX6gU9og9pD12ke5LUdfqKAAIIIIAAAhELEOCyBLXPvNnAxqImWQJSHIEECjyrh7RAzV7PL9BdGqq9EqhAlxFAAAEEEEAgCgECXI6KNrgFr7zxDFyOoByGQJkL2NsoP6NLNFqfL/Pe0j0EEEAAAQQQKJQAAS5P2ZkzZ2ru3Lnq7OxUTU1NnrVxOAIIlKvAt/U5vaa/ak8drs9rTrl2k34hgAACCCCAQIEFCHARAPf09KilpUVPPvkki5tE4EkVCJSjwK81T7/QdzRAA9SkX2gbDSrHbtInBBBAAAEEECiwAAEuQmAb5FasWMEWAhG6UhUC5SDwhl7Wt1TrdeU0XaMD9Jly6BZ9QAABBBBAAIEiCxDgCgBuVqycPn26rrjiClVWVhbgDFSJAAJxFLhao/Ufva1q1ekUXRHHLtBmBBBAAAEEEOhnAQJcBANgn4PzV8VecRHAUgUCZSawUFfoD7pfgzVUl+n+Musd3UEAAQQQQACBYggQ4LJQ9m/m7T+srq5Ora2tqqioyKI2iiKAQNIElutxfV8TvW43qEO76+CkEdBfBBBAAAEEEMhTgADnCOgPbwQ2RzSKIYDAZgJ2O4FjdL6O1fkIIYAAAggggAACWQkQ4LLgCrtV0n94dXU1i5dk4UlRBJIocKvO0d/1rIbrQE3QbUkkoM8IIIAAAgggkIcAAS4PPHOoXXmyq6urT02EuTxhORyBMhV4Qnfrfs30emeegzPPw/FCAAEEEEAAAQRcBQhwrlKUQwABBCIQeFtv6Rod7dVUp8t1iE6JoFaqQAABBBBAAIGkCBDgkjLS9BMBBEpG4Fp9Sm/pDX1Un1K9vlEy7aIhCCCAAAIIIFD6AgQ4xzFKtQIlt0o6AlIMAQR6BX6ia/W47tI2GqSv6pfIIIAAAggggAACzgIEOGeq9wumCnP+qgh2OcByCAIJETCLmJjFTMzr87pJe6omIT2nmwgggAACCCCQrwABLl9B3/H+YEeAixCWqhAoQwG7ncAndJaO16Vl2EO6hAACCCCAAAKFECDAFUC1u7tbZsuB9vZ2VVZWFuAMVIkAAnEX+IEu0p/1Ow3Rnpqou+PeHdqPAAIIIIAAAkUSIMAVAJoAVwBUqkSgzAT+qJ/oXl3u9epC/VA76cNl1kO6gwACCCCAAAKFECDAFUCVAFcAVKpEoAwF7G2UJ+gyfVzjy7CHdAkBBBBAAAEEohYgwEUtKokAVwBUqkSgDAVma4xe12pVabTO0g1l2EO6hAACCCCAAAJRCxDgHEV7enq0fv16p2faCHCOqBRDIOECv9DN+rXatYW2ULN+ra21TcJF6D4CCCCAAAIIZBIgwGUSeu/rqbYOGD58uDo6OlRVVdVbEwHOEZViCCRc4J9aqRs11lM4XTO1v45LuAjdRwABBBBAAIFMAgS4TEIhX2cfuBzQEnLIKz0bddtz/9baDRu9HlcOHKAv7Le1dq4YkBCBzN3EqK+RfQ7uYxqrMfrvPl+8/fl/69m173rvbbWF9NkRW+mIXbbMjJygEj/+63/0yOp3ent81LAtdfIeWyVIwK2rL77xru544d9a/x7VrtsN0MUHfcDt4ISUwshtoPkZ7uZEKQQKKUCAi1CXfeAixIxhVfaXWsVW8j4YBf8dwy5F3mSMNif9oabpGf1MO2gXfUWLewuY8Lb8zXd11j5ba+8dtlDw35EPTgwrNOHtt6+80xtsg/+OYZcK0mQbTPbcfguds+/WCv67ICeNWaUYuQ0YP8PdnCiFQKEFCHCFFqb+xAiEfXj8zcvv6Gcr/6PP7M6VEzMRMNr82+FFPapOfcX7woW6Rztpj9AP2PaD07BtB3gfwpP+SvUHkhv++LZHw9Wl92dIWPg334tPvPpO7x8Ikj6fMHKbAfwMd3OiFAKFFiDAFVqY+hMjYD4ArH5rY59bJvnQ3Xf4MQr/drC3UZqVKM2KlKmCv/U7d7+ttUvCb8u1V0wOG9r3lkmCyeZzLCzUpvJLzA/sQEcxcht5foa7OVEKgUILEOAKLUz9iREI+wDAbZR9hx+j8G+HmzVer+gFnaqrdJBO9K5Uhl0d4TbK9/1ShVxuo+w7x1L9EYnbKN93wsj91zQ/w92tKIlAIQUIcIXUpe5ECfCLLfNwYxRu9FvdoQd1vU7WNI3S5whwmadSyquUBDgCnMP06VOEAOcuxs9wdytKIlBIAQJcIXWpO1EC/GLLPNwYhRu9pdc1W3X6rKZopMbo/r++wxW4DNOJK3CZv99MCcJJZieMMhvZEvwMd7eiJAKFFCDAFVKXuhMlwC+2zMONUWqjVh2pE3SZPqb/0v1/fZcAR4DL/A3lUIJwkhkJo8xGBDh3I0oiUAwBAlwxlDlHIgR4uDvzMGOU2mixWrWjdtfhOkPdL28ZunppmF9m9fIswSIm7uPKAh2ZrTDKbGRK8DPczYlSCBRagABXaGHqT4wAyytnHmqMUhut0NN6Rg/qk2rUyjcGeZsu2327zFGsaNrXjm0EMn+/2RIskZ/ZCqPMRqYEP8PdnCiFQKEFCHCFFqb+xAjYD5Smw1/Yb9M+Xbc992/Zjb0TA5GmoxilnwUdOk8nq1k7a2/vL90vvPFu7ybVrEC5uZ35MPnI6nd01LBNWwmwgEn4/LJXK3fcZoC3Px4rUG7uhJHbbyh+hrs5UQqBQgsQ4AotTP2JErC/3NZu2Oj1u3LggD77wiUKI0VnMUo9C27T+dpXR+sw1WsrDfRC3LNr3/UO2GoL9YY55tH7AjbE2XdsmMOor4ANKOvf2fT+rtttCnO83hfAyG028DPczYlSCBRSgABXSF3qRgABBLIQ+L269Ijm6ZP6sg7UiRqgAVkcTVEEEEAAAQQQSIIAAS4Jo0wfEUAgFgIb9a6m63CvrefqZn1Yo2LRbhqJAAIIIIAAAsUTIMAVz5ozJUhg3bp1uvPOO3X++ecnqNfZd/WWW27RmWeeqcGDB2d/cJkeMUsn6P/0mvbRUTpT13u9XLx4sfbee2/tu+++Zdrr/Lv1q1/9Stttt51GjSL0ptN8/vnn9eKLL6q2tjZ/9DKt4e9//7vMfDI/m3ilF+BnODMEgf4RIMD1jztnLXMBApzbAPPLf3Onh3SDfqPbtaW20jQ9oi21NQHOYToR4ByQJBHgMjsR4DIb2RL8DHe3oiQCUQoQ4KLUpC4E3hMgwLlNBX75b+60Rss1R6d7Xxin67xFTbgCl3k+EeAyG5kSBLjMTgS4zEYEOHcjSiJQCAECXCFUqTPxAgQ4tylAgAt3uvK9Z99G6TRvWwECXOb5RIDLbESAczMiwLk5mVL8DHe3oiQCUQoQ4KLUpC4E3hMgwLlNBX75hzvdpSl6Tr9UpYbrYt1HgHOYTgQ4BySuwDkhEeCcmLxC/Ax3t6IkAlEKEOCi1KQuBAhwWc0BfvmHcz2nX+guTfW+2KhOLVn8AouYZJhZBDi3bz1uoczsRIDLbGRL8DPc3YqSCEQpQICLUpO6ECDAZTUH+OWfmsveRvkpXaTXF+9EgCPAZfW9laowAS4zIwEusxEBzt2IkggUQoAAVwhV6ky8ALdQuk0BAlxqpzmq1xr9RXvoY9ppcR0BjgDn9k2VoRQBLjMjAS6zEQHO3YiSCBRCgABXCFXqDBVYu3atJk+erJUrV3pfN/sQXXLJJWWpRYBzG1YCXGqnR3WbHta3vQKHLP6a9t17f/aBSzOtuIXS7XuOAJfZiQCX2YgA525ESQQKIUCAK4QqdW4mYMPbmDFjNHbsWAX/XW5kBDi3ESXApXZapzW6Tp/1Cuy7+GIdsncNAY4A5/aNlaYUAS4zIQEusxEBzt2IkggUQoAAVwhV6txMYOHChVq0aJFmzZqlyspK7+tLlizRnDlz+rxXLnQEOLeRJMCld5quw7VR72qPxefqE3sfT4AjwLl9YxHg8nIiwLnz8TPc3YqSCEQpQICLUpO6UgrMnj3b+5r/lkl7FW7ixIkaNWpUTnqNj+V0WMEP+sDb63TQC3fqyQPPL/i5Mp3ghTf6lthyC2mvwZmOKs7XD/3TLfrjPmfq7Q/0f4P+9pb01r/79nufHYrjkOosH6y6UtsOXaQdF43V6i1O0/9r725i7SruA4BPShvhJE7jOCR1HYUikLPhs0ZCcaWSKgmbOI6ERERYZAG0VCB2QIBFFkQBhKkUiUiRKWGRhUPDjnpRZVUvagUpRBQhIfOhVCDjtpEhwQQ7VRHV3GYec+fN+biP9+w77/yeFCn4no+Z3/zPufM/M2fuiW2fP6sF+q9TIfz2f+aLcOHHQzjnQ2e1WLOT/8Wxw+H3f/zRcPwza7uXrGcNFSuOQQAAGYdJREFUTv1vCK/9bv6In94Swic+vJ5nWduxtr95NHzqNy+FoxfsXdsB1mmvd98L4ZW35g/2px8O4TNb1ukEH+AwH/vd6+GCY4dn96Zl+HMPX1srHNiztv3sRaAFAQlcC63UeBlPnz4dvvvd786StDh9Mv2txzTKv/mXEE4Wne5l4Nr67snwzf/+SXh0x9lP4ELsXL+3DCqry/B3xx8NP/n0N8PJc85+AreMQp/61C/CF77w92H7oa+F58IXwi/fu2YZi7kUZfribw+Ht/7oo+GXW89+Aje73pYgqa01zK53jobPn34p/PMnz24CtxRB01GIHb9/PcR4ivempfhzD1+4GeLDt4N/vfBudiDQjIAErpmmaregG57AvfRMeO8/frFUQFv/JIRdHw/hmRNLVaylK8zu7SG8+NZyJuHLgrXvH/4xfOwXfxne3fp2OPX5F5elWEtXjmgU/96+8pdLV7ZlKtCWo7vCOSc/xqmnUT78+o4Q/yeW+iP3xCs7wul/2rt09/APXf61sOv8P5fALdONR1nWXUACt+6kDlgKnIkELvz7IfAENqXAX93zb+GTF76xKeumUgQItCtw4uj2cGT/Es5TvPpvJXDthpWSjxSQwI2EstnaBTY8gVvCKZRr19qAPZd4+s0G1HbTHXL79mc2XZ1UiACBBQSW+B5+4sTuBSpy5jY1hfLMWTvT2RGQwJ0d98mddaMWMfnX/5wc5cIVfv43q3e5+BMLH2bT78BpuIlrRnFBnC3nDO87pS1efyeEN4rFXmL9XXfvR8Gpd0N45aR705jrwr1pjNLqbb74Z2vbz14EWhCQwLXQSpugjFP7GYFN0GSqQIAAAQIECBAgsIQCErglbJTNWKS04uRll102+ymB9ViBcjM6qRMBAgQIECBAgACBPgEJnPg4YwIpaXvttddm59y7d+/c78KdsYI4EQECBAgQIECAAIFGBSRwjTacYhMgQIAAAQIECBAgMD0BCdz02lyNCRAgQIAAAQIECBBoVEAC12jDKTYBAgQIECBAgAABAtMTkMBNr83VmAABAgQIECBAgACBRgUkcI02nGITIECAAAECBAgQIDA9AQnc9NpcjQkQIECAAAECBAgQaFRAAtdowyn28gs8/fTT4Yknngj3339/2LJly/IX+AyW8Mknnwx33333yhlvueWWcNddd53BErRxqoceeigcOHBgVtidO3eGxx9/PFx00UVtFP4slPLll18ON954Y9i/f3+46qqrzkIJlveUeSylUu7bt8/9qWiy8t508OBBsfQHo/hTQDfffHN49tlnq4H+4IMPhuuuu255LwIlI7CJBCRwm6gxVWV5BFJHcvfu3TpIlQ7SI488spKMpE5B7HBL4t7Hih3J+ADgscceC9u2bQvxv3O35Yn25SjJqVOnwr333hueeuqpoNM93ybJZs+ePTrYPeFaXmPxIdwNN9wgngYu8fhwIFqle9Vy3BGUgsDmFpDAbe72VbuzIJA/wfWEe1xHMn7533nnnUaYiifd119//UqHWye8/2LOrzsJ3LxVekgSH5AYmazHUZdRTE7in4dLdTdJ7lnoZDglgRCCBE4YEFhHgdSJjB3Iw4cPh+PHjxuBG+EbRyxjByl2lkwR7O9g5kndCNpJbJLiJ07vitO4TKGcb/bo88ADD4SHH354Nprrb7VATETi/cco0vjoSA+VduzYIcEdz2ZLAusiIIFbF0YHIbBaIHYGJHDjIqOcLjhur2ltxaje3nkn8tprr/UOXIWpfK8rbmJ2wDxUNDpy5Mgsfm677bZw7Nix2QZGc7vvs2ZOTOs7SG2XS0ACt1ztoTSbSEACN64xvQPX75SmKMWtLBLQP3Jy4sQJCVwlnOK96NChQytTlFPSGze1yNL/g6VFXvLE1vTA7nuTGBr3/WYrAhslIIHbKFnHnbyABG44BFIn4NVXXzV1aYDLdKXVQOV7S1ahHL7m0has5q26FuJwH6/HlPgZf63ZksBGCEjgNkLVMQn84YmuKZTDT3Alb+MvF1OWVne647+kBSZ0KsfHUkp+vVP5/ghc7X5t6nI9priMv9ZsSWAjBCRwG6HqmAQkcL0xYORtbZeIBO59t6HfpPLbgv0xJoGb90nvwJVTSiUq9TgyMrm2e7i9CKyXgARuvSQdh0Ah4AuuHhKSt+FLpWskSWey384I3Gqfrqm3Vn6dt+rycB/vjim/Kzh8L7cFgY0SkMBtlKzjTl7AF3/3k1s/+jp8eZTv5KTk5Pbbb/djzB18Erg6TLkYh4WDxo0q5T8L4/fzVo9++13B4fu4LQhslIAEbqNkHXfyAhK41SGQOthpie5yC0t2z4uklfHSv/IxArfWG2u+mmk8himmdcn8Jxd27ty5snLnWt03434elGzGVlWn1gQkcK21mPISIECAAAECBAgQIDBZAQncZJtexQkQIECAAAECBAgQaE1AAtdaiykvAQIECBAgQIAAAQKTFZDATbbpVZwAAQIECBAgQIAAgdYEJHCttZjyEiBAgAABAgQIECAwWQEJ3GSbXsUJECBAgAABAgQIEGhNQALXWospLwECBAgQIECAAAECkxWQwE226VWcAAECBAgQIECAAIHWBCRwrbWY8hIgQIAAAQIECBAgMFkBCdxkm17FCRAgQIAAAQIECBBoTUAC11qLKS8BAgQIECBAgAABApMVkMBNtulVnAABAgQIECBAgACB1gQkcK21mPISIECAAAECBAgQIDBZAQncZJtexQkQIECAAAECBAgQaE1AAtdaiykvAQIECBAgQIAAAQKTFZDATbbpVZwAAQIECBAgQIAAgdYEJHCttZjyEiBAgAABAgQIECAwWQEJ3GSbXsUJECBAgAABAgQIEGhNQALXWospLwECBAgQIECAAAECkxWQwE226VWcAAECBAgQIECAAIHWBCRwrbWY8hIgQIAAAQIECBAgMFkBCdxkm17FCRAgQIAAAQIECBBoTUAC11qLKS8BAgQIECBAgAABApMVkMBNtulVnAABAgQIECBAgACB1gQkcK21mPISIECAAAECBAgQIDBZAQncZJtexQkQIECAAAECBAgQaE1AAtdaiykvAQIECBAgQIAAAQKTFZDATbbpVZwAAQIECBAgQIAAgdYEJHCttZjyEiBAgAABAgQIECAwWQEJ3GSbXsUJECBAgAABAgQIEGhNQALXWospLwECBAgQIECAAAECkxWQwE226VWcAAECBAgQIECAAIHWBCRwrbWY8hIgQIAAAQIECBAgMFkBCdxkm17FCRAgQIAAAQIECBBoTUAC11qLKS+BJRJ48803w8033zwr0WOPPRa2bdu2RKVTFAIECBAgQIDA5hOQwG2+NlUjAmdM4KGHHgoXXHBBuO66687YOZ2IAAECBAgQIDBlAQnclFtf3Ql8AIE4+vbGG2+ECy+88AMcxa4ECBAgQIAAAQKLCEjgFtGyLYEJCDz55JPh7rvvXlXTgwcPhquuumru3+O2TzzxRHX6ZBydO3DgQLjlllvCXXfdVZVL26QPL7/88tFTMU+dOhXuvffe8NRTT60cu+9cZb127twZHn/88XDRRRetqlOt/nGjsnxpCumzzz5brd+DDz44NzpZ1rfcKd++Vr99+/aF+++/P2zZsmVl15dffjnceOON4dixY6vKUJ4/blArc80tbfe5z31u1TnjcZ5++ulw5513rhjWylszy8tw/fXXr/iMtUn13bt3bzWuUkz+4Ac/CPGYeXzUGikaXXHFFZ2GtTrEut9www1zh6tdH123izHXRr7v2FgYa5jHQFe5k/Pu3bs7Yy6Wse8aqh071WXHjh2r2i+5jr0PlLFfi+OyrWrXRBmTMebz9q3tU7ZJuU35+Zg6xfaL5TUdfQJftKpI4AMKSOA+IKDdCWwmgdTpiHXKE4XUUdq/f/9cEhc7HMePH6928GLSdvHFF4eTJ0+u+ryrExePd+jQoWqnMHdOnbK809SVcHS9p5eOUXYyu+oUz19+1uUSty0/6+u4ljGUypwnOGn/V199da6D15VE18qWkthaotjV5l2d9C6L22+/fS5prZXvg9j0dfL7jGumyb1MRvuu6Vp9avHYdYxY975rY62xsEh8pTLEc337299e9WAmxfrPf/7z8OUvfznceuutc8VKbR9jMdalfLDTl6B2XTOp/Fu3bg3PP//87ForH67khSiPk9o3liU9MErxnq7xtE8Zo/G4cdsjR46EK6+8MnznO98JffuUsZTKvmfPnlns1+5FQ8lZKmvtIc1m+o5RFwIE1kdAArc+jo5CYFMI9HVyUwcnJXZdHcb83+P7cbURutjhjR2a8knzmE7oeiZNZTmGzp86vqlzOdTxj9vFv9ih7Os8lsFTWqfPa+3TlXCWncq+spbbpg5tbLsvfelL4e23354bLak5dR2/duxy20VsUn1jR/+SSy6ZSxb74rcvboY616V/LWnpG41O+4+5NtYaC4sYpriPI00p6SiTox//+Mez6dEf+chH5oxT28WHOdEtf8gQj5HaIMbNM888E+655565RKwrTpJftI0ju+XDotIlv7bSZ/l1c/r06dkCS2X5atdWsouJ2w9/+MPBfWrXXH7c+BCqvO8ll1rs5COJXSOEm+ILRiUIEFg3AQnculE6EIH2BbpGpWLNyuSlq7Ocd9BOnDgxN80uCfUlE13JS9x3KMEqPx/qVJd16EsAagZDx887mUPJXh49Q1NTU1JYG3Uok4XYQY+jKLXObN85Uyc1Ts/8/ve/Hx5++OGVVUa7Esmu6V9lm5b1G2uTt+/VV1896ySPGSlOCekjjzyyanR3KKZyo6EEMXbO+0aOxlwbtQSub5pyioWxhnH71LZxWl+ZoEWPBx54IHz1q18Njz766FwCllvFUaw4hbmcCpknyocPH161yFEttnPXMbHadact4yyaxL98hLB2/jwhe+6553r3ife0eE2UCWb+MOjcc88NP/3pT8M3vvGNlenOXbGT/v2mm24KP/rRj6ojmu1/s6gBAQLrLSCBW29RxyPQsEBf4lCOYNRGNMrOcJn0JZrUafn1r389OF0y5+wbRSnZx3bMyyQrvvvS9V5Q7hM7abEDG//K99Liv5UdtqFkr1bP8847r/d9mLGjkfHY+ftqtRDN2yq+H5g656mjHkcyUke4TPSHrMu6lyMYY23Kjv4dd9wxl2DE49SStDxpKdtqKGkvrdL0wEVHSsZeG+X5UhsPxcJYw3IU8Fe/+tXc6Gps25gwfv3rX59Z5qPkZZJYGwUrk9Q8ye6aol27rmojg3231jH3hlqcdt2j0rnGPhTqmlWQjlNLsPPR6TgaOnSNNvzVougECKyzgARunUEdjkCrAkOd8LKDVOssl52UvmlD5WIaYxaBGOok5fZ95863yzuhfZ3g8j2nvo5/7X21vgUmaguq5NOq+hZcqSUs5chc36hmssg7stu3b58bsYv75x390mmo85xvH88XRwPzd5XG2pTnifvlP2MxNJ20tnBG7ly7dmsLY+Tl7Vs4Jz/eItdGVxIXF6rpioWxhnncxqShlmDFZD2OnuXvt9buD7W4Kts6T7Jr10zXaG406Fr8KPeJ54uLDnW5lNd6+Y5tLQnt2id/sFGWre8aSzFWLryTx2tt2mWr3yXKTYDAxgtI4Dbe2BkINCEwNBJRPqkuO8u1Dl7t/acSI20TVwsc6oSNSURqCUnXYghl+cZ2guM5Fun4DyXHfQGSJ7rlSnZ95c1HiIY6qfH8eXJcThMbavuh6Xt5u8UkJJ+CtohNeZ5y2lptSl+s23otYFK2U0oe4r/3jcit9dooz9cVC4sY5m0Zjx/fdYvvqcWVTaNnTNxq0yNrbVx7oFLeF2rvgebTD2sPTRa5zpPR0HuM8fMyeRsafSv36YujrmusK3mrjUqPTVqb+EJRSAIENlRAArehvA5OoB2BRTrhsVZlZ7lvilBt5KMrkYv/XpuSGP99kY7dUOcsT8Jih/LSSy+tvtPTNSpXq29XB2+RBSa6IqYcVVuk0z4mgesbDcgT3TiKULZ918hXrEuZJJdui9iU58lHWXft2tX5nl9fbA91/MdcwX1TN1NyXE6PW6T9uhK5NIq5iGEezzFRj++7xfcb45Tg+P+/9a1vzU43JsnuGlXMFw7Jk7wXX3xxbppg1/Uydjpo7tI1Cpyc44Iq5U8edF0XXft0jer3vQ8cp2SXo7Sl25gHXWPi0DYECExHQAI3nbZWUwK9An2dprKD0bX4R9fvoeVLY/clE0NJ5NAUyvzzWNmhhTvyhKBr1box7+1s27ZtxbZ8P6yrA19rjKGOXNn5ri2mUDvuUIe4NjWx/HmI9G5UTETiCE3qpA8lImUiXZZlqM1TfbrOk6Z3XnvttdXFJVLiX1sIZKjsuWVf7PWNzJRThcv26Vo2fpFYKBOjvgu9jPn77rtvtmx+PEYcfYtTA8s2yUcaa8cul9zPR9hSPWK8xJ8dyNthkRHvodt3LblK/xb3LVe8HXo/t7ZPVwJXi+FkVhuZ7au3nxEYammfEyAQBSRw4oAAgcHVHcuRrzHvwyXWssPeN4o2lKCNeacuf/o/dK58wZJa4pXqUH7WldTF7WufDSVQeQj2Jbh5feJqeWMXPehzK8sby1KbihiPETv7X/nKV2bTBdNoRt/7b7UkpBxFG2vTN7qZlryP03BrP4LcNUI4NG04b5e+Ed0+377RuaG6j42Fse9PlQlrLHd6R+1nP/tZuOaaa2ZL/g+9s5hcSr+uZDwe75133gnxAU8aje8bNRy7OE++umR57+hL3mL5a7ZD+9TiuevdwPhe3tiFbsY+xPBVRYAAgSQggRMLBAj0/kZZ1zsqafGMtOBFvihFOXKRJxpd74SMnQaWnmzni56U0wvT+WuLicTPasfo60yXxx/q+Jed9r4phmX4lYuldCWRi079qx031SMuZpGmrQ695/PCCy+Et956ayVR6kpQah3bWjuNtembIhen/sUffz7//PNXTb/tio3oOrT4St42XbHUl8z3nTsee6jjvkgslCOmtdtaWd9Y9vSTAUePHp1NnyzbrS8BrW1bG+mM5/3e974XXnnlldnobfyx6774Hbq+ypip3Ttq77zl11Lt4UffPl0PpMp6dN3f+r5mFpka7uuKAAECUUACJw4IEJh1JONoVO2vawpQ6jDGRK5cHCA/Tq2TnDp+ccQk/Q0tYFI7ZlwQI/31rWJZTgErFwOJxxhKJPKObNy+b/pi3nFPC0LkdS2dy2lTtWl3eZkXmfqXn6t23LJ9+5KaFCfllNgDBw6sCp2+lTXTFLtaHHTZ9I04pvatrQg5pj59t4AyrmrTCbtGWoYSgjEJ5NhYGBNfNcMyAcmTpyuuuGIW5+XqiblXft3Ee0EtkczbOXrGv3i/6XIbE9/lNMTaA53alO74+3bx77Of/ezcKpd9U13L6zOPgb7Pyriq3aPG1NVXFAECBEoBCZyYIECAAAECBAgQIECAQCMCErhGGkoxCRAgQIAAAQIECBAgIIETAwQIECBAgAABAgQIEGhEQALXSEMpJgECBAgQIECAAAECBCRwYoAAAQIECBAgQIAAAQKNCEjgGmkoxSRAgAABAgQIECBAgIAETgwQIECAAAECBAgQIECgEQEJXCMNpZgECBAgQIAAAQIECBCQwIkBAgQIECBAgAABAgQINCIggWukoRSTAAECBAgQIECAAAECEjgxQIAAAQIECBAgQIAAgUYEJHCNNJRiEiBAgAABAgQIECBAQAInBggQIECAAAECBAgQINCIgASukYZSTAIECBAgQIAAAQIECEjgxAABAgQIECBAgAABAgQaEZDANdJQikmAAAECBAgQIECAAAEJnBggQIAAAQIECBAgQIBAIwISuEYaSjEJECBAgAABAgQIECAggRMDBAgQIECAAAECBAgQaERAAtdIQykmAQIECBAgQIAAAQIEJHBigAABAgQIECBAgAABAo0ISOAaaSjFJECAAAECBAgQIECAgARODBAgQIAAAQIECBAgQKARAQlcIw2lmAQIECBAgAABAgQIEJDAiQECBAgQIECAAAECBAg0IiCBa6ShFJMAAQIECBAgQIAAAQISODFAgAABAgQIECBAgACBRgQkcI00lGISIECAAAECBAgQIEBAAicGCBAgQIAAAQIECBAg0IiABK6RhlJMAgQIECBAgAABAgQISODEAAECBAgQIECAAAECBBoRkMA10lCKSYAAAQIECBAgQIAAAQmcGCBAgAABAgQIECBAgEAjAhK4RhpKMQkQIECAAAECBAgQICCBEwMECBAgQIAAAQIECBBoREAC10hDKSYBAgQIECBAgAABAgQkcGKAAAECBAgQIECAAAECjQhI4BppKMUkQIAAAQIECBAgQICABE4MECBAgAABAgQIECBAoBEBCVwjDaWYBAgQIECAAAECBAgQkMCJAQIECBAgQIAAAQIECDQiIIFrpKEUkwABAgQIECBAgAABAhI4MUCAAAECBAgQIECAAIFGBCRwjTSUYhIgQIAAAQIECBAgQEACJwYIECBAgAABAgQIECDQiIAErpGGUkwCBAgQIECAAAECBAhI4MQAAQIECBAgQIAAAQIEGhGQwDXSUIpJgAABAgQIECBAgAABCZwYIECAAAECBAgQIECAQCMCErhGGkoxCRAgQIAAAQIECBAgIIETAwQIECBAgAABAgQIEGhEQALXSEMpJgECBAgQIECAAAECBP4PZZsBZC3MDMIAAAAASUVORK5CYII=', `mes` = '8', `anio` = '2024', `tipo` = '9', `semana` = '0'
WHERE `id` = '282' 
 Execution Time:0.1764509677887

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '10'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.20580506324768

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAtoAAAJWCAYAAABmnCCQAAAAAXNSR0IArs4c6QAAIABJREFUeF7t3X/QZUV9J+BGUGd0SRhAIrKojDjobmKIzIKLWckm2UrtLmBhCkVI3C0kUkGRZIGpGcK4pYNAgVQKMZOCItSWUTShIqVgrclaiWwiK2S0iLFKURyjFGLGgVEwzOAvtvpS/XLeM+fce98ffc+5fZ73H5j3vbdP9/Pt+97P6dvnvAc89dRTTwVfBAgQIECAAAECBAisqsABgvaqemqMAAECBAgQIECAwEhA0DYRCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTIdx///3hTW96U/iHf/iHRo7zzz8//OEf/mH42Mc+Fn7rt36r8TG/8Ru/ET784Q+H7du3h3e9613hQx/6UDjnnHMWHtt0jPpj0oMfeeSR0XP/8i//cr92qgef1GZq5zvf+U74sz/7s3Dcccct6vu2bdtGfU3jW7t2bet0SI+tP+AXf/EXF7U9rWX9WNEu2raZxOOmx7znPe8JW7duXfh3/FnyP+yww0Zd/OxnPxt++Zd/efT/qY/x/9vqnNqcdJwmoNjnN7zhDeH3f//3w4033jh2fqT+xQdN479379792v27v/u78NrXvrbxOJP8q89tq2lquH6cSfNyOTUc14f63KrWpl7v+LNq/+o44+Z46ndTm9V2qnMqfn/S66Zeu6bHp2O3mad6/tEf/dGo5vX6VudtbKP+8/rrqW40bgypNuNek94/CBAoS0DQLquevRnNpHCy0qBdfzOtDrzpja76ht72RjhNm0888cQosDcF7eobblOgqRdn2lA2reVqB+3Y32owrPpME7Tj8+sBvinQr1bQnsa/KWS3BbL0/Un+1VpPW9PU9qR5mTto1z3GnQg01akpSE9qs2nsk16/6edNvtXXc9truD6Pr7322tHJbPxqOlFM87TtRCO11/bzSb+DBO3evFXpCIHsAoJ2duJhHiCFk9e85jWjleu2ld1pgkR9FagafNqCYPX76Y3/n/7pn8JLX/rS0QppPVBM2+YrXvGK1qBdXR2Oq9r1lbG2oF3vS31V9lvf+tYoDEyyrLc/jW3binY8XgwiKRDUw1M9aNf7lgJkCmKf+tSnRqvr9aC91MBRrVPddxr/9PwXvvCFo9X7uBqevNvqNW4u1+dm+ve4FfJUp2nm5XJqWJ8H1TBYD4CpTpdeemn4q7/6q/3mWHpubDN5xf8ftxI/qc16aE5zIDnHnzd9WlT/NOnwww9f9Fp88YtfvPBJRfJvqm383kMPPbToE7VU+0nzNv08OX7hC18YfcqT/p1eq/Ux1F8/S533w3wXMWoCZQgI2mXUsXejyBm0x4WP+Ca6fv36xi0mv/mbvxne+MY3jkJr/P+4VSJ9Tdtm29aRami6+eabwxVXXBFisK+Gk2mDdmrrc5/73KJVt1kG7XrwSgHila985WgYX/7yl8f2re60Y8eOFQftcYFxWv+2E6pxL6Bxc7l+orKUoJ3aXe68rM/dphOFasBrWn2O/f2Lv/iLUS3//M//fOH/05aotqAdj93mMqnNetCetF2krTb1vu3evXv02q6eRKVgnGySx1FHHRUuueSS/YJ5vc20ba2+gp1OPOrb2uqv3eSY5kn9BLZ3v7h1iACBVRcQtFedVIPVN+G2PdppRWfcdo36qlR6zlLCTHpDjKupsb1Xv/rVozfXFGLTG+G0bbYF7Wpoqu51HrdyNe6Y1Z/Flbtx+93bjrGc1dD0nLjqH1fr0snCV77yldHKXfr+pJOASSuD4+retO1mUmCc1r9tZb6+1776Km4LlNW26nOz6bdAPegmg3Hzcjk1rB47zaMm0zSX46c88VOntDpbDezjgnbTz6ZpM/Wvvj970idA1XFVnzvu04P6a6y+P7tep2owT0G8+ruiLUindprmSnVuxkWASddNeAchQKAsAUG7rHr2ZjST9rUuN2hXL5Cb5uP5pjCQ3kybtkVMarMtaNff0OuBo2nrzDRBO/Zx48aNMw/a8bg7d+4cXdgZTf76r/969P9xe0FcAa0H7bYTquXs0W4KheMCY5z0S/Fv2lc77qK9SXO5+txxe7Srj5tmXlZPEpdyQWt9pTv+u2le10N89VOB9EnMNEG7er3CNG1Wf0k1nXBNs62i+rxJ11xUfx5f+29/+9sbt6bUt66krSjTBu3qvEreddO0hWqaMfbml7mOECCwIgFBe0V8ntwmkGvryFKDdn3VrNrf9AYcv5fubrGcoD3uzgzjLopcatCe5daRGATiSmd1FTuubqdtMdME7WqYaNsLPk3gqIaqpvos13/aO16MC9r1VdhpPxmZZl7Gk7PlrmhX228yHndRaDWYLyVoV/dIN/1eaHttLfUuME0r9vUxJrf6CVSsT7Spb+lq2lLUtHrdtqLdtme9+qlFvMPJNPX0rkKAQFkCgnZZ9ezNaHIF7XjHj3poq7/xVvdoT3sXiGnbbFrRHheaYt/aPhJvC2XV4BHDSdo6MuugXV9Jjycm733ve8Mf/MEf7LeiPalvyw3a1QDUFsqX65/mTX2bS/V2gfEx9blcPTFrCnLpU4C22wVWV+DbXrAplE4TzOq21dA36QLPSbffTHfZSavrVZv6haVpj/SkNtsujG7ailP1qV+IGNup78GufgpQr011f3b1+oxx+/bbPimperSF7EknM8vdm96bX/I6QoDAVAKC9lRMHrRUgZxBu+2Nsb5vM90hpCkk1N+gl9pm9ePytsA8bjWwGrbqK30pONXvZDApzNZrtJyQVn1O/T7W1XtbT9qj3daXpdx1ZJrAOM6xyb/phKrpTiTV/jfN5WqIqgamaVa0x82L+rxcag2rF/hNc5/r+slLfcU23dmj/hpqCsVtfW1aBZ60BavppKrpriT1u74kv3F70n/7t3974WLpcXdPqYb2+l1Jmj4Nq5/UCNpLfdfweAJlCgjaZda181FN2tea3gjT3SimuWiwaStC00Dr9+huWtVreqMfd4FearN+H+14/PqdDqp9qgeBpp9NWtWc1rJ+Qd80FxxOuhtI9ROBpov20vgnnQS0rWi3jT16n3TSSeHcc89tnctxDl199dVh8+bNi+40Mc5/KdtAUjttJ41Nq+2TPkGJczFdEDfNvEz1aUKov4Zie7/6q7+68EeF2uA++clPhve///2N94Kvh8sLLrhg4Q89NbWXVo3jz9ruL98UWKsnSPV2p9luVX1OvQ/xj1LVv5JNfX92W71Sm22r9PVPHKYdwzQnTp3/8tYBAgRWVUDQXlVOjdXDSdvHyCsN2vE4TaFpKRc4NoXgcW3GY066bV19BjR93J0eM+lNPn1M32XQrm+reN7znrfori1dB+14B4d4K8K2LRJN/k17usfd8WLcpzMpOKX5HG+RF7eOtH3FLQu7du1qvJd7fV7EuRy/2v5y6nKD9h//8R+H3/3d3239K4zVFf4bbrghXHjhhaO/qNoUXtMWjHHzvPpard56rxrAU9tL/UNP1e0h47YQpROcP/3TP13Ynz1ub3+13frrr+3e8lWftnEI2t4jCQxPQNAeXs2NmAABAgQIECBAYAYCgvYMkB2CAAECBAgQIEBgeAKC9vBqbsQECBAgQIAAAQIzEBC0Z4DsEAQIECBAgAABAsMTELSHV3MjJkCAAAECBAgQmIGAoD0DZIcgQIAAAQIECBAYnoCgPbyaGzEBAgQIECBAgMAMBATtGSA7BAECBAgQIECAwPAEBO3h1dyICRAgQIAAAQIEZiAgaM8A2SEIECBAgAABAgSGJyBoD6/mRkyAAAECBAgQIDADAUF7BsgOQYAAAQIECBAgMDwBQXt4NTdiAgQIECBAgACBGQgI2jNAdggCBAgQIECAAIHhCQjaw6u5ERMgQIAAAQIECMxAQNCeAbJDECBAgAABAgQIDE9A0B5ezY2YAAECBAgQIEBgBgKC9gyQHYIAAQIECBAgQGB4AoL28GpuxAQIECBAgAABAjMQELRngOwQBAgQIECAAAECwxMQtIdXcyMmQIAAAQIECBCYgYCgPQPkWRxiz5494d3vfnd45zvfGdavX79wyH379oVt27aFe++9d/S9E088MWzdujWsWbNm9O9JP59F3x2DAAECBAgQIFCigKBdQFVjyL744ovDY489Fq655ppFQfv6668Pu3btGoXr+BVD9xFHHBEuuuii0b8n/bwAHkMgQIAAAQIECHQiIGh3wr56B92xY0fYsmVLOProo0eNXn755QtBe+fOneGKK65o/V58/LifV1fGV6/HWiJAgAABAgQIDENA0J7jOqdtH2eccUY49NBD9wvNMYRv3749XHfddWHdunWjkVafE/897ucbN26cYx1dJ0CAAAECBAh0KyBod+u/akdvWr2+/fbbQwzbTXuyU4ge9/MY4Oft685wZfh8+NjEbr8mnD3xMR5AgAABAksT+Fy4deITDgzPCZeHuyc+zgMIlCAgaJdQxRBCn4L27t27wze/+c1OZL/44g+Fb73gbzs5toMSIECAwGSBZz317PBfvvCByQ/M8IiDDz44bNiwYVkt3/b1H4fPf/cny3rucp90wgsODGe+7KDlPt3zeiAgaPegCKvRhT4F7SeffHI1hrSsNj510DXhvgM/vqznrvRJG376upU24fkECBCYqcBXn/V/Z3q8eLC4on3pk38z8+OmAz73uc9d1rEF7WWxDf5JgnYhU6ApaNujXUhxDYMAAQIEOhcQtDsvwVx2QNCey7Lt3+lJdxhJdxCpPi624q4jhUwAwyBAgACBrAKCdlbeYhsXtAspbVPQjkObdJ/sST8vhMcwCBAgQIDAigQE7RXxDfbJgnYhpW8L2pP+8uOknxfCYxgECBAgQGBFAoL2ivgG+2RBe7ClN3ACBAgQIEBgWgFBe1opj6sKCNrmAwECBAgQIEBggoCgbYosR0DQXo6a5xAgQIAAAQKDEhC0B1XuVRusoL1qlBoiQIAAAQIEShUQtEutbN5xCdp5fbVOgAABAgQIFCAgaBdQxA6GIGh3gO6QBAgQIECAwHwJCNrzVa++9FbQ7ksl9IMAAQIECBDorYCg3dvS9Lpjgnavy6NzBAgQIECAQB8EBO0+VGH++iBoz1/N9JgAAQIECBCYsUAM2l18nfmyg7o4rGOukoCgvUqQmiFAgAABAgQIECBQFRC0zQcCBAgQIECAAAECGQQE7QyomiRAgAABAgQIECAgaJsDBAgQIECAAAECBDIICNoZUDVJgAABAgQIECBAQNA2BwgQIECAAAECBAhkEBC0M6BqkgABAgQIECBAgICgbQ4QIECAAAECBAgQyCAgaGdA1SQBAgQIECBAgAABQdscIECAAAECBAgQIJBBQNDOgKpJAgQIECBAgMAsBB544IFw7rnnhoceemjhcFdffXU488wzF/69Z8+ecN5554X77rtvUZeOOuqocMstt4Rjjz22sau33XZbuPvuu8OVV14Z9u3b19hGeuLpp58e3vrWt4YLLrhgUV/qDd96663hsMMOC5s2bQrXXHNN67FnYTeLYwjas1B2DAIECBAgQIDAKgvEILx58+YQw+tJJ500an3v3r3hsssuG/1/DMhr164NKWjHcJseF38en3/DDTe0hu2moF1vY9yQYpCOX/E51a94ciBor/Jk0BwBAgQIECBAgMDqCIwLqylsH3nkkaNA2xa0276feihor7xWVrRXbqgFAgQIECBAgMBMBdpWi1Mn7rnnntHWjJtvvnn0rbh1pL4aLWjnL5mgnd/YEQgQIECAAIE5F/hO+GonI3hh2LDfcdOK9cknn7xoL3b1gdUQvWHDhsagHVesv/GNb+y3tcOK9uqVWtBePUstESBAgAABAoUKfDy8O9wX7pjp6I4Pp4XXh//ZGrTPOuusRXuuJwXt+sWQ8fHxIsa0l7t+oKVcDFndJ57asUc7BEF7pi8ZByNAgAABAgTmUaCPQXulK9rpjiWnnnpq46q2Pdorn6mC9soNtUCAAAECBAgULrAvPN7JCNeEgxuPuxp7tGPD1TAd71BS/RK0V15yQXvlhlogQIAAAQIECMxUYDXuOlIN2vFCyXe84x2LLpis7uGedOFk0+BtHbF1ZKYvCgcjQIAAAQIECKyWwErvo10Nz6961atG999OtwSsX3ApaC+vala0l+fmWQQIECBAgACBzgWa/urjtH8ZMna+ehFjva3zzz9/Ye9221+XTADHH3/86FaC69atWzCZdkW76S4qTSv247a5dF6Ilg4I2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+Ub3/nbb789bN++vfFBF1xwQTjjjDPCjh07wpYtWxY95uijjw7XXXddWLduXcE6hkaAAAECBAgQyCsgaOf17V3rMXzfcccdC0E6/juG7a1bt4Y1a9b0rr86RIAAAQIECBCYVwFBe14rt4x+79y5M2zatCls3rw5bNy4cdTC9ddfP/rvRRddtIwWPYUAAQIECBAgQKBNQNAeyNzYt29f2LZtWzjiiCMWQnX6XgzdcRuJLwIECBAgQIAAgdUTELRXz7LXLcXtIVdffXW45pprwvr160d93bNnT7j44ovDgw8+uNB3+7N7XUadI0CAAAECBOZIQNCeo2KtpKtNW0TSVpJzzjlnYUW7vod7OcfcvXv3cp7mOQQIECBAILvA4Ycfnv0YDkAgCQjaA5gLaeU63mkk7c1uG3Z67Gmnnbbs7SQxaD/66KMDkDVEAgQIEJg3gQ0bNsxbl/V3jgUE7Tku3rRdj9tG4m3+prlln33b06p6HAECBAgQIEBgvICgPYAZ0nYLv6YAvpTV7wHQGSIBAgQIECBAYNkCgvay6ebniW238GvaJhIfu2vXLvfVnp/y6ikBAgQIECDQUwFBu6eFWa1uTdoKUr/zyIknnihkrxa+dggQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIBpRNugAAAgAElEQVQAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoF14+Xfs2BG2bNmyaJRHH310uO6668K6devCvn37wrZt28K99947esyJJ54Ytm7dGtasWVO4jOERIECAAAECBPIKCNp5fTtv/fbbbw8xbLeF5+uvvz7s2rVr9PP4FUP3EUccES666KLO+64DBAgQIECAAIF5FhC057l6U/Q9Bun41RScd+7cGa644opw+eWXh/Xr148e1/S9KQ7jIQQIECBAgAABAjUBQbvgKZG2hWzcuDGcccYZ+400rnRv3759YRtJfEB6Tnx8fJ4vAgQIECBAgACB5QkI2stzm4tn7dmzJ1x88cXhwQcfXOhvdX9207aSSeF8LgaukwQIECBAgACBHggI2j0oQq4uxG0gmzZtCuecc87CinYM13fcccdoFfszn/nMfvu3VyNo7969Ozz66KO5hqVdAgQIECCwbIENGzYs+7meSGCpAoL2UsXm/PFplfu0004bjaR+oeRqBe05Z9J9AgQIEChU4PDDDy90ZIbVRwFBu49VydinapCO20js0c6IrWkCBAgQIEBg0AKCdsHlb7rYMa1oX3DBBeHQQw9115GC629oBAgQIECAQLcCgna3/lmPXt0mku46Ur1vdvyjNO6jnbUEGidAgAABAgQGLCBoF178+p1H6n/50V+GLHwCGB4BAgQIECDQmYCg3Rm9AxMgQIAAAQIECJQsIGiXXF1jI0CAAAECBAgQ6ExA0O6M3oEJECBAgAABAgRKFhC0S66usREgQIAAAQIECHQmIGh3Ru/ABAgQIECAAAECJQsI2iVX19gIECBAgAABAgQ6ExC0O6N3YAIECBAgQIAAgZIFBO2Sq2tsBAgQIECAAAECnQkI2p3ROzABAgQIECBAgEDJAoJ2ydU1NgIECBAgQIAAgc4EBO3O6B2YAAECBAgQIECgZAFBu+TqGhsBAgQIECBAgEBnAoJ2Z/QOTIAAAQIECBAgULKAoF1ydY2NAAECBAgQIECgMwFBuzN6ByZAgAABAgQIEChZQNAuubrGRoAAAQIECBAg0JmAoN0ZvQMTIECAAAECBAiULCBol1xdYyNAgAABAgQIEOhMQNDujN6BCRAgQIAAAQIEShYQtEuurrERIECAAAECBAh0JiBod0bvwAQIECBAgAABAiULCNolV9fYCBAgQIAAAQIEOhMQtDujd2ACBAgQIECAAIGSBQTtkqtrbAQIECBAgAABAp0JCNqd0TswAQIECBAgQIBAyQKCdsnVNTYCBAgQIECAAIHOBATtzugdmAABAgQIECBAoGQBQbvk6hobAQIECBAgQIBAZwKCdmf0DkyAAAECBAgQIFCygKBdcnWNjQABAgQIECBAoDMBQbszegcmQIAAAQIECBAoWUDQLrm6xkaAAAECBAgQINCZgKDdGb0DEyBAgAABAgQIlCwgaJdcXWMjQIAAAQIECBDoTEDQ7ozegQkQIECAAAECBEoWELRLrq6xESBAgAABAgQIdCYgaHdG78AECBAgQIAAAQIlCwjaJVfX2AgQIECAAAECBDoTELQ7o3dgAgQIECBAgACBkgUE7ZKra2wECBAgQIAAAQKdCQjandE7MAECBAgQIECAQMkCgnbJ1TU2AgQIECBAgACBzgQE7c7oHZgAAQIECBAgQKBkAUG75OoaGwECBAgQIECAQGcCgnZn9A5MgAABAgQIECBQsoCgXXJ1jY0AAQIECBAgQKAzAUG7M3oHJkCAAAECBAgQKFlA0C65usZGgAABAgQIECDQmYCg3Rm9AxMgQIAAAQIECJQsIGiXXF1jI0CAAAECBAgQ6ExA0O6M3oEJECBAgAABAgRKFhC0S66usREgQIAAAQIECHQmIGh3Ru/ABAgQIECAAAECJQsI2iVX19gIECBAgAABAgQ6ExC0O6N3YAIECBAgQIAAgZIFBO2Sq2tsBAgQIECAAAECnQkI2p3ROzABAgQIECBAgEDJAoJ2ydU1NgIECBAgQIAAgc4EBO3O6B2YAAECBAgQIECgZAFBu+TqGhsBAgQIECBAgEBnAoJ2Z/QOTIAAAQIECBAgULKAoF1ydY2NAAECBAgQIECgMwFBuzN6ByZAgAABAgQIEChZQNAuubrGRoAAAQIECBAg0JmAoN0ZvQMTIECAAAECBAiULCBol1xdYyNAgAABAgQIEOhMQNDujN6BCRAgQIAAAQIEShYQtEuurrERIECAAAECBAh0JiBod0bvwAQIECBAgAABAiULCNolV9fYCBAgQIAAAQIEOhMQtDujd2ACBAgQIECAAIGSBQTtkqtrbAQIECBAgAABAp0JCNqd0TswAQIECBAgQIBAyQKCdsnVNTYCBAgQIECAAIHOBATtzugdmAABAgQIECBAoGQBQbvk6hobAQIECBAgQIBAZwKCdmf0sznwzp07w6ZNm8L3v//90QFPPPHEsHXr1rBmzZrRv3fs2BG2bNmyqDNHH310uO6668K6detm00lHIUCAAAECBAgUKCBoF1jUNKQUsjdv3hw2btw4+vb1118fdu3atRC2b7/99lHYrobvgkkMjQABAgQIECAwMwFBe2bUsz9QU4iO4fuKK64Il19+eVi/fv0oeMeviy66aPYddEQCBAgQIECAQMECgnbBxW0aWjVov+hFLwrbtm0brXafccYZA5MwXAIECBAgQIBAXgFBO69v71qPq9x33HHHaA92/Lr44ovDgw8+uNDP1dif/eSTT/Zu3DpEgAABAgSiwHOf+1wQBGYmIGjPjLr7A6ULH6+66qrRKnbaw33OOecsrGhXg/hyL4bcvXt3+OY3v9n9gPWAAAECBAhUBA4++OCwYcMGJgRmJiBoz4y62wOlkH3BBReM3SayZ8+e0Sr3aaedZjtJtyVzdAIECBAgQGDOBQTtOS/gNN2fNmTHtvbt22ff9jSoHkOAAAECBAgQmCAgaBc+RerbRarDjT/bvn37ontmpxXtuPKdbglYOJHhESBAgAABAgSyCAjaWVj70WjTfbSrPWvaJlK/z3Y/RqIXBAgQIECAAIH5ExC0569mU/c4huY777yz8fHpgsgUttOdR+p/OXLqg3kgAQIECBAgQIDAIgFB24QgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uZAEQLv/8cfhm//y1Ojsaw5MIRzNjw7vPxnn1XE2JoG8c97nwr/6ys/CnuefHrM6557QPjvr3h2+Lm1BxQ55k9+88fhbx/+ycLY/sORB4b/+pKDihxrGpQ5XfacjnX+7Hd+Ev73t34cfvzTp6v+b9Y9K7zluGcXO6+H+DoutpgGNrWAoD01lQf2VeCD9/8o7HzspwvhOgaUvT8O4b+94tnhhYUGzzTGGK7jVwzdaw8K4Z2/8Jy+lmnZ/Uph5N//3NPhOr5Z/79//kn4zy8+KLz2hQcuu90+P9GcLntOx7n3te//NHz4qz8K63/m6XBdn+d9np/L6dsQX8fLcfKc8gQE7fJqOqgRpTerf3fEMyucTd8rCSW+Yf2fB38c/tPRzwTNpu+VMuZ4UhG/qicRTd8rZbzm9NMnTyXP6Ti+eDL18BNPLfokKn2vxEWCob2OS/l9ZBwrFxC0V26ohQ4F2t6M4y/1Q55zQJEfw8YV3b/f9ZNF22PSVpKfP/RZRW2pSOM68nmLaxkNvvToT4v81MKcfnrLV6lzOv26bAqesfZ/89BPwpuOPaiorW9DfB13+Lbo0D0TELR7VhDdWZpAU+iMLZS84tm0Etb2RrY0zf49uu3Tiba6928ES++ROf30dQalzulxYyt1FX+Ir+Olv/I9o1QBQbvUyg5kXEJJ2aFkiG/Q5nTZc1rQfuYi5pJPmAfyFmyYUwgI2lMgeUh/BYSSskOJoP3MnXN8StPf30NL7Vnbar0V7aVKejyB/gsI2v2vkR6OEbCftez9rJP2dpZ4S0Nzuuw5nX6d2aMdRncQitdalPg69sZNIAkI2ubCXAu4Q0P5d2gY2t0KzOny53T8pTvuriMlBs+hvY7n+o1V51dVQNBeVU6NdSHQds/hEt+sqqth8V7h7qM9rPtom9Nd/IbJc0z30S7/fvh5Zo5W501A0J63iulvo4C/olf2X9Eb4l+UM6fLntPxF5m/DFn+X3j1lk1A0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhQlcNNNN4U3v/nN4eCDDy5qXG2D+fa3vx3uuuuu0ZiH8BXH+vznPz9s3LhxCMMdjfEjH/lIOOWUU8KLXvSiQYz58ccfH435bW972yDGGwd5//33h6997Wvh1FNPHcSY77zzzvDyl788HHfccYMYr0EOW0DQHnb9ixu9oF1cSRcNSNAuu75xdIJ2+TUWtMuvsRE+IyBomw1FCQjaRZVzv8EI2mXXV9C2ol3+DDfCoQkI2kOreOHjFbTLLrCgXXZ9BW1Bu/wZboRDExC0h1bxwscraJddYEG77PoK2oJ2+TPcCIcmIGgPreKFj1fQLrvAgnbZ9RW0Be3yZ7gRDk1A0B5axQsfr6BddoEF7bLrK2gL2uXPcCMcmoCgPbSKFz5eQbvsAgvaZddX0Ba0y5/hRjg0AUF7aBWvjXffvn1h27Zt4d577x395MQTTwxbt24Na9asmUsZQXsuyzZ1pwXtqanm9oFu7ze3pZu6427vNzWVBxYgIGgXUMSVDOH6668Pu3btGoXr+BVD9xFHHBEuuuiilTTb2XMF7c7oZ3JgQXsmzJ0eRNDulH8mBxe0Z8LsID0RELR7UoguurFz585wxRVXhMsvvzysX79+1IWm73XRt+UeU9Bertx8PE/Qno86raSXgvZK9ObjuYL2fNRJL1dHQNBeHce5bGXHjh1h+/bt4brrrgvr1q0bjSFtJTnjjDOW/GeuH/9RCJf8fbcUJ3zppvCPG94cfvic2fwJ9u/9MIRdexeP+V8/P4TnHTQbh3/1L98Oxzx012jMs/j6+uMh/OSni4+04WdnceSnj/HSh+4KTx70/PDwz83uT7B/9fuLx7f2oBCOfv7sxvwLX/1I+MZRp4QfPH82f4J9749DePBfFo/viLUhHPKc2Yz5OT98PMQxf/7nZ/cn2P95bwjf/+Hi8b3sZ0I48IDZjPmwPfeHw7/3tXD/MbPZo12f0wc+K4SXzeZX5gj0uG/cGXYf8vLwyLru/gT7KS8M4eyn15d8EcgqIGhn5e1347fffnuIYbu6JzsF7Y0bN4YYtpfyFYP2f/zUUp6x+o9928M3hY8c8ebw+IEzfNdY/WFM3eKRT347/Mr37xqNeQhfcayPPev54QsHzy5od+365l0fCZ/52VPCw8+dTdAOT4UQZhQwm2wP/snjIY75piNnF7S7rvGGJ+4Px+37Wrjj0NkE7VF9Y507+jrt0TvD/WteHr76vO6C9v/4t4J2R+Uf3GEF7cGV/JkBZwva990RnvretzuRPeGwEL76WAgx9A/h68i1IbzoeSF8/pEhjDaEWN/4NZTxxrHGMX/7iRC+s/eA8FR4KhwQnvlvU9Xrj1mN56Q2qsebdJzlPudnnn1AePnPPDWqcXWs0xwv9q/r5yy1D/HxcbwHP/vpeT1N/+NzpvGoz5WlPCeNI9V82udOM0defdhT4Qc/CuH+x54e76zm8cJxfuVtQdAexntGH0YpaPehCh31IWfQDt/8QkejclgCBAgQIDBG4PX/U9A2QWYmIGjPjLp/B8qxR7vrrSP9U9YjAnMm0PHWkTnTms/udrx1pA9oVrT7UIVh9EHQHkadG0e52ncdids1hvSRfkT90vf2pz30OU9v5yjxq2m8P39IiSN9ZkxDG3PcpvJo7cLAqFFynZtqHC8OXHtgmXN7aHO6rYq/8sIy62tU/RIQtPtVj5n3prT7aM8c0AEJECBAgAABAi0CgvbAp0Zpfxly4OU0fAIECBAgQKBHAoJ2j4qhKwQIECBAgAABAuUICNrl1NJICBAgQIAAAQIEeiQgaPeoGLpCgAABAgQIECBQjoCgXU4tjYQAAQIECBAgQKBHAoJ2j4qhKwQIECBAgAABAuUICNrl1NJICBAgQIAAAQIEeiQgaPeoGLqycoEHHnggXHXVVeF973tfWLdu3cob7GkLcZznnntueOihh0Y9PP3008OVV14Z1q5d29Mer7xb99xzTzj77LNHDR111FHhlltuCccee+zKG56DFm677bZw9913F13j+pweQp337NkTzjvvvHDfffeNZuH5558fNm3aNAczculdrL5+688uedxLl/KM0gQE7dIqOuDxpDfqF7zgBeHmm28uNmincV577bXhpJNOGlX8mmuuCQ8//HCxQSy+SV966aUL4Tr+O4655Dqnl3Kq9wknnFBsfeNYh1TTON5U1wsvvDCceeaZIYXus846a/TvIXzVX9dDGLMxDk9A0B5ezYsccVzx27x5c3jd614XHnvssaIDWNPqZnzTjithMXyWuMobx3XMMccsBJC9e/eGyy67LMRQkk42SpzYaZzf+973wiGHHFJ00B7Cqn11jjadHA/JYIgnFiX+jjKmyQKC9mQjj+i5QPyFfckll4QtW7aERx55ZDArndWylB6061MwBdCTTz656NW/GMbiVzzJKH3rSBprqVsnqnN4KPN33FtH6Z/C9fxtU/dmKCBozxDbofILDO3j5yQaV8I++tGPFr2SX509QxhvdS5/+tOfLjpop+D5iU98YqHMJe/DT6u5cdvIxz/+8ZDGPZS9yk3b3/K/OzgCgW4EBO1u3B01k8AQg3a6yOjWW28tehtFnDLVC6quvvrqYlezUxCLq7txa0zpWwrSeONY04p2yft303i/+93vLlx30GSQ6ddk582WPp87B9aBXgkI2r0qh86sVGBoQTsFz5JDZ9OcSCugRx55ZJF3aahvoxhiMCm5xvUTqTTHSz65SGO0bWal73KeP28Cgva8VUx/xwoMKWgPNWRXQ0mJdx6p3/KtOuFL3k7R9MIudd92W9AewrUWQxijt2kCVQFB23woSmAoQXtI20XaVsCGUuv4Ai19RbspfJW+8lm/k07aGlXiyWP1TWZIr9ui3lwNZtkCgvay6TyxjwJD+CU+xAuJYtC84YYb9tvPOpR7DpcetJu2iZR+wWv95GIoe7RLn8t9fF/Up24FBO1u/R19lQWGELTjiteNN97YKFfyBZHpXulp4EPalz6EcFK/88jxxx9f/F106n8Ncwh3HSl1O9Aqv5VpriABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30ZI4FbrvttrB58+Zw9dVXhzPPPHOOR6LrBAgQIECAwGoJCNqrJamdwQo88MAD4aqrrgrve9/7wrp16wbrYOAECBAgQIDAYgFB24wgsEKBPXv2hDVr1oS1a9eusCVPJ0CAAAECBEoSELRLqqaxrJpADM/nnXdeuO+++xa1efzxx4ebb7550cp1XNE+99xzw7XXXhtOOumkRY9PW0rOP//8sGnTpv36d88994Szzz570fdvvfXW/dqpP3Hv3r3hsssuC5/4xCcWfjTtMY466qhwyy23hGOPPXZRs9dcc0248cYbGw2rW2LSeB966KH9Hlv3Gddm9bFN4zn99NPDlVdeuXACkyybOti0Zaf++KbapePGNqvHiv9OPzv55JNH24Hajt907Djuhx9+eNRm/KrXqjqGNM7oGefRhRdeuN/2o2ge509sN37FxzX5p3Zjn77xjW+01nOp9uNeWKnG02ybapo79ec1vSaqx0+vj2lfW/X202v7u9/97n6vg6XMh2qfJvWl+tj675ZJ42/7fZDmwhve8IZF86GpDlX3+uu//tqrv+7qtY9j/ehHP7rf78FV++WrIQKFCQjahRXUcFZHIL7ZX3rppfu9ETd9v+2x6c3tZS97WTjkkEP2C3JNz0vPaQpbaWTpMaeeeupCeG8LCPHN+M4771w0jqZjpOcfeeSR+50Q1E8k2sYb+1cNmPv27RudrJx11llj9623HTu2FY+VTmxSsKifsNT7l9r71re+tSgMpABXDS7VAFIPNCkQxeO96lWvGoXluk9T3+vfG3ci1hTWmk4IYt/j+OsneXXz9KlK6vtq2be9qtJcOPzww8NrXvOaxpPJ+rytnpCmfsYT1FTX6hwa9ylRfNyXv/zl8MpXvnK/41bbPeWUUxaduMbnfe5znwu7d+/e7+R42vlQPaGe9Dqv2tVfe02vrXjineZi29xJ29Xe/va3h9/7vd9bODlrqnv99VqdS/GTuDivqyea4/zTa2hSGF+d38JaIVCGgKBdRh2NYpUFxq3a1ANffOzdd9+9X5BOb1ivf/3rww033LBfSGoLjm3txSGOC1DVYBiDwLgx1N98x7XbtLLbtqJVfRN/5JFHWlf66+EjrdZWV9mbgm5aXa4+v96/cUGhbpL6+2u/9mvhBz/4waLAVjU67LDDWk8aqqvNsf/1Oow7MamOI/Y79uHxxx8fnZxUw1zbmOpjHxdqm14i9b6nx9TH0PTc6gnFMccc0/gaqJ9INL1OmoJf0wlfta3Uvxj4vv71r4ctW7Ys2roV6/zEE0+MfvaWt7xl4dObFFzf8573hI9//OOhPp+mnQ/VeTrpdT7JIP0eiCfXTSdz9d8T1ZrHTy3SJyfppKT6+yMeO7ZZHWe1tvHn9QWFtjmRvh+fM+mkapV/HWuOwFwLCNpzXfRTlb0AAArkSURBVD6dzyUwaVUnrS6mFaF6MKi+WcXA2bQaOe3K3aQ36rYA1badJT6+HtDa3lyr4T6G4Rj+2k4Q4mOrb/Jf/OIXGz8VqPd3mhXfccGvOpYXv/jFY4/ZdDISw18MG3Gl9V3vetfCtqBqKB930lC3rAfraT5qr4/hrrvu2u/Timk+bUi204b7aezbXmPVgPzpT396qqA9acvBtCvxab7GT0zi6nQ1aMc2YpCOJys33XTToouU66G2HrTT/J00H9JFz9O8zif9jhr3eorPrf983KcbTa/B+u+eSSdRTb8L0nPe+ta3hj/5kz8Zzc36NrlJ4/RzAkMVELSHWnnjbhUYt40iPqn6Rhf/3bQ9ovrmOGnVMO4Dn2Z/a9vqZdNAJoW7aVep61tSxm0HqW/NGLcyX+9z2uPatG0iPnbciUD1Zx/72Mf2W+GrHqseMqp1iv8fV2bT7RmrJ0LjThomWU4KUvWTmQ0bNoRLLrlkFB6rK+RN20DaAvVq2jfNr6aTi7atLen51b3AbdcTjKtztR/pNRiP+YEPfGDRCVKqY3x8XPFNW1LqbTfVZdr5kFaPp3mdj/tVO+mEqG1bVNMnO9V5lOZK0xgnBe36AkC11r/0S7+0cK1A/RoPbykECDQLCNpmBoGawKRVtUnbI9q2ZbStAlUvsmsLIPUwNmk1aVK4a9pu0XYhZLVP1T2s9YlTvciq6eLG6uOb9nhWLxKrX7DVtopX3YubPnpvCyFNgbZ6khSPEVdcqxcwVi+EbFuNHbd1JZ2Y1C+qTRbJtj5nqqF/XPhsOqFabftpTuSmDcjpRDVdALyUC16rc6K68rx169aFk5K0d3nbtm2jTyhS/ZpOnusnI/XX/bj5kMZR3XYxKcDWTxSiQduJZTq5jJ9KVa/FGLeanY4fP9WJc7jtpHhcreK8q17TUXebtJruzYQAgf0FBG2zgkBNYFJoqG+PqK7kjbs4blwArL6xnnDCCfvt904/b9rL3FTASUF70oV+bScbTStwTWFv0snKpEkX+x+Df/0OE03PS58GTLPiX61tbKu6vSb2Oa0kpz3Z02yXGbeXfNqtGU17x1Poj6vpbavF41YsJ10I2VaDun39cU21XUrITO2l58R/Vy94re85rh+/+hqLJ1fxHvZpH3Y6QYkrr9XaNs3bemis12rcfFjJ67w6ntjf2I/6Ra7p06HqJ13pmPX9+22hvK0mTWG57QLi+vxayiclk17jfk5gKAKC9lAqbZxTC0yzapQCWP2NJ71BNh0srV6OCyXjgtmkMFPtS1yVarrwLPWrGuzi9+LKbn3FvS1U19ttCh1LCZht/UwfYccVy7hCOekCuTiOSScY9e0g9QCbglp1r3dcSa1fUFatb9U9rSImy0lbA1I79X5XQ17bdpi2k5nVtK/f8jD2t+02h/FnTVugxp0AVccQV26bLgZsC/rVk6B4Z5F4cvTBD35wtLpd3eqT6le9FWZqs7pK3vS6b5oPccvENK/zaX7hNJ3UJ9/6XXDaQm5TKB93Yt401+LrP62Ep20x4z69cteRaarrMQSeFhC0zQQCNYFxqzZNV/SnAFjdxlC/Bd2kOwGkLkxz0V9T4Kw/b5qLG9OKZ9tjmwJ0W5CtB8ppP2Ie97hkFi3f8Y53TLxNYDQc1149gDbVOT4mhrV4S8YYzOJKY9uJSPx+3b3uMM0KYFsQTXfOiNtOlnoh5KT90pOs2vqdDJtuPznuJKftZ9Wx//qv/3rjCV/9F1TTXusYtOPtHGNYTHfcSSdw8aQz3vWnfu/4tgtjqycX4+ZD9ZaE1RPYphPHcXeHqf5V2frWjUm/F9pCedPcbArfqZ7V7SltbwiTTvS9kRAg0CwgaJsZBCoC4y6ErH+8X19RjD9vekNvCjVNb5D1Cw+b7iHctHrVFn6a+tP0/HEXTjatfDdtSWiyGLeiXg8Q9dW01M+4qhdXKsfdQaU+gZs+jm/ybgp/sQYx+HzpS18KL3nJS0ZbeNouhGxaUa5bTlphbwtEKRS9973vHd2irinYtq2WTxPuq8cdZ1+/FqBtq0Nsb9xx21Zdq+3FO7tMszWqfjKVTkhiH974xjeObvOX3H/nd35nFN6bQnHT3vrYRvUkuWk+tAX3aU5eqvv8m/aEV++hXZ3XbSeFbX/gKD23Ov/qCwHT/L6p9mHSljpvJAQICNrmAIGJAtUL8uoPrl+o2LTft+0PzTS9STV9/DzN3UfqfWz7S4/pjb/6lyebHlvdTlEP99UQX9/32hZw410gYptNH9Wn51Q/Fm+6eK96kdi02y/qwWTz5s0L36pfdDZuD3kK5akWbVsl2ixTWBs3l+Jj0vNjwGz640jJ5fOf/3zjX/JsCl+TLoSMx12KfdW0evLTdDHupE8xmrYiVF9T47akxH6kx9bna71f1drGu4407YGuB9EUyJtOIqvzIb0GlvI6Hzcv69cXNL1m4snDZz/72f3+uE7aS19/HVa3ddTnQ/X3S9v2l7bfJ5PqO/GXqwcQGKiAFe2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxX4/7T5Xt5dAok7AAAAAElFTkSuQmCC', `mes` = '8', `anio` = '2024', `tipo` = '10', `semana` = '0'
WHERE `id` = '283' 
 Execution Time:0.20622515678406

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '7'
AND `mes` = '8'
AND `semana` = '32'
AND `anio` = '2024' 
 Execution Time:0.20265698432922

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAvUAAAJWCAYAAADYynjNAAAAAXNSR0IArs4c6QAAIABJREFUeF7t3X3QZVV9J/qFb92tabWDUVsiV5EXa8qJjPaEXEyidcskTgUwmCJRkngTpILpFJekCG1DaJJLU3TbwKQIppO+QeomMUhCRSpCapKMVRPmJoww7QvqlAG5bYIDKDbpKNFuiMitdbjrcT+r9z4vz9nn7LfP84/Szznr5fNb55zv3mft/Rzz9NNPPx38ECBAgAABAgQIECDQWYFjhPrO1s7ACRAgQIAAAQIECIwEhHoLgQABAgQIECBAgEDHBYT6jhfQ8AkQIECAAAECBAgI9dYAAQIECBAgQIAAgY4LCPUdL6DhEyBAgAABAgQIEBDqrQECBAgQIECAAAECHRcQ6jteQMMnQIAAAQIECBAgINRbAwQIECBAgAABAgQ6LiDUd7yAhk+AAAECBAgQIEBAqLcGCBAgQIAAAQIECHRcQKjveAENnwABAgQIECBAgIBQbw0QIECAAAECBAgQ6LiAUN/xAho+AQIECBAgQIAAAaHeGiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECotwYIECBAgAABAgQIdFxAqO94AQ2fAAECBAgQIECAgFBvDRAgQIAAAQIECBDouIBQ3/ECGj4BAgQIECBAgAABod4aIECAAAECBAgQINBxAaG+4wU0fAIECBAgQIAAAQJCvTVAgAABAgQIECBAoOMCQn3HC2j4BAgQIECAAAECBIR6a4AAAQIECBAgQIBAxwWE+o4X0PAJECBAgAABAgQICPXWAAECBAgQIECAAIGOCwj1HS+g4RMgQIAAAQIECBAQ6q0BAgQIECBAgAABAh0XEOo7XkDDJ0CAAAECBAgQICDUWwMECBAgQIAAAQIEOi4g1He8gIZPgAABAgQIECBAQKi3BggQIECAAAECBAh0XECo73gBDZ8AAQIECBAgQICAUG8NECBAgAABAgQIEOi4gFDf8QIaPgECBAgQIECAAAGh3hogQIAAAQIECBAg0HEBob7jBTR8AgQIECBAgAABAkK9NUCAAAECBAgQIECg4wJCfccLaPgECBAgQIAAAQIEhHprgAABAgQIECBAgEDHBYT6jhfQ8AkQIECAAAECBAgI9dYAAQIECBAgQIAAgY4LCPUdL6DhEyBAgAABAgQIEBDqrQECBAgQIECAAAECHRcQ6jteQMMnQIAAAQIECBAgINRbAwQIECBAgAABAgQ6LiDUd7yAhk+AAAECBAgQIEBAqLcGCBAgQIAAAQIECHRcQKjveAENnwABAgQIECBAgIBQbw0QIECAAAECBAgQ6LiAUN/xAho+AQIECBAgQIAAAaHeGiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECotwYIECBAgAABAgQIdFxAqO94AQ2fAAECBAgQIECAgFBvDRAgQIAAAQIECBDouIBQ3/ECGj4BAgQIECBAgAABod4aIECAAAECBAgQINBxAaG+4wU0fAIECBAgQIAAAQJCvTVAgAABAgQIECBAoOMCQn3HC2j4BAgQIECAAAECBIR6a4AAAQIECBAgQIBAxwWE+o4X0PAJECBAgAABAgQICPXWAAECBAgQIECAAIGOCwj1HS+g4RMgQIAAAQIECBAQ6q0BAgQIECBAgAABAh0XEOo7XkDDJ0CAAAECBAgQICDUWwMECBAgQIAAAQIEOi4g1He8gH0b/s6dO8MVV1xROa2//du/Da997WvDz/zMz4S/+qu/Kn3chz70obBly5bw0z/90+HlL395+OM//uNw7LHHrjw27+PHfuzHjnpMenB87s/+7M+GcY+Jj53UZmrnyiuvDDt27Fg17vvuu2801nvvvTfE+b3pTW+qnH/xsfmD8ranscz7euyxx0a28Sd3S/2lx3z5y18Of/InfxJe8pKXrKpH9E9tHD58OPzqr/5q2Ldv3+jpaYxVY3v9618/avOUU04Jk/rJ559q9Pd///fhB3/wBysNi+OLD5rW/+/+7u9WtXvBBReE3/qt3wobNmwo7Wucf/G542oaGy7rZ9y6XEsNYz9pDZZNJl9bqY/4Gsw909qJr5uyn6o1PqnN1Fa+puK/T3rd5LXLH1/su8o81vOhhx5aqXmxvsV1m8ZZ/H3Z+0eqYXp81RzS+ih7L+vb+7/5ECAwn4BQP5+fZ9csME0QXWuozz+486HnH6p5eCj70J22zXGhvvjhXhb6i+OcJQBOY7mIUF8MobnPpFAf55oCUjpYqDp4qCvUT+Ofh8LU97hgP8k/WcxS09jvpHW5jFBf9Bh30FH19lB2IDCpzbK5F9ufFIrjQXPZ46tew2XrOL5e4kF5WW2LwT4P7PmBQtnvyw5OirWedGKh5rdizREg0EEBob6DRevzkNOH5bgzb9OElrKzW6ntqtCZh5MUMi655JLw13/91+EHfuAHjjozO22bH/nIR0Zn/KvOeG7atGlU1s9//vMrZ6rL6pzmlY+l7GzzNJZ5H9PYVp1Bj+H7ZS97WTjmmGNWzvLnYTgP9cU6FwNMDH1ve9vbRmf881Afx1z1LULVayNZ5GdU01wm+afnpzCavGN/6ZuFvO8q/3xtHjx4cHSWvGx9lc1n0rpcSw3jNyP5TzF4ltXpH/7hH8KrXvWq0bcw+es1PTcP71XfMKTaj2szjm/c67rqgDg/oM7/O3mm1/+DDz44qkextqnf3/md31n5pjCty+OPP37l26iydVv8fXR6wxvesPL45JbWSj6H4utHqO/zJ5+5EahHQKivx1ErNQlME0SnCS35h38cXtW2kvjY3/zN3wwf+MAHjtqm82d/9mej0Panf/qnIf3/FIDGjSNvs+pMffrQjmEgBqS4baTsLGbirQr18feprXHBeVKZprGtCvWx7TzkFcNK3FY1aWxFp61bt9YS6qvCadFskn/Zwdsky6q1nPvFdmYJ9bHdta7LNOZ8DHmoL4bJqu1KP/mTPxl+6qd+ajT2+P+L28qqQn3sv8wlretxbRZD/TRb1arqk48tP2BLBxgf//jHVw7Yoscv//Ivj/47HoTF12nxJECxzbT1r3iQVvb74naa/LUbx55q9PTTT4evfOUrpVsJJ61BvydAYFgCQv2w6t362Y7bspDOVKWAXranPj/blj44Zz0bmj5QY0iN+6Y/+clPjj7Ii2fSxgXsqrOeZfveU0BL203ic6vORI/rM//dtddeW3l9QtVZv3lD/S/90i+Fn/iJnxgdmLzjHe8YnZGMZ1/Tv9dxpr7qWoqyM7XjwmkKmNP4V33jMO4FVRXqU1upBmlt5ttDUtvFUD3NulxLDYuhvvitT5lpCqjFs87FAJzWb/xmatx+++LvpmkztptvPSrby15Vk+Jzx22bKnuN5fvpi33kBwEp9BftykJ7sY2ytZIO3uK3A/H/j3tfaP0buwESILAUAaF+Kcw6mVZgUaE+XTw56eLGNM78bF5xe0AK3PnX9lUXTBZDzqSDgmK4KbtgdppQnw5k9u7du/RQH8/WxjOa8Szle9/73vDzP//zo/8fz8D+6I/+6MQLZaPVWvfU5wF0UjgtsxznX7YPety3KpP21OdbeaYJ9dOsy3lCfXF/edlrpazt4rdNxW/DJoX6VK9p20yvzbI98NNsTcmfN+kamfT79No/7rjjjrrIPR0YFr+FKgvw40J9qmnRu2iatqEJ9dN+ingcgeEKCPXDrX0rZ76o7TezhPqyu2sUsdKH/byhvupiudhX1f7gtYT6SXcGKc5tLYGw+A3DjTfeGK666qpVZ+eLW1smXShbDGfjtvlM2lM/KZwWD7TKXghV/rPceaUq1Odnl6f9xmfadbmWGsYz9dNclFl1wXA0rNqOkoJ+fsCcjKdtM6/TrHcjSs+vuptMcc0UD9bS4+O1NflcyrZlzRLqy64xyL+N+eY3vznxjlStfDM3KAIEli4g1C+dXIfjBBYV6mOfxYsu8+0GxT31096NJH3YpgvmxrWZ76mfFNCqthVMs6c+hau0/WaZoT7OM/+GIPYff4rbl2ap81oulE3tV53BXat/Wrv5Rb152Cuewc0PAuPvykLjpAtlZ12X487slu2pT2t03JaWSd8+pLlOs6c+GUzbZtX7Rr6dqXj72mKd8jPvxS1DVYE+9lncT198jVddZzHuG6CyLUf5Gh13kBPHM8vr2acNAQLDEhDqh1Xv1s92lrA3LrRMe/ebsn226U41+daKsgvoyj7Yx7WZ38awLMiNC0RVob4YSvI7aswSAtZylje/FqB4n/j83vGTLpQt+9Zg1lA/TTgdd3CU+1ddVJpfYJm/uMrWcjGwpbpMe6a+al3k63KaazPyOaV94ONC47i1kZ+drhprHsDTwXbZa7nsjHfZBedVZ95TPfI7y+SPf/7zn79yN5qqawj+6I/+aNV1LmVn2FN/uW1+95u4rS7NrewASqhv/ceUARJorYBQ39rSDHNgk87axQ/ddFeUWUN92V7conLxD1uVnX1P/RVvTTlNm/FDPA8j4wLhuJAy7dnauL9/Gsv8D2FNmk/RvypsFy/8TN8a5Bcaz3LwNu196mN9/uIv/iL89m//duUfJouPiXOIP3EfdNme+HEHhPmrcpqz2vlBVf4twqQLZWMf8baRv/Ebv7Fye8/8bjVldw0ad0Fx8c5Cf/AHfxB+93d/d+UPhJW988Q6nnbaaeG8884r3RqWB9n9+/ePbuFa9ZPcy0J6VTiOcx63/idtWcuvWcjHUFbbZFPcTz9uDOO+fUivhfQNX1l9yuYwzYH2MD8tzJoAgVxAqLcmWiUwTRBda6jPz9yl/y5+/T1pn3xV4M7HnX+lXha6qg4cyrYMpLFOEyiq5pkXelyAmDYQFv+ibDroKZ75TCFnLbfbrNpTXzW2aUP99u3bw6c+9anKgFzln18DMenOK1UHLsUDp1iDdFvIqgtlYz/ve9/7wrnnnlv612XjvIvr8oYbbggXXnhh5YFNfmA2Tah/z3veM1o+H/zgByu3fxQPVONjy0J90WzcOs/XcNkdgIrroCrQV71uUnvjtmEVD6Z+7ud+btW+9qoDluI4i+8JZe8xZW+8Qn2rPo4MhkDnBIT6zpXMgAkQIECAAAECBAisFhDqrQgCBAgQIECAAAECHRcQ6jteQMMnQIAAAQIECBAgINRbAwQIECBAgAABAgQ6LiDUd7yAhk+AAAECBAgQIEBAqLcGCBAgQIAAAQIECHRcQKjveAENnwABAgQIECBAgIBQbw0QIECAAAECBAgQ6LiAUN/xAho+AQIECBAgQIAAAaHeGiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECoH/gauO2228KDDz4YLrroolUS+/fvD5deeunKv+3atSts2bJl5b8n/X7grKZPgAABAgQIEFiqgFC/VO52dRYD/d69e8MZZ5yxKtQfOHAgbNu2LWzfvn0U5GOA3717d9izZ0844YQTwqTft2uWRkOAAAECBAgQ6L+AUN//Gh81wyNHjoSdO3eG++67L7zwhS8Mr3/961eF+uuvv370nOLZ++K/Tfr9AElNmQABAgQIECDQqIBQ3yh/M53HM+/xLP2OHTvCvn37VgX4FPjjGfqzzz57ZYDpOZdcckm45pprRmfwy34f21y/fn0zE9MrAQIECBAgQGCgAkL9QAufpp2fdT906FC4+OKLw9atW4/aQx+36lxxxRXhyiuvrPz9ddddFzZt2tQp1cfDwfAfw9umGvMPhHOnepwHESBAgMB0Ah8PN0/1wP8t/HL4ofALUz3WgwgMUUCoH2LVC3NuU6j/7Gc/G5588smlV+TIc78WPvZ925berw4JECBAYHqBUx76iXDSl//D9E+o8ZEnn3xy2Lhx48wtHnri6fD+Ty3/c+19/+55YdO6Y2Yeryd0W0Co73b95h59m0L9E088Mfd81tLAvxzzWPjA885ay1Pnfs7J3/7hudvQAAECBJYpcP+z/usyu1vp64e/dUE4/al3N9J37HTdunUz9y3Uz0zmCXMICPVz4PXhqXmot6e+D1U1BwIECBBog4BQ34YqDGcMQv1wal0602nvZOPuNwNfKKZPgAABAjMLCPUzk3nCHAJC/Rx4fXhqWaifdB/6Sb/vg4s5ECBAgACBeQWE+nkFPX8WAaF+Fq0ePrYs1MdpTvqLsZN+30MqUyJAgAABAjMJCPUzcXnwnAJC/ZyAnk6AAAECBAgQKBMQ6q2LZQoI9cvU1hcBAgQIECAwGAGhfjClbsVEhfpWlMEgCBAgQIAAgb4JCPV9q2i75yPUt7s+RkeAAAECBAh0VECo72jhOjpsob6jhTNsAgQIECBAoN0CQn2769O30Qn1fauo+RAgQIAAAQKtEBDqW1GGwQxCqB9MqU2UAAECBAgQWKaAUL9MbX0J9dYAAQIECBAgQGABAkL9AlA1WSkg1FscBAgQIECAAIEFCMRQ/7H/+dQCWh7f5Fu/99lh07pjlt6vDpsVEOqb9dc7AQIECBAgQIAAgbkFhPq5CTVAgAABAgQIECBAoFkBob5Zf70TIECAAAECBAgQmFtAqJ+bUAMECBAgQIAAAQIEmhUQ6pv11zsBAgQIECBAgACBuQWE+rkJNUCAAAECBAgQIECgWQGhvll/vRMgQIAAAQIECBCYW0Con5tQAwQIECBAgAABAgSaFRDqm/XXOwECBAgQIECAAIG5BYT6uQk1QIAAAQIECBAgQKBZAaG+WX+9EyBAgAABAgSWLrBnz56wb9++lX5PPfXUcOONN4ZNmzat/Nutt94atm/fftTYLrjggrBt27bSMR86dCicf/75o9+fdtppoaqN9OSbb7453HnnnavGkjd81llnhauvvjrccMMNo19V9b10xJZ1KNS3rCCGQ4AAAQIECBBYlEAK3ccff/woKG/YsGHU1d133x3OPffcEEN2DOPxJwbyu+66a9Xj0vPjY8rCdVmoz9sYN7cHHnhg1G486DjxxBNXPTT+m1BfrSfUL+pVo10CBAgQIECAQMsExgXjGOwvueSScNNNN40CdVmorwr7aZpCfXMFF+qbs9czAQIECBAgQGBpAuPOgsdBHD58OFx22WXh9NNPD+ecc45Qv7TK1NORUF+Po1YIECBAgAABAqsE/jk8HI6Ef1m6yvrwXeHF4RVH9RvPxMcz9fne+eIDi2fn77jjjtLtN7/2a78WLr300qO2x8R2nKlferlXOhTqm7PXMwECBAgQINBjgRjqrw9nLX2GF4WPVob6W265ZdUe+Xxweagvu1A2Pqe4977YxiwXyqYLYNO+/tiOPfVrXy5C/drtPJMAAQIECBAgUCnQxlA/75n6ONnYRjyLn/beTwr1LpRdzotEqF+Os14IECBAgACBgQkcCY83NuP1YeNRfde1pz4/Gy/UN1bmVR0L9e2og1EQIECAAAECBBYuUMfdb4qh/sEHH1y17z7+rrjnvuoOOlUTtf1m7UtAqF+7nWcSIECAAAECBDolMO996uNki0H9M5/5zKrbYOYX4wr1y1seQv3yrPVEgAABAgQIEGiFQP6XXmf5i7L5Ba7Fto477rhVe+0n/UXZ3bt3j26fmX5mOVNfdjef/JuIcVuFWlGIGgch1NeIqSkCBAgQIECAAAECTQgI9U2o65MAAQIECBAgQIBAjQJCfY2YmiJAgAABAgQIECDQhIBQ34S6PgkQIECAAAECBAjUKCDU14ipKQIECBAgQIAAAQJNCAj1TajrkwABAgQIECBAgECNAkJ9jZiaIkCAAAECBAgQINCEgFDfhLo+CRAgQIAAAQIECNQoINTXiKkpAgQIECBAgAABAk0ICPVNqOuTAAECBAgQIECAQI0CQn2NmJoiQIAAAQIECBAg0ISAUN+Euj4JECBAgAABAgQI1Cgg1NeIqSkCBAgQIECAAAECTQgI9U2o65MAAQIECBAgQIBAjQJCfY2YmiJAgAABAgQIECDQhIBQ34S6PgkQIECAAAECBAjUKCDU14ipKQIECBAgQIAAAQJNCAj1TajrkwABAgQIECBAgECNAkJ9jZiaIkCAAAECBAgQINCEgFDfhLo+CRAgQIAAAQIECNQoINTXiKkpAgQIECBAgAABAk0ICPVNqOuTAAECBAgQIECAQI0CQn2NmJoiQIAAAQIECBAg0ISAUN+Euj4JECBAgAABAgQI1Cgg1NeIqSkCBAgQIECAAAECTQgI9U2o65MAAQIECBAgQIBAjQJCfY2YmiJAgAABAgQIECDQhIBQ34S6PgkQIECAAAECBAjUKCDU14ipKQIECBAgQIAAAQJNCAj1TajrkwABAgQIECBAgECNAkJ9jZiaIkCAAAECBAgQINCEgFDfhLo+CRAgQIAAAQIECNQoINTXiKkpAgQIECBAgAABAk0ICPVNqOuTAAECBAgQIECAQI0CQn2NmJoiQIAAAQIECBAg0ISAUN+Euj4JECBAgAABAgQI1Cgg1NeIqSkCBAgQIECAAAECTQgI9U2o65MAAQIECBAgQIBAjQJCfY2YmiJAgAABAgQIECDQhIBQ34S6PgkQIECAAAECBAjUKCDU14ipKQIECBAgQIAAAQJNCAj1TajrkwABAgQIECBAgECNAkJ9jZiaIkCAAAECBAgQINCEgFDfhLo+CRAgQIAAAQIECNQoINTXiKkpAgQIECBAgAABAk0ICPVNqLe4z0OHDoWLL744fOlLXzpqlK985SvDddddFzZs2BB27twZ7rnnnlWP2bp1azj77LNbPDtDI0CAAAECBAj0U0Co72dda51VCvpnnnnmKLSn/44hfsuWLbX2pTECBAgQIECAAIHZBYT62c0G94zrr78+PProo2HHjh1h/fr14cCBA+Gqq64Kl19+eTjhhBMG52HCBAgQIECAAIG2CQj1batIy8azf//+sHv37rBnz56VAB//be/evaOtOJs2bWrZiA2HAAECBAgQIDA8AaF+eDWfesZHjhwZ7Z1/6UtfGi666KKV5912222jUF/8sZ9+alYPJECAAAECBAjULiDU107anwarttnE7Tj33nvvypn6fM/9WgUOHjy41qd6HgECBAgQWJjAunXrwsaNGxfWvoYJ1CEg1Neh2NM24hn5uNUm7aUfN8342Ntvv32uLTn3339/TyVNiwABAgS6LLB582ahvssFHMjYhfqBFHrWaaatN/HuNtPcptI++1mFPZ4AAQIECBAgUJ+AUF+fZa9aqrptZVXYn+Wsfq+gTIYAAQIECBAg0AIBob4FRWjjEMbdtjLfahMfu23btrB9+3b3rW9jMY2JAAECBAgQ6L2AUN/7Eq9tgpO20+R3wNm1a5dAvzZqzyJAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAhxbtFUAAAgAElEQVQQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz03YvwaOHDkSdu7cGe65555Vk9u6dWs4++yzR/+2f//+cOmll678fteuXWHLli39wzAjAgQIECBAgEAHBIT6DhRp2UM8dOhQuPjii0MM8WVB/cCBA2Hbtm1h+/bto9/HgL979+6wZ8+ecMIJJyx7uPojQIAAAQIECAxeQKgf/BI4GiCG9quuuipcfvnlpSH9+uuvHz3poosuWnly2b+hJUCAAAECBAgQWI6AUL8c5071Es+87927N1x33XVh06ZNq8aetubEM/RpK058QHzObbfdFnbs2BHWr1/fqfkaLAECBAgQIECg6wJCfdcruIDxx3AeQ33xJ+2nr9qaM+5AYAFD1CQBAgQIECBAgEBBQKi3HI4SiFtp7r333pUz9SnIn3nmmeEtb3lL6X77OkL9/fffrxoECBAgQKB1Aps3bw4bN25s3bgMiEBRQKi3HqYSiGfvb7/99nDFFVeEK6+88qiLaOsI9QcPHpxqLB5EgAABAgSWKbBu3Tqhfpng+lqTgFC/JrbhPSmF9quvvjrccMMNo7ve2FM/vHVgxgQIECBAgEA7BYT6dtalsVFVXQgbz9THYB8vhN23b99ofO5+01iZdEyAAAECBAgQWCUg1FsQRwmkrTbp7jf5fendp96iIUCAAAECBAi0S0Cob1c9WjOa/A44+V+M9RdlW1MqAyFAgAABAgQIBKHeIiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECotwYIECBAgAABAgQIdFxAqO94AQ2fAAECBAgQIECAgFBvDRAgQIAAAQIECBDouIBQ3/ECGj4BAgQIECBAgAABod4aIECAAAECBAgQINBxAaG+4wU0fAIECBAgQIAAAQJCvTVAgAABAgQIECBAoOMCQn3HC2j4BAgQIECAAAECBIR6a4AAAQIECBAgQIBAxwWE+o4X0PAJECBAgAABAgQICPXWAAECBAgQIECAAIGOCwj1HS+g4RMgQIAAAQIECBAQ6q0BAgQIECBAgAABAh0XEOo7XkDDJ0CAAAECBAgQICDUWwMECBAgQIAAAQIEOi4g1He8gIZPgAABAgQIECBAQKi3BggQIECAAAECBAh0XECo73gBDZ8AAQIECBAgQICAUG8NECBAgAABAgQIEOi4gFDf8QIaPgECBAgQIECAAAGh3hogQIAAAQIECBAg0HEBob7jBTR8AgQIECBAgAABAkK9NUCAAAECBAgQIECg4wJCfccLaPgECBAgQIAAAQIEhHprgAABAgQIECBAgEDHBYT6jhfQ8AkQIECAAAECBAgI9dYAAQIECBAgQIAAgY4LCPUdL6DhEyBAgAABAgQIEBDqrQECBAgQIECAAAECHRcQ6jteQMMnQIAAAQIECBAgINRbAwQIECBAgAABAgQ6LiDUd7yAhk+AAAECBAgQIEBAqLcGCBAgQIAAAQIECHRcQKjveAENnwABAgQIECBAgIBQbw0QIECAAAECBAgQ6LiAUN/xAho+AQIECBAgQIAAAaHeGiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECotwYIECBAgAABAgQIdFxAqO94AQ2fAAECBAgQIECAgFBvDRAgQIAAAQIECBDouIBQ3/ECGj4BAgQIECBAgAABod4aIECAAAECBAgQINBxAaG+4wU0fAIECBAgQIAAAQJCvTVAgAABAgQIECBAoOMCQn3HC2j4BAgQIECAAAECBIR6a4AAAQIECBAgQIBAxwWE+o4X0PAJECBAgAABAgQICPXWAAECBAgQIECAAIGOCwj1HS+g4RMgQIAAAQIECBAQ6q0BAgQIECBAgAABAh0XEOo7XkDDJ0CAAAECBAgQICDUWwMECBAgQIAAAQIEOi4g1He8gIZPgAABAgQIECBAQKi3BkoFrr/++nDHHXes/G7Xrl1hy5Yto/8+cuRI2LlzZ7jnnntWPXfr1q3h7LPPJkqAAAECBAgQILBkAaF+yeBd6C4G+kcffTTs2LEjrF+/Phw4cCBs27YtbN++fRTsDx06FC6++OIQQ3wK+l2YlzESIECAAAECBPoqINT3tbJrnFdVYI9BP/5cdNFFo5B/1VVXhcsvvzyccMIJa+zJ0wgQIECAAAECBOoSEOrrkux5O8VQv3///rB3795w3XXXhU2bNvV85qZHgAABAgQIEGi/gFDf/ho1PsJ09v7MM88c7Zm/7bbbRqG++FPHfvonnnii8bkaAAECBAgQKBNYt24dGAKtFhDqW12e5geXLoqNI0l77ONZ+3vvvXflTH0e+tc66s9+9rPhySefXOvTPY8AAQIECCxE4OSTTw4bN25cSNsaJVCXgFBfl2QP20mB/pFHHpm41Saevb/99tsnPq6HTKZEgAABAgQIEGhcQKhvvATtHMAsgT7OwD77dtbRqAgQIECAAIFhCAj1w6jzTLMs23KTGki/i7eyLN6TPp6pj8E+bdGZqUMPJkCAAAECBAgQmEtAqJ+Lr59Pzu9Tn88y32qT38e+nypmRYAAAQIECBBor4BQ397aNDKyFNC/9rWvHdX/93//96+cic/vgFP8i7ONDFynBAgQIECAAIEBCwj1Ay6+qRMgQIAAAQIECPRDQKjvRx3NggABAgQIECBAYMACQv2Ai2/qBAgQIECAAAEC/RAQ6vtRR7MgQIAAAQIECBAYsIBQP+DimzoBAgQIECBAgEA/BIT6ftTRLAgQIECAAAECBAYsINQPuPimToAAAQIECBAg0A8Bob4fdTQLAgQIECBAgACBAQsI9QMuvqkTIECAAAECBAj0Q0Co70cdzYIAAQIECBAgQGDAAkL9gItv6gQIECBAgAABAv0QEOr7UUezIECAAAECBAgQGLCAUD/g4ps6AQIECBAgQIBAPwSE+n7U0SwIECBAgAABAgQGLCDUD7j4pk6AAAECBAgQINAPAaG+H3U0CwIECBAgQIAAgQELCPUDLr6pEyBAgAABAgQI9ENAqO9HHc2CAAECBAgQIEBgwAJC/YCLb+oECBAgQIAAAQL9EBDq+1FHsyBAgAABAgQIEBiwgFA/4OKbOgECBAgQIECAQD8EhPp+1NEsCBAgQIAAAQIEBiwg1A+4+KZOgAABAgQIECDQDwGhvh91NAsCBAgQIECAAIEBCwj1Ay6+qRMgQIAAAQIECPRDQKjvRx3NggABAgQIECBAYMACQv2Ai2/qBAgQIECAAAEC/RAQ6vtRR7MgQIAAAQIECBAYsIBQP+DimzoBAgQIECBAgEA/BIT6ftTRLAgQIECAAAECBAYsINQPuPimToAAAQIECBAg0A8Bob4fdTQLAgQIECBAgACBAQsI9QMuvqkTIECAAAECBAj0Q0Co70cdzYIAAQIECBAgQGDAAkL9gItv6gQIECBAgAABAv0QEOr7UUezIECAAAECBAgQGLCAUD/g4ps6AQIECBAgQIBAPwSE+n7U0SwIECBAgAABAgQGLCDUD7j4pk6AAAECBAgQINAPAaG+H3U0CwIECBAgQIAAgQELCPUDLr6pEyBAgAABAgQI9ENAqO9HHc2CAAECBAgQIEBgwAJC/YCLb+oECBAgQIAAAQL9EBDq+1FHsyBAgAABAgQIEBiwgFA/4OKbOgECBAgQIECAQD8EhPp+1NEsCBAgQIAAAQIEBiwg1A+4+KZOgAABAgQIECDQDwGhvh91NAsCBAgQIECAAIEBCwj1Ay6+qRMgQIAAAQIECPRDQKjvRx3NggABAgQIECBAYMACQv2Ai2/qBAgQIECAAAEC/RAQ6vtRR7MgQIAAAQIECBAYsIBQP+DimzoBAgQIECBAgEA/BIT6ftTRLAgQIECAAAECBAYsINQPuPhDn/rfffmp8J8e/Fb41refkfg3m54V3n3Kc3vN8hf/+K3w/zzy1Mocf2jzs8OP/y/P6eWcv3L46fB///2/hkNPPD2a36Z1x4Sff+1zw8s2HNPL+cZJWdMh9HlNp4X72599Mjz8jWfW9fpnh/AzJz83nPSiZ/VyXQ/xddzLQprUUgSE+qUw66RtAl/42rfDH9//r+GEFz4T5FMY+l9f1t+Qm88xBvz/9pWnwn84/jnhTS9/dttKNPd4YvA5/K0wCvLxJwb8Dc8J4f/4t8+bu+02NmBNPyf0fU3HdfeH9/1rOPD1b68E+bTO//fXPje8vIcHrEN7HbfxvcWYuiMg1HenVkZao0D8YHzkm0+vOnOb/q3PH46RsBhq4wdm/m81MjfWVDyA+c9f+lb4kVd+54Cl7N8aG+ACOramn0Ht65qOc0sHbv/+pd85+VD2bwtYXo00OcTXcSPQOu2NgFDfm1KayCwCZR/88QPkvzz0VPjpE5/Tu6+y01fYm59/zKotRvHM5uf+6duhbwcycV7//dGnVm1LSAav++5n9XLLkTX9zDtAX9d0nFvVgWms/Yuft/q1Pcv7YVsfO8TXcVtrYVzdEBDqu1Eno6xRoCrg9vlMbtXZvLIPzRqpG2uq7Kx1Vd0bG2SNHVvT37kupK9rOh2w5Aerff52Ymiv4xrfEjQ1UAGhfqCFH/K0BaD+B6ChhQFruv9rWqh/5gL3Ph+cD/lz2dzrERDq63HUSocEBKD+ByCh/pkXpG+f+nVHmKpvIfp6HcHQXscd+hg11JYKCPUtLYxhLVbA/uNnfNP+477d6nGIe3Gt6X6v6XEHafbUL/bzQusEuiIg1HelUsZZq8C4O4X0LeAmuLLQ19czfEO8a4Y1/cxK7+uajnNz95t+f/tU64ecxgYpINQPsuwm7Z7e/b+n99Dub21N939Nx3fuqvvU9/lkxJD+3oRPZwLzCAj18+h5bqcF/PXNfv/1zSH+JUprut9ruvitm78o29+/DN3pD1aDb1RAqG+UX+cECBAgQIAAAQIE5hcQ6uc31AIBAgQIECBAgACBRgWE+kb5dd4Ggf3794dvfOMb4c1vfnMbhrOUMdxxxx3hpJNOCqeccspS+muyk8cffzx8+MMfDr/4i7/Y5DCW2vd9990XvvCFL4Qzzjhjqf022dmdd94ZXvCCF4QtW7Y0OYyl9h3XdXzfesUrXrHUfpvo7OGHHw6xxu9617ua6F6fBDohINR3okwGuUgBoX6Rus23LdQ3X4NljECoX4Zyc30I9c3Z67k7AkJ9d2plpAsSEOoXBNuSZoX6lhRiwcMQ6hcM3HDzQn3DBdB9JwSE+k6UySAXKSDUL1K3+baF+uZrsIwRCPXLUG6uD6G+OXs9d0dAqO9OrYx0QQJC/YJgW9KsUN+SQix4GEL9goEbbl6ob7gAuu+EgFDfiTIZ5CIFhPpF6jbftlDffA2WMQKhfhnKzfUh1Ddnr+fuCAj13amVkS5IQKhfEGxLmhXqW1KIBQ9DqF8wcMPNC/UNF0D3nRAQ6jtRJoNcpIBQv0jd5tsW6puvwTJGINQvQ7m5PoT65uz13B0Bob47tWrVSGMQvvTSS1fGtGvXrs7eH1qob9XSqn0wQn3tpK1sUKhvZVlqG5RQXxulhnosINT3uLiLmtqBAwfCtm3bwvbt20dBPobi3bt3hz179oQTTjhhUd0urF2hfmG0rWhYqG9FGRY+CKF+4cSNdiDUN8qv844ICPUdKVSbhnn99dePhnPRRRetDKvs39o05nFjEeq7Uqm1jVOoX5tb154l1HetYrONV6ifzcujhykg1A+z7mue9ZEjR8LOnTtHZ+jPPvvslXZiML7tttvCjh07wvr162dq/z/+jxDu+9pMT6n1wZu/sj+s+9Y3wj8c9+Za2x3X2P3ZfJ/9rBBes3Fp3YdTvnhHOPjik8Jjm05ZeKeHvxXCl76xupuXbgjhxc9beNejDp735OPh397/4fCJ1/3icjoMIXzlcAhfe3J1d695YQjPPmY5Qzj20H3hJf/8hXDfq89YTochhP/38RCe+vbq7k5+0dK6D6966M7wxHNeEB552ZaldZq/jjc8J4RXvmBp3Y/W9RePe3P4lxe8YuGd/vOTITx6eHU33/uCEJ7/nIV3Pergu77xcHj1Q3eGz578ruV0WNHLvtMb7V7nBMYKCPUWyEwChw4dChdffHHYunXrqj30MdTv3bs3XHfddWHTpk0ztRlD/c0HZnpKrQ9+w+P7wwu//Y3wNy9aXqgPMdw9Xes0ZmrszH+6I9y3/qRw//MXH+pH81xSmC1D2PjU4+Fdj344/F+blxfqZyrGAh588jfvC6cc+UK4/buXF+oXMI2ZmnzL1+4MX3/WC8InNy4v1M80wAU8OK7r+L71yLrFh/oFDH+mJjc/8XCINf7wS5sL9RufG8J/edtMw/ZgAksVEOqXyt39zhYZ6p/+m32NAL3x2Ge6/cRjjXTfSKdxzo//awj3f30J3Tcd6p8bwskvHFZ943xjAPnkY985mno6PB2OCceEqv8tWwmTnpN+X3zuMp4T+4g/xfm84dhn/i2+jqvmmT9nmrHO+pz4+GnaTWMc1/6kvuOcH/nmMeHhw9+p7aTnlI1tWc+Jc57VJz3n5RueDq94/vj6psfma3lSPca9C66s8Ve9MWw86Y1C/RI+MnSxdgGhfu12g3zmIkN9+PP/c5CmJk2AAAECLRd4/RlCfctLZHghCPVWwUwCi9pT3+T2m5kA6npww9tv6prGVO00fKZ+qjF6EAECBCYI2H5jibRdQKhve4VaOL66737zN19u4SQXPKTP/fPRHbzuxQvutKHmH/5mCP+UXTQah9LX+ca5ldU3Xgi94dkNFWEJ3Q5pTSfOIc25bK7f/bww2hIzpJ+3vHxIszXXrgkI9V2rWAvG27f71LeA1BAIECBAgAABAnMJCPVz8Q33yX36i7LDraKZEyBAgAABAn0REOr7UknzIECAAAECBAgQGKyAUD/Y0ps4AQIECBAgQIBAXwSE+r5U0jwIECBAgAABAgQGKyDUD7b0Jk6AAAECBAgQINAXAaG+L5U0DwIECBAgQIAAgcEKCPWDLb2JEyBAgAABAgQI9EVAqO9LJc1jLoE9e/aEV7/61eGcc86Zq522PznOc9++fSvDvPnmm8Npp53W9mGveXyHDx8Ol112WfjoRz86auOCCy4I27ZtW3N7XXrioUOHwvnnnz+ab59rnK/pIdT51ltvDdu3bx8tx+OOOy7cdNNN4cQTT+zS8pxqrPnrt/ikPs97KhwPIlAiINRbFoMXSKFg9+7dvQ71cZ6PPPJIuPrqq8OGDRvCAw88EM4777xwzTXX9DL0pUCwefPmUbBN/3366af3us7pBZ3WdZ8P3IZW01jbWNe777473HjjjWHTpk0hBvxbbrll5b/7/oaev677Pl/zIzCLgFA/i5bH9kogncn86le/Gl7ykpeEd73rXb0Ne1VnbWNAiD99PHsdD1p27doVrr322lH4iT8xDMUAlA5serWgC5OJ83z/+98fDh482NuDtjjdoXwbkUpbdiA+NIOhHcT09T3KvBYjINQvxlWrHRCIHw5f/OIXw4UXXjjaojGUM7jF0vQ51JctwRh245zTWc4OLNOZhxiDXzxIe9/73jeaa5+336S5xnn2cftJXvwhrN9xC77v3y7O/GL3BAKZgFBvSQxeYIhf4RfPcr7zne/s7TcUxcWdzmj2eb7FtfzWt76193vqY8g999xzV72H9fm6iXgi4q677gpvf/vbw3ve857RvIe0tzzfQjj4Dy8ABIR6a4DAaoEhhvo05yjR960oxYvtTj311F6fpU+hL9b0yJEjvQ/1cb433HDDyoWifd9vnS6QLR645AZ9fX8f2jajvtbRvBYr4Ez9Yn213gGBoYX6NN8HH3yw1wG3avvNJZdc0su7heRbUYYaguLZ+77WuHjQFi92jz99P5BJr+Ohbz3qwEepIbZAQKhvQREMoVmBIYX6IQf6YgDq4/UTxdsc5q+oPm9Jyefa5332ZaE+zn8I18YMYY7NfhLqvQ8CQn0fqmgOcwkMJdQPactNXBBlZ/aGUus4/yGcqS8Len0+o1t2R6chrOkhzHGuDzFPJvD/Cwj1lsLgBYbygTG0i8xSqI1/eCndsnNIt8MbQqjPt9oM4WLo/EBmCHvqh7CWB/9BDKAWAaG+FkaNdFlgCKE+3QruoYceOqpUZ511Vm8vlk1h4NOf/vRo3n2/ULZY3KEEofwOOH3/I3Jpu036y9BDuPtNn7dUdfmz09jbJyDUt68mRkSAAAECBAgQIEBgJgGhfiYuDyZAgAABAgQIECDQPgGhvn01MSICBAgQIECAAAECMwkI9TNxeTABAgQIECBAgACB9gkI9e2riRERIECAAAECBAgQmElAqJ+Jy4MJECBAgAABAgQItE9AqG9fTYyIAAECBAgQIECAwEwCQv1MXB5MgAABAgQIECBAoH0CQn37amJEBAgQIECAAAECBGYSEOpn4vJgAgQIECBAgAABAu0TEOrbVxMjIkCAAAECBAgQIDCTgFA/E5cHEyBAgAABAgQIEGifgFDfvpoYEQECBAgQIECAAIGZBIT6mbg8mAABAgQIECBAgED7BIT69tXEiAgQIECAAAECBAjMJCDUz8TlwQQIECBAgAABAgTaJyDUt68mRkSAAAECBAgQIEBgJgGhfiYuDyZAgAABAgQIECDQPgGhvn01MSICBAgQIECAAAECMwkI9TNxeTABAgQIECBAgACB9gkI9e2riRERIECAAAECBAgQmElAqJ+Jy4MJECBAgAABAgQItE9AqG9fTYyIAAECBAgQIECAwEwCQv1MXB5MgAABAgQIECBAoH0CQn37amJEBAgQIECAAAECBGYSEOpn4vJgAgQIECBAgAABAu0TEOrbVxMjIkCAAAECBAgQIDCTgFA/E5cHEyBAgAABAgQIEGifgFDfvpoYEQECBAgQIECAAIGZBIT6mbg8mAABAgQIECBAgED7BIT69tXEiAgQIECAAAECBAjMJCDUz8TlwQQIECBAgAABAgTaJyDUt68mRkSAAAECBAgQIEBgJgGhfiYuDyZAgAABAgQIECDQPgGhvn01MSICBAgQIECAAAECMwkI9TNxeTCB+gQOHToUzj///FGDN954Y9i0aVN9jWuJAAECBAgQGJSAUD+ocptsmwT27NkTXv3qV4dzzjmnTcMyFgIECBAgQKCDAkJ9B4tmyN0XOHz4cDhy5Iiz890vpRkQIECAAIFWCAj1rSiDQfRF4NZbbw3bt28/ajq7d+8+6ox8PFP/yCOPhKuvvjps2LBh5TlpW85Xv/rVcNNNN4UTTzxxVXvxgOCyyy4LH/3oR1f+/ayzzjqqnTLTu+++O5x77rkrvzruuOOm7uOCCy4I27ZtW9XsAw88EM4777zw0EMPHdXdqaeeumpbUZzvvn37Sktd9BnXZnxy8bH5fOLvb7755nDaaaeN+kmWn/70pyeOr+rxZbVL/Rb7Sh3E38W5xi1V8Sduscr7z23i49K8r7nmmtH4y+ZWnETqO/YVH1u2hSv+Lv7Euo3zj4+JY/r1X//18Cu/8iul9ZzVftxrOs31e77ne6baepaPPfcre00U+0+vj3ggHesx6bX14IMPHjWu9Nouex1Mux6KW+wmvc5zv+J7y6T5V70fRPe0Fj7ykY+svB7L1mPsv+iezztfn2WvhTSHNNd3vvOdvpnsy4edebRSQKhvZVkMqosCKVhs3rx5Vfgt+/eqx6YP0o9//OPh4MGDIQW85FH1vHHBLj03PuaOO+5YFeLLwkgKXGecccaqeZT1EZ9/ySWXlB4YFA9a4hjigUhuUxZmY3i55ZZbJquODOQAAA0GSURBVIa9sr7T2C+88MJReCiGmPzgKD+oShbFEJ+84ziLB18p7JSFpzj+u+66a/T4z3zmM6U+ZWPP/63qoK/42igetOQHH2nsp59++lFBKj+ASG3WaV/1Gk7j+sd//MfRQ+I889oUn1vmEMd5ww03rKy7qvnkY0jr4UUvetHoACbvN7b7l3/5l+GNb3xj+IVf+IWVg+3U/mte85rw4he/+KgD6GnXQ/HgPT6n6nWejzt/7ZW9toprtGrtpC1/X/ziF1cdCOZ1z99n8rWUv2+M8y8ecI0L/l18zzdmAm0TEOrbVhHj6azAuLNRebhMj41nzdJZ5WLAvfLKK8Of//mfhzyQVYXUqvamCWvFEJrOZJadUSs7oBgXAsvOWI9rN811mjCbwmD83/zbgzxUp7Pm+YXIxfE99thjo28c8oOo2H5e1+Rw0kknhU984hPh0ksvXQmHudE4n+JZ9NhPcdzjDoKKL5C0Ht7+9rePvg0oHniMC1rFuRdd6rQvBtjimNPBS7SO/eWvgbKDlvwxZSGz6uCy2F4y3rhxY/jxH//xVa+9WOf4uvuRH/mR8LnPfe6oA9r4rVp0jgcTxW9FZlkPaSypNlWv80kGxfeBuHbzuZe9T6Sap29jims9f/8oWx/JbseOHWHnzp1HHaDn6znNIR28THMA19k3fwMn0BIBob4lhTCM7gtMc7YqBdeqM9zpgzGeaY5ntstCfVX4rBKcFPiLz5sU6vIP+6oP8jykxu056Wv//OxocXzf933fV3lGP5/fpLHmY8hDZprLBz7wgVG4LPsWoXhQlM6+pwOfOJ+4TSP+pIud8wOAcWMsWq5fv37VvKfdrpDaiEHz937v98K73/3ulQOMab9FSS7jvj1ai33ZeiyG8be+9a2jrTDThPpJ2zam/YYhrdc4tje/+c2rQn1sI/7Es9jFC9jzAJ0fJBbX76T1UAy68f9Xvc4nvRuO+wYqPjf//bhvbeLjy16D+XtP8aCz7ICt7L0gPSdu78kPOifN0e8JEJhdQKif3cwzCJQKjAtR+YdqWQjJP4irAnPaW1u1DzYfXNVZ2fxx0wTJac++51/PV4WufHtLMTAXv8EoAx+39SQPT/nZ/Pj75PuOd7yj8ix9WagvHqDE3+/atStce+21o4ueiwd2kw5QxllOCm1l44pbq2IgTXOdZJ4fxMxy8DeNfVnNyg5kyrYHFZ+b1lLV9R/FWpbVObVVfA2mf0sHY9E71jEeSMdvO4rfvhRfh2V1Kf7buPWQ1vO0r/Oqt9lpDr7KtpZVfWMV+ymulXjmv+wAfFyoLzuhUaz17//+74+mM64+PlYIEJhfQKif31ALBEYC484W5qE+/9Ct2tqSzg7nZ8aKoWpc2EnjqmqnWLppgmTZlpWyi2TzMaU9x2VLpXgB3rQXhxbbKV5AWGxr3NnJ4p7sGGLGBZ7cMIbntOc/nWGPZ5LTxa2prfi8eCa66izzuO0/VRdcxzaTbfzf4nUKKZimA4yqg8Kqg7c67ccdhBU9xn3TU2wjvxB22ouhYxtpTeRn1IsHQGmv+fHHH79qLeQH6mUHPsXX/bj1EA/6Zn2dVxmUXbydHhvnUrx2ZtJZ+rTWkmnV+1hVrcquwSm6pXU66eDNxwgBAvMLCPXzG2qBwEhgXECZ9PV21YWTk8Jm6jfeVabqIrRpg9M0oX7SRaBlgaAsyFQFy2m3UZQtuRQu4kWO8WxrOus/6c4303yTUTTMD8jimFNALPqM23KUHyjEC2qLtZ5ma1HVXv8YmtO3BFUXyVadiZ3mAuVp7Mu2Z5TVdtKWjrK+UghNwXbai2SLr7F4IHfnnXeOzhwXD4biGeV0R6rYd35xd1lAnnY9RJN5XufJIs03XQye/j2NLb9zT+wz1jW/y1Z678gvni+rSdWBQToQzO9eVVxfs3wD5KOEAIH5BIT6+fw8m8BIYJqzYWV7suPZ3fwsZJG0eMZ7XAAaFwLHPa/4gXvyySeP3eOch8hxH/7FrR1VH+plAWeWMFu2F7sY8KLjNBdPTjqYKdtSUwzLKRTGiwfjxYdp7uMOFnKTeS6SjWbpOoV0gDFuS9G4M7Flt1gtrsdxAW1cuC5+s5S/ZVRtIxvnV5zD/fffP1Wd8wOuP/zDPxxts4nXI8T99fl2qXHfXKSz/2Wv+6r1MO3rfJq31PxAPfnGbxqK4b2qXlUHAFUnJ8peI/kZ/jTucd/KufvNNNX1GAJrFxDq127nmQRWBMaFnbI7SxTDZnErSPEi0mnuSJEGMC64j9vrnz9v3Fn9PAxWPTbvryo0T7ptXtXyGncAlW+xmObM86QDsuKBRtWe//iY173udeGDH/zgynabcTUZF+KnPbNZFnrjc9MdXOLZ07K/c1B24DTJID8bXPYNwLhxxz7jePN76Y87oBr3u+LcP/axj63cQrTqjjt5WI1tx1D/3ve+N3zoQx8aXbBarG06wI0H3ePurjTreijehjOZjnOrOrBJ24XSbVvjxfP5LWhj+2VrsOoAYNx7Sf5az7f4jPsoWMu3MT5aCBBYm4BQvzY3zyKwSqAqOJdtMymG49hI3HddFh7yoFX1YZxflJqXpuqsXFnQSn0Ux1P2/HEX1Zad0a8K12UW4+6GUgwe8Y98Fc/85RfdxgA16cxzaq9sS0OZd1XQjDV4//vfv/K3BcZdJJsfHOVek745mHQgF+v6+c9/Pnz9618/KkRXXWQ57YFECoqT7Ivhumq7SGxrXL9V6zZvb5rtZWWvpXTwc+yxx45ef0X3T33qU6vug198TeXXQpRtZcrXw7iDhGkOUovXIZTt4Y/jq/ojdvnrqeoAq/haKM4pP+kw6f0mf/+Zpj4+TggQqEdAqK/HUSsDF0hfRecMZRex5vuzy85gpnbyD8Syr/CnvQtOPsayv4wZ+y3rI3/spH3MxeBQ3Kecn0ktBrS4daD4125zy/wPPZVtj0h7e6e5Q0jeftkWkfyCxKqtK+m5sc1xf0k2/r7MMg9RZX+VOI03Pj/dCrHsNpzJpay+47ZC1WVfdM0PtPL6T/MNQb6do/iaGretJ44jPTb+/+KtYMvGlWqb7uOe71kvC70x/JcdsObrIb4GZnmdFw3zORZf71VbhOK83/SmN42+fSj72wVlF7cXD5CL7Rb7G7eFqGy9TVPfgX90mD6BWgWE+lo5NUaAAAECBAgQIEBg+QJC/fLN9UiAAAECBAgQIECgVgGhvlZOjREgQIAAAQIECBBYvoBQv3xzPRIgQIAAAQIECBCoVUCor5VTYwQIECBAgAABAgSWLyDUL99cjwQIECBAgAABAgRqFRDqa+XUGAECBAgQIECAAIHlCwj1yzfXIwECBAgQIECAAIFaBYT6Wjk1RoAAAQIECBAgQGD5AkL98s31SIAAAQIECBAgQKBWAaG+Vk6NESBAgAABAgQIEFi+gFC/fHM9EiBAgAABAgQIEKhVQKivlVNjBAgQIECAAAECBJYvINQv31yPBAgQIECAAAECBGoVEOpr5dQYAQIECBAgQIAAgeULCPXLN9cjAQIECBAgQIAAgVoFhPpaOTVGgAABAgQIECBAYPkCQv3yzfVIgAABAgQIECBAoFYBob5WTo0RIECAAAECBAgQWL6AUL98cz0SIECAAAECBAgQqFVAqK+VU2MECBAgQIAAAQIEli8g1C/fXI8ECBAgQIAAAQIEahUQ6mvl1BgBAgQIECBAgACB5QsI9cs31yMBAgQIECBAgACBWgWE+lo5NUaAAAECBAgQIEBg+QJC/fLN9UiAAAECBAgQIECgVgGhvlZOjREgQIAAAQIECBBYvoBQv3xzPRIgQIAAAQIECBCoVUCor5VTYwQIECBAgAABAgSWLyDUL99cjwQIECBAgAABAgRqFRDqa+XUGAECBAgQIECAAIHlCwj1yzfXIwECBAgQIECAAIFaBYT6Wjk1RoAAAQIECBAgQGD5AkL98s31SIAAAQIECBAgQKBWAaG+Vk6NESBAgAABAgQIEFi+gFC/fHM9EiBAgAABAgQIEKhVQKivlVNjBAgQIECAAAECBJYvINQv31yPBAgQIECAAAECBGoVEOpr5dQYAQIECBAgQIAAgeULCPXLN9cjAQIECBAgQIAAgVoFhPpaOTVGgAABAgQIECBAYPkCQv3yzfVIgAABAgQIECBAoFYBob5WTo0RIECAAAECBAgQWL6AUL98cz0SIECAAAECBAgQqFVAqK+VU2MECBAgQIAAAQIEli8g1C/fXI8ECBAgQIAAAQIEahUQ6mvl1BgBAgQIECBAgACB5QsI9cs31yMBAgQIECBAgACBWgWE+lo5NUaAAAECBAgQIEBg+QJC/fLN9UiAAAECBAgQIECgVgGhvlZOjREgQIAAAQIECBBYvoBQv3xzPRIgQIAAAQIECBCoVUCor5VTYwQIECBAgAABAgSWLyDUL99cjwQIECBAgAABAgRqFfj/AJphKd4O7NyaAAAAAElFTkSuQmCC', `mes` = '8', `anio` = '2024', `tipo` = '7', `semana` = '32'
WHERE `id` = '281' 
 Execution Time:0.16694808006287

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-8-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 8
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.18717217445374

SELECT DISTINCT(os.id), `la`.`nombre` as `actividad`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, WEEKOFYEAR(os.fecha) AS num_semana, `os`.`ot`, `os`.`observaciones`, `os`.`seguimiento`
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 8
AND DATE_FORMAT(os.fecha, '%Y') = 2024
AND (`os`.`seguimiento` = 1 or `os`.`seguimiento` = 4 or `os`.`seguimiento` = 5)
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.22589421272278

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.15344190597534

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 1 
 Execution Time:0.1431930065155

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 3 
 Execution Time:0.14405798912048

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 4 
 Execution Time:0.14240384101868

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 5 
 Execution Time:0.14811706542969

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.14412617683411

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 1 
 Execution Time:0.15343594551086

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 3 
 Execution Time:0.15023398399353

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 4 
 Execution Time:0.14814686775208

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 5 
 Execution Time:0.15629696846008

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12' 
 Execution Time:0.15074396133423

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 1 
 Execution Time:0.16499614715576

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 3 
 Execution Time:0.15665292739868

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 4 
 Execution Time:0.1851270198822

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 5 
 Execution Time:0.14916586875916

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13' 
 Execution Time:0.18294906616211

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 1 
 Execution Time:0.14865589141846

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 3 
 Execution Time:0.15610480308533

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 4 
 Execution Time:0.14831686019897

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 5 
 Execution Time:0.1543071269989

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19' 
 Execution Time:0.15683698654175

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 1 
 Execution Time:0.15790987014771

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 3 
 Execution Time:0.15362286567688

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 4 
 Execution Time:0.14999318122864

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 5 
 Execution Time:0.14333009719849

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20' 
 Execution Time:0.14013695716858

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 1 
 Execution Time:0.16276407241821

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 3 
 Execution Time:0.14714908599854

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 4 
 Execution Time:0.18588209152222

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 5 
 Execution Time:0.16158890724182

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26' 
 Execution Time:0.18795704841614

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 1 
 Execution Time:0.15030908584595

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 3 
 Execution Time:0.17715883255005

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 4 
 Execution Time:0.27242803573608

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 5 
 Execution Time:0.15314102172852

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27' 
 Execution Time:0.14596796035767

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 1 
 Execution Time:0.14302015304565

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 3 
 Execution Time:0.14821195602417

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 4 
 Execution Time:0.14534211158752

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 5 
 Execution Time:0.15026807785034

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28' 
 Execution Time:0.15052700042725

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 1 
 Execution Time:0.1542980670929

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 3 
 Execution Time:0.14383602142334

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 4 
 Execution Time:0.1429009437561

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 5 
 Execution Time:0.1711368560791

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29' 
 Execution Time:0.22394204139709

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 1 
 Execution Time:0.14888215065002

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 3 
 Execution Time:0.14031982421875

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 4 
 Execution Time:0.1454029083252

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 5 
 Execution Time:0.14342093467712

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30' 
 Execution Time:0.16158008575439

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 1 
 Execution Time:0.15329813957214

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 3 
 Execution Time:0.16379618644714

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 4 
 Execution Time:0.15827798843384

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 5 
 Execution Time:0.14174699783325

SELECT *
FROM `proyectos`
WHERE `id` = '36' 
 Execution Time:0.10992503166199

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-08-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 08
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.20443105697632

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.14597296714783

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.1571671962738

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 1 
 Execution Time:0.14721202850342

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 3 
 Execution Time:0.15676498413086

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 4 
 Execution Time:0.14242506027222

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 5 
 Execution Time:0.14858293533325

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.19631314277649

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.17947316169739

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 1 
 Execution Time:0.14460802078247

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 3 
 Execution Time:0.14929914474487

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 4 
 Execution Time:0.15809988975525

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 5 
 Execution Time:0.19056296348572

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12' 
 Execution Time:0.14175391197205

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.16408801078796

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 1 
 Execution Time:0.14766597747803

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 3 
 Execution Time:0.14209818840027

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 4 
 Execution Time:0.14035296440125

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 5 
 Execution Time:0.14261507987976

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13' 
 Execution Time:0.14098405838013

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.1694540977478

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 1 
 Execution Time:0.14802193641663

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 3 
 Execution Time:0.14403295516968

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 4 
 Execution Time:0.14877796173096

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 5 
 Execution Time:0.14875888824463

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19' 
 Execution Time:0.13960790634155

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.14884305000305

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 1 
 Execution Time:0.14077186584473

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 3 
 Execution Time:0.14587593078613

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 4 
 Execution Time:0.14679384231567

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 5 
 Execution Time:0.14806079864502

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20' 
 Execution Time:0.14995884895325

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.15551805496216

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 1 
 Execution Time:0.15268301963806

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 3 
 Execution Time:0.14695000648499

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 4 
 Execution Time:0.14270710945129

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 5 
 Execution Time:0.16395401954651

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26' 
 Execution Time:0.14182806015015

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.1505811214447

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 1 
 Execution Time:0.14290618896484

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 3 
 Execution Time:0.14349412918091

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 4 
 Execution Time:0.15996599197388

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 5 
 Execution Time:0.14556908607483

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27' 
 Execution Time:0.15668106079102

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.15206503868103

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 1 
 Execution Time:0.15134692192078

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 3 
 Execution Time:0.14444398880005

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 4 
 Execution Time:0.1566801071167

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 5 
 Execution Time:0.14463591575623

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28' 
 Execution Time:0.14056301116943

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.15405511856079

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 1 
 Execution Time:0.14335894584656

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 3 
 Execution Time:0.14999914169312

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 4 
 Execution Time:0.15479207038879

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 5 
 Execution Time:0.15200304985046

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29' 
 Execution Time:0.14029407501221

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.1518771648407

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 1 
 Execution Time:0.14163994789124

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 3 
 Execution Time:0.14551401138306

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 4 
 Execution Time:0.14094209671021

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 5 
 Execution Time:0.15210700035095

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30' 
 Execution Time:0.14091897010803

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.15669989585876

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 1 
 Execution Time:0.1514949798584

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 3 
 Execution Time:0.14853191375732

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 4 
 Execution Time:0.14377307891846

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 5 
 Execution Time:0.14864587783813

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-09-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 09
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.18633794784546

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01' 
 Execution Time:0.14291501045227

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.15218782424927

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 1 
 Execution Time:0.14347505569458

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 3 
 Execution Time:0.15019202232361

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 4 
 Execution Time:0.14095377922058

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 5 
 Execution Time:0.14500999450684

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02' 
 Execution Time:0.14215087890625

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.15593910217285

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 1 
 Execution Time:0.1559681892395

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 3 
 Execution Time:0.14794111251831

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 4 
 Execution Time:0.14540100097656

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 5 
 Execution Time:0.20163702964783

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03' 
 Execution Time:0.14002084732056

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.17603802680969

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 1 
 Execution Time:0.14905095100403

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 3 
 Execution Time:0.14134812355042

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 4 
 Execution Time:0.15453696250916

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 5 
 Execution Time:0.15751504898071

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09' 
 Execution Time:0.15784502029419

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.16197800636292

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 1 
 Execution Time:0.14189720153809

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 3 
 Execution Time:0.14192795753479

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 4 
 Execution Time:0.15840816497803

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 5 
 Execution Time:0.17757105827332

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10' 
 Execution Time:0.15008616447449

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.17651987075806

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 1 
 Execution Time:0.14611601829529

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 3 
 Execution Time:0.15502309799194

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 4 
 Execution Time:0.17508101463318

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 5 
 Execution Time:0.14289712905884

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16' 
 Execution Time:0.14806795120239

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.14954304695129

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 1 
 Execution Time:0.15819406509399

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 3 
 Execution Time:0.2051420211792

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 4 
 Execution Time:0.14847803115845

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 5 
 Execution Time:0.17208099365234

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17' 
 Execution Time:0.14629197120667

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.16946005821228

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 1 
 Execution Time:0.14186787605286

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 3 
 Execution Time:0.14172601699829

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 4 
 Execution Time:0.14054799079895

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 5 
 Execution Time:0.14063096046448

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23' 
 Execution Time:0.16226696968079

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.18763613700867

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 1 
 Execution Time:0.1461329460144

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 3 
 Execution Time:0.16333103179932

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 4 
 Execution Time:0.14355397224426

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 5 
 Execution Time:0.19346809387207

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24' 
 Execution Time:0.2416410446167

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.28288102149963

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 1 
 Execution Time:0.23228597640991

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 3 
 Execution Time:0.16434597969055

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 4 
 Execution Time:0.14981913566589

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 5 
 Execution Time:0.14500308036804

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30' 
 Execution Time:0.14848208427429

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.14983510971069

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 1 
 Execution Time:0.15254378318787

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 3 
 Execution Time:0.14142799377441

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 4 
 Execution Time:0.14607882499695

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 5 
 Execution Time:0.14385199546814

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-01-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 01
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.17833614349365

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-02-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 02
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.1856951713562

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-03-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 03
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.17269802093506

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-04-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 04
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.171795129776

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-05-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 05
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.17661881446838

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-06-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 06
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.1675431728363

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-07-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 07
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.18335604667664

SELECT DISTINCT(semana)
FROM `ordenes_semanal`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%Y') = '2024'
AND DATE_FORMAT(fecha,'%m') = '08'
ORDER BY `semana` ASC 
 Execution Time:0.17345404624939

SELECT `la`.`nombre`, `la`.`fecha_ini`, `os`.`id`, `os`.`seguimiento`, `os`.`semana`, DATE_FORMAT(os.fecha, '%Y') as anio, DATE_FORMAT(os.fecha, '%d') as dia, `os`.`fecha_final`, `os`.`fecha`, `os`.`ot`, `os`.`turno`, `os`.`observaciones`, `c`.`foto`, `c`.`empresa`, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=1 and os2.fecha=os.fecha) as cont_can, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=2 and os2.fecha=os.fecha) as cont_pro, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=3 and os2.fecha=os.fecha) as cont_ok, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=4 and os2.fecha=os.fecha) as cont_re, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=5 and os2.fecha=os.fecha) as cont_no_rea
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id_proyecto`=`os`.`id_proyecto` and `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `clientes` `c` ON `c`.`id`=`la`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%Y') = 2024
AND `os`.`semana` = 32
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC 
 Execution Time:0.19143605232239

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.13867688179016

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.15131902694702

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-07' 
 Execution Time:0.14808702468872

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-08' 
 Execution Time:0.14569401741028

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-09' 
 Execution Time:0.16298198699951

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-10' 
 Execution Time:0.15461492538452

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-11' 
 Execution Time:0.14071297645569

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '1'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.11394190788269

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:,', `mes` = '8', `anio` = '2024', `tipo` = '1', `semana` = '0'
WHERE `id` = '276' 
 Execution Time:0.087225914001465

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '2'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.084999084472656

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:,', `mes` = '8', `anio` = '2024', `tipo` = '2', `semana` = '0'
WHERE `id` = '278' 
 Execution Time:0.085690021514893

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '4'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.086580038070679

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAl0AAAEFCAYAAADZt1IsAAAAAXNSR0IArs4c6QAAHelJREFUeF7t3VuoXXV+B/D/ydjkHGbSeJoWNKE0XgqlBU1ovdWH4uhTUR/EmKZ1KmIImqAP5nI8aVCU1GNuUpRECRFxagkx4oNGH1od2wdrEhlQmSmFekkZosLMcNDMTKLj5JT/nq7t2su19z4r7rPW3nt98iLZe63/5fNbxC//9V9rj8zMzMwEfwgQIECAAAECBOZUYETomlNfjRMgQIAAAQIEGgJClwuBAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFCCgNBVArIuCBAgQIAAAQJCl2uAAAECBAgQIFBA4MyZM2HevHkFzvjtoWcVun4+/Vn4x/d/Hc6cmSncoRMIECBAgAABAoMqMG/eSPiHi84Ji8cXFZ7CWYWu6U9Pht/79zOFO3MCAQIECBAgQGCQBc4ZCeGnfzUvnLtoYeFpCF2FyZxAgAABAgQI1FXgWyMh/Ezoqmv5zZsAAQIECBAoS0DoKktaPwQIECBAgECtBdxerHX5TZ4AAQIECBAoS0DoKktaPwQIECBAgECtBYSuWpff5AkQIECAAIGyBISusqT1Q4AAAQIECNRaQOiqdflNngABAgQIEChLQOgqS1o/BAgQIECAQK0FhK5al9/kCRAgQIAAgbIEhK6ypPVDgAABAgQI1FpA6Kp1+U2eAAECBAgQKEtA6CpLWj8ECBAgQIBArQWErlqX3+QJECBAgACBsgSErrKk9UOAAAECBAjUWkDoqnX5TZ7AYAs89CejYeKPFzQmsf1/Pg/3//fp5oRe/ctvh2v/4JzcCZ44fSb8/Q9PhR/87Muvff/d3z8nfP/Px8LS0Xkt37320y/Ddf/5y8EGM3oCBCoVELoq5dc5AQJnI/C9P/ydsOeSsbAw/gsWQvjiTLHQFc85+eVMWP/uqfDPP/l1cwj7l4+FO/5oftsh5Z1zNuN3DgEC9RQQuupZd7MmMLACecGoW+h66n+/CGvePhWyq1jp1at0u9n2fvzdheFPF/525avTKtnAoho4AQKlCAhdpTDrhACBXgkk4SgGpvNH5zXC0GxDVxxD+rZjErrSYSyvrWxYS0Jcr+akHQIE6iEgdNWjzmZJYCgFkhWobxq6kr1h8+e1X8lKr4T918kz4c9+cHIoTU2KAIG5ExC65s5WywQIzLFA0dCVDlfp/VnpQNVuw/xsgtkcT1fzBAgMuIDQNeAFNHwCdRaYbejKGmU3xOfdcsyeI3TV+UozdwK9ERC6euOoFQIEKhA429CV3ZMldFVQPF0SqKGA0FXDopsygWERmG3oiiHrP37+ZfM1E9k9YOnbi+2eTrSna1iuGvMgUJ2A0FWdvZ4JEPiGAkVCV3xlRLtwlX7vV96m/DjM9GsjPL34DQvndAI1FRC6alp40yYwDAJFQ1c2PJ3Ne7o8uTgMV445EKhGQOiqxl2vBAj0QOBsQld6Q3x2VavTTwfF4Xoxag+KpgkCNRYQumpcfFMnMOgCZxO64pzT4SobpLI/MZQYuaU46FeL8ROoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXkDoqr4GRkCAAAECBAjUQEDoqkGRTZEAAQIECBCoXqCS0PXYJ/Orn7kRECBAgAABAgRKFJgXQrj7vC/CuYsWFu51ZGZmZqboWdOfngw/PD0/nPOtbxU91fEECBAgQIAAgYEV+PI3vwl/MVpy6Ipa42eR8gZW2cAJECBAgACB2gtMf/aLMDIzU+5Kl9BV++sOAAECBAgQqJ2A0FW7kpswAQIECBAgUIWA0FWFuj4JECBAgACB2gkIXbUruQkTIECAAAECVQgIXVWo65MAAQIECBConYDQVbuSmzABAgQIECBQhYDQVYW6PgkQIECAAIHaCQhdtSu5CRMgQIAAAQJVCAhdVajrkwABAgQIEKidgNBVu5KbMAECBAgQIFCFgNBVhbo+CRAgQIAAgdoJCF21K7kJEyBAgAABAlUICF1VqOuTAAECBAgQqJ2A0FW7kpswAQIECBAgUIWA0FWFuj4JECBAgACB2gkIXbUruQkTGByBQ8+/ECa2bG0O+MCzz4QrLr+s6wR27Ho0PLlvf+O4pUuXhKf37wsXX3Rhy3nT09PhjrV3hbffebfx+Y03XB+mtj0UxsZGm8el+8/7PmljYtOGWY2r68AdQIDAUAsIXUNdXpMjMLgCMfAcOPhceGrfE2F8fDwcPfZWWH3rbaFb8IqB66OPP2kGqNjOY3v2tgSvJCytXnVLWHnzTeHUqdNhcuv9DawkeMX+tu/c3ew/thv/bN54bxM1fnbBsmWNNvwhQIBANwGhq5uQ7wkQKF2g3QpSNlBlB/be+x+ETROTYef2qebKVhKolpx/XjMwxSD2xptHWla24rm3r1kbdm2faqxaZQNV/P7hR3aE3TummiEwHcpKR9IhAQIDJyB0DVzJDJjA8AtkV5mSGcfPN05M5t4ujMfkhank82TVbHR0rLGqdfVVV7asUKXD2d3r1zWOWb1qZfO2YTp0xTbjrUm3FYf/WjRDAr0UELp6qaktAgR6ItBuRSu7GpXuLG9FKy+sxc/SK1rpNtL9Pr5nb8utw3ToevW118OHx4+33GrsycQ1QoDAUAsIXUNdXpMjMJgCefun4kw6bVxPQld2BSuel77tGP+evQWZKKVXyg6//ErLnrLkduOKFctbbjMOprBREyBQhYDQVYW6PgkQ6CjQLXQlG+DzVrraha5kdWvx4sUdQ1d68376Kcg7164JyW3H2Md1117T8vRjtw3+Sk6AAAGhyzVAgEDfCXQLXXl7qXq90pV+dUR2JeyBrZPhwW1TzX1heRv4+w7VgAgQqFxA6Kq8BAZAgEBWoB/2dGVDV7y1uWHzZNhy3+bGcNNPMsa/e32E65gAgW4CQlc3Id8TIFC6QPYdXckAuj292C6spduLbcUnD7O3KDttxM+GqjiOAwcPtbxyQugq/TLRIYGBExC6Bq5kBkxg+AXaPaXY7T1deaEsL0zltdPpycjsKyyy7+yy0jX816QZEuiFgNDVC0VtECDQc4EYjI4cPdbyRvrsO7qyb5vPe7N83qpZErDuWb+u5Y306ReoJhPKe2Iyu3/Mnq6el1+DBIZSQOgayrKaFIHhEOj2G4p5P/GTBKIXXzrcQFh+6SXN4JZWSYLXiRMfNT6OTyemf+InObbbpv74243tft9xOKpgFgQI9EpA6OqVpHYIECBAgAABAh0EhC6XBwECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBAGhqwRkXRAgQIAAAQIEhC7XAAECBAgQIECgBIFKQtfpz78IowvmlzA9XRAgQIAAAQIE+kMg5p+xBfPDuYsWFh7QyMzMzEzRs6Y/PRmErqJqjidAoFcCIyMjIf7Tlfw33W72u7k8Jum303gc81V1+sUpjiP50+5/gWUeE8eS9Nfuuu7nYzpZph3jcf3g/U1rW0noinjjZ5HyevWPrnYIECBAgAABAmULVHJ7Uegqu8z6I0CAAAECBKoWELqqroD+CRAgQIAAgVoICF21KLNJEiBAgAABAlULCF1VV0D/BAgQIECAQC0EhK5alNkkCRAgQIAAgaoFhK6qK6B/AgQIECBAoBYCQlctymySBAgQIECAQNUCQlfVFdA/AQIECBAgUAsBoasWZTZJAgQIECBAoGoBoavqCuifAAECBAgQqIWA0FWLMpskAQIECBAgULWA0FV1BfRPgAABAgQI1EJA6KpFmU2SAAECBAgQqFpA6Kq6AvonQIAAAQIEaiEgdNWizCZJgAABAgQIVC0gdFVdAf0TINBW4NDzL4SJLVub3x949plwxeWXdRXbsevR8OS+/Y3jli5dEp7evy9cfNGFLedNT0+HO9beFd5+593G5zfecH2Y2vZQGBsbbR6X7j/v+6SNiU0bZjWurgN3AAECQy0gdA11eU2OwOAKxMBz4OBz4al9T4Tx8fFw9NhbYfWtt4VuwSsGro8+/qQZoGI7j+3Z2xK8krC0etUtYeXNN4VTp06Hya33N7CS4BX7275zd7P/2G78s3njvU3U+NkFy5Y12vCHAAEC3QSErm5CvidAoHSBditI2UCVHdh7738QNk1Mhp3bp5orW0mgWnL+ec3AFIPYG28eaVnZiufevmZt2LV9qrFqlQ1U8fuHH9kRdu+YaobAdCgrHUmHBAgMnIDQNXAlM2ACwy+QXWVKZhw/3zgxmXu7MB6TF6aSz5NVs9HRscaq1tVXXdmyQpUOZ3evX9c4ZvWqlc3bhunQFduMtybdVhz+a9EMCfRSQOjqpaa2CBDoiUC7Fa3salS6s7wVrbywFj9Lr2il20j3+/ievS23DtOh69XXXg8fHj/ecquxJxPXCAECQy0gdA11eU2OwGAK5O2fijPptHE9CV3ZFax4Xvq2Y/x79hZkopReKTv88iste8qS240rVixvuc04mMJGTYBAFQJCVxXq+iRAoKNAt9CVbIDPW+lqF7qS1a3Fixd3DF3pzfvppyDvXLsmJLcdYx/XXXtNy9OP3Tb4KzkBAgSELtcAAQJ9J9AtdOXtper1Slf61RHZlbAHtk6GB7dNNfeF5W3g7ztUAyJAoHIBoavyEhgAAQJZgX7Y05UNXfHW5obNk2HLfZsbw00/yRj/7vURrmMCBLoJCF3dhHxPgEDpAtl3dCUD6Pb0Yruwlm4vthWfPMzeouy0ET8bquI4Dhw81PLKCaGr9MtEhwQGTkDoGriSGTCB4Rdo95Rit/d05YWyvDCV106nJyOzr7DIvrPLStfwX5NmSKAXAkJXLxS1QYBAzwViMDpy9FjLG+mz7+jKvm0+783yeatmScC6Z/26ljfSp1+gmkwo74nJ7P4xe7p6Xn4NEhhKAaFrKMtqUgSGQ6Dbbyjm/cRPEohefOlwA2H5pZc0g1taJQleJ0581Pg4Pp2Y/omf5Nhum/rjbze2+33H4aiCWRAg0CsBoatXktohQIAAAQIECHQQELpcHgQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgSErhKQdUGAAAECBAgQELpcAwQIECBAgACBEgQqCV2nP/8ijC6YX8L0dEGAAAECBAgQ6A+BmH/GFswP5y5aWHhAIzMzMzNFz5r+9GQQuoqqOZ4AgV4JjIyMhPhPV/LfdLvZ7+bymKTfTuNxzFfV6RenOI7kT7v/BZZ5TBxL0l+767qfj+lkmXaMx/WD9zetbSWhK+KNn0XK69U/utohQIAAAQIECJQtUMntRaGr7DLrjwABAgQIEKhaQOiqugL6J0CAAAECBGohIHTVoswmSYAAAQIECFQtIHRVXQH9EyBAgAABArUQELpqUWaTJECAAAECBKoWELqqroD+CRAgQIAAgVoICF21KLNJEiBAgAABAlULCF1VV0D/BAgQIECAQC0EhK5alNkkCRAgQIAAgaoFhK6qK6B/AgQIECBAoBYCQlctymySBAgQIECAQNUCQlfVFdA/AQIECBAgUAsBoasWZTZJAgQIECBAoGoBoavqCuifAAECBAgQqIWA0FWLMpskAQIECBAgULWA0FV1BfRPgEBbgUPPvxAmtmxtfn/g2WfCFZdf1lVsx65Hw5P79jeOW7p0SXh6/75w8UUXtpw3PT0d7lh7V3j7nXcbn994w/VhattDYWxstHlcuv+875M2JjZtmNW4ug7cAQQIDLWA0DXU5TU5AoMrEAPPgYPPhaf2PRHGx8fD0WNvhdW33ha6Ba8YuD76+JNmgIrtPLZnb0vwSsLS6lW3hJU33xROnTodJrfe38BKglfsb/vO3c3+Y7vxz+aN9zZR42cXLFvWaMMfAgQIdBMQuroJ+Z4AgdIF2q0gZQNVdmDvvf9B2DQxGXZun2qubCWBasn55zUDUwxib7x5pGVlK557+5q1Ydf2qcaqVTZQxe8ffmRH2L1jqhkC06GsdCQdEiAwcAJC18CVzIAJDL9AdpUpmXH8fOPEZO7twnhMXphKPk9WzUZHxxqrWldfdWXLClU6nN29fl3jmNWrVjZvG6ZDV2wz3pp0W3H4r0UzJNBLAaGrl5raIkCgJwLtVrSyq1HpzvJWtPLCWvwsvaKVbiPd7+N79rbcOkyHrldfez18ePx4y63GnkxcIwQIDLWA0DXU5TU5AoMpkLd/Ks6k08b1JHRlV7DieenbjvHv2VuQiVJ6pezwy6+07ClLbjeuWLG85TbjYAobNQECVQgIXVWo65MAgY4C3UJXsgE+b6WrXehKVrcWL17cMXSlN++nn4K8c+2akNx2jH1cd+01LU8/dtvgr+QECBAQulwDBAj0nUC30JW3l6rXK13pV0dkV8Ie2DoZHtw21dwXlreBv+9QDYgAgcoFhK7KS2AABAhkBfphT1c2dMVbmxs2T4Yt921uDDf9JGP8u9dHuI4JEOgmIHR1E/I9AQKlC2Tf0ZUMoNvTi+3CWrq92FZ88jB7i7LTRvxsqIrjOHDwUMsrJ4Su0i8THRIYOAGha+BKZsAEhl+g3VOK3d7TlRfK8sJUXjudnozMvsIi+84uK13Df02aIYFeCAhdvVDUBgECPReIwejI0WMtb6TPvqMr+7b5vDfL562aJQHrnvXrWt5In36BajKhvCcms/vH7Onqefk1SGAoBYSuoSyrSREYDoFuv6GY9xM/SSB68aXDDYTll17SDG5plSR4nTjxUePj+HRi+id+kmO7beqPv93Y7vcdh6MKZkGAQK8EhK5eSWqHAAECBAgQINBBQOhyeRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShAQukpA1gUBAgQIECBAQOhyDRAgQIAAAQIEShCoJHSd/vyLMLpgfgnT0wUBAgQIECBAoD8EYv4ZWzA/nLtoYeEBjczMzMwUPWv605NB6Cqq5ngCBHolMDIyEuI/Xcl/0+1mv5vLY5J+O43HMV9Vp1+c4jiSP+3+F1jmMXEsSX/trut+PqaTZdoxHtcP3t+0tpWErog3fhYpr1f/6GqHAAECBAgQIFC2QCW3F4WussusPwIECBAgQKBqAaGr6gronwABAgQIEKiFgNBVizKbJAECBAgQIFC1gNBVdQX0T4AAAQIECNRCQOiqRZlNkgABAgQIEKhaQOiqugL6J0CAAAECBGohIHTVoswmSYAAAQIECFQtIHRVXQH9EyBAgAABArUQELpqUWaTJECAAAECBKoWELqqroD+CRAgQIAAgVoICF21KLNJEiBAgAABAlULCF1VV0D/BAgQIECAQC0EhK5alNkkCRAgQIAAgaoFhK6qK6B/AgQIECBAoBYCQlctymySBAgQIECAQNUCQlfVFdA/AQINgaPH3gqrb72tqbH94W1h5c03tegcev6FMLFl69fE8o5NHzQ9PR3uWHtXePuddxsf37l2Tdi88d6WdrL9xy8PPPtMuOLyy9qO4cYbrg9T2x4KY2OjzWOSviY2bfjauUpNgEC9BYSuetff7An0hUAMU4/t2Rue3r8vXHzRhSEJLqtX3dISvOJxb7x55GtBp9MkkrauvOLyRtA6dep0mNx6f+OUdGB65p//Jdx4/V+H8fHxxndJwEsHrxjMtu/cHZ7a90TjuB27Hm0cmw5w8bMLli37WmDsC2iDIECgUgGhq1J+nRMg0G5lKBtwkiBUNHTF8HTg4HPNoBTbee/9D8Lta9aGXdun2q5GJeFsyfnnNUNVNlDFdh5+ZEfYvWOqEcLyxqzCBAgQSASELtcCAQKVCsSgsnFisrnK1fzH6f9vCaZv0+WtLHUbfDzno48/aVnVmu0twHR/SQhbvWplM6ilQ1ccR7yF6bZit4r4nkB9BYSu+tbezAn0hUC30JW+xXg2oavdStemicmwc/tU43Zm3p8kZF191ZXNW4WdVrpefe318OHx41/bK9YXyAZBgEBfCAhdfVEGgyBQX4F2t/qSz+9Zv64RepIQ9OJLh5tYS5cu+doKWVay3Z6u9G3DPP28FbJsgEtC2IoVy1tuM9a3mmZOgEAnAaHL9UGAQOUCMby89PIrzQAVA9bD23eEH/3ox+Fv/2ZV203p8bwn9+3PfcowPanZPL2YDXXtnohM+oztx6cg716/rrExP66IXXftNS1PSeY9/Vg5tgEQIFCZgNBVGb2OCRBIC6TDTFzB2vv4P4Wnnv5+I8xkXx2RPS+7Zyv9fd7ty+zTknmVSJ5e7PY6iuSJyge2ToYHt001xxtX6rrdwnQFECBQLwGhq171NlsCAyMw283ueXu2kkm2e/VE3pOJ7YJX9snH7Arahs2TYct9mxsfp59kjH/3+oiBudwMlEApAkJXKcw6IUCgqMBsX7/QKXR1ejXEbN751W6TfzKXdKiKxx44eKjlKUmhq2jVHU9guAWEruGur9kRGEiB2a5EdTtuLle6sqEw+84uK10DeekZNIE5FRC65pRX4wQIzEYg+zb4uEJ05OixlheaxgD14uFXwm3f+7tmk9kN+PGL7H6tvP1b2bfNx8D0r//2alh359pm28nPAuVths+79Zl9xYQ9XbOpvGMI1EtA6KpXvc2WQF8KpDfRxwHm/TZi9gnEeFzebx/mhazs7youv/SSlkBX9HUU7d4Xlh7jbF5n0ZfFMCgCBOZMQOiaM1oNEyBAgAABAgS+EhC6XA0ECBAgQIAAgRIEhK4SkHVBgAABAgQIEBC6XAMECBAgQIAAgRIEhK4SkHVBgAABAgQIEBC6XAMECBAgQIAAgRIEhK4SkHVBgAABAgQIEBC6XAMECBAgQIAAgRIEhK4SkHVBgAABAgQIEBC6XAMECBAgQIAAgRIEhK4SkHVBgAABAgQIEPjwJx+H8d/9Tjh30cLCGCMzMzMzRc+a/vRk45Txs+iwaF+OJ0CAAAECBAj0i0Aloev051+E0QXz+8XAOAgQIECAAAECcy7w2clfhkULv13eStfPpz8Lv/jVr8KZM4UXyeYcQwcECBAgQIAAgbkSmDcyEr7z7bGweHxR4S7O6vZi4V6cQIAAAQIECBAYEoEzZ86EefPmFZ6N0FWYzAkECBAgQIAAgeICQldxM2cQIECAAAECBAoLCF2FyZxAgAABAgQIECguIHQVN3MGAQIECBAgQKCwgNBVmMwJBAgQIECAAIHiAkJXcTNnECBAgAABAgQKCwhdhcmcQIAAAQIECBAoLiB0FTdzBgECBAgQIECgsIDQVZjMCQQIECBAgACB4gJCV3EzZxAgQIAAAQIECgsIXYXJnECAAAECBAgQKC4gdBU3cwYBAgQIECBAoLCA0FWYzAkECBAgQIAAgeICQldxM2cQIECAAAECBAoLCF2FyZxAgAABAgQIECguIHQVN3MGAQIECBAgQKCwgNBVmMwJBAgQIECAAIHiAv8HWNiqS7hnJasAAAAASUVORK5CYII=', `mes` = '8', `anio` = '2024', `tipo` = '4', `semana` = '0'
WHERE `id` = '277' 
 Execution Time:0.08243989944458

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '6'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.23437094688416

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABCMAAABoCAYAAADLnzp0AAAAAXNSR0IArs4c6QAAIABJREFUeF7tnXuwZUdVh/s+MnMnYYyTCUISSCIhFEWKGEBIAvIQ+ItXWSpiQKCoAAoUlggkJIWoVCSEZxUIYghSCMVDKKSSEEsLMFjIswIxlIqESEicCcIM42QgM5nHPdY6w7qzbt/Vvffuc84++5z9nb/uPWfv7t7f6l69+tePvTAYDAah4Wf3nrvDX9x2KKyuNr61YU5cDgEIQAACEIAABCAAAQhAAAIQgEAXCSwvL4UrzjkhrGze1Lh4CyVixJ69+8JJN642zowbIAABCEAAAhCAAAQgAAEIQAACEJgPAo/beiR87nH3QYyYD3PyFBCAAAQgAAEIQAACEIAABCAAge4TQIzovo0oIQQgAAEIQAACEIAABCAAAQhAYK4IIEbMlTl5GAhAAAIQgAAEIAABCEAAAhCAQPcJIEZ030aUEAIQgAAEIAABCEAAAhCAAAQgMFcEECPmypw8DAQgAAEIQAACEIAABCAAAQhAoPsEECO6byNKCAEIQAACEIAABCAAAQhAAAIQmCsCiBFzZU4eBgIQgAAEIAABCEAAAhCAAAQg0H0CiBHdtxElhAAEIAABCEAAAhCAAAQgAAEIzBUBxIi5MicPAwEIQAACEIAABCAAAQhAAAIQ6D4BxIju24gSQgACEIAABCAAAQhAAAIQgAAE5ooAYsRcmZOHgQAEIAABCEAAAhCAAAQgAAEIdJ8AYkT3bUQJIQABCEAAAhCAAAQgAAEIQAACc0UAMWKuzMnDQAACEIAABCAAAQhAAAIQgAAEuk8AMaL7NqKEEIAABCAAAQhAAAIQgAAEIACBuSKAGDFX5uRhukzg35+8NTxs62LYcWA1vOCm/eELuw63VtzPPfaE8JT7Lg/z+8APDoYX37y/tbzJCALzSuD5DzwuvOfcLWHr8sJU2vW8cuW5IFBF4MknL4e/fdSWcNrKYth3eBBeccv+8OE7D1Xd5v7+xoeuhEvP3hw2LR7rH685b0u4+IxNw+s//+PD4alf/llR2twEgXkjYNvef+xbDed8Yd+8PWKnnyfl+6Y5xhgVWC/FCDWYwDu4GsJVt94b3vCdA6Oy5P4OEbCDBFusUYOW0ke05ZlGnUOMKLVc8/tSdQ9/05zlpO6wfUCcR5PgCjFiUhYqS9dre574Wve6slJw16gEYvt4Av44xQjbP2r7R4wIwYo0uf6r7nWj1gvuTxOwNmjSh5UwtXlNK6YuKXdX7hnVVp7vk2fTiZFpjDFGZds7McILQrqoekuw/MAtCyOp/aNWjlm93wYR8TO04Ti1jt25f7BOMZ6maokY0V5tRoxoj3VpTogRpeS6fZ/X9rzA3OsjWDHWHdvG9vGC63GKEayM8G0fiwxylRcv2/gC0X067WjUAW6TUrMyogmtjdeOaitWRqxnujAYDAZNTbJn775w0o2rTW8b2/VeEDKNZfOpBxpnBzs2aDOUUByM2gBT2L773C3hzbceKF7OWYViVCdTlX7p74gRpeSa38dseXNmbd+hYsSoMwjYum3L5fOz9th9cBC2b1pwl/Cr/fUaSRUxoju29MTCeBA86ViJlRHHVkZIzbh3deBuR7N20PY0ql/tTk2cnZJ0NfacHYLtlXRUW03a97VH4lhOvVsZYWen7z0SwoNOWOzUVo15rGRtVmzbyKchMo3qZCbFCjFiUmQ3pssAtT3WpTkhRpSS6/Z9nhgRCw11run2U8536ax9vrn3SDj7hMXKQfAkVjwiRqzfppESGjTmsYIFYkT7bbSrsWf7JLqf46i2msdxYq/ECNvJydLNr/zkcPaAIm/f4g/uGYTHnrQ0rO3xTEq8VE2uqbOkTa+7/Z7VtfLY5hR3tN6sAbM6R4nFywqruFTtHY4Hlp/aeSi87MyjB13Jxy4BTi39lmteecv+5GFbo5ZB65jNP64zsRhx4UnLw8M05RMHDjYI+/JPjoQzjl8YHhJm63Jc16ch/HS1y6kjRsSdyTtvOxheddamYdAtH49n03qivsqm5S277aPt6ooRTZnn/ENs8xt3HQ7PvP9x62bum+bn2dhLYxKDtS62v9RA1vpp9W/C5NafrYZHnrixP/d8ecww5ydTfbkys/1S3bihi7wnUSbLVThpXxX3U019qOV83Q8PhSedvDz0t5KHfOLDKhEj1sdTEgv86i8uDWMfLxaQfsSb4EttW7Rt0trGpp0adNGPbWx53gA3FYt430uK9kDYXEySi3H63P/U9YfjtpUc3rtj/yB7oG/Xx429EiPiTu6Luw8nT0L3nF1c0TSgsA7Tq4w22E8NWMUBV4kRkrYeUOLlM+lDa+o2tGlfV7fR5c6W0A4xt/9fn1O5l4gR4yiDBGm6hNKyT3X2nn1soJcqkzB503fvXXN4cTp9GfBU1e9cnVFGtuNIpWeDslHqifqfyx+yee2NKnGefZvJqiNGjMLca4epfkLrxBO3L7titKRV5Y/Uxs99wHHJNCSdKnG2qm53/Xfb9oTZKSuLQ9HV+ia1vfjHuw6sbnjLUK4/t+nk/GROjNC2JmKUBv9VcUPXuY+zfGofr12kBqqp/G3s5Yk+2iYQI3yCNg7++I5D4fHbl4YTE8rVDmBte7P9SS6W1hgltaLVfq+2T9lRytK3fsxabZwD3KqYJCVG5PrMPvQ/df3gOG3lxZS2n6oaw3Rl3NgrMSLu5OQ1UKmg1A4sU4GgBnbeScxSKWO190N3HnTFD6mY5524GJ71tXtCbvmNVybJx34/78FmncaeCiZtRxWvktFXE6nNtDHHApBXF1Idr23kVaffesJBqgxqY28lhC2vdUi2LqYCNO8U8bhz13RSLLt4GGydOjPOa0rECOVmO3O102lbFtb8RpN6Ym2ROv162tuaxsm9SVop4VDr9a0/O1LEXNumrQOpwae15aj+SJ7dy1PeEmW/n3fBMBYjrCggtrETEPL/mccv1nrlcTyRIa9Gtt9VDYK8vrtu3NCnV0p67SA18In7efV38fdenJZb5eL54r72a/HBntpetL6L35FXospH3kr3W6ceNxT/6rYHTyS098bxhuYnqzPsvX3txyYpRtSNSbw4JRUfznv/UzcGaVOMmJVx4+NPHIR/uvD4sLL56OuUm3xm6gDL1B4dbylebglSvNz9o/9zqNbye2mw7/zewfCqB28aKsv6icWDlBiRK9Oo+4+aGH2Wrs0tF8vNQlqF3Q5K4uXs3jkMKVt4dh21DKllpJ7oljozwqtXdnY1Jaik6kFXVNZp1tOSbRqyzE7E0XHVk6ptOjKY0k+dVQLT5DmJvKvEiFNXFrIrDDzRoso/2L4itzXKe14vvzoDKk2rL2fGxGKEnQCwWzOV3QsfuCkpRqREbS9Iz/k966f1utykQx3/MYk20YU0U/XU81E5hl7fmNoKIM/tXc82jfXbNCRW3XlgMBQfdKuGsHvKfZfXVkrIAeEpMcJbIZFakRmvwtR2k/NjfezHJiVG2L6laiLNW5UXi3d96X/q+tC2xAgpj66mj+OTro0beyNG5JZ2aQXyFL4mAWYcHHpBhexTVGduK6635SO11Kbrlapug2zruliUyG2JiW2S28ozaTFCyhLP5lnbV4kR3gyDpqmD0SoxYpRlsW3Zt2v51BlMpALpEjGiqp7I7wRx62tJVeBatdy0hHlOuB5HfrnBU1+CwViMkFUF9tBqPWMjN7ipWtbaZOY8NWOLGLHRa1dtd9U7vBUQdYS5XBtAjPB70XhlhPVh0obkI+JDfG5VnW2fcq+9ztpf4hx7/o63uiWeyKvy6V2LE8ZdnkkMcFMTJN4g105iIUbkrTsJW3lnRiBGZOwwjVd71u3kRp3tqiNGfGHX4SGdOOBpsu8HMaK5G49nRezy3dwSzNzA0uv8SldGlJRhHGKEV95UuvN4gm/zmlR9xyTFiJJ6ghix0WZVgWvdWdEm/sGeEVBn8OTVtFx+iBHr+1VPNFCmucGNN4Pu7V2vqiPWX1Ydvqgro+LYoE+Hy9Y5p0v4eOcV1FkJhhhR3XfFV8RihExixBN7tm7HftX6vNz2C9lOZvspsad3wC+ietqGTVZ5eyKppGwPsMyt1kSMaN6W7B2TsBViREObTEOMqNpPFgcf8SF9GrjEnaUX0NjBgndmxOvOXhm+WUFFibrOWzCnzobgzIhjlVBs9LwHHBdecvMxxrHwE682iANFe4aHd690yE334FctdatbhnGsjEgdqFpnxi+1z1ks8KXH3yf89e33Drcb9PkzbjHCdvwl9UTur3NAWJ8GPlViROr8BWWpZ/w08Q+5065L8ovtlTobos9nRsjKiJTwL37KG9xUbXOr4ydz/bX6xtS2gdx2gnn2q7nn9oSd+ADQqvOcECOa1x5PjIjjYC+eyJ29kxPpvFVJqbOPUm+J6lM/JoNaiUdTTFPfe2eIjSpG2LOtUqu6+3xmxKRtlYovZmXc2IttGlWduxeoV51KLg236ds0bGONu4XcGzdSBxnGabBff+OrPXOcc1t3qk6vt+na5YJeZ5p7tWfTMoxDjPBCkjrpxrN2cTp97mgsizrLvK3gWbU/Mx40xdy9uuoFZLm6VnXgWPMwttt3VIkRdpbOe5IS/1C1smgUX6BlrNruMe8HHHvbNGJhIPWWIW9yIWf7uitRvDQkL7vkvcond7s1jV663AA1Jd7k3uyk96Qmh+J2wDYN34aeGJE6ONS2s7pvjPH6HTt48n6nHztmK+/so3gcULVFvWqlUdVEWip2TPk9e17V6J5jdlKYtK1SYkRVPNqVcePcixF1OrnUNXFgl3oVmNdZep1hqlLES69jZx8P8rxKPe9BZl2Xk9uS4zHylobmzurQPcdanjpp5sQISadpGV5w09FVH6XbNOw71iX/eOBatfw4Du69ul7XXvN4XZXzjw/nqiNGjFJPLGOvrvVpJklZ1BEjSpjn/EOVGFGSn/oCa+Pcwb3zvmopJUZ4b8MQZlUHJqp/tPvXq1ZG1NkWavsNb7DQt7c3VK1ejdtGfCC4Xdaf6o9YGdG8t/XECNtuYrHA86vemV2Shhx86YkNTeuCPlWf+7GqGCx+89o7bzsYXnXWprB1eaFy21MTMULK0ef+J9fC4nFbyseX2iq38jIVs3dp3Dj3YkRz95u/o24QO+58SW86BOosuZ9OycgVAhCAAAQgAAEIzA+BlHA4P0/Ik0AAAjEBxIhEnRB19un3Xw6PuvGna1d4+6z07Aeq1nwSQIyYT7vyVBCAAAQgAAEIdIdAndVj3SktJYEABMZFADEiI0Z4r+CUy9kbP67q1/10ECO6byNKCAEIQAACEIDAbBLwtg92aQn5bFKl1BCYHQKIEQ3FiL7t5ZydqjyZkiJGTIYrqUIAAhCAAAQgAIHUm+ogAwEI9IMAYkQ/7MxTQgACEIAABCAAAQhAAAIQgAAEOkMAMaIzpqAgEIAABCAAAQhAAAIQgAAEIACBfhBAjOiHnXlKCEAAAhCAAAQgAAEIQAACEIBAZwggRnTGFBQEAhCAAAQgAAEIQAACEIAABCDQDwKIEf2wM08JAQhAAAIQgAAEIAABCEAAAhDoDAHEiM6YgoJAAAIQgAAEIAABCEAAAhCAAAT6QQAxoh925ikhAAEIQAACEIAABCAAAQhAAAKdIYAY0RlTUBAIQAACEIAABCAAAQhAAAIQgEA/CExFjHjXDzf1g24PnvLXTloKX/rJkR48aXuPOBgMhpktLCwE/bsqd7lWPnWu12urrrfXeddW/W7LXDdPvafkebxnf+jWpfBfP12twsfvUyJw7i8shVvu7p//sHVV27nX3uPfbLvwfot9R+oa+7227ZL0rJ9KPcfiQgiDcNQ/8ekugeOXQrinf02xuwaJSra8EMLho6EBn44REB+3im06ZpVjxVlZDOEAYWBn7SMFO33zIFx0ymJY2dxcH1gY1Bn5RI+/Z+++cNOBTWF5aanTYChcPQKnH78Y7riHVl6PFle1TeDkTQth10GihLa5183vfpsXwv/ei33q8prF60SGSFm4jhCjz+wJMfJbaRqWZZM0bJ5VaZSma4WpttI4bnEhHPr5iMrLs0k5UmJykzSacLZ1JA5L64h8nh2rvivNs266cVtXUa+t52vbVl6dSU3IpMpWVe/qsE+VQ+5Nsbc+LjXx0rRsWtZYuM6Vo8ofaZuK06gqW64Nef55UuWI063LQoS8I2Hj5N44beXVjSacU8/SJA1vSN6kPns+t6puxH6q5Hop98FDh8MTTmpZjJDCbztx6yzGVZQZAhCAAAQgAAEIQAACEIAABCAAgREJ3PWj3UNdoNWVEYgRI1qN2yEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIADFiFq1GmSEAAQhAAAIQgAAEIAABCEAAAjNMADFiho1H0SEAAQhAAAIQgAAEIAABCEAAArNIoNdixJ49e8LFL31ZOP3008OVV7wxbNmyMrTh92777/CiF7807Nixc82mV73pivDs3/7NrI0/+alPh0svf/2Ga1L3vuVt7wjvu/oaNw8t283/dsvw94995EPh/Mc8el3akt+/fuWr68o+i5VwnsssdelDH/5IuPzSS9bql31eWwdOO+3U8MFrrg4PPutBa5dQD2andnzt698IF/3eC8OznvmMZJsUe++864fZ38UneO1dSJTmYX2TVz6tZ5e+9tUb/MzsWGD0kiqnP3jpi8Mlr/njDQnu338gXPb6N4RTT7l/9vebvvnNDW1ZEyvNw/oKr3zia1576WXhrVdduc6HjE5ltlIobSP6lGrjnA1L86AdhqB8r73u+qSf0xjsbVdd6foj5V/VTpvmQX8bgjL48a5dSR8m/F9z6WXZ36UvxD7j951dto8dO3nxrNCQfkw+Xv86flrtpzgO/6YcH/XIR7qxYmkeXfZvvRYjNDCIg/Mb/uEfw0MecvZaQKcdX5UgUVcc0IokzcSKIHEw9LgLLxgKIF6QKd+96c1vCW9/y5Vh27Zt7bc4cqwkkKpfemM8MJXr3/We96518FpPqAeVqDtxgfoJKUxOgEyJEdJRvPqSy4bB4IUXnO921iV5yD1XvfXt4QNX/9XQV3jBgHz3y2eeWSm4dgL0BAthB4ueIFQlRqivliI+93ef4/IsycP2LZK2CCLqF+R/LddFz3l2r8UkK9iVtsM6NqQdljdCG0if9yvnrvklm2KVGCH+6j+/852wd+/d7v0ledDfHrWAHbCkhPUqMQL7lLePqju7ap94QiOOO9Q3f+wTn5zrCdQS3xPbXPr7j378E8OvvcmFkjy67t96K0bUadC2glTNaMq1dcWIqrQ8ocEOFuJKVeW8+L1dAjZQlZy9Dt0TmOKBDvWgXbuNmpsGaE964hPCjV/8F3fWKNf25X7pqH/jWc8I7/rL97pBdkkesdAQ1ysvaBiVxazeLz78Y5/4u+FquTvuuGODDarECLn/+7ffPhR2UqvWmubhCQ1aV1TMrtv3zKpdmpS7pI3Y9OvYsCQP2uFRynYyRtrYBec/ZoPwmhMjVLT9/ZdcPBRZvdVcJXnQ364XI84552HDfuwPX/HyDaJqTozAPk28VfNrdezSNfvEfVLcb/Vl9WWJ77G1QO+XOPAz116/btJBryvJo+v+rbdihAYcYtzcsmk1fp1gr87yozoNMm7UUgYbyNQpS3MXxx3jImDtc/1nb3AHJSkb6kBFZrG/e+v3hoNTu3qGejAuK40/HQ3QZGmxBMlekJ0SI6zA+NSn/Ppw+5gXZDfNQ55SZtHtjLntlOT3VF7jJ9T9FLX9CXtZhhwH4jkxwvr27du3J7dMNM1Dg/vLX3fJutV66ht27NzJ9gxTtZq2Ed2eKUnUtWHTPGiHxwxk25CIdnY1oF6VEyNs3/nu97x3eEu85LskD+Ku9WLERc/5naGw+tWvfX2DKJsTI7DPZPs59VFds4+OqbQtxmJEnfHRZMm1k3qJ77Els5NDn/v8P7vjh5I8uu7feilG2FnpT//9Z2qJEXUaUp1r6sxC5hQsGajIUm4bmLbTxMilhIAnOuQGNLaTl/zirTgqRlAPSqwx2Xus7Xbv3j08PyJe6p8SI+KVMqnrSvLIzchKZycB57zu32xqcSsGCpt4oFTVdnU7zMrKluTZEk3zqFoZIQMyttgcs3RJG9G7bf+cs2FJHrTDo5RtG3rlK14+bCfy8c7tis+MiFeFpgbFJXkQd20UI1QYj4X1Ku66hazqOjl7p24dwD7dtk9uZYSI833ZVl7ie2ycYseRKVG2JI+ut5/v33lXOOWXtoeVzZuahm1hYTAYDJretWfvvuEt207c2vTWsVwfd2ZVWyYkUxs8ps5nsHt4tKDeAS46OL34RS8IL3/lH60dkmkP+rHKp54ZoQ35/R/4IIHnWGpCO4nkxAi751tLYwel20/aNpy1FgWcetCOvUbJJQ68xLfEs0opfxPXk1QQV5JH7L90UPSIR5zXmwChrl0tKx2M2oFSToyIxehUv1GSh603Uh5d7SJ/z/se3Lq2s4KCPVyvSTusa0PaYVOrHLve244oB4bbVUipIDwWbeNYSXMpyYO4a+NgV+IOqeuxsJ7qn7BPebuoe2dcT7tin7jNqjjxp6+/LPz5FVeuW51Z91ln8boS36PPGa+cT8UbJXl03b/1ToyIg/7U4EC+1zddpE4ErmoomoadHdXv7DkC2oif+fSnrc1QeqfSymyrzrzJrJ2+uSN3en9VGfl9sgRKxAgJzHRGiHowWfuMM/U4QFPnb2eVPH8TC6RSplSQXZpH7M90NkoEMZ39yr25Z5ycupxWLCBo+9OBUio48LbfpQZUJXnEYrcckGq388jMk74BKnWKeZe5j7NspW2kiQ1L86Adrl8ZoSuypE3YVUi5thOfxZLzqfatN3XyoL/1+55Y0EuJEV68g33G6d26bR8VRuSJ9XBau9VAVvGNOq4aL83xp+bFCHV8j5TEWznvTWqU5tFl/9YrMcJzoHVWRmgDKxEl4vTl/+s+e8OGw+3iyho3ERsoyW9Vp+OPv4mRYgmBEjEi93o+6kGJFdq5x/Mvcbv2/E0qsKt7bZ08YgJaL3XWIvfGlnbodSMXr+O3gXhq6X7dgEGesjSPmJDO4r/k4hetO/ejzlbAbtCeTClK22ETG5bmQTv0xYhYuPXEiLoCrTD2gvU6eRB3+YPdWJT16j/2mYw/S9VJXTErv3fVPnalzLe+dfPa+QdS5viNUO3Qm3wupb4nNdHh+cLSPLrs33ojRqQcZR0xQhWr3HuVU1XcWyLtHZiZO7BJA1jd253be8prPifvbJrkMMqZEQ8+60EbsrKHBFEPmlhi8td6AZp2GpK77ImWmYG4/dvZUq+UdmVVaR42XXsgonyfOpdEluj27eMNSO0gRleU2BlXb4ue5RavVCjJI7aD3f8ZH3TrnTHRJzuWtJGmNizJwwsE9fynPrXDVNBtl5vrSh97ZoSddfXqs50sKs3DE4v6FnelYmUressq3Tgexj7teNlZso/GqM94+tM2HKQdnzHRDr3J51Lqe+yqBa+UdgV8aR5d9m+9ESPEkeq2hlR1lKWvqQC8SiyoK0ak9hHHe+1sejbw1Jm51On4iBGTdzZNcvDECLk/d3aAvFpQ3qYR25J60IR8+9fm9tHqnmgJbK0YkQospPTeNo+SPGISVsTyAoJY5Gqf5PRyTPlnDbT/5v3vG75uy4oRub4hnrGSJyvJwxKJxQYrUMp1iBHf2DBQEi7WFnE7bGpD2mF5G606d0XO2dG32VgxIjdxFG8jKM2DuCu9RdAK6/LawT/5szeuW+GLfcrbRJM7q85JkbS6YB8bWxw4sH/Dwft9EyM07k/5t1RcoDGD3cY2j/6tN2JEqrFPcmWEV2Hq7iPW8nr7yZkRb+K6p3ttSozIzazZgQ71YLr2a5J7aoBiO5OTt28PZ5xxxtrJ8bl7vIFrSR72GeIl/LkTllkZsW2d+XWLndjwwgvOXzvfJxdEeMJj7vpUHrYgsU+JgzrECF+MyLXDpjakHTbxjOuvzQXSOtCSs9F37d69dnZSTrS1QpOKFyV5eIKfPWS6L3FXjrXGr7JqU/7+4DVXD183jH3K20PTO2fBPqmDGO0kah/FiJR/y/krqR+xzefRvyFGvO0dG2Yq5Y0VshxX3z/uzW7Fs5by/7XX3xBe+PznrfmW3PkQdvbbS18T8Qaz8YBC9w7zer6mbn3y16fEiHj5vtS1KmU0PriLejB5+zXJITdAscvAdbmdpO291s7m6Z1Qndou5uWhPsx2aDLreP5jHj3MJhY7cyu0mrCY1WtzbVB9vhz0qUvCvdUr8bPH9aJpHnF9iM+UiQM/zoxIixFeG5FZO3lrUfz6wljEs+2uaVunHR6jWRV42+X+ukWt6kytOM2SPHKCn/zWl/62SljQVcZ2+xn2aa/HmwX7eGMSGwtr7OO9Ua49kpPJqcT32C1qGpvFpbMT58rPm7hUXyVvwJFP/Hp5+a6L40rEiEiMEEN5e7hjg3pihAQ0eiK9pJN7y0W8bcSrMLmBgb2/5GDNyTRDUo0JpMQIOxC89rrrh7fp6cPe9ozUoZbUg+7UuapVDtrhqF/YsXPn8A0I9pV28dPEotUt3/6edCcXAAAEU0lEQVS2uwRd74vzsIOglGhpB9l9fxND1Qy5tjf1uXWCiLivaJqH2tZbJae/2f2mKT/SnZYy2ZI0bYfSpuJXF8YljG3YNA/aYX0xwgpGEhed+/CHV4q2GmDrUubTTj11eE8qWI/zsAOAvsddVYNd/f3Hu3YNV0Yoa7GBnItk63os8GCf0X1f1+2TE8Pt2Cq3LX50StNLoUqM8HxPvM3MK72NNdQnzpN/670YMb0qS84QgAAEIAABCEAAAhCAAAQgAIF+EkCM6KfdeWoIQAACEIAABCAAAQhAAAIQgMDUCCBGTA09GUMAAhCAAAQgAAEIQAACEIAABPpJADGin3bnqSEAAQhAAAIQgAAEIAABCEAAAlMjgBgxNfRkDAEIQAACEIAABCAAAQhAAAIQ6CcBxIh+2p2nhgAEIAABCEAAAhCAAAQgAAEITI0AYsTU0JMxBCAAAQhAAAIQgAAEIAABCECgnwQQI/ppd54aAhCAAAQgAAEIQAACEIAABCAwNQKIEVNDT8YQgAAEIAABCEAAAhCAAAQgAIF+EkCM6KfdeWoIQAACEIAABCAAAQhAAAIQgMDUCCBGTA09GUMAAhCAAAQgAAEIQAACEIAABPpJADGin3bnqSEAAQhAAAIQgAAEIAABCEAAAlMjgBgxNfRkDAEIQAACEIAABCAAAQhAAAIQ6CeBqYgR/3f3T/tJew6fenl5KRw+fGQOn4xHmgcCS0tL4cgR6mdXbbm8tBQOY5+ummcs5VoIIQzGkhKJTJLA4sJCWB1gqUkyHi1tWtJo/CZ3N5aZHNtxpLywsBAG+LZxoJxYGouLi+F+J28LK5s3Nc5jYVBg3bt+tLsos8al4wYIQAACEIAABCAAAQhAAAIQgAAEOktABIkTt57QuHxFYkTjXLgBAhCAAAQgAAEIQAACEIAABCAAAQj8nABiBFUBAhCAAAQgAAEIQAACEIAABCAAgVYJIEa0ipvMIAABCEAAAhCAAAQgAAEIQAACEECMoA5AAAIQgAAEIAABCEAAAhCAAAQg0CoBxIhWcZMZBCAAAQhAAAIQgAAEIAABCEAAAogR1AEIQAACEIAABCAAAQhAAAIQgAAEWiWAGNEqbjKDAAQgAAEIQAACEIAABCAAAQhAADGCOgABCEAAAhCAAAQgAAEIQAACEIBAqwQQI1rFTWYQgAAEIAABCEAAAhCAAAQgAAEIIEZQByAAAQhAAAIQgAAEIAABCEAAAhBolQBiRKu4yQwCEIAABCAAAQhAAAIQgAAEIAABxAjqAAQgAAEIQAACEIAABCAAAQhAAAKtEkCMaBU3mUEAAhCAAAQgAAEIQAACEIAABCCAGEEdgAAEIAABCEAAAhCAAAQgAAEIQKBVAogRreImMwhAAAIQgAAEIAABCEAAAhCAAAQQI6gDEIAABCAAAQhAAAIQgAAEIAABCLRKADGiVdxkBgEIQAACEIAABCAAAQhAAAIQgMD/A+JoRMZPf0KvAAAAAElFTkSuQmCC', `mes` = '8', `anio` = '2024', `tipo` = '6', `semana` = '0'
WHERE `id` = '280' 
 Execution Time:0.18073487281799

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '5'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.092395067214966

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAl0AAAEFCAYAAADZt1IsAAAAAXNSR0IArs4c6QAAGKZJREFUeF7t3c+LJvldB/Dqnt9Zx9nMrKC7LkbjSlAIojktSCT6HygehOhBb5KD5KAYiCh48CCCIl70oAERr141xIPrJYoElMAmuLJZCSQ762bczE7PTLd8e6nm2zXf+vH59NM1T8/z6tPu9OdT9dSrvlX1ru9Tz9N7R0dHR50fAgQIECBAgACBcxXYE7rO1dfCCRAgQIAAAQLHAkKXgUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQWEFA6FoB2SoIECBAgAABAkKXMUCAAAECBAgQCAgcHh52+/v7gY4PSlOh6+13vtP9wdcfdoeHR+EVaiBAgAABAgQIXFSB/f297nMfvdzd+fCt8CakQtc7797rbn/pMLwyDQQIECBAgACBiyxwea/rvvXJ/e75WzfDmyF0hck0ECBAgAABArsqIHTt6p633QQIECBAgMCqAkLXqtxWRoAAAQIECOyqgNC1q3vedhMgQIAAAQKrCghdq3JbGQECBAgQILCrAkLXru55202AAAECBAisKiB0rcptZQQIECBAgMCuCghdu7rnbTcBAgQIECCwqoDQtSq3lREgQIAAAQK7KiB07eqet90ECBAgQIDAqgJC16rcVkaAAAECBAjsqoDQtat73nYTIECAAAECqwoIXatyWxkBAgQIECCwqwJC167uedtNgAABAgQIrCogdK3KbWUECBAgQIDArgoIXRN7/h9efa77ue+7/ETFP37rUffzr73XffrlK92fffxGd7MoDn4ODrvuD19/0H3+q+93w+W89f5h9yv/er/74rcfnepaWvf7H7ve/dYr17qr+x+0/+e9w+4nvnjviddQL+/eo6PuN75yv/vCmw9P6j71wuXur3/6RvfS9f2u9ftSuHRdu3oAbet2j43dv/zvg+7X//1+9x+futn9+M39rh6n/bbU+7weW0vH51TdcDzVfsMxOPY66p76GKxf63A9re1sje+6bumyx7ahNy6/bx3z9fL73//px28c75e5n34/lrr6OK77+vPU3LL8fjsE6vHSv6Kxc3urdq5n7Hy/5Jj87VeuLx6X//T2o5PrYv/6/+Inb3S/9kNXR69X9WtrHat1f7+dY9es7dib469C6GrYjJ3E+tKzhq7WoGqtc2k4Gxt8w4vf8CS8JHQNl3FRB/q2H4iben1TJ9Cyjk2Grsw4nnt99WvcZOgqy22FkOH4Pq/QVW9Xv683EbpaF6OpILupcWY5mxOYOyZax9lU6BoL+U8zdNVjvXUNqQ3q697ctXjsuN7c3tn8koSuhml9Ih4Gn3KS+8iH9p+Y6RoLSGXx/fLKYLu2v3c8QzW8APSDrtQ/ODw6nj1rLXNsENZ3vv0mTV1QhnfIczNhNVNrXZsfmpYYFRievIdjrIyHN757eOaZrrOM4yWzaP24/9kXLp/M6I7d8c/NRk0dT/Wx9PbBUXfn6t6pmb+5ZZfjeOx1lfUOL4zDY6wVuurZ77nQWQeu4YV5bpYtOrbUn4/A8Jitz61T71S0Zqrr8TR3Q1SPxblxNtzyufqx42Zqdr3e1v68NbzWTc1kX6RrktDVOJbqE9bUzpw7aQ7DTzkQxgJVK5i1Qlc94F+7+7j7xPOXjkNc6+Tfeotp7C5i7u5jbl3nc0qy1KUCwxPU3EnoLG8vnmUcj52wW3fCrzx36cyhqxwbrUBVXFs3OtGZrkjoKuus6+fOH1MXt7m3Y6LjYek4U7dZgblrTf37+iZq7PidOq6XzHRNjed+y7Ohq75JGN4Qtl733FuS9e+nJj02u8fOvjShayZ0Te3MuZPmMHSVYPP6e4fdT926dOqOuj4Y/u3dx90rz+2PznT1QapcHP78jQfdL754ZfSZrLr2y//7uHv19qXjl9S6k2iFrsi6zj4ULeEsAmPT82PLPEvoOss4fhqhq75hqE/2/fgux/iDx133I8+dfsZtUzNdtVfZH30gnjt/TF3cluzvuYvWWcab3rMLzL3lVt8YlJuH+lrUOn7nwtDTDl1j433s3+cC6dyNx9n30PksQehquLbeY597EHa4mDrE1LNYf/fWw+7TL1899RZjfcf9hTcPul966UozdNWDrH89v/Nj104e9h/ObsyFprfuH40+SB9d1/kMT0tdKhC9wJ41dGXH8ZK3F/u77bmLSLFZEoz+9q2H3c/cuXR8c9IfN6W3/xBJCWI/cH3/iQ8WLFn2cP+0LozlXPDHXz/ofvOjV08d1y/d2Dt54HhuVns4AzE1a9CakbhIMwFLx/xFr4uO7/qaMvVM19gs95LQNTWezzrTVfrnZrSWTgj0r2UumG3jGBG6RvbK2AOqY28PLA1dv/vVB91nf/TqqQtAH5zKifGPvnbQ/d7HrjVDV32Q9oNz6sCt314sB+L/vH906u2az3zl/mjoiq5rGwf3Lr2m1jMRU9t/1tCVHcdzDw1HnzVZEozK2C/PYZZPIvdvHxab8gng8lM+ZfwLL145t9DVr/PF63snn+Aqx+9fvXmQDl1L9veS2bBdOka2bVuX3CiNzYbNPUjfeptwG0JXvc19OKwnB/pP/C+ZBaxDXPnvuUcqtmX/C10ze6L1XFTr04tLH6QvX9vwqy9fPbkA1G8Rzp2IWxfKqU8gDkNX+aqA+t/qGYDh24vRdW3LgN7V17HkIlzbnDV0ZcfxVOgaXiiiMwFjD9oObzjKcVZ+SggbflXDeTzTVS+zdp+a1S6vb+kN1dhXQwhd23022EToqsfW8Dm+pZ9WX3Kc1ZJz9WM3QmUZw9/VN/71NVToao/dvaOjo6PosH7n3Xvd7S8dRtueen3rkyTlRfXf0xUJXfUDwn//zYdd+ZRW+VRjSfmvv/e4ucyp7wSrccY+/dL/e31gfvvgg933wtW9U9/TlVnXU99BO/4Cog+VbiJ0Zcbx3Ak7cnJvncT776ur11PG/t984+HJrG4JZ+WnfB9Wf2Ga+zTYWKBb8iD9WJAr54zvvbw3+vzmlNWS/b3kor7jh81T3fx6/y75nsS5Z7rKxkzt8yUzXef5IH2P3R9rZZtbjymUuiUfBPFM18Lhe1FDVyt5Z0NX/SxVz9YfUGPPecx9H0+/nLEvs6zDWGu2oT7oM+tauPuVnZPAMCjPTbW3ZkH7lzb2vFD9bGKZ6cqM46cVuoazvGVbx2agWm9xbCp0DS+Mw+N/6VdGzH09QFnuRXze5ZwOj61c7JJgMTaDPXbTNBW6xh5YjxyTBXKufmqmqx7/5fj7xv3DJz7A0u+settbYfCizuR6e7FxOJYB/S93Hx1/n1FrAMwFpOEihxer8q3wY19cmvkkx9hU7NSFdeo7hKZO1kunfbfyLPeMv6jhmGp9sKL/nq6xu+yp/buJcTx3wj6vma5yLA9vNuZmDpY8Lxad6WrdxZd/iz5IPwxvU9/TtWT24hk/NLZ284ZjcuydiuH4mHv8o2xwmcX957uPu6+99/j4L5GMBbjIMbmJ0LX0g2pLv6dr7K9NbOtOF7pGQtfUn+JofeS7tYP7ty5aF6t64E29/VD+XFA9+zU2Dd0KSlOhazgz0i+3bEf/lmlkXds6wHftdc09YDt2Up8av8Mbj7GH3efGcZnFiZzg5x66L9vS+pMjwwtD6631/qJU/pxXPSsUfaar5Rb91v9s6Cp9rWdO69fkU4vbfwaYe2ehdR6eO877nk/euXzy4Y1eYurPbU2N5/53c8fw3ExX6wu+x55LXPKoy9ys/raNAKGrsUfGDoLhCWxuQEyFrtZXMpSLUmum65d/8Mrk360a3vX2d7ZToWvY0zpIx+6QPSuybYfx6dczNi7nvgB37KQ8Fboi4/hph646pIzNDK0VuoaBKTPT1e+Xsf190S5G231Une+rG/tLI2NhZCp01eft4bVsbpytEbqGY3/JTNXcF32f797Z7NKFrs16WhoBAgQIECBAoCkgdBkYBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBISuFZCtggABAgQIECAgdBkDBAgQIECAAIEVBJ5K6PqTb15dYdOevVUcHR11e3t7pzas9W/P3pbHtqg3+ciH9rs3vnsYa1b9zAgMj43y/+WnPobG/m3uOMsei5vsG+6ouXPB3O/L8jb5+lr+S1xb+yjS9+rtS91rdx8/M+PYhjxbAvtd133m+w+652/dDG/Y3lF/xgq0vv3Od7ov379yfHD7IXCeArevXeruPnDyPU9jyyawbQI//D1Xuv/6v4fb9rK8HgInN32fuHHQ3fnwrbBIKnSF16KBAAECBAgQIPCMCBweHnb7+2XOK/YjdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAkJXik0TAQIECBAgQCAmIHTFvFQTIECAAAECBFICQleKTRMBAgQIECBAICYgdMW8VBMgQIAAAQIEUgJCV4pNEwECBAgQIEAgJiB0xbxUEyBAgAABAgRSAv8PfnAtHrj438kAAAAASUVORK5CYII=', `mes` = '8', `anio` = '2024', `tipo` = '5', `semana` = '0'
WHERE `id` = '279' 
 Execution Time:0.095472097396851

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '9'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.18774890899658

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA3AAAAJWCAYAAAAZaGVIAAAAAXNSR0IArs4c6QAAIABJREFUeF7s3Q2cVFXh//EvPoSroK0JiiiWrk/5wJroFj5mpabL+sNcRTStldxE0RTYlv2l/QRdNwwNTRJdNrNcUVJyITPTnjRrFQ0r/flAZBChkqHyywVL+f/PxbPevdyZOTNzZ3bu3M+8Xr2S2XPPPed9zu7Od++95wzYuHHjRvFCAAEEEEAAAQQQQAABBBAoeYEBBLiSHyMaiAACCCCAAAIIIIAAAgh4AgQ4JgICCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45gACCCCAAAIIIIAAAgggEBMBAlxMBopmIoAAAggggAACCCCAAAIEOOYAAggggAACCCCAAAIIIBATAQJcTAaKZiKAAAIIIIAAAggggAACBDjmAAIIIIAAAggggAACCCAQEwECXEwGimYigAACCCCAAAIIIIAAAgQ45kDZCdxxxx06++yz+/Tr0Ucf1RFHHFFSfZ0xY4auuOIKTZ8+XZdffrmef/55nXHGGV4b77rrLu277745tzdYd84VpTjQGJvXWWedFXXVfep77bXXvHP89Kc/VSmOYUE73w+VRzFv7PdfY2Ojrr/+eq1YsSKyee1CEuX3kcv5KJNawM4nWyLsezhY5gc/+EGfnys9PT269NJLvSrMfKqoqJDrMXPnzvWOs3PRHGtf/p8t5r2RI0em/Llr22DqC7aP8UcAAQT6Q4AA1x/qnLMgAv5fsmEnsEGpICfPodK4Bjjb7mJ8kCHA5TCx8jiEAJcHHof2EQiGrGCIS/fz2v+zxQbyqVOn6tRTT/XCnA1m/hPaY1LV6w9xts6nn366T5tThbjf/OY3OvLII72yxfi5x1RCAAEEMgkQ4DIJ8fXYCNgPDCeccILMVYAPfehDXtv9HyRK6SpOFB+W+2NwCHD9oV6cc0YxJ4NX4PxXPYrTC87S3wJhf3hx+YOVnTv+n+E2PJmf3TvttNNmV3ODxzz33HNe2LJ1/OMf/0h5jA11xssGw+Af+oJX6ghw/T27OD8CCBgBAhzzoCwE0t02ZX8Bm9sUzW2Uqa7qpLr1a5dddvFucbzwwgtl/mIb/GDgf8+ERn9brrzySn3961/3jjMvf4DM9IHGfFgxtw++/PLLuummm7wgam4ltH8ltl/3v2dvuwz7IB78IBK8rch/zJ577tl7G6rt77bbbrvZX7/9f7EO+8u3S2AO/qXe3y6XK3DZepv6X3rpJc/S/2Et021ZwT8GmGOtU9jtgscff7yuvfba3vEaMWLEZn7+89v556/XnNOW8d8aHPyQGXZFIZN9cLxMneblv63X/DtYd6Yr2S63UPr/CLB8+XLvnOZlPxz7x8L/gdk/R21bzf+HXV0x7/tvRU7Xj1RXZPzfs2HzO5NF8Hsu+DPA/vD19zd4FaiQ8yLT97z9I1gufQ/+Ygn+4Scs6NtxMD9z7R/hzP+b7yMzlkuWLPF+LoWNtz1mzpw5m81h/7n9V/H8c8vvbH7eh42Nf46WxS9OOoEAArEVIMDFduhouF/A/pU27FmHoFS2AS54m42pz3w4f+WVV3qDmf9DpH3mJ+w4/wc41wBngkbwFXZ+/1+tU9UdbFPYMWEzy7i2traqpaWlz+1LYWEyeHy6v1inus3KfjDOJsC5evvbZ9qW7rassICVysf/vJe/LcZ43rx5XgBPd+tX2LOb9lzm2UjzATbYdhPws70dzNTherux/9Yx/7nTfZ9lE+CClmY+7bzzznrwwQf7fMmG0VTzJez7z7xnA1ymfqT7njXn/tjHPpbx1r10P2eCX7P9STcOtkwh54WLp2l7ptsW0/028s/PTD+fg3PHntv8v33+LXiu4DHf/OY3vQCXKvj7w5mtK9Uzbrbt+++/v1d0/vz53ELJRw8EECgJAQJcSQwDjchXIJvbtnIJcPbDgP/DVPC9sCtzNgD4PyDY97IJcPYY/wfR4Hv+v9wH6w677TH4nv23vx7b37C6wz4ghd36lOq5EjsOZuz9f233/4X9rbfeyriIif8Doot3qisc/vf9dZoP0vvtt19vO2y//fMoeAXOBDh/+LP1ffzjH99sIYbgFTbbDv8Vu7D3gseFtSvVVbhg/8yV6aDjlClTej+4hwWOVHVnE+DsfDFzwC5WE/Ze8Mqcf6zs94R9z9TlXwzI7+jSD/+4Br+n041f8GdY2NWk4Pdl2B+egu/de++93lWnQswLl+9565lN3/0W/p9Z6RYK8c+/4Fw28zMseAWPyfbqmm1n2M85+zP7d7/7nW677TbdfPPN3h9guIUy39/WHI8AAlEIEOCiUKSOfhcoZIAznbN/yQ+7VTP4QS3smQtTh/0gYz8UBm/1Cdbtv0XSfvAMC5/2PXOrpW2n/4PixIkTez8chw1UqkBpyob1Nxj8UgXibFZuC15FsoEomwDnHycXb1MmXRv9hqeffroXCvy3dtng6Q+cLisuBq8GBYOY/ypF2LxOdauXaU/wiopryLLPqYX1OdWVzVQfZLMJcGG3sIa9Fwxw/jLBMRw1alSfAGcDiEs//HW5LGhh6k51G2Xw9sngs7lh4+X//rTlH3jggc1uG4xqXoTdap3udvRUc9flF0C6n9GprtL5FzAJrngbdoxph71a6HJ7pP972Px32DHBq/QEOJfRpgwCCBRagABXaGHqL4pAIW+h9H9oD/ureqoAF/ywHyznGuD8wSwsrOUb4GxgsLcehV058oejVAHO30476GFX/uzX0t0+lkuAy9Y7GOCCYccflI477rg+CyPYZ4OC8y5VgAt7Fso6BANcqufi7BWIYIBLdXugqT/bAOev24bWVMEnVXDJJsCFXcVN916q+eR/P9sAl+o2Wb+dy/iF/aBLNTZhz/oFj7cB0j73VYh54RLg/H9ICrYx0zOA/vKpgqE/iAVDrv/5N/+2KqmOSfUHmbB+BsObvy/BuwP8z/8S4IryK52TIIBABgECHFOkLATS/dXY/lI/6qijvCtR2d5CmWuA84ce89+5XoGLMsClW9jC5cOc+RAV1RU4/4fb4PM+uQS4bL2DAS74wczvkSrAuYQV/wfFdLe5hl1Zy/Se/zbHbMxS/cEj7Apc0DXTDwwXE5dbes15Ut3mm8sVuEz9CLs92fY13a3EriEmLHSELbgR9M00B8KCveu8cPmetwEy3dwNtjn4sy64uJO9UyDsNmT/qqWmfaYue4u1OY/LMcGFeMLmW7qtAdL9UcS0IdOzfJm+R/g6AgggkK8AAS5fQY4vGQH7SzrTNgLZPs+Ua4DzPweVzzNw+QY48wHP2oQtm53u9rR0t1CG3eqWzTNwqRYsMM+ZZBNGsn0GLviBO+zDeb7PwAXDQvCDsv9DaD5X4Py3x9oQHBaMg9+k2T4Dl82iMsUIcLk+A5eqH3Z5evM9G/bh3GX8cgkx5nZrs+R92HOnth32GbhsrsC5zguXAHf33Xf3WdUxbO66zC87L8JWtQ27vdT+zBw+fHjv82/+n6Nhx4T9ocy8Z1fzDd4KH3xW1faDAFcyv9ZpCAIIpBAgwDE1ykYg3W1OppNhgSOs88EFKfIJcMH60/0VO9UzcFEEuOAzZrZd6VauNGXSBThbh39/prDb7VLdcpTuQ1KuAS4bb1vW9fa4sNUA7eqQYdsI+JewT7eSYD4Bzn+lJWwuu1xxDTsubNEcf7moVqHM9RbKdG0Om7OZVqG0ISlVvf5tNYJlUl2Bc7lF2NSVaoXH4FXpbAKc67xwCXD2Clw687CvBZ/H9P+8MIuSpPv+Nz+XzBYsZp6ZDbzt82+ZjjHfZ2HbnZhz++dsqrYFf0/YNmfzPG/Z/EKlIwggUNICBLiSHh4al4tA2Ifl4AfZ4Icr88vdLBVu/j+qAGfa7t8HLrgoQvDDUyEDnD+M2ZCV6kplpmfggoEn3ZLomfYi84+Vcf/a176mCRMmeHvfmQAUtpBLcE743bLxDtYT/FAXFjz9ZczXzct1ERP/scY4uDCKXawimw/q5upq2HiYfe5MuzLd3hdsk+lP8Paz4IfmTLePFeMKnOmXbWvwQ3eq26nT9cMlYGcaP/tcpH9eueyflmn/xFxuoXSdFy4Bzn/btLVOtahP8Hsq6Bq2D2bYz3jzs+niiy/WySefHLp3Zqpj7K2WYT/f7TYE2fyhjwCXy29gjkEAgWIIEOCKocw5EiWQ7nm8REEUqbPF8E5121iqxRGK1PXEnQbvxA05HUYAAQQQCBEgwDEtEIhYoBiBIuImx7q6Ynmnu+0q05XGWAOXUOMJcCU0GDQFAQQQQKDfBAhw/UbPictVoFiBolz9su1XMb3DQhzhLdsRy708AS53O45EAAEEECgfAQJc+YwlPUEAAQQQQAABBBBAAIEyFyDAlfkA0z0EEEAAAQQQQAABBBAoHwECXPmMJT1BAAEEEEAAAQQQQACBMhcgwJX5ANM9BBBAAAEEEEAAAQQQKB8BAlz5jCU9QQABBBBAAAEEEEAAgTIXIMCV+QDTPQQQQAABBBBAAAEEECgfAQJc+YwlPUEAAQQQQAABBBBAAIEyFyDAlfkA0z0EEEAAAQQQQAABBBAoHwECXPmMJT1BAAEEEEAAAQQQQACBMhcgwJX5ANM9BBBAAAEEEEAAAQQQKB8BAlz5jCU9QQABBBBAAAEEEEAAgTIXIMCV+QDTPQQQQAABBBBAAAEEECgfAQJc+YwlPUEAAQQQQAABBBBAAIEyFyDAlfkA0z0EEEAAAQQQQAABBBAoHwECXPmMJT1BAAEEEEAAAQQQQACBMhcgwJX5ANM9BBBAAAEEEEAAAQQQKB8BAlz5jCU9QQABBBBAAAEEEEAAgTIXIMCV+QDTPQQQQAABBBBAAAEEECgfAQJc+YwlPUEAAQQQQAABBBBAAIEyFyDAlfkA90f3li9frqamJjU3N2vUqFG9TVi7dq0mT56slStXeu/V1tbqkksu6Y8mck4EEEAAAQQQQAABBGIpQICL5bCVbqPXr1+vGTNm6PHHH9c111zTG+BseBszZozGjh2r4L9Lt0e0DAEEEEAAAQQQQACB0hEgwJXOWJRFSxYuXKg5c+Z4ffEHOPP+okWLNGvWLFVWVnpfX7JkiVfW/15ZINAJBBBAAAEEEEAAAQQKJECAKxBsEqs1t05eddVVGjdunG655ZY+t1DOnj3bI/HfMmmvwk2cOLHPrZZJtKPPCCCAAAIIIIAAAgi4CBDgXJQok1HA3jo5dOhQmdsk/c/A2a+Z5+HM7ZP2xW2UGVkpgAACCCCAAAIIIIBAHwECHBMiEgH/7ZAmmBHgImGlEgQQQAABBBBAAAEECHDMgWgFgrdCBlehLPQVuHXr1umFF16ItlPUhgACCCCAAAKxEzjwwAM1cODAnNq9dsNGfeP3b+d0bBQH1e+1lQ4dsmUUVVFHmQtwBa7MB7gY3Qs+31bsALdhw4ZidJNzIIAAAggggEAMBMo5wJk/mk+YMEFLly7tHYm2tjbV19fHYGRoYlQCBLioJBNaT3BvtyCD3euNRUwSOkHoNgIIIIAAAjERKPUrcAsWLPAWiOvs7FRNTY2nagPdiBEj1NraqoqKipho08x8BAhw+ehxbKhA2EbebCPAZEEAAQQQQACBUhYo5QC3bNkyNTQ06Nprr+0Nb9bSfm3SpElciSvlCRZh2whwEWJS1SaBsABnr9SNHDnS20qAFSiZLQgggAACCCBQSgKlHOBmzpyp1atXh15l6+npkQlxBx10kMdprtQ99thjfcp2d3dr6tSp6ujoUFVVlUx99jV37lzvP6urq9Xe3q5bb71V9r3GxkZvYTrzCjumrq6OK3/9MIkJcP2AXu6nDAtwps/B2y3t7ZXl7kH/EEAAAQQQQKD0BUo1wJmA1tLSotGjRztdYXMNcCak2efn7FW8VatWbfaevbJnApw5xoY62y4zsty+Wdz5TYArrjdnQwABBBBAAAEEEChBgVINcPY5t3HjxkUa4MxVOXPFrbKyUmFhzL43bNgw7yqcCXD+Y8wQpru1swSHuGyaRIArm6GkIwgggAACCCCAAAK5CiQtwAVvybS3SNpbJsMCnLG1Xzf/ne3VwVzHhuP6ChDgmBEIIIAAAggggAACiRco1QCXbUhyvYWSABffKU+Ai+/Y0XIEEEAAAQQQQACBiARKNcCZ7qVbxCT49cWLFzstYhJFgMv29s6Ihirx1RDgEj8FAEAAAQQQQAABBBAo5QCXzTYC5grc/Pnze59vMyNr3rvxxhv7rEKZS4ALewbOPh9nVrfkVRwBAlxxnDkLAggggAACCCCAQAkLlHKAsyHMZSNvE7LGjx/fu+G3DX+mDv82ArkEuLBVKO0iJyU8tGXXNAJc2Q0pHUIAAQQQQAABBBDIVqDUA5zpj3+5f9s/uxWAv7/mipsJe+Zl9nczV8m+8Y1veLdi2n3gcglw5pjBgwfrjjvu8Or27xNnQ2ZwD7psx4HymQUIcJmNKIEAAggggAACCCBQ5gJxCHD9OQSZnsPrz7Yl7dwEuKSNOP1FAAEEEEAAAQQQ2EyAAJd+UhDgSuebhgBXOmNBSxBAAAEEEEAAAQT6ScAEuAV//k8/nV06dMgWOnTIlv12/kwnJsBlEire1wlwxbPmTAgggAACCCCAAAIIIIBAXgIEuLz4OBgBBBBAAAEEEEAAAQQQKJ4AAa541pwJAQQQQAABBBBAAAEEEMhLgACXFx8HI4AAAggggAACCCCAAALFEyDAFc+aMyGAAAIIIIAAAggggAACeQkQ4PLi42AEEEAAAQQQQAABBBBAoHgCBLjiWXMmBBBAAAEEEEAAAQQQQCAvAQJcXnwcjAACCCCAAAIIIIAAAggUT4AA52i9du1aTZgwQU1NTaqpqXE8imIIIIAAAggggAACCCCAQHQCBDhHy7AAZ98bN26c6uvrHWuiGAIIIIAAAggggAACCCCQmwABztEtmwDX3d2tmTNnqr29XZWVlY5noBgCCCCAAAIIIIAAAgggkF6AAOc4QwhwjlAUQwABBBBAAAEEECiIgP08unTp0t76GxsbvUd8/C9zMWH8+PGhbejs7Ez5OFDw7rKenh61tLSoq6srZX9MfeaV6nz2wLq6OrW2turGG2/03po0aZJX94oVK9Je9DAXRUx//BdGli1bpoaGBq1ataq3XW1tbYm5I44A5/jtRYBzhKIYAggggAACCCCAQOQCNpT5g4oNWMEQZMpOnTpVHR0dqqqq6m2LrSNViEsV4IYNG7ZZSEzXwQULFmj+/PmhwcwEMvMyoTOsT/56bVCrra3tPb+pu7m5Wf4+2HaPGDHCC4kVFRWR+5dShQQ4x9EgwDlCUQwBBBBAAAEEEEAgUgEbZK699trQq2cmFK1evbo3vKQKcDbwjR49OvRqVbEDXKbgZcKauWJng2iqfhls27dsw2akA1WkyghwjtAEOEcoiiGAAAIIIIAAAghEKhAMaMHKgwEvLgHO9CMY0mzfbCAz/zZX1czL3HKZLqClC3iRDkg/V0aAcxwAApwjFMUQQAABBBBAAAEEIhPIdNXMnChYJt0tlObWxlS3GRb7Cpxpuw2f5pk4/6ruwVDqsqWXS5nIBqYfKyLAOeKHPTRqDw0+NMkqlI6oFEMAAQQQQAABBEpE4HX9XS/pyX5tTbXGbHZ+l22rgrcPplvEpLq6OuWiIdkuYpKqLtdn4Gxnw64wBuvIdBupqcvFql8HOKKTE+BygEwX5mx16b45cjglhyCAAAIIIIAAAggUUMAEuNmqK+AZ0ld9ir6uKANc2CImYYuC+FvVH1fgzPmDi6vYdtTU1PQuXkKAe3+kCHCO36ZmIt16663ekqepVrbxBzsCnCMsxRBAAAEEEEAAgRIQMAHue/pyv7XkGH0pNMBFeQul6Vy6q2P9FeDCriAGA6jL7ZEuZfptgCM8MQHOETMpE8KRg2IIIIAAAggggAACRRKIahGTYIAzFyf8q1f2V4DL1C7zdZdVJlnEpEgTMi6nIcDFZaRoJwIIIIAAAgggUF4CmW4fdN1GwAalxx57zFvIZPHixX32awt+3nUJTWHS2T4DZ+qw5z7vvPM0b94879ZJcwul/8U2Aps0uALn+P1NgHOEohgCCCCAAAIIIIBA5AJRbOQdtt3A+PHjezfFDgakYgY4A2aC6Ny5c5XuUSQ28ibAOX9z2QC3Zs2azXa1d66EgggggAACCCCAAAII5CgQtpBeY2Nj70Ifttp0q1B2dnb2ubJlA5E5dvjw4X0+59oA19XVlbLFYefP5QqcOUGqLQWCJ7flVq1a1ful4KrwORLH4jCuwDkOU7qVJ4OT3bFKiiGAAAIIIIAAAggggAACWQkQ4By5wm6h9P/FIlgNq1A6wlIMAQQQQAABBBBAAAEEnAUIcI5Urs/A2VBHgHOEpRgCCCCAAAIIIIAAAgg4CxDgHKlcA5xjdRRDAAEEEEAAAQQQQAABBLIWIMA5khHgHKEohgACCCCAAAIIIIAAAgUTIMAVjJaKEUAAAQQQQAABBBBAAIFoBQhwjp7mCpzZrX7SpEmqqKhwPIpiCCCAAAIIIIAAAggggEB0AgQ4R8t02wjU1dV5u9kT7BwxKYYAAggggAACCCCAAAI5CRDgcmJ7f6NB/waC/qpYhTJHWA5DAAEEEEAAAQQQQACBlAIEuIgnh92xfsWKFWpvb1dlZWXEZ6A6BBBAAAEEEEAAAQQQSKoAAS6ike/u7tb48eO92tra2lRfXx9RzVSDAAIIIIAAAggggAACCGwSIMDlORMIbnkCcjgCCCCAAAIIIIAAAgg4CxDgnKn6Fly2bJkaGhpknoHjiluOiByGAAIIIIAAAggggAACWQkQ4LLi6rt4CcEtSzyKI4AAAggggAACCCCAQF4CBDhHvlTbCDQ2NqqpqcmxFoohgAACCCCAAAIIIIAAArkLEOBysEu3J5ytjr3hcoDlEAQQQAABBBBAAAEEEEh9GHnKAAAgAElEQVQrQICLcIL4gx37wEUIS1UIIIAAAggggAACCCDgCRDgHCeCDWfmdsmamhrHoyiGAAIIIIAAAggggAACCEQnQIBztAwLcPa9cePGse+boyPFEEAAAQQQQAABBBBAIHcBApyjXTYBzuwNN3PmTLW3t6uystLxDBRDAAEEEEAAAQQQQAABBNILEOAcZwgBzhGKYggggAACCCCAAAIIIFAwAQKcIy0BzhGKYr0CP9Y1+ogO10f1KVQQQAABBBBAAAEEEIhEgADnyEiAc4SimCfwlt7QHNXrX/qnPqxDdbBOVrXGaIAGIIQAAggggAACCCCAQM4CBDhHOgKcIxTFPIHVek6/1316Sj/SO/q3994w7aeROlkjVattNBgpBBBAAAEEEEAAAQSyFiDAOZIR4ByhKNZH4J9a4YU4E+bMVTnz2lG7vxfkTtYOGoYYAggggAACCCCAAALOAgQ4Ryr/Jt3BQ9ra2vpsI8AqlI6oCSr2f/pHb5B7Xau9nm+nHXuD3FBVJUiDriKAAAIIIIAAAgjkKkCAy0EuXZiz1VVXV7ONQA625X7I23rLC3Lmf2u03Ovu1hroBbmDdLJGaGS5E9A/BBBAAAEEEEAAgTwECHB54AUP9Qc7AlyEsGValb218m/6Y28PD9QJXpir0ugy7TXdQgABBBBAAAEEEMhHgADnqOcPZ8OHD1dHR4eqqrjtzZGPYmkE/qQH9Xv9SMv1eG+pvXWkF+QO0GewQwABBBBAAAEEEECgV4AA5zgZbIBbs2aNmpubZZ57W7VqVe/RwefgHKulGAK9Ai/qUe/Wyuf0y973RugQL8h9TP+FFAIIIIAAAggggAACIsBlOQnMAiXjx4/3Alx9fb0WLFjgBbrgi1sos4SleK/AX/WUd0Xuad3f+94u2sfbfqBG4zRAW6CFAAIIIIAAAgggkFABAlyOA2+DW/DK27Jly9TQ0KAhQ4awiEmOthy2ScDsJWcXPHlX//Heq9Rw74rc4TpDFdoBKgQQQAABBBBAAIGECRDg8hzwmTNnau7cuers7FRNTU2etXE4ApsLrNXftET3elflevSmV2BbfdALcqNUrx21G2wIIIAAAggggAACCREgwEUw0D09PWppadGTTz7J4iYReFJFuMBbWqsluse7KveGXvYKbaUP6GCdpEP1Oe2q/aFDAAEEEEAAAQQQKHMBAlyEA2yD3IoVK7h9MkJXquor8I7e1hP6oXdF7tX39pIzJQ7Q8TpUY/URHQYZAggggAACCCCAQJkKEOAKMLBmxcrp06friiuuUGVlZQHOQJUIbBJ4Sgv1lO7TKv2pl8TsIWdWrdxfx8GEAAIIIIAAAgggUGYCBLgIBtQ+B+evir3iIoClCmeBZ/SgntSP9BffXnK7a6QO0Sk6RHXO9VAQAQQQQAABBBBAoLQFCHBZjI9/M2//YXV1dWptbVVFRUUWtVEUgegFlukxPal7++wlt7P29rYg+ITOiv6E1IgAAggggAACCCBQVAECnCO3P7wR2BzRKNZvAiv1tLfgyR98e8l9ULt6K1cepQZtqa37rW2cGAEEEEAAAQQQQCB3AQJcFnZht0r6D2fz7iwwKVoUgVe1TE9ogX6v+/TOe3vJVWh774rckfqCttOORWkHJ0EAAQQQQAABBBCIRoAAl6ejXXmyq6urT02EuTxhOTxSgde12gtyZguC9e/tJbeltvK2IDBBbkeNiPR8VIYAAggggAACCCBQGAECXGFcqRWBkhQwG4E/obv1pBbqTb3S28aP6tM6QudoV320JNtNoxBAAAEEEEAAAQQ2CRDgIp4J9jbLpF2BW758uZqamvTGG294orW1tbrkkkv66JrnCCdPnqyVK1emLBPxcFBdCoGNeleP6y4vyK3x7SW3lz6h0Tpbe6oGOwQQQAABBBBAAIESFCDA5TEo6Z6JS9JCJzaYjRkzRmPHjpX998iRI3tDXKoy9pg8hoFD8xRYqi5vwZNVeqa3pt10sD6h8TJX5nghgAACCCCAAAIIlI4AAc5xLFJtIWAON2Ft8ODBGjRokHcVKmmvhQsXatGiRZo1a1bvxuVLlizRnDlzet9zKZM0t1Lr77P6uZZogf6iJ3qbNlR76XCN06EaW2rNpT0IIIAAAggggEAiBQhwjsNuAtyUKVM0bdo0VVVVbXaUuRpnXkkMcGGEwcA2e/Zsr5j/tkp7VW7ixIkaNWqU40hQrNACy/W4d3vl8/pV76l20C46XGdotD5f6NNTPwIIIIAAAggggEAaAQJcFtPDf8tkZ2enamref06IAPc+ZPB2yfXr12vGjBleSDO3WNpXsFwWQ0HRIgiYWypNkPPvJbeNBusw1esonaetNbAIreAUCCCAAAIIIIAAAn4BAlwO88Ef5Nra2lRfXy8CnGSD2uOPP67dd9+99/bJQge4devWacOGDTmMJIe4CKzdcqX+WNGlZ7a5X+/qHe+QLbSFDlhfq1Fvnalt3610qYYyCCCAAAIIFFzAPNIycCB/YCw4NCfoVwECXB78wUVMGhsbuYXyPU/zDJwJt8Zo1113LegVOBPgVq9encdIcqiLQM9Wa/XCB3+qZR98WG9v8a/eQ/Z48wgd+M+x2v7tXV2qoQwCCCCAAAIFE9hjjz0IcAXTpeJSESDARTASYVfkIqg21lXYq25Dhw6VCbbcQhnr4ezT+Lf1lh7X3d6CJ2/49pLbR0fpKDVoNx1UPp2lJwgggAACCCCAQIkJEOAiHBCC3PuY/gBnFi5hEZMIJ1oJVfWEFnhh7h/6S2+rRqjaC3JVGl1CLaUpCCCAAAIIIIBAeQgQ4AowjibIdXd3q729vXdZ/QKcpmSqNOHs1Vdf1eWXX65tttnGa1dwgRK2ESiZ4SpIQ8xCJ2bBE/9ecmYLgqN1ng7Q8QU5J5UigAACCCCAAAJJFCDAJXHUI+7z8uXLvWf/zjrrrN5VJk2oe/rpp3sXMglu7s0KlBEPQolU97x+rcc1X2YrAvvaXkN1tCboUJ1aIq2kGQgggAACCCCAQHwFCHCOY5dqI+/q6urEXGlLR2VD3BtvvOEVO/zww/tckTPv2dC2cuVKr0xtbW2ffeEch4JiMRB4SU/qCd0lszm4fQ3Udt6tlUfo3Bj0gCYigAACCCCAAAKlKUCAy2FcUoU5f1UEuxxgOaTsBFbrOe8ZuaXq6u3bAA3wQpy5vXJrVZRdn+kQAggggAACCCBQSAECXIS6/mBHgIsQlqpiL7BWf1O37vKek9uod3v7c5hO867KDdbQ2PeRDiCAAAIIIIAAAsUQIMBlodzT06OWlhZ1dW26mlBXV6fW1lZVVHAVIQtGiiZY4C2t9YLcE7pbPXrTk/iQ9tBFuifBKnQdAQQQQAABBBBwFyDAuVt5m1KbDaNtaAtbbdKsPjl+/HhxBS4LWIomTuAdve0FuZ9pttf3L2qeRmhk4hzoMAIIIIAAAgggkK0AAc5RzN4eaVZbrKmp8Y7yv2f+bYKbfXV2dvaWczwFxRBInMC3VKs39LI+qQu8Z+J4IYAAAggggAACCKQXIMA5zpCwABe8pbKxsdFbTp8XAgi4Cdyvmd7tlB/RYTpH33E7iFIIIIAAAggggECCBQhwjoMfFuDMoeY2yrlz54orbo6QFEPAJ/Bn/VY/0CSZlSkv1n36oHbFBwEEEEAAAQQQQCCNAAHOcXqkC3D+5+Icq6MYAgi8J3ClRnn/dYquULXqcEEAAQQQQAABBBAgwOU/B9IFOFM7t07mb0wNyRTo1Ff0oh7VwTpJYzU9mQj0GgEEEEAAAQQQcBTgCpwjFAHOEYpiCGQp8KTu1WK1envBfUVd2kJbZVkDxRFAAAEEEEAAgeQIEOAcx9q/SXfwEBYvcUSkGAIhAuu0Rtfps95XztaN2kufwAkBBBBAAAEEEEAghQABLoepkS7M2erYBy4HWA5JrMAcna41Wq4jdK4+rUmJdaDjCCCAAAIIIIBAJgECXCahLL7uD3YEuCzgKJp4gYd0o36j72m4DtAEfS/xHgAggAACCCCAAAKpBAhwzA0EEOh3gZV6Wh3vbeR9ge7SUO3V722iAQgggAACCCCAQCkKEOAiHhW7LxxX4CKGpbqyF2jVkfq31utETVGNxpV9f+kgAggggAACCCCQiwABLhe1946xYS2sirq6OrW2tqqioiKPM3AoAskR+KFa9Iwe1H46Vmfom8npOD1FAAEEEEAAAQSyECDAOWKlW7jEhLXBgwdr0KBB7Afn6EkxBIICf9RPdK8u1zYapIvVpQptDxICCCCAAAIIIIBAQIAA5zglTICbMmWKpk2bpqqqqs2OMlfjzIsNvR1BKYZAQGCD/qU2HeO9e7pman8dhxECCCCAAAIIIIAAAS73OeC/ZbKzs1M1NTW9lRHgcnflSASsQLvO1So9o8N0uk5SEzAIIIAAAggggAACBLj854A/yLW1tam+vl4EuPxdqQGBX2uefqHvaCd9RBdqASAIIIAAAggggAACBLjo5kBwEZPGxkZuoYyOl5oSKPCyXtBcjfd6fp6+q910UAIV6DICCCCAAAIIIJBagGfgIpgdYVfkIqiWKhBIpMA3dbz+pX/qOE3UUWpIpAGdRgABBBBAAAEEUgkQ4CKcGwS5CDGpKrECXZqh3+s+7anD9XnNSawDHUcAAQQQQAABBMIECHAFmBcmyHV3d6u9vV2VlZUFOANVIlC+As/rV5qvydpCW3jbCeygXcq3s/QMAQQQQAABBBDIUoAAlyUYxRFAoPACV2qUd5L/0v9opGoLf0LOgAACCCCAAAIIxESAAOc4UKk28q6uruZKm6MhxRBwFbhdF+gvekLVqtUp+h/XwyiHAAIIIIAAAgiUvQABLochThXm/FUR7HKA5RAE3hP4nTr1U12n7bWzLtViSQOwQQABBBBAAAEEEDCfijZu3LgRiWgE/MGOABeNKbUkU+Cf+ptu/P83UJrX2bpJe6kmmRD0GgEEEEAAAQQQCAgQ4HKYEsH930wVdXV1am1tVUVFRQ41cggCCAQFbtApWqtVOlJf1Kd0IUAIIIAAAggggAACXIHLbg4sW7ZMDQ0Nqq2t3WzD7gULFqi5uVltbW2qr6/PrmJKI4DAZgIPaJa6dae3mbfZ1JsXAggggAACCCCAALdQZjUHzJU382pqago9rqenRy0tLRo2bFjKMlmdkMIIJFjgL3pct2uiJzBRd2uI9kywBl1HAAEEEEAAAQQ2CXALpeNMsM+3jRs3Lu0VNlvOhLyaGp7bceSlGAKhAtN1uDbqXZ2kJh2m01FCAAEEEEAAAQQSL0CAc5wC2QSzTFfqHE9JMQQSLzBfl+l5/Vr76zidrk1XwHkhgAACCCCAAAJJFiDAOY5+NgHOPA83f/589odztKUYAqkEntKPtEhXaRttr69okQZqO7AQQAABBBBAAIFECxDgHIc/mwDX3d0tcxWuvb1dlZWVjmegGAIIBAX+pbX6pj7jvX2GrtV++iRICCCAAAIIIIBAogUIcI7DH9y8e/jw4ero6FBVVdVmNRDgHFEphoCDwM06U6/oRR2uM/RZTXU4giIIIIAAAggggED5ChDgshzbYJCzh/sDHQEuS1SKI5BG4Oeao0fUoaHaSxfoLqwQQAABBBBAAIFECxDg8hz+VIGuurqaWyjztOVwBIzAKv1J7fqChzFB39NwHQAMAggggAACCCCQWAECXMRDbwOdqZZn4CLGpbrECrTpGG3Qv/RpTdIROjexDnQcAQQQQAABBBAgwDEHEECg5AXu1df0Rz2gvfRxna1vl3x7aSACCCCAAAIIIFAoAQJcoWSpFwEEIhP4kx7UPWrRltpKF6tL22toZHVTEQIIIIAAAgggECcBAlycRou2IpBQgXf0H12lj3u9H6vpOlgnJVSCbiOAAAIIIIBA0gUIcEmfAfQfgZgIdKhBK/UHVatOp+iKmLSaZiKAAAIIIIAAAtEKEOCi9aQ2BBAokMCjuk0P69v6oHbVJeoq0FmoFgEEEEAAAQQQKG0BAlyW47Ns2TI1NDRoyJAhrDKZpR3FEchHYI2Wa45O96o4R9/RR3RYPtVxLAIIIIAAAgggEEsBAlwWw2bD26RJk1RfX997ZE9Pj1paWtTV1SX/ht5ZVE1RBBBwELhOn9U6rdFRatBxmuhwBEUQQAABBBBAAIHyEiDAZTGeM2fO1OrVq9Xa2qqKigrvSBvehg0bpqamJnV3d2vq1Knq6OhQVVVVFrVTFAEEMgn8WK1aonu1uw5WgzoyFefrCCCAAAIIIIBA2QkQ4ByH1G7QPW7cuD5X38xVORPcTLizgc38t3mZ93khgEB0Ai/oEd2pS70KL9I9+pD2iK5yakIAAQQQQAABBGIgQIBzHCQb4Ewoq6mp6T0qLKwtWLBA8+fP5xk5R1uKIZCNwJUa5RU/Wc0apdOyOZSyCCCAAAIIIIBA7AUIcI5DGBbgUoU6cxulCXbt7e2qrKx0PAPFEEDAReAHukh/1u90gD6t09TmcghlEEAAAQQQQACBshEgwDkOZfBZN3OYudL22GOP9Xkmzr7PFThHWIohkKXA47pbP9FMVWgHXaofa2ttk2UNFEcAAQQQQAABBOIrQIDLYuz8V9bMYRMmTFDwmTjzPs/AZYFKUQSyFFinV3WdTvKOOkOztJ+OybIGiiOAAAIIIIAAAvEVIMBlOXbmqltzc7N3VFtbW58FTcx7JuSNHz9enZ2dfZ6Vy/I0FEcAgTQC39apek0rVKMzdaImY4UAAggggAACCCRGgAAX0VDb5+GWLl1KeIvIlGoQSCXwoL6l3+oH2ll768u6EygEEEAAAQQQQCAxAgS4xAw1HUWgfARe0pP6nhq9Dn1Jt2tXfbR8OkdPEEAAAQQQQACBNAIEOKYHAgjEUmCGPq539R99RhdrtM6JZR9oNAIIIIAAAgggkK0AAS5bsQzlzQImc+fOVXV1NdsIRGxLdQj4Be5Wk/5XP1eVRuss3QAOAggggAACCCCQCAECXB7DbMNaWBV1dXWbbS+Qx6k4FAEEAgJLtUj36UptqQ/oUi3WdtoRIwQQQAABBBBAoOwFCHCOQ+xfpCR4iAlrgwcP1qBBg9TU1ORYI8UQQCAfgX+rR606yqviVM3QQfpsPtVxLAIIIIAAAgggEAsBApzjMJkAN2XKFE2bNk1VVVWbHcXeb46QFEMgQoFbdLZW6zkdolNUp8sjrJmqEEAAAQQQQACB0hQgwGUxLv5bJoP7vBHgsoCkKAIRCfxSt+hXukWVGq6LdV9EtVINAggggAACCCBQugIEuBzGxh/k7GbeBLgcIDkEgTwFzNU3cxXOvM7VXH1Yh+ZZI4cjgAACCCCAAAKlLUCAy2N8gouYNDY28gxcHp4cikAuAt/QcVqvN3WMvqRj39sbLpd6OAYBBBBAAAEEEIiDAAEuglEKuyIXQbVUgQACDgI/0tf1tH6sEarWF9XucARFEEAAAQQQQACB+AoQ4CIcO4JchJhUhYCjwLN6WAv0Va/0xfqRKrWb45EUQwABBBBAAAEE4idAgMthzML2f/Pv+2a+3t3dzUbeOdhyCAK5CFypUd5htWrRoTo1lyo4BgEEEEAAAQQQiIUAAS6LYVq2bJkaGhpUW1u72bNuCxYsUHNzs+yiJllUS1EEEMhT4Dadr7/qKR2g43WaWvOsjcMRQAABBBBAAIHSFSDAZTE2mVaa7OnpUUtLi4YNG8ZiJlm4UhSBfAUe0/f1M83WdtpRl+rH2lJb51slxyOAAAIIIIAAAiUpQIBzHBazkfeECRM0btw41dfXpzzKlmtqalJNTY1j7RRDAIF8BNZqlW7QKV4VZ+p67aOj8qmOYxFAAAEEEEAAgZIVIMA5Dk02wSzTlTrHU1IMAQSyELheJ+tNvaKPa7xO0GVZHElRBBBAAAEEEEAgPgIEOMexyibAmefh5s+fzyImjrYUQyAKgfv1DT2hBdpF+6hRnVFUSR0IIIAAAggggEDJCRDgHIckmwBnVqA0V+Ha29tVWVnpeAaKIYBAPgLL9Jju0MVeFSbAmSDHCwEEEEAAAQQQKDcBApzjiNoAt3TpUu+I4cOHq6OjQ1VVVZvVQIBzRKUYAhEL2O0EjtdX9AmdHXHtVIcAAggggAACCPS/AAEuyzEIBjl7uD/QEeCyRKU4AhEJdOoSvajfaB8dqTP1rYhqpRoEEEAAAQQQQKB0BAhweY5FqkBXXV2dqFsolyxZomnTpvVqHn744br88su1zTbb9L5nrCZPnqyVK1d675n99C655JI8R4DDEXhfYInu0Y91jbbSQF2mn6hC28ODAAIIIIAAAgiUlQABLuLhtIHOVJuUZ+BseLvmmms0atQorV+/XjNmzPBkbYiz4W3MmDEaO3asgv+OeBioLqEC6/WmvqHjvN6fqqt1kE5IqATdRgABBBBAAIFyFSDAlevIFrFfs2fP9s7mv5q2fPlybzPz5uZmL9QtXLhQixYt0qxZs3oXdjHBb86cOX3eK2KzOVWZCtykev1Df9HHNFZj9N9l2ku6hQACCCCAAAJJFSDARTzyZvXJuXPnKmm3UAYZ7RW2iRMnegEuLOQFy0Q8FFSXUIGHdIN+o9u1o3bXJC1MqALdRgABBBBAAIFyFSDA5TGyNqyFVVFXV6fW1lZVVFTkcYb4HmqurrW1tXnbKey6667eLZUmyJnbJ+2L2yjjO76l3PK/6Y+apy96Tfyi2jVC1aXcXNqGAAIIIIAAAghkJUCAc+RKtViJOdyEtcGDB2vQoEHebYNJfwWfgTMeBLikz4ri9r9VR+jf2qBjdb6O0fnFPTlnQwABBBBAAAEECihAgHPENQFuypQp3kqLYXu/mStN5kWAk3e75COPPOJdfdtzzz17FzUp1BW4devWafXq1Y4jSbEkCPxm2A1aMfh32rlnfx238vIkdJk+IoAAAghI2mOPPTRw4EAsEChrAQJcFsPrv2Wys7NTNTU1vUcT4DZRBMObec9ekStkgNuwYUMWI0nRchd4fuDDemjwtV43z/nn9zT43Z3Lvcv0DwEEEEBA8u6IIsAxFcpdgACXwwj7g5x5zqu+vt672mReSb4CFxbeLC+LmOQw0TgkD4GNulKHeceblSjNipS8EEAAAQQQQACBchAgwOUxisFFTBobGxMb4NKFN0PMNgJ5TDQOzUngVp2jv+tZHagT9DldnVMdHIQAAggggAACCJSaAAEughEJuyIXQbWxqcKEszvuuKP3mbewhtsVJ0eOHOntF8cKlLEZ3tg29Ndq1y90swbpQ5qsByQNiG1faDgCCCCAAAIIIGAFCHARzoUkBjkbxFauXBkqafaBs1sHBMvW1tb22fw7wqGgKgT0D72km3SaJ3GWblCVRqOCAAIIIIAAAgjEXoAAV4AhNEGuu7tb7e3tqqysLMAZqBIBBFwErtVn9JbWarQ+r8/oEpdDKIMAAggggAACCJS0AAHOcXjsPnAjRoxI9AbdjlwUQ6AkBLo0Xb9Xl4ZpP52vH5REm2gEAggggAACCCCQjwABzlHPv5F3dXV12qtrCxYs0Pz587kC52hLMQQKJfC8fqX5muxVf4Hu0lDtVahTUS8CCCCAAAIIIFAUAQKcI7MNcOedd57mzZunNWvWqKOjI3RTbwKcIyrFECiCwJUa5Z3lBF2mj2t8Ec7IKRBAAAEEEEAAgcIJEOAcbW2AM/u8HXzwwWppaVFXV5eCG3qb6ghwjqgUQ6AIAt/Tl/WSlmgfHa0zdV0RzsgpEEAAAQQQQACBwgkQ4Bxt/QGupqbGO8quOhnc/40A54hKMQSKIPA7deqnuk4f0La6TD/RQG1XhLNyCgQQQAABBBBAoDACBDhH17AAZw41Ya25uVl1dXW9i5sQ4BxRKYZAEQT+T69plk7wznSartEB+kwRzsopEEAAAQQQQACBwggQ4BxdUwU4c/iyZcvU0NCgIUOGeAuXPPTQQyxi4uhKMQSKITBbdXpdf9ehOlW1ainGKTkHAggggAACCCBQEAECnCNrugBnqrBfN4ubHHvssXrmmWdYhdLRlmIIFFrgAX1T3ZqvD2mELtK9hT4d9SOAAAIIIIAAAgUTIMBFSNvT09O7uEmmrQYiPC1VIYBABoGX9JS+p/O9Uufpu9pNB2GGAAIIIIAAAgjEUoAAV4BhM4ubdHd3cwWuALZUiUCuAtN1mDZqoz6pL+toTci1Go5DAAEEEEAAAQT6VYAA58jv38jbfwhX2hwBKYZAPwvcqcv0gn6tj2iUztHN/dwaTo8AAggggAACCOQmQIDLwS1VmCPY5YDJIQgUSeApLdQiXe2d7VLdr+01tEhn5jQIIIAAAggggEB0AgS46Cx7FzJZunSpuDIXISxVIRCBwH/0tq7WaK+mOl2hQ1QXQa1UgQACCCCAAAIIFFeAAFdcb86GAAL9KPAdjdOrWqaD9Fmdqhn92BJOjQACCCCAAAII5CZAgMvNLe1RZgETs5CJ2ROusrKyAGegSgQQyEXg55qjR9ShwRqiy/STXKrgGAQQQAABBBBAoF8FCHAF4CfAFQCVKhGIQOAVvaibdaZX0+d1k/ZUTQS1UgUCCCCAAAIIIFA8AQJcAawJcAVApUoEIhJo09HaoLd0hM7Rp3VxRLVSDQIIIIAAAgggUBwBAlwBnAlwBUClSgQiErhH/60/6acaro9qgm6PqFaqQQABBBBAAAEEiiNAgHN07unp0fr1652eaSPAOaJSDIF+EHhWD2mBmr0zX6R79CHt0Q+t4JQIIIAAAggggEBuAgQ4R7dUe78NHz5cHR0dqqqq6q2JAOeISjEE+kngSo3yznyiJqvmvWfi+qkpnBYBBBBAAAEEEMhKgACXFdemwmzknQMahyBQQgLz1KC/6Q/aV8donGaVUMtoCgIIIIAAAgggkF6AABfhDPEHOzbyjhCWqhCIWOBRfVcP6yYN1HaarJ9qa20T8RmoDgEEEEAAAQQQKIwAAa4wrtSKAAIlLNXkOHQAACAASURBVPCGXta3VOu18HTN1P46roRbS9MQQAABBBBAAIH3BQhwzAYEEEikwHX//wm4dfqHDlO9TtJXE2lApxFAAAEEEEAgfgIEuIjHbObMmZo7d664hTJiWKpDIGKBRbpaT2mhdtKHdaF+GHHtVIcAAggggAACCBRGgACXh6sNa2FV1NXVqbW1VRUVFXmcgUMRQKBQAsvVre/rQq/6L+l27aqPFupU1IsAAggggAACCEQmQIBzpEy38qQJa4MHD9agQYPU1NTkWCPFEECgvwXsdgLHaaKOUkN/N4fzI4AAAggggAACGQUIcBmJNhUwAW7KlCmaNm1anz3f7OHmapx5EeAcQSmGQAkImCtw5krcR3S4ztGcEmgRTUAAAQQQQAABBNILEOCymCH+WyY7OztVU1PTezQBLgtIiiJQIgKP6279RDM1QFvoMj2gQdqxRFpGMxBAAAEEEEAAgXABAlwOM8Mf5Nra2lRfXy8CXA6QHIJAPwts0L/UpmO8VvyXrtRIndzPLeL0CCCAAAIIIIBAegECXB4zJLiISWNjI7dQ5uHJoQj0h8C3dape0wodrJM0VtP7owmcEwEEEEAAAQQQcBYgwDlTpS4YdkUugmqpAgEEiiDwoK7Xb3WHttdQXar7i3BGToEAAggggAACCOQuQIBztLOrUJpFSvzPvvkPJ8g5YlIMgRIS+Lue1a06x2vRubpZH9aoEmodTUEAAQQQQAABBPoKEOAcZ0RYgLPvjRs3znsOzr5MkOvu7lZ7e7sqKysdz0AxBBDoL4EZqtG7ekdH6gv6lC7qr2ZwXgQQQAABBBBAIKMAAS4j0aYC2QQ4E95MiCPAOeJSDIF+FrhLU/WcfqHddKDO02393BpOjwACCCCAAAIIpBYgwDnODgKcIxTFEIihwB90vxbqCq/lF+tHqtRuMewFTUYAAQQQQACBJAgQ4BxHmQDnCEUxBGIqcOV7z76dpCYdptNj2guajQACCCCAAALlLkCAcxxhApwjFMUQiKnALTpLq/W89tMndYaujWkvaDYCCCCAAAIIlLsAAc5xhAlwjlAUQyCmAr/UXP1Kt2obDdYUPagttXVMe0KzEUAAAQQQQKCcBQhwjqNLgHOEohgCMRUwm3mbTb3N60xdp310dEx7QrMRQAABBBBAoJwFCHCOo2sD3NKlSzc7oq2trc82AqxC6YhKMQRKTOAb+qTWa50O1+n6rJpKrHU0BwEEEEAAAQQQkAhwOcyCdGHOVlddXc02AjnYcggC/SlgVqI0K1IO1Z66QHf3Z1M4NwIIIIAAAgggECpAgItwYviDHQEuQliqQqBIAi/oEd2pS72zNapTu2ifIp2Z0yCAAAIIIIAAAm4CBDg3J0ohgEBCBOx2Ap/SRTpSX0hIr+kmAggggAACCMRFgACXxUj19PSopaVFXV1d3lF1dXVqbW1VRUVFFrVQFAEESlngNn1Jf9Xvtadq9HndVMpNpW0IIIAAAgggkEABAlwWgz5z5kytXr26N7SZf5sFS9rb21VZWenVZP49fvx4cQtlFrAURaCEBH6rO/SgrtcW2lJT9DNVaPsSah1NQQABBBBAAIGkCxDgHGdAum0Empo2rVZngpt9dXZ2qqamxrF2iiGAQKkIvKXXda0+7TXnVF2lg3RiqTSNdiCAAAIIIIAAAqxC6ToHwgJc8JbKxsZG2TDnWi/lEECg9ASu10l6U6+qWmN0ir5eeg2kRQgggAACCCCQWAGuwDkOfViAM4ea2yjnzp0rrrg5QlIMgRgI/FhtWqIfagftoq9ocQxaTBMRQAABBBBAICkCBDjHkU4X4PzPxTlWRzEEEChhgZV6Wh06z2vhF3Sr9tAhJdxamoYAAggggAACSRIgwDmOdroAZ6rg1klHSIohEBMBu53AUWrQcZoYk1bTTAQQQAABBBAodwECnOMIE+AcoSiGQJkIdOpivajHtJsO1nnqKJNe0Q0EEEAAAQQQiLsAAc5xBG2AW7p06WZHsHiJIyLFEIiRwO/VpS5N91r8FS3SDhoWo9bTVAQQQAABBBAoVwECXA4jmy7M2erYBy4HWA5BoIQE3tU7mqFNW4HUqkWH6tQSah1NQQABBBBAAIGkChDgIhx5f7AjwEUIS1UI9JPATTpN/9BL+qg+pXp9o59awWkRQAABBBBAAIH3BQhwzAYEEEAghcBDukG/0e2q0Paaqoc0QFtghQACCCCAAAII9KsAAa5f+Tk5AgiUssAaLdccne418SzdoCqNLuXm0jYEEEAAAQQQSIAAAS4Bg0wXEUAgd4GrNVr/0duq0TidqCm5V8SRCCCAAAIIIIBABAIEuAgQqQIBBMpXYIGa9awe0s6q0pc1v3w7Ss8QQAABBBBAIBYCBLhYDBONRACB/hL4X/1cd6vJO/0FuktDtVd/NYXzIoAAAggggAACIsBlOQmWLVumhoYGDRkyRO3t7aqsrMyyBoojgEDcBK7UKK/Jx+sr+oTOjlvzaS8CCCCAAAIIlJEAAS6LwbThbdKkSaqvr+89sqenRy0tLerq6tLw4cPV0dGhqqqqLGqmKAIIlLLArTpHf9ez2suLbzeWclNpGwIIIIAAAgiUuQABLosBnjlzplavXq3W1lZVVFR4R9rwNmzYMDU1Nam7u1tTp04lxGXhSlEESl3gUX1XD+smbamtve0EBmq7Um8y7UMAAQQQQACBMhUgwDkOrN2ke9y4cX2uvpmrcia4mXBnr7qZ/zYv8z4vBBCIv8CbelXX6ySvI6fpGh2gz8S/U/QAAQQQQAABBGIpQIBzHDYb4Ewoq6mp6T0qLKwtWLBA8+fP5xk5R1uKIRAHgWv1ab2l13WI6lSnK+LQZNqIAAIIIIAAAmUoQIBzHNSwAJcq1JnbKE2wY5ETR1yKIRADgfs0XUvVpUrtqovVFYMW00QEEEAAAQQQKEcBApzjqAafdTOHmSttjz32WJ9n4uz7XIFzhKUYAjER+Iue0O26wGvtefqudtNBMWk5zUQAAQQQQACBchIgwGUxmv4ra+awCRMmKPhMnHmfZ+CyQKUoAjESsNsJHKPzdazOj1HLaSoCCCCAAAIIlIsAAS7LkTRX3Zqbm72j2tra+ixoYt4zIW/8+PHq7Ozs86xclqehOAIIlKDA99Sol/SkdtdINWheCbaQJiGAAAIIIIBAuQsQ4CIaYfs83NKlSwlvEZlSDQKlJvCEfqj71eY16zLdr8EaWmpNpD0IIIAAAgggUOYCBLgyH2C6hwAC0Qn8W+vVqiO9Cut0uQ7RKdFVTk0IIIAAAggggICDAAHOASmbIub5t7lz56q6uppVKLOBoywCMRGYrTq9rr97e8GZPeF4IYAAAggggAACxRQgwOWhbcNaWBV1dXWbrU6Zx6k4FAEESkTgAc1St+7UtvqgpuqhEmkVzUAAAQQQQACBpAgQ4BxH2v+MW/AQE9YGDx6sQYMGyWz0zQsBBMpX4GU9r7k6y+vg53WT9lRN+XaWniGAAAIIIIBAyQkQ4ByHxAS4KVOmaNq0aaqqqtrsKLYOeJ9k9uzZGjFihMaOHdvHyRhOnjxZK1eu9N6vra3VJZdc4jgCFEOgdATsdgKf0Fk6XpeWTsNoCQIIIIAAAgiUvQABLosh9t8yGdwmgAC3CXLhwoWaM2eOJk6c2CfA2fA2ZswY7/3gv7MYBooi0O8Cd+pSvaBHtLP20ZfV2e/toQEIIIAAAgggkBwBAlwOY+0PcnYvuKQHuPXr12vGjBl6/PHHPdFggDPBbtGiRZo1a5YqKyu9MkuWLPHCnv+9HIaDQxAousCf9KDuUYt33gv1Q+2kDxe9DZwQAQQQQAABBJIpQIDLY9yDi5g0NjYm8hk4G95Wr16tr33ta7rqqqtkr7RZXnNbpXn5b5m0V+FM2Bs1alQeI8GhCBRfwN5GeaImq0ZnFr8BnBEBBBBAAAEEEilAgItg2MOuyEVQbSyrCLs10gY8E9L8z8VxG2Ush5hGvyfwHZ2hV/Vn7a0jNF6b/kDBCwEEEEAAAQQQKLQAAS5CYYKcQp9tK3SA27BhQ4SjSFUIuAk8uuU8PbpVh7bSB3TJhp9oa23jdiClEEAAAQQKKjBw4MCC1k/lCPS3AAGuACNgglx3d3ciN/Lujytw69at0wsvvFCAkaRKBFIL9Ax8TQ8fuOk5uEP/3Khhr38MLgQQQACBfhY48MADRYDr50Hg9AUXIMA5Ett94Mzy+K2traqoqHA8MlnF+iPAJUuY3paSwDU6Wm/rLR2qU1X73qImpdQ+2oIAAggggAAC5SdAgHMcU/9G3tXV1Wmvri1YsEDz58/nCpxvHzgWMXGcaBSLlcA9+m/9ST/VjtpNk/SjWLWdxiKAAAIIIIBAPAUIcI7jZgPceeedp3nz5mnNmjXq6OgI3dSbADd5s1Uo2UbAcaJRLFYCy/SY7tDFXpu/pNu1qz4aq/bTWAQQQAABBBCInwABznHMbIBramrSwQcfrJaWFnV1dSm4obepjgC3eYCzt1aOHDnS20qAFSgdJx7FSl7AbifwSV2go3VeybeXBiKAAAIIIIBAvAUIcI7j5w9wNTU13lF21cng/m8EuM0DnPGyoW3lypWeX21tbZ994RyHgmIIlJTAPH1Rf9MfNUKH6Iu6taTaRmMQQAABBBBAoPwECHCOYxoW4MyhJqw1Nzerrq6ud3GTJAc4R06KIVA2At26Uw9oltefyXpQg7Rj2fSNjiCAAAIIIIBA6QkQ4BzHJFWAM4cvW7ZMDQ0NGjJkiLdwyUMPPZTYRUwcOSmGQNkI9OhNzdRxXn9O0f+oWrVl0zc6ggACCCCAAAKlJ0CAcxyTdAHOVGG/bhY3OfbYY/XMM88kchVKR06KIVBWArN0gv5Pr+lAnaDP6eqy6hudQQABBBBAAIHSEiDAOY5HpgBnqunp6eld3CTTVgOOp6UYAgjEQODHatMS/dC7fdLcRskLAQQQQAABBBAolAABzlE2LMDZ98aNG6f6+vremsziJt3d3VyBc7SlGAJxFzCLmJjFTMzrXN2sD2tU3LtE+xFAAAEEEECgRAUIcI4Dk02AM+HNhDjzPFxlZaXjGSiGAAJxFrDbCRyhc/Tp9/aGi3N/aDsCCCCAAAIIlKYAAc5xXAhwjlAUQyChAt/XRC3X49pF+6pRdyRUgW4jgAACCCCAQKEFCHCOwgQ4RyiKIZBQgae1WD/S/3i9n6SF2lG7J1SCbiOAAAIIIIBAIQUIcI66BDhHKIohkGABexvlSWrSYTo9wRJ0HQEEEEAAAQQKJUCAc5QlwDlCUQyBBAvcqLH6p1ZqHx2lM3V9giXoOgIIIIAAAggUSoAA5yhLgHOEohgCCRZ4WDfpUX1XH1CFmvRzbamtE6xB1xFAAAEEEECgEAIEOEdVG+CWLl262RFtbW19thFgFUpHVIohUGYCr+mv+rY+5/VqnK7Tvjq6zHpIdxBAAAEEEECgvwUIcDmMQLowZ6tjI+8cYDkEgTIQmK7DtVHv6jDV6yR9tQx6RBcQQAABBBBAoJQECHARjoY/2BHgIoSlKgRiJHC3mvS/+rl21AhN0r0xajlNRQABBBBAAIE4CBDg4jBKtBEBBGIj8Lx+rfm6zGtvozq1i/aJTdtpKAIIIIAAAgiUvgABrvTHiBYigEDMBOx2Ap/SRTpSX4hZ62kuAggggAACCJSyAAHOcXRSPffGrZKOgBRDIEECczVeL+sFfViH6lzNTVDP6SoCCCCAAAIIFFqAAJeDMIuY5IDGIQgkSOAx3a6f6Qavx1P1sLbVDgnqPV1FAAEEEEAAgUIKEOAi1GURkwgxqQqBGAv8S//UN3W814NTdZUO0okx7g1NRwABBBBAAIFSEiDAZTEawStv3D6ZBR5FEUiYQJuO1Qb9nw7WSRqr6QnrPd1FAAEEEEAAgUIJEOAcZXt6etTS0qJhw4apqanJO2rBggVqbm5WZ2enampqHGuiGAIIJEGgSzP0e92nwdpJl+mBJHSZPiKAAAIIIIBAEQQIcI7I9uqbCW/+sDZz5kytXr1ara2tqqiocKyNYgggUO4Cf9VTuk3ne938om7VCB1S7l2mfwgggAACCCBQBAECnCNyqgDX3d0tE+La29tVWVnpWBvFEEAgCQJ2O4Gj1KDjNDEJXaaPCCCAAAIIIFBgAQKcI3C6ADd16lR1dHSoqqrKsTaKIYBAEgS+qwlaoaUapv11vr6fhC7TRwQQQAABBBAosAABzhGYAOcIRTEEEOgVeEoLtUhXe//+ihZpBw1DBwEEEEAAAQQQyEuAAOfIF1yBsq6uznvu7Q9/+IO4AueISDEEEibwH72tqzXa63WtWnSoTk2YAN1FAAEEEEAAgagFCHBZitqVJ4OHsaVAlpAURyAhAtfpJK3Tq9pPx+oMfTMhvaabCCCAAAIIIFAoAQJcnrJmEZPx48dvVguBLk9YDkegTAQe1Lf0W/1AA7WtvqpfaoC2KJOe0Q0EEEAAAQQQ6A8BAlzE6suWLVNDQ4OGDBnCypQR21IdAnEUeEUv6mad6TV9vGZrbx0Rx27QZgQQQAABBBAoEQECnONA+J+BGz58OKtOOrpRDAEEJLudQI3G6URNgQQBBBBAAAEEEMhZgADnSGcD3Jo1a9Tc3Ky2tjatWrWq92jz7/r6esfaKIYAAkkSuFOX6gU9og9pD12ke5LUdfqKAAIIIIAAAhELEOCyBLXPvNnAxqImWQJSHIEECjyrh7RAzV7PL9BdGqq9EqhAlxFAAAEEEEAgCgECXI6KNrgFr7zxDFyOoByGQJkL2NsoP6NLNFqfL/Pe0j0EEEAAAQQQKJQAAS5P2ZkzZ2ru3Lnq7OxUTU1NnrVxOAIIlKvAt/U5vaa/ak8drs9rTrl2k34hgAACCCCAQIEFCHARAPf09KilpUVPPvkki5tE4EkVCJSjwK81T7/QdzRAA9SkX2gbDSrHbtInBBBAAAEEECiwAAEuQmAb5FasWMEWAhG6UhUC5SDwhl7Wt1TrdeU0XaMD9Jly6BZ9QAABBBBAAIEiCxDgCgBuVqycPn26rrjiClVWVhbgDFSJAAJxFLhao/Ufva1q1ekUXRHHLtBmBBBAAAEEEOhnAQJcBANgn4PzV8VecRHAUgUCZSawUFfoD7pfgzVUl+n+Musd3UEAAQQQQACBYggQ4LJQ9m/m7T+srq5Ora2tqqioyKI2iiKAQNIElutxfV8TvW43qEO76+CkEdBfBBBAAAEEEMhTgADnCOgPbwQ2RzSKIYDAZgJ2O4FjdL6O1fkIIYAAAggggAACWQkQ4LLgCrtV0n94dXU1i5dk4UlRBJIocKvO0d/1rIbrQE3QbUkkoM8IIIAAAgggkIcAAS4PPHOoXXmyq6urT02EuTxhORyBMhV4Qnfrfs30emeegzPPw/FCAAEEEEAAAQRcBQhwrlKUQwABBCIQeFtv6Rod7dVUp8t1iE6JoFaqQAABBBBAAIGkCBDgkjLS9BMBBEpG4Fp9Sm/pDX1Un1K9vlEy7aIhCCCAAAIIIFD6AgQ4xzFKtQIlt0o6AlIMAQR6BX6ia/W47tI2GqSv6pfIIIAAAggggAACzgIEOGeq9wumCnP+qgh2OcByCAIJETCLmJjFTMzr87pJe6omIT2nmwgggAACCCCQrwABLl9B3/H+YEeAixCWqhAoQwG7ncAndJaO16Vl2EO6hAACCCCAAAKFECDAFUC1u7tbZsuB9vZ2VVZWFuAMVIkAAnEX+IEu0p/1Ow3Rnpqou+PeHdqPAAIIIIAAAkUSIMAVAJoAVwBUqkSgzAT+qJ/oXl3u9epC/VA76cNl1kO6gwACCCCAAAKFECDAFUCVAFcAVKpEoAwF7G2UJ+gyfVzjy7CHdAkBBBBAAAEEohYgwEUtKokAVwBUqkSgDAVma4xe12pVabTO0g1l2EO6hAACCCCAAAJRCxDgHEV7enq0fv16p2faCHCOqBRDIOECv9DN+rXatYW2ULN+ra21TcJF6D4CCCCAAAIIZBIgwGUSeu/rqbYOGD58uDo6OlRVVdVbEwHOEZViCCRc4J9aqRs11lM4XTO1v45LuAjdRwABBBBAAIFMAgS4TEIhX2cfuBzQEnLIKz0bddtz/9baDRu9HlcOHKAv7Le1dq4YkBCBzN3EqK+RfQ7uYxqrMfrvPl+8/fl/69m173rvbbWF9NkRW+mIXbbMjJygEj/+63/0yOp3ent81LAtdfIeWyVIwK2rL77xru544d9a/x7VrtsN0MUHfcDt4ISUwshtoPkZ7uZEKQQKKUCAi1CXfeAixIxhVfaXWsVW8j4YBf8dwy5F3mSMNif9oabpGf1MO2gXfUWLewuY8Lb8zXd11j5ba+8dtlDw35EPTgwrNOHtt6+80xtsg/+OYZcK0mQbTPbcfguds+/WCv67ICeNWaUYuQ0YP8PdnCiFQKEFCHCFFqb+xAiEfXj8zcvv6Gcr/6PP7M6VEzMRMNr82+FFPapOfcX7woW6Rztpj9AP2PaD07BtB3gfwpP+SvUHkhv++LZHw9Wl92dIWPg334tPvPpO7x8Ikj6fMHKbAfwMd3OiFAKFFiDAFVqY+hMjYD4ArH5rY59bJvnQ3Xf4MQr/drC3UZqVKM2KlKmCv/U7d7+ttUvCb8u1V0wOG9r3lkmCyeZzLCzUpvJLzA/sQEcxcht5foa7OVEKgUILEOAKLUz9iREI+wDAbZR9hx+j8G+HmzVer+gFnaqrdJBO9K5Uhl0d4TbK9/1ShVxuo+w7x1L9EYnbKN93wsj91zQ/w92tKIlAIQUIcIXUpe5ECfCLLfNwYxRu9FvdoQd1vU7WNI3S5whwmadSyquUBDgCnMP06VOEAOcuxs9wdytKIlBIAQJcIXWpO1EC/GLLPNwYhRu9pdc1W3X6rKZopMbo/r++wxW4DNOJK3CZv99MCcJJZieMMhvZEvwMd7eiJAKFFCDAFVKXuhMlwC+2zMONUWqjVh2pE3SZPqb/0v1/fZcAR4DL/A3lUIJwkhkJo8xGBDh3I0oiUAwBAlwxlDlHIgR4uDvzMGOU2mixWrWjdtfhOkPdL28ZunppmF9m9fIswSIm7uPKAh2ZrTDKbGRK8DPczYlSCBRagABXaGHqT4wAyytnHmqMUhut0NN6Rg/qk2rUyjcGeZsu2327zFGsaNrXjm0EMn+/2RIskZ/ZCqPMRqYEP8PdnCiFQKEFCHCFFqb+xAjYD5Smw1/Yb9M+Xbc992/Zjb0TA5GmoxilnwUdOk8nq1k7a2/vL90vvPFu7ybVrEC5uZ35MPnI6nd01LBNWwmwgEn4/LJXK3fcZoC3Px4rUG7uhJHbbyh+hrs5UQqBQgsQ4AotTP2JErC/3NZu2Oj1u3LggD77wiUKI0VnMUo9C27T+dpXR+sw1WsrDfRC3LNr3/UO2GoL9YY55tH7AjbE2XdsmMOor4ANKOvf2fT+rtttCnO83hfAyG028DPczYlSCBRSgABXSF3qRgABBLIQ+L269Ijm6ZP6sg7UiRqgAVkcTVEEEEAAAQQQSIIAAS4Jo0wfEUAgFgIb9a6m63CvrefqZn1Yo2LRbhqJAAIIIIAAAsUTIMAVz5ozJUhg3bp1uvPOO3X++ecnqNfZd/WWW27RmWeeqcGDB2d/cJkeMUsn6P/0mvbRUTpT13u9XLx4sfbee2/tu+++Zdrr/Lv1q1/9Stttt51GjSL0ptN8/vnn9eKLL6q2tjZ/9DKt4e9//7vMfDI/m3ilF+BnODMEgf4RIMD1jztnLXMBApzbAPPLf3Onh3SDfqPbtaW20jQ9oi21NQHOYToR4ByQJBHgMjsR4DIb2RL8DHe3oiQCUQoQ4KLUpC4E3hMgwLlNBX75b+60Rss1R6d7Xxin67xFTbgCl3k+EeAyG5kSBLjMTgS4zEYEOHcjSiJQCAECXCFUqTPxAgQ4tylAgAt3uvK9Z99G6TRvWwECXOb5RIDLbESAczMiwLk5mVL8DHe3oiQCUQoQ4KLUpC4E3hMgwLlNBX75hzvdpSl6Tr9UpYbrYt1HgHOYTgQ4BySuwDkhEeCcmLxC/Ax3t6IkAlEKEOCi1KQuBAhwWc0BfvmHcz2nX+guTfW+2KhOLVn8AouYZJhZBDi3bz1uoczsRIDLbGRL8DPc3YqSCEQpQICLUpO6ECDAZTUH+OWfmsveRvkpXaTXF+9EgCPAZfW9laowAS4zIwEusxEBzt2IkggUQoAAVwhV6ky8ALdQuk0BAlxqpzmq1xr9RXvoY9ppcR0BjgDn9k2VoRQBLjMjAS6zEQHO3YiSCBRCgABXCFXqDBVYu3atJk+erJUrV3pfN/sQXXLJJWWpRYBzG1YCXGqnR3WbHta3vQKHLP6a9t17f/aBSzOtuIXS7XuOAJfZiQCX2YgA525ESQQKIUCAK4QqdW4mYMPbmDFjNHbsWAX/XW5kBDi3ESXApXZapzW6Tp/1Cuy7+GIdsncNAY4A5/aNlaYUAS4zIQEusxEBzt2IkggUQoAAVwhV6txMYOHChVq0aJFmzZqlyspK7+tLlizRnDlz+rxXLnQEOLeRJMCld5quw7VR72qPxefqE3sfT4AjwLl9YxHg8nIiwLnz8TPc3YqSCEQpQICLUpO6UgrMnj3b+5r/lkl7FW7ixIkaNWpUTnqNj+V0WMEP+sDb63TQC3fqyQPPL/i5Mp3ghTf6lthyC2mvwZmOKs7XD/3TLfrjPmfq7Q/0f4P+9pb01r/79nufHYrjkOosH6y6UtsOXaQdF43V6i1O0/9r725i7SruA4BPShvhJE7jOCR1HYUikLPhs0ZCcaWSKgmbOI6ERERYZAG0VCB2QIBFFkQBhKkUiUiRKWGRhUPDjnpRZVUvagUpRBQhIfOhVCDjtpEhwQQ7VRHV3GYec+fN+biP9+w77/yeFCn4no+Z3/zPufM/M2fuiW2fP6sF+q9TIfz2f+aLcOHHQzjnQ2e1WLOT/8Wxw+H3f/zRcPwza7uXrGcNFSuOQQAAGYdJREFUTv1vCK/9bv6In94Swic+vJ5nWduxtr95NHzqNy+FoxfsXdsB1mmvd98L4ZW35g/2px8O4TNb1ukEH+AwH/vd6+GCY4dn96Zl+HMPX1srHNiztv3sRaAFAQlcC63UeBlPnz4dvvvd786StDh9Mv2txzTKv/mXEE4Wne5l4Nr67snwzf/+SXh0x9lP4ELsXL+3DCqry/B3xx8NP/n0N8PJc85+AreMQp/61C/CF77w92H7oa+F58IXwi/fu2YZi7kUZfribw+Ht/7oo+GXW89+Aje73pYgqa01zK53jobPn34p/PMnz24CtxRB01GIHb9/PcR4ivempfhzD1+4GeLDt4N/vfBudiDQjIAErpmmaregG57AvfRMeO8/frFUQFv/JIRdHw/hmRNLVaylK8zu7SG8+NZyJuHLgrXvH/4xfOwXfxne3fp2OPX5F5elWEtXjmgU/96+8pdLV7ZlKtCWo7vCOSc/xqmnUT78+o4Q/yeW+iP3xCs7wul/2rt09/APXf61sOv8P5fALdONR1nWXUACt+6kDlgKnIkELvz7IfAENqXAX93zb+GTF76xKeumUgQItCtw4uj2cGT/Es5TvPpvJXDthpWSjxSQwI2EstnaBTY8gVvCKZRr19qAPZd4+s0G1HbTHXL79mc2XZ1UiACBBQSW+B5+4sTuBSpy5jY1hfLMWTvT2RGQwJ0d98mddaMWMfnX/5wc5cIVfv43q3e5+BMLH2bT78BpuIlrRnFBnC3nDO87pS1efyeEN4rFXmL9XXfvR8Gpd0N45aR705jrwr1pjNLqbb74Z2vbz14EWhCQwLXQSpugjFP7GYFN0GSqQIAAAQIECBAgsIQCErglbJTNWKS04uRll102+ymB9ViBcjM6qRMBAgQIECBAgACBPgEJnPg4YwIpaXvttddm59y7d+/c78KdsYI4EQECBAgQIECAAIFGBSRwjTacYhMgQIAAAQIECBAgMD0BCdz02lyNCRAgQIAAAQIECBBoVEAC12jDKTYBAgQIECBAgAABAtMTkMBNr83VmAABAgQIECBAgACBRgUkcI02nGITIECAAAECBAgQIDA9AQnc9NpcjQkQIECAAAECBAgQaFRAAtdowyn28gs8/fTT4Yknngj3339/2LJly/IX+AyW8Mknnwx33333yhlvueWWcNddd53BErRxqoceeigcOHBgVtidO3eGxx9/PFx00UVtFP4slPLll18ON954Y9i/f3+46qqrzkIJlveUeSylUu7bt8/9qWiy8t508OBBsfQHo/hTQDfffHN49tlnq4H+4IMPhuuuu255LwIlI7CJBCRwm6gxVWV5BFJHcvfu3TpIlQ7SI488spKMpE5B7HBL4t7Hih3J+ADgscceC9u2bQvxv3O35Yn25SjJqVOnwr333hueeuqpoNM93ybJZs+ePTrYPeFaXmPxIdwNN9wgngYu8fhwIFqle9Vy3BGUgsDmFpDAbe72VbuzIJA/wfWEe1xHMn7533nnnUaYiifd119//UqHWye8/2LOrzsJ3LxVekgSH5AYmazHUZdRTE7in4dLdTdJ7lnoZDglgRCCBE4YEFhHgdSJjB3Iw4cPh+PHjxuBG+EbRyxjByl2lkwR7O9g5kndCNpJbJLiJ07vitO4TKGcb/bo88ADD4SHH354Nprrb7VATETi/cco0vjoSA+VduzYIcEdz2ZLAusiIIFbF0YHIbBaIHYGJHDjIqOcLjhur2ltxaje3nkn8tprr/UOXIWpfK8rbmJ2wDxUNDpy5Mgsfm677bZw7Nix2QZGc7vvs2ZOTOs7SG2XS0ACt1ztoTSbSEACN64xvQPX75SmKMWtLBLQP3Jy4sQJCVwlnOK96NChQytTlFPSGze1yNL/g6VFXvLE1vTA7nuTGBr3/WYrAhslIIHbKFnHnbyABG44BFIn4NVXXzV1aYDLdKXVQOV7S1ahHL7m0has5q26FuJwH6/HlPgZf63ZksBGCEjgNkLVMQn84YmuKZTDT3Alb+MvF1OWVne647+kBSZ0KsfHUkp+vVP5/ghc7X5t6nI9priMv9ZsSWAjBCRwG6HqmAQkcL0xYORtbZeIBO59t6HfpPLbgv0xJoGb90nvwJVTSiUq9TgyMrm2e7i9CKyXgARuvSQdh0Ah4AuuHhKSt+FLpWskSWey384I3Gqfrqm3Vn6dt+rycB/vjim/Kzh8L7cFgY0SkMBtlKzjTl7AF3/3k1s/+jp8eZTv5KTk5Pbbb/djzB18Erg6TLkYh4WDxo0q5T8L4/fzVo9++13B4fu4LQhslIAEbqNkHXfyAhK41SGQOthpie5yC0t2z4uklfHSv/IxArfWG2u+mmk8himmdcn8Jxd27ty5snLnWt03434elGzGVlWn1gQkcK21mPISIECAAAECBAgQIDBZAQncZJtexQkQIECAAAECBAgQaE1AAtdaiykvAQIECBAgQIAAAQKTFZDATbbpVZwAAQIECBAgQIAAgdYEJHCttZjyEiBAgAABAgQIECAwWQEJ3GSbXsUJECBAgAABAgQIEGhNQALXWospLwECBAgQIECAAAECkxWQwE226VWcAAECBAgQIECAAIHWBCRwrbWY8hIgQIAAAQIECBAgMFkBCdxkm17FCRAgQIAAAQIECBBoTUAC11qLKS8BAgQIECBAgAABApMVkMBNtulVnAABAgQIECBAgACB1gQkcK21mPISIECAAAECBAgQIDBZAQncZJtexQkQIECAAAECBAgQaE1AAtdaiykvAQIECBAgQIAAAQKTFZDATbbpVZwAAQIECBAgQIAAgdYEJHCttZjyEiBAgAABAgQIECAwWQEJ3GSbXsUJECBAgAABAgQIEGhNQALXWospLwECBAgQIECAAAECkxWQwE226VWcAAECBAgQIECAAIHWBCRwrbWY8hIgQIAAAQIECBAgMFkBCdxkm17FCRAgQIAAAQIECBBoTUAC11qLKS8BAgQIECBAgAABApMVkMBNtulVnAABAgQIECBAgACB1gQkcK21mPISIECAAAECBAgQIDBZAQncZJtexQkQIECAAAECBAgQaE1AAtdaiykvAQIECBAgQIAAAQKTFZDATbbpVZwAAQIECBAgQIAAgdYEJHCttZjyEiBAgAABAgQIECAwWQEJ3GSbXsUJECBAgAABAgQIEGhNQALXWospLwECBAgQIECAAAECkxWQwE226VWcAAECBAgQIECAAIHWBCRwrbWY8hIgQIAAAQIECBAgMFkBCdxkm17FCRAgQIAAAQIECBBoTUAC11qLKS+BJRJ48803w8033zwr0WOPPRa2bdu2RKVTFAIECBAgQIDA5hOQwG2+NlUjAmdM4KGHHgoXXHBBuO66687YOZ2IAAECBAgQIDBlAQnclFtf3Ql8AIE4+vbGG2+ECy+88AMcxa4ECBAgQIAAAQKLCEjgFtGyLYEJCDz55JPh7rvvXlXTgwcPhquuumru3+O2TzzxRHX6ZBydO3DgQLjlllvCXXfdVZVL26QPL7/88tFTMU+dOhXuvffe8NRTT60cu+9cZb127twZHn/88XDRRRetqlOt/nGjsnxpCumzzz5brd+DDz44NzpZ1rfcKd++Vr99+/aF+++/P2zZsmVl15dffjnceOON4dixY6vKUJ4/blArc80tbfe5z31u1TnjcZ5++ulw5513rhjWylszy8tw/fXXr/iMtUn13bt3bzWuUkz+4Ac/CPGYeXzUGikaXXHFFZ2GtTrEut9www1zh6tdH123izHXRr7v2FgYa5jHQFe5k/Pu3bs7Yy6Wse8aqh071WXHjh2r2i+5jr0PlLFfi+OyrWrXRBmTMebz9q3tU7ZJuU35+Zg6xfaL5TUdfQJftKpI4AMKSOA+IKDdCWwmgdTpiHXKE4XUUdq/f/9cEhc7HMePH6928GLSdvHFF4eTJ0+u+ryrExePd+jQoWqnMHdOnbK809SVcHS9p5eOUXYyu+oUz19+1uUSty0/6+u4ljGUypwnOGn/V199da6D15VE18qWkthaotjV5l2d9C6L22+/fS5prZXvg9j0dfL7jGumyb1MRvuu6Vp9avHYdYxY975rY62xsEh8pTLEc337299e9WAmxfrPf/7z8OUvfznceuutc8VKbR9jMdalfLDTl6B2XTOp/Fu3bg3PP//87ForH67khSiPk9o3liU9MErxnq7xtE8Zo/G4cdsjR46EK6+8MnznO98JffuUsZTKvmfPnlns1+5FQ8lZKmvtIc1m+o5RFwIE1kdAArc+jo5CYFMI9HVyUwcnJXZdHcb83+P7cbURutjhjR2a8knzmE7oeiZNZTmGzp86vqlzOdTxj9vFv9ih7Os8lsFTWqfPa+3TlXCWncq+spbbpg5tbLsvfelL4e23354bLak5dR2/duxy20VsUn1jR/+SSy6ZSxb74rcvboY616V/LWnpG41O+4+5NtYaC4sYpriPI00p6SiTox//+Mez6dEf+chH5oxT28WHOdEtf8gQj5HaIMbNM888E+655565RKwrTpJftI0ju+XDotIlv7bSZ/l1c/r06dkCS2X5atdWsouJ2w9/+MPBfWrXXH7c+BCqvO8ll1rs5COJXSOEm+ILRiUIEFg3AQnculE6EIH2BbpGpWLNyuSlq7Ocd9BOnDgxN80uCfUlE13JS9x3KMEqPx/qVJd16EsAagZDx887mUPJXh49Q1NTU1JYG3Uok4XYQY+jKLXObN85Uyc1Ts/8/ve/Hx5++OGVVUa7Esmu6V9lm5b1G2uTt+/VV1896ySPGSlOCekjjzyyanR3KKZyo6EEMXbO+0aOxlwbtQSub5pyioWxhnH71LZxWl+ZoEWPBx54IHz1q18Njz766FwCllvFUaw4hbmcCpknyocPH161yFEttnPXMbHadact4yyaxL98hLB2/jwhe+6553r3ife0eE2UCWb+MOjcc88NP/3pT8M3vvGNlenOXbGT/v2mm24KP/rRj6ojmu1/s6gBAQLrLSCBW29RxyPQsEBf4lCOYNRGNMrOcJn0JZrUafn1r389OF0y5+wbRSnZx3bMyyQrvvvS9V5Q7hM7abEDG//K99Liv5UdtqFkr1bP8847r/d9mLGjkfHY+ftqtRDN2yq+H5g656mjHkcyUke4TPSHrMu6lyMYY23Kjv4dd9wxl2DE49SStDxpKdtqKGkvrdL0wEVHSsZeG+X5UhsPxcJYw3IU8Fe/+tXc6Gps25gwfv3rX59Z5qPkZZJYGwUrk9Q8ye6aol27rmojg3231jH3hlqcdt2j0rnGPhTqmlWQjlNLsPPR6TgaOnSNNvzVougECKyzgARunUEdjkCrAkOd8LKDVOssl52UvmlD5WIaYxaBGOok5fZ95863yzuhfZ3g8j2nvo5/7X21vgUmaguq5NOq+hZcqSUs5chc36hmssg7stu3b58bsYv75x390mmo85xvH88XRwPzd5XG2pTnifvlP2MxNJ20tnBG7ly7dmsLY+Tl7Vs4Jz/eItdGVxIXF6rpioWxhnncxqShlmDFZD2OnuXvt9buD7W4Kts6T7Jr10zXaG406Fr8KPeJ54uLDnW5lNd6+Y5tLQnt2id/sFGWre8aSzFWLryTx2tt2mWr3yXKTYDAxgtI4Dbe2BkINCEwNBJRPqkuO8u1Dl7t/acSI20TVwsc6oSNSURqCUnXYghl+cZ2guM5Fun4DyXHfQGSJ7rlSnZ95c1HiIY6qfH8eXJcThMbavuh6Xt5u8UkJJ+CtohNeZ5y2lptSl+s23otYFK2U0oe4r/3jcit9dooz9cVC4sY5m0Zjx/fdYvvqcWVTaNnTNxq0yNrbVx7oFLeF2rvgebTD2sPTRa5zpPR0HuM8fMyeRsafSv36YujrmusK3mrjUqPTVqb+EJRSAIENlRAArehvA5OoB2BRTrhsVZlZ7lvilBt5KMrkYv/XpuSGP99kY7dUOcsT8Jih/LSSy+tvtPTNSpXq29XB2+RBSa6IqYcVVuk0z4mgesbDcgT3TiKULZ918hXrEuZJJdui9iU58lHWXft2tX5nl9fbA91/MdcwX1TN1NyXE6PW6T9uhK5NIq5iGEezzFRj++7xfcb45Tg+P+/9a1vzU43JsnuGlXMFw7Jk7wXX3xxbppg1/Uydjpo7tI1Cpyc44Iq5U8edF0XXft0jer3vQ8cp2SXo7Sl25gHXWPi0DYECExHQAI3nbZWUwK9An2dprKD0bX4R9fvoeVLY/clE0NJ5NAUyvzzWNmhhTvyhKBr1box7+1s27ZtxbZ8P6yrA19rjKGOXNn5ri2mUDvuUIe4NjWx/HmI9G5UTETiCE3qpA8lImUiXZZlqM1TfbrOk6Z3XnvttdXFJVLiX1sIZKjsuWVf7PWNzJRThcv26Vo2fpFYKBOjvgu9jPn77rtvtmx+PEYcfYtTA8s2yUcaa8cul9zPR9hSPWK8xJ8dyNthkRHvodt3LblK/xb3LVe8HXo/t7ZPVwJXi+FkVhuZ7au3nxEYammfEyAQBSRw4oAAgcHVHcuRrzHvwyXWssPeN4o2lKCNeacuf/o/dK58wZJa4pXqUH7WldTF7WufDSVQeQj2Jbh5feJqeWMXPehzK8sby1KbihiPETv7X/nKV2bTBdNoRt/7b7UkpBxFG2vTN7qZlryP03BrP4LcNUI4NG04b5e+Ed0+377RuaG6j42Fse9PlQlrLHd6R+1nP/tZuOaaa2ZL/g+9s5hcSr+uZDwe75133gnxAU8aje8bNRy7OE++umR57+hL3mL5a7ZD+9TiuevdwPhe3tiFbsY+xPBVRYAAgSQggRMLBAj0/kZZ1zsqafGMtOBFvihFOXKRJxpd74SMnQaWnmzni56U0wvT+WuLicTPasfo60yXxx/q+Jed9r4phmX4lYuldCWRi079qx031SMuZpGmrQ695/PCCy+Et956ayVR6kpQah3bWjuNtembIhen/sUffz7//PNXTb/tio3oOrT4St42XbHUl8z3nTsee6jjvkgslCOmtdtaWd9Y9vSTAUePHp1NnyzbrS8BrW1bG+mM5/3e974XXnnlldnobfyx6774Hbq+ypip3Ttq77zl11Lt4UffPl0PpMp6dN3f+r5mFpka7uuKAAECUUACJw4IEJh1JONoVO2vawpQ6jDGRK5cHCA/Tq2TnDp+ccQk/Q0tYFI7ZlwQI/31rWJZTgErFwOJxxhKJPKObNy+b/pi3nFPC0LkdS2dy2lTtWl3eZkXmfqXn6t23LJ9+5KaFCfllNgDBw6sCp2+lTXTFLtaHHTZ9I04pvatrQg5pj59t4AyrmrTCbtGWoYSgjEJ5NhYGBNfNcMyAcmTpyuuuGIW5+XqiblXft3Ee0EtkczbOXrGv3i/6XIbE9/lNMTaA53alO74+3bx77Of/ezcKpd9U13L6zOPgb7Pyriq3aPG1NVXFAECBEoBCZyYIECAAAECBAgQIECAQCMCErhGGkoxCRAgQIAAAQIECBAgIIETAwQIECBAgAABAgQIEGhEQALXSEMpJgECBAgQIECAAAECBCRwYoAAAQIECBAgQIAAAQKNCEjgGmkoxSRAgAABAgQIECBAgIAETgwQIECAAAECBAgQIECgEQEJXCMNpZgECBAgQIAAAQIECBCQwIkBAgQIECBAgAABAgQINCIggWukoRSTAAECBAgQIECAAAECEjgxQIAAAQIECBAgQIAAgUYEJHCNNJRiEiBAgAABAgQIECBAQAInBggQIECAAAECBAgQINCIgASukYZSTAIECBAgQIAAAQIECEjgxAABAgQIECBAgAABAgQaEZDANdJQikmAAAECBAgQIECAAAEJnBggQIAAAQIECBAgQIBAIwISuEYaSjEJECBAgAABAgQIECAggRMDBAgQIECAAAECBAgQaERAAtdIQykmAQIECBAgQIAAAQIEJHBigAABAgQIECBAgAABAo0ISOAaaSjFJECAAAECBAgQIECAgARODBAgQIAAAQIECBAgQKARAQlcIw2lmAQIECBAgAABAgQIEJDAiQECBAgQIECAAAECBAg0IiCBa6ShFJMAAQIECBAgQIAAAQISODFAgAABAgQIECBAgACBRgQkcI00lGISIECAAAECBAgQIEBAAicGCBAgQIAAAQIECBAg0IiABK6RhlJMAgQIECBAgAABAgQISODEAAECBAgQIECAAAECBBoRkMA10lCKSYAAAQIECBAgQIAAAQmcGCBAgAABAgQIECBAgEAjAhK4RhpKMQkQIECAAAECBAgQICCBEwMECBAgQIAAAQIECBBoREAC10hDKSYBAgQIECBAgAABAgQkcGKAAAECBAgQIECAAAECjQhI4BppKMUkQIAAAQIECBAgQICABE4MECBAgAABAgQIECBAoBEBCVwjDaWYBAgQIECAAAECBAgQkMCJAQIECBAgQIAAAQIECDQiIIFrpKEUkwABAgQIECBAgAABAhI4MUCAAAECBAgQIECAAIFGBCRwjTSUYhIgQIAAAQIECBAgQEACJwYIECBAgAABAgQIECDQiIAErpGGUkwCBAgQIECAAAECBAhI4MQAAQIECBAgQIAAAQIEGhGQwDXSUIpJgAABAgQIECBAgAABCZwYIECAAAECBAgQIECAQCMCErhGGkoxCRAgQIAAAQIECBAgIIETAwQIECBAgAABAgQIEGhEQALXSEMpJgECBAgQIECAAAECBP4PZZsBZC3MDMIAAAAASUVORK5CYII=', `mes` = '8', `anio` = '2024', `tipo` = '9', `semana` = '0'
WHERE `id` = '282' 
 Execution Time:0.19221305847168

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '10'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.1885039806366

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAtoAAAJWCAYAAABmnCCQAAAAAXNSR0IArs4c6QAAIABJREFUeF7t3X/QZUV9J+BGUGd0SRhAIrKojDjobmKIzIKLWckm2UrtLmBhCkVI3C0kUkGRZIGpGcK4pYNAgVQKMZOCItSWUTShIqVgrclaiWwiK2S0iLFKURyjFGLGgVEwzOAvtvpS/XLeM+fce98ffc+5fZ73H5j3vbdP9/Pt+97P6dvnvAc89dRTTwVfBAgQIECAAAECBAisqsABgvaqemqMAAECBAgQIECAwEhA0DYRCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTIdx///3hTW96U/iHf/iHRo7zzz8//OEf/mH42Mc+Fn7rt36r8TG/8Ru/ET784Q+H7du3h3e9613hQx/6UDjnnHMWHtt0jPpj0oMfeeSR0XP/8i//cr92qgef1GZq5zvf+U74sz/7s3Dcccct6vu2bdtGfU3jW7t2bet0SI+tP+AXf/EXF7U9rWX9WNEu2raZxOOmx7znPe8JW7duXfh3/FnyP+yww0Zd/OxnPxt++Zd/efT/qY/x/9vqnNqcdJwmoNjnN7zhDeH3f//3w4033jh2fqT+xQdN479379792v27v/u78NrXvrbxOJP8q89tq2lquH6cSfNyOTUc14f63KrWpl7v+LNq/+o44+Z46ndTm9V2qnMqfn/S66Zeu6bHp2O3mad6/tEf/dGo5vX6VudtbKP+8/rrqW40bgypNuNek94/CBAoS0DQLquevRnNpHCy0qBdfzOtDrzpja76ht72RjhNm0888cQosDcF7eobblOgqRdn2lA2reVqB+3Y32owrPpME7Tj8+sBvinQr1bQnsa/KWS3BbL0/Un+1VpPW9PU9qR5mTto1z3GnQg01akpSE9qs2nsk16/6edNvtXXc9truD6Pr7322tHJbPxqOlFM87TtRCO11/bzSb+DBO3evFXpCIHsAoJ2duJhHiCFk9e85jWjleu2ld1pgkR9FagafNqCYPX76Y3/n/7pn8JLX/rS0QppPVBM2+YrXvGK1qBdXR2Oq9r1lbG2oF3vS31V9lvf+tYoDEyyrLc/jW3binY8XgwiKRDUw1M9aNf7lgJkCmKf+tSnRqvr9aC91MBRrVPddxr/9PwXvvCFo9X7uBqevNvqNW4u1+dm+ve4FfJUp2nm5XJqWJ8H1TBYD4CpTpdeemn4q7/6q/3mWHpubDN5xf8ftxI/qc16aE5zIDnHnzd9WlT/NOnwww9f9Fp88YtfvPBJRfJvqm383kMPPbToE7VU+0nzNv08OX7hC18YfcqT/p1eq/Ux1F8/S533w3wXMWoCZQgI2mXUsXejyBm0x4WP+Ca6fv36xi0mv/mbvxne+MY3jkJr/P+4VSJ9Tdtm29aRami6+eabwxVXXBFisK+Gk2mDdmrrc5/73KJVt1kG7XrwSgHila985WgYX/7yl8f2re60Y8eOFQftcYFxWv+2E6pxL6Bxc7l+orKUoJ3aXe68rM/dphOFasBrWn2O/f2Lv/iLUS3//M//fOH/05aotqAdj93mMqnNetCetF2krTb1vu3evXv02q6eRKVgnGySx1FHHRUuueSS/YJ5vc20ba2+gp1OPOrb2uqv3eSY5kn9BLZ3v7h1iACBVRcQtFedVIPVN+G2PdppRWfcdo36qlR6zlLCTHpDjKupsb1Xv/rVozfXFGLTG+G0bbYF7Wpoqu51HrdyNe6Y1Z/Flbtx+93bjrGc1dD0nLjqH1fr0snCV77yldHKXfr+pJOASSuD4+retO1mUmCc1r9tZb6+1776Km4LlNW26nOz6bdAPegmg3Hzcjk1rB47zaMm0zSX46c88VOntDpbDezjgnbTz6ZpM/Wvvj970idA1XFVnzvu04P6a6y+P7tep2owT0G8+ruiLUindprmSnVuxkWASddNeAchQKAsAUG7rHr2ZjST9rUuN2hXL5Cb5uP5pjCQ3kybtkVMarMtaNff0OuBo2nrzDRBO/Zx48aNMw/a8bg7d+4cXdgZTf76r/969P9xe0FcAa0H7bYTquXs0W4KheMCY5z0S/Fv2lc77qK9SXO5+txxe7Srj5tmXlZPEpdyQWt9pTv+u2le10N89VOB9EnMNEG7er3CNG1Wf0k1nXBNs62i+rxJ11xUfx5f+29/+9sbt6bUt66krSjTBu3qvEreddO0hWqaMfbml7mOECCwIgFBe0V8ntwmkGvryFKDdn3VrNrf9AYcv5fubrGcoD3uzgzjLopcatCe5daRGATiSmd1FTuubqdtMdME7WqYaNsLPk3gqIaqpvos13/aO16MC9r1VdhpPxmZZl7Gk7PlrmhX228yHndRaDWYLyVoV/dIN/1eaHttLfUuME0r9vUxJrf6CVSsT7Spb+lq2lLUtHrdtqLdtme9+qlFvMPJNPX0rkKAQFkCgnZZ9ezNaHIF7XjHj3poq7/xVvdoT3sXiGnbbFrRHheaYt/aPhJvC2XV4BHDSdo6MuugXV9Jjycm733ve8Mf/MEf7LeiPalvyw3a1QDUFsqX65/mTX2bS/V2gfEx9blcPTFrCnLpU4C22wVWV+DbXrAplE4TzOq21dA36QLPSbffTHfZSavrVZv6haVpj/SkNtsujG7ailP1qV+IGNup78GufgpQr011f3b1+oxx+/bbPimperSF7EknM8vdm96bX/I6QoDAVAKC9lRMHrRUgZxBu+2Nsb5vM90hpCkk1N+gl9pm9ePytsA8bjWwGrbqK30pONXvZDApzNZrtJyQVn1O/T7W1XtbT9qj3daXpdx1ZJrAOM6xyb/phKrpTiTV/jfN5WqIqgamaVa0x82L+rxcag2rF/hNc5/r+slLfcU23dmj/hpqCsVtfW1aBZ60BavppKrpriT1u74kv3F70n/7t3974WLpcXdPqYb2+l1Jmj4Nq5/UCNpLfdfweAJlCgjaZda181FN2tea3gjT3SimuWiwaStC00Dr9+huWtVreqMfd4FearN+H+14/PqdDqp9qgeBpp9NWtWc1rJ+Qd80FxxOuhtI9ROBpov20vgnnQS0rWi3jT16n3TSSeHcc89tnctxDl199dVh8+bNi+40Mc5/KdtAUjttJ41Nq+2TPkGJczFdEDfNvEz1aUKov4Zie7/6q7+68EeF2uA++clPhve///2N94Kvh8sLLrhg4Q89NbWXVo3jz9ruL98UWKsnSPV2p9luVX1OvQ/xj1LVv5JNfX92W71Sm22r9PVPHKYdwzQnTp3/8tYBAgRWVUDQXlVOjdXDSdvHyCsN2vE4TaFpKRc4NoXgcW3GY066bV19BjR93J0eM+lNPn1M32XQrm+reN7znrfori1dB+14B4d4K8K2LRJN/k17usfd8WLcpzMpOKX5HG+RF7eOtH3FLQu7du1qvJd7fV7EuRy/2v5y6nKD9h//8R+H3/3d3239K4zVFf4bbrghXHjhhaO/qNoUXtMWjHHzvPpard56rxrAU9tL/UNP1e0h47YQpROcP/3TP13Ynz1ub3+13frrr+3e8lWftnEI2t4jCQxPQNAeXs2NmAABAgQIECBAYAYCgvYMkB2CAAECBAgQIEBgeAKC9vBqbsQECBAgQIAAAQIzEBC0Z4DsEAQIECBAgAABAsMTELSHV3MjJkCAAAECBAgQmIGAoD0DZIcgQIAAAQIECBAYnoCgPbyaGzEBAgQIECBAgMAMBATtGSA7BAECBAgQIECAwPAEBO3h1dyICRAgQIAAAQIEZiAgaM8A2SEIECBAgAABAgSGJyBoD6/mRkyAAAECBAgQIDADAUF7BsgOQYAAAQIECBAgMDwBQXt4NTdiAgQIECBAgACBGQgI2jNAdggCBAgQIECAAIHhCQjaw6u5ERMgQIAAAQIECMxAQNCeAbJDECBAgAABAgQIDE9A0B5ezY2YAAECBAgQIEBgBgKC9gyQHYIAAQIECBAgQGB4AoL28GpuxAQIECBAgAABAjMQELRngOwQBAgQIECAAAECwxMQtIdXcyMmQIAAAQIECBCYgYCgPQPkWRxiz5494d3vfnd45zvfGdavX79wyH379oVt27aFe++9d/S9E088MWzdujWsWbNm9O9JP59F3x2DAAECBAgQIFCigKBdQFVjyL744ovDY489Fq655ppFQfv6668Pu3btGoXr+BVD9xFHHBEuuuii0b8n/bwAHkMgQIAAAQIECHQiIGh3wr56B92xY0fYsmVLOProo0eNXn755QtBe+fOneGKK65o/V58/LifV1fGV6/HWiJAgAABAgQIDENA0J7jOqdtH2eccUY49NBD9wvNMYRv3749XHfddWHdunWjkVafE/897ucbN26cYx1dJ0CAAAECBAh0KyBod+u/akdvWr2+/fbbQwzbTXuyU4ge9/MY4Oft685wZfh8+NjEbr8mnD3xMR5AgAABAksT+Fy4deITDgzPCZeHuyc+zgMIlCAgaJdQxRBCn4L27t27wze/+c1OZL/44g+Fb73gbzs5toMSIECAwGSBZz317PBfvvCByQ/M8IiDDz44bNiwYVkt3/b1H4fPf/cny3rucp90wgsODGe+7KDlPt3zeiAgaPegCKvRhT4F7SeffHI1hrSsNj510DXhvgM/vqznrvRJG376upU24fkECBCYqcBXn/V/Z3q8eLC4on3pk38z8+OmAz73uc9d1rEF7WWxDf5JgnYhU6ApaNujXUhxDYMAAQIEOhcQtDsvwVx2QNCey7Lt3+lJdxhJdxCpPi624q4jhUwAwyBAgACBrAKCdlbeYhsXtAspbVPQjkObdJ/sST8vhMcwCBAgQIDAigQE7RXxDfbJgnYhpW8L2pP+8uOknxfCYxgECBAgQGBFAoL2ivgG+2RBe7ClN3ACBAgQIEBgWgFBe1opj6sKCNrmAwECBAgQIEBggoCgbYosR0DQXo6a5xAgQIAAAQKDEhC0B1XuVRusoL1qlBoiQIAAAQIEShUQtEutbN5xCdp5fbVOgAABAgQIFCAgaBdQxA6GIGh3gO6QBAgQIECAwHwJCNrzVa++9FbQ7ksl9IMAAQIECBDorYCg3dvS9Lpjgnavy6NzBAgQIECAQB8EBO0+VGH++iBoz1/N9JgAAQIECBCYsUAM2l18nfmyg7o4rGOukoCgvUqQmiFAgAABAgQIECBQFRC0zQcCBAgQIECAAAECGQQE7QyomiRAgAABAgQIECAgaJsDBAgQIECAAAECBDIICNoZUDVJgAABAgQIECBAQNA2BwgQIECAAAECBAhkEBC0M6BqkgABAgQIECBAgICgbQ4QIECAAAECBAgQyCAgaGdA1SQBAgQIECBAgAABQdscIECAAAECBAgQIJBBQNDOgKpJAgQIECBAgMAsBB544IFw7rnnhoceemjhcFdffXU488wzF/69Z8+ecN5554X77rtvUZeOOuqocMstt4Rjjz22sau33XZbuPvuu8OVV14Z9u3b19hGeuLpp58e3vrWt4YLLrhgUV/qDd96663hsMMOC5s2bQrXXHNN67FnYTeLYwjas1B2DAIECBAgQIDAKgvEILx58+YQw+tJJ500an3v3r3hsssuG/1/DMhr164NKWjHcJseF38en3/DDTe0hu2moF1vY9yQYpCOX/E51a94ciBor/Jk0BwBAgQIECBAgMDqCIwLqylsH3nkkaNA2xa0276feihor7xWVrRXbqgFAgQIECBAgMBMBdpWi1Mn7rnnntHWjJtvvnn0rbh1pL4aLWjnL5mgnd/YEQgQIECAAIE5F/hO+GonI3hh2LDfcdOK9cknn7xoL3b1gdUQvWHDhsagHVesv/GNb+y3tcOK9uqVWtBePUstESBAgAABAoUKfDy8O9wX7pjp6I4Pp4XXh//ZGrTPOuusRXuuJwXt+sWQ8fHxIsa0l7t+oKVcDFndJ57asUc7BEF7pi8ZByNAgAABAgTmUaCPQXulK9rpjiWnnnpq46q2Pdorn6mC9soNtUCAAAECBAgULrAvPN7JCNeEgxuPuxp7tGPD1TAd71BS/RK0V15yQXvlhlogQIAAAQIECMxUYDXuOlIN2vFCyXe84x2LLpis7uGedOFk0+BtHbF1ZKYvCgcjQIAAAQIECKyWwErvo10Nz6961atG999OtwSsX3ApaC+vala0l+fmWQQIECBAgACBzgWa/urjtH8ZMna+ehFjva3zzz9/Ye9221+XTADHH3/86FaC69atWzCZdkW76S4qTSv247a5dF6Ilg4I2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+XTeQIECBAgQIAAgb4KCNp9rYx+ESBAgAABAgQIzLWAoD3X5dN5AgQIECBAgACBvgoI2n2tjH4RIECAAAECBAjMtYCgPdfl03kCBAgQIECAAIG+Cgjafa2MfhEgQIAAAQIECMy1gKA91+Ub3/nbb789bN++vfFBF1xwQTjjjDPCjh07wpYtWxY95uijjw7XXXddWLduXcE6hkaAAAECBAgQyCsgaOf17V3rMXzfcccdC0E6/juG7a1bt4Y1a9b0rr86RIAAAQIECBCYVwFBe14rt4x+79y5M2zatCls3rw5bNy4cdTC9ddfP/rvRRddtIwWPYUAAQIECBAgQKBNQNAeyNzYt29f2LZtWzjiiCMWQnX6XgzdcRuJLwIECBAgQIAAgdUTELRXz7LXLcXtIVdffXW45pprwvr160d93bNnT7j44ovDgw8+uNB3+7N7XUadI0CAAAECBOZIQNCeo2KtpKtNW0TSVpJzzjlnYUW7vod7OcfcvXv3cp7mOQQIECBAILvA4Ycfnv0YDkAgCQjaA5gLaeU63mkk7c1uG3Z67Gmnnbbs7SQxaD/66KMDkDVEAgQIEJg3gQ0bNsxbl/V3jgUE7Tku3rRdj9tG4m3+prlln33b06p6HAECBAgQIEBgvICgPYAZ0nYLv6YAvpTV7wHQGSIBAgQIECBAYNkCgvay6ebniW238GvaJhIfu2vXLvfVnp/y6ikBAgQIECDQUwFBu6eFWa1uTdoKUr/zyIknnihkrxa+dggQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIBpRNugAAAgAElEQVQAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoD3o8hs8AQIECBAgQIBALgFBO5esdgkQIECAAAECBAYtIGgPuvwGT4AAAQIECBAgkEtA0M4lq10CBAgQIECAAIFBCwjagy6/wRMgQIAAAQIECOQSELRzyWqXAAECBAgQIEBg0AKC9qDLb/AECBAgQIAAAQK5BATtXLLaJUCAAAECBAgQGLSAoF14+Xfs2BG2bNmyaJRHH310uO6668K6devCvn37wrZt28K99947esyJJ54Ytm7dGtasWVO4jOERIECAAAECBPIKCNp5fTtv/fbbbw8xbLeF5+uvvz7s2rVr9PP4FUP3EUccES666KLO+64DBAgQIECAAIF5FhC057l6U/Q9Bun41RScd+7cGa644opw+eWXh/Xr148e1/S9KQ7jIQQIECBAgAABAjUBQbvgKZG2hWzcuDGcccYZ+400rnRv3759YRtJfEB6Tnx8fJ4vAgQIECBAgACB5QkI2stzm4tn7dmzJ1x88cXhwQcfXOhvdX9207aSSeF8LgaukwQIECBAgACBHggI2j0oQq4uxG0gmzZtCuecc87CinYM13fcccdoFfszn/nMfvu3VyNo7969Ozz66KO5hqVdAgQIECCwbIENGzYs+7meSGCpAoL2UsXm/PFplfu0004bjaR+oeRqBe05Z9J9AgQIEChU4PDDDy90ZIbVRwFBu49VydinapCO20js0c6IrWkCBAgQIEBg0AKCdsHlb7rYMa1oX3DBBeHQQw9115GC629oBAgQIECAQLcCgna3/lmPXt0mku46Ur1vdvyjNO6jnbUEGidAgAABAgQGLCBoF178+p1H6n/50V+GLHwCGB4BAgQIECDQmYCg3Rm9AxMgQIAAAQIECJQsIGiXXF1jI0CAAAECBAgQ6ExA0O6M3oEJECBAgAABAgRKFhC0S66usREgQIAAAQIECHQmIGh3Ru/ABAgQIECAAAECJQsI2iVX19gIECBAgAABAgQ6ExC0O6N3YAIECBAgQIAAgZIFBO2Sq2tsBAgQIECAAAECnQkI2p3ROzABAgQIECBAgEDJAoJ2ydU1NgIECBAgQIAAgc4EBO3O6B2YAAECBAgQIECgZAFBu+TqGhsBAgQIECBAgEBnAoJ2Z/QOTIAAAQIECBAgULKAoF1ydY2NAAECBAgQIECgMwFBuzN6ByZAgAABAgQIEChZQNAuubrGRoAAAQIECBAg0JmAoN0ZvQMTIECAAAECBAiULCBol1xdYyNAgAABAgQIEOhMQNDujN6BCRAgQIAAAQIEShYQtEuurrERIECAAAECBAh0JiBod0bvwAQIECBAgAABAiULCNolV9fYCBAgQIAAAQIEOhMQtDujd2ACBAgQIECAAIGSBQTtkqtrbAQIECBAgAABAp0JCNqd0TswAQIECBAgQIBAyQKCdsnVNTYCBAgQIECAAIHOBATtzugdmAABAgQIECBAoGQBQbvk6hobAQIECBAgQIBAZwKCdmf0DkyAAAECBAgQIFCygKBdcnWNjQABAgQIECBAoDMBQbszegcmQIAAAQIECBAoWUDQLrm6xkaAAAECBAgQINCZgKDdGb0DEyBAgAABAgQIlCwgaJdcXWMjQIAAAQIECBDoTEDQ7ozegQkQIECAAAECBEoWELRLrq6xESBAgAABAgQIdCYgaHdG78AECBAgQIAAAQIlCwjaJVfX2AgQIECAAAECBDoTELQ7o3dgAgQIECBAgACBkgUE7ZKra2wECBAgQIAAAQKdCQjandE7MAECBAgQIECAQMkCgnbJ1TU2AgQIECBAgACBzgQE7c7oHZgAAQIECBAgQKBkAUG75OoaGwECBAgQIECAQGcCgnZn9A5MgAABAgQIECBQsoCgXXJ1jY0AAQIECBAgQKAzAUG7M3oHJkCAAAECBAgQKFlA0C65usZGgAABAgQIECDQmYCg3Rm9AxMgQIAAAQIECJQsIGiXXF1jI0CAAAECBAgQ6ExA0O6M3oEJECBAgAABAgRKFhC0S66usREgQIAAAQIECHQmIGh3Ru/ABAgQIECAAAECJQsI2iVX19gIECBAgAABAgQ6ExC0O6N3YAIECBAgQIAAgZIFBO2Sq2tsBAgQIECAAAECnQkI2p3ROzABAgQIECBAgEDJAoJ2ydU1NgIECBAgQIAAgc4EBO3O6B2YAAECBAgQIECgZAFBu+TqGhsBAgQIECBAgEBnAoJ2Z/QOTIAAAQIECBAgULKAoF1ydY2NAAECBAgQIECgMwFBuzN6ByZAgAABAgQIEChZQNAuubrGRoAAAQIECBAg0JmAoN0ZvQMTIECAAAECBAiULCBol1xdYyNAgAABAgQIEOhMQNDujN6BCRAgQIAAAQIEShYQtEuurrERIECAAAECBAh0JiBod0bvwAQIECBAgAABAiULCNolV9fYCBAgQIAAAQIEOhMQtDujd2ACBAgQIECAAIGSBQTtkqtrbAQIECBAgAABAp0JCNqd0TswAQIECBAgQIBAyQKCdsnVNTYCBAgQIECAAIHOBATtzugdmAABAgQIECBAoGQBQbvk6hobAQIECBAgQIBAZwKCdmf0sznwzp07w6ZNm8L3v//90QFPPPHEsHXr1rBmzZrRv3fs2BG2bNmyqDNHH310uO6668K6detm00lHIUCAAAECBAgUKCBoF1jUNKQUsjdv3hw2btw4+vb1118fdu3atRC2b7/99lHYrobvgkkMjQABAgQIECAwMwFBe2bUsz9QU4iO4fuKK64Il19+eVi/fv0oeMeviy66aPYddEQCBAgQIECAQMECgnbBxW0aWjVov+hFLwrbtm0brXafccYZA5MwXAIECBAgQIBAXgFBO69v71qPq9x33HHHaA92/Lr44ovDgw8+uNDP1dif/eSTT/Zu3DpEgAABAgSiwHOf+1wQBGYmIGjPjLr7A6ULH6+66qrRKnbaw33OOecsrGhXg/hyL4bcvXt3+OY3v9n9gPWAAAECBAhUBA4++OCwYcMGJgRmJiBoz4y62wOlkH3BBReM3SayZ8+e0Sr3aaedZjtJtyVzdAIECBAgQGDOBQTtOS/gNN2fNmTHtvbt22ff9jSoHkOAAAECBAgQmCAgaBc+RerbRarDjT/bvn37ontmpxXtuPKdbglYOJHhESBAgAABAgSyCAjaWVj70WjTfbSrPWvaJlK/z3Y/RqIXBAgQIECAAIH5ExC0569mU/c4huY777yz8fHpgsgUttOdR+p/OXLqg3kgAQIECBAgQIDAIgFB24QgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uYAAQIECBAgQIAAgQwCgnYGVE0SIECAAAECBAgQELTNAQIECBAgQIAAAQIZBATtDKiaJECAAAECBAgQICBomwMECBAgQIAAAQIEMggI2hlQNUmAAAECBAgQIEBA0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhAgQIAAAQIECBDIICBoZ0DVJAECBAgQIECAAAFB2xwgQIAAAQIECBAgkEFA0M6AqkkCBAgQIECAAAECgrY5QIAAAQIECBAgQCCDgKCdAVWTBAgQIECAAAECBARtc4AAAQIECBAgQIBABgFBOwOqJgkQIECAAAECBAgI2uZAEQLv/8cfhm//y1Ojsaw5MIRzNjw7vPxnn1XE2JoG8c97nwr/6ys/CnuefHrM6557QPjvr3h2+Lm1BxQ55k9+88fhbx/+ycLY/sORB4b/+pKDihxrGpQ5XfacjnX+7Hd+Ev73t34cfvzTp6v+b9Y9K7zluGcXO6+H+DoutpgGNrWAoD01lQf2VeCD9/8o7HzspwvhOgaUvT8O4b+94tnhhYUGzzTGGK7jVwzdaw8K4Z2/8Jy+lmnZ/Uph5N//3NPhOr5Z/79//kn4zy8+KLz2hQcuu90+P9GcLntOx7n3te//NHz4qz8K63/m6XBdn+d9np/L6dsQX8fLcfKc8gQE7fJqOqgRpTerf3fEMyucTd8rCSW+Yf2fB38c/tPRzwTNpu+VMuZ4UhG/qicRTd8rZbzm9NMnTyXP6Ti+eDL18BNPLfokKn2vxEWCob2OS/l9ZBwrFxC0V26ohQ4F2t6M4y/1Q55zQJEfw8YV3b/f9ZNF22PSVpKfP/RZRW2pSOM68nmLaxkNvvToT4v81MKcfnrLV6lzOv26bAqesfZ/89BPwpuOPaiorW9DfB13+Lbo0D0TELR7VhDdWZpAU+iMLZS84tm0Etb2RrY0zf49uu3Tiba6928ES++ROf30dQalzulxYyt1FX+Ir+Olv/I9o1QBQbvUyg5kXEJJ2aFkiG/Q5nTZc1rQfuYi5pJPmAfyFmyYUwgI2lMgeUh/BYSSskOJoP3MnXN8StPf30NL7Vnbar0V7aVKejyB/gsI2v2vkR6OEbCftez9rJP2dpZ4S0Nzuuw5nX6d2aMdRncQitdalPg69sZNIAkI2ubCXAu4Q0P5d2gY2t0KzOny53T8pTvuriMlBs+hvY7n+o1V51dVQNBeVU6NdSHQds/hEt+sqqth8V7h7qM9rPtom9Nd/IbJc0z30S7/fvh5Zo5W501A0J63iulvo4C/olf2X9Eb4l+UM6fLntPxF5m/DFn+X3j1lk1A0DYHCBAgQIAAAQIECGQQELQzoGqSAAECBAgQIECAgKBtDhQlcNNNN4U3v/nN4eCDDy5qXG2D+fa3vx3uuuuu0ZiH8BXH+vznPz9s3LhxCMMdjfEjH/lIOOWUU8KLXvSiQYz58ccfH435bW972yDGGwd5//33h6997Wvh1FNPHcSY77zzzvDyl788HHfccYMYr0EOW0DQHnb9ixu9oF1cSRcNSNAuu75xdIJ2+TUWtMuvsRE+IyBomw1FCQjaRZVzv8EI2mXXV9C2ol3+DDfCoQkI2kOreOHjFbTLLrCgXXZ9BW1Bu/wZboRDExC0h1bxwscraJddYEG77PoK2oJ2+TPcCIcmIGgPreKFj1fQLrvAgnbZ9RW0Be3yZ7gRDk1A0B5axQsfr6BddoEF7bLrK2gL2uXPcCMcmoCgPbSKFz5eQbvsAgvaZddX0Ba0y5/hRjg0AUF7aBWvjXffvn1h27Zt4d577x395MQTTwxbt24Na9asmUsZQXsuyzZ1pwXtqanm9oFu7ze3pZu6427vNzWVBxYgIGgXUMSVDOH6668Pu3btGoXr+BVD9xFHHBEuuuiilTTb2XMF7c7oZ3JgQXsmzJ0eRNDulH8mBxe0Z8LsID0RELR7UoguurFz585wxRVXhMsvvzysX79+1IWm73XRt+UeU9Bertx8PE/Qno86raSXgvZK9ObjuYL2fNRJL1dHQNBeHce5bGXHjh1h+/bt4brrrgvr1q0bjSFtJTnjjDOW/GeuH/9RCJf8fbcUJ3zppvCPG94cfvic2fwJ9u/9MIRdexeP+V8/P4TnHTQbh3/1L98Oxzx012jMs/j6+uMh/OSni4+04WdnceSnj/HSh+4KTx70/PDwz83uT7B/9fuLx7f2oBCOfv7sxvwLX/1I+MZRp4QfPH82f4J9749DePBfFo/viLUhHPKc2Yz5OT98PMQxf/7nZ/cn2P95bwjf/+Hi8b3sZ0I48IDZjPmwPfeHw7/3tXD/MbPZo12f0wc+K4SXzeZX5gj0uG/cGXYf8vLwyLru/gT7KS8M4eyn15d8EcgqIGhn5e1347fffnuIYbu6JzsF7Y0bN4YYtpfyFYP2f/zUUp6x+o9928M3hY8c8ebw+IEzfNdY/WFM3eKRT347/Mr37xqNeQhfcayPPev54QsHzy5od+365l0fCZ/52VPCw8+dTdAOT4UQZhQwm2wP/snjIY75piNnF7S7rvGGJ+4Px+37Wrjj0NkE7VF9Y507+jrt0TvD/WteHr76vO6C9v/4t4J2R+Uf3GEF7cGV/JkBZwva990RnvretzuRPeGwEL76WAgx9A/h68i1IbzoeSF8/pEhjDaEWN/4NZTxxrHGMX/7iRC+s/eA8FR4KhwQnvlvU9Xrj1mN56Q2qsebdJzlPudnnn1AePnPPDWqcXWs0xwv9q/r5yy1D/HxcbwHP/vpeT1N/+NzpvGoz5WlPCeNI9V82udOM0defdhT4Qc/CuH+x54e76zm8cJxfuVtQdAexntGH0YpaPehCh31IWfQDt/8QkejclgCBAgQIDBG4PX/U9A2QWYmIGjPjLp/B8qxR7vrrSP9U9YjAnMm0PHWkTnTms/udrx1pA9oVrT7UIVh9EHQHkadG0e52ncdids1hvSRfkT90vf2pz30OU9v5yjxq2m8P39IiSN9ZkxDG3PcpvJo7cLAqFFynZtqHC8OXHtgmXN7aHO6rYq/8sIy62tU/RIQtPtVj5n3prT7aM8c0AEJECBAgAABAi0CgvbAp0Zpfxly4OU0fAIECBAgQKBHAoJ2j4qhKwQIECBAgAABAuUICNrl1NJICBAgQIAAAQIEeiQgaPeoGLpCgAABAgQIECBQjoCgXU4tjYQAAQIECBAgQKBHAoJ2j4qhKwQIECBAgAABAuUICNrl1NJICBAgQIAAAQIEeiQgaPeoGLqycoEHHnggXHXVVeF973tfWLdu3cob7GkLcZznnntueOihh0Y9PP3008OVV14Z1q5d29Mer7xb99xzTzj77LNHDR111FHhlltuCccee+zKG56DFm677bZw9913F13j+pweQp337NkTzjvvvHDfffeNZuH5558fNm3aNAczculdrL5+688uedxLl/KM0gQE7dIqOuDxpDfqF7zgBeHmm28uNmincV577bXhpJNOGlX8mmuuCQ8//HCxQSy+SV966aUL4Tr+O4655Dqnl3Kq9wknnFBsfeNYh1TTON5U1wsvvDCceeaZIYXus846a/TvIXzVX9dDGLMxDk9A0B5ezYsccVzx27x5c3jd614XHnvssaIDWNPqZnzTjithMXyWuMobx3XMMccsBJC9e/eGyy67LMRQkk42SpzYaZzf+973wiGHHFJ00B7Cqn11jjadHA/JYIgnFiX+jjKmyQKC9mQjj+i5QPyFfckll4QtW7aERx55ZDArndWylB6061MwBdCTTz656NW/GMbiVzzJKH3rSBprqVsnqnN4KPN33FtH6Z/C9fxtU/dmKCBozxDbofILDO3j5yQaV8I++tGPFr2SX509QxhvdS5/+tOfLjpop+D5iU98YqHMJe/DT6u5cdvIxz/+8ZDGPZS9yk3b3/K/OzgCgW4EBO1u3B01k8AQg3a6yOjWW28tehtFnDLVC6quvvrqYlezUxCLq7txa0zpWwrSeONY04p2yft303i/+93vLlx30GSQ6ddk582WPp87B9aBXgkI2r0qh86sVGBoQTsFz5JDZ9OcSCugRx55ZJF3aahvoxhiMCm5xvUTqTTHSz65SGO0bWal73KeP28Cgva8VUx/xwoMKWgPNWRXQ0mJdx6p3/KtOuFL3k7R9MIudd92W9AewrUWQxijt2kCVQFB23woSmAoQXtI20XaVsCGUuv4Ai19RbspfJW+8lm/k07aGlXiyWP1TWZIr9ui3lwNZtkCgvay6TyxjwJD+CU+xAuJYtC84YYb9tvPOpR7DpcetJu2iZR+wWv95GIoe7RLn8t9fF/Up24FBO1u/R19lQWGELTjiteNN97YKFfyBZHpXulp4EPalz6EcFK/88jxxx9f/F106n8Ncwh3HSl1O9Aqv5VpriABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30hAABAgQIECBAoCABQbugYhoKAQIECBAgQIBAfwQE7f7UQk8IECBAgAABAgQKEhC0CyqmoRAgQIAAAQIECPRHQNDuTy30ZI4FbrvttrB58+Zw9dVXhzPPPHOOR6LrBAgQIECAwGoJCNqrJamdwQo88MAD4aqrrgrve9/7wrp16wbrYOAECBAgQIDAYgFB24wgsEKBPXv2hDVr1oS1a9eusCVPJ0CAAAECBEoSELRLqqaxrJpADM/nnXdeuO+++xa1efzxx4ebb7550cp1XNE+99xzw7XXXhtOOumkRY9PW0rOP//8sGnTpv36d88994Szzz570fdvvfXW/dqpP3Hv3r3hsssuC5/4xCcWfjTtMY466qhwyy23hGOPPXZRs9dcc0248cYbGw2rW2LSeB966KH9Hlv3Gddm9bFN4zn99NPDlVdeuXACkyybOti0Zaf++KbapePGNqvHiv9OPzv55JNH24Hajt907Djuhx9+eNRm/KrXqjqGNM7oGefRhRdeuN/2o2ge509sN37FxzX5p3Zjn77xjW+01nOp9uNeWKnG02ybapo79ec1vSaqx0+vj2lfW/X202v7u9/97n6vg6XMh2qfJvWl+tj675ZJ42/7fZDmwhve8IZF86GpDlX3+uu//tqrv+7qtY9j/ehHP7rf78FV++WrIQKFCQjahRXUcFZHIL7ZX3rppfu9ETd9v+2x6c3tZS97WTjkkEP2C3JNz0vPaQpbaWTpMaeeeupCeG8LCPHN+M4771w0jqZjpOcfeeSR+50Q1E8k2sYb+1cNmPv27RudrJx11llj9623HTu2FY+VTmxSsKifsNT7l9r71re+tSgMpABXDS7VAFIPNCkQxeO96lWvGoXluk9T3+vfG3ci1hTWmk4IYt/j+OsneXXz9KlK6vtq2be9qtJcOPzww8NrXvOaxpPJ+rytnpCmfsYT1FTX6hwa9ylRfNyXv/zl8MpXvnK/41bbPeWUUxaduMbnfe5znwu7d+/e7+R42vlQPaGe9Dqv2tVfe02vrXjineZi29xJ29Xe/va3h9/7vd9bODlrqnv99VqdS/GTuDivqyea4/zTa2hSGF+d38JaIVCGgKBdRh2NYpUFxq3a1ANffOzdd9+9X5BOb1ivf/3rww033LBfSGoLjm3txSGOC1DVYBiDwLgx1N98x7XbtLLbtqJVfRN/5JFHWlf66+EjrdZWV9mbgm5aXa4+v96/cUGhbpL6+2u/9mvhBz/4waLAVjU67LDDWk8aqqvNsf/1Oow7MamOI/Y79uHxxx8fnZxUw1zbmOpjHxdqm14i9b6nx9TH0PTc6gnFMccc0/gaqJ9INL1OmoJf0wlfta3Uvxj4vv71r4ctW7Ys2roV6/zEE0+MfvaWt7xl4dObFFzf8573hI9//OOhPp+mnQ/VeTrpdT7JIP0eiCfXTSdz9d8T1ZrHTy3SJyfppKT6+yMeO7ZZHWe1tvHn9QWFtjmRvh+fM+mkapV/HWuOwFwLCNpzXfRTlb0AAArkSURBVD6dzyUwaVUnrS6mFaF6MKi+WcXA2bQaOe3K3aQ36rYA1badJT6+HtDa3lyr4T6G4Rj+2k4Q4mOrb/Jf/OIXGz8VqPd3mhXfccGvOpYXv/jFY4/ZdDISw18MG3Gl9V3vetfCtqBqKB930lC3rAfraT5qr4/hrrvu2u/Timk+bUi204b7aezbXmPVgPzpT396qqA9acvBtCvxab7GT0zi6nQ1aMc2YpCOJys33XTToouU66G2HrTT/J00H9JFz9O8zif9jhr3eorPrf983KcbTa/B+u+eSSdRTb8L0nPe+ta3hj/5kz8Zzc36NrlJ4/RzAkMVELSHWnnjbhUYt40iPqn6Rhf/3bQ9ovrmOGnVMO4Dn2Z/a9vqZdNAJoW7aVep61tSxm0HqW/NGLcyX+9z2uPatG0iPnbciUD1Zx/72Mf2W+GrHqseMqp1iv8fV2bT7RmrJ0LjThomWU4KUvWTmQ0bNoRLLrlkFB6rK+RN20DaAvVq2jfNr6aTi7atLen51b3AbdcTjKtztR/pNRiP+YEPfGDRCVKqY3x8XPFNW1LqbTfVZdr5kFaPp3mdj/tVO+mEqG1bVNMnO9V5lOZK0xgnBe36AkC11r/0S7+0cK1A/RoPbykECDQLCNpmBoGawKRVtUnbI9q2ZbStAlUvsmsLIPUwNmk1aVK4a9pu0XYhZLVP1T2s9YlTvciq6eLG6uOb9nhWLxKrX7DVtopX3YubPnpvCyFNgbZ6khSPEVdcqxcwVi+EbFuNHbd1JZ2Y1C+qTRbJtj5nqqF/XPhsOqFabftpTuSmDcjpRDVdALyUC16rc6K68rx169aFk5K0d3nbtm2jTyhS/ZpOnusnI/XX/bj5kMZR3XYxKcDWTxSiQduJZTq5jJ9KVa/FGLeanY4fP9WJc7jtpHhcreK8q17TUXebtJruzYQAgf0FBG2zgkBNYFJoqG+PqK7kjbs4blwArL6xnnDCCfvt904/b9rL3FTASUF70oV+bScbTStwTWFv0snKpEkX+x+Df/0OE03PS58GTLPiX61tbKu6vSb2Oa0kpz3Z02yXGbeXfNqtGU17x1Poj6vpbavF41YsJ10I2VaDun39cU21XUrITO2l58R/Vy94re85rh+/+hqLJ1fxHvZpH3Y6QYkrr9XaNs3bemis12rcfFjJ67w6ntjf2I/6Ra7p06HqJ13pmPX9+22hvK0mTWG57QLi+vxayiclk17jfk5gKAKC9lAqbZxTC0yzapQCWP2NJ71BNh0srV6OCyXjgtmkMFPtS1yVarrwLPWrGuzi9+LKbn3FvS1U19ttCh1LCZht/UwfYccVy7hCOekCuTiOSScY9e0g9QCbglp1r3dcSa1fUFatb9U9rSImy0lbA1I79X5XQ17bdpi2k5nVtK/f8jD2t+02h/FnTVugxp0AVccQV26bLgZsC/rVk6B4Z5F4cvTBD35wtLpd3eqT6le9FWZqs7pK3vS6b5oPccvENK/zaX7hNJ3UJ9/6XXDaQm5TKB93Yt401+LrP62Ep20x4z69cteRaarrMQSeFhC0zQQCNYFxqzZNV/SnAFjdxlC/Bd2kOwGkLkxz0V9T4Kw/b5qLG9OKZ9tjmwJ0W5CtB8ppP2Ie97hkFi3f8Y53TLxNYDQc1149gDbVOT4mhrV4S8YYzOJKY9uJSPx+3b3uMM0KYFsQTXfOiNtOlnoh5KT90pOs2vqdDJtuPznuJKftZ9Wx//qv/3rjCV/9F1TTXusYtOPtHGNYTHfcSSdw8aQz3vWnfu/4tgtjqycX4+ZD9ZaE1RPYphPHcXeHqf5V2frWjUm/F9pCedPcbArfqZ7V7SltbwiTTvS9kRAg0CwgaJsZBCoC4y6ErH+8X19RjD9vekNvCjVNb5D1Cw+b7iHctHrVFn6a+tP0/HEXTjatfDdtSWiyGLeiXg8Q9dW01M+4qhdXKsfdQaU+gZs+jm/ybgp/sQYx+HzpS18KL3nJS0ZbeNouhGxaUa5bTlphbwtEKRS9973vHd2irinYtq2WTxPuq8cdZ1+/FqBtq0Nsb9xx21Zdq+3FO7tMszWqfjKVTkhiH974xjeObvOX3H/nd35nFN6bQnHT3vrYRvUkuWk+tAX3aU5eqvv8m/aEV++hXZ3XbSeFbX/gKD23Ov/qCwHT/L6p9mHSljpvJAQICNrmAIGJAtUL8uoPrl+o2LTft+0PzTS9STV9/DzN3UfqfWz7S4/pjb/6lyebHlvdTlEP99UQX9/32hZw410gYptNH9Wn51Q/Fm+6eK96kdi02y/qwWTz5s0L36pfdDZuD3kK5akWbVsl2ixTWBs3l+Jj0vNjwGz640jJ5fOf/3zjX/JsCl+TLoSMx12KfdW0evLTdDHupE8xmrYiVF9T47akxH6kx9bna71f1drGu4407YGuB9EUyJtOIqvzIb0GlvI6Hzcv69cXNL1m4snDZz/72f3+uE7aS19/HVa3ddTnQ/X3S9v2l7bfJ5PqO/GXqwcQGKiAFe2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxUQtAdaeMMmQIAAAQIECBDIKyBo5/XVOgECBAgQIECAwEAFBO2BFt6wCRAgQIAAAQIE8goI2nl9tU6AAAECBAgQIDBQAUF7oIU3bAIECBAgQIAAgbwCgnZeX60TIECAAAECBAgMVEDQHmjhDZsAAQIECBAgQCCvgKCd11frBAgQIECAAAECAxX4/7T5Xt5dAok7AAAAAElFTkSuQmCC', `mes` = '8', `anio` = '2024', `tipo` = '10', `semana` = '0'
WHERE `id` = '283' 
 Execution Time:0.21239185333252

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '7'
AND `mes` = '8'
AND `semana` = '32'
AND `anio` = '2024' 
 Execution Time:0.19095993041992

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAvUAAAJWCAYAAADYynjNAAAAAXNSR0IArs4c6QAAIABJREFUeF7t3X3QZVV9J/qFb92tabWDUVsiV5EXa8qJjPaEXEyidcskTgUwmCJRkngTpILpFJekCG1DaJJLU3TbwKQIppO+QeomMUhCRSpCapKMVRPmJoww7QvqlAG5bYIDKDbpKNFuiMitdbjrcT+r9z4vz9nn7LfP84/Szznr5fNb55zv3mft/Rzz9NNPPx38ECBAgAABAgQIECDQWYFjhPrO1s7ACRAgQIAAAQIECIwEhHoLgQABAgQIECBAgEDHBYT6jhfQ8AkQIECAAAECBAgI9dYAAQIECBAgQIAAgY4LCPUdL6DhEyBAgAABAgQIEBDqrQECBAgQIECAAAECHRcQ6jteQMMnQIAAAQIECBAgINRbAwQIECBAgAABAgQ6LiDUd7yAhk+AAAECBAgQIEBAqLcGCBAgQIAAAQIECHRcQKjveAENnwABAgQIECBAgIBQbw0QIECAAAECBAgQ6LiAUN/xAho+AQIECBAgQIAAAaHeGiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECotwYIECBAgAABAgQIdFxAqO94AQ2fAAECBAgQIECAgFBvDRAgQIAAAQIECBDouIBQ3/ECGj4BAgQIECBAgAABod4aIECAAAECBAgQINBxAaG+4wU0fAIECBAgQIAAAQJCvTVAgAABAgQIECBAoOMCQn3HC2j4BAgQIECAAAECBIR6a4AAAQIECBAgQIBAxwWE+o4X0PAJECBAgAABAgQICPXWAAECBAgQIECAAIGOCwj1HS+g4RMgQIAAAQIECBAQ6q0BAgQIECBAgAABAh0XEOo7XkDDJ0CAAAECBAgQICDUWwMECBAgQIAAAQIEOi4g1He8gIZPgAABAgQIECBAQKi3BggQIECAAAECBAh0XECo73gBDZ8AAQIECBAgQICAUG8NECBAgAABAgQIEOi4gFDf8QIaPgECBAgQIECAAAGh3hogQIAAAQIECBAg0HEBob7jBTR8AgQIECBAgAABAkK9NUCAAAECBAgQIECg4wJCfccLaPgECBAgQIAAAQIEhHprgAABAgQIECBAgEDHBYT6jhfQ8AkQIECAAAECBAgI9dYAAQIECBAgQIAAgY4LCPUdL6DhEyBAgAABAgQIEBDqrQECBAgQIECAAAECHRcQ6jteQMMnQIAAAQIECBAgINRbAwQIECBAgAABAgQ6LiDUd7yAhk+AAAECBAgQIEBAqLcGCBAgQIAAAQIECHRcQKjveAENnwABAgQIECBAgIBQbw0QIECAAAECBAgQ6LiAUN/xAho+AQIECBAgQIAAAaHeGiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECotwYIECBAgAABAgQIdFxAqO94AQ2fAAECBAgQIECAgFBvDRAgQIAAAQIECBDouIBQ3/ECGj4BAgQIECBAgAABod4aIECAAAECBAgQINBxAaG+4wU0fAIECBAgQIAAAQJCvTVAgAABAgQIECBAoOMCQn3HC2j4BAgQIECAAAECBIR6a4AAAQIECBAgQIBAxwWE+o4X0PAJECBAgAABAgQICPXWAAECBAgQIECAAIGOCwj1HS+g4RMgQIAAAQIECBAQ6q0BAgQIECBAgAABAh0XEOo7XkDDJ0CAAAECBAgQICDUWwMECBAgQIAAAQIEOi4g1He8gH0b/s6dO8MVV1xROa2//du/Da997WvDz/zMz4S/+qu/Kn3chz70obBly5bw0z/90+HlL395+OM//uNw7LHHrjw27+PHfuzHjnpMenB87s/+7M+GcY+Jj53UZmrnyiuvDDt27Fg17vvuu2801nvvvTfE+b3pTW+qnH/xsfmD8ranscz7euyxx0a28Sd3S/2lx3z5y18Of/InfxJe8pKXrKpH9E9tHD58OPzqr/5q2Ldv3+jpaYxVY3v9618/avOUU04Jk/rJ559q9Pd///fhB3/wBysNi+OLD5rW/+/+7u9WtXvBBReE3/qt3wobNmwo7Wucf/G542oaGy7rZ9y6XEsNYz9pDZZNJl9bqY/4Gsw909qJr5uyn6o1PqnN1Fa+puK/T3rd5LXLH1/su8o81vOhhx5aqXmxvsV1m8ZZ/H3Z+0eqYXp81RzS+ih7L+vb+7/5ECAwn4BQP5+fZ9csME0QXWuozz+486HnH6p5eCj70J22zXGhvvjhXhb6i+OcJQBOY7mIUF8MobnPpFAf55oCUjpYqDp4qCvUT+Ofh8LU97hgP8k/WcxS09jvpHW5jFBf9Bh30FH19lB2IDCpzbK5F9ufFIrjQXPZ46tew2XrOL5e4kF5WW2LwT4P7PmBQtnvyw5OirWedGKh5rdizREg0EEBob6DRevzkNOH5bgzb9OElrKzW6ntqtCZh5MUMi655JLw13/91+EHfuAHjjozO22bH/nIR0Zn/KvOeG7atGlU1s9//vMrZ6rL6pzmlY+l7GzzNJZ5H9PYVp1Bj+H7ZS97WTjmmGNWzvLnYTgP9cU6FwNMDH1ve9vbRmf881Afx1z1LULVayNZ5GdU01wm+afnpzCavGN/6ZuFvO8q/3xtHjx4cHSWvGx9lc1n0rpcSw3jNyP5TzF4ltXpH/7hH8KrXvWq0bcw+es1PTcP71XfMKTaj2szjm/c67rqgDg/oM7/O3mm1/+DDz44qkextqnf3/md31n5pjCty+OPP37l26iydVv8fXR6wxvesPL45JbWSj6H4utHqO/zJ5+5EahHQKivx1ErNQlME0SnCS35h38cXtW2kvjY3/zN3wwf+MAHjtqm82d/9mej0Panf/qnIf3/FIDGjSNvs+pMffrQjmEgBqS4baTsLGbirQr18feprXHBeVKZprGtCvWx7TzkFcNK3FY1aWxFp61bt9YS6qvCadFskn/Zwdsky6q1nPvFdmYJ9bHdta7LNOZ8DHmoL4bJqu1KP/mTPxl+6qd+ajT2+P+L28qqQn3sv8wlretxbRZD/TRb1arqk48tP2BLBxgf//jHVw7Yoscv//Ivj/47HoTF12nxJECxzbT1r3iQVvb74naa/LUbx55q9PTTT4evfOUrpVsJJ61BvydAYFgCQv2w6t362Y7bspDOVKWAXranPj/blj44Zz0bmj5QY0iN+6Y/+clPjj7Ii2fSxgXsqrOeZfveU0BL203ic6vORI/rM//dtddeW3l9QtVZv3lD/S/90i+Fn/iJnxgdmLzjHe8YnZGMZ1/Tv9dxpr7qWoqyM7XjwmkKmNP4V33jMO4FVRXqU1upBmlt5ttDUtvFUD3NulxLDYuhvvitT5lpCqjFs87FAJzWb/xmatx+++LvpmkztptvPSrby15Vk+Jzx22bKnuN5fvpi33kBwEp9BftykJ7sY2ytZIO3uK3A/H/j3tfaP0buwESILAUAaF+Kcw6mVZgUaE+XTw56eLGNM78bF5xe0AK3PnX9lUXTBZDzqSDgmK4KbtgdppQnw5k9u7du/RQH8/WxjOa8Szle9/73vDzP//zo/8fz8D+6I/+6MQLZaPVWvfU5wF0UjgtsxznX7YPety3KpP21OdbeaYJ9dOsy3lCfXF/edlrpazt4rdNxW/DJoX6VK9p20yvzbI98NNsTcmfN+kamfT79No/7rjjjrrIPR0YFr+FKgvw40J9qmnRu2iatqEJ9dN+ingcgeEKCPXDrX0rZ76o7TezhPqyu2sUsdKH/byhvupiudhX1f7gtYT6SXcGKc5tLYGw+A3DjTfeGK666qpVZ+eLW1smXShbDGfjtvlM2lM/KZwWD7TKXghV/rPceaUq1Odnl6f9xmfadbmWGsYz9dNclFl1wXA0rNqOkoJ+fsCcjKdtM6/TrHcjSs+vuptMcc0UD9bS4+O1NflcyrZlzRLqy64xyL+N+eY3vznxjlStfDM3KAIEli4g1C+dXIfjBBYV6mOfxYsu8+0GxT31096NJH3YpgvmxrWZ76mfFNCqthVMs6c+hau0/WaZoT7OM/+GIPYff4rbl2ap81oulE3tV53BXat/Wrv5Rb152Cuewc0PAuPvykLjpAtlZ12X487slu2pT2t03JaWSd8+pLlOs6c+GUzbZtX7Rr6dqXj72mKd8jPvxS1DVYE+9lncT198jVddZzHuG6CyLUf5Gh13kBPHM8vr2acNAQLDEhDqh1Xv1s92lrA3LrRMe/ebsn226U41+daKsgvoyj7Yx7WZ38awLMiNC0RVob4YSvI7aswSAtZylje/FqB4n/j83vGTLpQt+9Zg1lA/TTgdd3CU+1ddVJpfYJm/uMrWcjGwpbpMe6a+al3k63KaazPyOaV94ONC47i1kZ+drhprHsDTwXbZa7nsjHfZBedVZ95TPfI7y+SPf/7zn79yN5qqawj+6I/+aNV1LmVn2FN/uW1+95u4rS7NrewASqhv/ceUARJorYBQ39rSDHNgk87axQ/ddFeUWUN92V7conLxD1uVnX1P/RVvTTlNm/FDPA8j4wLhuJAy7dnauL9/Gsv8D2FNmk/RvypsFy/8TN8a5Bcaz3LwNu196mN9/uIv/iL89m//duUfJouPiXOIP3EfdNme+HEHhPmrcpqz2vlBVf4twqQLZWMf8baRv/Ebv7Fye8/8bjVldw0ad0Fx8c5Cf/AHfxB+93d/d+UPhJW988Q6nnbaaeG8884r3RqWB9n9+/ePbuFa9ZPcy0J6VTiOcx63/idtWcuvWcjHUFbbZFPcTz9uDOO+fUivhfQNX1l9yuYwzYH2MD8tzJoAgVxAqLcmWiUwTRBda6jPz9yl/y5+/T1pn3xV4M7HnX+lXha6qg4cyrYMpLFOEyiq5pkXelyAmDYQFv+ibDroKZ75TCFnLbfbrNpTXzW2aUP99u3bw6c+9anKgFzln18DMenOK1UHLsUDp1iDdFvIqgtlYz/ve9/7wrnnnlv612XjvIvr8oYbbggXXnhh5YFNfmA2Tah/z3veM1o+H/zgByu3fxQPVONjy0J90WzcOs/XcNkdgIrroCrQV71uUnvjtmEVD6Z+7ud+btW+9qoDluI4i+8JZe8xZW+8Qn2rPo4MhkDnBIT6zpXMgAkQIECAAAECBAisFhDqrQgCBAgQIECAAAECHRcQ6jteQMMnQIAAAQIECBAgINRbAwQIECBAgAABAgQ6LiDUd7yAhk+AAAECBAgQIEBAqLcGCBAgQIAAAQIECHRcQKjveAENnwABAgQIECBAgIBQbw0QIECAAAECBAgQ6LiAUN/xAho+AQIECBAgQIAAAaHeGiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECoH/gauO2228KDDz4YLrroolUS+/fvD5deeunKv+3atSts2bJl5b8n/X7grKZPgAABAgQIEFiqgFC/VO52dRYD/d69e8MZZ5yxKtQfOHAgbNu2LWzfvn0U5GOA3717d9izZ0844YQTwqTft2uWRkOAAAECBAgQ6L+AUN//Gh81wyNHjoSdO3eG++67L7zwhS8Mr3/961eF+uuvv370nOLZ++K/Tfr9AElNmQABAgQIECDQqIBQ3yh/M53HM+/xLP2OHTvCvn37VgX4FPjjGfqzzz57ZYDpOZdcckm45pprRmfwy34f21y/fn0zE9MrAQIECBAgQGCgAkL9QAufpp2fdT906FC4+OKLw9atW4/aQx+36lxxxRXhyiuvrPz9ddddFzZt2tQp1cfDwfAfw9umGvMPhHOnepwHESBAgMB0Ah8PN0/1wP8t/HL4ofALUz3WgwgMUUCoH2LVC3NuU6j/7Gc/G5588smlV+TIc78WPvZ925berw4JECBAYHqBUx76iXDSl//D9E+o8ZEnn3xy2Lhx48wtHnri6fD+Ty3/c+19/+55YdO6Y2Yeryd0W0Co73b95h59m0L9E088Mfd81tLAvxzzWPjA885ay1Pnfs7J3/7hudvQAAECBJYpcP+z/usyu1vp64e/dUE4/al3N9J37HTdunUz9y3Uz0zmCXMICPVz4PXhqXmot6e+D1U1BwIECBBog4BQ34YqDGcMQv1wal0602nvZOPuNwNfKKZPgAABAjMLCPUzk3nCHAJC/Rx4fXhqWaifdB/6Sb/vg4s5ECBAgACBeQWE+nkFPX8WAaF+Fq0ePrYs1MdpTvqLsZN+30MqUyJAgAABAjMJCPUzcXnwnAJC/ZyAnk6AAAECBAgQKBMQ6q2LZQoI9cvU1hcBAgQIECAwGAGhfjClbsVEhfpWlMEgCBAgQIAAgb4JCPV9q2i75yPUt7s+RkeAAAECBAh0VECo72jhOjpsob6jhTNsAgQIECBAoN0CQn2769O30Qn1fauo+RAgQIAAAQKtEBDqW1GGwQxCqB9MqU2UAAECBAgQWKaAUL9MbX0J9dYAAQIECBAgQGABAkL9AlA1WSkg1FscBAgQIECAAIEFCMRQ/7H/+dQCWh7f5Fu/99lh07pjlt6vDpsVEOqb9dc7AQIECBAgQIAAgbkFhPq5CTVAgAABAgQIECBAoFkBob5Zf70TIECAAAECBAgQmFtAqJ+bUAMECBAgQIAAAQIEmhUQ6pv11zsBAgQIECBAgACBuQWE+rkJNUCAAAECBAgQIECgWQGhvll/vRMgQIAAAQIECBCYW0Con5tQAwQIECBAgAABAgSaFRDqm/XXOwECBAgQIECAAIG5BYT6uQk1QIAAAQIECBAgQKBZAaG+WX+9EyBAgAABAgSWLrBnz56wb9++lX5PPfXUcOONN4ZNmzat/Nutt94atm/fftTYLrjggrBt27bSMR86dCicf/75o9+fdtppoaqN9OSbb7453HnnnavGkjd81llnhauvvjrccMMNo19V9b10xJZ1KNS3rCCGQ4AAAQIECBBYlEAK3ccff/woKG/YsGHU1d133x3OPffcEEN2DOPxJwbyu+66a9Xj0vPjY8rCdVmoz9sYN7cHHnhg1G486DjxxBNXPTT+m1BfrSfUL+pVo10CBAgQIECAQMsExgXjGOwvueSScNNNN40CdVmorwr7aZpCfXMFF+qbs9czAQIECBAgQGBpAuPOgsdBHD58OFx22WXh9NNPD+ecc45Qv7TK1NORUF+Po1YIECBAgAABAqsE/jk8HI6Ef1m6yvrwXeHF4RVH9RvPxMcz9fne+eIDi2fn77jjjtLtN7/2a78WLr300qO2x8R2nKlferlXOhTqm7PXMwECBAgQINBjgRjqrw9nLX2GF4WPVob6W265ZdUe+Xxweagvu1A2Pqe4977YxiwXyqYLYNO+/tiOPfVrXy5C/drtPJMAAQIECBAgUCnQxlA/75n6ONnYRjyLn/beTwr1LpRdzotEqF+Os14IECBAgACBgQkcCY83NuP1YeNRfde1pz4/Gy/UN1bmVR0L9e2og1EQIECAAAECBBYuUMfdb4qh/sEHH1y17z7+rrjnvuoOOlUTtf1m7UtAqF+7nWcSIECAAAECBDolMO996uNki0H9M5/5zKrbYOYX4wr1y1seQv3yrPVEgAABAgQIEGiFQP6XXmf5i7L5Ba7Fto477rhVe+0n/UXZ3bt3j26fmX5mOVNfdjef/JuIcVuFWlGIGgch1NeIqSkCBAgQIECAAAECTQgI9U2o65MAAQIECBAgQIBAjQJCfY2YmiJAgAABAgQIECDQhIBQ34S6PgkQIECAAAECBAjUKCDU14ipKQIECBAgQIAAAQJNCAj1TajrkwABAgQIECBAgECNAkJ9jZiaIkCAAAECBAgQINCEgFDfhLo+CRAgQIAAAQIECNQoINTXiKkpAgQIECBAgAABAk0ICPVNqOuTAAECBAgQIECAQI0CQn2NmJoiQIAAAQIECBAg0ISAUN+Euj4JECBAgAABAgQI1Cgg1NeIqSkCBAgQIECAAAECTQgI9U2o65MAAQIECBAgQIBAjQJCfY2YmiJAgAABAgQIECDQhIBQ34S6PgkQIECAAAECBAjUKCDU14ipKQIECBAgQIAAAQJNCAj1TajrkwABAgQIECBAgECNAkJ9jZiaIkCAAAECBAgQINCEgFDfhLo+CRAgQIAAAQIECNQoINTXiKkpAgQIECBAgAABAk0ICPVNqOuTAAECBAgQIECAQI0CQn2NmJoiQIAAAQIECBAg0ISAUN+Euj4JECBAgAABAgQI1Cgg1NeIqSkCBAgQIECAAAECTQgI9U2o65MAAQIECBAgQIBAjQJCfY2YmiJAgAABAgQIECDQhIBQ34S6PgkQIECAAAECBAjUKCDU14ipKQIECBAgQIAAAQJNCAj1TajrkwABAgQIECBAgECNAkJ9jZiaIkCAAAECBAgQINCEgFDfhLo+CRAgQIAAAQIECNQoINTXiKkpAgQIECBAgAABAk0ICPVNqOuTAAECBAgQIECAQI0CQn2NmJoiQIAAAQIECBAg0ISAUN+Euj4JECBAgAABAgQI1Cgg1NeIqSkCBAgQIECAAAECTQgI9U2o65MAAQIECBAgQIBAjQJCfY2YmiJAgAABAgQIECDQhIBQ34S6PgkQIECAAAECBAjUKCDU14ipKQIECBAgQIAAAQJNCAj1TajrkwABAgQIECBAgECNAkJ9jZiaIkCAAAECBAgQINCEgFDfhLo+CRAgQIAAAQIECNQoINTXiKkpAgQIECBAgAABAk0ICPVNqLe4z0OHDoWLL744fOlLXzpqlK985SvDddddFzZs2BB27twZ7rnnnlWP2bp1azj77LNbPDtDI0CAAAECBAj0U0Co72dda51VCvpnnnnmKLSn/44hfsuWLbX2pTECBAgQIECAAIHZBYT62c0G94zrr78+PProo2HHjh1h/fr14cCBA+Gqq64Kl19+eTjhhBMG52HCBAgQIECAAIG2CQj1batIy8azf//+sHv37rBnz56VAB//be/evaOtOJs2bWrZiA2HAAECBAgQIDA8AaF+eDWfesZHjhwZ7Z1/6UtfGi666KKV5912222jUF/8sZ9+alYPJECAAAECBAjULiDU107anwarttnE7Tj33nvvypn6fM/9WgUOHjy41qd6HgECBAgQWJjAunXrwsaNGxfWvoYJ1CEg1Neh2NM24hn5uNUm7aUfN8342Ntvv32uLTn3339/TyVNiwABAgS6LLB582ahvssFHMjYhfqBFHrWaaatN/HuNtPcptI++1mFPZ4AAQIECBAgUJ+AUF+fZa9aqrptZVXYn+Wsfq+gTIYAAQIECBAg0AIBob4FRWjjEMbdtjLfahMfu23btrB9+3b3rW9jMY2JAAECBAgQ6L2AUN/7Eq9tgpO20+R3wNm1a5dAvzZqzyJAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAhxbtFUAAAgAElEQVQQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz02oAQIECBAgQIAAAQLNCgj1zfrrnQABAgQIECBAgMDcAkL93IQaIECAAAECBAgQINCsgFDfrL/eCRAgQIAAAQIECMwtINTPTagBAgQIECBAgAABAs0KCPXN+uudAAECBAgQIECAwNwCQv3chBogQIAAAQIECBAg0KyAUN+sv94JECBAgAABAgQIzC0g1M9NqAECBAgQIECAAAECzQoI9c36650AAQIECBAgQIDA3AJC/dyEGiBAgAABAgQIECDQrIBQ36y/3gkQIECAAAECBAjMLSDUz03YvwaOHDkSdu7cGe65555Vk9u6dWs4++yzR/+2f//+cOmll678fteuXWHLli39wzAjAgQIECBAgEAHBIT6DhRp2UM8dOhQuPjii0MM8WVB/cCBA2Hbtm1h+/bto9/HgL979+6wZ8+ecMIJJyx7uPojQIAAAQIECAxeQKgf/BI4GiCG9quuuipcfvnlpSH9+uuvHz3poosuWnly2b+hJUCAAAECBAgQWI6AUL8c5071Es+87927N1x33XVh06ZNq8aetubEM/RpK058QHzObbfdFnbs2BHWr1/fqfkaLAECBAgQIECg6wJCfdcruIDxx3AeQ33xJ+2nr9qaM+5AYAFD1CQBAgQIECBAgEBBQKi3HI4SiFtp7r333pUz9SnIn3nmmeEtb3lL6X77OkL9/fffrxoECBAgQKB1Aps3bw4bN25s3bgMiEBRQKi3HqYSiGfvb7/99nDFFVeEK6+88qiLaOsI9QcPHpxqLB5EgAABAgSWKbBu3Tqhfpng+lqTgFC/JrbhPSmF9quvvjrccMMNo7ve2FM/vHVgxgQIECBAgEA7BYT6dtalsVFVXQgbz9THYB8vhN23b99ofO5+01iZdEyAAAECBAgQWCUg1FsQRwmkrTbp7jf5fendp96iIUCAAAECBAi0S0Cob1c9WjOa/A44+V+M9RdlW1MqAyFAgAABAgQIBKHeIiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECotwYIECBAgAABAgQIdFxAqO94AQ2fAAECBAgQIECAgFBvDRAgQIAAAQIECBDouIBQ3/ECGj4BAgQIECBAgAABod4aIECAAAECBAgQINBxAaG+4wU0fAIECBAgQIAAAQJCvTVAgAABAgQIECBAoOMCQn3HC2j4BAgQIECAAAECBIR6a4AAAQIECBAgQIBAxwWE+o4X0PAJECBAgAABAgQICPXWAAECBAgQIECAAIGOCwj1HS+g4RMgQIAAAQIECBAQ6q0BAgQIECBAgAABAh0XEOo7XkDDJ0CAAAECBAgQICDUWwMECBAgQIAAAQIEOi4g1He8gIZPgAABAgQIECBAQKi3BggQIECAAAECBAh0XECo73gBDZ8AAQIECBAgQICAUG8NECBAgAABAgQIEOi4gFDf8QIaPgECBAgQIECAAAGh3hogQIAAAQIECBAg0HEBob7jBTR8AgQIECBAgAABAkK9NUCAAAECBAgQIECg4wJCfccLaPgECBAgQIAAAQIEhHprgAABAgQIECBAgEDHBYT6jhfQ8AkQIECAAAECBAgI9dYAAQIECBAgQIAAgY4LCPUdL6DhEyBAgAABAgQIEBDqrQECBAgQIECAAAECHRcQ6jteQMMnQIAAAQIECBAgINRbAwQIECBAgAABAgQ6LiDUd7yAhk+AAAECBAgQIEBAqLcGCBAgQIAAAQIECHRcQKjveAENnwABAgQIECBAgIBQbw0QIECAAAECBAgQ6LiAUN/xAho+AQIECBAgQIAAAaHeGiBAgAABAgQIECDQcQGhvuMFNHwCBAgQIECAAAECQr01QIAAAQIECBAgQKDjAkJ9xwto+AQIECBAgAABAgSEemuAAAECBAgQIECAQMcFhPqOF9DwCRAgQIAAAQIECAj11gABAgQIECBAgACBjgsI9R0voOETIECAAAECBAgQEOqtAQIECBAgQIAAAQIdFxDqO15AwydAgAABAgQIECAg1FsDBAgQIECAAAECBDouINR3vICGT4AAAQIECBAgQECotwYIECBAgAABAgQIdFxAqO94AQ2fAAECBAgQIECAgFBvDRAgQIAAAQIECBDouIBQ3/ECGj4BAgQIECBAgAABod4aIECAAAECBAgQINBxAaG+4wU0fAIECBAgQIAAAQJCvTVAgAABAgQIECBAoOMCQn3HC2j4BAgQIECAAAECBIR6a4AAAQIECBAgQIBAxwWE+o4X0PAJECBAgAABAgQICPXWAAECBAgQIECAAIGOCwj1HS+g4RMgQIAAAQIECBAQ6q0BAgQIECBAgAABAh0XEOo7XkDDJ0CAAAECBAgQICDUWwMECBAgQIAAAQIEOi4g1He8gIZPgAABAgQIECBAQKi3BkoFrr/++nDHHXes/G7Xrl1hy5Yto/8+cuRI2LlzZ7jnnntWPXfr1q3h7LPPJkqAAAECBAgQILBkAaF+yeBd6C4G+kcffTTs2LEjrF+/Phw4cCBs27YtbN++fRTsDx06FC6++OIQQ3wK+l2YlzESIECAAAECBPoqINT3tbJrnFdVYI9BP/5cdNFFo5B/1VVXhcsvvzyccMIJa+zJ0wgQIECAAAECBOoSEOrrkux5O8VQv3///rB3795w3XXXhU2bNvV85qZHgAABAgQIEGi/gFDf/ho1PsJ09v7MM88c7Zm/7bbbRqG++FPHfvonnnii8bkaAAECBAgQKBNYt24dGAKtFhDqW12e5geXLoqNI0l77ONZ+3vvvXflTH0e+tc66s9+9rPhySefXOvTPY8AAQIECCxE4OSTTw4bN25cSNsaJVCXgFBfl2QP20mB/pFHHpm41Saevb/99tsnPq6HTKZEgAABAgQIEGhcQKhvvATtHMAsgT7OwD77dtbRqAgQIECAAIFhCAj1w6jzTLMs23KTGki/i7eyLN6TPp6pj8E+bdGZqUMPJkCAAAECBAgQmEtAqJ+Lr59Pzu9Tn88y32qT38e+nypmRYAAAQIECBBor4BQ397aNDKyFNC/9rWvHdX/93//96+cic/vgFP8i7ONDFynBAgQIECAAIEBCwj1Ay6+qRMgQIAAAQIECPRDQKjvRx3NggABAgQIECBAYMACQv2Ai2/qBAgQIECAAAEC/RAQ6vtRR7MgQIAAAQIECBAYsIBQP+DimzoBAgQIECBAgEA/BIT6ftTRLAgQIECAAAECBAYsINQPuPimToAAAQIECBAg0A8Bob4fdTQLAgQIECBAgACBAQsI9QMuvqkTIECAAAECBAj0Q0Co70cdzYIAAQIECBAgQGDAAkL9gItv6gQIECBAgAABAv0QEOr7UUezIECAAAECBAgQGLCAUD/g4ps6AQIECBAgQIBAPwSE+n7U0SwIECBAgAABAgQGLCDUD7j4pk6AAAECBAgQINAPAaG+H3U0CwIECBAgQIAAgQELCPUDLr6pEyBAgAABAgQI9ENAqO9HHc2CAAECBAgQIEBgwAJC/YCLb+oECBAgQIAAAQL9EBDq+1FHsyBAgAABAgQIEBiwgFA/4OKbOgECBAgQIECAQD8EhPp+1NEsCBAgQIAAAQIEBiwg1A+4+KZOgAABAgQIECDQDwGhvh91NAsCBAgQIECAAIEBCwj1Ay6+qRMgQIAAAQIECPRDQKjvRx3NggABAgQIECBAYMACQv2Ai2/qBAgQIECAAAEC/RAQ6vtRR7MgQIAAAQIECBAYsIBQP+DimzoBAgQIECBAgEA/BIT6ftTRLAgQIECAAAECBAYsINQPuPimToAAAQIECBAg0A8Bob4fdTQLAgQIECBAgACBAQsI9QMuvqkTIECAAAECBAj0Q0Co70cdzYIAAQIECBAgQGDAAkL9gItv6gQIECBAgAABAv0QEOr7UUezIECAAAECBAgQGLCAUD/g4ps6AQIECBAgQIBAPwSE+n7U0SwIECBAgAABAgQGLCDUD7j4pk6AAAECBAgQINAPAaG+H3U0CwIECBAgQIAAgQELCPUDLr6pEyBAgAABAgQI9ENAqO9HHc2CAAECBAgQIEBgwAJC/YCLb+oECBAgQIAAAQL9EBDq+1FHsyBAgAABAgQIEBiwgFA/4OKbOgECBAgQIECAQD8EhPp+1NEsCBAgQIAAAQIEBiwg1A+4+KZOgAABAgQIECDQDwGhvh91NAsCBAgQIECAAIEBCwj1Ay6+qRMgQIAAAQIECPRDQKjvRx3NggABAgQIECBAYMACQv2Ai2/qBAgQIECAAAEC/RAQ6vtRR7MgQIAAAQIECBAYsIBQP+DimzoBAgQIECBAgEA/BIT6ftTRLAgQIECAAAECBAYsINQPuPhDn/rfffmp8J8e/Fb41refkfg3m54V3n3Kc3vN8hf/+K3w/zzy1Mocf2jzs8OP/y/P6eWcv3L46fB///2/hkNPPD2a36Z1x4Sff+1zw8s2HNPL+cZJWdMh9HlNp4X72599Mjz8jWfW9fpnh/AzJz83nPSiZ/VyXQ/xddzLQprUUgSE+qUw66RtAl/42rfDH9//r+GEFz4T5FMY+l9f1t+Qm88xBvz/9pWnwn84/jnhTS9/dttKNPd4YvA5/K0wCvLxJwb8Dc8J4f/4t8+bu+02NmBNPyf0fU3HdfeH9/1rOPD1b68E+bTO//fXPje8vIcHrEN7HbfxvcWYuiMg1HenVkZao0D8YHzkm0+vOnOb/q3PH46RsBhq4wdm/m81MjfWVDyA+c9f+lb4kVd+54Cl7N8aG+ACOramn0Ht65qOc0sHbv/+pd85+VD2bwtYXo00OcTXcSPQOu2NgFDfm1KayCwCZR/88QPkvzz0VPjpE5/Tu6+y01fYm59/zKotRvHM5uf+6duhbwcycV7//dGnVm1LSAav++5n9XLLkTX9zDtAX9d0nFvVgWms/Yuft/q1Pcv7YVsfO8TXcVtrYVzdEBDqu1Eno6xRoCrg9vlMbtXZvLIPzRqpG2uq7Kx1Vd0bG2SNHVvT37kupK9rOh2w5Aerff52Ymiv4xrfEjQ1UAGhfqCFH/K0BaD+B6ChhQFruv9rWqh/5gL3Ph+cD/lz2dzrERDq63HUSocEBKD+ByCh/pkXpG+f+nVHmKpvIfp6HcHQXscd+hg11JYKCPUtLYxhLVbA/uNnfNP+477d6nGIe3Gt6X6v6XEHafbUL/bzQusEuiIg1HelUsZZq8C4O4X0LeAmuLLQ19czfEO8a4Y1/cxK7+uajnNz95t+f/tU64ecxgYpINQPsuwm7Z7e/b+n99Dub21N939Nx3fuqvvU9/lkxJD+3oRPZwLzCAj18+h5bqcF/PXNfv/1zSH+JUprut9ruvitm78o29+/DN3pD1aDb1RAqG+UX+cECBAgQIAAAQIE5hcQ6uc31AIBAgQIECBAgACBRgWE+kb5dd4Ggf3794dvfOMb4c1vfnMbhrOUMdxxxx3hpJNOCqeccspS+muyk8cffzx8+MMfDr/4i7/Y5DCW2vd9990XvvCFL4Qzzjhjqf022dmdd94ZXvCCF4QtW7Y0OYyl9h3XdXzfesUrXrHUfpvo7OGHHw6xxu9617ua6F6fBDohINR3okwGuUgBoX6Rus23LdQ3X4NljECoX4Zyc30I9c3Z67k7AkJ9d2plpAsSEOoXBNuSZoX6lhRiwcMQ6hcM3HDzQn3DBdB9JwSE+k6UySAXKSDUL1K3+baF+uZrsIwRCPXLUG6uD6G+OXs9d0dAqO9OrYx0QQJC/YJgW9KsUN+SQix4GEL9goEbbl6ob7gAuu+EgFDfiTIZ5CIFhPpF6jbftlDffA2WMQKhfhnKzfUh1Ddnr+fuCAj13amVkS5IQKhfEGxLmhXqW1KIBQ9DqF8wcMPNC/UNF0D3nRAQ6jtRJoNcpIBQv0jd5tsW6puvwTJGINQvQ7m5PoT65uz13B0Bob47tWrVSGMQvvTSS1fGtGvXrs7eH1qob9XSqn0wQn3tpK1sUKhvZVlqG5RQXxulhnosINT3uLiLmtqBAwfCtm3bwvbt20dBPobi3bt3hz179oQTTjhhUd0urF2hfmG0rWhYqG9FGRY+CKF+4cSNdiDUN8qv844ICPUdKVSbhnn99dePhnPRRRetDKvs39o05nFjEeq7Uqm1jVOoX5tb154l1HetYrONV6ifzcujhykg1A+z7mue9ZEjR8LOnTtHZ+jPPvvslXZiML7tttvCjh07wvr162dq/z/+jxDu+9pMT6n1wZu/sj+s+9Y3wj8c9+Za2x3X2P3ZfJ/9rBBes3Fp3YdTvnhHOPjik8Jjm05ZeKeHvxXCl76xupuXbgjhxc9beNejDp735OPh397/4fCJ1/3icjoMIXzlcAhfe3J1d695YQjPPmY5Qzj20H3hJf/8hXDfq89YTochhP/38RCe+vbq7k5+0dK6D6966M7wxHNeEB552ZaldZq/jjc8J4RXvmBp3Y/W9RePe3P4lxe8YuGd/vOTITx6eHU33/uCEJ7/nIV3Pergu77xcHj1Q3eGz578ruV0WNHLvtMb7V7nBMYKCPUWyEwChw4dChdffHHYunXrqj30MdTv3bs3XHfddWHTpk0ztRlD/c0HZnpKrQ9+w+P7wwu//Y3wNy9aXqgPMdw9Xes0ZmrszH+6I9y3/qRw//MXH+pH81xSmC1D2PjU4+Fdj344/F+blxfqZyrGAh588jfvC6cc+UK4/buXF+oXMI2ZmnzL1+4MX3/WC8InNy4v1M80wAU8OK7r+L71yLrFh/oFDH+mJjc/8XCINf7wS5sL9RufG8J/edtMw/ZgAksVEOqXyt39zhYZ6p/+m32NAL3x2Ge6/cRjjXTfSKdxzo//awj3f30J3Tcd6p8bwskvHFZ943xjAPnkY985mno6PB2OCceEqv8tWwmTnpN+X3zuMp4T+4g/xfm84dhn/i2+jqvmmT9nmrHO+pz4+GnaTWMc1/6kvuOcH/nmMeHhw9+p7aTnlI1tWc+Jc57VJz3n5RueDq94/vj6psfma3lSPca9C66s8Ve9MWw86Y1C/RI+MnSxdgGhfu12g3zmIkN9+PP/c5CmJk2AAAECLRd4/RlCfctLZHghCPVWwUwCi9pT3+T2m5kA6npww9tv6prGVO00fKZ+qjF6EAECBCYI2H5jibRdQKhve4VaOL66737zN19u4SQXPKTP/fPRHbzuxQvutKHmH/5mCP+UXTQah9LX+ca5ldU3Xgi94dkNFWEJ3Q5pTSfOIc25bK7f/bww2hIzpJ+3vHxIszXXrgkI9V2rWAvG27f71LeA1BAIECBAgAABAnMJCPVz8Q33yX36i7LDraKZEyBAgAABAn0REOr7UknzIECAAAECBAgQGKyAUD/Y0ps4AQIECBAgQIBAXwSE+r5U0jwIECBAgAABAgQGKyDUD7b0Jk6AAAECBAgQINAXAaG+L5U0DwIECBAgQIAAgcEKCPWDLb2JEyBAgAABAgQI9EVAqO9LJc1jLoE9e/aEV7/61eGcc86Zq522PznOc9++fSvDvPnmm8Npp53W9mGveXyHDx8Ol112WfjoRz86auOCCy4I27ZtW3N7XXrioUOHwvnnnz+ab59rnK/pIdT51ltvDdu3bx8tx+OOOy7cdNNN4cQTT+zS8pxqrPnrt/ikPs97KhwPIlAiINRbFoMXSKFg9+7dvQ71cZ6PPPJIuPrqq8OGDRvCAw88EM4777xwzTXX9DL0pUCwefPmUbBN/3366af3us7pBZ3WdZ8P3IZW01jbWNe777473HjjjWHTpk0hBvxbbrll5b/7/oaev677Pl/zIzCLgFA/i5bH9kogncn86le/Gl7ykpeEd73rXb0Ne1VnbWNAiD99PHsdD1p27doVrr322lH4iT8xDMUAlA5serWgC5OJ83z/+98fDh482NuDtjjdoXwbkUpbdiA+NIOhHcT09T3KvBYjINQvxlWrHRCIHw5f/OIXw4UXXjjaojGUM7jF0vQ51JctwRh245zTWc4OLNOZhxiDXzxIe9/73jeaa5+336S5xnn2cftJXvwhrN9xC77v3y7O/GL3BAKZgFBvSQxeYIhf4RfPcr7zne/s7TcUxcWdzmj2eb7FtfzWt76193vqY8g999xzV72H9fm6iXgi4q677gpvf/vbw3ve857RvIe0tzzfQjj4Dy8ABIR6a4DAaoEhhvo05yjR960oxYvtTj311F6fpU+hL9b0yJEjvQ/1cb433HDDyoWifd9vnS6QLR645AZ9fX8f2jajvtbRvBYr4Ez9Yn213gGBoYX6NN8HH3yw1wG3avvNJZdc0su7heRbUYYaguLZ+77WuHjQFi92jz99P5BJr+Ohbz3qwEepIbZAQKhvQREMoVmBIYX6IQf6YgDq4/UTxdsc5q+oPm9Jyefa5332ZaE+zn8I18YMYY7NfhLqvQ8CQn0fqmgOcwkMJdQPactNXBBlZ/aGUus4/yGcqS8Len0+o1t2R6chrOkhzHGuDzFPJvD/Cwj1lsLgBYbygTG0i8xSqI1/eCndsnNIt8MbQqjPt9oM4WLo/EBmCHvqh7CWB/9BDKAWAaG+FkaNdFlgCKE+3QruoYceOqpUZ511Vm8vlk1h4NOf/vRo3n2/ULZY3KEEofwOOH3/I3Jpu036y9BDuPtNn7dUdfmz09jbJyDUt68mRkSAAAECBAgQIEBgJgGhfiYuDyZAgAABAgQIECDQPgGhvn01MSICBAgQIECAAAECMwkI9TNxeTABAgQIECBAgACB9gkI9e2riRERIECAAAECBAgQmElAqJ+Jy4MJECBAgAABAgQItE9AqG9fTYyIAAECBAgQIECAwEwCQv1MXB5MgAABAgQIECBAoH0CQn37amJEBAgQIECAAAECBGYSEOpn4vJgAgQIECBAgAABAu0TEOrbVxMjIkCAAAECBAgQIDCTgFA/E5cHEyBAgAABAgQIEGifgFDfvpoYEQECBAgQIECAAIGZBIT6mbg8mAABAgQIECBAgED7BIT69tXEiAgQIECAAAECBAjMJCDUz8TlwQQIECBAgAABAgTaJyDUt68mRkSAAAECBAgQIEBgJgGhfiYuDyZAgAABAgQIECDQPgGhvn01MSICBAgQIECAAAECMwkI9TNxeTABAgQIECBAgACB9gkI9e2riRERIECAAAECBAgQmElAqJ+Jy4MJECBAgAABAgQItE9AqG9fTYyIAAECBAgQIECAwEwCQv1MXB5MgAABAgQIECBAoH0CQn37amJEBAgQIECAAAECBGYSEOpn4vJgAgQIECBAgAABAu0TEOrbVxMjIkCAAAECBAgQIDCTgFA/E5cHEyBAgAABAgQIEGifgFDfvpoYEQECBAgQIECAAIGZBIT6mbg8mAABAgQIECBAgED7BIT69tXEiAgQIECAAAECBAjMJCDUz8TlwQQIECBAgAABAgTaJyDUt68mRkSAAAECBAgQIEBgJgGhfiYuDyZAgAABAgQIECDQPgGhvn01MSICBAgQIECAAAECMwkI9TNxeTCB+gQOHToUzj///FGDN954Y9i0aVN9jWuJAAECBAgQGJSAUD+ocptsmwT27NkTXv3qV4dzzjmnTcMyFgIECBAgQKCDAkJ9B4tmyN0XOHz4cDhy5Iiz890vpRkQIECAAIFWCAj1rSiDQfRF4NZbbw3bt28/ajq7d+8+6ox8PFP/yCOPhKuvvjps2LBh5TlpW85Xv/rVcNNNN4UTTzxxVXvxgOCyyy4LH/3oR1f+/ayzzjqqnTLTu+++O5x77rkrvzruuOOm7uOCCy4I27ZtW9XsAw88EM4777zw0EMPHdXdqaeeumpbUZzvvn37Sktd9BnXZnxy8bH5fOLvb7755nDaaaeN+kmWn/70pyeOr+rxZbVL/Rb7Sh3E38W5xi1V8Sduscr7z23i49K8r7nmmtH4y+ZWnETqO/YVH1u2hSv+Lv7Euo3zj4+JY/r1X//18Cu/8iul9ZzVftxrOs31e77ne6baepaPPfcre00U+0+vj3ggHesx6bX14IMPHjWu9Nouex1Mux6KW+wmvc5zv+J7y6T5V70fRPe0Fj7ykY+svB7L1mPsv+iezztfn2WvhTSHNNd3vvOdvpnsy4edebRSQKhvZVkMqosCKVhs3rx5Vfgt+/eqx6YP0o9//OPh4MGDIQW85FH1vHHBLj03PuaOO+5YFeLLwkgKXGecccaqeZT1EZ9/ySWXlB4YFA9a4hjigUhuUxZmY3i55ZZbJquODOQAAA0GSURBVIa9sr7T2C+88MJReCiGmPzgKD+oShbFEJ+84ziLB18p7JSFpzj+u+66a/T4z3zmM6U+ZWPP/63qoK/42igetOQHH2nsp59++lFBKj+ASG3WaV/1Gk7j+sd//MfRQ+I889oUn1vmEMd5ww03rKy7qvnkY0jr4UUvetHoACbvN7b7l3/5l+GNb3xj+IVf+IWVg+3U/mte85rw4he/+KgD6GnXQ/HgPT6n6nWejzt/7ZW9toprtGrtpC1/X/ziF1cdCOZ1z99n8rWUv2+M8y8ecI0L/l18zzdmAm0TEOrbVhHj6azAuLNRebhMj41nzdJZ5WLAvfLKK8Of//mfhzyQVYXUqvamCWvFEJrOZJadUSs7oBgXAsvOWI9rN811mjCbwmD83/zbgzxUp7Pm+YXIxfE99thjo28c8oOo2H5e1+Rw0kknhU984hPh0ksvXQmHudE4n+JZ9NhPcdzjDoKKL5C0Ht7+9rePvg0oHniMC1rFuRdd6rQvBtjimNPBS7SO/eWvgbKDlvwxZSGz6uCy2F4y3rhxY/jxH//xVa+9WOf4uvuRH/mR8LnPfe6oA9r4rVp0jgcTxW9FZlkPaSypNlWv80kGxfeBuHbzuZe9T6Sap29jims9f/8oWx/JbseOHWHnzp1HHaDn6znNIR28THMA19k3fwMn0BIBob4lhTCM7gtMc7YqBdeqM9zpgzGeaY5ntstCfVX4rBKcFPiLz5sU6vIP+6oP8jykxu056Wv//OxocXzf933fV3lGP5/fpLHmY8hDZprLBz7wgVG4LPsWoXhQlM6+pwOfOJ+4TSP+pIud8wOAcWMsWq5fv37VvKfdrpDaiEHz937v98K73/3ulQOMab9FSS7jvj1ai33ZeiyG8be+9a2jrTDThPpJ2zam/YYhrdc4tje/+c2rQn1sI/7Es9jFC9jzAJ0fJBbX76T1UAy68f9Xvc4nvRuO+wYqPjf//bhvbeLjy16D+XtP8aCz7ICt7L0gPSdu78kPOifN0e8JEJhdQKif3cwzCJQKjAtR+YdqWQjJP4irAnPaW1u1DzYfXNVZ2fxx0wTJac++51/PV4WufHtLMTAXv8EoAx+39SQPT/nZ/Pj75PuOd7yj8ix9WagvHqDE3+/atStce+21o4ueiwd2kw5QxllOCm1l44pbq2IgTXOdZJ4fxMxy8DeNfVnNyg5kyrYHFZ+b1lLV9R/FWpbVObVVfA2mf0sHY9E71jEeSMdvO4rfvhRfh2V1Kf7buPWQ1vO0r/Oqt9lpDr7KtpZVfWMV+ymulXjmv+wAfFyoLzuhUaz17//+74+mM64+PlYIEJhfQKif31ALBEYC484W5qE+/9Ct2tqSzg7nZ8aKoWpc2EnjqmqnWLppgmTZlpWyi2TzMaU9x2VLpXgB3rQXhxbbKV5AWGxr3NnJ4p7sGGLGBZ7cMIbntOc/nWGPZ5LTxa2prfi8eCa66izzuO0/VRdcxzaTbfzf4nUKKZimA4yqg8Kqg7c67ccdhBU9xn3TU2wjvxB22ouhYxtpTeRn1IsHQGmv+fHHH79qLeQH6mUHPsXX/bj1EA/6Zn2dVxmUXbydHhvnUrx2ZtJZ+rTWkmnV+1hVrcquwSm6pXU66eDNxwgBAvMLCPXzG2qBwEhgXECZ9PV21YWTk8Jm6jfeVabqIrRpg9M0oX7SRaBlgaAsyFQFy2m3UZQtuRQu4kWO8WxrOus/6c4303yTUTTMD8jimFNALPqM23KUHyjEC2qLtZ5ma1HVXv8YmtO3BFUXyVadiZ3mAuVp7Mu2Z5TVdtKWjrK+UghNwXbai2SLr7F4IHfnnXeOzhwXD4biGeV0R6rYd35xd1lAnnY9RJN5XufJIs03XQye/j2NLb9zT+wz1jW/y1Z678gvni+rSdWBQToQzO9eVVxfs3wD5KOEAIH5BIT6+fw8m8BIYJqzYWV7suPZ3fwsZJG0eMZ7XAAaFwLHPa/4gXvyySeP3eOch8hxH/7FrR1VH+plAWeWMFu2F7sY8KLjNBdPTjqYKdtSUwzLKRTGiwfjxYdp7uMOFnKTeS6SjWbpOoV0gDFuS9G4M7Flt1gtrsdxAW1cuC5+s5S/ZVRtIxvnV5zD/fffP1Wd8wOuP/zDPxxts4nXI8T99fl2qXHfXKSz/2Wv+6r1MO3rfJq31PxAPfnGbxqK4b2qXlUHAFUnJ8peI/kZ/jTucd/KufvNNNX1GAJrFxDq127nmQRWBMaFnbI7SxTDZnErSPEi0mnuSJEGMC64j9vrnz9v3Fn9PAxWPTbvryo0T7ptXtXyGncAlW+xmObM86QDsuKBRtWe//iY173udeGDH/zgynabcTUZF+KnPbNZFnrjc9MdXOLZ07K/c1B24DTJID8bXPYNwLhxxz7jePN76Y87oBr3u+LcP/axj63cQrTqjjt5WI1tx1D/3ve+N3zoQx8aXbBarG06wI0H3ePurjTreijehjOZjnOrOrBJ24XSbVvjxfP5LWhj+2VrsOoAYNx7Sf5az7f4jPsoWMu3MT5aCBBYm4BQvzY3zyKwSqAqOJdtMymG49hI3HddFh7yoFX1YZxflJqXpuqsXFnQSn0Ux1P2/HEX1Zad0a8K12UW4+6GUgwe8Y98Fc/85RfdxgA16cxzaq9sS0OZd1XQjDV4//vfv/K3BcZdJJsfHOVek745mHQgF+v6+c9/Pnz9618/KkRXXWQ57YFECoqT7Ivhumq7SGxrXL9V6zZvb5rtZWWvpXTwc+yxx45ef0X3T33qU6vug198TeXXQpRtZcrXw7iDhGkOUovXIZTt4Y/jq/ojdvnrqeoAq/haKM4pP+kw6f0mf/+Zpj4+TggQqEdAqK/HUSsDF0hfRecMZRex5vuzy85gpnbyD8Syr/CnvQtOPsayv4wZ+y3rI3/spH3MxeBQ3Kecn0ktBrS4daD4125zy/wPPZVtj0h7e6e5Q0jeftkWkfyCxKqtK+m5sc1xf0k2/r7MMg9RZX+VOI03Pj/dCrHsNpzJpay+47ZC1WVfdM0PtPL6T/MNQb6do/iaGretJ44jPTb+/+KtYMvGlWqb7uOe71kvC70x/JcdsObrIb4GZnmdFw3zORZf71VbhOK83/SmN42+fSj72wVlF7cXD5CL7Rb7G7eFqGy9TVPfgX90mD6BWgWE+lo5NUaAAAECBAgQIEBg+QJC/fLN9UiAAAECBAgQIECgVgGhvlZOjREgQIAAAQIECBBYvoBQv3xzPRIgQIAAAQIECBCoVUCor5VTYwQIECBAgAABAgSWLyDUL99cjwQIECBAgAABAgRqFRDqa+XUGAECBAgQIECAAIHlCwj1yzfXIwECBAgQIECAAIFaBYT6Wjk1RoAAAQIECBAgQGD5AkL98s31SIAAAQIECBAgQKBWAaG+Vk6NESBAgAABAgQIEFi+gFC/fHM9EiBAgAABAgQIEKhVQKivlVNjBAgQIECAAAECBJYvINQv31yPBAgQIECAAAECBGoVEOpr5dQYAQIECBAgQIAAgeULCPXLN9cjAQIECBAgQIAAgVoFhPpaOTVGgAABAgQIECBAYPkCQv3yzfVIgAABAgQIECBAoFYBob5WTo0RIECAAAECBAgQWL6AUL98cz0SIECAAAECBAgQqFVAqK+VU2MECBAgQIAAAQIEli8g1C/fXI8ECBAgQIAAAQIEahUQ6mvl1BgBAgQIECBAgACB5QsI9cs31yMBAgQIECBAgACBWgWE+lo5NUaAAAECBAgQIEBg+QJC/fLN9UiAAAECBAgQIECgVgGhvlZOjREgQIAAAQIECBBYvoBQv3xzPRIgQIAAAQIECBCoVUCor5VTYwQIECBAgAABAgSWLyDUL99cjwQIECBAgAABAgRqFRDqa+XUGAECBAgQIECAAIHlCwj1yzfXIwECBAgQIECAAIFaBYT6Wjk1RoAAAQIECBAgQGD5AkL98s31SIAAAQIECBAgQKBWAaG+Vk6NESBAgAABAgQIEFi+gFC/fHM9EiBAgAABAgQIEKhVQKivlVNjBAgQIECAAAECBJYvINQv31yPBAgQIECAAAECBGoVEOpr5dQYAQIECBAgQIAAgeULCPXLN9cjAQIECBAgQIAAgVoFhPpaOTVGgAABAgQIECBAYPkCQv3yzfVIgAABAgQIECBAoFYBob5WTo0RIECAAAECBAgQWL6AUL98cz0SIECAAAECBAgQqFVAqK+VU2MECBAgQIAAAQIEli8g1C/fXI8ECBAgQIAAAQIEahUQ6mvl1BgBAgQIECBAgACB5QsI9cs31yMBAgQIECBAgACBWgWE+lo5NUaAAAECBAgQIEBg+QJC/fLN9UiAAAECBAgQIECgVgGhvlZOjREgQIAAAQIECBBYvoBQv3xzPRIgQIAAAQIECBCoVUCor5VTYwQIECBAgAABAgSWLyDUL99cjwQIECBAgAABAgRqFfj/AJphKd4O7NyaAAAAAElFTkSuQmCC', `mes` = '8', `anio` = '2024', `tipo` = '7', `semana` = '32'
WHERE `id` = '281' 
 Execution Time:0.20267701148987

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-8-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 8
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.17235279083252

SELECT DISTINCT(os.id), `la`.`nombre` as `actividad`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, WEEKOFYEAR(os.fecha) AS num_semana, `os`.`ot`, `os`.`observaciones`, `os`.`seguimiento`
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 8
AND DATE_FORMAT(os.fecha, '%Y') = 2024
AND (`os`.`seguimiento` = 1 or `os`.`seguimiento` = 4 or `os`.`seguimiento` = 5)
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.19223713874817

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.13909292221069

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 1 
 Execution Time:0.15322613716125

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 3 
 Execution Time:0.14139699935913

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 4 
 Execution Time:0.16963601112366

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 5 
 Execution Time:0.14969182014465

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.20216202735901

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 1 
 Execution Time:0.18146395683289

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 3 
 Execution Time:0.21161198616028

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 4 
 Execution Time:0.14738893508911

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 5 
 Execution Time:0.14300298690796

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12' 
 Execution Time:0.17959690093994

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 1 
 Execution Time:0.13993096351624

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 3 
 Execution Time:0.18608808517456

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 4 
 Execution Time:0.14626312255859

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 5 
 Execution Time:0.14655804634094

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13' 
 Execution Time:0.14988899230957

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 1 
 Execution Time:0.21295285224915

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 3 
 Execution Time:0.14011406898499

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 4 
 Execution Time:0.16471099853516

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 5 
 Execution Time:0.20525097846985

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19' 
 Execution Time:0.14319396018982

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 1 
 Execution Time:0.14904403686523

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 3 
 Execution Time:0.14534497261047

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 4 
 Execution Time:0.1736421585083

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 5 
 Execution Time:0.14403581619263

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20' 
 Execution Time:0.15741205215454

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 1 
 Execution Time:0.14303302764893

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 3 
 Execution Time:0.14396715164185

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 4 
 Execution Time:0.17134690284729

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 5 
 Execution Time:0.14703917503357

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26' 
 Execution Time:0.14461803436279

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 1 
 Execution Time:0.17085599899292

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 3 
 Execution Time:0.17537307739258

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 4 
 Execution Time:0.15115284919739

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 5 
 Execution Time:0.14235997200012

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27' 
 Execution Time:0.139643907547

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 1 
 Execution Time:0.1425518989563

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 3 
 Execution Time:0.14045310020447

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 4 
 Execution Time:0.14013910293579

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 5 
 Execution Time:0.1675341129303

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28' 
 Execution Time:0.14623188972473

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 1 
 Execution Time:0.14066505432129

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 3 
 Execution Time:0.14107203483582

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 4 
 Execution Time:0.14488005638123

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 5 
 Execution Time:0.21518492698669

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29' 
 Execution Time:0.20992493629456

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 1 
 Execution Time:0.14743900299072

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 3 
 Execution Time:0.14719295501709

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 4 
 Execution Time:0.14760088920593

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 5 
 Execution Time:0.15250110626221

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30' 
 Execution Time:0.14006805419922

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 1 
 Execution Time:0.16396403312683

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 3 
 Execution Time:0.15670084953308

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 4 
 Execution Time:0.16142702102661

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 5 
 Execution Time:0.14456081390381

SELECT *
FROM `proyectos`
WHERE `id` = '36' 
 Execution Time:0.11250305175781

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-08-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 08
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.17257905006409

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.21504211425781

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.17347002029419

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 1 
 Execution Time:0.15352201461792

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 3 
 Execution Time:0.1407949924469

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 4 
 Execution Time:0.14802598953247

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05'
AND `seguimiento` = 5 
 Execution Time:0.15607285499573

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.16279602050781

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.21315908432007

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 1 
 Execution Time:0.30720901489258

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 3 
 Execution Time:0.59078907966614

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 4 
 Execution Time:0.31369805335999

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06'
AND `seguimiento` = 5 
 Execution Time:0.17849111557007

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12' 
 Execution Time:0.15657806396484

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.16262006759644

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 1 
 Execution Time:0.17286014556885

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 3 
 Execution Time:0.14891695976257

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 4 
 Execution Time:0.29583311080933

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-12'
AND `seguimiento` = 5 
 Execution Time:0.18983697891235

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13' 
 Execution Time:0.57509088516235

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:1.6547510623932

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 1 
 Execution Time:0.86091995239258

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 3 
 Execution Time:1.5267951488495

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 4 
 Execution Time:0.37260007858276

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-13'
AND `seguimiento` = 5 
 Execution Time:0.16766595840454

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19' 
 Execution Time:0.34510517120361

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.19780898094177

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 1 
 Execution Time:0.2893590927124

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 3 
 Execution Time:0.96306395530701

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 4 
 Execution Time:1.8122129440308

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-19'
AND `seguimiento` = 5 
 Execution Time:3.4353339672089

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20' 
 Execution Time:1.0276389122009

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:1.9534039497375

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 1 
 Execution Time:0.45689988136292

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 3 
 Execution Time:0.24502682685852

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 4 
 Execution Time:0.14003705978394

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-20'
AND `seguimiento` = 5 
 Execution Time:0.31631898880005

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26' 
 Execution Time:0.18802905082703

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.16123604774475

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 1 
 Execution Time:0.18251991271973

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 3 
 Execution Time:0.15017199516296

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 4 
 Execution Time:0.14290714263916

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-26'
AND `seguimiento` = 5 
 Execution Time:0.16022109985352

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27' 
 Execution Time:0.14991903305054

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.18491005897522

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 1 
 Execution Time:0.14878702163696

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 3 
 Execution Time:0.18753886222839

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 4 
 Execution Time:0.15080690383911

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-27'
AND `seguimiento` = 5 
 Execution Time:0.17058086395264

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28' 
 Execution Time:0.15274000167847

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.16445112228394

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 1 
 Execution Time:0.15128302574158

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 3 
 Execution Time:0.16834592819214

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 4 
 Execution Time:0.14604687690735

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-28'
AND `seguimiento` = 5 
 Execution Time:0.14402604103088

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29' 
 Execution Time:0.14358496665955

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.15443205833435

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 1 
 Execution Time:0.14615607261658

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 3 
 Execution Time:0.15715384483337

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 4 
 Execution Time:0.21048784255981

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-29'
AND `seguimiento` = 5 
 Execution Time:0.17990112304688

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30' 
 Execution Time:0.17617607116699

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '08' 
 Execution Time:0.26322889328003

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 1 
 Execution Time:0.41507887840271

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 3 
 Execution Time:0.45469784736633

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 4 
 Execution Time:0.53265905380249

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-30'
AND `seguimiento` = 5 
 Execution Time:0.52145314216614

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-09-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 09
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.19070792198181

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01' 
 Execution Time:0.13935708999634

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.19415593147278

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 1 
 Execution Time:0.14263200759888

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 3 
 Execution Time:0.16336798667908

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 4 
 Execution Time:0.15000414848328

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-01'
AND `seguimiento` = 5 
 Execution Time:0.16115093231201

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02' 
 Execution Time:0.16460013389587

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.16277480125427

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 1 
 Execution Time:0.26350712776184

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 3 
 Execution Time:0.20170998573303

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 4 
 Execution Time:0.14224791526794

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-02'
AND `seguimiento` = 5 
 Execution Time:0.16423201560974

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03' 
 Execution Time:0.14017391204834

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.15182185173035

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 1 
 Execution Time:0.16012191772461

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 3 
 Execution Time:0.24602103233337

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 4 
 Execution Time:0.16605687141418

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-03'
AND `seguimiento` = 5 
 Execution Time:0.28643918037415

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09' 
 Execution Time:0.38621401786804

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.20335102081299

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 1 
 Execution Time:0.21515703201294

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 3 
 Execution Time:0.21209788322449

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 4 
 Execution Time:0.36132192611694

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-09'
AND `seguimiento` = 5 
 Execution Time:0.25873303413391

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10' 
 Execution Time:0.61025285720825

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.16258215904236

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 1 
 Execution Time:0.23057913780212

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 3 
 Execution Time:0.21070504188538

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 4 
 Execution Time:0.19753098487854

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-10'
AND `seguimiento` = 5 
 Execution Time:0.17382597923279

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16' 
 Execution Time:0.23425388336182

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.2112820148468

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 1 
 Execution Time:0.14643287658691

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 3 
 Execution Time:0.15572595596313

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 4 
 Execution Time:0.16977214813232

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-16'
AND `seguimiento` = 5 
 Execution Time:0.52993297576904

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17' 
 Execution Time:0.1694700717926

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.2681040763855

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 1 
 Execution Time:0.15438008308411

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 3 
 Execution Time:0.18453192710876

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 4 
 Execution Time:0.1433379650116

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-17'
AND `seguimiento` = 5 
 Execution Time:0.36700701713562

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23' 
 Execution Time:0.25410413742065

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.19973182678223

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 1 
 Execution Time:0.19312000274658

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 3 
 Execution Time:0.21311902999878

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 4 
 Execution Time:0.21524000167847

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-23'
AND `seguimiento` = 5 
 Execution Time:0.22785210609436

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24' 
 Execution Time:0.19842410087585

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.47163391113281

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 1 
 Execution Time:0.20609188079834

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 3 
 Execution Time:0.14425301551819

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 4 
 Execution Time:0.15253901481628

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-24'
AND `seguimiento` = 5 
 Execution Time:0.16834998130798

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30' 
 Execution Time:0.15553903579712

SELECT count(id) as tot_turnos
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%m') = '09' 
 Execution Time:0.26530790328979

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 1 
 Execution Time:0.19374084472656

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 3 
 Execution Time:0.1602680683136

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 4 
 Execution Time:0.14804100990295

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `turno` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-09-30'
AND `seguimiento` = 5 
 Execution Time:0.15441608428955

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-01-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 01
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.22310495376587

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-02-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 02
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.21122908592224

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-03-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 03
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.25972104072571

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-04-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 04
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.16904902458191

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-05-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 05
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.24951505661011

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-06-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 06
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.17937588691711

SELECT DISTINCT(os.id), `os`.`id_limpieza`, `os`.`tipo_act`, `os`.`turno`, `os`.`estatus`, `os`.`fecha`, `pe`.`nombre` as `supervisor`, WEEKOFYEAR(os.fecha) AS num_semana, `c`.`foto` as `logo_cli`, `p`.`turno` as `turno_proy`, `p`.`turno2` as `turno_proy2`, `p`.`turno3` as `turno_proy3`, `la`.`days`, WEEK("2024-07-01", 0) as num_sems
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `personal` `pe` ON `pe`.`personalId`=`os`.`id_supervisor`
JOIN `proyectos` `p` ON `p`.`id`=`os`.`id_proyecto`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%m') = 07
AND DATE_FORMAT(os.fecha, '%Y') = 2024
GROUP BY `os`.`fecha`, `os`.`turno`
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC, `num_semana` ASC 
 Execution Time:0.21344804763794

SELECT DISTINCT(semana)
FROM `ordenes_semanal`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND DATE_FORMAT(fecha,'%Y') = '2024'
AND DATE_FORMAT(fecha,'%m') = '08'
ORDER BY `semana` ASC 
 Execution Time:0.15217185020447

SELECT `la`.`nombre`, `la`.`fecha_ini`, `os`.`id`, `os`.`seguimiento`, `os`.`semana`, DATE_FORMAT(os.fecha, '%Y') as anio, DATE_FORMAT(os.fecha, '%d') as dia, `os`.`fecha_final`, `os`.`fecha`, `os`.`ot`, `os`.`turno`, `os`.`observaciones`, `c`.`foto`, `c`.`empresa`, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=1 and os2.fecha=os.fecha) as cont_can, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=2 and os2.fecha=os.fecha) as cont_pro, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=3 and os2.fecha=os.fecha) as cont_ok, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=4 and os2.fecha=os.fecha) as cont_re, (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=5 and os2.fecha=os.fecha) as cont_no_rea
FROM `ordenes_semanal` `os`
JOIN `limpieza_actividades` `la` ON `la`.`id_proyecto`=`os`.`id_proyecto` and `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
JOIN `clientes` `c` ON `c`.`id`=`la`.`id_cliente`
WHERE `os`.`estatus` = 1
AND `os`.`id_proyecto` = '36'
AND DATE_FORMAT(os.fecha, '%Y') = 2024
AND `os`.`semana` = 32
ORDER BY `os`.`fecha` ASC, `os`.`turno` ASC 
 Execution Time:0.41539001464844

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-05' 
 Execution Time:0.17786002159119

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-06' 
 Execution Time:0.17330288887024

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-07' 
 Execution Time:0.17672300338745

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-08' 
 Execution Time:0.17527198791504

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-09' 
 Execution Time:0.14205098152161

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-10' 
 Execution Time:0.14524602890015

SELECT count(id) as tot_estatus
FROM `ordenes_semanal` `os`
WHERE `estatus` = 1
AND `id_proyecto` = '36'
AND `fecha` = '2024-08-11' 
 Execution Time:0.14769506454468

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '2'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.38080596923828

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:,', `mes` = '8', `anio` = '2024', `tipo` = '2', `semana` = '0'
WHERE `id` = '278' 
 Execution Time:0.093592166900635

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '4'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.21601104736328

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAl8AAADnCAYAAAAguTMsAAAAAXNSR0IArs4c6QAAGhxJREFUeF7t3H+MXMVhB/A5CODDjsWJNAq4qDbQKm1VMKr4kaSAEpCqVsAfCBe5NXUtg1XUfxJs33GXky1bjo87Y1D+qIhcI+RiyTJGTpRGVf+AEIgoYJoKjFpVKmAXyzZKYjlqQu+aGLZ627zV87vdu31z5+e3u5/9b29n3sx8ZnT6at6811er1WrBhwABAgQIECBAoBSBPuGrFGeNECBAgAABAgTqAsKXhUCAAAECBAgQKFFA+CoRW1MECBAgQIAAAeHLGiBAgAABAgQIlCggfJWIrSkCBAgQIECAgPBlDRAgQIAAAQIEShQQvkrE1hQBAgQIECBAQPiyBggQIECAAAECJQoIXyVia4oAAQIECBAgIHxZAwQIECBAgACBEgWErxKxNUWAAAECBAgQEL6sAQIECBAgQIBAiQLCV4nYmiJAgAABAgQICF/WAAECBAgQIECgRAHhq0RsTREgQIAAAQIEhC9rgAABAgQIECBQooDwVSK2pggQIECAAAECwpc1QIAAAQIECBAoUUD4KhFbUwQIECBAgAAB4csaIECAAAECBAiUKCB8lYitKQIECBAgQICA8GUNECBAgAABAgRKFBC+SsTWFAECBAgQINBdArVaLfT19RUaVOHwteFHp8OZjz8u1IjCBAgQIECAAIFuE7jwwgvC1t/tDwsv7S80tMLh66uHfhq+efKiQo0oTIAAAQIECBDoNoHf6a+Ff/2jS4SvbptY4yFAgAABAgSqKSB8VXNe9IoAAQIECBDoUgHhq0sn1rAIECBAgACBagoIX9WcF70iQIAAAQIEulRA+OrSiTUsAgQIECBAoJoCwlc150WvCBAgQIAAgS4VEL66dGINiwABAgQIEKimgPBVzXnRKwIECBAgQKBLBYSvLp1YwyJAgAABAgSqKSB8VXNe9IoAAQIECBDoUgHhq0sn1rAIECBAgACBagoIX9WcF70iQIAAAQIEulRA+OrSiTUsAgQIECBAoJoCwlc150WvCBBoU2Dr5xeEod++JPzkl5+Ev/zRZPj+T8/Ua6Z/v/iC6Rd68Sdnwp3//NG0H1744sJwx2986qy/tyrbZvcUI0CAwDQB4cuiIECgIwW+8plPhb//w/6wZMH/p6vjU+2Hr6R8NlQ9cNVF4W+v6w+f/lRfU4ufn6mFvzk8GZ499quOtNJpAgSqJSB8VWs+9IYAgTYEmoWlmcLX0//1y/DgW5Nn7Yal5ZPmsiEuLZv8PbsT9u8//yT8/vd/3kbvFCFAgMDMAsKXFUKAQMcJpOHr0OmPwxULLgi/9+kLZtz5SgNVdrcsDV9//psXhbW/dXHdoNktxn/7yqfr1//lJyGM/+f/hk3/MdVxXjpMgEC1BISvas2H3hAgUFAgDUft7HztXt4/LWilu1utwlW2TnZXrGA3FSdAgEBDQPiyGAgQ6GiBdsJXfoDZoDbbzpbw1dHLQ+cJVFJA+KrktOgUAQLtChQNX/kdMjtf7UorR4DAfAkIX/Ml6ToECJwXgXbCV3K78OVTZxpPNGYDWLNbkdmBpNf3xON5mV6NEuhKAeGrK6fVoAj0jkC74St52rFZ0Mq/ssLTjr2zdoyUwPkSEL7Ol7x2CRCYF4Ei4StpsNkZr5leyJrUyd+qnJeOuwgBAj0rIHz17NQbOIHuECgavrLvCMvfSkyvlZXxhGN3rBOjIFAlAeGrSrOhLwQIECBAgEDXCwhfXT/FBkiAAAECBAhUSUD4qtJs6AsBAgQIECDQ9QLCV9dPsQESIECAAAECVRIQvqo0G/pCgAABAgQIdL2A8NX1U2yABAgQIECAQJUEhK8qzYa+ECBAgAABAl0vIHx1/RQbIAECBAgQIFAlgc8vDOFfvnhxWHhpf6Fu9dVqtVqRGl899NPwzZMXFamiLAECBAgQIECg6wSEr66bUgMiQIAAAQIEqiwgfFV5dvSNAAECBAgQ6DqB0sLXPx3/n/DGLy7sOkADIkCAAAECBAgUEfjMRSH81ec+OfdnvpLwteCSi4v0TVkCBAgQIECAQNcJ/OrMmfDFxbVzH76OHDsZll11RdcBGhABAgQIECBAoIjAj0/9LCzsv0T4KoKmLAECBAgQIEAgVkD4ipVTjwABAgQIECAQISB8RaCpQoAAAQIECBCIFRC+YuXUI0CAAAECBAhECAhfEWiqECBAgAABAgRiBYSvWDn1CBAgQIAAAQIRAsJXBJoqBAgQIECAAIFYAeErVk49AgQIECBAgECEgPAVgaYKAQIECBAgQCBWQPiKlVOPAAECBAgQIBAhIHxFoKlCgAABAgQIEIgVEL5i5dQjQIAAAQIECEQICF8RaKoQIECAAAECBGIFhK9YOfUIEChN4I1Db4aVq1Y32tu3d0+4+aYbW7bfTvkDzx8MQyOj9WssWXJleGb3rnDtNVfXv58+fTqsXfdweOvtw/Xv+faSuq++9noY27Y19PcvKM1BQwQIdIeA8NUd82gUBLpWIAlSG4aGG+Eo/z0/8HbKJ+Fp3/7nwtO7ngoDAwMh/33i8SfCsqVLw4r77g3J9cZ37GyUffe998P2xybCzomxel0fAgQIFBUQvoqKKU+AQGkCk5NTYXh0U/jSF26pB6H0k4Sj5DO44ZGz+tJO+XRXa2jj+sbuWbbenXd8OWzZNhY2jw7Xw1Xy2/bxibD6gVX1nbGk7dtvu3XGnbfSgDREgEBHCghfHTltOk2gNwSSXaaNQ8Nhx/hY45ZgMvJWt/3aKX/4nXfO2snKB7qH1q5pGb5OnToVXn7lh9NCX2/MhlESIDBfAsLXfEm6DgEC8y6Qv+WXNjCXv7/w4ktNz2ulgW7T10fC1m9sb+y2JW3t238gDK7/WpjY+WRjR2zeB+uCBAj0jIDw1TNTbaAEOk9gLiErex4re53ZwldyiH5qarJx4D49jH/w29+pnwO74YblYc2D68Lx4yfC8uuva5wF6zxdPSZA4HwJCF/nS167BAjMKnC+wlf+Ccbs7tfo5i1h5NHB+m3QZLfsyNGjbkPOOpMKECCQFRC+rAcCBCor0M4ZrmxQaqf8bGe+8of4kwP66weH64Erf+YraW/Ps3vDyNCgV05UdhXpGIHqCQhf1ZsTPSJA4NcCzZ5MTH7Kvgoii9VO+WYBLX3aceX9K6Y9xZh/7UT2wL3wZakSIBAjIHzFqKlDgEBpAvl3cOVvRc72zq5mty6TQHXi5IeNl6S2enoyvd2Yvkw1H9xavfKiNBwNESDQkQLCV0dOm04T6C2B7Nvo84fc8+ErkZmpfCqXBKdv7dpd/3rP3XdNe1t9q1uY2bfnN6vXWzNjtAQIxAgIXzFq6hAgQIAAAQIEIgWEr0g41QgQIECAAAECMQLCV4yaOgQIECBAgACBSAHhKxJONQIECBAgQIBAjIDwFaOmDgECBAgQIEAgUkD4ioRTjQABAgQIECAQIyB8xaipQ4AAAQIECBCIFBC+IuFUI0CAAAECBAjECAhfMWrqECBAgAABAgQiBYSvSDjVCBAgQIAAAQIxAsJXjJo6BAgQIECAAIFIgVLD12WLF0V2UzUCBAgQIECAQHcIfDQ5FQYWLwoLL+0vNKC+Wq1WK1LjyLGTQfgqIqYsAQLnSqCvry+0+heW/a3dcvl+zvc1ZnKI7WP2mq7R/kqbj7ltt7WZ5sU12hUop1zRuSo1fC276opyFLRCgAABAgQIEKioQKm3HYWviq4C3SJAgAABAgRKExC+SqPWEAECBAgQIEAgBOHLKiBAgAABAgQIlCggfJWIrSkCBAgQIECAgPBlDRAgQIAAAQIEShQQvkrE1hQBAgQIECBAQPiyBggQIECAAAECJQoIXyVia4oAAQIECBAgIHxZAwQIECBAgACBEgWErxKxNUWAAAECBAgQEL6sAQIECBAgQIBAiQLCV4nYmiJAgAABAgQICF/WAAECBAgQIECgRAHhq0RsTREgECfwxqE3w8pVqxuV9+3dE26+6caWF2un/IHnD4ahkdH6NZYsuTI8s3tXuPaaq+vfT58+Hdauezi89fbh+vd8e0ndV197PYxt2xr6+xfEDUotAgR6VkD46tmpN3ACnSGQBKkNQ8ONcJT/nh9FO+WT8LRv/3Ph6V1PhYGBgZD/PvH4E2HZ0qVhxX33huR64zt2Nsq++977YftjE2HnxFi9rg8BAgSKCghfRcWUJ0CgNIHJyakwPLopfOkLt9SDUPpJwlHyGdzwyFl9aad8uqs1tHF9Y/csW+/OO74ctmwbC5tHh+vhKvlt+/hEWP3AqvrOWNL27bfdOuPOW2lAGiJAoCMFhK+OnDadJtAbAsku08ah4bBjfKxxSzAZeavbfu2UP/zOO2ftZOUD3UNr17QMX6dOnQovv/LDaaGvN2bDKAkQmC8B4Wu+JF2HAIF5F8jf8ksbmMvfX3jxpabntdJAt+nrI2HrN7Y3dtuStvbtPxAG138tTOx8srEjNu+DdUECBHpGQPjqmak2UAKdJzCXkJU9j5W9zmzhKzlEPzU12Thwnx7GP/jt79TPgd1ww/Kw5sF14fjxE2H59dc1zoJ1nq4eEyBwvgSEr/Mlr10CBGYVOF/hK/8EY3b3a3TzljDy6GD9NmiyW3bk6FG3IWedSQUIEMgKCF/WAwEClRVo5wxXNii1U362M1/5Q/zJAf31g8P1wJU/85W0t+fZvWFkaNArJyq7inSMQPUEhK/qzYkeESDwa4FmTyYmP2VfBZHFaqd8s4CWPu248v4V055izL92InvgXviyVAkQiBEQvmLU1CFAoDSB/Du48rciZ3tnV7Nbl0mgOnHyw8ZLUls9PZnebkxfppoPbq1eeVEajoYIEOhIAeGrI6dNpwn0lkD2bfT5Q+758JXIzFQ+lUuC07d27a5/vefuu6a9rb7VLczs2/Ob1eutmTFaAgRiBISvGDV1CBAgQIAAAQKRAsJXJJxqBAgQIECAAIEYAeErRk0dAgQIECBAgECkgPAVCacaAQIECBAgQCBGQPiKUVOHAAECBAgQIBApIHxFwqlGgAABAgQIEIgREL5i1NQhQIAAAQIECEQKCF+RcKoRIECAAAECBGIEhK8YNXUIECBAgAABApECwlcknGoECBAgQIAAgRgB4StGTR0CBAgQIECAQKRAqeHrssWLIrupGgECBAgQIECgOwQ+mpwKA4sXhYWX9hcaUF+tVqsVqXHk2MkgfBURU5YAgXMl0NfXF1r9C8v+1m65fD/n+xozOcT2MXtN12h/pc3H3Lbb2kzz4hrtCpRTruhclRq+ll11RTkKWiFAgAABAgQIVFSg1NuOwldFV4FuESBAgAABAqUJCF+lUWuIAAECBAgQIBCC8GUVECBAgAABAgRKFBC+SsTWFAECBAgQIEBA+LIGCBAgQIAAAQIlCghfJWJrigABAgQIECAgfFkDBAgQIECAAIESBYSvErE1RYAAAQIECBAQvqwBAgQIECBAgECJAsJXidiaIkCAAAECBAgIX9YAAQIECBAgQKBEAeGrRGxNESBAgAABAgSEL2uAAAECBAgQIFCigPBVIramCBCIE3jj0Jth5arVjcr79u4JN990Y8uLtVP+wPMHw9DIaP0aS5ZcGZ7ZvStce83V9e+nT58Oa9c9HN56+3D9e769pO6rr70exrZtDf39C+IGpRYBAj0rIHz17NQbOIHOEEiC1Iah4UY4yn/Pj6Kd8kl42rf/ufD0rqfCwMBAyH+fePyJsGzp0rDivntDcr3xHTsbZd997/2w/bGJsHNirF7XhwABAkUFhK+iYsoTIFCawOTkVBge3RS+9IVb6kEo/SThKPkMbnjkrL60Uz7d1RrauL6xe5atd+cdXw5bto2FzaPD9XCV/LZ9fCKsfmBVfWcsafv2226dceetNCANESDQkQLCV0dOm04T6A2BZJdp49Bw2DE+1rglmIy81W2/dsoffueds3ay8oHuobVrWoavU6dOhZdf+eG00Ncbs2GUBAjMl4DwNV+SrkOAwLwL5G/5pQ3M5e8vvPhS0/NaaaDb9PWRsPUb2xu7bUlb+/YfCIPrvxYmdj7Z2BGb98G6IAECPSMgfPXMVBsogc4TmEvIyp7Hyl5ntvCVHKKfmppsHLhPD+Mf/PZ36ufAbrhheVjz4Lpw/PiJsPz66xpnwTpPV48JEDhfAsLX+ZLXLgECswqcr/CVf4Ixu/s1unlLGHl0sH4bNNktO3L0qNuQs86kAgQIZAWEL+uBAIHKCrRzhisblNopP9uZr/wh/uSA/vrB4Xrgyp/5Strb8+zeMDI06JUTlV1FOkagegLCV/XmRI8IEPi1QLMnE5Ofsq+CyGK1U75ZQEufdlx5/4ppTzHmXzuRPXAvfFmqBAjECAhfMWrqECBQmkD+HVz5W5GzvbOr2a3LJFCdOPlh4yWprZ6eTG83pi9TzQe3Vq+8KA1HQwQIdKSA8NWR06bTBHpLIPs2+vwh93z4SmRmKp/KJcHpW7t217/ec/dd095W3+oWZvbt+c3q9dbMGC0BAjECwleMmjoECBAgQIAAgUgB4SsSTjUCBAgQIECAQIyA8BWjpg4BAgQIECBAIFJA+IqEU40AAQIECBAgECMgfMWoqUOAAAECBAgQiBQQviLhVCNAgAABAgQIxAgIXzFq6hAgQIAAAQIEIgWEr0g41QgQIECAAAECMQLCV4yaOgQIECBAgACBSAHhKxJONQIECBAgQIBAjIDwFaOmDgECBAgQIEAgUqDU8HXZ4kWR3VSNAAECBAgQINAdAh9NToWBxYvCwkv7Cw2or1ar1YrUOHLsZBC+iogpS4DAuRLo6+sLrf6FZX9rt1y+n/N9jZkcYvuYvaZrtL/S5mNu221tpnlxjXYFyilXdK5KDV/LrrqiHAWtECBAgAABAgQqKlDqbUfhq6KrQLcIECBAgACB0gSEr9KoNUSAAAECBAgQCEH4sgoIECBAgAABAiUKCF8lYmuKAAECBAgQICB8WQMECBAgQIAAgRIFkjdAfPbyy8p51YQD9yXOrKYIECBAgACBSgoIX5WcFp0iQIAAAQIEulVA+OrWmTUuAgQIECBAoJICwlclp0WnCBAgQIAAgW4VEL66dWaNiwABAgQIEKikgPBVyWnRKQIECBAgQKBbBYSvbp1Z4yJAgAABAgQqKSB8VXJadIoAAQIECBDoVgHhq1tn1rgIdKjA5ORUGB7dFL77D9+rj2D59deFp3c9FQYGBurf87+nw9y3d0+4+aYbm4763ffeD2seXBeOHz9R/318+7aw4r57G2XfOPRmWLlqdeP7PXffFca2bQ39/Quattms/viOnWf1s0P5dZsAgRIEhK8SkDVBgEB7AmmwuvKKz4XBDY/UK008/kQ4cfLDRhhKy6y8f0XLsJVtLQ1ej4+P1cvnvydlf/DyK+H66/6gHvDS6yd/TwPYgecPhiNHj9b7lNTfODQcdoyPhWuvuTqcPn06rB8cDiOPDta/+xAgQGA2AeFrNiG/EyBQmkCyA5XfQUrCzdp1D4ehjevr4Sn/fbbOJcHp1ddeP2snKwl0yScNePlrZPuxYEF/2D4+EVY/sKoRrpL6t992a70/yfWTT3YnbbY++Z0Agd4WEL56e/6NnkClBJLQs2//gbOCUrr7tWzp0nrAKbrTdC7D1+WXXx72PLs3jAwNNm5RVgpUZwgQqKSA8FXJadEpAr0p0GznK38rMn9+K5Ga6bxXulO28v4/q4e3Zrcds9r58mn4S3fKkvrbH5sI27ZsDk/t+ruzdsR6c9aMmgCBogLCV1Ex5QkQOGcCafC55eabGrcEk52roZHR8NfrHmx6mzA9LN9OAHvr7cP1vufLZg/xL1lyZXhm966zzm/lD/kn9T/44Fj9HNhDa9fUb4sm125W95xhuTABAh0rIHx17NTpOIHuFEgDWBqUkicLk5CT3nZsNupmtxbTcvndtGaH+rPXTH//4IMPWj69mN39mtj5ZEgP/7e6bdqdM2VUBAjECghfsXLqESBQikA7Tze2Cl+t6uafWMwPZKZD/dlr5s98JfW2bBsLm0eHG6/GKAVJIwQIdJSA8NVR06WzBHpPoNk5sLxCq6cXW4WouYSv/Gsnsgfuha/eW59GTCBGQPiKUVOHAIFzIpDsKr30g5fDn/7JH9ev3+xwfBLGkk/6QtX8ma/8gfkkLO3b/1zjFmL+PV5TU5Phu9/7x7D6gb9ojCkJc6+/cWjabcf0duPOibH6zlY+3M10+/OcgLkoAQIdKSB8deS06TSB7hTIH2xvdoA9/zb6fJlmTyumh/ZTtewb7Ju9MT//hvuk3ky7aOnb8/Nv4+/OWTIqAgTmKiB8zVVQfQIECBAgQIBAAQHhqwCWogQIECBAgACBuQoIX3MVVJ8AAQIECBAgUEBA+CqApSgBAgQIECBAYK4CwtdcBdUnQIAAAQIECBQQEL4KYClKgAABAgQIEJirgPA1V0H1CRAgQIAAAQIFBISvAliKEiBAgAABAgTmKiB8zVVQfQIECBAgQIBAAQHhqwCWogQIECBAgACBuQoIX3MVVJ8AAQIECBAgUECg1PB12eJFBbqmKAECBAgQIECg+wR+9t+/CJ+9/LKw8NL+QoPrq9VqtSI1jp34cTjz8cdFqihLgAABAgQIEOg6gQsvuCBcPrD43IevrpMzIAIECBAgQIBApECyh9XX11eoduGdr0JXV5gAAQIECBAgQOAsAeHLgiBAgAABAgQIlCggfJWIrSkCBAgQIECAgPBlDRAgQIAAAQIEShQQvkrE1hQBAgQIECBAQPiyBggQIECAAAECJQoIXyVia4oAAQIECBAgIHxZAwQIECBAgACBEgWErxKxNUWAAAECBAgQEL6sAQIECBAgQIBAiQLCV4nYmiJAgAABAgQICF/WAAECBAgQIECgRAHhq0RsTREgQIAAAQIEhC9rgAABAgQIECBQooDwVSK2pggQIECAAAEC/we4wKFaMvuzKgAAAABJRU5ErkJggg==', `mes` = '8', `anio` = '2024', `tipo` = '4', `semana` = '0'
WHERE `id` = '277' 
 Execution Time:0.11178994178772

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '1'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.087702035903931

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA1YAAAH0CAYAAAA+F7ynAAAAAXNSR0IArs4c6QAAIABJREFUeF7t3X+sHtddJ+ATsYBvgU3vpi01lg3xWgVK1RpiYdawSBQoQgoRaXGxXDZFjlurXjmBxHEcU7dNnbrmOlk2W9XI6a21W6hlakGQ4z9KKbSVSBZXTnFQIWXXjcGsG0gbmQJbm0Uou+dN5+7c1+/vmfedMzPPmz+Se+/MmXOe7/Hr+8mZOe91L7zwwgvBiwABAgQIECBAgAABAgQmFrhOsJrYzokECBAgQIAAAQIECBDoCAhWJgIBAgQIECBAgAABAgQKCghWBQGdToAAAQIECBAgQIAAAcHKHCBAgAABAgQIECBAgEBBAcGqIKDTCRAgQIAAAQIECBAgIFiZAwQIECBAgAABAgQIECgoIFgVBHQ6AQIECBAgQIAAAQIEBCtzgAABAgQIECBAgAABAgUFBKuCgE4nQIAAAQIECBAgQICAYGUOECBAgAABAgQIECBAoKCAYFUQ0OkECBAgQIAAAQIECBAQrMwBAgQIECBAgAABAgQIFBQQrAoCOp0AAQIECBAgQIAAAQKClTlAgAABAgQIECBAgACBggKCVUFApxMgQIAAAQIECBAgQECwMgcIECBAgAABAgQIECBQUECwKgjodAIECBAgQIAAAQIECAhW5gABAgQIECBAgAABAgQKCghWBQGdToAAAQIECBAgQIAAAcHKHCBAgAABAgQIECBAgEBBAcGqIKDTCRAgQIAAAQIECBAgIFiZAwQIECBAgAABAgQIECgoIFgVBHQ6AQIECBAgQIAAAQIEBCtzgAABAgQIECBAgAABAgUFBKuCgE4nQIAAAQIECBAgQICAYGUOECBAgAABAgQIECBAoKCAYFUQ0OkECBAgQIAAAQIECBAQrMwBAgQIECBAgAABAgQIFBQQrAoCOp0AAQIECBAgQIAAAQKClTlAgAABAgQIECBAgACBggKCVUFApxMgQIAAAQIECBAgQECwMgcIECBAgAABAgQIECBQUECwKgjodAIECBAgQIAAAQIECAhW5gABAgQIECBAgAABAgQKCghWBQGdToAAAQIECBAgQIAAAcHKHCBAgAABAgQIECBAgEBBAcGqIKDTCRAgQIAAAQIECBAgIFiZAwQIECBAgAABAgQIECgoIFgVBHQ6AQIECBAgQIAAAQIEBCtzgAABAgQIECBAgAABAgUFBKuCgE4nQIAAAQIECBAgQICAYGUOECBAgAABAgQIECBAoKCAYFUQ0OkECBAgQIAAAQIECBAQrMwBAgQIECBAgAABAgQIFBQQrAoCOp0AAQIECBAgQIAAAQKClTlAgAABAgQIECBAgACBggKCVUFApxMgQIAAAQIECBAgQECwMgcIECBAgAABAgQIECBQUECwKgjodAIECBAgQIAAAQIECAhW5gABAgQIECBAgAABAgQKCghWBQGdToAAAQIECBAgQIAAAcHKHCBAgAABAgQIECBAgEBBAcGqIKDTCRAgQIAAAQIECBAgIFiZAwQIECBAgAABAgQIECgoIFgVBHQ6AQIECBAgQIAAAQIEBCtzgAABAgQIECBAgAABAgUFBKuCgE4nQIAAAQIECBAgQICAYGUOECBAgAABAgQIECBAoKCAYFUQ0OkECBAgQIAAAQIECBAQrMwBAgQIECBAgAABAgQIFBQQrAoCOn1ygeeffz685S1vCb/3e7+31Mhv/uZvdr4XX9nP439/9KMfDTfccEPn+1euXAm//Mu/HP74j/84/NZv/VZYs2ZN5+ujR492ft6rjXiN173udZ3jX/ayl11z3awD+XMPHDgQ3vWud4U/+qM/Cj/8wz98zUD/4i/+Ivz8z/98eNOb3hT2798fHn/88fAjP/IjneN+6qd+almfY/9/4Rd+ofOz9773vZ3jY/u//du/3enTd3/3d3e+jtfr9dqxY0f4tV/7tfA7v/M7nXayr+fm5jqHZ9fO2u5lm283O/9rX/vaNRbdbfercDb+p556aumQ7PrZN0btRxxH9/j7uWfzIfPM96/bPf5slH72OiZrd5DHKON7xSte0Rlb93iyOZHNuUH1z5+bnTdsDmT9z8+97HuDbCf/E+1MAgQIECDQbgHBqt31r2z0g36R7Q4H2S/Sowar/C/3+bAzSrDKB7MiwSq7VgxMWRDMgl8ZwSrfz0mD1Tve8Y7w1re+NeSDUTYh8v3vNUnyrt0/z4ebUYJHPjB2t9UvAPQKC9m5+euP2s9B8zG22x0YxwmOZQSrfD3yY8//j4DucB37OCis9RtTZW8KLkyAAAECBGouIFjVvIB17H4+aPT6xbA7AI0brPK/WOd/sRzWbvcvpkWCVT74dP/SPixYjbpSk/9lu9cv1d2//Ocd8zXI/4Ld7/v5eZYPS/1WB/PfH9SP+LPuFcgYRrPw0O+X/+7Vnl7t5Fcmh/Uzq9EP/dAPdVYGs5XAfqumvf7c9Tu23zzqt2I1bGUrH6wGzYFsTnSH5H7fr+N7iT4TIECAAIGUBASrlKrRkr70+yU2+z/sr3/96zu33o17K2C8NfANb3hDOHz4cOe2qx/4gR/o3CL41a9+tSP79NNPL7sVsDuwlRWs4i1af/mXfxm+67u+q/NL+uc+97nOLYIxJMRb/coKVrH/WVvjBqtBNcjc/+Zv/mbpNsX81Bx0razdV77ylctuhYznDwopWfgY9TbEUYLVV77ylSX3eOtl/tXdz3hsvK2zbsFq0BwY9D8GhgXXlrwVGSYBAgQIEChVQLAqlVNjowhkv5j3WtXInz9JsLr//vvDu9/97nDPPfeEDRs2dH5Z3rlzZyfcZM9kDXrGKl4/WzGYdMUqhp34iuOMv8AeOXKk89/x1ruf/dmfHRqsehlmVtkvxPG2wjim+O/Y3/jKwlt3iOjlOCgcZQE3//xXvk+9Qk3282z1KQbL/HNxw4JVr1v2Bs2PQbcCdj+P1qud7n5mwarXbZGx78Pm6qDxjbti1av++VXYUebA7t27lz2HGFcBewXL7PnAUf7cOoYAAQIECBAYLCBYmSEzFxj2S33WoUmC1Qc/+MHOcyVxtSiuWMVfsj/xiU90NokYJVjlb8MqEqzWrl3b2WTid3/3d8Ov//qvd/oTf4mNK2rDVqxGCVbxF/3YZgxT8Zfu7tA2LKAOq0H3xhrTDlax/VE2mcj60S9Y5Ve8xgmA/YLVsGfNhjlnIbXXJiijbl7RfTtk/rx+c0CwmvnbmgsSIECAAIEgWJkEMxcYditgDCVxZ8BJglXcYe9jH/tYJ0h9+7d/e7juuuvC4uJieOCBB64JVnHg3asqeYwiwerNb35zZ7Us9uFv//ZvOyto2S/Bw4LVKM9Yde8kF68Vx97rmaRejqndCtg9CbPw0GuXv6xuMbgOWkka55bF7lsBL1682KlfXMEaZbUq9qnsZ6yy+ddvh8lhc8CtgDN/a3NBAgQIEGi5gGDV8glQxfCHbV4R+5R/Rire7tZrk4vsF86XvOQly257Onv27NLW5nEF433ve1/4lV/5lZkGq3j7YX4r+V636/Xbbn2cYNW9696owWqUzSv6Pe80yuYVo/ajXyAZtmnEoNWobE6P089eQXPcTR769bnX9uj5vvW79bRfjbrH3m8ODNu8Ivtz1uujBKp4X3BNAgQIECBQdwHBqu4VrGn/B22Dnf+lfNCzNNlx3bvKRZL8asMb3/jGZcEre8Zq1BWrXsTxl+HYTq/PscqvSMVbwLIA+IUvfGHZc1DjfI5V1sbHP/7xTmjsFTRjP8cJNIO2GB92C9yo25jn7UbZvKLbepxdAXvVadR+9lvBG2dTjX7jG+TcawfLfLDOn5t9v1eozI8zb2a79Zq+Qeo2AQIECNRSQLCqZdma0elhHxCcjXLYxgb9PjC4+wOEu5+xmnaw6r4Vrdeug6N+QPCgYDVsi/RBgaZXDUbdmW+cZ6L6rUzlZ3J3CBj0OUujrFhlbY/Sz2HbrccPmB52S2BR52GbXEw6B3xAcDPeL42CAAECBNIXEKzSr5EeEiBAgAABAgQIECCQuIBglXiBdI8AAQIECBAgQIAAgfQFBKv0a6SHBAgQIECAAAECBAgkLiBYJV4g3SNAgAABAgQIECBAIH0BwSr9GukhAQIECBAgQIAAAQKJCwhWiRdI9wgQIECAAAECBAgQSF9AsEq/RnpIgAABAgQIECBAgEDiAoJV4gXSPQIECBAgQIAAAQIE0hcQrNKvkR4SIECAAAECBAgQIJC4gGCVeIF0jwABAgQIECBAgACB9AUEq/RrpIcECBAgQIAAAQIECCQuIFglXiDdI0CAAAECBAgQIEAgfQHBKv0a6SEBAgQIECBAgAABAokLCFaJF0j3CBAgQIAAAQIECBBIX0CwSr9GekiAAAECBAgQIECAQOICglXiBdI9AgQIECBAgAABAgTSFxCs0q+RHhIgQIAAAQIECBAgkLiAYJV4gXSPAAECBAgQIECAAIH0BQSr9GukhwQIECBAgAABAgQIJC4gWCVeIN0jQIAAAQIECBAgQCB9AcEq/RqV1sPLly+H+++/P9xxxx1h7dq1S+2ePXs23HfffZ2vr7/++rCwsLDs548++mg4cuRI5+erV68ODz30UJifny+tXxoiQIAAAQIECBAgUHcBwaruFRyx/zFU3X333eHv//7vlwWnZ555JjzwwAPhne98ZydMxZAVQ1QWnrq/jiErfm///v1hxYoVI17dYQQIECBAgAABAgSaLSBYNbu+ndE9/PDD4fTp0+HHfuzHwvnz55dCVPaz+O8777yzc+zVq1fDgQMHwq233hpe85rXdP57w4YNna/jq9+qVwsYDZEAAQIECBAgQIBAXwHBqgWT4xOf+ET40R/90fClL31p2epUFqLywSkftm677bbOKtfOnTs74SofvLrPaQGjIRIgQIAAAQIECBAQrMyBELpv+8uvTmXBKTrF2/0uXrwYYrDq9UxWXAFbs2bN0ipW023/LHwyfDl8senDND4CBAgQaKnAy8O/Dd8XfqKlozdsAuUJWLEqzzL5llIJVk8++WTyVvkOfmn+yfC5tY/Uqs86S4AAAQIERhX4gWfeHr7j8k2jHp7EcTfdNFl/N+yY7e8gZ49O1s8kkHVibAHBamyy+p7QL1i5FXBwTT8fPhG+Ei7Ut/B6ToAAAQIEBgi8LNwYXhPe0AojwaoVZa5skIJVZfSzv3B3sIo9iLf1xZfNK2ZfD1ckQIAAAQIEZiuQUrC6cuVK2LdvXzh16tQSwqFDh8LmzZtni+JqpQkIVqVRpt9Qr2Blu/X066aHBAgQIECAQDkCqQSruEvztm3bwq5du5aCVNx5efv27WHjxo1hz5495QxYKzMVEKxmyl3txXoFq9gjHxBcbV1cnQABAgQIEJiNQArBKlup2rRp0zWrUzFwxVC1sLAQbrjhhk7Qil/HsBVfJ0+eDE888UQ4ePBg5yNydu/eHd70pjeFuNJ16dKlcMstt4R77rmnE9jOnTsXVq1aFY4dOxbWrVvXOffChRcfbTh69Gjn38ePH19qezYVaPZVBKtm19foCBAgQIAAAQIEvi6QQrDKh6cYePq9shWsQcEqBq+4U3MWtOLXX/7yl5fCVAxo8RXbiMFq7969nRAWbzfMVs0OHz4sXJX0J0SwKglSMwQIECBAgAABAmkLpBCszpw501mRWlxcDPPz84WDVT545YNUbDhe68SJE53gdfr06aXVrrm5uc51u49Pu3rp906wSr9GekiAAAECBAgQIFCCQBuC1Y033rh0i2F3sIq3Auaf38rfWpiFrRKYW9uEYNXa0hs4AQIECBAgQKBdAikEq7JvBexesRKsqpvTglV19q5MgAABAgQIECAwQ4EUgtWgzSvyz1W96lWvGrp5RffmFvHWvkHBKtv4wq2A05l0gtV0XLVKgAABAgQIECCQmEAKwSqSDNpuPduMIh4XP+dq5cqVndv3stDVvVnFOCtWcfOKbCfAUVfOEith0t0RrJIuj84RIECAAAECBAiUJZBKsIrjyYJS3BY9e3V/QHAWwOJW6uvXrw+33357+P3f//1luwCOE6w+/vGPdy716U9/uvPv/HbrnrcqPssEq+KGWiBAgAABAgQIEKiBQErBatZcgtP0xQWr6Ru7AgECBAjMUOA9X3rPDK/W/1LXXXddeOGFFwr3pVc77/mONMZYeHAaIDBjAcHqxQ8XtgPgdCaeYDUdV60SIECAQEUCMVjd/+z9FV19+pd998p3B8Fq+s6uQKBpAlaspl9RwWr6xq5AgAABAjMUEKxmiO1SBAgQILAkIFiZDAQIECDQKAHBqlHlNBgCBAjURkCwqk2pdJQAAQIERhEQrEZRcgwBAgQIlC0gWJUtqj0CBAgQqFRgWLD65Ks+GX78236808d/+Jd/CP/xr/9j+I3nf6Pz9eu/7fXhIzd+JKz6xlVLY+g+Jj+4/3DDfwgfXP3B8G3f8G2db//51T8P3/dn37d0SPfPP/yVD4ftf7X9Gp/sul+4+oXwE//jJwb6ecaq0unl4gQIEOgrIFiZHAQIECDQKIFBwSqGqn//rf8+/Orf/Gp415feFd77He8N73j5O8Jd/+uuTriKX7/l37wlvO2v3hb+8B/+cKBLFoa++i9f7YSp7nCUharPfu2znbAU2/6lV/xS+M/P/efOtfOvLOz9wT/8gWDVqNloMAQItElAsGpTtY2VAAECLRDoF6z6rQr92ff9WXj2n5/tBJrF71wM/+5b/92yVad+ZFlw+tjljy2tQsWAtPIbV3bOj229ef7NSyti/a4fA9e2l20L33zdN4enrjwlWLVgjhoiAQLNFBCsmllXoyJAgEBrBYYFq49/9ePLbseLwSq+YhiKwWj93Ppww7+6ofO9QbcBDlux6i5ArxWr2MaHvvND4bGvPhZ+bv7nglsBWzttDZwAgQYICFYNKKIhECBAgMD/Fxh2K+APvuQHl1aRYti595X3hvP/dL4TrGLIuv4brg+3XbitcytgDFr543s5x3NeveLVnR/1eoYq/5xV961+2QrZrou7Os92CVZmMgECBOorIFjVt3Z6ToAAAQI9BIZtXpEPQpf++VL4q3/6q/DSf/XSnrf/DdpUovsZqu4VrH4hLFsdi+e/f9X7w32X7guX/s8lwcpsJkCAQM0FBKuaF1D3CRAgQGC5wLBg1e2Vf8aqXxjKnsHK/7z7Gar4s0EbVMSf58/Z+8q94b//43/v3JZoV0CzmAABAvUXEKzqX0MjIECAAIGcwDjBKr8BxWf+8TOdrdPzm1Fkgaf7uazukJRt154PVpu+dVP4nhXfs3RbYf6c45ePh63zW5e2ac8XsHvL9u7i2m7ddCfQDIErV66Effv2hVOnTi0NaMeOHWHPnj1LX/c6Jvvh8ePHw8aNG6/BuHz5cti9e3e47777wrp168LJkyfD3r17e6LF673xjW8M27ZtC5cuXep7zK5duzp93bRpU7j55ps7/71y5cplfc1OPn/+fOf7CwsLnevHV3cf1q9fHxYXF8P8/Hwzivn1UQhWjSqnwRAgQIDAsGes8mGn+5mqXs9YdYejTHjYrYDduwZ2H5+vlBUr85ZAuwRi+IhhJoaUfJCKYeTMmTNLoSMLVlu2bFkWouIx99xzTzh27NhSeMkEewWrCxcu9AxB3er9rpd9PwarzZs3d/oY+9orHMXvP/vss+HgwYOd5mMIu3jx4rJj4/lbt24N/cJhXWeDYFXXyuk3AQIECPQUGLZilX/Gqteuf4N+HjezyAet7g8Ajs9sZRtfxM5lm2N803Xf1Olrv8+pEqxMZgLtEegOKd0jj8EkvmLg6hd0usNTvo1ZBKt4je3bt4cY+GLQyoe6+P3Y97iaFleqnnjiiU7ImpubWzbUQeGsrrNBsKpr5fSbAAECBCYKVnVncytg3Suo/20X6HWrXN4k/vz9739/ePDBB8OKFSs6Kz69Vqw+85nP9FyFmkWwiv3tFZryYSnre7bK1V33fqGxzvNDsKpz9fSdAAECBK4RGLZiVXcywaruFdT/KgXuDxtmevl3h7PXXG/YSk0+GK1ateqa57CyBm+55ZaeK0HjPGPVfSveqLcCxj50B8TulbhsVStbveoFH1fnbrzxxmWrXjMtUMkXE6xKBtUcAQIECFQrIFhV6+/qBFIWqGuw6l6xyp7ROnz48DUbWMxqxSrWOX/bYnfQEqxS/pOgbwQIECBAYAQBwWoEJIcQIFCZQBm3AnaHmvxgZhms8qtvn/zkJ0N+k4xhz5K5FbCyKejCBAgQIEBgNAHBajQnRxEgUI3AsMAxyuYVWbCKt9F9//d//9IzWXH78lkGq2wsP/mTPxk+/OEPL21akcnavKKaOeaqBAgQIECgFAHBqhRGjRAgMEWBotut51e9brjhhs4OfdmzTN3PcMVwU/Z263ma7DOqej3zlQUv261PcTJpmgABAgQITEtAsJqWrHYJEChToMwPCM4+Fyr2L254kf98q0EfENz9Qb3jbF6RWfTber1X+Mq+5wOCy5xJ2iJAgAABAlMSEKymBKtZAgQIEBgoYFdAE4QAAQIEGiUgWDWqnAZDgACB2ggIVrUplY4SIECAwCgCgtUoSo4hQIAAgbIFBKuyRbVHgAABApUKCFaV8rs4AQIEWisgWLW29AZOgACBZgoIVs2sq1ERIEAgdQHBKvUK6R8BAgQIjCUgWI3F5WACBAgQKElAsCoJUjMECBAgkIaAYJVGHfSCAAECbRMQrNpWceMlQIBAwwUEq4YX2PAIECCQqIBglWhhdIsAAQIEJhMQrCZzcxYBAgQIFBMQrIr5OZsAAQIEEhMQrBIriO4QIECgJQKCVUsKbZgECBBoi4Bg1ZZKGycBAgTSEhCs0qqH3hAgQIBAQQHBqiCg0wkQIEBgIgHBaiI2JxEgQIBAqgKCVaqV0S8CBAg0W0CwanZ9jY4AAQKtExCsWldyAyZAgEASAoJVEmXQCQIECBAoS0CwKktSOwQIECAwjoBgNY6WYwkQIEAgeQHBKvkS6SABAgQaKSBYNbKsBkWAAIH2CghW7a29kRMgQKBKAcGqSn3XJkCAAIHSBQSr0kk1SIAAAQIjCAhWIyA5hAABAgTqIyBY1adWekqAAIEmCQhWTaqmsRAgQIBAEKxMAgIECBCoQkCwqkLdNQkQIEBgagKC1dRoNUyAAAECAwQEK9ODAAECBBolIFg1qpwGQ4AAgdoICFa1KZWOEiBAgMAoAoLVKEqOIUCAAIGyBQSrskW1R4AAAQKVCghWlfK7OAECBForIFi1tvQGToAAgWYKCFbNrKtRESBAIHUBwSr1CukfAQIECIwlIFiNxeVgAgQIEChJQLAqCVIzBAgQIJCGgGCVRh30ggABAm0TEKzaVnHjJUCAQMMFBKuGF9jwCBAgkKiAYJVoYXSLAAECBCYTEKwmc3MWAQIECBQTEKyK+TmbAAECBBITEKwSK4juECBAoCUCglVLCm2YBAgQaIuAYNWWShsnAQIE0hIQrNKqh94QIECAQEEBwaogoNMJECBAYCIBwWoiNicRIECAQKoCglWqldEvAgQINFtAsGp2fY2OAAECrRMQrFpXcgMmQIBAEgKCVRJl0AkCBAgQKEtAsCpLUjsECBAgMI6AYDWOlmMJECBAIHkBwSr5EukgAQIEGikgWDWyrAZFgACB9goIVu2tvZETIECgSgHBqkr9iq999erVcODAgfDZz372mp7cfPPN4c477wy9jlm9enV46KGHwvz8fMUjcHkCBAhcKyBYmRUECBAgUIWAYFWFesLXPHv2bDh06FBYWFgIa9euDc8880w4ceJEuOuuu8KKFSsS7rmuESBA4EUBwcpMIECAAIEqBASrKtQTvebly5fD3XffHX7mZ34m3HrrrZ1exqD1+OOPd1avvAgQIFAHAcGqDlXSRwIECDRPQLBqXk0nHtGjjz7aCVL79+9fWp2K3/vUpz4Vnn766U67bgOcmNeJBAjMSECwmhG0yxAgQIDAMgHByoToCGSrVTt37gwbNmxYUnn44YfDc889txS2YtB67LHHPGNl3hAgkKyAYJVsaXSMAAECjRYQrBpd3tEHF1eqjhw5MjQwZZtZxPCV3S44+lVePPLJJ58c9xTHEyBAYGSBR8KL/zT19fbw4j9eBNoscNNNN7V5+MaeqIBglWhhZt2tuDIVX6M8SxWPXbNmzcTBatZjcz0CBNolYMWqXfU2WgIECKQiIFilUokK+9HvNsBe389WrOJqVf6WwQq779IECBBYJiBYmRAECBAgUIWAYFWFemLXjFuqP/DAA+Gd73xnZ4v1/KvXM1bdG1wkNhzdIUCg5QKCVcsngOETIECgIgHBqiL4lC477PmqGK5Onz7d6bJdAVOqnL4QINBLQLAyLwgQIECgCgHBqgp11yRAgACBqQkIVlOj1TABAgQIDBAQrEwPAgQIEGiUgGDVqHIaDAECBGojIFjVplQ6SoAAAQKjCAhWoyg5hgABAgTKFhCsyhbVHgECBAhUKiBYVcrv4gQIEGitgGDV2tIbOAECBJopIFg1s65GRYAAgdQFBKvUK6R/BAgQIDCWgGA1FpeDCRAgQKAkAcGqJEjNECBAgEAaAoJVGnXQCwIECLRNQLBqW8WNlwABAg0XEKwaXmDDI0CAQKICglWihdEtAgQIEJhMQLCazM1ZBAgQIFBMQLAq5udsAgQIEEhMQLBKrCC6Q4AAgZYICFYtKbRhEiBAoC0CglVbKm2cBAgQSEtAsEqrHnpDgAABAgUFBKuCgE4nQIAAgYkEBKuJ2JxEgAABAqkKCFapVka/CBAg0GwBwarZ9TU6AgQItE5AsGpdyQ2YAAECSQgIVkmUQScIECBAoCwBwaosSe0QIECAwDgCgtU4Wo4lQIAAgeQFBKvkS6SDBAgQaKSAYNXIshoUAQIE2isgWLW39kZOgACBKgUEqyr1XZsAAQIEShcQrEon1SABAgQIjCAgWI2A5BACBAgQqI+AYFXLavMaAAAgAElEQVSfWukpAQIEmiTQ+GB15cqVsG/fvnDq1Kmluu3YsSPs2bOnSXU0FgIECBD4uoBgZSoQIECAQBUCjQ5Wly9fDtu3bw9btmwJmzdvXvJdWFgIp0+fDseOHQvr1q2rwt01CRAgQGBKAoLVlGA1S4AAAQIDBRodrGKAiq9eq1NnzpwJ8eeLi4thfn7eNCFAgACBhggIVg0ppGEQIECgZgKNDVbZalUMVRs3buxZlkHBq2Z11F0CBAgQ+LqAYGUqECBAgEAVAq0OVnHV6sSJE+HgwYNhbm6uCn/XJECAAIGSBQSrkkE1R4AAAQIjCTQ2WGWbVsTnq/qtWLkdcKQ54iACBAjUSkCwqlW5dJYAAQKNEWhssIoVGnarn2DVmHlsIAQIEFgSEKxMBgIECBCoQqDRwer8+fNh27Zt4fDhwz1XrYYFryoK4poECBAgUExAsCrm52wCBAgQmEyg0cEqksRVqa1bt4buz646efJk5/kquwJONnGcRYAAgVQFBKtUK6NfBAgQaLZAY4NVflfA1772tdd8SPAtt9xi04pmz22jI0CgpQKCVUsLb9gECBCoWKAVwarf5hUV27s8AQIECExBQLCaAqomCRAgQGCogGA1lMgBBAgQIFAnAcGqTtXSVwIECDRHQLBqTi2NhAABAgT+n4BgZRoQIECAQBUCjQ9W586dW3Jdv369zSqqmGWuSYAAgRkKCFYzxHYpAgQIEFgSaHyw2rNnT2er9bi1+tGjR68p/fHjx/t+gLB5QoAAAQL1ExCs6lczPSZAgEATBFoTrLqLlX3G1ctf/nKrWE2YycZAgACBrwsIVqYCAQIECFQh0NpgVQW2axIgQIDA9AUEq+kbuwIBAgQIXCvQ2GCl2AQIECDQTgHBqp11N2oCBAhULSBYVV0B1ydAgACBUgUEq1I5NUaAAAECIwo0Nlhdvnw5bN++PeR3BbRRxYizwmEECBCosYBgVePi6ToBAgRqLNDYYJWvSbZRxaVLl64p1Y4dO0LcOdCLAAECBJohIFg1o45GQYAAgboJtCJY9SvKyZMnw4kTJ+wKWLdZq78ECBAYICBYmR4ECBAgUIVA64JVfvXq0KFDYfPmzVW4uyYBAgQITElAsJoSrGYJECBAYKBAa4JVXJ3au3dvB8OzVv5UECBAoLkCglVza2tkBAgQSFmg8cFqYWEhHD16NKxfv94tfynPRH0jQIBASQKCVUmQmiFAgACBsQQaG6y6dwUUrMaaFw4mQIBAbQUEq9qWTscJECBQa4HGBqt8VfK3Aea/L2zVeu7qPAECBHoKCFYmBgECBAhUIdCKYNULNgtbwlUV0841CRAgMD0BwWp6tlomQIAAgf4CrQ1WJgUBAgQINFNAsGpmXY2KAAECqQs0Nlhlz1jFD//duHFj6nXQPwIECBAoSUCwKglSMwQIECAwlkBrgtWVK1fCvn37wpYtW5aCVgxfK1asCHNzc2OhOZgAAQIE0hUQrNKtjZ4RIECgyQKtDlZnzpwJcTv2xcXFMD8/3+Q6GxsBAgRaIyBYtabUBkqAAIGkBAQrwSqpCakzBAgQKCogWBUVdD4BAgQITCIgWAlWk8wb5xAgQCBZAcEq2dLoGAECBBotIFgJVo2e4AZHgED7BASr9tXciAkQIJCCQOOD1blz55Y5Hz9+fGnzCs9YpTAF9YEAAQLlCghW5XpqjQABAgRGE2hssMoPP/sw4F4kPiB4tIniKAIECNRFQLCqS6X0kwABAs0SaEWw6lWyLGwJVs2a0EZDgAABwcocIECAAIEqBFobrKrAdk0CBAgQmL6AYDV9Y1cgQIAAgWsFGhus4of/bt++PWTPWK1atSocO3YsrFu3zjwgQIAAgQYLCFYNLq6hESBAIGGBxgarvHm/Z6yErYRnpq4RIEBgQgHBakI4pxEgQIBAIYHGBqtsxWrLli1h8+bN1yB5xqrQvHEyAQIEkhUQrJItjY4RIECg0QKND1bd260fOnSoZ9BqdJUNjgABAi0SEKxaVGxDJUCAQEICjQ1W0fjKlSth3759YeXKlWHPnj2h1y2B+c+1SqguukKAAAECEwoIVhPCOY0AAQIECgk0OlhlMvGDgLdu3Rp27NjRCVjxld0qGP97cXExzM/PF4J0MgECBAikISBYpVEHvSBAgEDbBFoRrLKiLiwshNOnT9sdsG2z3HgJEGiVgGDVqnIbLAECBJIRaFWwyq9UrVmzJhw8eDDMzc0lUwwdIUCAAIHiAoJVcUMtECBAgMD4Aq0LVhnR+fPnw7Zt28Lhw4fDxo0bx5dzBgECBAgkKSBYJVkWnSJAgEDjBVobrGJl47NX8fbAtj9j9fDDD3dukcxe119/fcdl7dq1nW89+uij4ciRI53/Xr16dXjooYc8k9b4twYDJFBfAcGqvrXTcwIECNRZoPHBKtu4oleR8ptZ1LmIRfoeN/F48MEHw+7du3uGpbNnz3ZCVRamYsiK39u/f39YsWJFkUs7lwABAlMREKymwqpRAgQIEBgi0Nhgle36F2/zy3YCNBuuFXjmmWfCiRMnwl133XVNULp69Wo4cOBA2LBhQ7j11ls7J0fX+++/P9xxxx1LK1pcCRAgkJKAYJVSNfSFAAEC7RFobLDKPsPq1KlTPavp86teZImrTx/5yEfC008/3fk6fxtgDFF333132LlzZydcxVevsNWePy5GSoBAHQQEqzpUSR8JECDQPIHGBqt+peq+NXD9+vWtfsYq3tr32GOPLd3qF4PWoUOHOs9Yxc/26rU6FZ/JirsqZqtYzftjYUQECNRZQLCqc/X0nQABAvUVaF2wqm+pZtfzGJzi67bbbptKsHryySdnNxhXIkCgdQKPhBf/aerr7eHFf7wItFngpptuavPwjT1RgcYGq+wZq3PnznXoV61a5YOBR5yEcRXr4sWLnWDlVsAR0RxGgEAyAlaskimFjhAgQKBVAo0NVvkqnjx5Muzdu/eawrY9bPV7Xiq71e+nf/qnbV7RqrcDgyXQDAHBqhl1NAoCBAjUTaAVwapXUbKw5Rmra5+xym+vbrv1uv2R1l8CBAQrc4AAAQIEqhBobbCqAjvVa+Y/ALj7w4Fjn31AcKqV0y8CBHoJCFbmBQECBAhUIdDqYBV3CIy73y0uLvb8cNwqCuKaBAgQIFBMQLAq5udsAgQIEJhMQLASrCabOc4iQIBAogKCVaKF0S0CBAg0XECwEqwaPsUNjwCBtgkIVm2ruPESIEAgDQHBSrBKYybqBQECBEoSEKxKgtQMAQIECIwl0NhgNcrnWHnGaqy54mACBAjUQkCwqkWZdJIAAQKNE2hssMpXqt/nWMVj2r7deuNmtAERINB6AcGq9VMAAAECBCoRaEWw6iXrc6wqmW8uSoAAgakLCFZTJ3YBAgQIEOgh0NpgZTYQIECAQDMFBKtm1tWoCBAgkLpAo4PV+fPnw7Zt28KlS5c6dTh+/HjYuHFj6jXRPwIECBAoICBYFcBzKgECBAhMLNDYYHXlypWwb9++sGnTprB58+aQhaxdu3Z1vvYiQIAAgWYKCFbNrKtRESBAIHWBxgarbFfAPXv2LK1SxeeqnnjiiXDw4MEwNzeXem30jwABAgQmEBCsJkBzCgECBAgUFmhVsLK9euH5ogECBAgkLyBYJV8iHSRAgEAjBQSrRpbVoAgQINBeAcGqvbU3cgIECFQp0Phgde7cuY7vqlWrwu233x5OnToVFhcXw/z8fJXurk2AAAECUxIQrKYEq1kCBAgQGCjQ2GCVH3W/Dwj24cD+dBAgQKB5AoJV82pqRAQIEKiDQCuCVa9C+IDgOkxPfSRAgMD4AoLV+GbOIECAAIHiAo0NVtmugFu2bLG9evF5ogUCBAjURkCwqk2pdJQAAQKNEmh8sMqescqqdujQIUGrUVPYYAgQILBcQLAyIwgQIECgCoHGBquImX1I8MqVK0P8PKtez1odP3586XOuqiiAaxIgQIBAuQKCVbmeWiNAgACB0QQaHawygvj5VVu3bg07duzoBKz4ym4VjP9tl8DRJoujCBAgUAcBwaoOVdJHAgQINE+gFcEqK9vCwkI4ffp0OHbsWFi3bl3zqmlEBAgQIBAEK5OAAAECBKoQaFWwyq9UrVmzJhw8eDDMzc1V4e6aBAgQIDAlAcFqSrCaJUCAAIGBAq0LVpnG+fPnw7Zt28Lhw4c9Y+UPCQECBBokIFg1qJiGQoAAgRoJtDZYxRrFZ6/i7YGesarRjNVVAgQIDBEQrEwRAgQIEKhCoPHBKtu4ohdufjOLKvBdkwABAgTKFxCsyjfVIgECBAgMF2hssMp2/du4cePSToDDORxBgAABAnUXEKzqXkH9J0CAQD0FGhusss+wOnXqVM/K+Pyqek5YvSZAgMAwAcFqmJCfEyBAgMA0BBobrPphdd8auH79es9YTWNmaZMAAQIVCQhWFcG7LAECBFou0Lpg1fJ6Gz4BAgQaLyBYNb7EBkiAAIEkBRobrLJnrM6dO7cEb7OKJOegThEgQKBUAcGqVE6NESBAgMCIAo0NVvnx99oZcNWqVeHYsWNh3bp1I1I5jAABAgTqICBY1aFK+kiAAIHmCbQiWHWXLVvN+vKXvyxcNW9OGxEBAi0XEKxaPgEMnwABAhUJtDJYRets18D43wcPHgxzc3MVlcBlCRAgQKBMAcGqTE1tESBAgMCoAq0NVhEo3iK4sLBgV8BRZ4vjCBAgUAMBwaoGRdJFAgQINFBAsBKsGjitDYkAgTYLCFZtrr6xEyBAoDqB1gerEydOuBWwuvnnygQIEChdQLAqnVSDBAgQIDCCQGODVa/t1qPHoUOHwubNm0P28y1btnS+9iJAgACBZggIVs2oo1EQIECgbgKNDVbdhTh58mTYu3fvsm/7XKu6TVf9JUCAwHABwWq4kSMIECBAoHyB1gSr8um0SIAAAQIpCghWKVZFnwgQINB8gcYHq7hS9YEPfGDZ51XF7124cCHs2bOn+RU2QgIECLRMQLBqWcENlwABAokINDpYZbf/HT9+PGzcuHEZedxm/dlnn7VxRSITUTcIECBQloBgVZakdggQIEBgHIHGBqvsA4A3bdrUc3OKYT8fB9GxBAgQIJCOgGCVTi30hAABAm0SaGywynb9i7f7da9WZQWOHxBsu/U2TXdjJUCgDQKCVRuqbIwECBBIT6D1wSreEri4uBjm5+fTq44eESBAgMDYAoLV2GROIECAAIESBBobrKJNDE3x1W+TirhiJViVMIs0QYAAgYQEBKuEiqErBAgQaJFAo4PV+fPnw7Zt28Lhw4d73g44LHi1aB4YKgECBBojIFg1ppQGQoAAgVoJNDpYxUrEVamtW7eG7g8D7rUNe60qp7MECBAg0FNAsDIxCBAgQKAKgcYHq4ia7QB46tSpJeNVq1Yt+2yrKvBdkwABAgTKFxCsyjfVIgECBAgMF2hFsBrO4AgCBAgQaIqAYNWUShoHAQIE6iUgWNWrXnpLgAABAkMEBCtThAABAgSqEBCsqlB3TQIECBCYmoBgNTVaDRMgQIDAAAHByvQgQIAAgUYJCFaNKqfBECBAoDYCglVtSqWjBAgQIDCKgGA1ipJjCBAgQKBsAcGqbFHtESBAgEClAoJVpfwuToAAgdYKCFatLb2BEyBAoJkCglUz62pUBAgQSF1AsEq9QvpHgAABAmMJCFZjcTmYAAECBEoSEKxKgtQMAQIECKQhIFilUQe9IECAQNsEBKu2Vdx4CRAg0HABwarhBTY8AgQIJCogWCVaGN0iQIAAgckEBKvJ3JxFgAABAsUEBKtifs4mQIAAgcQEBKvECqI7BAgQaImAYNWSQhsmAQIE2iIgWLWl0sZJgACBtAQEq7TqoTcECBAgUFBAsCoI6HQCBAgQmEhAsJqIzUkECBAgkKqAYJVqZfSLAAECzRYQrJpdX6MjQIBA6wQEq9aV3IAJECCQhIBglUQZdIIAAQIEyhIQrMqS1A4BAgQIjCMgWI2j5VgCBAgQSF5AsEq+RDpIgACBRgoIVo0sq0ERIECgvQKCVXtrb+QECBCoUkCwqlI/kWs/+uij4ciRI53eXH/99WFhYSGsXbu28/XVq1fDgQMHwmc/+9ml3q5evTo89NBDYX5+PpER6AYBAgT+v4BgZTYQIECAQBUCglUV6gldM4aqxx57bCkonT17thOysuD0zDPPhBMnToS77rorrFixIqGe6woBAgR6CwhWZgYBAgQIVCEgWFWhnsg1s9WoDRs2hFtvvXXZClX8On4/Bq3HH3883HnnnYn0WjcIECAwWECwMkMIECBAoAoBwaoK9YSvefny5XD33XeHnTt3doJVXNH61Kc+FZ5++ulOr90GmHDxdI0AgY6AYGUiECBAgEAVAoJVFeoJXzMGqbhKtX///s6tfw8//HB47rnnlr7uvnUw4aHoGgECLRUQrFpaeMMmQIBAxQKCVcUFSOnyMTR99KMfXbZ5RXf/et0+OO4YnnzyyXFPcTwBAgRGFngkvPhPU19vDy/+40WgzQI33XRTm4dv7IkKCFaJFmbW3RolVGV9iqtYa9asWXoua9Z9dT0CBAgMErBiZX4QIECAQBUCglUV6oldMwalp5566pot1Luft4rdzlasss0tEhuK7hAgQMAzVuYAAQIECFQiIFhVwp7ORYc9M9XrGav8M1jpjERPCBAg8KKAFSszgQABAgSqEBCsqlBP5JrZitRf//VfX9OjuCtgtgV7DFenT5/uHGNXwESKpxsECPQVEKxMDgIECBCoQkCwqkLdNQkQIEBgagKC1dRoNUyAAAECAwQEK9ODAAECBBolIFg1qpwGQ4AAgdoICFa1KZWOEiBAgMAoAoLVKEqOIUCAAIGyBQSrskW1R4AAAQKVCghWlfK7OAECBForIFi1tvQGToAAgWYKCFbNrKtRESBAIHUBwSr1CukfAQIECIwlIFiNxeVgAgQIEChJQLAqCVIzBAgQIJCGgGCVRh30ggABAm0TEKzaVnHjJUCAQMMFBKuGF9jwCBAgkKiAYJVoYXSLAAECBCYTEKwmc3MWAQIECBQTEKyK+TmbAAECBBITEKwSK4juECBAoCUCglVLCm2YBAgQaIuAYNWWShsnAQIE0hIQrNKqh94QIECAQEEBwaogoNMJECBAYCIBwWoiNicRIECAQKoCglWqldEvAgQINFtAsGp2fY2OAAECrRMQrFpXcgMmQIBAEgKCVRJl0AkCBAgQKEtAsCpLUjsECBAgMI6AYDWOlmMJECBAIHkBwSr5EukgAQIEGikgWDWyrAZFgACB9goIVu2tvZETIECgSgHBqkp91yZAgACB0gUEq9JJNUiAAAECIwgIViMgOYQAAQIE6iMgWNWnVnpKgACBJgkIVk2qprEQIECAQBCsTAICBAgQqEJAsKpC3TUJECBAYGoCgtXUaDVMgAABAgMEBCvTgwABAgQaJSBYNaqcBkOAAIHaCAhWtSmVjhIgQIDAKAKC1ShKjiFAgACBsgUEq7JFtUeAAAEClQoIVpXyuzgBAgRaKyBYtbb0Bk6AAIFmCghWzayrUREgQCB1AcEq9QrpHwECBAiMJSBYjcXlYAIECBAoSUCwKglSMwQIECCQhoBglUYd9IIAAQJtExCs2lZx4yVAgEDDBQSrhhfY8AgQIJCogGCVaGF0iwABAgQmExCsJnNzFgECBAgUExCsivk5mwABAgQSExCsEiuI7hAgQKAlAoJVSwptmAQIEGiLgGDVlkobJwECBNISEKzSqofeECBAgEBBAcGqIKDTCRAgQGAiAcFqIjYnESBAgECqAoJVqpXRLwIECDRbQLBqdn2NjgABAq0TEKxaV3IDJkCAQBICglUSZdAJAgQIEChLQLAqS1I7BAgQIDCOgGA1jpZjCRAgQCB5AcEq+RLpIAECBBopIFg1sqwGRYAAgfYKCFbtrb2REyBAoEoBwapKfdcmQIAAgdIFBKvSSTVIgAABAiMICFYjIDmEAAECBOojIFjVp1Z6SoAAgSYJCFZNqqaxECBAgEAQrEwCAgQIEKhCQLCqQt01CRAgQGBqAoLV1Gg1TIAAAQIDBAQr04MAAQIEGiUgWDWqnAZDgACB2ggIVrUplY4SIECAwCgCgtUoSo4hQIAAgbIFBKuyRbVHgAABApUKCFaV8rs4AQIEWisgWLW29AZOgACBZgoIVs2sq1ERIEAgdQHBKvUK6R8BAgQIjCUgWI3F5WACBAgQKElAsCoJUjMECBAgkIaAYJVGHfSCAAECbRMQrNpWceMlQIBAwwUEq4YX2PAIECCQqIBglWhhdKtagX/52tfCn77tbeHS8eOdjvzbe+8N33voULWdatjVGU+3oP/76r+EO/7L+fDUF/+xc6E3bJgPB9+2droXTaR1wSqRQhTshveIgoBDTm/ze8R0ZbXeZgHBqs3VN/a+Ak/v3dv5WQxT2V/uL/vxHw+rt22jVpIA45Ig+zSz70PPdH4Sw1T2C9QPvfpfh7fdvHK6F06gdcEqgSKU0AXvESUgDmiize8R05XVepsFBKs2V9/Yewr849NPh3O/+Ith/X/9r+Fbv/d7O8c8/5nPhIuPPBJe+6EPhW94yUvIFRRgXBBwyOmfv/C/w71Hnwm/umNteM2N39I5+g8/dzl89JPPhf9yx7rwLSu+YbodqLh1wariApRwee8RJSAOaKLt7xHT1dV6mwUEqzZX39h7CsQQ9fS994YfPH06fNPLXtY5ptdf8vgmF2A8ud0oZ8YQ9Z9O/q9w7N7vCa946Td2Tun1i9QobdXxGMGqjlVb3mfvEdOtYdvfI6arq/U2CwhWba6+sfcNVt2rU//nK18J59761vDqBx9cWsXCN7lArxVAxpN7dp/Za3Xqub/757Dr4f8Z9t/2nUurWOVdMa2WBKu06jFJb7xHTKI2+jltf48YXcqRBMYTEKzG83J0CwT8hT79IjOernHbf2kSrKY7v2bRuveI6Sq3/T1iurpab7OAYNXm6ht73xUrtwJOd3K4zWe6vm2/zUewmu78mkXr3iOmq9z294jp6mq9zQKCVZurb+w9BTw0Pf2JwXi6xm1/MF2wmu78mkXr3iOmq9z294jp6mq9zQKCVZurb+x9BWzzO/3JwXi6xm3eSlmwmu7cmlXr3iOmK93m94jpymq9zQKCVZurb+x9BXww5fQnB+PpGrf5wz8Fq+nOrVm17j1iutJtfo+YrqzW2ywgWLW5+sZOgACBBgoIVg0sqiERIECgBgKCVQ2KpIsECBAgMLqAYDW6lSMJECBAoDwBwao8Sy01VOCRRx4Jb3/72xs6ujSGxXi6dWibr2A13flURettm8OzNuY7a3HXa6qAYNXUyhpXaQL+wimNsm9DjKdr3DZfwWq686mK1ts2h2dtzHfW4q7XVAHBqqmVNa7SBPyFUxqlYDV9yp5XaNscFqwqmmhTvGzb5vAUKb1HzBrX9VolIFi1qtwGO4mAv9AnURvvHMbjeY17dNt8BatxZ0j6x7dtDs+6InxnLe56TRUQrJpaWeMqTcBfOKVRWrGaPqX/G/3/BASriibaFC/rfXiKuCEEvtP11Xp7BASr9tTaSCcU8BfOhHBjnMZ4DKwJDm2br2A1wSRJ/JS2zeFZl4PvrMVdr6kCglVTK2tcpQn4C6c0SitW06e0YmXFqqJZNt3Leh/mO10BrRMoR0CwKsex0a08+uij4ciRI50xrl69Ojz00ENhfn6+0WPOD85f6NMvNePpGrfN14rVdOdTFa23bQ7P2pjvrMVdr6kCglVTK1vSuM6ePdsJVVmYiiErfm///v1hxYoVJV0l7Wb8hTP9+jCernHbfAWr6c6nKlpv2xyetTHfWYu7XlMFBKumVraEcV29ejUcOHAgbNiwIdx6662dFi9fvhzuv//+cMcdd4S1a9eWcJX0m/AXzvRrxHi6xm3zFaymO5+qaL1tc3jWxnxnLe56TRUQrJpa2RLGFUPU3XffHXbu3NkJV/HVK2yVcKmkm/AXzvTLw3i6xm3zFaymO5+qaL1tc3jWxnxnLe56TRUQrJpa2RLG1W916uGHHw5r1qxZWsUq4VLjNfGe94Twd3833jkFjn7k1a8Ob//zP7+2heuuC+GFFwq0/PVTu9t56UtDiGOs8pWK8TQMUvANIXzp+X+axuh6tnn6t/9buPlNb53Z9b7jhm+e2bV6XUiwmgG/94ipI3uPmDqxCxAoXUCwKp20OQ1OK1hlq1/NkTISAgQIECBAYJYC8XlvLwKpCQhWqVUkof64FTChYugKAQIECBAgQIBA0gKCVdLlqbZzNq+o1t/VCRAgQIAAAQIE6iMgWNWnVpX01HbrlbC7KAECBAgQIECAQM0EBKuaFayK7rb9A4KrMHdNAgQIECBAgACBegkIVvWql94SIECAAAECBAgQIJCggGCVYFF0iQABAgQIECBAgACBegkIVvWql94SIECAAAECBAgQIJCggGCVYFF0iQABAgQIECBAgACBegkIVvWql94SIECAAAECBAgQIJCggGCVYFF0iQABAgQIECBAgACBegkIVvWql94SIECAAAECBAgQIJCggGCVYFF0iUCbBK5cuRL27dsXtmzZEjZu3NimoU9trJcvXw7bt28P586du+Yaq1atCseOHQvr1q2b2vWb3vCZM2fC1q1bew7z+PHj5nFJE6Db2dwtCVYzBAhMTUCwmhqthgkQGCaQhapTp04Fv5AO0xr95zFY7d69O9x3330C1OhsIx8Zf+E/ceJEOHjwYJibm1s6L37/nnvuEVxHlux/4MLCQjh9+vQyy/Pnz4dt27aFXbt2hc2bN5dwFU0QIECgXAHBqlxPrREgMKLAyZMnw969e8Mtt9wSLl68GPbs2eP/9I9oN+wwwWqYULGf9wtW2f8o2LRpk1/8CxBH3xisFhcXw/z8/LKWYriK7xXx51ZdCyA7lQCBqQgIVlNh1SgBAsMEPv3pT8XQ/LoAAAkuSURBVIfXve51ncPibWuC1TCx0X8uWI1uNcmRgtUkaqOfE0NTfMX3hF6v+PMbb7xReB2d1JEECMxIQLCaEbTLECDQWyB7HkiwKm+GDHrGKq4Qdt/CVt6V29GSWwGnV+dRVv3iaveFCxf6Bq/p9U7LBAgQGCwgWJkhBAhUKiBYlc9vxap803yL/TavsLlCcXfBqrihFggQqE5AsKrO3pUJEAghCFblTwPBqnzT7mCV37wim8NxV8t+t69Nt0fNat2tgM2qp9EQaJOAYNWmahsrgQQFBKvyiyJYlW86KFjFn9mxrjxzm1eUZ6klAgRmKyBYzdbb1QgQ6BIQrMqfEoJV+abDglX8ebbTpY8OKO5vu/XihlogQGD2AoLV7M1dkQCBnIBgVf50GLR5RbzaoUOH7KhWgL3f5hWxyV6BoMClWn2qDwhudfkNnkAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgAABAgQIECBAgEAtBQSrWpZNpwkQIECAAAECBAgQSElAsEqpGvpCgACBmggsLCyEo0eP9u3t8ePHw6te9aqwffv2sGfPnrBx48Zlx548eTI88cQT4eDBg+Hq1aud486dO3dNezt27OicH19XrlwJ+/btCxcvXgyLi4thfn5+6fgzZ86E2Kf898+fPx+2bdsWLl261Dlu1apV4dixY2HdunU1UdZNAgQIEKiTgGBVp2rpKwECBBIUyIekubm5pR7GYBNDUQw83WEmfi++4s/7HXf58uVlwSx+vXv37hD/fe+99y4La919iF/v3bs3xICXhboYvrZu3brsewly6hIBAgQI1FRAsKpp4XSbAAECqQjkQ1K+TzHInDhxorMqlQ9c3YGp12pT1k5s+8YbbwybN28O2XG33357+PznP7+0khWPzfchHnfPPff0XJ0adK1UPPWDAAECBOopIFjVs256TYAAgSQEstvzNm3a1Ak/+Ve/wNW9QhWPe/bZZ68JYN1tZ6tSMTTt378/3HfffZ2VsOy4LVu2hNe+9rWd2wV79Sf2LVv1ys5NAlEnCBAgQKARAoJVI8poEAQIEKhGoN9tfIMCV37VaMWKFX2DUAxSccUre24qH9TyK1n5Ptxwww2d2wX7BSfBqpp54qoECBBog4Bg1YYqGyMBAgSmJNDv1rru2/26V7KyFapBG1esX79+KVT1un0wu83wT//0T5c2rnj++efD+9///vDggw8u29wiu/6g576mRKRZAgQIEGiJgGDVkkIbJgECBKYh0G/jin6Bq3slq1fQ6dVm93GxnRigbrvttvAnf/In4cKFCwM3wsjG3q+/07DRJgECBAi0S0Cwale9jZYAAQKlCgzauKJ7+/N44e6A1CvoZOFr5cqVSxtU9Apq8dz4iqEq2+Bi0C2Ig1bRSkXRGAECBAi0UkCwamXZDZoAAQLFBQYFlexncUOJbFOL7HOlbr755qXA1C+YxdD0gQ98YGlnv/h1tiqV9Ty29773vS988YtfDIcPHx64rXp27V27dl2zyUZxCS0QIECAAIEQBCuzgAABAgQmEhj2vFIWrvIf/Jv/XKlRVpfiZ1DFMNRrp7/s/CeffPKardW7r+3DgScqsZMIECBAYAwBwWoMLIcSIECAAAECBAgQIECgl4BgZV4QIECAAAECBAgQIECgoIBgVRDQ6QQIECBAgAABAgQIEBCszAECBAgQIECAAAECBAgUFBCsCgI6nQABAgQIECBAgAABAoKVOUCAAAECBAgQIECAAIGCAoJVQUCnEyBAgAABAgQIECBAQLAyBwgQIECAAAECBAgQIFBQQLAqCOh0AgQIECBAgAABAgQICFbmAAECBAgQIECAAAECBAoKCFYFAZ1OgAABAgQIECBAgAABwcocIECAAAECBAgQIECAQEEBwaogoNMJECBAgAABAgQIECAgWJkDBAgQIECAAAECBAgQKCggWBUEdDoBAgQIECBAgAABAgQEK3OAAAECBAgQIECAAAECBQUEq4KATidAgAABAgQIECBAgIBgZQ4QIECAAAECBAgQIECgoIBgVRDQ6QQIECBAgAABAgQIEBCszAECBAgQIECAAAECBAgUFBCsCgI6nQABAgQIECBAgAABAoKVOUCAAAECBAgQIECAAIGCAoJVQUCnEyBAgAABAgQIECBAQLAyBwgQIECAAAECBAgQIFBQQLAqCOh0AgQIECBAgAABAgQICFbmAAECBAgQIECAAAECBAoKCFYFAZ1OgAABAgQIECBAgAABwcocIECAAAECBAgQIECAQEEBwaogoNMJECBAgAABAgQIECAgWJkDBAgQIECAAAECBAgQKCggWBUEdDoBAgQIECBAgAABAgT+L6vpZZsO54V/AAAAAElFTkSuQmCC', `mes` = '8', `anio` = '2024', `tipo` = '1', `semana` = '0'
WHERE `id` = '276' 
 Execution Time:0.40687990188599

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '5'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.10637402534485

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAl8AAADnCAYAAAAguTMsAAAAAXNSR0IArs4c6QAAFLlJREFUeF7t3U+oHVcdB/C5SV7S1xgQ04paixVpqQpFcNdWlCq4cudCBBXRjYhQQVBcuOhCdCVFxI0uVBAXunKjokURKhZbpKCIRWypacSm7SK1TfKSd+W8cJ7nTc78u3n9vcl9n7tJ8t7c+c185ndmvndm7mSxXC6XjRcBAgQIECBAgECIwEL4CnFWhAABAgQIECCwIyB8aQQCBAgQIECAQKCA8BWIrRQBAgQIECBAQPjSAwQIECBAgACBQAHhKxBbKQIECBAgQICA8KUHCBAgQIAAAQKBAsJXILZSBAgQIECAAAHhSw8QIECAAAECBAIFhK9AbKUIECBAgAABAsKXHiBAgAABAgQIBAoIX4HYShEgQIAAAQIEhC89QIAAAQIECBAIFBC+ArGVIkCAAAECBAgIX3qAAAECBAgQIBAoIHwFYitFgAABAgQIEBC+9AABAgQIECBAIFBA+ArEVooAAQIECBAgIHzpAQIECBAgQIBAoIDwFYitFAECBAgQIEBA+NIDBAgQIECAAIFAAeErEFspAgQIECBAgIDwpQcIECBAgAABAoECwlcgtlIECBAgQIDAegksl8tmsVhMWqnJ4etLj7/UXL5yZVIRExMgQIAAAQIE1k3g6NEjzUPv3GxO3rw5adUmh68HHzvXPHx2Y1IRExMgQIAAAQIE1k3grs1l88T9J4Svdduw1ocAAQIECBCYp4DwNc/tYqkIECBAgACBNRUQvtZ0w1otAgQIECBAYJ4Cwtc8t4ulIkCAAAECBNZUQPha0w1rtQgQIECAAIF5Cghf89wulooAAQIECBBYU4G7TzbNn+497tuOa7p9rRYBAgQIECAwMwHha2YbxOIQIECAAAEC6y0gfK339rV2BAgQIECAwMwEhK+ZbRCLQ4AAAQIECKy3gPC13tvX2hEgQIAAAQIzExC+ZrZBLA4BAgQIECCw3gLC13pvX2tHgAABAgQIzExA+BqxQf7ywKnmXaeO7JnyzIXt5pOPv7rzsx++d7O57aa9v08//+v57ebdj5xvvveezeYzbzu++/7fPH+5+dCj/90zv/Y033/mUvPZP1+df379+t6TzQdvPbbzz1z/kXOXd3//wC3H9ixLex4P3X1T8+U7TzTHjzTNKvMfQWWSmQuUPVAuauqHH/9ra7d/cu/maT5x+0bznXs2m1PHFk3u3/a82u9J721PU/Z+bVy1e7vs6dr80/R5PuWYKOd9abtpvvnUxeZrf7uwZ+uU05y/vGw+/+SrzY+e3dodr+X7utzSDPNYKufXHuNtv6df2d6zT6i1Tbm+5fvztOUyz7ztLN6KAu3jQnlcKWdZmy7/vravHzOe2/uHO24+snv86Vqd1PePvnhlz3Hmdy9c3t131I5b5bKXy7ruPS989QyK2sbPk19P+Ko1YPtA1B4w7WBVO6C0p2nvnPvC15j5r7j/8LaZCHSFnRwgrjd81cJA+YEh1RkTvtJ0eV5nXl12BsLMOhS+2nXTv9tje7/DV3t8Xk/4ahu226n2YW4mLWcxVhRo74/bs2n3V1/4Gur/rg9T+xW+0kmE3MO141Yev+UYPAw9L3z1DI6yAdqJ/Ct33tR84cm9Z766PpnngZGa68SRxU7F8pN4DkUXt6/+vnZmKs8jNW+arjwDkVehNmDLZeoLX2Pmv+J+xNtmINB31jRt+/RaNXyl9+aerH1yTT1d69laaGqPub5lGhO+Xri0bE4fX1xzpjjXyb+fEr5qZxLSsrTDbfkhq3amIS9/3+/Kg2rX/LrO7M2g7SzCigJdZ1G7+qHcf+djS7m/n3omu6vH8+r0HUtqv+uavtb7h6Xnha8Vwlf5ljGXRWrBphwM+UCQw1ktfOXBmN539sL2zunf9hm0cllqQa9vwIyZ/4r7EW87YIFyB1c761oL71N21mX4Kuff/tCR+rp25qt8T9mjadqv//3idZ35qo2pcpy8FuGrVnOV8NUez/myaO0A2PXB74BbT/kVBPpCU5pd7aRALXx1Xc5L8xi67Ljf4avrONle7t+eu7w73mtn0odsVuA+sLcIXz307fs8xlzqK2eXG7g8CKXm+sibNq65rHLr8SPNL/+z1Xz4jRvXnPkqB0qa53MXltV7t9oN3g5pH7jlWPV9Y+d/YF2q8HUJ9O2Ex36Q6NtZp3nk3k1/z5+8c6D/+b+3mtR77bO1EWe+UihJr3TPZg4oeVw/f2m7eeaVZXPvG47ujsep93yVB4hyfX7/wpXmY7f9f5ynZWjfMzd05qs0r4Wrcrz3herrah5vDhcYGq/tDyjp/uGuy45d9wUOha9ypYdC0Nh7i8uTDPmDRP5Z7t/bNhe742Tde174GhhatRttyxDWd22+Fr6+9Y9LzRffcXznQFQGqXQg+OlzW83n7rj2hvgyvKWm7boPph2+0mXR/GWA2o2Q+Yb+sfMP3wspuC8CXZfP2zPvO4s7FL6++/TF5qNv2dj54knqtR88e2lnJ5peZc+Pveerdk/lKjfcp/f84cXLOze354PIp24/vnPmOC1LeqW/T7ns2HVgaofJb9+zuRv6vvHUhcnha+hTvvC1L8NjdjMZGq9TwldaudpJg4MIX+0rL+UtBbX7zoSvemsulsvlckrXPvjYuebhsxtT3jKradshbMoNwe1wk+4Xy5/E89mprnDUdUCsfYqoTVue1q2dWZsy/1ltEAszWmDok3Se0fWEr3S2K51BypfD85mftAPtCh5dXwAoA9qYy/p9N9y36//kzFbzvtNHm3SmuVzmKeFr6J6vHBzLM81dZ/+SfdclSWe+Rrf4Wk04NF77wlcZtPpuNxgKX/t92TFtoPZY/tlzWztXYtIrny0/TD3vzNfEYVseMIa+np9n3Q5f7z99bOeTeL55Pt2QnJovvdqPguj7inuefx4oXQeq2kEuv2fK/CdSmXwmAkNnUMrFrAWZ9Pu+m2jzzjP3b/qzvAG//Kr50D1fbbL9CF/pcS/lN6rSeEtnmtOjYr5614nJZ77Ghq/0GJjat7aGHkORH0Mz5Z4v33icyWDbh8UYGq+1bw7W7vlKi1L7JuHQeO56FFG5alNvuM/vLS8z5kv+5SXzw9TzwlfPYEmNkl7lM7lyM+dPGOUNgmO+7ZguG6ZXvv8j/b32STnv4Ie+cpven+t2Hahqj8xYZf77sF8xiwMQaF8ab/dp6rH03KnyK+FpMcuQUduJ5x1wDl/lWEg/y2eTyn4/qPDV9ay92hnkoW+OTQlftdsSxoav5HZYvvl1AMNi1iX349uO5X4/jcX0/K3HXrqy87y79nEs/WzoOZD7Eb7KfUbXt/YPS88LXwPhKz/UtD3ZmOd85Wk+/taNPfecpJt6a4Or75p437PBxlwCbR982mftxsw/LbfXjSnQ98y6MmgNTVcGh3b4Sjvw8sNCDnldl9W6zrKVwn33VOaezfdW1R6yWluG8tLMKuGr3QHZZMxZw/TeKeGrPHvR1XlDl4huzI493Es9NA7bN8GPec5X7cG+XY8i6urx/PNVz3yNfZ5k3zMJ2x8Mb9ROEb56tlzXJbmue1K6AlotfI35dJ3ml5+MP/RU/DFfy29fMp06//ZT+W/Upj/My107k1r7NlPfper2Djif+er69DyH8FWGmDKkRYSvVLt0nxq+0vtr+yLfcFz/kTzmsnVS6Atf7S9+ZbV2H/bdgtI37dhvO+a6Q/9TSy3g5Z+tU88LX+s/fq0hAQIECBAgMCMB4WtGG8OiECBAgAABAusvIHyt/za2hgQIECBAgMCMBISvGW0Mi0KAAAECBAisv4Dwtf7b2BoSIECAAAECMxIQvma0MSwKAQIECBAgsP4Cwtf6b2NrSIAAAQIECMxI4K7NZfPE/Seakzdf/T9wx74O3f/tOBbGdAQIECBAgACBPgHhS38QIECAAAECBAIFhK9AbKUIECBAgAABAsKXHiBAgAABAgQIBAqEha9fnHml+ePLRwNXbX1KLZfLZrFYNOnP9Or6e3uNx7wnTZNeQ/MeO68x85lab5X1uu/0sebRF6+sTxNYEwIERgu87mjTvGz4j/YyYbzA6WPL5tNvXr72N9z/6uyFZnt7O34NVTyUAm8/tdH88/zWoVx3K03gsAucOLpoLl65+mHVi8AcBY4sFs19r29e+/A1x5W3TAQIECBAgACBgxDIV7Wm1J78qIkpMzctAQIECBAgQIDAXgHhS0cQIECAAAECBAIFhK9AbKUIECBAgAABAsKXHiBAgAABAgQIBAoIX4HYShEgQIAAAQIEhC89QIAAAQIECBAIFBC+ArGVIkCAAAECBAgIX3qAAAECBAgQIBAoIHwFYitFgAABAgQIEBC+9AABAgQIECBAIFBA+ArEVooAAQIECBAgIHzpAQIECBAgQIBAoIDwFYitFAECBAgQIEBA+NIDBAgQIECAAIFAAeErEFspAgQIECBAgIDwpQcIECBAgAABAoECwlcgtlIECBAgQIAAAeFLDxAgQIAAAQIEAgWEr0BspQgQIECAAAECwpceIECAAAECBAgECghfgdhKESBAgAABAgSELz1AgAABAgQIEAgUEL4CsZUiQIAAAQIECAhfeoAAAQIECBAgECggfAViK0WAAAECBAgQEL70AAECBAgQIEAgUED4CsRWigABAgQIECAgfOkBAgQIECBAgECggPAViK0UAQIECBAgQED40gMECBAgQIAAgUAB4SsQWykCBAgQIECAgPClBwgQIECAAAECgQLCVyC2UgQIECBAgAAB4UsPECBAgAABAgQCBYSvQGylCBAgQIAAAQLClx4gQIAAAQIECAQKCF+B2EoRIECAAAECBIQvPUCAAAECBAgQCBQQvgKxlSJAgAABAgQICF96gAABAgQIECAQKCB8BWIrRYAAAQIECBAQvvQAAQIECBAgQCBQQPgKxFaKAAECBAgQICB86QECBAgQIECAQKCA8BWIrRQBAgQIECBAQPjSAwQIECBAgACBQAHhKxBbKQIECBAgQICA8KUHCBAgQIAAAQKBAsJXILZSBAgQIECAAAHhSw8QIECAAAECBAIFhK9AbKUIECBAgAABAsKXHiBAgAABAgQIBAoIX4HYShEgQIAAAQIEhC89QIAAAQIECBAIFBC+ArGVIkCAAAECBAgIX3qAAAECBAgQIBAoIHwFYitFgAABAgQIEBC+9AABAgQIECBAIFBA+ArEVooAAQIECBAgIHzpAQIECBAgQIBAoIDwFYitFAECBAgQIEBA+NIDBAgQIECAAIFAAeErEFspAgQIECBAgIDwpQcIECBAgAABAoECwlcgtlIECBAgQIAAAeFLDxAgQIAAAQIEAgWEr0BspQgQIECAAAECwpceIECAAAECBAgECghfgdhKESBAgAABAgSELz1AgAABAgQIEAgUEL4CsZUiQIAAAQIECAhfeoAAAQIECBAgECggfAViK0WAAAECBAgQEL70AAECBAgQIEAgUED4CsRWigABAgQIECAgfOkBAgQIECBAgECggPAViK0UAQIECBAgQED40gMECBAgQIAAgUAB4SsQWykCBAgQIECAgPClBwgQIECAAAECgQLCVyC2UgQIECBAgAAB4UsPECBAgAABAgQCBYSvQGylCBAgQIAAAQLClx4gQIAAAQIECAQKCF+B2EoRIECAAAECBIQvPUCAAAECBAgQCBQQvgKxlSJAgAABAgQICF96gAABAgQIECAQKCB8BWIrRYAAAQIECBAQvvQAAQIECBAgQCBQQPgKxFaKAAECBAgQICB86QECBAgQIECAQKCA8BWIrRQBAgQIECBAQPjSAwQIECBAgACBQAHhKxBbKQIECBAgQICA8KUHCBAgQIAAAQKBAsJXILZSBAgQIECAAAHhSw8QIECAAAECBAIFhK9AbKUIECBAgAABAsKXHiBAgAABAgQIBAoIX4HYShEgQIAAAQIEhC89QIAAAQIECBAIFBC+ArGVIkCAAAECBAgIX3qAAAECBAgQIBAoIHwFYitFgAABAgQIEBC+9AABAgQIECBAIFBA+ArEVooAAQIECBAgIHzpAQIECBAgQIBAoIDwFYitFAECBAgQIEBA+NIDBAgQIECAAIFAAeErEFspAgQIECBAgIDwpQcIECBAgAABAoECwlcgtlIECBAgQIAAAeFLDxAgQIAAAQIEAgWEr0BspQgQIECAAAECwpceIECAAAECBAgECghfgdhKESBAgAABAgSELz1AgAABAgQIEAgUEL4CsZUiQIAAAQIECAhfeoAAAQIECBAgECggfAViK0WAAAECBAgQEL70AAECBAgQIEAgUED4CsRWigABAgQIECAgfOkBAgQIECBAgECggPAViK0UAQIECBAgQED40gMECBAgQIAAgUAB4SsQWykCBAgQIECAgPClBwgQIECAAAECgQLCVyC2UgQIECBAgAAB4UsPECBAgAABAgQCBYSvQGylCBAgQIAAAQLClx4gQIAAAQIECAQKCF+B2EoRIECAAAECBIQvPUCAAAECBAgQCBQQvgKxlSJAgAABAgQICF96gAABAgQIECAQKCB8BWIrRYAAAQIECBAQvvQAAQIECBAgQCBQQPgKxFaKAAECBAgQICB86QECBAgQIECAQKCA8BWIrRQBAgQIECBAQPjSAwQIECBAgACBQAHhKxBbKQIECBAgQICA8KUHCBAgQIAAAQKBAsJXILZSBAgQIECAAAHhSw8QIECAAAECBAIFhK9AbKUIECBAgAABAsKXHiBAgAABAgQIBAoIX4HYShEgQIAAAQIEhC89QIAAAQIECBAIFBC+ArGVIkCAAAECBAgIX3qAAAECBAgQIBAoIHwFYitFgAABAgQIEBC+9AABAgQIECBAIFBA+ArEVooAAQIECBAgIHzpAQIECBAgQIBAoIDwFYitFAECBAgQIEBA+NIDBAgQIECAAIFAAeErEFspAgQIECBAgIDwpQcIECBAgAABAoECwlcgtlIECBAgQIAAAeFLDxAgQIAAAQIEAgWEr0BspQgQIECAAAECwpceIECAAAECBAgECghfgdhKESBAgAABAgSELz1AgAABAgQIEAgUEL4CsZUiQIAAAQIECPwPs8GBPKu9ldAAAAAASUVORK5CYII=', `mes` = '8', `anio` = '2024', `tipo` = '5', `semana` = '0'
WHERE `id` = '279' 
 Execution Time:0.093010902404785

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '9'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.31664109230042

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA3YAAAIUCAYAAABW5r5oAAAAAXNSR0IArs4c6QAAIABJREFUeF7s3Qt4HFXB//EfItggguGilNhI28hFEQJUAuGOclGxSjFQC3JJC9EiRWkb00jr21bTmrZoUSupaVTQWqxSCBURlBdQqtECwStgpFqMoFwqKKTc7P9/hp59J5Pd7Jnd2c3Oznefx0fanDlzzuecpPPLmTmzw7Zt27aJDwIIIIAAAggggAACCCCAQGwFdiDYxXbsaDgCCCCAAAIIIIAAAggg4AkQ7JgICCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfQJqPAAIIIIAAAggggAACCBDsmAMIIIAAAggggAACCCCAQMwFCHYxH0CajwACCCCAAAIIIIAAAggQ7JgDCCCAAAIIIIAAAggggEDMBQh2MR9Amo8AAggggAACCCCAAAIIEOyYAwgggAACCCCAAAIIIIBAzAUIdjEfwCQ3/6mnntK5556rH//4xymGb3/7297fjeTnoYce0jnnnKOjjjpKX/ziF3XDDTfovPPOU65ts/00ffrOd76jPffcM+/uDQwM6LOf/aymTp2qAw44IO/6ghUUos2RN7LEK7SG++23nzePKioqQrXYzBX/vFu4cKHmzZunn//85zrmmGNC1eVauBjncG1LuZWzP1ceeOABr2vBcQx+Pfjz5p577pEZH/szxI6Vqeu0004b9LPF/Hz41Kc+pY6ODu9cCxYs0Ny5c1Okwa83NTWlnaPmnMcee6wyfb3cxoj+IIAAAiMtQLAb6RHg/DkJBC9i/JUEL0JyOkEeBwWDXdgL8jxO7XSovSj75S9/qeuvv55g56RW/EJRB7vi94AzRiWQ7pdY/nCX6eehP/yZINff3+8FsKVLl3oh3/+x4cv8nT/U2TI2KAZDnf168Oeuv80Eu6hmAvUggAACwwsQ7JghsRPwX1j4fyttfzt86KGHeoFlr732Sq3e2d9S2zL2OPvn2bNn69lnn/V+Q21/e/3ggw96v202H1vevxJlyl1xxRXe1/0XNdlW7Oxvys25VqxYIfMbeHu8/Zrtg1lNS7f6Ndxv5+1KzVVXXeWtZpr/2fqqq6uHXLRlumAL/hY/OFHSXWzaC8lsK3Z+o912201LliwZ5Gz+YC2MjbkIte3ZZZddBvUhXTv9jl/96le9uuzK1/PPP5+aF+bvzDiYc0yfPn3ICrDtT7r2WlPTVrNC6x9H83fpLoCzrdr6V1FuvPFGfe1rX0u12/yCINuqjH+MhluxO/zwwz1DE+5Nv82Ft53nkyZNSvn6bcOMWTBQ2BDhr89+7wXnVfB70359uPmYzjq4omU9TH3+7y//LzpcLUwd/vrMn/3hZbjvQbtCHuz/cP2zZe3PieDYZvqzLW/7eNxxx+n0008f9HPRtN3c5fD44497PzftfLZ3HGzevHnQHQiZ/rzPPvsMWvXzz2WCXez+maXBCCAQUwGCXUwHLsnNHm5FzFxMnHzyyd6tZunCRaZgF/Q89dRT9Y9//MO7WPdfCNqw6L/90x5rL0hdg13wnCYc2Asr/4WiP4iYC7gnn3wyFST8ddgL2eAFpy1jLq7a2trU2tqausUq3cW8v07/BbD/7zP91t5enNqLRXsBHLx9dLgVV9sP/4WhqcdcpM6aNSvtaoK/ncH+27G0F6rW0z+GP/3pT/W9731vkIs5p+1PJvPgPDHHZGp/MFAEx3+4cTOrLPaC2s5Je3ymWytdgp291c7WZRzf/OY367bbbks1z4aDMGM2nIG9yL/vvvtSvzjxW5jvowkTJqSd45lW44NzJWidzjbTLwpcLLKF0lzH0vVuA1O/+WWIXXE3/f/BD36Q+nPw55wdO/NLDvMzzPysOeuss7zbK+338l/+8hcvmNlfaAV/4ZPp6+lWlm3/7S+v7Pdeqd29kOR/R+k7AgiUpwDBrjzHtax7FbxoydTZMMEu3QW8uTi1KxvmAsX8+cADD0yt6tiL1+BzJMHfaAefsfOvRJkLK3sRFGyD/Q14MCTdeuut3rNTwdUxuyJlz2cvoG17bH12xct/K2a6Z2Fsu1wuNoO3d6ZbLfWPk73QNH9nL06D5ws6mbLZ2ulfdQsGXevhD3aZQlGwP+bc5mLYttcf8O0FsG1vplW54Z4/869+ZppXdlwzjXvwotk12AXbHwxewXnkMmb+7xX/M4K2TZlW0zKtWmW7vTkYbNL9EsIGE/NLBv/PEP8KpatF0DrTilqm78FMv+jIFoBcbqNOV8a079JLL01763W21cBs5wzOMztWJjieffbZg1b7CHZl/U8znUMAgRIQINiVwCDQhHACwQuRKIKdDS/pwqD/gtwGO//FavA33nZ1J9PmKcEL/GB/gm0IBjtz+2bw+RhTxl4U2+CX6Tfu6YJdutDh+oyXf7VkuNtg0wU7u2pgvhY8n30OKN1tff6/8x/38Y9/XBdccEFq4xpzIRmsN7gCGrzITtcfG+zsmJo/21sZg8E0GOyCqzfpwmS64JLJIzjfM4Uhl2DnD/eZLtCDt+S5jpldGQquMJr2+43sedOtDgdv9822SYe18Ztkej7NlPWvAoexsAEluIqZ6VbJ4M8IO+dc+xccc394yrSpiX+eGeOf/exnQzY4Ge4XJcGfH+meyQ3+Mibbz0KCXbh/6yiNAAIIhBUg2IUVo/yIC2S7FXPcuHHeqlq6kBa8eM0WqswFWLpgZxDsc3vZLmYyrdgFV2YyhUvXYGcvjDdu3DhoN8Rg+9IFu3QrKcMFu+Bv8e2ze/biz3XFzr9C4RLssrUz12CXrT+5BLtgWB5uxS6fYJfpdtlCBTvXMRsu2Nm57r+l0R9Egh7BVfBMASFdiPavsgd/ePlvTw4T7OxtpMF+uAa74C9/svUvOHfS/Wzz3x7tt7R/X1VVNWhny0w7VgbviMgUStOt6Ge6RdX/i6codvUd8X+EaAACCCBQogIEuxIdGJqVWSDb5inmSP+tYHZTAP/GIcENGsKu2Jnns/K9FTPXYBe8FTMoFbygdwl22W5x9K8KmPMFL7ztKpi1dg12pq5st2L6L1KztTPMrZj+cJ6tP2GDXXCDCv+GL+lW7NLdimnH0a5SBW/FzPYzolDBznXM0q1u+9vsX+3KtMoZ/D513YTD/wsb+1ym/1ZMfzvS3WqYbfUyuJocDDnZvgeDq+rZXgsQ/LoNlul2sgzOLzu3zPewfc3FcOdzCdWZbtMm2GX7ruTrCCCAQGEFCHaF9aX2AgkMdwER3AkuuEGEaVIUwS7YtbCbp+Qa7DJt5OG6WhDcVdJ1U5JMF+X+vw97K2a62/SCG2+kW31It+lHMCDadmXaPCVdsAu2J7jzpeutmDbYpdtkJ9tmJ8F5FXxOK9jGTM9AFirYhRmz4TY1sSvLwf6a/phVd/McaSYL/4pdpo187C94TJjJtJmJ/znaMCt26V4XYM7n+j1og51L/0yZTLeT2p85mfpn2mOec5sxY4auvvpq79Ummeoabudc/8/MTJvopLslONuzkQX654FqEUAAgcQKEOwSO/Tx73i6C5Tgb/79FyHmwsPcqvehD30o72Bn9PJ93UGuwc7cyhS8uEq31XqmZ+z8m0f4L0aDF8jZXnfgv5g0F5BmN1Lzeghz3uCK1XCbRWR73UEwCLm00wYK0wdzQWsubNO97sDeTmtDng0Twf7YXRpdg525Fdj/ywczPh/72Md04YUXpnYjTPcd6A9C9nUV/uc5hxv3YH2FCnbGIMyYBcNdcFObdMEuuPupOeaOO+4YtPOj/ziXV0sEw89wz5BlW7Hzb8BjAtE3v/lNXXPNNbKrgq7PudpfUGTrX7pwl+29cvZ724Tk6667LnXreKZfiqV7BUS6F5RnCpEEu/j/m0oPEEAg/gIEu/iPIT0ookC297MVsSmxPlWhfpOf7jbdQp0r1gOQQ+NxzAGNQxBAAAEEECiiAMGuiNicKv4CBLtoxrCQISHd7X+m1dleDh5Nz8q3lkKOWfmq0TMEEEAAAQSKJ0CwK541ZyoDAYJdNINYyJCQ7rY8l3fxRdOz8q2lkGNWvmr0DAEEEEAAgeIJEOyKZ82ZEEAAAQQQQAABBBBAAIGCCBDsCsJKpQgggAACCCCAAAIIIIBA8QQIdsWz5kwIIIAAAggggAACCCCAQEEECHYFYaVSBBBAAAEEEEAAAQQQQKB4AgS74llzJgQQQAABBBBAAAEEEECgIAIEu4KwUikCCCCAAAIIIIAAAgggUDwBgl3xrDkTAggggAACCCCAAAIIIFAQAYJdQVipFAEEEEAAAQQQQAABBBAongDBrnjWnAkBBBBAAAEEEEAAAQQQKIgAwa4grFSKAAIIIIAAAggggAACCBRPgGBXPGvOhAACCCCAAAIIIIAAAggURIBgVxBWKkUAAQQQQAABBBBAAAEEiidAsCueNWdCAAEEEEAAAQQQQAABBAoiQLArCCuVIoAAAggggAACCCCAAALFEyDYFc+aMyGAAAIIIIAAAggggAACBREg2BWElUoRQAABBBBAAAEEEEAAgeIJEOyKZ82ZEEAAAQQQQAABBBBAAIGCCBDsCsKazEq3bt2qhQsXasKECTrzzDNTCOvWrdOKFSu8P48ZM0bLli1TZWVlMpHoNQIIIIAAAggggAACBRAg2BUANalV2gA3ffr0VLDbuHGjF+psmDNlzN/NnTtXo0aNSioV/UYAAQQQQAABBBBAIFIBgl2knMmt7JFHHtGqVav03HPP6YQTTvCCXboVvC1btmj+/PmaMWOGxo0bl1wweo4AAggggAACCCCAQIQCBLsIMZNalQlwV111lT74wQ9q9erVqVsxTYibOXOmzAqeuT3TfDLdrplUO/qNAAIIIIAAAggggEAUAgS7KBQTXoe5tfKee+5RU1PToGfsMq3OLV++XNXV1YOew0s4Id1HAAEEEEAAAQQQQCAvAYJdXnwcbMLb0qVLNWvWLFVUVBQl2N17773AI4AAAggggAACOQscccQROR3b8ssXcjou14MWH/W6XA/luAQKEOwSOOhRdtmsvh1zzDHerZbB2yy5FTNKaepCAAEEEEAAgZEWINiN9Ahw/uEECHbMj5wFbHB79NFHh9RxxhlnDLk10xRi85ScuTkQAQQQQAABBEZYoJSC3cDAgFpbW9Xd3Z1SWbx4sRoaGkZYidOPlADBbqTky/C86TZG4XUHZTjQdAkBBBBAAIGECpRKsOvr61NjY6Muu+yyVJAzvzyfNm2a6urq1NzcnNARSna3CXbJHv9Ie88LyiPlpDIEEEAAAQQQKDGBUgh2dqWuvr5+yOqcCXwm1LW3t2vPPff0gp75swl75rN27Vpt2LBBbW1t3iM0Zo+Es846S2alr7+/XxMnTtTs2bO9wNjb26uqqip1dXWppqbGO3bTpk1ePR0dHd7/m93Qbd0lNlSJbA7BLpHDTqcRQAABBBBAAAEEwgqUQrDzhzcTuDJ97ArecMHOBD+zU7kNeubPTzzxRCrMmYBoPqYOE+xaWlq8EGhu97SrhkuWLCHchZ1IBSpPsCsQLNUigAACCCCAAAIIlJdAKQS7np4eb0Wus7NTlZWVeQc7f/DzBzlTsTnXmjVrvOC3fv361Gqf2QndfILly2u049cbgl38xowWI4AAAggggAACCIyAQBKC3dixY1O3eAaDnbkV0//8nv/WThv2RmBYOOV2AYIdUwEBBBBAAAEEEEAAAQeBUgh2Ud+KGVyxI9g5TIQSLUKwK9GBoVkIIIAAAggggAACpSVQCsFuuM1T/M/V7b///lk3TwlurmJurRwu2NmNV7gVs7TmpW0Nwa40x4VWIYAAAggggAACCJSYQCkEO0My3OsO7GYoppx5z93o0aO92ydt6AtulhJmxc5snmJ3wnRdOSyxISzr5hDsynp46RwCCCCAAAIIIIBAVAKlEuxMf2xQM68lsJ/gC8ptADSvMqitrdXUqVN1++23D9oFM0ywu/XWW71T3Xnnnd7/+193wPN2Uc2y3Osh2OVux5EIIIAAAggggAACCRIopWBXbHaCW7HFw5+PYBfejCMQQAABBBBAAAEEEihAsHv15ebsgFmak59gV5rjQqsQQAABBBBAAAEEECgZAVbsSmYoMjaEYFf6Y0QLEUAAAQQQQAABBBBAAIFhBQh2TBAEEEAAAQQQQAABBBBAIOYCBLuYDyDNRwABBBBAAAEEEEAAAQQIdswBBBBAAAEEEEAAAQQQQCDmAgS7mA8gzUcAAQQQQAABBBBAAAEECHbMAQQQQAABBBBAAAEEEEAg5gIEu5gPIM1HAAEEEEAAAQQQQAABBAh2DnNgy5YtmjZtmpqbm1VXV+dwBEUQQAABBBBAAAEEEEAAgeIJEOwcrIPBbmBgQK2trZo8eXIq6Jkyo0aNUkVFhUONFEEAAQQQQAABBBBAAAEEohMg2DlYugS7np4etbe3q7OzU5WVlQ61UgQBBBBAAAEEEEAAAQQQiEaAYOfgSLBzQKIIAggggAACCCCAQNEE7B1k3d3dqXM2NTV5jw7ZT7oy9murV69O+4iRue6dNWuW5syZo5qaGq1du1YtLS1p+2XON2nSJDU2Nqq/vz9jmcsuu8y7262+vl5nnHGG99+jR48e1FZ7cF9fn/f3ZsHEnN98gm2ora1lMSWNNsHO4duPYOeARBEEEEAAAQQQQACBogiY8GPClAlJ/iBnwpC5i8zeQZbu8SHTQFNm9uzZ6urqSoUn2/B0wW7Tpk1pQ1iws5nOZ//eBLuGhgbv/JnudDN//9hjj6mtrc2r3oTAzZs3Dwpy5vgpU6YoUzgtyiCU4EkIdg6DQrBzQKIIAggggAACCCCAQMEFgiEpeEITjMzHBL5MQSsY3vx1FCPY2Wtrs1+FCXr+UOnfsNCs1G3YsMELecF9LHgMauhUI9g5fPvZydfb2zuotP+3BEwuB0iKIIAAAggggAACCOQlkO5WRX+F5uuLFi3S0qVLvY39ghv+mbLmuvWuu+5KuwpXjGBn2pAutPmvp23b7SpfEC1TaM0LN+YHE+xCDuBw9xlzv29ITIojgAACCCCAAAIxEpivCUVt7We1ccj5si0m+INZVVWVF+z8z+HZCidOnJh2JSzMM3bBWyFdb8U0bQgG1OBKpMvrxszq5NixYwet+hV1gErsZAS7CAbEhj2CXQSYVIEAAggggAACCJSoQFyDnf8VXTZUmWf0lixZMmQDlWKt2Jl2+G8bDQY9gl34bwKCnYOZy8RyqIYiCCCAAAIIIIAAAgjkJRDFrZjBUOVvUDGDnX/18Sc/+Yn8m7Rke5aQWzGHTiOCncO3FsHOAYkiCCCAAAIIIIAAAgUXyBZ4XDZPscHO3MZ42GGHpZ7JM+9iLmaws3055ZRTtGrVKu+Zv7q6upQhm6eEm04EOwcvgp0DEkUQQAABBBBAAAEEiiKQ7+sO/Kt+e+65p/w7UQaf4TPhKurXHfiR7CNN6Z75s8GP1x24TSuCnYMTwc4BiSIIIIAAAggggAACRROI8gXl9r1wpvFmwxX/++3CbBwYZvMUC5Xp1Qfpwp/9O/a1SD/NCHYO337pXnfAhHKAowgCCCCAAAIIIIAAAggURYBg58AcXLEz9y53dHQMOTK45atD1RRBAAEEEEAAAQQQQAABBPIWINg5EGa7FdPe57z33nurs7NT5sFTPggggAACCCCAAAIIIIBAsQQIdg7S2YKdQxUUQQABBBBAAAEEEEAAAQQKJkCwKxgtFSOAAAIIIIAAAggggAACxREg2BXHmbMggAACCCCAAAIIIIAAAgUTINg50KbbFZONUhzgKIIAAggggAACCCCAAAJFESDYhWS2G6X09/cPObKpqUnNzc0ha6Q4AggggAACCCCAAAIIIJCfAMEuP7/U0ebljWvWrGFXzIg8qQYBBBBAAAEEEEAAAQTcBQh27lZDSvpX7xYvXqyGhoY8auNQBBBAAAEEEEAAAQQQQCA3AYJdDm5mda6lpcU7kmftcgDkEAQQQAABBBBAAAEEEIhUgGAXgrO9vV0dHR2qra3llssQbhRFAAEEEEAAAQQQQACBwgoQ7Bx8g7tiEuwc0CiCAAIIIIAAAggggAACRRMg2IWk9t+G6T+UsBcSkuIIIIAAAggggAACCCAQmQDBLgJKG/YIdxFgUgUCCCCAAAIIIIAAAgiEFiDYOZDZWzHNO+rq6uocjqAIAggggAACCCCAAAIIIFA8AYKdg3Uw2A0MDKi1tVWTJ09OBT1TZtSoUaqoqHCokSIIIIAAAggggAACCCCAQHQCBDsHS5dg19PTI7NrZmdnpyorKx1qpQgCCCCAAAIIIIAAAgggEI0Awc7BkWDngEQR/Un36LXaSWN1JBoIIIAAAggggAACCBRVgGDnwE2wc0CiiL6nZv1Rd2i8jtZBOkkH6ETtqj2QQQABBBBAAAEEEECg4AIEOwdigp0DUsKLPKen9YDW6wH9UP/Unz2NSr0lFfCqdWjCheg+AggggAACCCCAQCEFCHYOusEXlNtDVq9endo8hWfsHCATUuRh3e0FvD/op6keH6gTZf/3Ou2aEAm6iQACCCCAAAIIIFAsAYJdSOlMLyg31fAeu5CYZV78aT3qBbzf6If6lx7zevsmjd8e8E7SaB1Y5gJ0DwEEEEAAAQQQQKBYAgS7CKR5QXkEiGVexW91qxfy/qxfeD19rXZOBbwDdLxeq9eVuQDdQwABBBBAAAEEECikAMHOQdfeimneW9fQ0OBwBEUQSC/wuB7yAp55Hm9Az3qF9tXbvZBnNlt5k8ZBhwACCCCAAAIIIIBAaAGCnQNZpmfsFi9eTNBz8KPIUIFX9KJ6vc1W1utR/cYrUKHdvHBnQt7bdKxeo9dAhwACCCCAAAIIIICAkwDBzolJGhgYUGtrq0aPHq3m5male9bOv5mKY7UUQ0B/1X2pZ/Fe0cueSLUO276Kd7z20BiUEEAAAQQQQAABBBAYVoBgF3KCmN0vp0yZoqamJi/gmY9d0TP/3dnZqcrKypC1UhwB6Xn9S726efsrE/o8kl21V2o3zfE6CiYEEEAAAQQQQAABBNIKEOxynBjt7e1av369urq6VFNTk2MtHIZAeoE/6edewPu9bk8VGKc677145jbN3bUPdAgggAACCCCAAAIIpAQIdnlMBrtSV11drba2NlVUVORRG4ciMFRgi/6uB7av4v1Lf/cKVKpq+22aJ+hpg4s9AAAgAElEQVStOhw2BBBAAAEEEEAAAQREsItgEvT19amxsVFLlixJvbA8gmqpAoFBAr/Tbd5mK33akPp786oEu9nK67UHYggggAACCCCAAAIJFSDYRTTw5tk7c3smz9hFBEo1GQX+qT7dr27vVs0BPeOV21vjvIC3v47VW3QIeggggAACCCCAAAIJEyDYhRhwu3FKukP8m6mEqJKiCOQssE3/1f26yQt4m9Xr1bOjdkrdplmjeu8VCnwQQAABBBBAAAEEyl+AYOcwxvZZurq6utROmA6HUQSBogmYd+H1bl/Fe0UveefdVwd578V7m47RaB1YtLZwIgQQQAABBBBAAIHiCxDsHMztO+y6u7vTlub9dQ6IFCmKwAv6j+7TTfqNfqjH9bB3zlHa1Qt4++s4L+TtpFFFaQsnQQABBBBAAAEEECieAMEuD+vgrZm1tbU8Y5eHJ4dGK/Bn/UL362b9XrelKh6jQ3WATvAC3ps0PtoTUhsCCCCAAAIIIIDAiAkQ7EaMnhMjUByBZ/VP3a8bvWfxtqjfO6nZQdNutlKjY/Qa7VicxnAWBBBAAAEEEEAAgYIIEOwKwpqsSjdu3Kg5c+Z4nR4zZoyWLVumysrKFMK6deu0YsWKjF9PltbI9vYP+qn3LN6fdE+qIeN0ZOo2zT1UPbIN5OwIIIAAAggggAACOQkQ7BzY7OYpvb2v7jyY6ZPEWzFNqFu8eLH3qodx48bJhDjzd3PnztWoUaO8/zahzoa94Ncd+ClSAIEntMnbUdM8i/ectnhneKP2lXkvnrlNc7yOLsBZqRIBBBBAAAEEEECgUAIEOwdZE+wWLFigefPmDVqJMi8mb25u9kJNTU2NQ03lV2T58uVepy6//HLv/x955BF97nOf05VXXql9991XCxcu1IQJE3TmmWd6XzeW8+fP14wZM7wgyGfkBcw78Xp1szbr/lRj7EYr5jbNN2r0yDeSFiCAAAIIIIAAAggMK0Cwc5wga9euVUtLiyZOnKi2tjZVVFSIYDcUz78iZ3YTnTlzpqZPn+6FO/PZunXrkLDnOAQUK7DA3/UH71m8Xq3Xy3rRO9te2k/7b1/F209HFLgFVI8AAggggAACCCCQqwDBLoSc/7UH5oXkkyZNSvyKneWzz9ntvvvuqdsyM63OmVW+6urq1CpeiCGgaBEEXtJW7zZN8z/7ygSzucqru2ke692quav2LEJLOAUCCCCAAAIIIICAqwDBzlXKV86s1DU2Nqq/v19VVVXq6upK7K2YQT4T5uwq3fjx49PedplvsLv33ntzGDUOyUXgyTc8qEf3ukf9e/wqdfjuz4/R3s8crDc/e4gq/8PttLm4cgwCCCCAwMgKHHEEd6GM7Ahw9kIIEOzyULW3ZyZx05Th2Oxzd+effz63YuYxv0rp0Of0tO7TjTLP423R37ymvU67eLdp1qjeW8mr0G6l1GTaggACCCCAAAIIJEqAYJfncAdvzzSbqSTlk+l5ORvszO2qbJ5SfrPhId2l+3STHtbdqc69Re/0btE0IW9fvb38Ok2PEEAAAQQQQACBEhcg2EU0QOb2zEWLFmnp0qWDds6MqPqSrSb4OoPg6w943UHJDl3eDXtaj25/Fq9bZkXPfHZRpfbf/hyeCXo7qSLv81ABAggggAACCCCAQHYBgl12I2+L/mnTpsm+x47n6gaj+V9A7t88xZbiBeUOkyzmRX6jW7yQ9xf93/OPb9Xh3iqe+d+blMzXgcR8WGk+AggggAACCMRIgGAXcrDsc3XBwwh7ISEpXpYCj+mh1I6aL+sFr4+7683bX3r+asjbUa8ty77TKQQQQAABBBBAYCQFCHYR6LOJSgSIVFFWAq/oJe85PLOK95j+mOrbONVtfxbvGO2lt5ZVn+kMAggggAACCCAwkgIEu5HU59wIJEBgs+73Ap558bn9VOotXsB7r2YnQIAuIoAAAggggAAChRcg2Dka8+46RyiKIZBBYEDP6n7vlQk36Un9Va/TrrpA12i0DsQMAQQQQAABBBBAIE8Bgp0DoH2lQX19vRoaGmRuvVyzZo06Ozu9HTB7eno0ZcoU8T47B0yKICDpWn1Mm7RRp+pTOlrnYoIAAggggAACCCCQpwDBzgHQ7opp3lFXV1cnG/ROOeUUrVq1ytstk1DnAEkRBLYLbNQP9EMt0v46Th/RF3FBAAEEEEAAAQQQyFOAYOcAGAx25pD29nZ1dHTIvIQ7SS8ld+CiCAJZBcxtme06WTurQufrGlXpHVmPoQACCCCAAAIIIIBAZgGCncPsyBTszKGEOgdAiiCQRuA6XapH1KP36DIdowswQgABBBBAAAEEEMhDgGDngJcp2I0dO9Z75o4PAgiEF7hPN+pmfU7jdbTO05fDV8ARCCCAAAIIIIAAAikBgp3DZLDBzjxLZz7mebrq6mrZzVQcqqAIAggEBF7Qc1qsE/Ra7ezdjjlGh2CEAAIIIIAAAgggkKMAwS4knH0ZefCwqqoqdXV1qaamJmSNFEcguQKrdbn+pHt0sqbrODUmF4KeI4AAAggggAACeQoQ7PIENIfbsMfOmBFgUkWiBB7Qet2o/9FYHanztSJRfaezCCCAAAIIIIBAlAIEuyg1qQsBBEIJvKwX9Hkdox30Gu9l5W/V4aGOpzACCCCAAAIIIIDAqwIEO2YCAgiMqMAaXaGHdLdO1CU6QZeMaFs4OQIIIIAAAgggEFcBgp3DyAU3T8l0CLdiOmBSBIGAwG/0I63TXG+17kKtxAcBBBBAAAEEEEAgBwGCnQOaCXYLFizQvHnzVFlZmTqir6/Pe4+deVk5m6Y4QFIEgTQC/9V/tVBHel/5qFZo3Pb/BgsBBBBAAAEEEEDAXYBg52hlN0iZOHGi2traVFFRIYKdIx7FEMgi8D0164+6w9sZ0+yQyQcBBBBAAAEEEEAgnADBLoTXwMCAWltb1d3draamJk2aNIkVuxB+FEUgk8Dvdbu+rzmq1qG6SKuAQgABBBBAAAEEEAgpQLALCWaKm5W6xsZG9ff3i/fX5QDIIQikEZivd0napvP0ZY3X0RghgAACCCCAAAIIhBAg2IXAChbl/XV54HEoAgGB76tVv9dtOkYX6D26DB8EEEAAAQQQQACBEAIEuxBY6YoGb880m6nwQQCB8ALmGTvzrF2V3qFGdek12jF8JRyBAAIIIIAAAggkVIBgF9HAm9szFy1apKVLlw7aOTOi6qkGgUQIfE5H6xW9pCn6kt6mYxPRZzqJAAIIIIAAAghEIUCwc1AMvseO5+oc0CiCQA4C6zRPv9EtOlrn6VR9MocaOAQBBBBAAAEEEEimAMEu5Ljb5+qChxH2QkJSHIE0Ag/rbn1XV2i0DvR2x9xJr8MJAQQQQAABBBBAwEGAYOeAlK0Im6hkE+LrCLgLtOlYvaStOkdLdaBOdD+QkggggAACCCCAQIIFCHYJHny6jkApCtyk+erVzarTR3S6ZpZiE2kTAggggAACCCBQcgIEuxBD4n9/nT3MvKicnTBDIFIUgSwCfdqg72iG3qy36SJ16nV6PWYIIIAAAggggAACWQQIdo5TpKenR7Nnz1ZXV5dqampSR7W3t6ujo0OrV69WXV2dY20UQwCB4QS+oBO1Vf9Rg76gt+vdYCGAAAIIIIAAAggQ7PKfA/ZddfX19WpoaBhSoVnJM6t2JuT5Q1/+Z6YGBJIpcLM+r/u0Tu9Sg96nTycTgV4jgAACCCCAAAIhBFixc8Cyrzsw4S3TqpzZQGXTpk3clungSREEsgk8ol/pOk3X3hrn3Y5Zod2yHcLXEUAAAQQQQACBRAsQ7ByG3yXYmVs1zYpdZ2cnLyh3MKUIAtkE2vVuDegZnaU2HaxTsxXn6wgggAACCCCAQKIFCHYOw0+wc0CiCAIRC9yiL+jXWqsjNElnqDXi2qkOAQQQQAABBBAoLwGCncN42mDX29vrlU73MnJW7BwgKYJACIG/6j59U5doD1WrUZ16vfYIcTRFEUAAAQQQQACBZAkQ7EKOt30ZebrDamtruRUzpCfFERhOYJlO03/0lM7UAh2i94GFAAIIIIAAAgggkEGAYBfB1LBhj2AXASZVIOATuFVL1aM1OkwTNVHzsEEAAQQQQAABBBAg2DEHEEAgbgKP6gF1aareqH293TF305vi1gXaiwACCCCAAAIIFEWAFbuiMHMSBBDIVeBLer+e0T+8FTuzcscHAQQQQAABBBBAYKgAwY5ZgQACJS1wm76kX+jbOlTv14c0v6TbSuMQQAABBBBAAIGREiDYjZQ850UAASeBfv1enbpAu+vNukBfV6X2dTqOQggggAACCCCAQJIECHZJGm36ikBMBZZrov6lv+sD+owO15kx7QXNRgABBBBAAAEECidAsCucLTUjgEBEAj/Rl3WPvqWDdZrO0ucjqpVqEEAAAQQQQACB8hEg2DmO5cDAgFpbW7V582beVedoRjEEohJ4TA9qpc7TG7SXLtRK76XlfBBAAAEEEEAAAQT+T4Bg5zgbzLvqNmzYoLa2NlVUVHhH9fT0aMqUKd5/NzU1qbm52bE2iiGAQFiBr2iSntJmvV8tmqAPhz2c8ggggAACCCCAQFkLEOwchteu1k2ePFl1dXXeEVu2bNG0adO8MHfIIYd4q3n19fVqaGhwqJEiCCAQVuB/9TXdrVV6u96jBi0OezjlEUAAAQQQQACBshYg2DkMrz/E2WBnVvA2bdqUWqUzq3dr1qwZtKLnUDVFEEDAUeCf6tPXNFm76I3e7Zh7a5zjkRRDAAEEEEAAAQTKX4Bg5zDGwWCXLuiZYNfe3s7zdw6eFEEgV4EVOltP6BG9V7N1pM7JtRqOQwABBBBAAAEEyk6AYOc4pCa0mY+59TLT83as2DliUgyBHAXu0krdqZU6UCfpHC3JsRYOQwABBBBAAAEEyk+AYOc4pvY5u+7ubtXW1g5ambNfGz16NBuoOHpSDIFcBJ7SX/UVnaVReoMuVIferP1zqYZjEEAAAQQQQACBshMg2OU5pHZnzIkTJ/J8XZ6WHI6Ai0CHpuhxPaxT9SkdrXNdDqEMAggggAACCCBQ9gIEu7IfYjqIQHkJ/ExdukMrdICO12RdVV6dozcIIIAAAggggECOAgS7HOE4DAEERkZgi/6uqzVRO2sXXaBrtK/ePjIN4awIIIAAAggggEAJCRDsHAbD7oLZ29s7bOngs3cOVVMEAQRyEFipj+ox/VHv0Qwdo/NzqIFDEEAAAQQQQACB8hIg2DmMpwl2CxYs0Lx581RZWZk6oq+vz9ssxeyYWVNT41ATRRBAIAqBe3StfqKrVaN6nauro6iSOhBAAAEEEEAAgVgLEOwch8+84qClpUX+TVIIdo54FEMgYoFn9U99Ue/TjtrZux1zjA6J+AxUhwACCCCAAAIIxEuAYBdivPyvPGhqatKkSZNYsQvhR1EEohRYpYv0N/1WJ2u6jlNjlFVTFwIIIIAAAgggEDsBgl0OQ2ZW6hobG9Xf36+qqip1dXVxK2YOjhyCQD4Cv9Rq/VhXaZyO1Ee1Ip+qOBYBBBBAAAEEEIi9AMEujyG0t2eyaUoeiByKQI4Cz2mLluoU7aDXeC8rr9ZhOdbEYQgggAACCCCAQPwFCHZ5jmHw9kyzmQofBBAojsA3NE2b1asT1aQTdHFxTspZEEAAAQQQQACBEhQg2EU0KOb2zEWLFmnp0qWDds6MqHqqQQCBNAI9ul63aon20xG6QB0YIYAAAggggAACiRUg2IUYev+zdfYws4kKq3QhECmKQIQCL+g5LdYJXo3n62saq3dFWDtVIYAAAggggAAC8REg2DmOVU9Pj2bPnj1koxTzDruOjg6tXr1adXV1jrVRDAEEohL4lj6mv2ijjtdUnaSPR1Ut9SCAAAIIIIAAArESINg5DJd9jq6+vl4NDQ1DjuB9dg6IFEGgQAIb9QP9UIs0RoeqUasKdBaqRQABBBBAAAEESluAYOcwPlu2bNG0adO8Wy4zrcqZHTI3bdrEbZkOnhRBIEqBl/WiPq96r8rz9BWN11FRVk9dCCCAAAIIIIBALAQIdg7D5BLszK2a5rbMzs5ONk9xMKUIAlEKXKdL9Yh6dKwu1Lv1iSirpi4EEEAAAQQQQCAWAgQ7h2Ei2DkgUQSBERS4XzepWwtVpYM1Td8cwZZwagQQQAABBBBAYGQECHYO7jbY9fb2eqWrqqqGbKLCip0DJEUQKJDANv1XC3SkV/sULdfbdEyBzkS1CCCAAAIIIIBAaQoQ7EKOi3mWrqWlJe1RtbW13IoZ0pPiCEQlsFqX60+6R/X6qE7R5VFVSz0IIIAAAggggEAsBAh2EQyTDXtJDXbr1q3TihUrPMndd9/de9Zw3LhxKVn/18eMGaNly5bxHGIE844qBgs8oPW6Uf+j0TpIl+g6eBBAAAEEEEAAgUQJEOwSNdzRd9aEtptvvjkV1jZu3OiFPBvegn825c3fzZ07V6NGjYq+QdSYaIH5muD1/xwt04HbX1yeaBA6jwACCCCAAAKJESDYJWaoo+/o1q1btXDhQk2YMEFnnnmmdwL7d+bPBx988JCvm+cV58+frxkzZgxa1Yu+ddSYRIHrNVMP6i4dpSk6TVckkYA+I4AAAggggEBCBQh2DgMf3Dwl0yFJvRXT72GsZs6cqenTp2v8+PGp/zbhzx/8/GHQYQgogoCTwG91q27QldpH+6tJq52OoRACCCCAAAIIIFAOAgQ7h1E0YWXBggWaN2/eoGfD+vr6vBeSm2fKampqHGoq/yL+Wy0HBgbSrs4tX75c1dXVqVW+8lehh8UUsLdjnq12HaSTi3lqzoUAAggggAACCIyYAMHOkd5ukDJx4kS1tbWpoqJCBLvBeCbUfec730ltnpLptst8g929997rOGoUS6LAxnHX6PHK+7XfEyfp4M2Tk0hAnxFAAAEEsggcccQRGCFQdgIEuxBDalagWltb1d3draamJk2aNIkVu+1+wVBn/tp/Wya3YoaYaBTNS+D3ul3f1xztrXGaru/lVRcHI4AAAggggAACcREg2OUwUmalrrGxUf39/WlfVp5DlbE+xKzAPfDAA0NeY5BucxU2T4n1UMem8fP1Lknb9GG16R06NTbtpqEIIIAAAggggECuAgS7XOUkJf39dYYu+LqDICevO8hjgnFozgI/UKt+p9t0hCbpDLXmXA8HIoAAAggggAACcREg2OU5UsHbM81mKkn52FstH3300SFdNrti2lcg8ILypMyI0unng/pfXa/Z2lPV+oRuKJ2G0RIEEEAAAQQQQKBAAgS7iGDN7ZmLFi3S0qVLB+2cGVH1VIMAAiEFPqej9Ype0iQt1Dv13pBHUxwBBBBAAAEEEIiXAMHOYbzse+zq6uq8zVL4IIBA6Qus0zz9RrfoMH1QEzW39BtMCxFAAAEEEEAAgTwECHYOeP4XlPtfd+A/tKenx9vmv7OzkxU7B1OKIFBogT/p51qtT6pSVZqhmwp9OupHAAEEEEAAAQRGVIBg58Bvgt2sWbN0ySWXeOHNvFzbvsvOHk6wc4CkCAJFFlik4/SiBvRBfVa1+kCRz87pEEAAAQQQQACB4gkQ7Bys7YqduQ3zkEMO8d5lt3nz5kGrcwQ7B0iKIFBkgZu0QL3q1qF6vz6k+UU+O6dDAAEEEEAAAQSKJ0Cwc7D2BzvznJ3dCdMf7gh2DpAUQaDIAo+oR9fpUu2uffRJ3SxphyK3gNMhgAACCCCAAALFESDYOTgHg509xNyWuX79enV1dempp57iGTsHS4ogUGyBxTpRL+g/+oCu1OH6ULFPz/kQQAABBBBAAIGiCBDsHJgzBTtzqA13U6dOVXd3N5unOHhSBIFiCtysz+s+rdM7dbom6XPFPDXnQgABBBBAAAEEiiZAsHOkNuHu6aef1vjx44ccsXbtWrW0tKi2tpZg5+hJMQSKJfAX3adv6RLtqr282zF31E7FOjXnQQABBBBAAAEEiiZAsIuI2oS7NWvWEOwi8qQaBKIUWKL36Hn9S+/XHE3QWVFWTV0IIIAAAggggEBJCBDsHIbB/x47U7yqqsp7rq6mpsbhaIoggMBIC9yiL+jXWqt36BR9WItGujmcHwEEEEAAAQQQiFyAYBeS1N52GTyMsBcSkuIIFFHgUf1GXWrULqrU5erWzqoo4tk5FQIIIIAAAgggUHgBgl0ExjxjFwEiVSBQYIFlOk3/0VM6XbNVp3MKfDaqRwABBBBAAAEEiitAsCuuN2dDAIERErhVS9WjNTpIJ+tstY9QKzgtAggggAACCCBQGAGCXUSuvKA8IkiqQaBAAo/pQa3UearQbpqhmzRKbyjQmagWAQQQQAABBBAovgDBLiJzgl1EkFSDQAEFvqT36xn9Q6fpCh2lKQU8E1UjgAACCCCAAALFFSDYReRNsIsIkmoQKKDAbfqSfqFva38dr4/oqgKeiaoRQAABBBBAAIHiChDsIvIm2EUESTUIFFDgn/qzvqZz9Drtohnq1i56YwHPRtUIIIAAAggggEDxBAh2DtYu77Ej2DlAUgSBEhBYron6l/6uUzRD9Tq/BFpEExBAAAEEEEAAgfwFCHYhDTO9x85UU1tbq87OTlVWVoasleIIIFAsgZ/oy7pH31KN6nWuri7WaTkPAggggAACCCBQUAGCXQS8vMcuAkSqQKBIAk9ps76iSXqtdvZeVr6r9irSmTkNAggggAACCCBQOAGCXeFsqRkBBEpUwAQ7E/DerUt1rC4q0VbSLAQQQAABBBBAwF2AYOduRUkEECgTgf/V13S3Vmmc6vRRfbVMekU3EEAAAQQQQCDJAgQ7h9EPbp6S6RCesXPApAgCJSBgNk8xm6i8Rjt6LyvfXfuUQKtoAgIIIIAAAgggkLsAwc7BzgS7BQsWaN68eYM2Runr61Nzc7Pa29tVU1PjUBNFEECgVARW6Gw9oUd0opp0gi4ulWbRDgQQQAABBBBAICcBgp0jm90gZeLEiWpra1NFRYUIdo54FEOgBAXu0td1pzo0VhN0vq4pwRbSJAQQQAABBBBAwF2AYOdupYGBAbW2tqq7u1tNTU2aNGkSK3Yh/CiKQCkJ/FtP6Cq912vSZbpRe+gtpdQ82oIAAggggAACCIQSINiF4nq1sFmpa2xsVH9/v6qqqtTV1cWtmDk4cggCIy1wjaboH3pYx2uaTtLHRro5nB8BBBBAAAEEEMhZgGCXM53E++vywONQBEpA4Gfq0h1aoWrV6iJ1lkCLaAICCCCAAAIIIJCbAMEuN7fUUcHbM81mKnwQQCAeAs/rX1qi93iNvVTf117aLx4Np5UIIIAAAggggEBAgGDnMCXs6w5MaKurq0t7hLk9c9GiRVq6dOmgnTMdqqcIAgiMoMDXdb7+rj94Lyo3LyzngwACCCCAAAIIxFGAYOcwasFgZ1fpJk+enAp6psyoUaO83TL5IIBAfAQ26Frdrqv1Fr1TU/WN+DScliKAAAIIIIAAAj4Bgp3DdHAJdj09Pd777Do7O1mxczClCAKlIvCCntNineA15+O6Xm/S+FJpGu1AAAEEEEAAAQScBQh2DlQEOwckiiAQY4FVukh/029Vr/N1imbEuCc0HQEEEEAAAQSSKkCwcxh5gp0DEkUQiLFAj76rW7VM++rtuljXxrgnNB0BBBBAAAEEkipAsHMYeYKdAxJFEIixwMt6UZ9XvdeDJn1H++iAGPeGpiOAAAIIIIBAEgUIdg6jboNdb2/voNKrV69ObZ7CM3YOkBRBoIQFvqmL9Vfdr6M0RafpihJuKU1DAAEEEEAAAQSGChDsQs4K+1LydIfV1tayeUpIT4ojUCoCG/V9/VCLtY/2V5NWl0qzaAcCCCCAAAIIIOAkQLBzYhq+kA17BLsIMKkCgRES2Kb/aoGO9M4+Td9Sld4xQi3htAgggAACCCCAQHgBgl14M45AAIEyFfiWPqa/aKOO1Nl6r5rLtJd0CwEEEEAAAQTKUYBg5ziqfX19amxsVH9/v6qqqtTV1aWamhrHoymGAAJxELhfN6lbC7W3xmm6vheHJtNGBBBAAAEEEEDAEyDYOUyEgYEBtba2qr6+Xg0NDTK3Xq5Zsyb1PJ3ZOGXKlCniVkwHTIogUOIC8zXBa+FFWqVqHVriraV5CCCAAAIIIIDAqwIEO4eZkOl1B6eccopWrVols1smoc4BkiIIxEDg2/qE/qxfaoLO0vs1JwYtpokIIIAAAggggADBzmkOBIOdOai9vV0dHR1qampSczPP4jhBUgiBGAg8oFt0o+ZpT71Vn9APYtBimogAAggggAACCBDsnOZApmBnDibUORFSCIFYCdjbMS/USr1Vh8eq7TQWAQQQQAABBJIpwK2YDuOeKdiNHTvWe+aODwIIlJfAal2uP+keHa4P6QO6srw6R28QQAABBBBAoCwFCHYOw2qDnXmWznzM83TV1dWpzVQcqqAIAgjESOD3ul3f1xztobfoMt0Yo5bTVAQQQAABBBBIqgDBLuTI25eRBw/jFQghISmOQIkL2NsxP6oVGrf9xeUl3mSahwACCCCAAAIJFiDYRTD4NuyxM2YEmFSBQIkIrNFMPaS7dKjO0If0PyXSKpqBAAIIIIAAAgikFyDYMTMQQACBNAIP6k5dr1naXfvok1qPEQIIIIAAAgggUNICBLuSHh4ahwACIylgb8c8V1erRvUj2RTOjQACCCCAAAIIDCtAsGOCIIAAAhkEvqdm/VF36J16ryZpIU4IIIAAAggggEDJChDsSnZoaBgCCIy0gHnlgXn1wRu0t67Qj0a6OZwfAQQQQAABBBDIKECwY3IggAACwwgs0JHapv9qsq7SAToeKwQQQAABBBBAoCQFCHYlOSw0CgEESkXgB2rV73Sb3qFT9WG1lUqzaAcCCCCAAAIIIDBIgGDnOCEGBgbU2tqqzZs3q7OzU5WVlY5HUgwBBOIs8Ih+pes0Xa/XHpql2+LcFdqOAAIIIIAAAn2A380AACAASURBVGUsQLBzHFzzrroNGzaora1NFRUV3lE9PT2aMmWK999NTU1qbm52rI1iCCAQJ4HP6Wi9opd0ttp1kE6OU9NpKwIIIIAAAggkRIBg5zDQdrVu8uTJqqur847YsmWLpk2b5oW5Qw45xFvNq6+vV0NDg0ONFEEAgTgJrNM8/Ua36O06WQ1qj1PTaSsCCCCAAAIIJESAYOcw0P4QZ4OdWcHbtGlTapXOrN6tWbNm0IqeQ9UUQQCBGAj8Vffpm7pEFdpNzbojBi2miQgggAACCCCQNAGCncOIB4NduqBngl17ezvP3zl4UgSBOAos0nF6UQPeBipmIxU+CCCAAAIIIIBAKQkQ7BxHw4Q28zG3XmZ63o4VO0dMiiEQQ4GbtEC96vZeeWBefcAHAQQQQAABBBAoJQGCneNo2Ofsuru7VVtbO2hlzn5t9OjRbKDi6EkxBOIm8Df9Vqt0kXbWLpqju+PWfNqLAAIIIIAAAmUuQLDLc4DtzpgTJ07k+bo8LTkcgVIXWKwT9YL+ozO1QIfofaXeXNqHAAIIIIAAAgkSINglaLDpKgII5CewXm26VzfobTpGU7Q8v8o4GgEEEEAAAQQQiFCAYBchJlUhgEB5CzymB7VS5+m1ep0+o3vKu7P0DgEEEEAAAQRiJUCwcxguuwtmb2/vsKWDz945VE0RBBCImcASvUfP61/6oOapVhNj1nqaiwACCCCAAALlKkCwcxhZE+wWLFigefPmqbKyMnVEX1+ft1mK2TGzpqbGoSaKIIBA3AV+pHb9St/TeB2l8/SVuHeH9iOAAAIIIIBAmQgQ7BwH0rzioKWlRf5NUgh2g/E2btyodevWae7cuRo1alTqi+bvVqxY4f15zJgxWrZs2aCA7DgEFEOgJAT+qT/razpHO+q1ulK/LIk20QgEEEAAAQQQQIBgF2IO+F950NTUpEmTJrFit93PhLo5c+boyCOPHBTszN+bUGfDnAl55u+C4S/EMFAUgREXuEqn6996UmeoVUdo0oi3hwYggAACCCCAAAIEuxzmgFmpa2xsVH9/v6qqqtTV1ZXYWzHNbaozZ87Us88+q8MPP1zPPfdcKrRt3bpVCxcu1IQJE3TmmWd60qb8/PnzNWPGDI0bNy4HfQ5BYOQFbtMX9Qt9R/tpgi7QNSPfIFqAAAIIIIAAAokXINjlMQXs7ZlJ3jTFBLXf/e53Ou6447zbMP2rcTb0TZ8+3Qt35pMu7OUxBByKwIgIPKXN+oomaQftoLnq0Q56zYi0g5MigAACCCCAAAJWgGCX51wI3p5pNlNJ6iddsEu3Ord8+XJVV1enVvGS6kW/4y3wRb1fz+ofep8+rXepId6dofUIIIAAAgggEHsBgl1EQ2huz1y0aJGWLl2a2I1BihXs7r333ohGjWoQyF3gwap16tvnVu3xn7ep/qFZuVfEkQgggAACRRc44ogjin5OTohAoQUIdg7C9j12dXV13mYpfNILcCsmMyNJAs/oMX1JH/C6/Blt0Gu1c5K6T18RQAABBBBAoMQECHYOA+J/Qbn/dQf+Q3t6erz32XV2drJit/11B2ye4jC5KBJrgav1QW1R///fI3Om6vSRWPeFxiOAAAIIIIBAvAUIdg7jZ4LdrFmzdMkll3jhzTwf1tbWpoqKitTRBDsN2TzF4PC6A4cJRpHYCtypDt2lr+stOkRT1RXbftBwBBBAAAEEEIi/AMHOYQztip25DfOQQw5Ra2urNm/ePGh1jmCXPtgZXl5Q7jDJKBJLgf/oSS3T6V7b5+hu7axdYtkPGo0AAggggAAC8Rcg2DmMoT/Ymefs7E6Y/nBHsHOApAgCZShgXntgXn9wii5XvT5ahj2kSwgggAACCCAQBwGCncMoBYOdPcTclrl+/XrvBeVPPfVU4p+xc6CkCAJlJ/AzdekOrVCV3q5purbs+keHEEAAAQQQQCAeAgQ7h3HKFOzMoTbcTZ06Vd3d3YnePMWBkiIIlJ3AgJ5Ru97t9atZd6hCu5VdH+kQAggggAACCJS+AMHOcYxMuHv66ac1fvz4IUesXbtWLS0tqq2tJdg5elIMgXIS+JrO1j/1iN6tS3WsLiqnrtEXBBBAAAEEEIiJAMHOYaCGW7Gzh5twt2bNGoKdgydFECg3gQ26TrdrufbR/mrS6nLrHv1BAAEEEEAAgRgIEOwcBinT5imTJ0+W2UzFfEyZUaNGDXoFgkPVFEEAgTIQeFHPa5GO93oyUz/WrtqzDHpFFxBAAAEEEEAgTgIEO4fRcgl27IrpAEkRBMpY4BpN0T/0sE7Sx3S8ppVxT+kaAggggAACCJSiAMHOYVQIdg5IFEEg4QI9WqNbtVRv0nh9XNcnXIPuI4AAAggggECxBQh2DuIEOwckiiCQcIGX9aI+r3pP4QrdojfoTQkXofsIIIAAAgggUEwBgp2DNsHOAYkiCCCgr+t8/V1/8G7FNLdk8kEAAQQQQAABBIolQLBzkLbBrre3d1Dp1atXpzZP4Rk7B0iKIFDmAhv1A/1Qi7SX9tOl+n6Z95buIYAAAggggEApCRDsQo6GfWddusN4j11ITIojUGYC27RNC/Qur1eXq1tv1L5l1kO6gwACCCCAAAKlKkCwi2BkeEF5BIhUgUCZCKzSRfqbfqtjdaHerU+USa/oBgIIIIAAAgiUugDBrtRHiPYhgECsBO5Xt7q1QHtojC7Tuli1ncYigAACCCCAQHwFCHYOYxd8xq6qqkpdXV2qqalxOJoiCCCQNIH5muB1+RO6QXuqOmndp78IIIAAAgggMAICBLuQ6JmesSPshYSkOAJlLPANXazNul9H6zydqk+WcU/pGgIIIIAAAgiUigDBLoKR4Bm7CBCpAoEyEviNbtE6zdPuGq1P6uYy6hldQQABBBBAAIFSFSDYlerI0C4EEIi1gL0dc7q+p701LtZ9ofEIIIAAAgggUPoCBDvHMerr61NjY6P6+/u9I/zvsHOsgmIIIJAggWv1cW3Sr1WnyTpdsxLUc7qKAAIIIIAAAiMhQLBzUB8YGFBra6vq6+vV0NAgG/Iuu+wy7898EEAAgaDA73W7vq85eoPepCt0C0AIIIAAAggggEBBBQh2Drx2V8zm5mbV1dV5R5jn6jZs2KC2tjZVVFQ41EIRBBBImoC9HfNj+q7erLclrfv0FwEEEEAAAQSKKECwc8BOF+x6enrU3t6uzs5OVVZWOtRCEQQQSJrAdzRDfdqgd6lB79Onk9Z9+osAAggggAACRRQg2DlgE+wckCiCAAJDBB7Unbpes7Sr9tBM3YYQAggggAACCCBQMAGCnQNtuheUT506Vd3d3azYOfhRBIEkC9jbMS/RdRqtg5JMQd8RQAABBBBAoIACBLuQuJleUF5bW0vIC2lJcQSSILBGV+gh3a3DdaY+oM8kocv0EQEEEEAAAQRGQIBgFwE6LyiPAJEqEChTgT/pHq3W5dpFu2u2flqmvaRbCCCAAAIIIDDSAgQ7hxGwt2JOnjyZ1xs4eFEEAQQGC9jbMafqG3qL3gkPAggggAACCCAQuQDBzoE0+IydPWTx4sUEPQc/iiCQdIG1+rT+oJ+qVh/QB/XZpHPQfwQQQAABBBAogADBzhHVvqR89OjRMu+zS/es3erVq1PvuXOslmIIIJAAgU36la7VdL1Or1eL7kpAj+kiAggggAACCBRbgGAXUty8v27KlClqamryAp752BU989+81y4kKMURSIiAvR3zQn1db9VhCek13UQAAQQQQACBYgkQ7HKUNi8nX79+vbq6ulRTU5NjLRyGAAJJEbhBV+q3ulXv1Hs1SQuT0m36iQACCCCAAAJFEiDY5QFtV+qqq6vV1tamioqKPGrjUAQQKGeBzbpf39DF2kmj1Kqfl3NX6RsCCCCAAAIIjIAAwS4C9L6+PjU2NmrJkiU8YxeBJ1UgUK4CC3Sktum/ukDXaD9NKNdu0i8EEEAAAQQQGAEBgl1E6ObZO3N7Js/YRQRKNQiUoUC3Fuh+detgnaqz1FaGPaRLCCCAAAIIIDBSAgS7EPJ245R0h/g3UwlRJUURQCBBAv36nTp1oXbUTrpSv0hQz+kqAggggAACCBRagGDnIGyfpaurq0vthOlwGEUQQACBIQKfV71e1os6T1/ReB2FEAIIIIAAAgggEIkAwc6B0b7Drru7O21p3l/ngEgRBBDwBNarTffqBh2kk3W22lFBAAEEEEAAAQQiESDY5cEYvDWztraWZ+zy8ORQBJIg8LgeUofOlbSDPqtfJ6HL9BEBBBBAAAEEiiBAsCsCMqdAAAEE/AJtOk4vaUBT9CW9TceCgwACCCCAAAII5C1AsHMgtM/Y9fb2eqWrqqp4MbmDG0UQQCC9wI+0RL/S9dpfx+sjugomBBBAAAEEEEAgbwGCXUjCtWvXqqWlZchRhL2QkBRHIMECT+gRrdDZnsBntTHBEnQdAQQQQAABBKISINhFIGnDHs/YRYBJFQgkROALOklb9W+do6U6UCcmpNd0EwEEEEAAAQQKJUCwi0iWF5RHBEk1CCRE4DZ9Sb/Qt1Wjep2rqxPSa7qJAAIIIIAAAoUSINhFJEuwiwiSahBIiMBT2qyvaJLXW27HTMig000EEEAAAQQKKECwiwiXYBcRJNUgkCCBJXqPnte/9GEt1jv0ngT1nK4igAACCCCAQNQCBLuIRAl2EUFSDQIJEvipvqqf6xsapyP1Ua1IUM/pKgIIIIAAAghELUCwcxB1ed0Bwc4BkiIIIDBI4F96TMv1Ae/vuB2TyYEAAggggAAC+QgQ7ELqZXrdgamGXTFDYlIcAQR0lU7Xv/WkztLndbBOQwQBBBBAAAEEEMhJgGCXE9vgg3jdQQSIMa3iHwPb9M0HX9KWF7Z5Pdj39Ttoxjt3jmlvCt9svIYa36mVuksrtZ8O1wVamSqAVbj5iBde4QTClWZ+uXth5W5FSQSiFiDYRS1KfYkSuPq3L3r9NWHO/mM2epcddP4BOyXKwbWzeA2VMqt1ZtXOfPy3Y2LlOqteLYcXXuEEwpVmfrl7YeVuRUkEohYg2EUtSn2JEbjn8Vd0+6Mv65Qxr9Ux++zo9fuHf31Zv3v6v7rwwJ305oodEmPh0lG8Mit9SWfoGT2uc/Vl1ehoYeUyo/6vDF54hRMIV5r55e6FlbsVJREohADBrhCq1JkIARPifv3PV3Tu/jvpbbu/xutzun/UEoHh0Em8MiP9TF26Qyt0vKbqJH3c+wUBc8thUm0vgpe7lf0FFPPL3Yz5hZW7ACURGFkBgt3I+nP2GAukW5370zP/1fV9L+ukqh1Tq3gx7mKkTccrM+fzekZL9G4dpomaqHlpV36ZW5n9mFvhvlXxwiucgHtp5pa7FSURKIQAwa4QqtSZCAH+AQs3zHgN7/Vlnam36jC9T5/Wj/+645Bbegl2BLtw33F44RWVgHs9/Jx3t6IkAoUQINgVQpU6EyHA7Tnhhhmv4b026Dr9XX/Qe9WsO//6Bm7FDDG9mFshsLY/C8ytmO5mzC+s3AUoicDIChDsRtafs8dYgIfEww0eXsN7vajn9V19Su9Xix56vJqNeUJML+ZWCKwMzwKz8VNmQ+aX+/zCyt2KkggUQoBgVwhV6kyMANs6hxtqvIb3ulYfV50+ogN0nK7+7UteYV6l4TbHmFtuTrYUXniFE3Avzdxyt6IkAlELEOyiFqW+RAnwItZww43X8F49WqMn9IjerUv17MBu+uaDL2nLC9u8g/Z9/Q5eyOOTXoC5FW5m4IVXOAH30swtdytKIhC1AMEualHqQwABBHIUeEUvqV3v1kn6mI7SlBxr4TAEEEAAAQQQSKIAwS6Jo06fEUCgZAU6NEWP62F9VF/VONWVbDtpGAIIIIAAAgiUlgDBrrTGg9bEWGDlypW65JJLYtyD4jYdr/Te9+oGrVeb3qTx+riu9wphFW5u4oVXOIFwpZlf7l5YuVtREoEoBAh2UShSBwJcfIeeA/yDn5lsviZ4X2zVz7WTRhHsQs4u5lY4MLzwCifgXpq55W5FSQSiECDYRaFIHQgQ7ELPAf7Bz0y2Shfqb/qdTtMV3rN2WIWbXnjhFU4gXGnml7sXVu5WlEQgCgGCXRSK1IEAwS70HOAf/MxkverWTVqgPfVWfUI/INiFnF3MrXBgeOEVTsC9NHPL3YqSCEQhQLCLQpE6ECDYhZ4D/IM/PJm9HbNFd+pbK1fz/GaIGcbcCoHFz65wWHiF8uJ7MRQXhRHIW4BglzchFSDwqgD/gIWbCXgN7/VNXay/6n69RzP0+5VbCXYhphdzKwQWP7vCYeEVyovvxVBcFEYgbwGCXd6EVIAAwS6XOcA/+MOr/VY/0g2aq0pVadTK9xPsQkwy5lYILIJKOCy8QnnxvRiKi8II5C1AsMubkAqyCaxbt04rVqzwio0ZM0bLli1TZWVltsNi93X+AQs3ZHhl97K3Y45eeQnBLjtXqgRzKwQWQSUcFl6hvPheDMVFYQTyFiDY5U1IBcMJbNy40Qt1NsyZkGf+bu7cuRo1alRZ4fEPWLjhxCu713Warkf0KxHsslv5SzC38AonEK4088vdCyt3K0oiEIUAwS4KRepIK7B161YtXLhQEyZM0JlnnumV2bJli+bPn68ZM2Zo3LhxZSXHP2DhhhOv7F5/0E+0Vi0Eu+xUg0owt8KB4YVXOAH30swtdytKIhCFAMEuCkXqSCtgQtzMmTM1ffp0L9yZT7qw58rX8ZD09YddSxe/3CWPrdTK0ZcU/8QZzrhN0g4l05qhDSklr1K2+sAHJnjB7ltjJ+mFrXuVxIiWspcBKqW5ZdqDV7hpi5e7F1buVulKXry/1HRAfnVwNAKlJECwK6XRKLO2ZFqdW758uaqrq1OreK7dJti5Sr1ajn/w3b1K2aquboYOvfFg3XniNu2wbQdt28G0ls9wAifeuYPnFZdPIcc1Xd3Bv4ub10iPK17uI2CsSukXnsGWE+zcx5KS8RAg2MVjnGLZyoIFu/mvrv7xQSAJAvucIL1rWRJ6Sh8RQKDcBJ66V9rQVKK9+uxGEexKdGxoVs4CBLuc6Tgwm0DSbsXM5lHsr5fyKlSxLbKdr9St9j+gI1sX+DoCeQu4rO6Zk7iWy7tBvgpcz1nI1c98+5NPH0qlX6598Fs9/FCpJjsR7PKd1BxfcgIEu5IbkvJpUNSbp5hbMePw2WEHaVuWu8BMGfvJVjbYZ/+x5mvB4+3XNz45VOuIPUtL0Fr5zaL4O9tLa+E3Svd39z5V+lbeBTVzK9QExisUF/MrBFcUP6fS/dzjZ9erAoX6dzFdvTxjF2LiU7TkBQh2JT9E8W5gkl53EO+RovUIIIAAAggggAACcRYg2MV59GLS9qS8oDwmw0EzEUAAAQQQQAABBMpQgGBXhoNKlxBAAAEEEEAAAQQQQCBZAgS7ZI03vUUAAQQQQAABBBBAAIEyFCDYleGg0iUEEEAAAQQQQAABBBBIlgDBLlnjTW8RQAABBBBAAAEEEECgDAUIdmU4qHQJAQQQQAABBBBAAAEEkiVAsEvWeNNbBBBAAAEEEEAAAQQQKEMBgl0ZDipdQgABBBBAAAEEEEAAgWQJEOySNd70toACPT09WrNmjdra2lRRUVHAM8W76vb2dnV0dHidqK2tVWdnpyorK+PdqQK1fmBgQK2treru7vbOsHjxYjU0NBTobOVTrXWrr6/HK8OwBucW34/Dz/+gFz+7MnutXbtWLS0tQwpgVj4/Y+lJ6QoQ7Ep3bGhZjARMqJsyZYomTpxIsBtm3Eyoe+yxx1JG5gJgw4YNmKUxsxeSo0ePVnNzs7Zs2aJp06Z5/11XVxej747iN9VeWBKEM9v39fXp2muv1Zw5c/hFlMMU5WeXA1KGIvZn1+TJk/lFS+6MHImAkwDBzomJQgikF7D/YD3xxBM68cQT9e9//5uQkuUfd38wMX6zZs3yLi5ramqYZj4Bc+FtrMwFpbUx/20+5u/5pBcwbl/96lf17LPP6vTTT+dCMsNEMb+Muuuuu5hLDt9I6b4XHQ6jyHaBYCgGBgEECidAsCucLTUnQMAEkwceeMALdaw+hR9wLpjczVixy25lVjkXLVrkhbmuri5xK2ZmM/Pz6pZbbtHdd9/tFeI2ueGtuLMg+/dfuhL8jM/NjaMQyFWAYJerHMchEBAg2IWfEqxAZTfzP9vT1NTECsswZHYV6rLLLvOeTSTYZcZKd2uheUaYZ16Hmpmf7Zs2bdLYsWNTz45x2332n12mBD/j3ZwohUBUAgS7qCSpJ/ECBLtwU8D8g28uxLmQdHdjjmW2MiuaCxYs0Lx58zRq1CiCnfu08kqy4czwK3ZmMxD/M5vcXph9gnGXQXYjSiAQtQDBLmpR6kusABfd7kNPqHO38pfktqbhV6BOOOEEb2MZQkpu88t8X5pVKXZeHexnV+z8z7byvZh9jplf3Jk5xS/vsltRAoGoBAh2UUlST+IFCHbZp4C94DYleS3E8F7pLoq4mExvZlcGent7hxTg9tWhZulWUuz3ptm5kF1XB5ule5UN34vZf95zG2Z2I0ogELUAwS5qUepLrADBLvvQc/tSdiNbIrjqFHz9gXtNySvJil32MWf7/uxGmb4Xzd/zs2x4P74H3ecXJRGIUoBgF6UmdSVagGA3/PCb33A3Njaqv79/SMHVq1ezSpCGL7gSxeqT248YLirdnEw46ejo8AqzK6ZbUOnu7vYKsnnK8F48X+f2PUgpBKIWINhFLUp9CCCAAAIIIIAAAggggECRBQh2RQbndAgggAACCCCAAAIIIIBA1AIEu6hFqQ8BBBBAAAEEEEAAAQQQKLIAwa7I4JwOAQQQQAABBBBAAAEEEIhagGAXtSj1IYAAAggggAACCCCAAAJFFiDYFRmc0yGAAAIIIIAAAggggAACUQsQ7KIWpT4EEEAAAQQQQAABBBBAoMgCBLsig3M6BBBAAAEEEEAAAQQQQCBqAYJd1KLUhwACCCCAAAIIIIAAAggUWYBgV2RwTocAAggggAACCCCAAAIIRC1AsItalPoQQAABBBBAAAEEEEAAgSILEOyKDM7pEEAAAQQQQAABBBBAAIGoBQh2UYtSHwIIIIAAAggggAACCCBQZAGCXZHBOR0CCCCAAAIIIIAAAgggELUAwS5qUepDAAEEEEAAAQQQQAABBIosQLArMjinQwABBBBAAAEEEEAAAQSiFiDYRS1KfQgggAACCCCAAAIIIIBAkQUIdkUG53QIIIAAAggggAACCCCAQNQCBLuoRakPAQQQQAABBBBAAAEEECiyAMGuyOCcDgEEEEAAAQQQQAABBBCIWoBgF7Uo9SGAAAIIIIAAAggggAACRRYg2BUZnNMhgAACCCCAAAIIIIAAAlELEOyiFqU+BBBAAAEEEEAAAQQQQKDIAgS7IoNzOgQQQAABBBBAAAEEEEAgagGCXdSi1IcAAggggAACCCCAAAIIFFmAYFdkcE6HAAIIIIAAAggggAACCEQtQLCLWpT6EEAAAQQQQAABBBBAAIEiCxDsigzO6RBAAAEEEEAAAQQQQACBqAUIdlGLUh8CCCCAAAIIIIAAAgggUGQBgl2RwTkdAggggAACCCCAAAIIIBC1AMEualHqQwCBvAS2bNmiadOmqbq6Wm1tbaqoqMirPg5GAAEEEEAAAQSSIECwS8Io00cEYiSwdu1ar7UNDQ0xajVNRQABBBBAAAEERlaAYDey/pwdAQQCAv39/aqqqsIFAQQQQAABBBBAIIQAwS4EFkURQCA3gb6+PjU2NsqENv9n8eLFg1bmBgYG1Nraqvr6+iErdu3t7ero6FDwGFOfvX2zt7c3VX1TU5Oam5uHbbA9X3d3d6pcuvpdy5nVxpaWliHnNEG1q6tLNTU13tdcy/X09GjKlClp+zBx4sTUrarpfIP9sH7Byvz12K8Fy6aztH1YvXq16urqUtXasTD25u/T9TXoYQ42fTDHmHMbJ5e+b926NeNtu+a8GzZs8Iy+/OUve3Mn08f0wXyisk53HuvyxBNPDJoLmdoUHIPa2lp1dnaqsrLSO8TFx5Qz309mfgfHyZqb78szzjhj0PfKcG213wubN28e1B7bJtNufzv9893le9I/X4LzJPh9mKk+0wbzufjii735YX8uZPp5Y7//g0b+tgT9/eM23M+t3H5ichQCCCCQmwDBLjc3jkIAgRAC5iI0eMFnL4ZGjx6duqgMhgJ7CnvRb/581FFHpb0InTx5cioMpqs72Fwbhi677LLUcfb8/rpsOf/Fry1ngos/PNoLymCg9IcM88yga7ngcenIbfuWLFmSCljBfmS68Az+fbrnG20Zc27/M4+mD3/84x910EEHDTIIBrR0fU3X5uAcce27sd599931mc98JhWcTVtdja2p6/lMEBrOOtO3han/u9/9rp588slBx6crb9r+2GOPDfI2x69ZsyYVmlzaa8Zz1qxZ3i8+Pv3pTw8K4P7vkRNOOGHQ14Zr63B1pmuTnVO77bab3vjGNw773Gy6Pto+jxo1ygup9udFpjlt5taiRYt05ZVXen2338vBny3B44Pz1tXb1nPvvfc6BfYQPzYpigACCIQWINiFJuMABBAIK5DpIjTdKs3s2bMHXSD5L0DHjh2bWoWxm6rYC7mlS5emVjNM+9KFSdvudAHOfs1/nPk78xt/f9AL1mFXpob7rb2/jfYCNd2qpGs5v79pr7n4DW404ze3K1u2rf7jTblNmzbJBFz/hbO/TKYAOHXqVN1+++2aN2/eoJUkG+KH62u6sGvDjDl3ppXbYNvNqpxZTdlll12GBPtMK7+mDn/4dl1xcbFOt9nP/2vv/lGlWKI4AN89iJmZ+DASRHABIiYGggtwHwYmgoFrMBNTBcHQxBUIYqS4AAP38DgD53JuWdXdPt8/qj4j9fbMdH3VPbd+c6pqMsg+ffr07O3bt92KdLYrzyWuuVEl9MaNG4d88nq6EOcyJgAACjJJREFUdevW2eXLly9UwaMtUXWLCuK9e/fOQ/Heueb9EX3/+fPn3Q82MqQ+ePDgVDltq3ltf8a/c31tfX/IPsuKbvy7vYZqP8bmS+2HSTXsbz02PsRpr7/2vSpeP1/v2rVrZ1+/fr1wH/zqe6TjCRAg8HcICHZ/h6LnIEBgU2BUPelN28vpczlAjoFkhr0fP378NFgbVc+2TuhItaM3cGyfM9oVYTMGor0qVC8sRhvaqs9WqOyFsXoevcpje569QWkek23oDYR7ATDOpz7fmzdvzg3SLIJiHDeqwMZxNUC3AXDrcfWc8rp6+PDhqUqT4T6rSo8fP75QxRsFuKOvd8S6d93VClyEmzZY9q6r8BmFoKPnm0E0gu/379/PQ1g4hFdUv1+/fn3uFuexd65578Q9+eTJk7M07gXSek9cunTpwlTbI2+Zvcplr/8zpI+Cdw1hEfbv3r17+sCm3lt74b79oKgNkR8+fNid+n2kzY4hQIDA7wgIdr+j57EECOwKbA1C25+1AXBvulS+eF1j11szthXIthpQg9towJ7BblQhbKuDR48brUuM82jXFtVjR+vh2sCcISynur1///5UuRutS8zKXvy8tuHLly/nFcN4zlrp2Kqa9iqjOdA+0vZ6bWSFJatco9cdBdwjr5f9v2fdXie9KX69vuhdo7EucLQesbdmtb028n6KqZY1eGRfthXwI+da79H2g426RjJDYobYo2E0HzdaT5tO9QOfWJM5qnTm8XVqZXy40p7rVrDrVfjb6l+8jp18d38dOIAAgX9YQLD7h4E9PYHVBbaqRbWyEgPYdvpTu85lb3BYA95oY4W9AWD2195x7UBwtCFKPF/dtOFXjjsSAOr1VUNHfc0jG6ccCbE5SK+D2tqHUZWplZCtymitrsSmOnWgfaSi2gshGUxHjx8FviOv1wtsGa56G+7E8b21nlthd/ThQYScunnHkfOt1+fNmzfPXr58eaquxbTcWHsWf6/V1iPn2t5/tQ8/ffp0oZo+Cl696bG9dm9VwPMeqpudbJnE9fru3bvzKd69Y/dCf11j234AFf9u1ymu/r6v/QQI/DcCgt1/4+5VCSwjsFe1yfVh7eC+hrQWazSQzuO2BoVbn8z3qkCjgWg7EGwHe6MQ+leP+5ULpg6qe4F5FCAyuLU/r23pre+KgXL8iQBRp0SOpuDGsTVI9tbbjc4lz629ruIcYw1brPd78eLFhemh+ZjR4H/rPPfc2wBTjx/tXNmrwm29Thu6jpxv7bMI3NkvUZmNP+10xCPn2l7zOaXz0aNHZx8/fjyv+Ob51t1ms31HdsbMY3vtjP9rp6mO7rXRxj+966D3PpUmNUBuVXf33pf2riU/J0CAwO8KCHa/K+jxBAhsCowG021FrB1YHRmEj9bU7FXbRgPjo+eQwSTDx2hg2Q76jx63t96nBpXe9MmtKtqos7YC+N5GLDHYjYrQnTt3TpuDxEYu8We0AUq74U3tj19pe1vRjOe5ffv26RzazUfaPkuHX3m9Pev8Oot47r+ynm+0NjCer93kZq/yVY3j8RF6Y13cq1evTl8DUKcjjsJ/r0LXbkiSoT5s9qYl770XtG2q4X8U0qpNnUa8tfZ2a+OUnE4Zx8R6yPo1Jb17J6/9qID2Ns7x64EAAQL/poBg929qey0CCwr0QlRv2lcdbEX1brTBSC9ktLtWtlM4W/beJhi9Kl9vcNgbYI6mcbWPP3rc3pTTbM+oMlk3nWgroaNLcDRwbiskbSjLEBMVodglMb+OYtSG9pzbAHS07b2poxFOnz9/fmpi/LwGrdHzHn29I9Z1YD+6BveCZG/DkPraf/zxx08bf/T6tN4n8fPon6tXr55dv379tNtmDfJRxatfpTAKvXWdZb0Gnz17dvbt27fT1zjk+fV2kj364UE4tn6jjVR691Tv/aUa9abx1vZvVWFb6wy21tct+MtNkwn8DwUEu/9hpzglArMIjKZT9qaiZQDMbffDoN3CP/6vNwWvfglxHHNkA5X23La+gLhdo9ZOudobsOYn/71dPbOva4Ugjht9WXYcvzc1rE53O7Ieq15v7RrAdurc6PnycWkzmtrX9k2vKrTX9lG4yeeK9rQ7So5C9eg80+RXrNvAXb8jsRrvTaVsr7d6vxw9314VtN5TGdJiR9H4EGXvXPO+bKtqGaLye9xi3V47VbK6tJuWjK69ej+O3kfu379/eviVK1d++i7F3uYy9Vquju012VuTOpo+a33dLL+ttIPAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCwsINgt3PmaToAAAQIECBAgQIDAHAKC3Rz9qBUECBAgQIAAAQIECCws8CdDEJZYvLDpzQAAAABJRU5ErkJggg==', `mes` = '8', `anio` = '2024', `tipo` = '9', `semana` = '0'
WHERE `id` = '282' 
 Execution Time:0.18665194511414

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '6'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.11428999900818

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABCYAAABdCAYAAAB5AWcuAAAAAXNSR0IArs4c6QAAHWFJREFUeF7tnX+sZdVVx/e7b+bNGygIDFiYKVOgQIxGSjVCW4P0B4mxFqO1FDGlpAGJlTQB+TEFCQSC0KFFpE1bpWCDNCKFVGNr1aRFsdpaTFMk0RighUIHMMx0ovyYmTfvzTPrPtaddffsfc4+797z4979ef/AvHfO2Xt/1tpr7/09e68zs7y8vOwq/Fzx3Z1ucWmpwh1cCgEIQAACEIAABCAAAQhAAAIQgMC0E1izZtZtPfUn3GyvV6mpM1WFiUsf2e7ueH5tpUK4GAIQgAAEIAABCEAAAhCAAAQgAIHpJvChn1x0f3b64QgT021mWgcBCEAAAhCAAAQgAAEIQAACEOgmAYSJbtqFWkEAAhCAAAQgAAEIQAACEIAABLIggDCRhZlpJAQgAAEIQAACEIAABCAAAQhAoJsEECa6aRdqBQEIQAACEIAABCAAAQhAAAIQyIIAwkQWZqaREIAABCAAAQhAAAIQgAAEIACBbhJAmOimXagVBCAAAQhAAAIQgAAEIAABCEAgCwIIE1mYmUZCAAIQgAAEIAABCEAAAhCAAAS6SQBhopt2oVYQgAAEIAABCEAAAhCAAAQgAIEsCCBMZGFmGgkBCEAAAhCAAAQgAAEIQAACEOgmAYSJbtqFWkEAAhCAAAQgAAEIQAACEIAABLIggDCRhZlpJAQgAAEIQAACEIAABCAAAQhAoJsEECa6aRdqBQEIQAACEIAABCAAAQhAAAIQyIIAwkQWZqaRk0jg/GPXus+cst4dsmbG3f3DBXfRo7tqbcbX336we/dRa9xLi8vuksd2uXuf3VtreTwcAhCIE7jxp+bdlpPWubmea6T/YwsIQKCYwKhjZGhM/893HeJ++pCe27Z7n/vQd3e5h7YvYgYITD2Bdx25xv35z693m+Z77hsvLrqzvvXK1Le56Qb68WrbruWJYI4w4ZyzE8CFfc5tfWKPu+6/dzftQ5TXMQLaqW21mly033XqenfhG+f6xTcRuEeddHXMfFNTHRuf/EY1IVhNDcgGG2InXX6x//XSPvczD71UWhuEiVJEnbxAF5pSudh8wl7T5JjSSWAdqpS1S6ifjjpGhsb0XIUJP0aG+oF/DeJNc53Fimh1zD/t+JY6JjbX+u6UNIod/Hh10sGzg5cdXWaOMOGc8xegdXTCKm6u9emy41Rpz6RdW7SoqGsSGbI5OyYmzXPqqS/CRD1c63wqwkSddLv9bLu4DYnKNq7L3+saU7pNqXu1S7HLqMIEOyb22z0UI32h3R/7ECaa6zejLIhTasmOiRRKzo1iB3ZMlDC+9JHt7o7n16ZZosGrrNF3LCy7DXMzrW6ps50VYaJBRzBFxXYqiG0+fcp69/Endo/1mENXbD7qpKsda01/qbw5nzwbj6NPY/fJs7vUWIWJ2HxC46z+HWGiG3a249+63kzwCFUdYyQ7JnpO+4I/5y3rS93wnOmsxSgL4ukk0k6rRrFDHfGqCQrZ75jQRahMDr60ba87/9iVrfOh4xz2TYgotz98ddm9/YjZA954+Dsw/O2cIaVYnvfQi0vu3E1r+wOi/bE7OMqe3YTTTHsZVY5Q+PawA6sNCrd/f8Fd9qa5fr4I+xYt9jZcbP6tHy8Fz5j799iJrR/EpCzJGyE/WrfYdtWU+spz7HX/tH3RnX302iExr+yN4bT7z7jbV7ZALbJ56A1TkX3KbEv8SbNuqjBRFD98u7/tiDX9s+jyY+1qr/vKC3vdO45c42RhpWNYahmhe20s1JZzfKjYB7R/SVz2F7jWL0LCRIi3tbXt6zI+vPGgmf4Z7Ye3L7qfO2x2ML7YGsbu12t4AeKc//b2mPlev6/5bEYdI+0cT/sRwsR+YcLOldXXpQ/t2bfc923ry0V9ZdP6mUF+LmvD0I4Vf6eMnSulRfvpvCq0IA4tdNUO1nbWpx98bq/7yHEruZJC9hW72jVObH2Ua/6VkB1Cc8IUe4mnat66LjPPXpjQDiTB66OP7RokBokpt6EQpAtDm1gkdJ0MRH/xo72DMvzJQ5EwcfPje4L3MVkc/6AQGqhCk3F/gedP9DSISzDWgdXWVp753O7lgfjgi1EhYSI0GMt96oM28OjE1z439DttW0p9JQFnKPeGTBhuf3LBXXbiXH+i7P+0fTxq/F7S3BNj4lWqzZV90fECvWY1thUSLFaH/SFFmCiLH9buoX6rY1TIP3QC+Jsb1w7EDFvDUe6V59Cf4/3fzinkKrvAVVu9uBB+sRHqf1aIsgsuW4MUYeK337B2kLPIr33uW+TVLsJFBL2N8zN9Vv5LpVHHSISJ/Z5nY6QV2TS26FxHYpX2I+unZX1FdrdK37MvbuyLSEnwfeaGNfSJSChLWehKgvQiYcKKs1qM2tDGMrV50bHVXHeW1S1MdJF51sJESD0NKYLWcNqB7L3aYWyQ04m6Db7SIaUjX3L8ypvz0OQuNqG1C9LYs3NVFOtYIobECbsACymW+jsRIWTQu+DYucFuhZDf6OIgZnO/DCtqhe4Vv3h4x+JAEdUBQN6ganZ/9VWbBCe0KC2qr50QWB8ODVD6u9wnvqP4aBVhIiRWKHtdmNjJtm+fa05ed4DPSt2JP9UsWCQC+YKkxnM/ftg+avuPfSMvcSZ2XUqMSrnX9nG/bL7cc6BfWGHi2z9e7C98tF/qmCBM5afsK0h+TH3ilaVBfC+aqPt2si9NYm+dcxYXdUwrWjSJvUJjn50r6LgcGyNDfZIdEytfZdD+oDZQYUH8UneLFc0j/L4iz/O/ahR7EUmfODCOjUOYsHNmZa/zDxvLdIwJjS+h9Ve10Xiyr65bmOgi86yFiTIRQgfq0ILLDlL+pMOfMPjlfOyk+aG3WDYoxhapsbNCk3qGaFJChS9QaFDVNyqhdug1cswnNPH0JyLyDP1skt2p409irOgQKleC+z3PLhywVSsU2FIHHSnHr68uXmNvk0J1y1XtHoefVz3KoZ/ditmtyD66cIrZtiy2sVBdoVsmTMg1+sUd3x7KPjSxtiJR6LqQUFgUo2wZZSJjqGy+XlUsTEg+It06+5fb9rozNsy6o+Z6/bfyofEhJkKGJvOxIxj2rb4e54mdUx7l/PI4YlsXnhFjEBIMYvOt1DESYWK/xf3jM7pDVK6499kF94FNKznpRHzVOXPsCJv1I+0rcszUzqu0L+oxt9DCWJ5Dn1ihmTpHLDvKoS9Mi0RWGXvsrnAb21J2H3YhjtRVhzqFia4yz1aYKJo4qoP56nfZZF2DZ8rkPXZW2w+m+lm52JsqhIm6wsHwc/03IPJXzd0QqoGIWscd1BurMBE79mH91U6EQ7sein5XddLl+3lsa7rUj8/wrt5PxyVM6BuoooVqmZiWEttW39LpubNsMhXbhqwEYrsqyoQJ+9Z7lDJCi1uEiTT/tG9lZfz2c07IMQ6ZrKvAG9rl5O/CLHrLaGsV2tkkf4+9dWQRNrwbrCg2igg36hiJMBEXJuwiyU+G6Qs/9hhArK+IveyOwG/uWHK/tWl/Piy7k9SKsvSJFRuFfHU1OSZShQn7Ug1hYn8/Kdr5KHk7dMxPEZLkqTbHRFeZZytMFJ2rUZcIHdEY9SiHf9wiZbu+nRDK/3OUI22CuNqrZDCTrYMqCslz7CRf+MuPvvGMbYH175H8DNbeVY9y2B0TsbdlqbsjioKY9bFQfct27yBCrNbzwveNS5iI7XSxpcZsy1GOajYtEyZiC0hbSlmsCB3LsrGoahn23thClqMc5X7gCxN+XiD/6JxvRz0KKLuPfIEo9pa3SHyQv/lHSv3Fgo355S2criuKBHVtaei4o38ESxYJ/sssX8hFmIgLE7LTzxdTYwlCVVQo6isiTIQSaIZyLnGUY8Uuf/yz8+7/9jon7PyjF1aYCyW6jCW/TBUmxP5dPFbQRrQrs0NofA4l7ffnc74w0VXm2QoTsTdCIREglrDSFzBSkl/GtuPbASyUNZ/kl82Fh1iCSalBSqLTokSC2gp/8R6yeSj5ZVnCp1BCoVSxougNq61vbPEay8uR86R3HF5bJKLGju9IuUVvmfx6+QlQ/Ql12Q6znM+nh2xcJkykJCItE89DCcOsHaqW4duwaMFG8st4z/aFCRsXi+KozffhP71sx0RR7PVzzIRqnmsOoFB+COUTEnNsDp6YjYp2ViBMFAsTlk9oThzKm1VkBzsOyv/7866iuV6OfSLEw74EK+I1DmGii4kYxzGHq/qMMjsUxXspKyZ+h4SJLjLPUpiwRg0Fn9Df/WzYRcmr/Ald7HOOvrCh57N9pwslH4vdW7UDcP2BBGKdPrRLITR5DwUF/axmaHCU34VsHvtcaOpnskY5ylFU36LjQ7HFEAvX1fe0cQkTD21fjOY+KBMmtPZFsW31LZy+O8uEiRhPO6mwdtdPefqfG5bry3bUFMWosntjRw7JLZEuTIREQumLRdui5R6Zm+j28zJhInWBFYolOYtMoV2N1rL+3+3xzNWMkQgTxcJELG6G8n1Ynw/1FY1RoZ1n1sb0if00Yru7Yn0ixr3IXmWxLDT/zu2Txil2sH4rTDUvi/2wQsqOidD6Q37XJvMshYlxTINjWyPH8WyeMR0EyP8xHXakFRCAAAQgAAEITB6BMvF18lpEjSEw3QQQJhLt+y9nvM796dN7+p/7lB8/GaJmwk98HJdlQABhIgMj00QIQAACEIAABDpJgLw4nTQLlYJAlADCRKJzxM7b5ngOLRFZ9pchTGTvAgCAAAQgAAEIQKBBAqEjpRwnbdAAFAWBEQggTCTCKzqnm/gILsuMAMJEZganuRCAAAQgAAEItErAFyYQJVo1B4VDoBIBhIlKuLgYAhCAAAQgAAEIQAACEIAABCAAgXESQJgYJ02eBQEIQAACEIAABCAAAQhAAAIQgEAlAggTlXBxMQQgAAEIQAACEIAABCAAAQhAAALjJHDB65fc3acd5mZ7vUqPnVleXl6ucselj2x3dzy/tsotXAsBCEAAAhCAAAQgAAEIQAACEIDAlBNAmJhyA9M8CEAAAhCAAAQgAAEIQAACEIBAlwkgTHTZOtQNAhCAAAQgAAEIQAACEIAABCAw5QQQJqbcwDQPAhCAAAQgAAEIQAACEIAABCDQZQKNCRN/v+1V952XZ7vMgrqNkcDG+Rn33O5KaUjGWHp+j9KULzMzM4PGx35nr5GL5bqmfzdKmanW9cuowkjKsExOfl3PPf7yvtSiJ+Y6ZRSyR6wRVa6tE4TW46g5515cqLOkyXh2yL+Lah5LE+XHgnG0flxlFaW2OuHgnnvq1ekec7rS90b1iVg7Tjio537w6vTF2VF51XX/kXMzbvvC9PQZGx+qxjE/tqTcP87yfBunlF+XX/DcYQKsZ5r1iDcfvM+dvXFd/ckvRZiYXzfXbOsorTUCh6yZcS8tTs+A1xpICu4EgQ1zM27HFE3gOgF1TJU4eHbGvbJErBkTzsYfIxNwK6rYRas/8fevUwFRfn/EXM/t3Ls89CwVXu11OuGvUo69xz5LYaW0oegZer9/Tags/V2obSHjxZ5t6+yXU7U+sXqW1ce372Frndv5WpxNYRpazBX5SJG9Ygyq2HjS6rO+59yrSysvJqr0tdX2gRQ+fl1SfNP6a5H9U5/tx4uQaDGOmOXHIvuioChmVa1PFT4aV4ruGTWGrqY+qX03FjdC9gr5SqgcXc+k2ivmP1WYptS3rK1F8besjil/T5kshPqcvS/09z17FtxZx8zXL0w89ezz7vhjj0lpB9dAAAIQgAAEIAABCEAAAhCAAAQgkAmBZ7b9j9t0zFEIE5nYm2ZCAAIQgAAEIAABCEAAAhCAAAQ6RQBholPmoDIQgAAEIAABCEAAAhCAAAQgAIG8CCBM5GVvWgsBCEAAAhCAAAQgAAEIQAACEOgUAYSJTpmDykAAAhCAAAQgAAEIQAACEIAABPIigDCRl71pLQQgAAEIQAACEIAABCAAAQhAoFME5GMZmze9nuSXnbIKlYEABCAAAQhAAAIQgAAEIAABCGRCAGEiE0PTTAhAAAIQgAAEIAABCEAAAhCAQBcJIEx00SrUCQIQgAAEIAABCEAAAhCAAAQgkAkBhIlMDE0zIQABCEAAAhCAAAQgAAEIQAACXSSAMNFFq1AnCEAAAhCAAAQgAAEIQAACEIBAJgQQJjIxNM2EAAQgAAEIQAACEIAABCAAAQh0kQDCRBetQp0gAAEIQAACEIAABCAAAQhAAAKZEECYyMTQNBMCEIAABCAAAQhAAAIQgAAEINBFAggTxio7d+50F178EffW009zV13x+4O/PPDgl92Wa64d/Pt3L75o6O++Yb/zyL+78z54wdCvf+3s97pbbrrRrV8/P/i9lvfofzzW/529xj7j1Def4u6+83Pu8MMP71+3a9dud/W117lffNtb3Tnvf18X/Yo6jUBA/O2pp58O+pj1xU2bNrov3HWnO/FNJ/RL8/3pvi/e404/7ReG/Phfv/1vB/jhCFXl1g4S8P3Aj1fWh/y4pPduufLyId/pYDOzqpI/pti+rePB33zlq30mW2++aWhckHu3fuK2oTEkK3gT0lj6bXcNxbjbvm2KYqDUjnGteRthk+aZa4n+uB+by+n6cpLWAwgTxq80sPkT+a/93T+4d77jzL6oEBMvrHtKZ73v/gcKF4BPfv8H7sMXXew+ufWWAxYAUsblV13trvnYVf1F562f/KP+41UsKVq4ttdNKHlUAmLnP7nzrv5jQuKX2P2++780WGD4/5b7jz/uuP6ixF+MiL/d/PFb3W233jIQuEatL/d3j4DGp/PO/UDfD/x/Wz+Yn19/gMBpfah7rcuzRtKXr9hy9UCE9P9txwOx75Vbrnaf2HpLf+zwx5I8CXa/1fTb7tqIcbd925TFQMa15m2ETZpn7osSG485ur8uVJFC/y3XTfJ6AGHiNUtrYDv00EOdNW7I9WSgKnrzXPZ3daLzzj0n+FZSOvzD//zNgRAhdbvn3i+6a7Zc5Xbv3uVuuOkWd/21V7PAbC8ujL1kKyR8/u4vDAlR8o/Qm2y7c+asd79zyC/kbzdvvdVdcP4HB+LWmb90Bm/Bx265bj0wFHusb339G//Yr7DutLKxxsYZu7OrWy3Mqzax3XEqVn/0kt8b6uc6IdG+Lv5g7Z0XvclpLf22m7Zi3G3fLmUxUBZmfpxjXKvXbtikXr5lTw/tgrQvJTYccfhErwcQJszRCBEKRBCQH3uUw3eSMuGhbEdD2dbaImHi05/5rGOBWdZtJ/vv/g4ZaU3MZ/Ta37nww9FAtGPHjiGha7LpUPsYgdhkwQ5Y3/veo0FhIrTAhXT7BPwdEFojHYOu+4Nr3O2f+tRAgLTCxIYNGwaCNkJT+7ak33bXBrGaMe62b7OyGChHpL/6t19jXGvQVNikQdiBokJrBCuinnzSiRO9HkCYeG3Li4oRIYNbvyg6gqHX2S358jv/7I8KF7LtXnNX2HwBfqfXLTmbNx/LArPdeNBI6SEfjIlhdoFy4x/ePMg7oseJrrr8Mnfrbbezw6YRy7VbSCw/hP291FDzDehRDhFkn3nm2aGJXbstoXQlEFsYxXZY6c6/m2643n3uzs8PCRZQ7SYB+m037SK1Ytxt3zYpMfDxJ55kXGvQVNikQdheUbEXUPb37/3V9wwd05209UD2woQ/8BQpUZJExE9EWeae6ixynSa/VOHCJinzzzHapDIibMgC89rrb+jnnZC3nipolCXiLKsff+8egdUIE+JbcsxHkreKn6rQ9eW/+ut+3om3vOXUfk6Tbdueq+zD3SNEjUIEUhY4kgzVJgmT+PG+3/j1Qf4ROUakeU78JIpQb55AygRQBSZNfilJrkRokgS6spPKjwmaLLf51lAi/XayfKBMmGDcrd+eKTFQEsMzrtVviyqCOTapxx4pwoTNLzaJ64GshYlQwCvbMaGT/82bNyd/3cBfMEgZzz3/wtD9fvIr36XtrglNrCnXyNc5Yrkq6ukWPLVuAqsVJvzt2lYlVVFLFiVlR43qbh/Pr4dAqjBhS7f5bnQxK8fYSJpYj42qPjV1Um6fa3dNyG4pHR9SkjJXrR/Xj06Afjs6w7qekCJMMO7WRX/luauJgYxr2KReAu09PVWY8Gs4SeuBbIUJ/1MrvhH9TzH6Ez+b+bzMRf2JR2iwK/oEqJ1Q+jkmSG5WRn/y/r6aHBN+ThS7sPRzTJDkcPJ8IqXGKTkm/LflKlKVJVFMKZ9rxk8g5SyvXRjZCbmfY0JiAomTx2+jUZ9Ivx2VYH33l+WYYNytj70+uWoMlPsY1+q1Czapl2/Z08tyTMjOWPszaeuBbIWJmOHLdkzIfbFOGXumf31osCt6a+J/OtQmv0SYKOvCk/f3kA+GfK7o6y7+p4JiX3khKd7k+UdRjWOiVujzxf4n1uxXXKQMeRaJdtv1j9i4EPusq//pUP2ak37qGmGiXXtWmXfEdrjQb5uzIeNuc6xjJVWNgfSP+m2GTepnXFRC6OW29Xs5RmN/Jm09gDDhWd+f2IuxH3/8CfeeX/nl/pWxnBF6NEOuuf+BB92557zfyWQwdH3sm7P+8Q5dHEiOAP28n3VIzSmw5crL+Qxku3FirKXHxDH/CFBsm6k/oQ0lU5UKF315ZqwN4mGNEfCT88YmECFRy/pdVfG1sQZmWFAo/5AmMLUTEH9ikrJTL0OcnWwy/baTZulXinG3fdukxkDGteZshU2aY+2X5B/9T91xL2vSSVgPIEyUCBPqAJJARH/8hJN24JJrJO+DJiKTf4cSVPpHSfwvd8h9sYWn/eoHCeraCw51lVy0a8faPuQzsQWln0xVE7HW1Qae2x4BXeRIolP5kWSI/ta+kI/5MSl0X3utyrtkm9gtlIA5JkBZX6iauDlv4s23nn7bPPPUEhl3U0nVd11ZDFQRSf5rX7owrmGT+gi092R/bRpaC07qegBhoj2/omQIQAACEIAABCAAAQhAAAIQgED2BBAmsncBAEAAAhCAAAQgAAEIQAACEIAABNojgDDRHntKhgAEIAABCEAAAhCAAAQgAAEIZE8AYSJ7FwAABCAAAQhAAAIQgAAEIAABCECgPQIIE+2xp2QIQAACEIAABCAAAQhAAAIQgED2BBAmsncBAEAAAhCAAAQgAAEIQAACEIAABNojgDDRHntKhgAEIAABCEAAAhCAAAQgAAEIZE8AYSJ7FwAABCAAAQhAAAIQgAAEIAABCECgPQIIE+2xp2QIQAACEIAABCAAAQhAAAIQgED2BBAmsncBAEAAAhCAAAQgAAEIQAACEIAABNojgDDRHntKhgAEIAABCEAAAhCAAAQgAAEIZE+gMWHi6R+94JaXl7MHnguA2V7PLe3bl0tzaeeUE5idnXVLS0tT3srJbF6v13P7iDWTabwx1po+OkaYLT1qdrbnlpaYNzSFn9jZFGnKmWQCrGeatV5vtufecPRRTrhX+ZlZrqAyyKRx5/++7Hq9mSplcC0EIAABCEAAAhCAAAQgAAEIQAACU05A5IWD1s+7+XVzlVpaSZio9GQuhgAEIAABCEAAAhCAAAQgAAEIQAACJQQQJnARCEAAAhCAAAQgAAEIQAACEIAABFojgDDRGnoKhgAEIAABCEAAAhCAAAQgAAEIQABhAh+AAAQgAAEIQAACEIAABCAAAQhAoDUCCBOtoadgCEAAAhCAAAQgAAEIQAACEIAABBAm8AEIQAACEIAABCAAAQhAAAIQgAAEWiOAMNEaegqGAAQgAAEIQAACEIAABCAAAQhAAGECH4AABCAAAQhAAAIQgAAEIAABCECgNQIIE62hp2AIQAACEIAABCAAAQhAAAIQgAAEECbwAQhAAAIQgAAEIAABCEAAAhCAAARaI4Aw0Rp6CoYABCAAAQhAAAIQgAAEIAABCEAAYQIfgAAEIAABCEAAAhCAAAQgAAEIQKA1AggTraGnYAhAAAIQgAAEIAABCEAAAhCAAAQQJvABCEAAAhCAAAQgAAEIQAACEIAABFojgDDRGnoKhgAEIAABCEAAAhCAAAQgAAEIQABhAh+AAAQgAAEIQAACEIAABCAAAQhAoDUC/w90AzDolUgZRQAAAABJRU5ErkJggg==', `mes` = '8', `anio` = '2024', `tipo` = '6', `semana` = '0'
WHERE `id` = '280' 
 Execution Time:0.085314989089966

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '10'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.21247792243958

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAt4AAAIUCAYAAAAt5yuNAAAAAXNSR0IArs4c6QAAIABJREFUeF7t3X2spdVdL/BFr3oHYtJOatqLhImMpFTjtdSZAKGaNkpfUESnDZagNg3STksshBSmlDIQmCkzDh0bMBnDS0isiigpE1tjbGuMRksKmbG0JTe2wamhIdRqO3f+sDNXveVmbe86WWedZ7+cM+esvfbzfM4/M+ecvZ+11ue39t7fZ+21n3PGSy+99FLwRYAAAQIECBAgQIDAhgqcIXhvqK+DEyBAgAABAgQIEBgJCN4mAgECBAgQIECAAIEKAoJ3BWRNECBAgAABAgQIEBC8zQECBAgQIECAAAECFQQE7wrImiBAgAABAgQIECAgeJsDBAgQIECAAAECBCoICN4VkDVBgAABAgQIECBAQPA2BwgQIECAAAECBAhUEBC8KyBrggABAgQIECBAgIDgbQ4QIECAAAECBAgQqCAgeFdA1gQBAgQIECBAgAABwdscIECAAAECBAgQIFBBQPCugKwJAgQIECBAgAABAoK3OUCAAAECBAgQIECggoDgXQFZEwQIECBAgAABAgQEb3OAAAECBAgQIECAQAUBwbsCsiYIECBAgAABAgQICN7mAAECBAgQIECAAIEKAoJ3BWRNECBAgAABAgQIEBC8zQECBAgQIECAAAECFQQE7wrImiBAgAABAgQIECAgeJsDBAgQIECAAAECBCoICN4VkDVBgAABAgQIECBAQPA2BwgQIECAAAECBAhUEBC8KyBrggABAgQIECBAgIDgbQ4QIECAAAECBAgQqCAgeFdA1gQBAgQIECBAgAABwdscIECAAAECBAgQIFBBQPCugKwJAgQIECBAgAABAoK3OUCAAAECBAgQIECggoDgXQFZEwQIECBAgAABAgQEb3OAAAECBAgQIECAQAUBwbsCsiYIECBAgAABAgQICN7mAAECBAgQIECAAIEKAoJ3BWRNECBAgAABAgQIEBC8zQECBAgQIECAAAECFQQE7wrImiBAgAABAgQIECAgeJsDBAgQIECAAAECBCoICN4VkDVBgAABAgQIECBAQPA2BwgQIECAAAECBAhUEBC8KyBrggABAgQIECBAgIDgbQ4QIECAAAECBAgQqCAgeFdA1gQBAgQIECBAgAABwdscIECAAAECBAgQIFBBQPCugKwJAgQIECBAgAABAoK3OUCAAAECBAgQIECggoDgXQFZEwQIECBAgAABAgQEb3OAAAECBAgQIECAQAUBwbsCsiYIECBAgAABAgQICN7mAAECBAgQIECAAIEKAoJ3BWRNECBAgAABAgQIEBC8zQECBAgQIECAAAECFQQE7wrImiBAgAABAgQIECAgeJsDBAgQIECAAAECBCoICN4VkDVBgAABAgQIECBAQPA2BwgQIECAAAECBAhUEBC8KyBrggABAgQIECBAgIDgbQ4QIECAAAECBAgQqCAgeFdA1gQBAgQIECBAgAABwdscIECAAAECBAgQIFBBQPCugKwJAgQIECBAgAABAoK3OUCAAAECBAgQIECggoDgXQFZEwQIECBAgAABAgQEb3OAAAECBAgQIECAQAUBwbsCsiYIECBAgAABAgQICN7mAAECBAgQIECAAIEKAoJ3BWRNECBAgAABAgQIEBC8zQECBAgQIECAAAECFQQE7wrImiBAgAABAgQIECAgeJsDBAgQIECAAAECBCoICN4VkDVBgAABAgQIECBAQPA2BwgQIECAAAECBAhUEBC8KyBrggABAgQIECBAgIDgbQ4QIECAAAECBAgQqCAgeFdA1gQBAgQIECBAgAABwdscIECAAAECBAgQIFBBQPCugKwJAgQIECBAgAABAoK3OUCAAAECBAgQIECggoDgXQF5aE18+9vfDr/6q78aPvOZz6wY+s6dO8PHP/7x8Pzzz4d3vvOd4Utf+tKy27zuda8Lf/zHfxz+5E/+JHzyk58c/f+CCy4Y3ebkyZPhpptuCg888MDSfe6+++6we/fuZcf4/Oc/H376p3969LO/+7u/C294wxtW9OOrX/3qivbz2+7Zs2di+2kcZ555Zmd5//AP/zD82q/92orf5W3M4lQev6tfqZHk80//9E8htv8P//APSw65U97uW9/61tFt/+Iv/qKzv3/wB38wqmV57H/913/trF/sS2zr5ptvXlGr1M/U5itf+crRj2apV36b1EZZ93T8aHTHHXeMnXvRdFx9uuZMum2am2k+xtum342bZ3m9fuiHfmjq4yL2LZ/nXfNslrmVBp/Xuuux0vW46hpn1/HKOpSPz/I46fc/8zM/E7Zv375s/pTjzB+f5Xwp69tln9r6whe+sOw5ZGjPxcZLgEB7AoJ3ezVZ+B6lF/sf+ZEfGYXsrnCaXljf8Y53rAjOEaAMmF3HTMe45JJLlrWTvzBPCuYpVOYBKv2sK+DmYWBSOBkXyFJgSm3M4lROhrUG7zzY5CG2DN7lyUcMsPFnP/VTPzUK0inUp+A9rn5dkzjVpazJtHqVc6V0nMUojTnZTwvM5clMOtnL58xqg3ce2MvAmx932jzr6vs4k65apxOe/EQlneSmk4M0tkm3jXMgnuCOM419uvfee5eCbxzXDTfcEA4cOBB+93d/d2kunXXWWaO5Fb/i88V3v/vd0QlKPGFOJ3Dpd3//93+/rM00vjJ8p59Pe5wu/JOtARAgsHACgvfClaz9Ds8SKFcbvGM4iy+m8cW8DAO5SGp78+bNox8fP3582X3S7+OL+rgV067gn4esGBxjIC1DWN6PrnBUrhrH28eAMekEZZZQWYbErhXvPICkQPTqV786nHHGGctWvPMAk9coBaC1Bu8UhMqVzWn1imMrQ/O0R0DXyUk5J5944onRCv+4lerURn5yF8de1mpagJ90opTuW56I5D/vmmezzK30GEntv+UtbxmF4HK85Zyc9Ngq3ac9hsu6xe9jf7oew7lTCvSpr2WAz/vR1Yd85V3wnvZo8XsCBGoLCN61xQfQ3noH77QKN0tAzV/sI3UZrmYNcWVgygPK/fffP1q5m9SfcYEsDxGrGVeaNmtZ8b7lllvCZz/72RD/ffvb3z5aXTxx4sSyE5O01WQjgncKR7HBfOtQGaq76pXfJv5/0snOJKO1Bu+8jn/1V3+1YvvRWoN3moflNopZ5tkscyuurudjft/73hfe/e53h/Idijykln2Z9lQ1brU5r0N+4hD7/bd/+7cr3gVL/fzmN785mh9HjhxZtlI+qZ2uk5f8xPKf//mfbTWZVki/J0CgqoDgXZV7GI1N2rucgtO01bI8YEa1uB98lm0N0+43LSiNC29lfycF4HiMSeEonQy89rWvHbvnd1zAXEvw/u3f/u3Rfvu4yn/99deP2owBPL5tn1awu4J32gIyaatJuUc/jj0P73mw6xrTtHrF403bO1w+qrqMxm01Ke+bh89xe+a7tijNssc7bTXJHx/l/WaZZ7PMrVjrfMzphCvVO1/ZXs3++eQ17XFUbvXYsmXL6IQv7u+O8y//SvMsvRvysY99bNkJzriT5XxLTnJMtr/+678ejh07tuJEaRjPwEZJgEDLAoJ3y9VZ0L6tZsW7DG7pbfdZAlnJU7Ybf5/vS45hY1pgGBe8y/tNWzmfJRyl4D3LSv6k1dz0u0lBMYaQ2Of3v//94Zd/+ZdHK+BxX28ZvEvTFGjGfbhy2snQuO0UsZ1Z6tUV0NLPJoXdrg9XriYwxzbKENw1r6fNp66TgBQ0u05EZplns8ytGLzLtif1tfyw8bQPD3fVL32WIw/y6fGc7OL2rvzDzvn+/lTPst9dj7W8v3lf4xh///d/f/Q4P3TokOC9oK8huk2gzwKCd5+rO6exrSZ4z/Lhylm3ZHS9QOertuUqYLnyVoa89IGztFqXrxaO+2DntBXBeWw1iQEvfsU9vmlf98MPPxz27t07ccU791hL8B63nSIdd5Z6lVN42hU/4u3HXUkkD+rTAnM8TnmbritlTDvOuPDbFWy79lt3zbNZtpp0PWaS97irm8Qxz3KFmXGPk7iiP+5qJOmDlXGbVlr5T+OIx8v7VO7pLrea5O8Y5Hu4U9txS1V8bE97V2pOT4+aJUBg4AKC98AnwEYMf72Dd3yhnuXDlfnqWTmucuVtNR+ujMfquvRh/Pm4D2/N8gG4eP8aH66MwTu/fFsMfR/96EfDRz7ykQ0L3pO2U6TazFKvrvk5bX6NWzHNt5FMC8xdl67M+zLr1VG63rmJxyn3usefdV3iMrVZfji2/OzCpEtJlobT9nJPsymDd/rQc5rPcVtT2Ua+Eh3feepaFS9PyLo+XJlOguNVZsrHXh7kyzFP+xDtRjwPOiYBAgS6BARv82LdBaYFozxkzLLiXX5QLF2iMF8NjG9h/8Zv/MZoLPEFOO1hTX3Jf9610ppetLsuJxg/7BWDzrhLyc2yZSC2X7Y7i1NZnLXs8Y79e9vb3ra0nzx+X+757drjnbe92hXvaVtxuuoS2+v6efmuxWo+H5BWV9Mx0gnYtHA57h2N8ufTro6S1yvNo3EhsJyDyb/8eVffS+/SrDzZSX3oGue4OTZtDqR+dZ2MxmPGr/g4nfauRX7loXGXE4zHmhamrXiv+1O7AxIgsA4Cgvc6IDrEcoFJH65MK2HTrgPd9aI56Q/oTHobvSuEzPoHdH7v935vdM3hrj/EMWm7ybjVt1n/gM64Vclxq8RxFfuee+4Jt91229IqdvoDOl0nE+X2mbUG764PV8a+/Mqv/Er4uZ/7uc6HRgxmH/rQh8I111yzbIvBuHAYf16Oe9J2ia65U67Ax21DXX/gKLaVtuZ0nWyV201SmC4HmuqX7zMed9t438suuyycc8454ZlnnlmxGj4u7Jdtlh8wLE9C85O/3K98LKzlD+hMeofgkUceCU899dTSByvHrex3bRuJ8yt/LIyb/13zQfD2ykSAQIsCgneLVdEnAgQIECBAgACB3gkI3r0rqQERIECAAAECBAi0KCB4t1gVfSJAgAABAgQIEOidgODdu5IaEAECBAgQIECAQIsCgneLVdEnAgQIECBAgACB3gkI3r0rqQERIECAAAECBAi0KCB4t1gVfSJAgAABAgQIEOidgODdu5IaEAECBAgQIECAQIsCgneLVdEnAgQIECBAgACB3gkI3r0rqQERIECAAAECBAi0KCB4t1gVfSJAgAABAgQIEOidgODdu5IaEAECBAgQIECAQIsCgneLVdEnAgQIECBAgACB3gkI3r0rqQERIECAAAECBAi0KCB4t1gVfSJAgAABAgQIEOidgODdu5IaEAECBAgQIECAQIsCgneLVdEnAgQIECBAgACB3gkI3r0rqQERIECAAAECBAi0KCB4t1iV0+zTqVOnwp49e8KOHTvC9u3bl452+PDhcOjQodH3119//ej36evYsWNh165d4cSJE+Giiy4Ku3fvDps2bTrNnrg7AQIECBAgQIBAEhC8ezYXUuh++umnw759+5aCdwzWe/fuDbfffvtoxOn/W7duDek+MaRffvnlo9Ae/58H854xGQ4BAgQIECBAoLqA4F2dfOMaTKvWF1xwQXjxxRdHq9ppxTuudh85cmRpJfu+++4LW7ZsGYXrPJTHIB5vF29v1XvjauXIBAgQIECAwPAEBO8e1TyG7fgVt4h88IMfXBa8Y9COXzfeeOPo3/z7GLTjFpSDBw+GzZs3j4J3/n2PiAyFAAECBAgQIDA3AcF7bvQb1/Dx48c7g3da4Y4txxXt559/fhTEyxXuuAJ+//33hzvvvHMUxBfp66/DA4vUXX0lQIAAgRDCm8JODgQGISB497DM8w7eR48enZvqZ153U/iP7/vu3NrXMAECBAisTuD7//Os8NYvfXx1d1rHW2/btm3VR7v1C/9n1fdZyx32X/Lf13I392lYQPBuuDhr7dq44B2P1/etJla81zpr3I8AAQLzE1i0FW/Be35zZdFbFrwXvYId/e8K3vnWkniX8sOV+dYSH67s4aQwJAIECBBYNwHBe90oB3cgwbuHJe8K3i4n2MNCGxIBAgQIzEVA8J4Ley8aFbx7Ucblg+gK3vEW/oBOD4ttSAQIECBQXUDwrk7emwYF796U0kAIECBAgACBGgKCdw3lfrYhePezrkZFgAABAgQIbJCA4L1BsAM4rOA9gCIbIgECBAgQILB+AoL3+lkO7UiC99AqbrwECBAgQIDAaQkI3qfFN+g7C96DLr/BEyBAgAABAqsVELxXK+b2SUDwNhcIECBAgAABAqsQELxXgeWmywQEbxOCAAECBAgQILAKAcF7FVhuKnibAwQIECBAgACBtQoI3muVcz8r3uYAAQIECBAgQIAAgQoCgncFZE0QIECAAAECBAgQELzNAQIECBAgQIAAAQIVBATvCsiaIECAAAECBAgQICB4mwMECBAgQIAAAQIEKggI3hWQNUGAAAECBAgQIEBA8DYHCBAgQIAAAQIECFQQELwrIGuCAAECBAgQIECAgOBtDhAgQIAAAQIECBCoICB4V0DWBAECBAgQIEBgPQVOnjwZbrvttvCpT31qdNhzzjknPPLII+H8888ffX/8+PFw3XXXhWeeeWZZszt37gy7du1a0ZV0+/i7iy++OBw4cCA88MADK26X2nniiSc6fx/vENv4wAc+MOrfpZdeGl7/+teHa6+9dvSzq666atkxH3/88fDYY4+Fhx9+OGzevHn0u/izW2+9del2+/fvX3G/9bSseSzBu6a2tggQIECAAAECpymQQvLVV1+9FEife+65ZeG2DNJ5GM/vl7rSFbzj77pCetn9GNLL26YTgxi8r7jiilEIP/vss5cdL90m/3k81osvvhjuueeecOaZZ4Z0m3j89LPT5Jvr3QXvufJrnAABAgQIECCwOoGuoBuP8NRTT41WquPqcfyKK95pBTu1MO6+Gxm84yp318p2PFmI/Yt9iiv1ef/T6nd+wlCOZXVqbdxa8G6jDnpBgAABAgQIEJgq0LWSne6U/+41r3nNiuA9633TVpNyFXtc56ateMfgnVbk77333tFWlvgVw/iTTz65tJI97qQg3nbS76aiNXQDwbuhYugKAQIECBAg0L7AX4eVe59n6fUZ4YzRzV4KL43+Td+XP0u/f1PYueKw5SpxfoN8e8dll13Wucd73H7pWfd4d+0RnyV4l9tK8r7GYF5+Xw68DOmzeLd4G8G7xaroEwECBAgQINCswF1he5W+3RmOnHbwzrdndO2pTg1s9FaT2E4enl944YVl20wE7ypTSiMECBAgQIAAgcUSWMuKd7nana94xxXu+Pt8JTz+v2vFe9btIl1bTWKbs+6jXs3WjllWvGPb+Wr9F7/4xWXbTOLvbTVZrMeB3hIgQIAAAQIEei9wOh+uzIP3Qw89FM4777zRlVHKLSwbEbzTqvaFF144usxhvOJJfnnBWU8KFrnAtposcvX0nQABAgQIEBicwFovJ1huNcm3fnz5y19euiJKvKLIRgTvWKh0je7yuuOpiC4nOLjpbMAECBAgQIAAgbYF1voHdK688soV18hOf4Tn0UcfXbriyLg/oBNV8tvF72fdahJvm65usm3btrHX5fYHdNqee3pHgAABAgQIECBAoHkBW02aL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlTRGAgQIECAAAECBJoXELybL5EOEiBAgAABAgQI9EFA8O5DFY2BAAECBAgQIECgeQHBu/kS6SABAgQIECBAgEAfBATvPlRxyhhOnToV9uzZE55++ullt3z5y18eDhw4ELZu3RoOHz4cDh06tPT7c889Nxw8eDBs3rx5AEKGSIAAAQIECBDYeAHBe+ONm2zhvvvuG/XrxhtvHP0bv9+yZUvYsWNHk/3VKQIECBAgQIDAogsI3otewTX0/8iRI6PV7bSinVbEY+jevn37Go7oLgQIECBAgAABAtMEBO9pQj37fQrZMWCn1e3jx4+Hu+66K9xwww2jbSe+CBAgQIAAAQIE1l9A8F5/06aPWK52x84eO3Ys7Nq1K5w4cWKp7/v27bP63XQldY4AAQIECBBYNAHBe9Eqdpr9Lfd2x8PFML5///6lD1qW36+2yaNHj672Lm5PgAABAgTmIrBt27a5tKvRYQoI3gOq+6x7ubu2owyIyVAJECBAgAABAhsiIHhvCGubB41bSvbu3Rtuv/32iXu5Zw3obY5SrwgQIECAAAECbQoI3m3WZUN6FbeQxOt17969O2zatGmpjfiz+Lv08/j9pz/9adfx3pAqOCgBAgQIECAwVAHBe0CVLwN2PvT8D+jkf1hnQDyGSoAAAQIECBDYUAHBe0N5HZwAAQIECBAgQIDAfwkI3mYCAQIECBAgQIAAgQoCgncFZE0QIECAAAECBAgQELzNAQIECBAgQIAAAQIVBATvCsiaIECAAAECBAgQICB4mwMECBAgQIAAAQIEKggI3hWQNUGAAAECBAgQIEBA8DYHCBAgQIAAAQIECFQQELwrIGuCAAECBAgQIECAgOBtDhAgQIAAAQIECBCoICB4V0DWBAECBAgQIECAAAHB2xwgQIAAAQIECBAgUEFA8K6ArAkCBAgQIECAAAECgrc5QIAAAQIECBAgQKCCgOBdAVkTBAgQIECAAAECBARvc4AAAQIECBAgQIBABQHBuwKyJggQIECAAAECBAgI3uYAAQIECBAgQIAAgQoCgncFZE0QIECAAAECBAgQELzNAQIECBAgQIAAAQIVBATvCsiaIECAAAECBAgQICB4mwMECBAgQIAAAQIEKggI3hWQNUGAAAECBAgQIEBA8DYHCBAgQIAAAQIECFQQELwrIGuCAAECBAgQIECAgOBtDhAgQIAAAQIECBCoICB4V0DWBAECBAgQIECAAAHB2xwgQIAAAQIECBAgUEFA8K6ArAkCBAgQIECAAAECgrc5QIAAAQIECBAgQKCCgOBdAVkTBAgQIECAAAECBARvc4AAAQIECBAgQIBABQHBuwKyJggQIECAAAECBAgI3uYAAQIECBAgQIAAgQoCgncFZE0QIECAAAECBAgQELzNAQIECBAgQIAAAQIVBATvCsiaIECAAAECBAgQICB4mwMECBAgQIAAAQIEKggI3hWQNUGAAAECBAgQIEBA8DYHCBAgQIAAAQIECFQQELwrIGuCAAECBAgQIECAgOBtDhAgQIAAAQIECBCoICB4V0DWBAECBAgQIECAAAHB2xwgQIAAAQIECBAgUEFA8K6ArAkCBAgQIECAAAECgrc5QIAAAQIECBAgQKCCgOBdAVkTBAgQIECAAAECBARvc4AAAQIECBAgQIBABQHBuwKyJggQIECAAAECBAgI3uYAAQIECBAgQIAAgQoCgncFZE0QIECAAAECBAgQELzNAQIECBAgQIAAAQIVBATvCsiaIECAAAECBAgQICB4mwMECBAgQIAAAQIEKggI3hWQNUGAAAECBAgQIEBA8DYHCBAgQIAAAQIECFQQELwrIGuCAAECBAgQIECAgOBtDhAgQIAAAQIECBCoICB4V0DWBAECBAgQIECAAAHB2xwgQIAAAQIECBAgUEFA8K6ArAkCBAgQIECAAAECgrc5QIAAAQIECBAgQKCCgOBdAVkTBAgQIECAAAECBARvc4AAAQIECBAgQIBABQHBuwKyJggQIECAAAECBAgI3uYAAQIECBAgQIAAgQoCgncFZE0QIECAAAECBAgQELwHMgdOnToV9uzZE55++umlEV9xxRXhxhtvHH1/7NixsGvXrnDixIlw0UUXhd27d4dNmzYNRMcwCRAgQIAAAQIbLyB4b7xxEy0cP3483HXXXeGGG24IW7duXdanFMq3b98eLr/88lFAj//fsWNHE33XCQIECBAgQIBAHwQE7z5UcYYxxBXt+++/P9x5551h8+bNy+4Rf7d3795w++23j0L5kSNHwuHDh616z+DqJgQIECBAgACBWQUE71mlFvx2k8J0/N2hQ4fCwYMHR6G8/H7Bh677BAgQIECAAIEmBATvJsqw8Z2IK9gxXKevc889d1nQzle4J62Ob3xPtUCAAAECBAgQ6KeA4N3Puq4Y1X333Re+9a1vLW0fyb9/9tlnl20tOd3gffTo0YGoGiYBAgQILLrAtm3bFn0I+r9AAoL3AhVrPbua7+v+zne+Y6vJeuI6FgECBAgQIECgQ0DwHui0yFe14xVP8g9e+nDlQCeFYRMgQID/3AvHAAAgAElEQVQAAQIbKiB4byhvGwfPLxcYLxGYvn/Vq141uo63ywm2USe9IECAAAECBPotIHj3u75Loyv/gE75R3L8AZ2BTATDJECAAAECBOYmIHjPjV7DBAgQIECAAAECQxIQvIdUbWMlQIAAAQIECBCYm4DgPTd6DRMgQIAAAQIECAxJQPAeUrWNlQABAgQIECBAYG4Cgvfc6DVMgAABAgQIECAwJAHBe0jVNlYCBAgQIECAAIG5CQjec6PXMAECBAgQIECAwJAEBO8hVdtYCRAgQIAAAQIE5iYgeM+NXsMECBAgQIAAAQJDEhC8h1RtYyVAgAABAgQIEJibgOA9N3oNEyBAgAABAgQIDElA8B5StY2VAAECBAgQIEBgbgKC99zoNUyAAAECBAgQIDAkAcF7SNU2VgIECBAgQIAAgbkJCN5zo9cwAQIECBAgQIDAkAQE7yFV21gJECBAgAABAgTmJiB4z41ewwQIECBAgAABAkMSELyHVG1jJUCAAAECBAgQmJuA4D03eg0TIECAAAECBAgMSUDwHlK1jZUAAQIECBAgQGBuAoL33Og1TIAAAQIECBAgMCQBwXtI1TZWAgQIECBAgACBuQkI3nOj1zABAgQIECBAgMCQBATvIVXbWAkQIECAAAECBOYmIHjPjV7DBAgQIECAAAECQxIQvIdUbWMlQIAAAQIECBCYm4DgPTd6DRMgQIAAAQIECAxJQPAeUrWNlQABAgQIECBAYG4Cgvfc6DVMgAABAgQIECAwJAHBe0jVNlYCBAgQIECAAIG5CQjec6PXMAECBAgQIECAwJAEBO8hVdtYCRAgQIAAAQIE5iYgeM+NXsMECBAgQIAAAQJDEhC8h1RtYyVAgAABAgQIEJibgOA9N3oNEyBAgAABAgQIDElA8B5StY2VAAECBAgQIEBgbgKC99zoNUyAAAECBAgQIDAkAcF7SNU2VgIECBAgQIAAgbkJCN5zo9cwAQIECBAgQIDAkAQE7yFV21gJECBAgAABAgTmJiB4z41ewwQIECBAgAABAkMSELyHVG1jJUCAAAECBAgQmJuA4D03eg0TIECAAAECBAgMSUDwHlK1jZUAAQIECBAgQGBuAoL33Og1TIAAAQIECBAgMCQBwXtI1TZWAgQIECBAgACBuQkI3nOj1zABAgQIECBAgMCQBATvIVXbWAkQIECAAAECBOYmIHjPjV7DBAgQIECAAAECQxIQvIdUbWMlQIAAAQIECBCYm4DgPTd6DRMgQIAAAQIECAxJQPAeUrWNlQABAgQIECBAYG4Cgvfc6DVMgAABAgQIECAwJAHBe0jVNlYCBAgQIECAAIG5CQjec6PXMAECBAgQIECAwJAEBO8hVdtYCRAgQIAAAQIE5iYgeM+NXsMECBAgQIAAAQJDEhC8h1RtYyVAgAABAgQIEJibgOA9N3oNEyBAgAABAgQIDElA8B5StY2VAAECBAgQIEBgbgKC99zoNUyAAAECBAgQIDAkAcF7SNU2VgIECBAgQIAAgbkJCN5zo9cwAQIECBAgQIDAkAQE7yFV21gJECBAgAABAgTmJiB4z41ewwQIECBAgAABAkMSELyHVG1jJUCAAAECBAgQmJuA4D03eg0TIECAAAECBAgMSUDwHki1jx8/Hj74wQ+Gb3zjG6MRX3TRRWH37t1h06ZNo+8PHz4cDh06tKRx7rnnhoMHD4bNmzcPRMgwCRAgQIAAAQIbKyB4b6xvE0c/depU2LNnT9i+fXvYsWNHSN+/6lWvCjfeeOOoj/fdd1/YsmXL6Pe+CBAgQIAAAQIE1l9A8F5/04U4YlzhPnLkyGjVO37FYB5DdwznvggQIECAAAECBNZfQPBef9OFOGIevE+ePBnuuuuucMMNN4StW7cuRP91kgABAgQIECCwaAKC96JVbB36m/Z7/+Iv/uJolfvYsWNh165d4cSJE0tH37dvn9XvdbB2CAIECBAgQIBAEhC8BzYX0v7uOOz04cq45WT//v3hwIEDoxXv8vvVEh09enS1d3F7AgQIECAwF4Ft27bNpV2NDlNA8B5Q3btCd9fwyw9jDojIUAkQIECAAAECGyYgeG8YbVsH7rqSybgeptv6sGVbNdQbAgQIECBAYLEFBO/Frt/MvY+XC/zWt7617Nrd6c75By3jdb3j95/+9Kddx3tmXTckQIAAAQIECEwXELynGy38Lco/npMGlP+RnPwP6Lz85S9f2u+98IM3AAIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0A0CBAgQIECAAIF+Cwje/a6v0REgQIAAAQIECDQiIHg3UgjdIECAAAECBAgQ6LeA4N3v+hodAQIECBAgQIBAIwKCdyOF0I3TE/jEV/8j/K/j3xsd5Mc3vyy864LvP70DLsC9jVmdF2CarrmL5rf5vebJ444EGhYQvBsujq7NJvD5b/7f8Llv/Gd487nfN7pD+v8b/sd/m+0AC3grY1bnBZy2M3fZ/Da/Z54sbkhgwQQE7wUrmO6uFIgrYy9+96Xw7td+f3j1mWeE+7/y7+EVP3BGr1e9jVmd+/xcYH6b332e38Y2bAHBe9j178XoY9COXzf8zx8Y/Vt+34tBFoMwZnXu47xOYzK/ze8+z29jG7aA4D3s+vdi9OUKd1wt+9///tJSEO/FIDuCd76qb8x9rPJ/ha+h1TmdPA9t3EOs9RDH3M9nKqNajYDgvRott21SYIhP3sYcgpONJh+O69Ip89v8XpeJ5CAEGhQQvBssii6tTsDb0t6WXt2MWZxbD3Fud20Xs31scebsano61Pm9GiO37Z+A4N2/mg5uROXK51A+XJlvpzHmfk77Ic7tWMkhjtuYV26t6uej2qiGLiB4D30G9GD8Lj3m0mM9mMadQxji3I4QQxy3MQ/jeayvz1XGNbuA4D27lVs2LOCPbfhjGw1Pz9Pq2hDndlr19kex/CGw03rwuDOBBgUE7waLoksECBAgQIAAAQL9ExC8+1dTIyJAgAABAgQIEGhQQPBusCi6tHaBBx98MLz3ve9d+wEW8J7GvIBFW0OXh1jnyDTEcRvzGh4g7kJgQQQE7wUplG7OJuAFazanRb+VOi96BWfvv1rPbrXItxxinRe5Xvq+dgHBe+127tmgwBCfvI25wYm4AV0aYp2teG/ARGr0kEOd342WQ7c2UEDw3kBch64vMMQnb2OuP8/m0eIQ6yx4z2OmzafNoc7v+WhrdZ4Cgvc89bW97gJDfPI25nWfRk0ecIh1FrybnIob0qmhzu8NwXTQpgUE76bLo3OrFRjik7cxr3aWLObth1hnwXsx5+paej3U+b0WK/dZbAHBe7Hrp/eFwBCfvI15GA+DIdZZ8B7G3B5qnYdTXSPNBQRv82EkcOzYsbBr165w4sSJcNFFF4Xdu3eHTZs2LZzOEMOJMS/cNF1Th4dY56EGsiHWeohjXtMTgTstvIDgvfAlPP0BnDp1KuzZsyds3749XH755Uv/37Fjx+kfvPIRhvjkbcyVJ9mcmhtinQXvOU22OTQ71Pk9B2pNzllA8J5zAVpoPq527927N9x+++1h69at4ciRI+Hw4cMLueo9xCdvY27hUbTxfRhinQXvjZ9XrbQw1Pndir9+1BMQvOtZN9tSDNqHDh0KBw8eDJs3bx4F7/z7WTv+wFdDeOhrs956Y2733hcfDA+eXe8vV74UQjhjY4Yy81GNeWaqNd9wiHWOWEMctzGv+WFyWnes/TxWdvY9rwlh5wWnNQR3JjCTgOA9E1O/b1SucMcV8Pvvvz/ceeedoyA+65fgPavU+t6u9guWYLK+9Zv1aLXrLHjPWpn1v13tWg/xMS14r/+8dcTZBATv2Zx6fat1D953be+1l8ERIECAQI8E7jwSrHj3qJ6ND0XwbrxANbrXp60mNbzyNlpYKTLmjRcYYp1bWfHe+Ooub2GItR7imK14135kaS8JCN7mwuhSgvnWkrV+uDJuNWnh64z/v+n6pfhqEvdgnxFC+n/6fhQq1uH3R7+9csTbXllfwZiHUedyZqW65/O5a/Z1zY/yPuVt4u/N7//S9Jiu85xW83ms67XBHu86dR56K4L30GdACKFPlxNUTgIECBAgQIBAqwKCd6uVqdyvvvwBncpsmiNAgAABAgQIzCwgeM9M5YYECBAgQIAAAQIE1i4geK/dzj0JECBAgAABAgQIzCwgeM9M5YYECBAgQIAAAQIE1i4geK/dzj0JECBAgAABAgQIzCwgeM9M5YYECBAgQIAAAQIE1i4geK/dzj0bFDhw4EA477zzwlVXXdVg79avS8ePHw/XXXddeOaZZ0YHvfLKK8M999wTzjzzzPVrpMEjPf744+HWW28d9ezCCy8MDz/8cNi8eXODPd2YLsX5Hb927dq1MQ00ctSnnnoqXHPNNUu9Oeecc8IjjzwSzj///EZ6uDHdyOd33x/T5XNYEh3i43pjZpOjtiogeLdaGf1atUAMJQ888EDYv39/r4P3yZMnw2233RYuvfTS0TjT92effXavA1kMY7HGKWzH/7/44ouDOOGID4YURnfu3NnrOsexxgD69a9/vffjzJ/kYn1vueWWpROMoZxkJYPyeW3VLwDuQGBBBATvBSmUbo4XSCsnr3jFK0Y3etvb3tbr4N0lEYPKk08+OZgQmoJoHsT7/BiJoWTfvn3h2WefDZdccknvA+lQ3rnK5+wQx5yPf4jPYX1+zjK28QKCt9mx8AIxeH/nO98JP/zDP7xsJXjhB7aKAQzxRWtIK4KxvvErrgLHrz5vNUkrn1dffXW4+OKLV/EoWNybpsWDWNehjDmv1tDHv7gzV8/XIiB4r0XNfZoUGOpblelFKwaVvu9tTyvdcf/vUPb9xvrefffd4Y477ggPPfRQ74N3197fvm8fi2O++eabwzve8Y7RVrkXXnhhMJ/biBN6iAsHTb6I6lQVAcG7CrNGaggMMXinMUffIXy4Mp9H5Z7YGnNsHm3Elf03vvGNo5XQIazyP/fcc+Haa68N995772jM5ffzqMFGt5lONrZs2TJ6HMev+DmOvn9uI45ziM/bGz2fHL9tAcG77fro3SoEhvYEPuTQPZQX7Bg6P/GJT4QPf/jDoyvWDCF4dz3k+z7urq0W5YeJV/FUuFA3Tav9cY73/ao1C1UYnd0wAcF7w2gduLbAkIL3UK5kMmkODWFfaH55udyi75eaK+ve9w8edu1rj8H7scce6/07WUM5waj9eqi9dgUE73Zro2erFBhS8B7apfTiVChfoGMojcFkSNfy7vvKb1edh7KlKN/nnLaapEuGrvKpcKFuPsRLRy5UgXR23QUE73UndcB5CQwleA/5D0/kK8BD+XBl/ngaQvBO4Tv/AzqPPvroIK72kc/vIVyvPdZ6KHN6Xq+L2m1PQPBuryZ6RIAAAQIECBAg0EMBwbuHRTUkAgQIECBAgACB9gQE7/ZqokcECBAgQIAAAQI9FBC8e1hUQyJAgAABAgQIEGhPQPBuryZ6RIAAAQIECBAg0EMBwbuHRTUkAgQIECBAgACB9gQE7/ZqokcECBAgQIAAAQI9FBC8e1hUQyJAgAABAgQIEGhPQPBuryZ6RIAAAQIECBAg0EMBwbuHRTUkAgQIECBAgACB9gQE7/ZqokcECBAgQIAAAQI9FBC8e1hUQyJAgAABAgQIEGhPQPBuryZ6RIAAAQIECBAg0EMBwbuHRTUkAgQIECBAgACB9gQE7/ZqokcECBAgQIAAAQI9FBC8e1hUQyJAgAABAgQIEGhPQPBuryZ6RIAAAQIECBAg0EMBwbuHRTUkAgQIECBAgACB9gQE7/ZqokcECBAgQIAAAQI9FBC8e1hUQyJAgAABAgQIEGhPQPBuryZ6RIAAAQIECBAg0EMBwbuHRTUkAgQIECBAgACB9gQE7/ZqokcECBAgQIAAAQI9FBC8e1hUQyJAgAABAgQIEGhPQPBuryZ6RIAAAQIECBAg0EMBwbuHRTUkAgQIECBAgACB9gQE7/ZqokcECBAgQIAAAQI9FBC8e1hUQyJAgAABAgQIEGhPQPBuryZ6RIAAAQIECBAg0EMBwbuHRTUkAgQIECBAgACB9gQE7/ZqokcECBAgQIAAAQI9FBC8e1hUQ1psgccffzz8zu/8TnjkkUfC+eefv9iD0XsCBAgQIEBgSUDwNhkINCRw/PjxcPfdd4c77rgjbN68uaGe6QoBAgQIECBwugKC9+kKuj+BdRSIwXvTpk3hzDPPXMejOhQBAgQIECDQgoDg3UIV9KH3AjFQX3fddeGZZ55ZGus555yzYjtJ3GYSv6666qql2508eTLcdttto+/vueeeFaH8qaeeCtdcc83S7a+88srO2+XIsZ1bb7116Uf79+9f1mb8xXPPPReuvfba8MILL4xu13Xc8jjpgDt37gy7du0afVseJ93mwgsvDA8//PBoZT+N8VOf+tSKuZC3O22sBw4cCA888MCKYzz66KPh4osvXvr5tLFNMs9r1DW2fFyxwXisffv2hXe9612jrUPlGPLOpn7GccTbJZ90m/izv/mbvwnvec97Vsyn/DjR/41vfOOyedHVTvxZadY1F0rQWbdDlT6lzSwW6TZlDVPfu5zG9W/csca90zTrOPPHQfk4ycdYjj/Nj/j4vvrqq0e1TfM3fwzlt4uPkfK5I3fuel5J9UvPQ/GxmT8eev8EbIAEGhIQvBsqhq70VyC+MMbw9bGPfWxpC0n8WXwBjMEnBrIyoOVh68EHHxy92N50003LtqDEF/V4/zygxe/jVwq+uWpXoEwvxvGFPwX+rsARf/bYY4+taOu8887rPFGIx4sv7rGP8X7lSUM83te//vVRP2cJBNPGOs6vtJ9lbLE/N998c/je974XPvKRjyzbax99Y6gdN7ayn7H9T3ziE+HDH/7w6KQptv/kk0+OPTlK43j22WfDhz70oWUBadKJWfJO9Z7WTgqu+VyZpQ7JJs7HX/iFXxgb4Mr5Hdsp+zRLH+Nt/vzP/zz8/M///LJ5lo5/ySWXhA984ANLJ6ST+peO9WM/9mPLHh9ljWJfZx1nWe/88VcadI03PT62b98ejhw5MpoX8SuG8UsvvXRpzPG46bGWz+l423hSn8J01+MkzYl4jD/7sz/z+ZH+vtQY2QIICN4LUCRdXHyBcQEjfzHtWnVLQTm+AP/Lv/xLeMtb3rIsBObhNV/ViqExBr3yw5nj+pG/WH/7299edkKQ65f9zV/wx91u3IlAHsjjqnp+EtJV8WljfeUrX9m5Pz4FqOiRQmY62RnX5xTEfvRHfzScddZZS+En1iN+8DWuOMeV+q6xpZrlJx5xJTOdCE06MUqBL+7zf/Ob3xxi+M5PoA4dOrRiDuTjy+s9rZ2yn/mJXteJUhnof+mXfmk0J/N3Z3LPrhOuss1Z+hhPWOPXD/7gDy6ziPf9iZ/4iVF93vSmNy01neZ4V//SfT73uc8t+xxF7Ovzzz+/bCyTjlPOm3QiFn+e3pWIdSvnbPkYTx47duwIhw8fHq16p5Xo/DilZX6SGR+veb3GnYDGvnzlK18ZdT2dBC7+M6sREFg8AcF78WqmxwsoMCl8plDWteqWB+K//Mu/DFu2bFm2whh/f8stt8y0gjXLamakzcN1ST1LWM7D1U/+5E+uWLnLA1xa8R63Kl4GuUlj7fKL989XBx966KGlVcNJY/vyl788etv/7W9/+7J3KvI24v3LVcl8bPH/MZTmq9Tjwm7el9TG+973vrB79+6lE6hx2yG6VjhnaSfV+sUXX5y6NSn1L59D8UTns5/9bLj++us7H5Fp+8O9997buSo+Sx/TmMuTkGT0sz/7s6OV7hRWJ/UvD6RPPPHEsnmQv4uRTn7SSeW0ceaDz0+Uu05IyndfJs37PLSXj8lJdl0nYrnX008/3flu2AI+reoygYUUELwXsmw6vUgCk14ky1WtfGW0fBHPb5uPP+0vnbS3swyg466YMi0M5UEhhtNym0tsJ9+SEr/vWhUvQ9m4veLlftlJY+1atcwNr7jiilFQLrdkJMt8bPHt+PhV3ievwbiV5mSQgnce6rr2+qf20x7mvI1ye0G+ZaXrBKYMyPlnCsp24vf53vpZPxuQtsmcOnVq6hV48v3N5R7tWSzywPinf/qnSycIyfTo0aPL3gHI39Ep+5efuHzta19bWiWODvke/FS/1Ywzd+zai55bp+0jkx5r+RaVGPzLd7Amzb3yJD8fdzx5T/NykZ5D9ZVAnwQE7z5V01iaFJgW0NKqb7l/t1zJnLYqnELpuAA17f4Rb1JfUyDJ+5t/QDPh5x8gG/fBynjbPKBMWmXvKmrXWMd9sDJ9WHA1Y8vDcr7ymNeoa99+6msaTwzueaibdJ88SKdwVJ7o5CdmZVv5Cuss7eSueXAc9+HK8h2TaZ758VMAL+dG+bmHstbpJCS+85BOOuK2pPj/+I5AXE1Pl96c1r/83YoYylOYLbcoTTvOpCeZce9ApWPGlfm0dWjciXSySo+PcZ8PKe3GfSA4zeX47lN5gtHkE6ZOEei5gODd8wIb3vwFxoWgrv2uaa/ouNXArqsidAWo/ENZ6feTgnd6cX7Na16zYnUt3b9cgS/DcteWh7LNru0us26BKStZrmZPCxWTgmJ+rMsuu2zZSm5evz/6oz9aWmEd55m3U4a6aR8mLPfn5sf64he/uGKr0Ti7ae2Me1R0fSAyD/izXDFm0iMuX42dpY/pRCevSdwuFB8n5RaQcSdeKcCWJ7Zp/r7+9a9ftmVm2nEmja9rFTudfMYPgKaTo0n7sMs/ntW1haq06wr26UQ5ncSN26o0/2dIPSAwLAHBe1j1Nto5CEz6QGP6UFT5tnjXfcoX63GrxGXASEMeF6rKE4Nxx82DdbzWeNe2jfJt7q4PRJYBfVLYm7SqmweLMiyPK/MsY4sfVsu3dKQwFfcZ5x/KG7dvPx9zuao57cOEXeEo3ufVr351+Md//MelSxJOq+ks7XR9AHdcIBxXo3Fzravusc/lNpr4s66r78Sf531JJzC/+Zu/GT75yU+OrmKStgPFMDtL/8p93OnEKZ7Mpg/QznKc8kQ037pUntyN2+fetdqdb9HKt4KVj88y3KfQnV+VKPnFx2h5ic5pW9Lm8BSpSQKDEhC8B1Vug52HQFcIKl/gy7fBJ10tJL+UXbnHelqIjbfPP0zXtWLaFRa63v7uugpJ3n58ge/68GHX3vWuveJ5rbpW0/O24m279j+X9Z5lbF2hKP7st37rt0ZX0UhXR+kaW9nPrg9Wdr0bkQfpchyxzx/96EfDy172smWXo0xBtrwCybQP+OUnM2XwHXeSOC7Ij9su0TUP87k26UO3qX/5SUg80YvvaMSrzLz2ta8dfZgyD9LT+heDevmOSAr2+WUbpx2nPEkovfLHVzyZjo/jMhB3vfMybotKHqBTwM/bjL+P8/Dss8+e+oHJcSdJ83hO1CaBIQsI3kOuvrFvuEC+dzZvrOuPbKT9u5Pegu/aB57/8ZxZVrPyt9LH3b7c6tL1h0+6wnIabwwC6Y+8dP2xjnx1L37gq2uvePTK9xvnH9SLv8v7Pi4AdhV42ti6Akr5Vv64rUDlHz3Jw+GkDxMm3/ihv3If97i9u3FsXSvLs7STVlTLbRVdnw+YdDLXtQ0iP4nI/wBT/Hna9jFLH/N3HlLAjP+m61ynIJ1OHuJYystnpv69//3vHwX1tB889TH6pa0dsxyn6zJ86fMG8f6T/thTmrPvfOc7w7/927+tuDRiuY0nP1b+WYlxP8/n+rg/NpRf9nDDn/w0QIBAp4DgbWIQIECAAAECBAgQqCAgeFdA1gQBAgQIECBAgAABwdscIECAAAECBAgQIFBBQPCugKwJAgQIECBAgAABAoK3OUCAAAECBAgQIECggoDgXQFZEwQIECBAgAABAgQEb3OAAAECBAgQIECAQAUBwbsCsiYIECBAgAABAgQICCGNx7wAAAJ0SURBVN7mAAECBAgQIECAAIEKAoJ3BWRNECBAgAABAgQIEBC8zQECBAgQIECAAAECFQQE7wrImiBAgAABAgQIECAgeJsDBAgQIECAAAECBCoICN4VkDVBgAABAgQIECBAQPA2BwgQIECAAAECBAhUEBC8KyBrggABAgQIECBAgIDgbQ4QIECAAAECBAgQqCAgeFdA1gQBAgQIECBAgAABwdscIECAAAECBAgQIFBBQPCugKwJAgQIECBAgAABAoK3OUCAAAECBAgQIECggoDgXQFZEwQIECBAgAABAgQEb3OAAAECBAgQIECAQAUBwbsCsiYIECBAgAABAgQICN7mAAECBAgQIECAAIEKAoJ3BWRNECBAgAABAgQIEBC8zQECBAgQIECAAAECFQQE7wrImiBAgAABAgQIECAgeJsDBAgQIECAAAECBCoICN4VkDVBgAABAgQIECBAQPA2BwgQIECAAAECBAhUEBC8KyBrggABAgQIECBAgIDgbQ4QIECAAAECBAgQqCAgeFdA1gQBAgQIECBAgAABwdscIECAAAECBAgQIFBBQPCugKwJAgQIECBAgAABAoK3OUCAAAECBAgQIECggoDgXQFZEwQIECBAgAABAgQEb3OAAAECBAgQIECAQAUBwbsCsiYIECBAgAABAgQICN7mAAECBAgQIECAAIEKAoJ3BWRNECBAgAABAgQIEBC8zQECBAgQIECAAAECFQQE7wrImiBAgAABAgQIECAgeJsDBAgQIECAAAECBCoICN4VkDVBgAABAgQIECBAQPA2BwgQIECAAAECBAhUEBC8KyBrggABAgQIECBAgMD/A8GjQjuvvoBhAAAAAElFTkSuQmCC', `mes` = '8', `anio` = '2024', `tipo` = '10', `semana` = '0'
WHERE `id` = '283' 
 Execution Time:0.19324707984924

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '7'
AND `mes` = '8'
AND `semana` = '32'
AND `anio` = '2024' 
 Execution Time:0.18298888206482

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAvYAAAIUCAYAAABxbWipAAAAAXNSR0IArs4c6QAAIABJREFUeF7t3X+spdVZL/BFr947EJN2UtNeJExkJKXeeC11JkCopo3SHyii0wZLUJsGaaclFkIKU0oZCMyUGYeODZiM4UdIrIooKRNbY2xrjEZLCpmxtCU3tsGpoSHUajt3/rAzV73lZm3vOq6zzrv3u8+Zfc5+91qf88/MOefd77uez7P2Od/33evd54yXXnrppeCDAAECBAgQIECAAIGFFjhDsF/o/hk8AQIECBAgQIAAgZGAYG8iECBAgAABAgQIEKhAQLCvoIlKIECAAAECBAgQICDYmwMECBAgQIAAAQIEKhAQ7CtoohIIECBAgAABAgQICPbmAAECBAgQIECAAIEKBAT7CpqoBAIECBAgQIAAAQKCvTlAgAABAgQIECBAoAIBwb6CJiqBAAECBAgQIECAgGBvDhAgQIAAAQIECBCoQECwr6CJSiBAgAABAgQIECAg2JsDBAgQIECAAAECBCoQEOwraKISCBAgQIAAAQIECAj25gABAgQIECBAgACBCgQE+wqaqAQCBAgQIECAAAECgr05QIAAAQIECBAgQKACAcG+giYqgQABAgQIECBAgIBgbw4QIECAAAECBAgQqEBAsK+giUogQIAAAQIECBAgINibAwQIECBAgAABAgQqEBDsK2iiEggQIECAAAECBAgI9uYAAQIECBAgQIAAgQoEBPsKmqgEAgQIECBAgAABAoK9OUCAAAECBAgQIECgAgHBvoImKoEAAQIECBAgQICAYG8OECBAgAABAgQIEKhAQLCvoIlKIECAAAECBAgQICDYmwMECBAgQIAAAQIEKhAQ7CtoohIIECBAgAABAgQICPbmAAECBAgQIECAAIEKBAT7CpqoBAIECBAgQIAAAQKCvTlAgAABAgQIECBAoAIBwb6CJiqBAAECBAgQIECAgGBvDhAgQIAAAQIECBCoQECwr6CJSiBAgAABAgQIECAg2JsDBAgQIECAAAECBCoQEOwraKISCBAgQIAAAQIECAj25gABAgQIECBAgACBCgQE+wqaqAQCBAgQIECAAAECgr05QIAAAQIECBAgQKACAcG+giYqgQABAgQIECBAgIBgbw4QIECAAAECBAgQqEBAsK+giUogQIAAAQIECBAgINibAwQIECBAgAABAgQqEBDsK2iiEggQIECAAAECBAgI9uYAAQIECBAgQIAAgQoEBPsKmqgEAgQIECBAgAABAoK9OUCAAAECBAgQIECgAgHBvoImKoEAAQIECBAgQICAYG8OECBAgAABAgQIEKhAQLCvoIlKIECAAAECBAgQICDYmwMECBAgQIAAAQIEKhAQ7CtoohIIECBAgAABAgQICPbmAAECBAgQIECAAIEKBAT7CpqoBAIECBAgQIAAAQKCvTlAgAABAgQIECBAoAIBwb6CJiqBAAECBAgQIECAgGBvDhAgQIAAAQIECBCoQECwr6CJSiBAgAABAgQIECAg2JsDBAgQIECAAAECBCoQEOwraKISCBAgQIAAAQIECAj25gABAgQIECBAgACBCgQE+wqaqAQCBAgQIECAAAECgr05QIAAAQIECBAgQKACAcG+giYqgQABAgQIECBAgIBgbw4QIECAAAECBAgQqEBAsK+giUogQIAAAQIECBAgINibAwQIECBAgAABAgQqEBDsK2iiEggQIECAAAECBAgI9uYAAQIECBAgQIAAgQoEBPsKmqgEAgQIECBAgAABAoK9OUCAAAECBAgQIECgAgHBvoImKoEAAQIECBAgQICAYG8OECBAgAABAgQIEKhAQLCvoIlKIECAAAECBAgQICDYmwMECBAgQIAAAQIEKhAQ7CtoohIIECBAgAABAgQICPbmAAECBAgQIECAAIEKBAT7Cpq46CV8+9vfDr/8y78cPvOZz6woZefOneHjH/94eP7558M73/nO8KUvfWnZNq973evCH/7hH4Y/+qM/Cp/85CdH/7/gggtG25w8eTLcdNNN4YEHHlh6zN133x127969bB+f//znw0/+5E+OvvY3f/M34Q1veMOKcXz1q19dcfx82z179kw8fqrjzDPP7GzX7//+74df+ZVfWfG9/BjTOJX77xpXOkjy+Yd/+IcQj/93f/d3Sw65U37ct771raNt/+zP/qxzvL/3e7836mW573/+53/u7F8cSzzWzTffvKJXaZzpmK985StHX5qmX/k26Rhl39P+o9Edd9wxdu5F03H96Zozads0N9N8jNum742bZ3m/fvAHf7D3eRHHls/zrnk2zdxKxee97nqudD2vuurs2l/Zh/L5We4nff+nfuqnwvbt25fNn7LO/PlZzpeyv1326Vhf+MIXlv0MWfSfrcZPgEB7AoJ9ez0fXMUpTPzwD//wKMR3hd/0i/sd73jHimAeCyoDbNc+0z4uueSSZcfJf/FPCv4ptOYBLX2tK0DnYWNS+BkX+FIgS8eYxqls7lqDfR6c8pBcBvvy5CYG5Pi1n/iJnxgF9XTSkIL9uP51TcrUl7Inff0q50rpOI1RqjnZ9wXy8mQpnUzmc2a1wT4/ISgDdb7fvnnWNfZxJl29TidU+YlQOolOJx+ptknbxjkQT6DHmcYx3XvvvUvBOtZ1ww03hAMHDoTf/u3fXppLZ5111mhuxY/48+K73/3u6AQonpCnE8T0vb/9279ddsxUXxnu09f7nqeD++FpQAQIECgEBHtTYu4C0wTW1Qb7GP7iL+sYFsqwkRecjr158+bRl48fP77sMen7MTSMu+LbdWKRh7gYTGPgLUNePo6u8FVe9Y7bxwAz6QRomtBahtCuK/Z5wEmB69WvfnU444wzll2xzwNS3qMUsNYa7FPQKq/M9vUr1laG8r4J3nXyU87JJ554YvQKxbgr7ekY+cljrL3sVd8JwqQTsfTY8kQn/3rXPJtmbqXnSDr+W97yllHILust5+Sk51bp3vccLvsWP4/j6XoO507phCGNtTxByMfRNYb8lQPBvu/Z4vsECAxdQLAfeocaGN+sg326ijhNAM7DRKQuw9u0IbEMZHkAuv/++0dXHieNZ1zgy0PKaupK02YtV+xvueWW8NnPfjbEf9/+9rePro6eOHFi2YlPWoqzHsE+ha94wHxpVRnau/qVbxP/P+lkapLRWoN93se/+Iu/WLE8a63BPs3DcpnJNPNsmrkVXx3Ia37f+94X3v3ud4fyFZY8BJdj6ftRNe5qed6H/MQkjvuv//qvV7yKl8b5zW9+czQ/jhw5suxK/6TjdJ0c5Seu//iP/2gpTl8jfZ8AgUELCPaDbk8bg5u0djwFs76rfXmAjWpxPf40yz76HtcXxMaFw3K8kwJ23Mek8JVONl772teOXXM9LsCuJdj/5m/+5uh+h/gqxfXXXz86Zgz4cVlDugLfFezTEplJS3HKeyRi7fnJQR4cu2rq61fcX9/a7fJZ1WU0bilO+dg83I67Z6FrCdc0a+zTUpz8+VE+bpp5Ns3cir3Oa04ndKnf+ZX51dy/kLz6nkflUpgtW7aMTijj+vo4//KPNM/Sqzkf+9jHlp1AjTsZz5csJcdk+6u/+qvh2LFjK07E2vgJrEoCBGoSEOxr6uaC1rKaK/ZlMEzLEqYJfCVPedz4/XxdeAwzfYFkXLAvH9d35X+a8JWC/TSvREy6Gp2+NymIxpATx/z+978//OIv/uLoCn5cV10G+9I0BaZxN8/2nWyNW24SjzNNv7oCYPrapDDddfPsagJ5PEYZsrvmdd986jrJSEG260Rnmnk2zdyKwb489qSxljeT990c3tW/dC9NfqKQns/JLi5/y29mz++vSP0sx931XMvHm4811vi7v/u7o+f5oUOHBPsF/R1i2AQI/KeAYG82zF1gNcF+mptnp12y0hUA8qvO5VXM8sphGSLTDYXpamN+tXPcjbt9VzTnsRQnBsj4EddYp3X1Dz/8cNi7d+/EK/a5x1qC/bjlJmm/0/SrnMx97xgTtx/3TjT5iUBfII/7KbfpeqeVvv2MC9ddwblrvXvXPJtmKU7XcyZ5j3t3nFjzNO9QNO55El+RGPduNunG2biMLb1ykeqI+8vHVK6pL5fi5K945Gvo07HjkrP43O57VW3uPygNgAABAlMICPZTINlkfQVmHexjEJjm5tn86l9ZYXnlcDU3z8Z9db01Z/z6uJvzprnBMT5+I26ejcE+f3vBGCo/+tGPho985CPrFuwnLTdJvZmmX10ztW9+jbvimy+z6QvkXW+tmo9l2nfX6XrlKe6nvNcgfq3rLVjTMcubn8t7Rya91Wlp2LeWvs+mDPbppvY0n+Oyr/IY+ZX0+MpZ11X98oSv6+bZdJId36WofO7lJwplzX03Sa/vT0R7J0CAwNoFBPu123nkjAT6glceYqa5Yl/eCJjeQjO/mhlf4v+1X/u1UQXxF3xaQ5zGkn+960pxCgVdb3cZb+aLQWrcWx1Os6QiHr887jROZUvWssY+ju9tb3vb0nr++Hm55rprjX1+7NVese9bqtTVl3i8rq+Xr7qs5v6MdHU47SOd4PWF13GvyJRf73t3nbxfaR6NC5nlHEz+5de7xl56l2blyVQaQ1ed4+ZY3xxI4+o62Y37jB/xedr3qkv+zlXj3u4y7qsvrLtiP6Mf6HZDgMBcBQT7ufI7eB7Ouv5AVbqS1/c+6F2/lCf9gapJywy6Qs60f6Dqd37nd0bvud31h24mLccZd/Vw2j9QNe6q6rir3PEq/D333BNuu+22pavw6Q9UdZ2slMuL1hrsu26ejWP5pV/6pfAzP/MznU+IGPw+9KEPhWuuuWbZEoxx4TN+vax70nKSrrlTvoIQl1V1/QGxeKy0dKnrZK5cjpPCello6l++znvctvGxl112WTjnnHPCM888s+Jq/riTifKY5Q2k5UlufnKZ+5XPhbX8gapJr3A88sgj4amnnlq6cXbcKxNdy2ri/MqfC+Pmf9d8EOz9PiJAoAYBwb6GLqqBAAECBAgQIECgeQHBvvkpAIAAAQIECBAgQKAGAcG+hi6qgQABAgQIECBAoHkBwb75KQCAAAECBAgQIECgBgHBvoYuqoEAAQIECBAgQKB5AcG++SkAgAABAgQIECBAoAYBwb6GLqqBAAECBAgQIECgeQHBvvkpAIAAAQIECBAgQKAGAcG+hi6qgQABAgQIECBAoHkBwb75KQCAAAECBAgQIECgBgHBvoYuqoEAAQIECBAgQKB5AcG++SkAgAABAgQIECBAoAYBwb6GLqqBAAECBAgQIECgeQHBvvkpAIAAAQIECBAgQKAGAcG+hi6qgQABAgQIECBAoHkBwb75KQCAAAECBAgQIECgBgHBvoYuqoEAAQIECBAgQKB5AcG++SkAgAABAgQIECBAoAYBwb6GLp5mDadOnQp79uwJO3bsCNu3b1/a2+HDh8OhQ4dGn19//fWj76ePY8eOhV27doUTJ06Eiy66KOzevTts2rTpNEfi4QQIECBAgAABAmsVEOzXKlfJ41Kof/rpp8O+ffuWgn0M7nv37g233377qNL0/61bt4b0mHgScPnll49OCuL/8+BfCY8yCBAgQIAAAQILIyDYL0yrZj/QdNX9ggsuCC+++OLoqny6Yh+v1h85cmTpSvx9990XtmzZMgrveeiPQT9uF7d31X72PbJHAgQIECBAgMC0AoL9tFIVbhfDfPyIS2g++MEPLgv2McjHjxtvvHH0b/55DPJxic7BgwfD5s2bR8E+/7xCKiURIECAAAECBAYvINgPvkXrP8Djx493Bvt0hT6OIF6Rf/7550dBv7xCH6/g33///eHOO+8cBf1F+vjL8MAiDddYCRAgQCCE8KawkwMBAh0Cgr1pEeYd7I8ePTq3LnzmdTeFf/u+787t+A5MgAABAqsT+P5/Pyu89UsfX92DZrj1tm3bVr23W7/wf1b9mLU8YP8l/20tD/OYigQE+4qaudZSxgX7uL/al+K4Yr/WWeNxBAgQmJ/Aol2xF+znN1daO7Jg31rHO+rtCvb50pv4kPLm2XzpjZtnTSICBAgQIDBeQLA3OzZKQLDfKOkBH6cr2Hu7ywE3zNAIECBAYKEEBPuFatdCD1awX+j2zWbwXcE+7tkfqJqNr70QIECAQNsCgn3b/d/I6gX7jdR2LAIECBAgQKA5AcG+uZbPrWDBfm70DkyAAAECBAi0ICDYt9DlYdQo2A+jD0ZBgAABAgQIVCog2Ffa2AGWJdgPsCmGRIAAAQIECNQjINjX08uhVyLYD71DxkeAAAECBAgstIBgv9DtW6jBC/YL1S6DJUCAAAECBBZNQLBftI4t7ngF+8XtnZETIECAAAECCyAg2C9AkyoZomBfSSOVQYAAAQIECAxTQLAfZl9qHJVgX2NX1USAAAECBAgQINCcgGDfXMsVTIAAAQIECBAgUKOAYF9jV9VEgAABAgQIECDQnIBg31zLFUyAAAECBAgQIFCjgGBfY1fVRIAAAQIECBAg0JyAYN9cyxVMgAABAgQIECBQo4BgX2NX1USAAAECBAgQINCcgGDfXMsVTIAAAQIECBAgUKOAYF9jV9VEgAABAgQIECDQnIBg31zLFUyAAAECBAi0JnDy5Mlw2223hU996lOj0s8555zwyCOPhPPPP3/0+fHjx8N1110XnnnmmWU0O3fuDLt27VrBlbaP37v44ovDgQMHwgMPPLBiu3ScJ554ovP78QHxGB/4wAdG47v00kvD61//+nDttdeOvnbVVVct2+fjjz8eHnvssfDwww+HzZs3j74Xv3brrbcubbd///4Vj2ul34J9K51WJwECBAgQINCkQArhV1999VLgfe6555aF5zKo52E/f1wC7Ar28XtdJwElejwJKLdNJx4x2F9xxRWjkH/22Wcv21/aJv963NeLL74Y7rnnnnDmmWeGtE3cf/paS00X7FvqtloJECBAgACB5gS6gnREeOqpp0ZX2uPV7/gRr9inK/AJadxj1zPYx6v0XVfm48lIHF8cU3ylIR9/unqfn5CUtbTQeMG+hS6rkQABAgQIEGhSoOtKfNdV99e85jUrgv20j01LcWZ1xT4G+/SKwr333jta6hM/Yth/8sknl67EjzvpiNtO+l7NE0Gwr7m7aiNAgAABAgTmLvCXYeXa82kGdUY4Y7TZS+Gl0b/p8/Jr6ftvCjtX7La8yp1vkC9/ueyyyzrX2I9brz7tGvuuNfp9S3FisC+X3eRjzb8fl+6U6/C7TgKm8a5hG8G+hi6qgQABAgQIEBiswF1h+4aM7c5w5LSDfb58pWtNezrAei/FKcP5Cy+8sGwZThn0y8LLq/sb0oABHESwH0ATDIEAAQIECBCoV2AtV+zLq/X5Fft4hT5+P7+SH//fdcV+2uU0XUtx4jGnXce+mqUv01yxj8fOX2344he/uGwZTvy+pTgrnzOCfb0/R1RGgAABAgQIEBgbgKe5eTbf5qGHHgrnnXfeaOlLucRnPYJ9uip/4YUXjt6Gs1x2M+1JR0tTQLBvqdtqJUCAAAECBJoTWOvbXZZLcfLlLV/+8peX3lEnviPNegT72Kj0HvXl++6nJnq7y+XTWbBv7umtYAIECBAgQKA1gbX+gaorr7xyxXvEpz9y9eijjy69Y824P1AVnfPt4ufTLsWJ26Z3x9m2bdvY96X3B6r+czYL9q09s9VLgAABAgQIECBQpYBgX2VbFUWAAAECBAgQINCagGDfWsfVS4AAAQIECBAgUKWAYF9lWxVFgAABAgQIECDQmoBg31rH1UuAAAECBAgQIFClgGBfZVsVRYAAAQIECBAg0JqAYN9ax9VLgAABAgQIECBQpYBgX2VbFUWAAAECBAgQINCagGDfWsfVS4AAAQIECBAgUKWAYF9lWxVFgAABAgQIECDQmoBg31rH1UuAAAECBAgQIFClgGBfZVsVRYAAAQIECBAg0JqAYN9ax9VLgAABAgQIECBQpYBgX2VbFUWAAAECBAgQINCagGDfWsfVS4AAAQIECBAgUKWAYF9lWxVFgAABAgQIECDQmoBg31rH1UuAAAECBAgQIFClgGBfZVsVRYAAAQIECBAg0JqAYN9ax9VLgAABAgQIECBQpYBgX2VbFUWAAAECBAgQINCagGDfWsfVS4AAAQIECBAgUKWAYF9lWxVFgAABAgQIECDQmoBg31rH1UuAAAECBAgQIFClgGBfZVsVRYAAAQIECBAg0JqAYN9ax9VLgAABAgQIECBQpYBgX2VbFUWAAAECBAgQINCagGDfWsfVS4AAAQIECBAgUKWAYF9lWxVFgAABAgQIECDQmoBg31rH1UuAAAECBAgQIFClgGBfZVsVRYAAAQIECBAg0JqAYN9ax9VLgAABAgQIECBQpYBgX2VbFUWAAAECBAgQINCagGDfWsfVS4AAAQIECBAgUKWAYF9lWxVFgAABAgQIECDQmoBg31rH1UuAAAECBAgQIFClgGBfZVsVRYAAAQIECBAg0JqAYN9ax9VLgAABAgQIECBQpYBgX2VbFUWAAAECBAgQINCagGDfWsfVS4AAAQIECBAgUKWAYF9lWxVFgAABAgQIECDQmoBg31rHp6j31KlTYc+ePeHpp59etvXLX/7ycODAgbB169Zw+PDhcOjQoaXvn3vuueHgwYNh8+bNUxzBJgQIECBAgAABArMWEOxnLVrp/u67775RZTfeeOPo3/j5li1bwo4dOyqtWFkECBAgQIAAgcUSEOwXq19zGe2RI0dGV+fTFfl0RT+G+u3bt89lTA5KgAABAgQIECCwXECwNyMmCqQQHwN8ujp//PjxcNddd4UbbrhhtCzHBwECBAgQIECAwPwFBPv592DQIyiv1sfBHjt2LOzatSucOHFiaez79u1z9X7QnTQ4AgQIECBAoHYBwb72Dp9mfeXa+ri7GPb379+/dCNt+flqD3n06NHVPsT2BAgQIEBgLgLbtm2by3EdlMA0AoL9NEqNbjPtWvqu5TqNkimbAAECBAgQIDA3AcF+bvTDP3BccrN3795w++23T1xLP+0JwPArNkICBAgQIECAwOIKCPaL27t1H3lcYhPfr3737t1h06ZNS8eLX4vfS1+Pn3/605/2Pvbr3hEHIECAAAECBAiMFxDszY6xAmWAzzfM/0BV/oercBIgQIAAAQIECMxHQLCfj7ujEiBAgAABAgQIEJipgGA/U047I0CAAAECBAgQIDAfAcF+Pu6OSoAAAQIECBAgQGCmAoL9TDntjAABAgQIECBAgMB8BAT7+bg7KgECBAgQIECAAIGZCgj2M+W0MwIECBAgQIAAAQLzERDs5+PuqAQIECBAgAABAgRmKiDYz5TTzggQIECAAAECBAjMR0Cwn4+7oxIgQIAAAQIECBCYqYBgP1NOOyNAgAABAgQIECAwHwHBfj7ujkqAAAECBAgQIEBgpgKC/Uw57YwAAQIECBAgQIDAfAQE+/m4OyoBAgQIECBAgACBmQoI9jPltDMCBAgQIECAAAEC8xEQ7Ofj7qgECBAgQIAAAQIEZiog2M+U084IECBAgAABAgQIzEdAsJ+Pu6MSIECAAAECBAgQmKmAYD9TTjsjQIAAAQIECBAgMB8BwX4+7o5KgAABAgQIECBAYKYCgv1MOe2MAAECBAgQIECAwHwEBPv5uDsqAQIECBAgQIAAgZkKCPYz5bQzAgQIECBAgAABAvMREOzn4+6oBAgQIECAAAECBGYqINjPlNPOCBAgQIAAAQIECMxHQLCfj7ujEiBAgAABAgQIEJipgGA/U047I0CAAAECBAgQIDAfAcF+Pu6OSoAAAQIECBAgQGCmAoL9TDntjAABAgQIECBAgMB8BAT7+bg7KgECBAgQIECAAIGZCgj2M+W0MwIECBAgQIAAAQLzERDs5+PuqAQIECBAgAABAgRmKiDYz5TTzggQIECAAAECBAjMR0Cwn4+7oxIgQIAAAQIECBCYqYBgP1NOOyNAgAABAgQIECAwHwHBfj7ujkqAAAECBAgQIEBgpgKC/Uw57YwAAQIECBAgQIDAfAQE+/m4OyoBAgQIECBAgACBmQoI9jPltDMCBAgQIECAAAEC8xEQ7Ofj7qgECBAgQIAAAQIEZiog2M+U084IECBAgAABAgQIzEdAsJ+Pu6MSIECAAAECBAgQmKmAYD9TTjsjQIAAAQIECBAgMB8BwX4+7o5KgAABAgQIECBAYKYCgv1MOe2MAAECBAgQIECAwHwEBPv5uDsqAQIECBAgQIAAgZkKCPYz5bQzAgQIECBAgAABAvMREOzn4+6oBAgQIECAAAECBGYqINjPlNPOCBAgQIAAAQIECMxHQLCfj7ujEiBAgAABAgQIEJipgGA/U047I0CAAAECBAgQIDAfAcF+Pu6OSoAAAQIECBAgQGCmAoL9TDntjAABAgQIECBAgMB8BAT7+bg7KgECBAgQIECAAIGZCgj2M+W0MwIECBAgQIAAAQLzERDs5+PuqAQIECBAgAABAgRmKiDYz5TTzggQIECAAAECBAjMR0Cwn4+7oxIgQIAAAQIECBCYqYBgP1NOOyNAgAABAgQIECAwHwHBfj7ujkqAAAECBAgQIEBgpgKC/Uw57YwAAQIECBAgQIDAfAQE+/m4OyoBAgQIECBAgACBmQoI9jPltDMCBAgQIECAAAEC8xEQ7Ofj7qgECBAgQIAAAQIEZiog2M+U084IECBAgAABAgQIzEdAsJ+Pu6MSIECAAAECBAgQmKmAYD9TTjsjQIAAAQIECBAgMB8BwX4+7o5KgAABAgQIECBAYKYCgv1MOevZ2alTp8KePXvC008/vVTUFVdcEW688cbR58eOHQu7du0KJ06cCBdddFHYvXt32LRpUz0AKiFAgAABAgQILJiAYL9gDduo4R4/fjzcdddd4YYbbghbt25ddtgU+rdv3x4uv/zy0QlA/P+OHTs2aniOQ4AAAQIECBAgUAgI9qZEp0C8In///feHO++8M2yZ9tGUAAAgAElEQVTevHnZNvF7e/fuDbfffvso9B85ciQcPnzYVXtziQABAgQIECAwRwHBfo74Qz70pLAev3fo0KFw8ODBUegvPx9yXcZGgAABAgQIEKhVQLCvtbOnWVe8Ah/De/o499xzlwX5/Ar9pKv7pzkMDydAgAABAgQIEJhSQLCfEqq1ze67777wrW99a2l5Tf75s88+u2zpzekG+6NHj7bGq14CBAgQWFCBbdu2LejIDbsFAcG+hS7PoMZ8Xf13vvMdS3FmYGoXBAgQIECAAIFZCgj2s9SseF/5Vfn4jjn5jbVunq248UojQIAAAQIEFkZAsF+YVm3cQPO3s4xvYZk+f9WrXjV6H3tvd7lxvXAkAgQIECBAgMC0AoL9tFKNbVf+garyj1D5A1WNTQjlEiBAgAABAoMXEOwH3yIDJECAAAECBAgQINAvINj3G9mCAAECBAgQIECAwOAFBPvBt8gACRAgQIAAAQIECPQLCPb9RrYgQIAAAQIECBAgMHgBwX7wLTJAAgQIECBAgAABAv0Cgn2/kS0IECBAgAABAgQIDF5AsB98iwyQAAECBAgQIECAQL+AYN9vZAsCBAgQIECAAAECgxcQ7AffIgMkQIAAAQIECBAg0C8g2Pcb2YIAAQIECBAgQIDA4AUE+8G3yAAJECBAgAABAgQI9AsI9v1GtiBAgAABAgQIECAweAHBfvAtMkACBAgQIECAAAEC/QKCfb+RLQgQIECAAAECBAgMXkCwH3yLDJAAAQIECBAgQIBAv4Bg329kCwIECBAgQIAAAQKDFxDsB98iAyRAgAABAgQIECDQLyDY9xvZggABAgQIECBAgMDgBQT7wbfIAAkQIECAAAECBAj0Cwj2/Ua2IECAAAECBAgQIDB4AcF+8C0yQAIECBAgQIAAAQL9AoJ9v5EtCBAgQIAAAQIECAxeQLAffIsMkAABAgQIECBAgEC/gGDfb2QLAgQIECBAgAABAoMXEOwH3yIDJECAAAECBAgQINAvINj3G9mCAAECBAgQIECAwOAFBPvBt8gACRAgQIAAAQIECPQLCPb9RrYgQIAAAQIECBAgMHgBwX7wLTJAAgQIECBAgAABAv0Cgn2/kS0IECBAgAABAgQIDF5AsB98iwyQAAECBAgQIECAQL+AYN9vZAsCBAgQIECAAAECgxcQ7AffIgMkQIAAAQIECBAg0C8g2Pcb2YIAAQIECBAgQIDA4AUE+8G3yAAJECBAgAABAgQI9AsI9v1GtiBAgAABAgQIECAweAHBfvAtMkACBAgQIECAAAEC/QKCfb+RLQgQIECAAAECBAgMXkCwH3yLDJAAAQIECBAgQIBAv4Bg329kCwIECBAgQIAAAQKDFxDsB98iAyRAgAABAgQIECDQLyDY9xvZggABAgQIECBAgMDgBQT7wbfIAAkQIECAAAECBAj0Cwj2/Ua2IECAAAECBAgQIDB4AcF+8C0yQAIECBAgQIAAAQL9AoJ9v5EtCBAgQIAAAQIECAxeQLAffIsMkAABAgQIECBAgEC/gGDfb2QLAgQIECBAgAABAoMXEOwH3yIDJECAAAECBAgQINAvINj3G9mCAAECBAgQIECAwOAFBPvBt8gACRAgQIAAAQIECPQLCPb9RrYgQIAAAQIECBAgMHgBwX7wLTJAAgQIECBAgAABAv0Cgn2/kS0IECBAgAABAgQIDF5AsB98iwyQAAECBAgQIECAQL+AYN9vZAsCBAgQIECAAAECgxcQ7AffIgMkQIAAAQIECBAg0C8g2Pcb2YIAAQIECBAgQIDA4AUE+8G3yAAJECBAgAABAgQI9AsI9v1GtiBAgAABAgQIECAweAHBfvAtMkACBAgQIECAAAEC/QKCfb+RLQgQIECAAAECBAgMXkCwH3yLDJAAAQIECBAgQIBAv4Bg329kCwIECBAgQIAAAQKDFxDsB98iAyRAgAABAgQIECDQLyDY9xvZggABAgQIECBAgMDgBQT7wbfIAAkQIECAAAECBAj0Cwj2/UZNbnH8+PHwwQ9+MHzjG98Y1X/RRReF3bt3h02bNo0+P3z4cDh06NCSzbnnnhsOHjwYNm/e3KSXogkQIECAAAEC8xYQ7OfdgQEe/9SpU2HPnj1h+/btYceOHSF9/qpXvSrceOONoxHfd999YcuWLaPv+yBAgAABAgQIEJi/gGA//x4sxAjiFfojR46MrtrHjxj8Y6iP4d8HAQIECBAgQIDA/AUE+/n3YCFGkAf7kydPhrvuuivccMMNYevWrQsxfoMkQIAAAQIECNQuINjX3uEZ1JfW2//8z//86Cr9sWPHwq5du8KJEyeW9r5v3z5X72dgbRcECBAgQIAAgbUKCPZrlWvkcWl9fSw33Twbl+Ts378/HDhwYHTFvvx8tTRHjx5d7UNsT4AAAQIE5iKwbdu2uRzXQQlMIyDYT6PU6DZdob6LorzZtlEuZRMgQIAAAQIE5iog2M+Vf7gH73onnHGjTdu6mXa4/TQyAgQIECBAoH4Bwb7+Hq+pwvh2lt/61reWvXd92lF+I218X/v4+ac//WnvY78maQ8iQIAAAQIECMxGQLCfjWNVeyn/OFUqLv8jVPkfqHr5y1++tN6+KgjFECBAgAABAgQWSECwX6BmGSoBAgQIECBAgACBcQKCvblBgAABAgQIECBAoAIBwb6CJiqBAAECBAgQIECAgGBvDhAgQIAAAQIECBCoQECwr6CJSiBAgAABAgQIECAg2JsDBAgQIECAAAECBCoQEOwraKISCBAgQIAAAQIECAj25gABAgQIECBAgACBCgQE+wqaqAQCBAgQIECAAAECgr05QIAAAQIECBAgQKACAcG+giYqgQABAgQIECBAgIBgbw4QIECAAAECBAgQqEBAsK+giUogQIAAAQIECBAgINibAwQIECBAgAABAgQqEBDsK2iiEggQIECAAAECBAgI9uYAAQIECBAgQIAAgQoEBPsKmqgEAgQIECBAgAABAoK9OUCAAAECBAgQIECgAgHBvoImKoEAAQIECBAgQICAYG8OECBAgAABAgQIEKhAQLCvoIlKIECAAAECBAgQICDYmwMECBAgQIAAAQIEKhAQ7CtoohIIECBAgAABAgQICPbmAAECBAgQIECAAIEKBAT7CpqoBAIECBAgQIAAAQKCvTlAgAABAgQIECBAoAIBwb6CJiqBAAECBAgQIECAgGBvDhAgQIAAAQIECBCoQECwr6CJSiBAgAABAgQIECAg2JsDBAgQIECAAAECBCoQEOwraKISCBAgQIAAAQIECAj25gABAgQIECBAgACBCgQE+wqaqAQCBAgQIECAAAECgr05QIAAAQIECBAgQKACAcG+giYqgQABAgQIECBAgIBgbw4QIECAAAECBAgQqEBAsK+giUogQIAAAQIECBAgINibAwQIECBAgAABAgQqEBDsK2iiEggQIECAAAECBAgI9uYAAQIECBAgQIAAgQoEBPsKmqgEAgQIECBAgAABAoK9OUCAAAECBAgQIECgAgHBvoImKoEAAQIECBAgQICAYG8OECBAgAABAgQIEKhAQLCvoIlKIECAAAECBAgQICDYmwMECBAgQIAAAQIEKhAQ7CtoohIIECBAgAABAgQICPbmAAECBAgQIECAAIEKBAT7CpqohNULfOKr/xb+1/HvjR74Pza/LLzrgu9f/U4W7BFq1ucFm7KrGq75bX6vasLYmEClAoJ9pY1V1niBz3/z/4bPfePfw5vP/b7RRun/b/jv/6VaNjXrc7WTO4RgfpvfNc9vtRFYjYBgvxot21YhEK/svfjdl8K7X/v94dVnnhHu/8q/hlf81zOqvmqvZn2u4sk7pgjz2/yueX6rjcBqBAT71WjZtgqBGOTjxw3/87+O/i0/r6LIogg163ON8zrVZH6b3zXPb7URWI2AYL8aLdtWIVBeoY9X+/73v760FPSrKLIj2OevSqi5xi7/R7hrrc/p5Ly1ulvsdYs11/mTSlXrKSDYr6eufQ9SoMVfDmoOwcnMIJ+OMxmU+W1+z2Qi2QmBCgQE+wqaqITVCXjZ3sv2q5sxi7N1i3M7XbGP/1pe958GizNrpx9pq/N7eiFbEghBsDcLmhMor9y2cvNsvtxIzXVO+xbnduxki3WreeXSszqf1aoisDoBwX51XrauQMBb43lrvAqmcWcJLc7tCNFi3Wpu4+dYrT+r1LV+AoL9+tna84AF/DEbf8xmwNPztIbW4txOV+390Tl/aO+0njweTKACAcG+giYqgQABAgQIECBAgIBgbw4QIECAAAECBAgQqEBAsK+giUpYu8CDDz4Y3vve9659Bwv4SDUvYNPWMOQW+xyZWqxbzWt4gngIgUoFBPtKG6us6QT8QpzOadG30udF7+D049fr6a0WecsW+7zI/TL2jRMQ7DfO2pEGKNDiLwc1D3AirsOQWuyzK/brMJEGustW5/dA22FYAxIQ7AfUDEPZeIEWfzmoeePn2TyO2GKfBft5zLT5HLPV+T0fbUddJAHBfpG6ZawzF2jxl4OaZz6NBrnDFvss2A9yKq7LoFqd3+uCaadVCQj2VbVTMasVaPGXg5pXO0sWc/sW+yzYL+ZcXcuoW53fa7HymLYEBPu2+q3aQqDFXw5qbuNp0GKfBfs25narfW6nuyo9HQHB/nT0Gn7ssWPHwq5du8KJEyfCRRddFHbv3h02bdq0cCIthh81L9w0XdOAW+xzq4GvxV63WPOafhB4UHMCgn1zLT/9gk+dOhX27NkTtm/fHi6//PKl/+/YseP0d77Be2jxl4OaN3iSzelwLfZZsJ/TZJvDYVud33OgdsgFExDsF6xhQxhuvFq/d+/ecPvtt4etW7eGI0eOhMOHDy/kVfsWfzmoeQjPovUfQ4t9FuzXf14N5Qitzu+h+BvHcAUE++H2ZrAji0H+0KFD4eDBg2Hz5s2jYJ9/Pu3AH/hqCA99bdqt12e79774YHjw7I37y7MvhRDOWJ9Spt6rmqemWvOGLfY5YrVYt5rX/DQ5rQdu9M+xcrDveU0IOy84rRI8mMC6CAj268Ja907LK/TxCv79998f7rzzzlHQn/ZDsJ9WarbbbfQvRMFntv2bdm8b3WfBftrOzH67je51i89pwX7289Ye10dAsF8f16r3OvNgf9f2qr0UR4AAAQIVCdx5JLhiX1E/KytFsK+soRtRTk1LcTbCKz/GEK50qXn9BVrs81Cu2K9/d5cfocVet1izK/Yb/cxyvLUKCPZrlWv4ceXSm7XePBuX4gzh44z/v+j9pfjbKq6BPyOE9P/0+Si0zOD7R7+9suJtr9x4BTW30edyZqW+5/O5a/Z1zY/yMeU28fvm939oek5vzM+0jfw51vW7wRr7jemzo6xOQLBfnZetQwg1vd2lhhIgQIAAAQIEahEQ7Gvp5AbXUcsfqNpgNocjQIAAAQIECKybgGC/brR2TIAAAQIECBAgQGDjBAT7jbN2JAIECBAgQIAAAQLrJiDYrxutHRMgQIAAAQIECBDYOAHBfuOsHYkAAQIECBAgQIDAugkI9utGa8cECBAgQIAAAQIENk5AsN84a0caoMCBAwfCeeedF6666qoBjm52Qzp+/Hi47rrrwjPPPDPa6ZVXXhnuueeecOaZZ87uIAPc0+OPPx5uvfXW0cguvPDC8PDDD4fNmzcPcKTrM6Q4v+PHrl271ucAA9nrU089Fa655pql0ZxzzjnhkUceCeeff/5ARrg+w8jnd+3P6fJnWBJt8Xm9PrPJXmsREOxr6aQ6Vi0QQ88DDzwQ9u/fX3WwP3nyZLjtttvCpZdeOqozfX722WdXHfhi2Is9TmE+/v/FF19s4oQmPhlS2N25c2fVfY61xoD79a9/vfo68x9ysb+33HLL0glMKydxyaD8ubbqXwAeQKBSAcG+0sYqa7xAuvLzile8YrTR2972tqqDfZdEDEJPPvlkMyE3Bd086Nf8HImhZ9++feHZZ58Nl1xySfWBt5VX3vI522LNef0t/gyr+WeW2mYnINjPztKeFkQgBvvvfOc74Yd+6IeWXclekOHPZJgt/lJs6Ypm7G/8iFex40fNS3HSldurr746XHzxxTN5fgx9J+niROxrKzXnPWm9/qHPT+Obr4BgP19/R5+jQKsv5aZfijEI1X5vQbpSH9dft7LuOvb37rvvDnfccUd46KGHqg/2XWuva19eF2u++eabwzve8Y7RUsIXXnihmftm4oRu8cLEHH9VOvSCCQj2C9Yww52dQIvBPtUcFVu4eTafLeWa5NnNpGHtKb4y8cY3vnF0JbeFVymee+65cO2114Z77713VHP5+bC6M5vRpJOZLVu2jJ7H8SPeR1P7fTOxzhZ/bs9m1thLKwKCfSudVucKgdZ+QbQc6lsJBDHUfuITnwgf/vCHR+941EKw7/rRVnvdXUtRypvFa/2Rn16tiHO89nc9qrWH6lpfAcF+fX3tfcACLQX7Vt4JZ9J0a2Fdbv72h7lF7W+FWPa99htLu+4riMH+scceq/6VuFZOYAb8q9PQBi4g2A+8QYa3fgItBfvW3uoxzpoyAMTQG4NPS+9lX/uV664+t7LkKl9nnpbipLe0Xb+fmvPfc4tvbTp/dSNYJAHBfpG6ZawzFWgl2Lf8h13yK9it3DybP0laCPYp3Od/oOrRRx9t4t1i8vndwt8riL1uZU7P9JednTUlINg31W7FEiBAgAABAgQI1Cog2NfaWXURIECAAAECBAg0JSDYN9VuxRIgQIAAAQIECNQqINjX2ll1ESBAgAABAgQINCUg2DfVbsUSIECAAAECBAjUKiDY19pZdREgQIAAAQIECDQlINg31W7FEiBAgAABAgQI1Cog2NfaWXURIECAAAECBAg0JSDYN9VuxRIgQIAAAQIECNQqINjX2ll1ESBAgAABAgQINCUg2DfVbsUSIECAAAECBAjUKiDY19pZdREgQIAAAQIECDQlINg31W7FEiBAgAABAgQI1Cog2NfaWXURIECAAAECBAg0JSDYN9VuxRIgQIAAAQIECNQqINjX2ll1ESBAgAABAgQINCUg2DfVbsUSIECAAAECBAjUKiDY19pZdREgQIAAAQIECDQlINg31W7FEiBAgAABAgQI1Cog2NfaWXURIECAAAECBAg0JSDYN9VuxRIgQIAAAQIECNQqINjX2ll1ESBAgAABAgQINCUg2DfVbsUSIECAAAECBAjUKiDY19pZdREgQIAAAQIECDQlINg31W7FEiBAgAABAgQI1Cog2NfaWXURIECAAAECBAg0JSDYN9VuxRIgQIAAAQIECNQqINjX2ll1ESBAgAABAgQINCUg2DfVbsUSIECAAAECBAjUKiDY19pZdREgQIAAAQIECDQlINg31W7FEiBAgAABAgQI1Cog2NfaWXU1I/D444+H3/qt3wqPPPJIOP/885upW6EECBAgQIDAcgHB3owgsMACx48fD3fffXe44447wubNmxe4EkMnQIAAAQIETldAsD9dQY8nMEeBGOw3bdoUzjzzzDmOwqEJECBAgACBIQgI9kPogjEQKARiYL/uuuvCM888s/Sdc845Z8Vym7gMJ35cddVVS9udPHky3HbbbaPP77nnnhWh/6mnngrXXHPN0vZXXnll53b5kOJxbr311qUv7d+/f9kx4zeee+65cO2114YXXnhhtF3Xfsv9pB3u3Lkz7Nq1a/RpuZ+0zYUXXhgefvjh0SsTqcZPfepTK+ZOfty+Wg8cOBAeeOCBFft49NFHw8UXX7z09b7aJpnnPeqqLa8rHjDua9++feFd73rXaGlVWUM+2DTOWEfcLvmkbeLX/uqv/iq85z3vWTGf8v1E/ze+8Y3L5kXXceLXSrOuuVCCTrtcrPQpbaaxSNuUPUxj73IaN75x+xr3Stm0debPg/J5ktdY1p/mR3x+X3311aPepvmbP4fy7eJzpPzZkTt3/VxJ/Us/h+JzM38++IFNgMBwBQT74fbGyBoWiL94Y7j72Mc+trTEJn4t/oKNwSoGvjIA5mHuwQcfHP0yv+mmm5Yt0YmhIT4+D4Dx8/iRgnXO3hVY0y/7GCzSCUVXoIlfe+yxx1Yc67zzzus8EYn7i+EhjjE+rjwpifv7+te/PhrnNIGjr9ZxfqX9NLXF8dx8883he9/7XvjIRz6y7F6H6BtD87jaynHG43/iE58IH/7wh0cnZfH4Tz755NiTr1THs88+Gz70oQ8tC2CTTvySd+p333FSMM7nyjR9SDZxPv7cz/3c2IBYzu94nHJM04wxbvOnf/qn4Wd/9meXzbO0/0suuSR84AMfWDrhnTS+tK8f/dEfXfb8KHsUxzptnWW/8+dfadBVb3p+bN++PRw5cmQ0L+JHDPuXXnrpUs1xv+m5ls/puG28aJDCetfzJM2JuI8/+ZM/cf9Ow7+LlL54AoL94vXMiBsQGBdg8l/WXVcNUxCPv+D/6Z/+KbzlLW9ZFjLzcJxflYuhNAbJ8ubbcePIw8C3v/3tZScceXvK8eaBYtx240408sAfXxXIT3K6pkRfra985Ss7709IAS16pBCbTqbGjTkFvR/5kR8JZ5111lK4iv2INzbHK+bxlYau2lLP8hObeCU2nWhNOvFKgTLeZ/HmN785xHCfn6AdOnRoxRzI68v73Xeccpz5iWTXiVh5wvALv/ALozmZv7qUe3ad0JXHnGaM8YQ4fvzAD/zAMov42B/7sR8b9edNb3rT0qHTHO8aX3rM5z73uWX3scSxPv/888tqmbSfct6kE7349fSqSuxbOWfL53jy2LFjRzh8+PDoqn26kp7vp7TMT2Lj8zXv17gT3DiWr3zlK6Ohp5PMBn70KpHAwgsI9gvfQgXUKDAp3KbQ13XVMA/cf/7nfx62bNmy7App/P4tt9wy1RW4aa7GRvs8vJe9mCaM5+Htx3/8x1dcecwDYrpiP+6qfhkUJ9Xa5Rcfn1/dfOihh5auek6q7ctf/vJoWcTb3/72Za+05MeIjy+vqua1xf/H0JtfZR8XpvOxpGO8733vC7t37146QRu3XKTrCu00x0m9fvHFF3uXbqXx5XMonkh99rOfDddff33nUzYtD7n33ns7r+pPM8ZUc3mSk4x++qd/enSlPoXhSePLA+8TTzyxbB7kr8Kkk6t00tpXZ158fiLedcJTvno0ad7nJwXlc3KSXdeJXu719NNPd76aV+PPXTURqEFAsK+hi2qoSmDSL+Hyqlx+ZbcMCfm2OVBa3ztpbW0ZcMe9405f2MqDSAy/5TKgeJx8yU78vOuqfhn6xq3VL9crT6q166prbnjFFVeMgni5ZCVZ5rXF5Qrxo3xM3oNxV8qTQQr2eWjsutciHT+tIc+PUS6/yJf0dJ0glQE8v6ejPE78PL+3Ydp7M9IyolOnTvW+g1O+vrxcIz+NRR5I//iP/3jpBCSZHj16dNkrGPkrUuX48hOjr33ta0tXuaNDfg9E6t9q6swdu+4FyK3T8ppJz7V8CU88sShfgZs098qLCHnd8eJAmpdV/ZBVDIGKBQT7ipurtMUU6AuA6ap1uX66vBLbd1U7hd5xAa3v8VF30lhT4MnHm9+Am7qT3yA47sbZuG0egCa9StDV9a5ax904m24GXU1teRjPr5zmPeq6byKNNdUTTwzy0DjpMXlQT+GrPJHKT/zKY+VXiKc5Tu6aB9NxN8+Wr/j0eeb7TwG/nBvlfSdlr9NJTnzlJJ3UxGVb8f/xFY34akB6a9i+8eWvtsTQn8JyuYSrbz+TfgqNewUt7TO+spCWVo07UU9W6fkx7v6c0m7cDd9pLsdXz8oTmMX8iWrUBNoSEOzb6rdqF0BgXMjqWm+c1uqOu5rZ9a4aXQEtv+kufX9SsE+//F/zmtesuDqYHl++glCG8a4lIeUxu5YDTbtEqGx1eTW+L7RMCqL5vi677LJlV6Lz/v3BH/zB0hXicZ75ccrQ2HezaLk+Ot/XF7/4xRVLscbZ9R1n3NOm64bX/ARimnccmvSUzK8mTzPGdCKV9yQup4rPk3KJzLgTuxSQyxPnNH9f//rXL1tS1LefSfV1XYVPJ7fxBt908jVpHXz5x+m6lpiVdl0nDulEPJ0kjlvKtQA/Qg2RQNMCgn3T7Vf8EAUm3bCabnorlw10PaYMA+OucpcBJpmMC23lice4/ebBPb7XfteylnIZQNcNr+UJwKQwOemqdB5cyjA+bh5MU1u8GTFf8pLCWlznnd90Oe6+ibzm8qps382iXeErPubVr351+Pu///ult8zs6+k0x+m6wXpc4BzXo3FzravvcczlMqP4ta53b4pfz8eSTpB+/dd/PXzyk58cvQtOWi4Vw/I04yvX0acTs3iynG6QnmY/5YluvrSrPHkcd59B19X6fAlbvlSufH6WJw8p1OfvapX84nO0fAvZviV7Q/wZakwEWhYQ7FvuvtoHKdAVssoAUS4TmPRuM/lbLZZr3PtCctw+v1my64pvVxjpWh7Q9S42+fFjgOi6ubTr3oGutfp5M7teDciPFbftWn9eTohpausKXfFrv/EbvzF6F5b07jpdtZXj7LpxtuvVlDyol3XEMX/0ox8NL3vZy5a9XWoKyuU72PTdwJmfLJXBetxJ6LgThXHLSbrmYT7XJt1UncaXn+TEE8n4ikx8l6LXvva1o5tl86DeN754IlC+opNOHPK3Fe3bT3kSUnrlz694sh6fx2Xg7nrlaNwSnjygpxOI/Jjx+3Eenn322b03xI47CRvkD02DIkBgSUCwNxkIDEggX7ucD6vrj9ik9dOTlih0rcPP/zjVNFfj8qUG47YvlwJ1/WGhrjCe6o1BI/0Rpa4/hpNfnYw39HWt1Y9e+Xrv/EbM+L187OMCZtdU6KutKwCVSx3GLZUq/6hQHj4n3SyafONNneU6+nFrp2NtXVfGpzlOuiJcLjvpuj9j0sli1zKR/CQl/wNn8etpWcw0Y8xfOdEhW6oAAANfSURBVEkBNv6b3uc9BfV0chJrKd/eNY3v/e9//+hEIK3HT2OMfmnpyzT76XqbyHS/R3z8pD+mlubsO9/5zvAv//IvK966s1zmlO8rv1dl3NfzuT7uj3nlb8s5oB+ThkKAwAQBwd70IECAAAECBAgQIFCBgGBfQROVQIAAAQIECBAgQECwNwcIECBAgAABAgQIVCAg2FfQRCUQIECAAAECBAgQEOzNAQIECBAgQIAAAQIVCAj2FTRRCQQIECBAgAABAgQEe3OAAAECBAgQIECAQAUCgn0FTVQCAQIECBAgQIAAAcHeHCBAgAABAgQIECBQgYBgX0ETlUCAAAECBAgQIEBAsDcHCBAgQIAAAQIECFQgINhX0EQlECBAgAABAgQIEBDszQECBAgQIECAAAECFQgI9hU0UQkECBAgQIAAAQIEBHtzgAABAgQIECBAgEAFAoJ9BU1UAgECBAgQIECAAAHB3hwgQIAAAQIECBAgUIGAYF9BE5VAgAABAgQIECBAQLA3BwgQIECAAAECBAhUICDYV9BEJRAgQIAAAQIECBAQ7M0BAgQIECBAgAABAhUICPYVNFEJBAgQIECAAAECBAR7c4AAAQIECBAgQIBABQKCfQVNVAIBAgQIECBAgAABwd4cIECAAAECBAgQIFCBgGBfQROVQIAAAQIECBAgQECwNwcIECBAgAABAgQIVCAg2FfQRCUQIECAAAECBAgQEOzNAQIECBAgQIAAAQIVCAj2FTRRCQQIECBAgAABAgQEe3OAAAECBAgQIECAQAUCgn0FTVQCAQIECBAgQIAAAcHeHCBAgAABAgQIECBQgYBgX0ETlUCAAAECBAgQIEBAsDcHCBAgQIAAAQIECFQgINhX0EQlECBAgAABAgQIEBDszQECBAgQIECAAAECFQgI9hU0UQkECBAgQIAAAQIEBHtzgAABAgQIECBAgEAFAoJ9BU1UAgECBAgQIECAAAHB3hwgQIAAAQIECBAgUIGAYF9BE5VAgAABAgQIECBAQLA3BwgQIECAAAECBAhUICDYV9BEJRAgQIAAAQIECBAQ7M0BAgQIECBAgAABAhUICPYVNFEJBAgQIECAAAECBAR7c4AAAQIECBAgQIBABQKCfQVNVAIBAgQIECBAgACB/wfVAEI7KVYSUQAAAABJRU5ErkJggg==', `mes` = '8', `anio` = '2024', `tipo` = '7', `semana` = '32'
WHERE `id` = '281' 
 Execution Time:0.16771697998047

SELECT *
FROM `graficas_limpieza`
WHERE `id_proyecto` = '36'
AND `tipo` = '3'
AND `mes` = '8'
AND `anio` = '2024' 
 Execution Time:0.086275815963745

UPDATE `graficas_limpieza` SET `id_proyecto` = '36', `grafica` = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA00AAAGmCAYAAACgH29EAAAAAXNSR0IArs4c6QAAHaxJREFUeF7t2aERwDAMBMG4/6ZTgQ2O/oYLaGVyk/P5CBAgQIAAAQIECBAgQOAqcNgQIECAAAECBAgQIECAwF1ANHkdBAgQIECAAAECBAgQeAiIJs+DAAECBAgQIECAAAECoskbIECAAAECBAgQIECAQBPwp6m5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBEQDSNHNqaBAgQIECAAAECBAg0AdHU3EwRIECAAAECBAgQIDAiIJpGDm1NAgQIECBAgAABAgSagGhqbqYIECBAgAABAgQIEBgREE0jh7YmAQIECBAgQIAAAQJNQDQ1N1MECBAgQIAAAQIECIwIiKaRQ1uTAAECBAgQIECAAIEmIJqamykCBAgQIECAAAECBEYERNPIoa1JgAABAgQIECBAgEATEE3NzRQBAgQIECBAgAABAiMComnk0NYkQIAAAQIECBAgQKAJiKbmZooAAQIECBAgQIAAgREB0TRyaGsSIECAAAECBAgQINAERFNzM0WAAAECBAgQIECAwIiAaBo5tDUJECBAgAABAgQIEGgCoqm5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBEQDSNHNqaBAgQIECAAAECBAg0AdHU3EwRIECAAAECBAgQIDAiIJpGDm1NAgQIECBAgAABAgSagGhqbqYIECBAgAABAgQIEBgREE0jh7YmAQIECBAgQIAAAQJNQDQ1N1MECBAgQIAAAQIECIwIiKaRQ1uTAAECBAgQIECAAIEmIJqamykCBAgQIECAAAECBEYERNPIoa1JgAABAgQIECBAgEATEE3NzRQBAgQIECBAgAABAiMComnk0NYkQIAAAQIECBAgQKAJiKbmZooAAQIECBAgQIAAgREB0TRyaGsSIECAAAECBAgQINAERFNzM0WAAAECBAgQIECAwIiAaBo5tDUJECBAgAABAgQIEGgCoqm5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBEQDSNHNqaBAgQIECAAAECBAg0AdHU3EwRIECAAAECBAgQIDAiIJpGDm1NAgQIECBAgAABAgSagGhqbqYIECBAgAABAgQIEBgREE0jh7YmAQIECBAgQIAAAQJNQDQ1N1MECBAgQIAAAQIECIwIiKaRQ1uTAAECBAgQIECAAIEmIJqamykCBAgQIECAAAECBEYERNPIoa1JgAABAgQIECBAgEATEE3NzRQBAgQIECBAgAABAiMComnk0NYkQIAAAQIECBAgQKAJiKbmZooAAQIECBAgQIAAgREB0TRyaGsSIECAAAECBAgQINAERFNzM0WAAAECBAgQIECAwIiAaBo5tDUJECBAgAABAgQIEGgCoqm5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBEQDSNHNqaBAgQIECAAAECBAg0AdHU3EwRIECAAAECBAgQIDAiIJpGDm1NAgQIECBAgAABAgSagGhqbqYIECBAgAABAgQIEBgREE0jh7YmAQIECBAgQIAAAQJNQDQ1N1MECBAgQIAAAQIECIwIiKaRQ1uTAAECBAgQIECAAIEmIJqamykCBAgQIECAAAECBEYERNPIoa1JgAABAgQIECBAgEATEE3NzRQBAgQIECBAgAABAiMComnk0NYkQIAAAQIECBAgQKAJiKbmZooAAQIECBAgQIAAgREB0TRyaGsSIECAAAECBAgQINAERFNzM0WAAAECBAgQIECAwIiAaBo5tDUJECBAgAABAgQIEGgCoqm5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBEQDSNHNqaBAgQIECAAAECBAg0AdHU3EwRIECAAAECBAgQIDAiIJpGDm1NAgQIECBAgAABAgSagGhqbqYIECBAgAABAgQIEBgREE0jh7YmAQIECBAgQIAAAQJNQDQ1N1MECBAgQIAAAQIECIwIiKaRQ1uTAAECBAgQIECAAIEmIJqamykCBAgQIECAAAECBEYERNPIoa1JgAABAgQIECBAgEATEE3NzRQBAgQIECBAgAABAiMComnk0NYkQIAAAQIECBAgQKAJiKbmZooAAQIECBAgQIAAgREB0TRyaGsSIECAAAECBAgQINAERFNzM0WAAAECBAgQIECAwIiAaBo5tDUJECBAgAABAgQIEGgCoqm5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBEQDSNHNqaBAgQIECAAAECBAg0AdHU3EwRIECAAAECBAgQIDAiIJpGDm1NAgQIECBAgAABAgSagGhqbqYIECBAgAABAgQIEBgREE0jh7YmAQIECBAgQIAAAQJNQDQ1N1MECBAgQIAAAQIECIwIiKaRQ1uTAAECBAgQIECAAIEmIJqamykCBAgQIECAAAECBEYERNPIoa1JgAABAgQIECBAgEATEE3NzRQBAgQIECBAgAABAiMComnk0NYkQIAAAQIECBAgQKAJiKbmZooAAQIECBAgQIAAgREB0TRyaGsSIECAAAECBAgQINAERFNzM0WAAAECBAgQIECAwIiAaBo5tDUJECBAgAABAgQIEGgCoqm5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBEQDSNHNqaBAgQIECAAAECBAg0AdHU3EwRIECAAAECBAgQIDAiIJpGDm1NAgQIECBAgAABAgSagGhqbqYIECBAgAABAgQIEBgREE0jh7YmAQIECBAgQIAAAQJNQDQ1N1MECBAgQIAAAQIECIwIiKaRQ1uTAAECBAgQIECAAIEmIJqamykCBAgQIECAAAECBEYERNPIoa1JgAABAgQIECBAgEATEE3NzRQBAgQIECBAgAABAiMComnk0NYkQIAAAQIECBAgQKAJiKbmZooAAQIECBAgQIAAgREB0TRyaGsSIECAAAECBAgQINAERFNzM0WAAAECBAgQIECAwIiAaBo5tDUJECBAgAABAgQIEGgCoqm5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBEQDSNHNqaBAgQIECAAAECBAg0AdHU3EwRIECAAAECBAgQIDAiIJpGDm1NAgQIECBAgAABAgSagGhqbqYIECBAgAABAgQIEBgREE0jh7YmAQIECBAgQIAAAQJNQDQ1N1MECBAgQIAAAQIECIwIiKaRQ1uTAAECBAgQIECAAIEmIJqamykCBAgQIECAAAECBEYERNPIoa1JgAABAgQIECBAgEATEE3NzRQBAgQIECBAgAABAiMComnk0NYkQIAAAQIECBAgQKAJiKbmZooAAQIECBAgQIAAgREB0TRyaGsSIECAAAECBAgQINAERFNzM0WAAAECBAgQIECAwIiAaBo5tDUJECBAgAABAgQIEGgCoqm5mSJAgAABAgQIECBAYERANI0c2poECBAgQIAAAQIECDQB0dTcTBEgQIAAAQIECBAgMCIgmkYObU0CBAgQIECAAAECBJqAaGpupggQIECAAAECBAgQGBEQTSOHtiYBAgQIECBAgAABAk1ANDU3UwQIECBAgAABAgQIjAiIppFDW5MAAQIECBAgQIAAgSYgmpqbKQIECBAgQIAAAQIERgRE08ihrUmAAAECBAgQIECAQBMQTc3NFAECBAgQIECAAAECIwKiaeTQ1iRAgAABAgQIECBAoAmIpuZmigABAgQIECBAgACBEQHRNHJoaxIgQIAAAQIECBAg0AREU3MzRYAAAQIECBAgQIDAiIBoGjm0NQkQIECAAAECBAgQaAKiqbmZIkCAAAECBAgQIEBgREA0jRzamgQIECBAgAABAgQINAHR1NxMESBAgAABAgQIECAwIiCaRg5tTQIECBAgQIAAAQIEmoBoam6mCBAgQIAAAQIECBAYERBNI4e2JgECBAgQIECAAAECTUA0NTdTBAgQIECAAAECBAiMCIimkUNbkwABAgQIECBAgACBJiCampspAgQIECBAgAABAgRGBETTyKGtSYAAAQIECBAgQIBAExBNzc0UAQIECBAgQIAAAQIjAqJp5NDWJECAAAECBAgQIECgCYim5maKAAECBAgQIECAAIERAdE0cmhrEiBAgAABAgQIECDQBERTczNFgAABAgQIECBAgMCIgGgaObQ1CRAgQIAAAQIECBBoAqKpuZkiQIAAAQIECBAgQGBE4AdjpAGndD447wAAAABJRU5ErkJggg==', `mes` = '8', `anio` = '2024', `tipo` = '3', `semana` = '0'
WHERE `id` = '284' 
 Execution Time:0.080726146697998

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:1.4660449028015

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.93465089797974

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.53479194641113

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.11849594116211

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.19132089614868

SELECT `p`.`id`, `p`.`fecha_ini`, `p`.`fecha_fin`, `p`.`comentarios`, `p`.`reg`, `c`.`empresa`, `s`.`servicio`, `u`.`Usuario`, `p`.`id_cliente`
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1
ORDER BY `p`.`id` ASC
 LIMIT 50 
 Execution Time:0.14091897010803

SELECT COUNT(1) as total
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1 
 Execution Time:0.085191011428833

SELECT `p`.`id`, `p`.`fecha_ini`, `p`.`fecha_fin`, `p`.`comentarios`, `p`.`reg`, `c`.`empresa`, `s`.`servicio`, `u`.`Usuario`, `p`.`id_cliente`
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1
ORDER BY `p`.`id` ASC
 LIMIT 50 
 Execution Time:0.087306976318359

SELECT COUNT(1) as total
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1 
 Execution Time:0.10931015014648

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.080468893051147

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.10963702201843

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.097701072692871

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.2000560760498

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.11796903610229

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.16628503799438

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.13032102584839

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.087380170822144

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.12810492515564

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.18608093261719

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.085168123245239

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.090386867523193

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.22041296958923

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.18068313598633

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.081681966781616

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.092679977416992

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.095457077026367

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.092111110687256

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.080859899520874

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.097784996032715

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.084579944610596

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.084089994430542

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081618070602417

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.082318067550659

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.15052580833435

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.12615990638733

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0' 
 Execution Time:0.1065080165863

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '1'
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.088060140609741

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '1' 
 Execution Time:0.082319974899292

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.088974952697754

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0' 
 Execution Time:0.081921100616455

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `zona` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `nombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `mp` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `propuesta` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `recursos` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `tiempo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `horas_hombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `days` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `grupo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_ini` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `seguimiento` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.092753887176514

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `zona` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `nombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `mp` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `propuesta` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `recursos` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `tiempo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `horas_hombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `days` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `grupo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_ini` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
 ) 
 Execution Time:0.092448949813843

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '1'
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.10068702697754

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '1' 
 Execution Time:0.10104703903198

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '1'
AND   (
`id` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `zona` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `nombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `mp` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `propuesta` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `recursos` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `tiempo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `horas_hombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `days` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `grupo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_ini` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `seguimiento` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.082540035247803

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '1'
AND   (
`id` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `zona` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `nombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `mp` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `propuesta` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `recursos` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `tiempo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `horas_hombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `days` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `grupo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_ini` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
 ) 
 Execution Time:0.081313848495483

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.093371152877808

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0' 
 Execution Time:0.084905862808228

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `zona` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `nombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `mp` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `propuesta` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `recursos` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `tiempo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `horas_hombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `days` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `grupo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_ini` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `seguimiento` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.13652896881104

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `zona` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `nombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `mp` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `propuesta` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `recursos` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `tiempo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `horas_hombre` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `days` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `grupo` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
OR  `fecha_ini` LIKE '%Limpieza de PBA Y PBS%' ESCAPE '!'
 ) 
 Execution Time:0.085685968399048

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.085358858108521

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0' 
 Execution Time:0.13780188560486

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%l%' ESCAPE '!'
OR  `zona` LIKE '%l%' ESCAPE '!'
OR  `nombre` LIKE '%l%' ESCAPE '!'
OR  `mp` LIKE '%l%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%l%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%l%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%l%' ESCAPE '!'
OR  `propuesta` LIKE '%l%' ESCAPE '!'
OR  `recursos` LIKE '%l%' ESCAPE '!'
OR  `tiempo` LIKE '%l%' ESCAPE '!'
OR  `horas_hombre` LIKE '%l%' ESCAPE '!'
OR  `days` LIKE '%l%' ESCAPE '!'
OR  `grupo` LIKE '%l%' ESCAPE '!'
OR  `fecha_ini` LIKE '%l%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%l%' ESCAPE '!'
OR  `seguimiento` LIKE '%l%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.14477300643921

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%l%' ESCAPE '!'
OR  `zona` LIKE '%l%' ESCAPE '!'
OR  `nombre` LIKE '%l%' ESCAPE '!'
OR  `mp` LIKE '%l%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%l%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%l%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%l%' ESCAPE '!'
OR  `propuesta` LIKE '%l%' ESCAPE '!'
OR  `recursos` LIKE '%l%' ESCAPE '!'
OR  `tiempo` LIKE '%l%' ESCAPE '!'
OR  `horas_hombre` LIKE '%l%' ESCAPE '!'
OR  `days` LIKE '%l%' ESCAPE '!'
OR  `grupo` LIKE '%l%' ESCAPE '!'
OR  `fecha_ini` LIKE '%l%' ESCAPE '!'
 ) 
 Execution Time:0.38990211486816

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%lim%' ESCAPE '!'
OR  `zona` LIKE '%lim%' ESCAPE '!'
OR  `nombre` LIKE '%lim%' ESCAPE '!'
OR  `mp` LIKE '%lim%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%lim%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%lim%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%lim%' ESCAPE '!'
OR  `propuesta` LIKE '%lim%' ESCAPE '!'
OR  `recursos` LIKE '%lim%' ESCAPE '!'
OR  `tiempo` LIKE '%lim%' ESCAPE '!'
OR  `horas_hombre` LIKE '%lim%' ESCAPE '!'
OR  `days` LIKE '%lim%' ESCAPE '!'
OR  `grupo` LIKE '%lim%' ESCAPE '!'
OR  `fecha_ini` LIKE '%lim%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%lim%' ESCAPE '!'
OR  `seguimiento` LIKE '%lim%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.11051082611084

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%lim%' ESCAPE '!'
OR  `zona` LIKE '%lim%' ESCAPE '!'
OR  `nombre` LIKE '%lim%' ESCAPE '!'
OR  `mp` LIKE '%lim%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%lim%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%lim%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%lim%' ESCAPE '!'
OR  `propuesta` LIKE '%lim%' ESCAPE '!'
OR  `recursos` LIKE '%lim%' ESCAPE '!'
OR  `tiempo` LIKE '%lim%' ESCAPE '!'
OR  `horas_hombre` LIKE '%lim%' ESCAPE '!'
OR  `days` LIKE '%lim%' ESCAPE '!'
OR  `grupo` LIKE '%lim%' ESCAPE '!'
OR  `fecha_ini` LIKE '%lim%' ESCAPE '!'
 ) 
 Execution Time:0.15756702423096

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limp%' ESCAPE '!'
OR  `zona` LIKE '%limp%' ESCAPE '!'
OR  `nombre` LIKE '%limp%' ESCAPE '!'
OR  `mp` LIKE '%limp%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limp%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limp%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limp%' ESCAPE '!'
OR  `propuesta` LIKE '%limp%' ESCAPE '!'
OR  `recursos` LIKE '%limp%' ESCAPE '!'
OR  `tiempo` LIKE '%limp%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limp%' ESCAPE '!'
OR  `days` LIKE '%limp%' ESCAPE '!'
OR  `grupo` LIKE '%limp%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limp%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%limp%' ESCAPE '!'
OR  `seguimiento` LIKE '%limp%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.14392900466919

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limp%' ESCAPE '!'
OR  `zona` LIKE '%limp%' ESCAPE '!'
OR  `nombre` LIKE '%limp%' ESCAPE '!'
OR  `mp` LIKE '%limp%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limp%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limp%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limp%' ESCAPE '!'
OR  `propuesta` LIKE '%limp%' ESCAPE '!'
OR  `recursos` LIKE '%limp%' ESCAPE '!'
OR  `tiempo` LIKE '%limp%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limp%' ESCAPE '!'
OR  `days` LIKE '%limp%' ESCAPE '!'
OR  `grupo` LIKE '%limp%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limp%' ESCAPE '!'
 ) 
 Execution Time:0.15040302276611

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpi%' ESCAPE '!'
OR  `zona` LIKE '%limpi%' ESCAPE '!'
OR  `nombre` LIKE '%limpi%' ESCAPE '!'
OR  `mp` LIKE '%limpi%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpi%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpi%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpi%' ESCAPE '!'
OR  `propuesta` LIKE '%limpi%' ESCAPE '!'
OR  `recursos` LIKE '%limpi%' ESCAPE '!'
OR  `tiempo` LIKE '%limpi%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpi%' ESCAPE '!'
OR  `days` LIKE '%limpi%' ESCAPE '!'
OR  `grupo` LIKE '%limpi%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpi%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%limpi%' ESCAPE '!'
OR  `seguimiento` LIKE '%limpi%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.10565996170044

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpi%' ESCAPE '!'
OR  `zona` LIKE '%limpi%' ESCAPE '!'
OR  `nombre` LIKE '%limpi%' ESCAPE '!'
OR  `mp` LIKE '%limpi%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpi%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpi%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpi%' ESCAPE '!'
OR  `propuesta` LIKE '%limpi%' ESCAPE '!'
OR  `recursos` LIKE '%limpi%' ESCAPE '!'
OR  `tiempo` LIKE '%limpi%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpi%' ESCAPE '!'
OR  `days` LIKE '%limpi%' ESCAPE '!'
OR  `grupo` LIKE '%limpi%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpi%' ESCAPE '!'
 ) 
 Execution Time:0.10447096824646

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpie%' ESCAPE '!'
OR  `zona` LIKE '%limpie%' ESCAPE '!'
OR  `nombre` LIKE '%limpie%' ESCAPE '!'
OR  `mp` LIKE '%limpie%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpie%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpie%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpie%' ESCAPE '!'
OR  `propuesta` LIKE '%limpie%' ESCAPE '!'
OR  `recursos` LIKE '%limpie%' ESCAPE '!'
OR  `tiempo` LIKE '%limpie%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpie%' ESCAPE '!'
OR  `days` LIKE '%limpie%' ESCAPE '!'
OR  `grupo` LIKE '%limpie%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpie%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%limpie%' ESCAPE '!'
OR  `seguimiento` LIKE '%limpie%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.090898036956787

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpie%' ESCAPE '!'
OR  `zona` LIKE '%limpie%' ESCAPE '!'
OR  `nombre` LIKE '%limpie%' ESCAPE '!'
OR  `mp` LIKE '%limpie%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpie%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpie%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpie%' ESCAPE '!'
OR  `propuesta` LIKE '%limpie%' ESCAPE '!'
OR  `recursos` LIKE '%limpie%' ESCAPE '!'
OR  `tiempo` LIKE '%limpie%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpie%' ESCAPE '!'
OR  `days` LIKE '%limpie%' ESCAPE '!'
OR  `grupo` LIKE '%limpie%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpie%' ESCAPE '!'
 ) 
 Execution Time:0.089431047439575

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza %' ESCAPE '!'
OR  `zona` LIKE '%limpieza %' ESCAPE '!'
OR  `nombre` LIKE '%limpieza %' ESCAPE '!'
OR  `mp` LIKE '%limpieza %' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza %' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza %' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza %' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza %' ESCAPE '!'
OR  `recursos` LIKE '%limpieza %' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza %' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza %' ESCAPE '!'
OR  `days` LIKE '%limpieza %' ESCAPE '!'
OR  `grupo` LIKE '%limpieza %' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza %' ESCAPE '!'
OR  `fecha_realiza` LIKE '%limpieza %' ESCAPE '!'
OR  `seguimiento` LIKE '%limpieza %' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.095367908477783

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza %' ESCAPE '!'
OR  `zona` LIKE '%limpieza %' ESCAPE '!'
OR  `nombre` LIKE '%limpieza %' ESCAPE '!'
OR  `mp` LIKE '%limpieza %' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza %' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza %' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza %' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza %' ESCAPE '!'
OR  `recursos` LIKE '%limpieza %' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza %' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza %' ESCAPE '!'
OR  `days` LIKE '%limpieza %' ESCAPE '!'
OR  `grupo` LIKE '%limpieza %' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza %' ESCAPE '!'
 ) 
 Execution Time:0.097872018814087

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza d%' ESCAPE '!'
OR  `zona` LIKE '%limpieza d%' ESCAPE '!'
OR  `nombre` LIKE '%limpieza d%' ESCAPE '!'
OR  `mp` LIKE '%limpieza d%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza d%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza d%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza d%' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza d%' ESCAPE '!'
OR  `recursos` LIKE '%limpieza d%' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza d%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza d%' ESCAPE '!'
OR  `days` LIKE '%limpieza d%' ESCAPE '!'
OR  `grupo` LIKE '%limpieza d%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza d%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%limpieza d%' ESCAPE '!'
OR  `seguimiento` LIKE '%limpieza d%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.11934089660645

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza d%' ESCAPE '!'
OR  `zona` LIKE '%limpieza d%' ESCAPE '!'
OR  `nombre` LIKE '%limpieza d%' ESCAPE '!'
OR  `mp` LIKE '%limpieza d%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza d%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza d%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza d%' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza d%' ESCAPE '!'
OR  `recursos` LIKE '%limpieza d%' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza d%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza d%' ESCAPE '!'
OR  `days` LIKE '%limpieza d%' ESCAPE '!'
OR  `grupo` LIKE '%limpieza d%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza d%' ESCAPE '!'
 ) 
 Execution Time:0.092623949050903

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza de%' ESCAPE '!'
OR  `zona` LIKE '%limpieza de%' ESCAPE '!'
OR  `nombre` LIKE '%limpieza de%' ESCAPE '!'
OR  `mp` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza de%' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza de%' ESCAPE '!'
OR  `recursos` LIKE '%limpieza de%' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza de%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza de%' ESCAPE '!'
OR  `days` LIKE '%limpieza de%' ESCAPE '!'
OR  `grupo` LIKE '%limpieza de%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza de%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%limpieza de%' ESCAPE '!'
OR  `seguimiento` LIKE '%limpieza de%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.12527704238892

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza de%' ESCAPE '!'
OR  `zona` LIKE '%limpieza de%' ESCAPE '!'
OR  `nombre` LIKE '%limpieza de%' ESCAPE '!'
OR  `mp` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza de%' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza de%' ESCAPE '!'
OR  `recursos` LIKE '%limpieza de%' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza de%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza de%' ESCAPE '!'
OR  `days` LIKE '%limpieza de%' ESCAPE '!'
OR  `grupo` LIKE '%limpieza de%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza de%' ESCAPE '!'
 ) 
 Execution Time:0.12542796134949

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza de %' ESCAPE '!'
OR  `zona` LIKE '%limpieza de %' ESCAPE '!'
OR  `nombre` LIKE '%limpieza de %' ESCAPE '!'
OR  `mp` LIKE '%limpieza de %' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza de %' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza de %' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza de %' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza de %' ESCAPE '!'
OR  `recursos` LIKE '%limpieza de %' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza de %' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza de %' ESCAPE '!'
OR  `days` LIKE '%limpieza de %' ESCAPE '!'
OR  `grupo` LIKE '%limpieza de %' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza de %' ESCAPE '!'
OR  `fecha_realiza` LIKE '%limpieza de %' ESCAPE '!'
OR  `seguimiento` LIKE '%limpieza de %' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.09293794631958

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza de %' ESCAPE '!'
OR  `zona` LIKE '%limpieza de %' ESCAPE '!'
OR  `nombre` LIKE '%limpieza de %' ESCAPE '!'
OR  `mp` LIKE '%limpieza de %' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza de %' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza de %' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza de %' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza de %' ESCAPE '!'
OR  `recursos` LIKE '%limpieza de %' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza de %' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza de %' ESCAPE '!'
OR  `days` LIKE '%limpieza de %' ESCAPE '!'
OR  `grupo` LIKE '%limpieza de %' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza de %' ESCAPE '!'
 ) 
 Execution Time:0.09237003326416

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza de%' ESCAPE '!'
OR  `zona` LIKE '%limpieza de%' ESCAPE '!'
OR  `nombre` LIKE '%limpieza de%' ESCAPE '!'
OR  `mp` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza de%' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza de%' ESCAPE '!'
OR  `recursos` LIKE '%limpieza de%' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza de%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza de%' ESCAPE '!'
OR  `days` LIKE '%limpieza de%' ESCAPE '!'
OR  `grupo` LIKE '%limpieza de%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza de%' ESCAPE '!'
OR  `fecha_realiza` LIKE '%limpieza de%' ESCAPE '!'
OR  `seguimiento` LIKE '%limpieza de%' ESCAPE '!'
 )
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.14300894737244

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
AND   (
`id` LIKE '%limpieza de%' ESCAPE '!'
OR  `zona` LIKE '%limpieza de%' ESCAPE '!'
OR  `nombre` LIKE '%limpieza de%' ESCAPE '!'
OR  `mp` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_cotiza` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_modi1` LIKE '%limpieza de%' ESCAPE '!'
OR  `frecuencia_modi2` LIKE '%limpieza de%' ESCAPE '!'
OR  `propuesta` LIKE '%limpieza de%' ESCAPE '!'
OR  `recursos` LIKE '%limpieza de%' ESCAPE '!'
OR  `tiempo` LIKE '%limpieza de%' ESCAPE '!'
OR  `horas_hombre` LIKE '%limpieza de%' ESCAPE '!'
OR  `days` LIKE '%limpieza de%' ESCAPE '!'
OR  `grupo` LIKE '%limpieza de%' ESCAPE '!'
OR  `fecha_ini` LIKE '%limpieza de%' ESCAPE '!'
 ) 
 Execution Time:0.1026451587677

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.10589694976807

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.080629825592041

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.08955717086792

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.1299250125885

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.081806898117065

SELECT `p`.*, `s`.`servicio`, date_format(p.fecha_ini, "%m") as mes_ini, date_format(p.fecha_fin, "%m") as mes_fin, date_format(p.fecha_ini, "%Y") as anio_ini, date_format(p.fecha_fin, "%Y") as anio_fin
FROM `proyectos` `p`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
WHERE `p`.`estatus` = 1
AND `id_cliente` IS NULL 
 Execution Time:0.086158037185669

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.10657000541687

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.12564086914062

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.079754114151001

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.1171338558197

SELECT `p`.`id`, `p`.`fecha_ini`, `p`.`fecha_fin`, `p`.`comentarios`, `p`.`reg`, `c`.`empresa`, `s`.`servicio`, `u`.`Usuario`, `p`.`id_cliente`
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1
ORDER BY `p`.`id` ASC
 LIMIT 50 
 Execution Time:0.079658031463623

SELECT COUNT(1) as total
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1 
 Execution Time:0.084443092346191

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.080593109130859

SELECT `p`.`id`, `p`.`fecha_ini`, `p`.`fecha_fin`, `p`.`comentarios`, `p`.`reg`, `c`.`empresa`, `s`.`servicio`, `u`.`Usuario`, `p`.`id_cliente`
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1
ORDER BY `p`.`id` ASC
 LIMIT 50 
 Execution Time:0.083362102508545

SELECT COUNT(1) as total
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1 
 Execution Time:0.082356929779053

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.095779895782471

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.10328602790833

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.10811400413513

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.083984136581421

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.084054946899414

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.079638004302979

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.09959602355957

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081414937973022

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.080307006835938

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.085591077804565

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.080601930618286

SELECT `id`, `zona`, `nombre`, `mp`, `frecuencia_cotiza`, `frecuencia_modi1`, `frecuencia_modi2`, `propuesta`, `recursos`, `tiempo`, `horas_hombre`, `days`, `grupo`, `fecha_ini`, `fecha_realiza`, `seguimiento`
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0'
ORDER BY `id` DESC
 LIMIT 10 
 Execution Time:0.085515022277832

SELECT COUNT(1) as total
FROM `limpieza_actividades`
WHERE `estatus` = 1
AND `id_cliente` = '18'
AND `critica` = '0' 
 Execution Time:0.082084894180298

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.089260101318359

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.082780122756958

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.080010890960693

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.079298973083496

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081140995025635

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.086938858032227

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.13539695739746

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.089453935623169

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.15602898597717

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.6886420249939

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.16014385223389

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.22470188140869

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.17785215377808

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.17580914497375

SELECT `ep`.`id`, `ep`.`id_proyecto`, `ep`.`id_empleado`, `ep`.`tipo_empleado`, `p`.`nombre`, `ep`.`fecha_asignacion`
FROM `empleados_proyecto` `ep`
JOIN `personal` `p` ON `p`.`personalId`=`ep`.`id_empleado` and `p`.`estatus`=1
WHERE `ep`.`id_proyecto` = '36'
AND `ep`.`estatus` = 1
GROUP BY `ep`.`id_empleado` 
 Execution Time:0.10417699813843

SELECT `ep`.`id`, `ep`.`id_proyecto`, `ep`.`id_empleado`, `ep`.`tipo_empleado`, `p`.`nombre`, `ep`.`fecha_asignacion`
FROM `empleados_proyecto` `ep`
JOIN `personal` `p` ON `p`.`personalId`=`ep`.`id_empleado` and `p`.`estatus`=1
WHERE `ep`.`id_proyecto` = '36'
AND `ep`.`estatus` = 1
AND `tipo_empleado` = 2
GROUP BY `ep`.`id_empleado` 
 Execution Time:0.085029125213623

SELECT `la`.`id`, `la`.`nombre`, 1 as `tipo`
FROM `limpieza_actividades` `la`
WHERE `la`.`id_proyecto` = '36'
AND `la`.`estatus` = 1 UNION SELECT `la2`.`id`, `la2`.`nombre`, 2 as `tipo`
FROM `limpieza_actividades_critica` `la2`
WHERE `la2`.`id_proyecto` = '36'
AND `la2`.`estatus` = 1 
 Execution Time:0.081732988357544

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.079967021942139

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.093132019042969

SELECT *
FROM `ordenes_semanal`
WHERE `id` = '61801' 
 Execution Time:0.083157062530518

SELECT `oe`.`id`, `oe`.`id_proyecto`, `oe`.`id_empleado`, `p`.`nombre`
FROM `ordenes_empleado` `oe`
JOIN `personal` `p` ON `p`.`personalId`=`oe`.`id_empleado` and `p`.`estatus`=1
WHERE `oe`.`id_orden` = '61801'
AND `oe`.`estatus` = 1 
 Execution Time:0.081664085388184

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.083105802536011

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.08127498626709

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.084762096405029

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081476926803589

SELECT `ep`.`id`, `ep`.`id_proyecto`, `ep`.`id_empleado`, `ep`.`tipo_empleado`, `p`.`nombre`, `ep`.`fecha_asignacion`
FROM `empleados_proyecto` `ep`
JOIN `personal` `p` ON `p`.`personalId`=`ep`.`id_empleado` and `p`.`estatus`=1
WHERE `ep`.`id_proyecto` = '36'
AND `ep`.`estatus` = 1
GROUP BY `ep`.`id_empleado` 
 Execution Time:0.10488295555115

SELECT `ep`.`id`, `ep`.`id_proyecto`, `ep`.`id_empleado`, `ep`.`tipo_empleado`, `p`.`nombre`, `ep`.`fecha_asignacion`
FROM `empleados_proyecto` `ep`
JOIN `personal` `p` ON `p`.`personalId`=`ep`.`id_empleado` and `p`.`estatus`=1
WHERE `ep`.`id_proyecto` = '36'
AND `ep`.`estatus` = 1
AND `tipo_empleado` = 2
GROUP BY `ep`.`id_empleado` 
 Execution Time:0.12897300720215

SELECT `la`.`id`, `la`.`nombre`, 1 as `tipo`
FROM `limpieza_actividades` `la`
WHERE `la`.`id_proyecto` = '36'
AND `la`.`estatus` = 1 UNION SELECT `la2`.`id`, `la2`.`nombre`, 2 as `tipo`
FROM `limpieza_actividades_critica` `la2`
WHERE `la2`.`id_proyecto` = '36'
AND `la2`.`estatus` = 1 
 Execution Time:0.10389280319214

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.086360931396484

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.09111213684082

SELECT *
FROM `ordenes_semanal`
WHERE `id` = '61801' 
 Execution Time:0.081719875335693

SELECT `oe`.`id`, `oe`.`id_proyecto`, `oe`.`id_empleado`, `p`.`nombre`
FROM `ordenes_empleado` `oe`
JOIN `personal` `p` ON `p`.`personalId`=`oe`.`id_empleado` and `p`.`estatus`=1
WHERE `oe`.`id_orden` = '61801'
AND `oe`.`estatus` = 1 
 Execution Time:0.10184216499329

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.096712827682495

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.083969116210938

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081739902496338

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.080690860748291

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.081805944442749

SELECT `ep`.`id`, `ep`.`id_proyecto`, `ep`.`id_empleado`, `ep`.`tipo_empleado`, `p`.`nombre`, `ep`.`fecha_asignacion`
FROM `empleados_proyecto` `ep`
JOIN `personal` `p` ON `p`.`personalId`=`ep`.`id_empleado` and `p`.`estatus`=1
WHERE `ep`.`id_proyecto` = '36'
AND `ep`.`estatus` = 1
GROUP BY `ep`.`id_empleado` 
 Execution Time:0.088526010513306

SELECT `ep`.`id`, `ep`.`id_proyecto`, `ep`.`id_empleado`, `ep`.`tipo_empleado`, `p`.`nombre`, `ep`.`fecha_asignacion`
FROM `empleados_proyecto` `ep`
JOIN `personal` `p` ON `p`.`personalId`=`ep`.`id_empleado` and `p`.`estatus`=1
WHERE `ep`.`id_proyecto` = '36'
AND `ep`.`estatus` = 1
AND `tipo_empleado` = 2
GROUP BY `ep`.`id_empleado` 
 Execution Time:0.097559213638306

SELECT `la`.`id`, `la`.`nombre`, 1 as `tipo`
FROM `limpieza_actividades` `la`
WHERE `la`.`id_proyecto` = '36'
AND `la`.`estatus` = 1 UNION SELECT `la2`.`id`, `la2`.`nombre`, 2 as `tipo`
FROM `limpieza_actividades_critica` `la2`
WHERE `la2`.`id_proyecto` = '36'
AND `la2`.`estatus` = 1 
 Execution Time:0.086446046829224

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.13502287864685

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.09437894821167

SELECT *
FROM `ordenes_semanal`
WHERE `id` = '21759' 
 Execution Time:0.085927963256836

SELECT `oe`.`id`, `oe`.`id_proyecto`, `oe`.`id_empleado`, `p`.`nombre`
FROM `ordenes_empleado` `oe`
JOIN `personal` `p` ON `p`.`personalId`=`oe`.`id_empleado` and `p`.`estatus`=1
WHERE `oe`.`id_orden` = '21759'
AND `oe`.`estatus` = 1 
 Execution Time:0.097427129745483

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.082086801528931

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081127882003784

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.080557823181152

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.083679914474487

SELECT `ep`.`id`, `ep`.`id_proyecto`, `ep`.`id_empleado`, `ep`.`tipo_empleado`, `p`.`nombre`, `ep`.`fecha_asignacion`
FROM `empleados_proyecto` `ep`
JOIN `personal` `p` ON `p`.`personalId`=`ep`.`id_empleado` and `p`.`estatus`=1
WHERE `ep`.`id_proyecto` = '36'
AND `ep`.`estatus` = 1
GROUP BY `ep`.`id_empleado` 
 Execution Time:0.083927869796753

SELECT `ep`.`id`, `ep`.`id_proyecto`, `ep`.`id_empleado`, `ep`.`tipo_empleado`, `p`.`nombre`, `ep`.`fecha_asignacion`
FROM `empleados_proyecto` `ep`
JOIN `personal` `p` ON `p`.`personalId`=`ep`.`id_empleado` and `p`.`estatus`=1
WHERE `ep`.`id_proyecto` = '36'
AND `ep`.`estatus` = 1
AND `tipo_empleado` = 2
GROUP BY `ep`.`id_empleado` 
 Execution Time:0.08343505859375

SELECT `la`.`id`, `la`.`nombre`, 1 as `tipo`
FROM `limpieza_actividades` `la`
WHERE `la`.`id_proyecto` = '36'
AND `la`.`estatus` = 1 UNION SELECT `la2`.`id`, `la2`.`nombre`, 2 as `tipo`
FROM `limpieza_actividades_critica` `la2`
WHERE `la2`.`id_proyecto` = '36'
AND `la2`.`estatus` = 1 
 Execution Time:0.091654062271118

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.081360101699829

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.097637891769409

SELECT *
FROM `ordenes_semanal`
WHERE `id` = '21759' 
 Execution Time:0.083483934402466

SELECT `oe`.`id`, `oe`.`id_proyecto`, `oe`.`id_empleado`, `p`.`nombre`
FROM `ordenes_empleado` `oe`
JOIN `personal` `p` ON `p`.`personalId`=`oe`.`id_empleado` and `p`.`estatus`=1
WHERE `oe`.`id_orden` = '21759'
AND `oe`.`estatus` = 1 
 Execution Time:0.082832098007202

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.082128047943115

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.082946062088013

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081240892410278

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.082722902297974

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.0814528465271

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.1019811630249

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.07984185218811

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.10359716415405

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.086241960525513

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.077698945999146

SELECT `p`.`id`, `p`.`fecha_ini`, `p`.`fecha_fin`, `p`.`comentarios`, `p`.`reg`, `c`.`empresa`, `s`.`servicio`, `u`.`Usuario`, `p`.`id_cliente`
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1
ORDER BY `p`.`id` ASC
 LIMIT 50 
 Execution Time:0.083632946014404

SELECT COUNT(1) as total
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1 
 Execution Time:0.082246065139771

SELECT `p`.`id`, `p`.`fecha_ini`, `p`.`fecha_fin`, `p`.`comentarios`, `p`.`reg`, `c`.`empresa`, `s`.`servicio`, `u`.`Usuario`, `p`.`id_cliente`
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1
ORDER BY `p`.`id` ASC
 LIMIT 50 
 Execution Time:0.09117603302002

SELECT COUNT(1) as total
FROM `proyectos` `p`
JOIN `clientes` `c` ON `c`.`id`=`p`.`id_cliente`
JOIN `servicios` `s` ON `s`.`id`=`p`.`id_servicio`
JOIN `usuarios` `u` ON `u`.`personalId`=`p`.`id_user_reg`
WHERE `p`.`estatus` = 1 
 Execution Time:0.082427978515625

SELECT *
FROM `clientes`
WHERE `id` = '18' 
 Execution Time:0.085186958312988

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.086540937423706

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.082582950592041

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081376075744629

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.083405017852783

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.089945793151855

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.083807945251465

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.079204082489014

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.08470606803894

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.092255115509033

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.10085296630859

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.083563089370728

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.19896221160889

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.2082200050354

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`id_limpieza` = '278'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.12417697906494

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`id_limpieza` = '278'
AND `os`.`semana` = '35' 
 Execution Time:0.10752296447754

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.086521863937378

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.081881046295166

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.094680070877075

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.088573217391968

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.10639095306396

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.10039019584656

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`id_limpieza` = '278'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.11256718635559

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`id_limpieza` = '278'
AND `os`.`semana` = '35' 
 Execution Time:0.19986295700073

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.22844696044922

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.19109392166138

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.083284854888916

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.097599029541016

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.083114147186279

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.092288017272949

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.10308480262756

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.085312843322754

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.2214150428772

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.18390893936157

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.083441019058228

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.082776069641113

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.089545011520386

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.081912994384766

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.091275930404663

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.079982995986938

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.23202586174011

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.17488980293274

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.084688901901245

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.099776983261108

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.095750093460083

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.096637010574341

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.13019299507141

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.07925009727478

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.20748209953308

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.17471790313721

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%p%' ESCAPE '!'
OR  `os`.`id` LIKE '%p%' ESCAPE '!'
OR  `os`.`ot` LIKE '%p%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%p%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%p%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%p%' ESCAPE '!'
OR  `os`.`firma` LIKE '%p%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%p%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%p%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%p%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%p%' ESCAPE '!'
OR  `os`.`tipo_act` LIKE '%p%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%p%' ESCAPE '!'
OR  `os`.`semana` LIKE '%p%' ESCAPE '!'
 )
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.21291589736938

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%p%' ESCAPE '!'
OR  `os`.`id` LIKE '%p%' ESCAPE '!'
OR  `os`.`ot` LIKE '%p%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%p%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%p%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%p%' ESCAPE '!'
OR  `os`.`firma` LIKE '%p%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%p%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%p%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%p%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%p%' ESCAPE '!'
OR  `os`.`semana` LIKE '%p%' ESCAPE '!'
 ) 
 Execution Time:0.19948792457581

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%prep%' ESCAPE '!'
OR  `os`.`id` LIKE '%prep%' ESCAPE '!'
OR  `os`.`ot` LIKE '%prep%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%prep%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%prep%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%prep%' ESCAPE '!'
OR  `os`.`firma` LIKE '%prep%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%prep%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%prep%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prep%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prep%' ESCAPE '!'
OR  `os`.`tipo_act` LIKE '%prep%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%prep%' ESCAPE '!'
OR  `os`.`semana` LIKE '%prep%' ESCAPE '!'
 )
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.21130609512329

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%prep%' ESCAPE '!'
OR  `os`.`id` LIKE '%prep%' ESCAPE '!'
OR  `os`.`ot` LIKE '%prep%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%prep%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%prep%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%prep%' ESCAPE '!'
OR  `os`.`firma` LIKE '%prep%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%prep%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%prep%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prep%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%prep%' ESCAPE '!'
OR  `os`.`semana` LIKE '%prep%' ESCAPE '!'
 ) 
 Execution Time:0.19854283332825

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`id` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`ot` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`firma` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%prepa%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%prepa%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepa%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`tipo_act` LIKE '%prepa%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`semana` LIKE '%prepa%' ESCAPE '!'
 )
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.23729491233826

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`id` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`ot` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`firma` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%prepa%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%prepa%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepa%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%prepa%' ESCAPE '!'
OR  `os`.`semana` LIKE '%prepa%' ESCAPE '!'
 ) 
 Execution Time:0.21694898605347

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`id` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`ot` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`firma` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%prepar%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%prepar%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepar%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`tipo_act` LIKE '%prepar%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`semana` LIKE '%prepar%' ESCAPE '!'
 )
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.19282793998718

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`id` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`ot` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`firma` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%prepar%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%prepar%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepar%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%prepar%' ESCAPE '!'
OR  `os`.`semana` LIKE '%prepar%' ESCAPE '!'
 ) 
 Execution Time:0.20944094657898

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`id` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`ot` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`firma` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%prepara%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%prepara%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepara%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`tipo_act` LIKE '%prepara%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`semana` LIKE '%prepara%' ESCAPE '!'
 )
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.20234990119934

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
AND   (
`os`.`id` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`id` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`ot` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`id_limpieza` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`fecha` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`realiza` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`firma` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`firma_cliente` LIKE '%prepara%' ESCAPE '!'
OR  `c`.`empresa` LIKE '%prepara%' ESCAPE '!'
OR  `la`.`nombre` LIKE '%prepara%' ESCAPE '!'
OR  `p`.`nombre` LIKE '%prepara%' ESCAPE '!'
OR  `os`.`semana` LIKE '%prepara%' ESCAPE '!'
 ) 
 Execution Time:0.20917415618896

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 10 
 Execution Time:0.19893479347229

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.1621789932251

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50 
 Execution Time:0.2011981010437

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.22366285324097

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50, 50 
 Execution Time:0.22315001487732

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.18252897262573

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.1735680103302

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.080853939056396

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.087454080581665

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.098474025726318

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.085204839706421

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.1187698841095

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50, 50 
 Execution Time:0.5690279006958

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.24175000190735

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50 
 Execution Time:0.25524711608887

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.28816199302673

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50, 50 
 Execution Time:0.21163988113403

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.19538593292236

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.10522818565369

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.092260837554932

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.10384702682495

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.12248802185059

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.45256304740906

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.086899995803833

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50, 50 
 Execution Time:0.22712707519531

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.21505188941956

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50 
 Execution Time:0.22291803359985

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.17979717254639

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50, 50 
 Execution Time:0.20958995819092

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.18112707138062

SELECT `la`.*, date_format(la.fecha_ini, "%m") as mes
FROM `limpieza_actividades` `la`
WHERE `la`.`estatus` = 1
AND `la`.`id_cliente` = '18'
ORDER BY `la`.`nombre` ASC 
 Execution Time:0.24684309959412

SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='1' ORDER BY men.orden ASC 
 Execution Time:0.13993501663208

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='1' ORDER BY menus.MenusubId ASC 
 Execution Time:0.1012270450592

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='2' ORDER BY menus.MenusubId ASC 
 Execution Time:0.08575701713562

SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='1' AND menus.tipo='1' and menus.MenuId='4' ORDER BY menus.MenusubId ASC 
 Execution Time:0.085402011871338

SELECT *
FROM `bitacora_correo`
WHERE `fecha` = '2024-09-09' 
 Execution Time:0.082282066345215

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
LEFT JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50, 50 
 Execution Time:0.22503995895386

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.26621890068054

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
LEFT JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50 
 Execution Time:0.25474286079407

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.48638916015625

SELECT `os`.`id`, `os`.`id`, `os`.`ot`, `os`.`id_limpieza`, `os`.`fecha`, `os`.`realiza`, `os`.`firma`, `os`.`firma_cliente`, `c`.`empresa`, `la`.`nombre`, `os`.`tipo_act`, `os`.`seguimiento`, `p`.`nombre` as `per_name`, `os`.`semana`
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
LEFT JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35'
ORDER BY `os`.`id` DESC
 LIMIT 50, 50 
 Execution Time:0.28929996490479

SELECT COUNT(1) as total
FROM `ordenes_semanal` `os`
JOIN `clientes` `c` ON `c`.`id`=`os`.`id_cliente`
JOIN `personal` `p` ON `p`.`personalId`=`os`.`id_user_reg`
JOIN `limpieza_actividades` `la` ON `la`.`id`=`os`.`id_limpieza` and `la`.`estatus`=1
WHERE `os`.`estatus` = 1
AND `os`.`id_cliente` = '18'
AND DATE_FORMAT(os.fecha,'%Y') = 2024
AND `os`.`turno` = '1'
AND `os`.`id_proyecto` = '36'
AND `os`.`semana` = '35' 
 Execution Time:0.16122388839722

