<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloactividadesrealizadas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function proyecto($id){
        $sql = "SELECT 
                    pro.id,
                    cli.empresa,
                    ser.servicio,
                    pro.id_cliente,
                    cli.correo,
                    pro.fecha_ini,
                    pro.fecha_fin
                FROM proyectos as pro 
                INNER JOIN clientes as cli on cli.id=pro.id_cliente 
                INNER JOIN servicios as ser on ser.id=pro.id_servicio 
                WHERE pro.id=$id";
        $query = $this->db->query($sql);
        return $query;
    }

    
    function get_result($params){
        $proyecto=$params['proyecto'];
        $columns = array( 
            0=>'ar.id',
            1=>'ar.descripcion',
            2=>'ar.fecha',
            3=>'ar.horas',
            4=>'d.departamento',
            5=>'ar.file1',
            6=>'ar.file2',
            7=>'ar.file3'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('actividades_realizadas ar');
        $this->db->join('departamentos d','d.id=ar.departamento');
            
            $where = array('ar.activo'=>1,'ar.idproyecto'=>$proyecto);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $proyecto=$params['proyecto'];
        $columns = array( 
            0=>'ar.id',
            1=>'ar.descripcion',
            2=>'ar.fecha',
            3=>'ar.horas',
            4=>'d.departamento',
            5=>'ar.file1',
            6=>'ar.file2',
            7=>'ar.file3'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('actividades_realizadas ar');
        $this->db->join('departamentos d','d.id=ar.departamento');
            
            $where = array('ar.activo'=>1,'ar.idproyecto'=>$proyecto);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_result_cli($params){
        $proyecto=$params['proyecto'];
        $dep=$params['dep'];
        $columns = array( 
            0=>'id',
            1=>'id_cliente',
            2=>'nombre',
            3=>'fecha',
            4=>'fecha_formateada',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('vista_actividades_realizadas');
        $this->db->where(array('id'=>$proyecto));
        if($dep>0){
            $this->db->where(array('departamento'=>$dep));
        }
        $this->db->group_by("fecha_formateada");
            
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result_cli($params){
        $proyecto=$params['proyecto'];
        $dep=$params['dep'];
        $columns = array( 
            0=>'id',
            1=>'id_cliente',
            2=>'nombre',
            3=>'fecha',
            4=>'fecha_formateada',
        );

        $this->db->select('COUNT(*) as total');
        $this->db->from('vista_actividades_realizadas');
        $this->db->where(array('id'=>$proyecto));
        if($dep>0){
            $this->db->where(array('departamento'=>$dep));
        }
        $this->db->group_by("fecha_formateada");


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getActividades($id_proy,$where){
        $this->db->select("ard.*,d.departamento");
        $this->db->from("actividades_realizadas_des ard");
        $this->db->join("departamentos d","d.id=ard.dg_dep");
        $this->db->where("ard.activo",1);
        $this->db->where("ard.proyecto",$id_proy);
        if($where!=""){
            $this->db->where($where);  
        }
        $query=$this->db->get();
        return $query->result();
    }
    
}