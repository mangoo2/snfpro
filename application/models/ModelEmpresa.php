<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelEmpresa extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'e.id',
            1=>'e.alias',
            2=>'e.razon_social',
            3=>'e.tipo',
            4=>'e.foto',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('empresa e');
        $where = array('e.activo'=>$params['tipo_registro']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_result($params){
        $columns = array( 
            0=>'e.id',
            1=>'e.alias',
            2=>'e.razon_social',
            3=>'e.tipo',
            4=>'e.tipo',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('empresa e');
        $where = array('e.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getDatosCPEstado_gruop($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        $this->db->group_by("ep.codigo");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDatosCPEstado($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        $query=$this->db->get();
        return $query->result();
    }

}