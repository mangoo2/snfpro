<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProyectos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.fecha_ini',
            2=>'p.fecha_fin',
            3=>'p.comentarios',
            4=>'p.reg',
            5=>'c.empresa',
            6=>'s.servicio',
            7=>'u.Usuario',
            8=>'p.id_cliente',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('proyectos p');
        $this->db->join('clientes c','c.id=p.id_cliente');
        $this->db->join('servicios s','s.id=p.id_servicio');
        $this->db->join('usuarios u','u.personalId=p.id_user_reg');
        $this->db->where("p.estatus",1);
        if($this->session->userdata("usuarioid_tz")!=1){ // != de superadmin
            /*if($this->session->userdata("perfilid_tz")==1){ //perfil admin
                $this->db->where("p.id_user_reg",$this->session->userdata("idpersonal_tz")); 
            }else if($this->session->userdata("perfilid_tz")==2){ //perfil operador
                $this->db->where("p.id_user_reg",$this->session->userdata("personalId_reg"));
            }*/
            $this->db->where("p.id_cliente",$this->session->userdata("empresa"));  //solo por si es el usuario perteneciente a cliente, comentar lo de arriba y descomentar esta linea
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.fecha_ini',
            2=>'p.fecha_fin',
            3=>'p.comentarios',
            4=>'p.reg',
            5=>'c.empresa',
            6=>'s.servicio',
            7=>'u.Usuario',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('proyectos p');
        $this->db->join('clientes c','c.id=p.id_cliente');
        $this->db->join('servicios s','s.id=p.id_servicio');
        $this->db->join('usuarios u','u.personalId=p.id_user_reg');
        $this->db->where("p.estatus",1);

        if($this->session->userdata("usuarioid_tz")!=1){ // != de superadmin
            /*if($this->session->userdata("perfilid_tz")==1){ //perfil admin
                $this->db->where("p.id_user_reg",$this->session->userdata("idpersonal_tz")); 
            }else if($this->session->userdata("perfilid_tz")==2){ //perfil operador
                $this->db->where("p.id_user_reg",$this->session->userdata("personalId_reg")); 
            }*/
            $this->db->where("p.id_cliente",$this->session->userdata("empresa"));  //solo por si es el usuario perteneciente a cliente, comentar lo de arriba y descomentar esta linea
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function getProyecto($id){
        $columns = array( 
            0=>'p.id',
            1=>'p.fecha_ini',
            2=>'p.fecha_fin',
            3=>'p.comentarios',
            4=>'p.reg',
            5=>'c.empresa',
            6=>'s.servicio',
            7=>'u.Usuario',
            8=>'p.id_cliente',
            9=>'p.id_servicio',
            10=>'s.descripcion',
            11=>'p.turno',
            12=>'p.turno2',
            13=>'p.turno3',
            14=>'p.dias_laborables'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('proyectos p');
        $this->db->join('clientes c','c.id=p.id_cliente');
        $this->db->join('servicios s','s.id=p.id_servicio');
        //$this->db->join('usuarios u','u.UsuarioID=p.id_user_reg');
        $this->db->join('usuarios u','u.personalId=p.id_user_reg');
        $this->db->where("p.id",$id);

        $query=$this->db->get();
        return $query->row();
    }

    function getProyectoEmpleados($id,$where){
        $columns = array( 
            0=>'ep.id',
            1=>'ep.id_proyecto',
            2=>'ep.id_empleado',
            3=>'ep.tipo_empleado',
            4=>'p.nombre',
            5=>'ep.fecha_asignacion'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('empleados_proyecto ep');
        $this->db->join('personal p','p.personalId=ep.id_empleado and p.estatus=1');
        $this->db->where("ep.id_proyecto",$id);
        $this->db->where("ep.estatus",1);
        $this->db->group_by("ep.id_empleado");
        if($where!=0){
            $this->db->where($where);   
        }

        $query=$this->db->get();
        return $query->result();
    }

    /* ******************ACTIVIDADES DE LIMPIEZA****************************/
    function getDataLimpieza($params){
        $columns = array( 
            0=>'id',
            1=>'zona',
            2=>'nombre',
            3=>'mp',
            4=>'frecuencia_cotiza',
            5=>'frecuencia_modi1',
            6=>'frecuencia_modi2',
            7=>'propuesta',
            8=>'recursos',
            9=>'tiempo',
            10=>'horas_hombre',
            11=>'days',
            12=>'grupo',
            13=>'fecha_ini',
            14=>'fecha_realiza',
            15=>'seguimiento'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('limpieza_actividades');
        $this->db->where("estatus",1);
        $this->db->where("id_cliente",$params["id_cliente"]);
        $this->db->where("critica",$params["critica"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function getDataLimpieza_total($params){
        $columns = array( 
            0=>'id',
            1=>'zona',
            2=>'nombre',
            3=>'mp',
            4=>'frecuencia_cotiza',
            5=>'frecuencia_modi1',
            6=>'frecuencia_modi2',
            7=>'propuesta',
            8=>'recursos',
            9=>'tiempo',
            10=>'horas_hombre',
            11=>'days',
            12=>'grupo',
            13=>'fecha_ini',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('limpieza_actividades');
        $this->db->where("estatus",1);
        $this->db->where("id_cliente",$params["id_cliente"]);
        $this->db->where("critica",$params["critica"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /*************************************************/

    /* ******************ACTIVIDADES CRITICA DE LIMPIEZA****************************/
    function getDataLimpiezaCritica($params){
        $columns = array( 
            0=>'id',
            1=>'zona',
            2=>'nombre',
            3=>'gente',
            4=>'horas',
            5=>'frecuencia',
            6=>'turno'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('limpieza_actividades_critica');
        $this->db->where("estatus",1);
        $this->db->where("id_cliente",$params["id_cliente"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function getDataLimpiezaCritica_total($params){
        $columns = array( 
            0=>'id',
            1=>'zona',
            2=>'nombre',
            3=>'gente',
            4=>'horas',
            5=>'frecuencia',
            6=>'turno'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('limpieza_actividades_critica');
        $this->db->where("estatus",1);
        $this->db->where("id_cliente",$params["id_cliente"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    /* ******************ORDENES SEMANALES****************************/
    function getDataOrdenes($params){
        if( !empty($params['search']['value']) ) {
            $cols="la.nombre";
        }else{
            $cols="la.nombre as nombre_crit";
        }
        //if($params["tipo_act"]==0){ //normal
            $la="la.nombre";
        /*}else{
            $la="lac.nombre as nombre_crit";
        }*/
        $columns = array( 
            0=>'os.id',
            1=>'os.id',
            2=>'os.ot',
            3=>'os.id_limpieza',
            4=>'os.fecha',
            5=>'os.realiza',
            6=>'os.firma',
            7=>'os.firma_cliente',
            8=>'c.empresa',
            //9=>'la.nombre',
            10=>$la,
            11=>"os.tipo_act",
            12=>"os.seguimiento",
            13=>"p.nombre as per_name",
            14=>"os.semana",
            //15=>"IFNULL(GROUP_CONCAT(p.nombre SEPARATOR '<br>'),'') as pers_realiza"
        );
        $columns2 = array( 
            0=>'os.id',
            1=>'os.id',
            2=>'os.ot',
            3=>'os.id_limpieza',
            4=>'os.fecha',
            5=>'os.realiza',
            6=>'os.firma',
            7=>'os.firma_cliente',
            8=>'c.empresa',
            9=>'la.nombre',
            10=>$cols,
            11=>"os.tipo_act",
            12=>"p.nombre",
            13=>"os.semana"
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ordenes_semanal os');
        $this->db->join('clientes c','c.id=os.id_cliente');
        $this->db->join('personal p','p.personalId=os.id_user_reg');
        //$this->db->join('ordenes_empleado op','op.id_orden=os.id and op.estatus=1');
        //$this->db->join('personal p','p.personalId=op.id_empleado');
        //if($params["tipo_act"]==0){ //normal
            $this->db->join('limpieza_actividades la','la.id=os.id_limpieza and la.estatus=1',"left");
        /*}else{ //critica
            //$this->db->join('limpieza_actividades_critica lac','lac.id=os.id_limpieza and lac.estatus=1 and os.tipo_act=2');
            $this->db->join('limpieza_actividades lac','lac.id=os.id_limpieza and lac.estatus=1 and os.tipo_act=2');
        }*/
        
        $this->db->where("os.estatus",1);
        $this->db->where("os.id_cliente",$params["id_cliente"]);

        $anio = $params['anio'];
        $this->db->where("DATE_FORMAT(os.fecha,'%Y')=".$anio);
        $this->db->where("os.turno",$params["turno"]);
        $this->db->where("os.id_proyecto",$params["id_proy"]);
        if($params["tipo"]!="0"){
            $this->db->where("os.tipo",$params["tipo"]); //1=semanal, 2=diario
        }
        if($params["frecuencia"]!="0"){
            $this->db->where("la.frecuencia_cotiza",$params["frecuencia"]);
        }
        if($params["id_act"]!=0){
            $this->db->where("os.id_limpieza",$params["id_act"]); //actividad
        }
        
        if(intval($params["semana"])>9)
            $this->db->where("os.semana",$params["semana"]);
        else if(intval($params["semana"])<=9 && intval($params["semana"])>0)
            $this->db->where("os.semana","0".$params["semana"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function getDataOrdenes_total($params){
        //if($params["tipo_act"]==0){ //normal
            $la="la.nombre";
        /*}else{
            $la="lac.nombre as nombre_crit";
        }*/
        $columns = array( 
            0=>'os.id',
            1=>'os.id',
            2=>'os.ot',
            3=>'os.id_limpieza',
            4=>'os.fecha',
            5=>'os.realiza',
            6=>'os.firma',
            7=>'os.firma_cliente',
            8=>'c.empresa',
            //9=>'la.nombre',
            10=>$la,
            11=>"p.nombre",
            12=>"os.semana"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('ordenes_semanal os');
        $this->db->join('clientes c','c.id=os.id_cliente');
        $this->db->join('personal p','p.personalId=os.id_user_reg');
        //$this->db->join('ordenes_empleado op','op.id_orden=os.id and op.estatus=1');
        //$this->db->join('personal p','p.personalId=op.id_empleado');
        
        //if($params["tipo_act"]==0){ //normal
            $this->db->join('limpieza_actividades la','la.id=os.id_limpieza and la.estatus=1');
        /*}else{ //critica
            //$this->db->join('limpieza_actividades_critica lac','lac.id=os.id_limpieza and lac.estatus=1 and os.tipo_act=2');
            $this->db->join('limpieza_actividades lac','lac.id=os.id_limpieza and lac.estatus=1 and os.tipo_act=2');    
        }*/
        $this->db->where("os.estatus",1);
        $this->db->where("os.id_cliente",$params["id_cliente"]);
        
        $anio = $params['anio'];
        $this->db->where("DATE_FORMAT(os.fecha,'%Y')=".$anio);
        $this->db->where("os.turno",$params["turno"]);
        $this->db->where("os.id_proyecto",$params["id_proy"]);
        if($params["tipo"]!="0"){
            $this->db->where("os.tipo",$params["tipo"]); //1=semanal, 2=diario
        }
        if($params["frecuencia"]!="0"){
            $this->db->where("la.frecuencia_cotiza",$params["frecuencia"]);
        }
        if($params["id_act"]!=0){
            $this->db->where("os.id_limpieza",$params["id_act"]); //actividad
        }
        if(intval($params["semana"])>9)
            $this->db->where("os.semana",$params["semana"]);
        else if(intval($params["semana"])<=9 && intval($params["semana"])>0)
            $this->db->where("os.semana","0".$params["semana"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    /*function getActividadesLimpieza($id){
        $this->db->select("la.id, la.nombre, 1 as tipo"); //act dany 
        $this->db->from('limpieza_actividades la');
        $this->db->where("la.id_proyecto",$id);
        $this->db->where("la.estatus",1);
        $query = $this->db->get_compiled_select(); 

        $this->db->select("la2.id, la2.nombre, 2 as tipo"); //act dani
        $this->db->from('limpieza_actividades_critica la2');
        $this->db->where("la2.id_proyecto",$id);
        $this->db->where("la2.estatus",1);
        $query2 = $this->db->get_compiled_select();

        $queryunions=$query." UNION ".$query2;
        return $this->db->query($queryunions)->result();
    }*/

    function getActividadesLimpieza($id){
        $this->db->select("la.id, la.nombre, 1 as tipo"); 
        $this->db->from('limpieza_actividades la');
        $this->db->where("la.id_proyecto",$id);
        $this->db->where("la.estatus",1);
        $query = $this->db->get(); 
        return $query->result();
    }
    /*************************************************/

    function getActividadesDiario($fecha,$id_cli,$id_proy,$pormes){
        $this->db->select("o.id, o.tipo_act, o.turno, o.estatus, o.causas, la.nombre, lac.nombre as nombre_crit");
        $this->db->from('ordenes_diario o');
        $this->db->join('limpieza_actividades la','la.id=o.id_actividad and o.tipo_act=1','left');
        $this->db->join('limpieza_actividades_critica lac','lac.id=o.id_actividad and o.tipo_act=2','left');
        if($pormes=="")
            $this->db->where("o.fecha",$fecha);
        else
            $this->db->where("MONTH(o.fecha)",$pormes);
        $this->db->where("o.id_proyecto",$id_proy);
        $this->db->where("o.id_cliente",$id_cli);
        $this->db->where("o.activo",1);
        $query=$this->db->get();
        return $query->result();
    }

    function getActividadesDiario2($params){
        $this->db->select("o.id, o.tipo_act, o.turno, o.estatus, o.causas, o.fecha, la.nombre, lac.nombre as nombre_crit");
        $this->db->from('ordenes_diario o');
        $this->db->join('limpieza_actividades la','la.id=o.id_actividad and o.tipo_act=1','left');
        $this->db->join('limpieza_actividades_critica lac','lac.id=o.id_actividad and o.tipo_act=2','left');
        
        $this->db->where("o.turno",$params["turno"]);
        $this->db->where("DATE_FORMAT(o.fecha,'%Y')",$params["anio"]);
        $this->db->where("o.id_proyecto",$params["id_proy"]);
        $this->db->where("o.id_cliente",$params["id_cliente"]);
        $this->db->where("o.activo",1);
        $query=$this->db->get();
        return $query->result();
    }

    function getActividadesMes($id_cli,$id_proy,$pormes){
        /*$this->db->select('o.id, o.tipo_act, o.turno, o.estatus, la.nombre, lac.nombre as nombre_crit, date_format(o.fecha,"%d") as dia,
                        (select IFNULL(GROUP_CONCAT(o.estatus SEPARATOR "</th>"),0) as th_table) as th_table');*/
        $this->db->select('o.id, o.tipo_act, o.turno, o.estatus, o.causas, la.nombre, lac.nombre as nombre_crit, date_format(o.fecha,"%d") as dia');
        $this->db->from('ordenes_diario o');
        $this->db->join('limpieza_actividades la','la.id=o.id_actividad and o.tipo_act=1','left');
        $this->db->join('limpieza_actividades_critica lac','lac.id=o.id_actividad and o.tipo_act=2','left');

        $this->db->where("MONTH(o.fecha)",$pormes);
        $this->db->where("o.id_proyecto",$id_proy);
        $this->db->where("o.id_cliente",$id_cli);
        $this->db->where("o.activo",1);
        //$this->db->group_by('o.turno');
        //$this->db->group_by('o.tipo_act');
        $this->db->order_by('o.turno',"ASC");
        $this->db->order_by('o.fecha',"ASC");
        $query=$this->db->get();
        return $query->result();
    }

    /* **************************************************/
    /* ******************MATRIZ DE ACCIONES****************************/
    function getDataMatrizAcciones($params){
        $columns = array( 
            0=>'id',
            1=>'area',
            2=>'hallazgo',
            3=>'fecha_deteccion',
            4=>'falla',
            5=>'acciones',
            6=>'afectacion',
            7=>'criticidad',
            8=>'id_resp',
            9=>'avance',
            10=>'fecha_compromiso',
            11=>'fecha_cierre',
            12=>'observaciones',
            13=>'antes',
            14=>'durante',
            15=>'despues',
            16=>'p.nombre',
            17=>'tipo_hallazgo',
            18=>'responsable_cli'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('matriz_acciones ma');
        $this->db->join('personal p','p.personalId=ma.id_resp',"left");
        $this->db->where("ma.estatus",1);
        $this->db->where("id_cliente",$params["id_cliente"]);
        $this->db->where("id_proyecto",$params["id_proyecto"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function getDataMatrizAcciones_total($params){
        $columns = array( 
            0=>'id',
            1=>'area',
            2=>'hallazgo',
            3=>'fecha_deteccion',
            4=>'falla',
            5=>'acciones',
            6=>'afectacion',
            7=>'criticidad',
            8=>'id_resp',
            9=>'avance',
            10=>'fecha_compromiso',
            11=>'fecha_cierre',
            12=>'observaciones',
            13=>'antes',
            14=>'durante',
            15=>'p.nombre',
            17=>'tipo_hallazgo',
            18=>'responsable_cli'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('matriz_acciones ma');
        $this->db->join('personal p','p.personalId=ma.id_resp');
        $this->db->where("ma.estatus",1);
        $this->db->where("id_cliente",$params["id_cliente"]);
        $this->db->where("id_proyecto",$params["id_proyecto"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /*************************************************/

    /* ******************MONITOREO DE ACTIVIDADES ****************************/
    function getDataMonitoreo($params){
        $columns = array( 
            0=>'ma.id',
            1=>'ma.zona',
            2=>'la.nombre',
            4=>'ma.frecuencia',
            5=>'ma.inspeccion',
            6=>'ma.fecha',
            7=>'ma.mes',
            8=>'ma.tipo',
            9=>'ma.seguimiento',
            10=>'ma.observaciones'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('monitoreo_actividad ma');
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza');
        $this->db->where("ma.estatus",1);
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        if($params["tipo"]!="2"){
            $this->db->where("ma.tipo",$params["tipo"]);
        }
        $this->db->where("ma.id_limpieza",$params["id_act"]);

        $this->db->where("WEEKOFYEAR(ma.fecha)",$params["semana"]);

        //$this->db->group_by("DATE_FORMAT(ma.fecha,'%m')");
        //$this->db->group_by("ma.id_limpieza");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function getDataMonitoreo_total($params){
        $columns = array( 
            0=>'ma.id',
            1=>'ma.zona',
            2=>'la.nombre',
            4=>'ma.frecuencia',
            5=>'ma.inspeccion',
            6=>'ma.fecha',
            7=>'ma.mes',
            8=>'ma.tipo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('monitoreo_actividad ma');
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza');
        $this->db->where("ma.estatus",1);
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        if($params["tipo"]!="2"){
            $this->db->where("ma.tipo",$params["tipo"]);
        }
        $this->db->where("ma.id_limpieza",$params["id_act"]);
        $this->db->where("WEEKOFYEAR(ma.fecha)",$params["semana"]);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getActividadesMonitoreo($id_proy,$id_cli,$cri=0){
        $this->db->select('la.*, date_format(la.fecha_ini,"%m") as mes');
        $this->db->from("limpieza_actividades la");
        $this->db->where("la.estatus",1);
        //$this->db->where("la.id_proyecto",$id_proy);
        if($cri==1){
            $this->db->where("la.critica",1);
        }
        $this->db->where("la.id_cliente",$id_cli);
        $this->db->order_by("la.nombre","asc");
        $query=$this->db->get();
        return $query->result();
    }

    /* ****************** PRESENTISMO DE PERSONAL ****************************/
    function getDataPresentismo($params){
        $columns = array( 
            0=>'pp.id',
            1=>'pp.tipo_incidencia',
            2=>'pp.fecha',
            4=>'p.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('incidencia_pers_proy pp');
        $this->db->join('personal p','p.personalId=pp.id_personal');
        $this->db->where("pp.estatus",1);
        $this->db->where("pp.id_proyecto",$params["id_proyecto"]);
        if($params["tipo"]!="0"){
            $this->db->where("tipo_incidencia",$params["tipo"]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function getDataPresentismo_total($params){
        $columns = array( 
            0=>'pp.id',
            1=>'pp.tipo_incidencia',
            2=>'pp.fecha',
            4=>'p.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('incidencia_pers_proy pp');
        $this->db->join('personal p','p.personalId=pp.id_personal');
        $this->db->where("pp.estatus",1);
        $this->db->where("pp.id_proyecto",$params["id_proyecto"]);
        if($params["tipo"]!="0"){
            $this->db->where("tipo_incidencia",$params["tipo"]);
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_incidencia($id,$tipo){
        $where="";
        if($tipo!="0"){
            $where="and pp.tipo_incidencia=$tipo";
        }
        $sql = "SELECT pp.id,pp.tipo_incidencia,pp.fecha,p.nombre
                FROM incidencia_pers_proy AS pp
                INNER JOIN personal AS p ON p.personalId = pp.id_personal
                WHERE pp.estatus=1 AND pp.id_proyecto=$id $where";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_monitoreo($id,$tipo,$id_act){
        $sql = "SELECT ma.id,ma.zona,la.nombre,ma.frecuencia,ma.inspeccion,ma.fecha,ma.mes,ma.tipo,ma.seguimiento,ma.observaciones,ma.foto
                FROM monitoreo_actividad AS ma
                INNER JOIN limpieza_actividades AS la ON la.id = ma.id_limpieza
                WHERE ma.estatus=1 AND ma.id_proyecto=$id AND ma.tipo=$tipo AND ma.id_limpieza=$id_act ORDER BY ma.id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    } 

    public function getPuestoRotacion($id){
        $this->db->select("pr.*, ps.puesto, pr.puesto as id_puesto");
        $this->db->from("puesto_rotacion pr");
        $this->db->join('puestos ps','ps.id=pr.puesto','left');
        $this->db->where("id_proyecto",$id);
        $this->db->where("pr.estatus",1);

        $query=$this->db->get();
        return $query->result();
    }

    public function getOrdenesResp($id){
        $columns = array( 
            0=>'oe.id',
            1=>'oe.id_proyecto',
            2=>'oe.id_empleado',
            3=>'p.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ordenes_empleado oe');
        $this->db->join('personal p','p.personalId=oe.id_empleado and p.estatus=1');
        $this->db->where("oe.id_orden",$id);
        $this->db->where("oe.estatus",1);

        $query=$this->db->get();
        return $query->result();
    }

}