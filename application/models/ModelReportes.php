<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelReportes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function getProyectos_cliente($id){
        $this->db->select('p.*, s.servicio, , date_format(p.fecha_ini,"%m") as mes_ini, date_format(p.fecha_fin,"%m") as mes_fin,
                            date_format(p.fecha_ini,"%Y") as anio_ini, date_format(p.fecha_fin,"%Y") as anio_fin');
        $this->db->from("proyectos p");
        $this->db->join("servicios s","s.id=p.id_servicio");
        $this->db->where("p.estatus",1);
        $this->db->where("id_cliente",$id);
        $query=$this->db->get();
        return $query->result();
    }

    public function get_table_salidas($params){
        /*$this->db->select('p.*, od.id_actividad, od.tipo_act, od.turno, od.estatus, od.fecha, pe.nombre as supervisor,
                        "Órdenes de trabajo" as name_salida, c.telefono');
        $this->db->from("proyectos p");
        $this->db->join("clientes c","c.id=p.id_cliente");
        $this->db->join("ordenes_semanal os","os.id_proyecto=p.id and os.estatus=1");
        $this->db->join("ordenes_diario od","od.id_proyecto=p.id and od.activo=1");
        $this->db->join("matriz_acciones ma","ma.id_proyecto=p.id and ma.estatus=1");
        $this->db->join("personal pe","pe.personalId=os.id_supervisor");
        $this->db->where("p.estatus",1);
        //$this->db->where("p.id_cliente",$params["id_cliente"]);
        $this->db->where("p.id",$params["id_proy"]);
        $this->db->group_by("p.id");
        $query=$this->db->get();
        return $query->result();*/
        
        $this->db->select('p.id, p.reg, os.id_limpieza, os.tipo_act, os.turno, os.estatus, os.fecha, pe.nombre as supervisor,
                        "Órdenes de trabajo" as name_salida, c.telefono, 1 as tipo');
        $this->db->from("proyectos p");
        $this->db->join("clientes c","c.id=p.id_cliente");
        $this->db->join("ordenes_semanal os","os.id_proyecto=p.id and os.estatus=1");
        //$this->db->join("ordenes_diario od","od.id_proyecto=p.id and od.activo=1");
        $this->db->join("personal pe","pe.personalId=os.id_supervisor");
        $this->db->where("p.estatus",1);
        //$this->db->where("p.id_cliente",$params["id_cliente"]);
        $this->db->where("p.id",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(os.fecha, '%m')=".$params['mes']."");
        $this->db->where("DATE_FORMAT(os.fecha, '%Y')=".$params['anio']."");
        $this->db->group_by("p.id");
        $query = $this->db->get_compiled_select();

        $this->db->select('p.id, p.reg, ma.id_cliente, ma.id_proyecto, ma.estatus, ma.area, ma.falla, pe.nombre as supervisor,
                        "Matríz de Acciones" as name_salida, c.telefono, 2 as tipo');
        $this->db->from("proyectos p");
        $this->db->join("clientes c","c.id=p.id_cliente");
        $this->db->join("matriz_acciones ma","ma.id_proyecto=p.id and ma.estatus=1");
        $this->db->join("personal pe","pe.personalId=ma.id_resp");
        $this->db->where("p.estatus",1);
        //$this->db->where("p.id_cliente",$params["id_cliente"]);
        $this->db->where("p.id",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha_deteccion, '%m')=".$params['mes']."");
        $this->db->where("DATE_FORMAT(ma.fecha_deteccion, '%Y')=".$params['anio']."");
        $this->db->group_by("p.id");
        $query2 = $this->db->get_compiled_select();

        $this->db->select("p.id,p.reg,p.id_cliente,p.id_servicio,'' as turno,'' as estatus, '' as fecha,'' as supervisor,'Actividades Realizadas' as name_salida,c.telefono,4 as tipo");
        $this->db->from("proyectos p");
        $this->db->join("clientes c","c.id=p.id_cliente");
        $this->db->join("actividades_realizadas  ar","ar.idproyecto=p.id");
        $this->db->where("p.id",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ar.fecha, '%m')=".$params['mes']."");
        $this->db->where("DATE_FORMAT(ar.fecha, '%Y')=".$params['anio']."");
        $this->db->where("p.estatus",1);
        $this->db->where("ar.activo",1);
        $this->db->group_by("p.id");
         $query3 = $this->db->get_compiled_select();

        $this->db->select("p.id,p.reg,p.id_cliente,p.id_servicio,'' as turno,'' as estatus, ma.fecha,'' as supervisor,'Monitoreo de Áreas' as name_salida,c.telefono, 5 as tipo");
        $this->db->from("proyectos p");
        $this->db->join("clientes c","c.id=p.id_cliente");
        $this->db->join("monitoreo_actividad ma","ma.id_proyecto=p.id");
        $this->db->where("p.id",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha, '%m')=".$params['mes']."");
        $this->db->where("DATE_FORMAT(ma.fecha, '%Y')=".$params['anio']."");
        $this->db->where("p.estatus",1);
        $this->db->where("ma.estatus",1);
        $this->db->group_by("p.id");
        $query4 = $this->db->get_compiled_select();

        $this->db->select("p.id,p.reg,p.id_cliente,p.id_servicio,'' as turno,'' as estatus, pp.fecha,'' as supervisor,'Presentismo Rotación' as name_salida,c.telefono, 6 as tipo");
        $this->db->from("proyectos p");
        $this->db->join("clientes c","c.id=p.id_cliente");
        $this->db->join("incidencia_pers_proy pp","pp.id_proyecto=p.id");
        $this->db->where("p.id",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(pp.fecha, '%m')=".$params['mes']."");
        $this->db->where("DATE_FORMAT(pp.fecha, '%Y')=".$params['anio']."");
        $this->db->where("p.estatus",1);
        $this->db->where("pp.estatus",1);
        $this->db->group_by("p.id");
        $query5 = $this->db->get_compiled_select();

        $this->db->select("p.id,p.reg,p.id_cliente,p.id_servicio,'' as turno,'' as estatus, p.fecha_ini as fecha,'' as supervisor,'Matriz de Versatilidad' as name_salida,c.telefono, 3 as tipo");
        $this->db->from("proyectos p");
        $this->db->join("clientes c","c.id=p.id_cliente");
        if(!isset($params["id_proy"])){
            $this->db->join("matriz_versatibilidad ma","ma.idproyecto=p.id");
        }else{
            $id_proy=$params['id_proy'];
            $this->db->join("matriz_versatibilidad ma","ma.idproyecto=p.id and ma.idproyecto=".$id_proy."");
        }
        $this->db->where("p.id",$params["id_proy"]);
        $this->db->where("p.estatus",1);
        $this->db->where("ma.activo",1);

        /*$this->db->where("DATE_FORMAT(ma.reg, '%m')=".$params['mes']."");
        $this->db->or_where("DATE_FORMAT(ma.fecha_update, '%m')=".$params['mes']."");

        $this->db->where("DATE_FORMAT(ma.reg, '%Y')=".$params['anio']."");
        $this->db->or_where("DATE_FORMAT(ma.fecha_update, '%Y')=".$params['anio']."");*/

        $this->db->where("DATE_FORMAT(ma.reg, '%m')=".$params['mes']."");
        $this->db->where("DATE_FORMAT(ma.reg, '%Y')=".$params['anio']."");
        
        $this->db->or_where("DATE_FORMAT(ma.fecha_update, '%m')=".$params['mes']."");
        $this->db->where("DATE_FORMAT(ma.fecha_update, '%Y')=".$params['anio']."");

        $this->db->group_by("p.id");
        $query6 = $this->db->get_compiled_select();

        $queryunions=$query." UNION ".$query2." UNION ".$query3." UNION ".$query4." UNION ".$query5." UNION ".$query6;
        return $this->db->query($queryunions)->result();
    }

    public function get_table_salidasCliente($params){
        if($params["tipo"]==1){
            $this->db->select('p.id, p.reg, os.id_limpieza, os.tipo_act, os.turno, os.estatus, os.fecha, pe.nombre as supervisor,
                            "Órdenes de trabajo" as name_salida, c.telefono, 1 as tipo');
            $this->db->from("proyectos p");
            $this->db->join("clientes c","c.id=p.id_cliente");
            $this->db->join("ordenes_semanal os","os.id_proyecto=p.id and os.estatus=1");
            //$this->db->join("ordenes_diario od","od.id_proyecto=p.id and od.activo=1");
            $this->db->join("personal pe","pe.personalId=os.id_supervisor");
            $this->db->where("p.estatus",1);
            //$this->db->where("p.id_cliente",$params["id_cliente"]);
            $this->db->where("p.id",$params["id_proy"]);
            $this->db->where("DATE_FORMAT(os.fecha, '%m')=".$params['mes']."");
            $this->db->group_by("p.id");
            $query=$this->db->get();
            return $query->result();
        }
        if($params["tipo"]==2){
            $this->db->select('p.id, p.reg, ma.id_cliente, ma.id_proyecto, ma.estatus, ma.area, ma.falla, pe.nombre as supervisor,
                            "Matríz de Acciones" as name_salida, c.telefono, 2 as tipo');
            $this->db->from("proyectos p");
            $this->db->join("clientes c","c.id=p.id_cliente");
            $this->db->join("matriz_acciones ma","ma.id_proyecto=p.id and ma.estatus=1");
            $this->db->join("personal pe","pe.personalId=ma.id_resp");
            $this->db->where("p.estatus",1);
            //$this->db->where("p.id_cliente",$params["id_cliente"]);
            $this->db->where("p.id",$params["id_proy"]);
            $this->db->where("DATE_FORMAT(ma.fecha_deteccion, '%m')=".$params['mes']."");
            $this->db->group_by("p.id");
            $query=$this->db->get();
            return $query->result();
        }
        if($params["tipo"]==3){
            $this->db->select("p.id,p.reg,p.id_cliente,p.id_servicio,'' as turno,'' as estatus, ma.fecha,'' as supervisor,'Monitoreo de Áreas' as name_salida,c.telefono, 5 as tipo");
            $this->db->from("proyectos p");
            $this->db->join("clientes c","c.id=p.id_cliente");
            $this->db->join("monitoreo_actividad ma","ma.id_proyecto=p.id");
            $this->db->where("p.id",$params["id_proy"]);
            $this->db->where("DATE_FORMAT(ma.fecha, '%m')=".$params['mes']."");
            $this->db->where("p.estatus",1);
            $this->db->where("ma.estatus",1);
            $this->db->group_by("p.id");
            $query=$this->db->get();
            return $query->result();
        }
        if($params["tipo"]==4){
            $this->db->select("p.id,p.reg,p.id_cliente,p.id_servicio,'' as turno,'' as estatus, '' as fecha,'' as supervisor,'Actividades Realizadas' as name_salida,c.telefono,4 as tipo");
            $this->db->from("proyectos p");
            $this->db->join("clientes c","c.id=p.id_cliente");
            $this->db->join("actividades_realizadas  ar","ar.idproyecto=p.id");
            $this->db->where("p.id",$params["id_proy"]);
            $this->db->where("DATE_FORMAT(ar.fecha, '%m')=".$params['mes']."");
            $this->db->where("p.estatus",1);
            $this->db->where("ar.activo",1);
            $this->db->group_by("p.id");
            $query=$this->db->get();
            return $query->result();
        }
        if($params["tipo"]==5){
            $this->db->select("p.id,p.reg,p.id_cliente,p.id_servicio,'' as turno,'' as estatus, p.fecha_ini as fecha,'' as supervisor,'Matriz de Versatilidad' as name_salida,c.telefono, 3 as tipo");
            $this->db->from("proyectos p");
            $this->db->join("clientes c","c.id=p.id_cliente");
            $this->db->join("matriz_versatibilidad ma","ma.idproyecto=p.id");
            $this->db->where("p.id",$params["id_proy"]);
            //$this->db->where("DATE_FORMAT(ma.reg, '%m')=".$params['mes']."");
            $this->db->where("p.estatus",1);
            $this->db->where("ma.activo",1);
            $this->db->group_by("p.id");
            $query=$this->db->get();
            return $query->result();
        }
        if($params["tipo"]==6){
            $this->db->select("p.id,p.reg,p.id_cliente,p.id_servicio,'' as turno,'' as estatus, pp.fecha,'' as supervisor,'Presentismo Rotación' as name_salida,c.telefono, 6 as tipo");
            $this->db->from("proyectos p");
            $this->db->join("clientes c","c.id=p.id_cliente");
            $this->db->join("incidencia_pers_proy pp","pp.id_proyecto=p.id");
            $this->db->where("p.id",$params["id_proy"]);
            $this->db->where("DATE_FORMAT(pp.fecha, '%m')=".$params['mes']."");
            $this->db->where("p.estatus",1);
            $this->db->where("pp.estatus",1);
            $this->db->group_by("p.id");
            $query=$this->db->get();
            return $query->result();
        }
    }

    /*public function get_data_ordenes($params){
        $mes = $params["mes"];
        $anio = $params["anio"];
        $this->db->select('DISTINCT(od.id), od.id_actividad, od.tipo_act, od.turno, od.estatus, od.causas, od.fecha, pe.nombre as supervisor, WEEKOFYEAR(od.fecha) AS num_semana, c.foto as logo_cli, p.turno as turno_proy, p.turno2 as turno_proy2, p.turno3 as turno_proy3');
        $this->db->from("ordenes_semanal os");
        $this->db->join("ordenes_diario od","od.id_proyecto=os.id_proyecto and od.activo=1");
        $this->db->join("personal pe","pe.personalId=os.id_supervisor");
        $this->db->join("proyectos p","p.id=os.id_proyecto");
        $this->db->join("clientes c","c.id=os.id_cliente");
        $this->db->where("od.activo",1);
        $this->db->where("os.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(od.fecha, '%m')={$mes}");
        $this->db->where("DATE_FORMAT(od.fecha, '%Y')={$anio}");
        //$this->db->group_by("od.id");

        $this->db->group_by("od.fecha");
        $this->db->group_by("od.turno");
        
        $this->db->order_by("od.fecha","asc");
        $this->db->order_by("od.turno","asc");
        $this->db->order_by("num_semana","asc");
        $query=$this->db->get();
        return $query->result();
    }*/ //se comenta este y debajo se pone la funcion para la nueva estructura

    public function get_data_ordenes($params,$band){
        $mes = $params["mes"];
        $anio = $params["anio"];
        $fecha = $params["anio"]."-".$params["mes"]."-01";
        $this->db->select('DISTINCT(os.id), os.id_limpieza, os.tipo_act, os.turno, os.estatus, os.fecha, pe.nombre as supervisor, WEEKOFYEAR(os.fecha) AS num_semana, c.foto as logo_cli, p.turno as turno_proy, p.turno2 as turno_proy2, p.turno3 as turno_proy3, la.days,
            WEEK("'.$fecha.'",0) as num_sems');
        $this->db->from("ordenes_semanal os");
        $this->db->join("limpieza_actividades la","la.id=os.id_limpieza and la.estatus=1");
        $this->db->join("personal pe","pe.personalId=os.id_supervisor");
        $this->db->join("proyectos p","p.id=os.id_proyecto");
        $this->db->join("clientes c","c.id=os.id_cliente");
        $this->db->where("os.estatus",1);
        //$this->db->where("os.tipo",2); //diario - debe traer todos de l-d
        $this->db->where("os.id_proyecto",$params["id_proy"]);
        //if($band==0){
            $this->db->where("DATE_FORMAT(os.fecha, '%m')={$mes}");
            $this->db->where("DATE_FORMAT(os.fecha, '%Y')={$anio}");
        /*}else{
            //$this->db->where("DATE_FORMAT(os.fecha, '%m')>={$mes}");
            $this->db->where("DATE_FORMAT(os.fecha, '%Y')={$anio}");
        }*/
        $this->db->group_by("os.fecha");
        $this->db->group_by("os.turno");
        
        $this->db->order_by("os.fecha","asc");
        $this->db->order_by("os.turno","asc");
        $this->db->order_by("num_semana","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function get_ordenes_coments($params){
        $mes = $params["mes"];
        $anio = $params["anio"];
        $this->db->select('DISTINCT(os.id), la.nombre as actividad, os.turno, os.estatus, os.fecha, WEEKOFYEAR(os.fecha) AS num_semana,
                        os.ot, os.observaciones, os.seguimiento');
        $this->db->from("ordenes_semanal os");
        $this->db->join("limpieza_actividades la","la.id=os.id_limpieza and la.estatus=1");
        $this->db->join("personal pe","pe.personalId=os.id_supervisor");
        $this->db->join("proyectos p","p.id=os.id_proyecto");
        $this->db->join("clientes c","c.id=os.id_cliente");
        $this->db->where("os.estatus",1);
        $this->db->where("os.id_proyecto",$params["id_proy"]);
        
        $this->db->where("DATE_FORMAT(os.fecha, '%m')={$mes}");
        $this->db->where("DATE_FORMAT(os.fecha, '%Y')={$anio}");
        $this->db->where("(os.seguimiento=1 or os.seguimiento=4 or os.seguimiento=5)");
        /*$this->db->group_by("os.fecha");
        $this->db->group_by("os.turno");*/
        
        $this->db->order_by("os.fecha","asc");
        $this->db->order_by("os.turno","asc");
        $this->db->order_by("num_semana","asc");
        $query=$this->db->get();
        return $query->result();
    }

    /*public function get_data_limpieza_orden($params){
        //$mes = $params["mes"];
        $semana = $params["semana"];
        $this->db->select("la.*, DATE_FORMAT(la.fecha_realiza, '%d') as dia, DAYOFYEAR(la.fecha_realiza) AS num_dia, WEEKOFYEAR(la.fecha_realiza) AS num_semana, os.ot, os.turno, c.foto");
        $this->db->from("limpieza_actividades la");
        $this->db->join("ordenes_semanal os","os.id_proyecto=la.id_proyecto and os.id_limpieza=la.id");
        $this->db->join("clientes c","c.id=la.id_cliente");
        $this->db->where("la.estatus",1);
        $this->db->where("la.id_proyecto",$params["id_proy"]);
        //$this->db->where("DATE_FORMAT(la.fecha_realiza, '%m')={$mes}");
        $this->db->where(" WEEKOFYEAR(la.fecha_realiza)={$semana}");

        $this->db->group_by("la.id");
        $this->db->order_by("la.fecha_realiza","asc");
        $this->db->order_by("od.turno","asc");
        //$this->db->order_by("num_semana","asc");
        $query=$this->db->get();
        return $query->result();
    }*/

    public function get_data_limpieza_orden($params){
        //$mes = $params["mes"];
        $semana = $params["semana"];
        $anio = $params["anio"];
        $this->db->select("la.nombre, la.fecha_ini, os.id, os.seguimiento, os.semana, DATE_FORMAT(os.fecha, '%Y') as anio, DATE_FORMAT(os.fecha, '%d') as dia, os.fecha_final, os.fecha, os.ot, os.turno, os.observaciones, c.foto, c.empresa,
            (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=1 and os2.fecha=os.fecha) as cont_can,
            (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=2 and os2.fecha=os.fecha) as cont_pro,
            (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=3 and os2.fecha=os.fecha) as cont_ok,
            (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=4 and os2.fecha=os.fecha) as cont_re,
            (select count(seguimiento) from ordenes_semanal as os2 where os2.id=os.id and os2.seguimiento=5 and os2.fecha=os.fecha) as cont_no_rea");
        $this->db->from("ordenes_semanal os");
        $this->db->join("limpieza_actividades la","la.id_proyecto=os.id_proyecto and la.id=os.id_limpieza and la.estatus=1");
        $this->db->join("clientes c","c.id=la.id_cliente");
        $this->db->where("os.estatus",1);
        $this->db->where("os.id_proyecto",$params["id_proy"]);
        //$this->db->where("DATE_FORMAT(la.fecha_realiza, '%m')={$mes}");
        $this->db->where("DATE_FORMAT(os.fecha, '%Y')={$anio}");
        $this->db->where("os.semana={$semana}");
        $this->db->order_by("os.fecha","asc");
        $this->db->order_by("os.turno","asc");
        //$this->db->order_by("num_semana","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function get_mes_actividades($id){
        $this->db->select("DISTINCT(la.fecha_ini)");
        $this->db->from("limpieza_actividades la");
        $this->db->where("la.estatus",1);
        $this->db->where("la.id_proyecto",$id);
        //$this->db->group_by("la.fecha_realiza");
        $this->db->order_by("la.fecha_ini","asc");
        //$this->db->order_by("num_semana","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function get_semanas_programa($id,$anio,$mes){
        $this->db->select("DISTINCT(semana)");
        $this->db->from("ordenes_semanal");
        $this->db->where("estatus",1);
        $this->db->where("id_proyecto",$id);
        $this->db->where("DATE_FORMAT(fecha,'%Y')",$anio);
        if(intval($mes)>9)
            $this->db->where("DATE_FORMAT(fecha,'%m')",$mes);
        else
            $this->db->where("DATE_FORMAT(fecha,'%m')","0".$mes);

        $this->db->order_by("semana","ASC");
        $query=$this->db->get();
        return $query->result();
    }

    /*public function count_estatus($id,$estatus,$fecha,$id_proy,$turno){
        $this->db->select('count(id) as tot_estatus');
        $this->db->from("ordenes_diario od");
        $this->db->where("od.activo",1);
        //$this->db->where("od.id",$id);
        $this->db->where("od.turno",$turno);
        $this->db->where("od.id_proyecto",$id_proy);
        $this->db->where("od.fecha",$fecha);
        $this->db->where("od.estatus",$estatus);
        $query=$this->db->get();
        return $query->row();
    }*/ //se comenta y se hace nueva funcion debajo para nva estructura

    public function count_estatus($id,$estatus,$fecha,$id_proy,$turno){
        $this->db->select('count(id) as tot_estatus');
        $this->db->from("ordenes_semanal os");
        $this->db->where("estatus",1);
        //$this->db->where("os.tipo",2); //diario - debe traer todos de l-d
        if($turno>0){
            $this->db->where("turno",$turno);
        }
        $this->db->where("id_proyecto",$id_proy);
        $this->db->where("fecha",$fecha);
        if($estatus>0){
            $this->db->where("seguimiento",$estatus);
        }
        $query=$this->db->get();
        return $query->row();
    }

    public function count_turnos($fecha,$id_proy,$turno){
        $mes = date('m', strtotime($fecha));
        $this->db->select('count(id) as tot_turnos');
        $this->db->from("ordenes_semanal os");
        $this->db->where("estatus",1);
        
        $this->db->where("turno",$turno);
        $this->db->where("id_proyecto",$id_proy);
        $this->db->where("DATE_FORMAT(fecha,'%m')",$mes);

        $query=$this->db->get();
        return $query->row();
    }

    /*public function get_result_monitoreo($params,$tipo,$where){
        $this->db->select('ma.*, la.nombre, LAST_DAY(ma.fecha) as ultimo, DATE_FORMAT(ma.fecha, "%Y") as anio, DATE_FORMAT(ma.fecha, "%d") as dia, c.foto, DATE_FORMAT(ma.fecha, "%d") as dias_act, ma.seguimiento as seguimiento_act');
        $this->db->from("monitoreo_actividad ma");
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza');
        $this->db->join('clientes c','c.id=ma.id_cliente');
        $this->db->where("ma.estatus",1);
        $this->db->where("ma.tipo",$tipo);
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%Y')",$params["anio"]);
        if(intval($params["mes"])>9){
            $this->db->where("ma.mes",$params["mes"]);
        }
        else{
            $this->db->where("ma.mes",$params["mes"]);
            $this->db->or_where("ma.mes","0".$params["mes"]);
        }
        if($where!="" && $tipo==1){
            $this->db->where($where);
        }
        //$this->db->group_by("ma.id_limpieza");
        $this->db->order_by("ma.fecha","ASC");
        
        $query=$this->db->get();
        return $query->result();
    }*/

    public function get_result_monitoreo($params,$tipo,$where){
        $this->db->select("ma.*, la.nombre, LAST_DAY(ma.fecha) as ultimo, DATE_FORMAT(ma.fecha, '%Y') as anio, DATE_FORMAT(ma.fecha, '%d') as dia, c.foto
            , IFNULL(GROUP_CONCAT(1 SEPARATOR '<td>'),0) as valor,
            GROUP_CONCAT(DATE_FORMAT(ma.fecha, '%d') ORDER BY ma.fecha asc SEPARATOR ',') as dias_act,
            IF(observaciones!='', GROUP_CONCAT(ma.observaciones SEPARATOR '<br>'),'') as comentarios_br,
            GROUP_CONCAT(ma.seguimiento ORDER BY ma.fecha asc SEPARATOR ',') as seguimiento_act, count(ma.id_limpieza) as tot_act");
        $this->db->from("monitoreo_actividad ma");
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza and la.estatus=1');
        $this->db->join('clientes c','c.id=ma.id_cliente');
        $this->db->where("ma.estatus",1);
        if($tipo!=2){
            $this->db->where("ma.tipo",$tipo);
        }
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%Y')",$params["anio"]);
        if(isset($params["id_act"])){
            $this->db->where("ma.id_limpieza",$params["id_act"]);
        }
        if($where!="" && $tipo==1){
            $this->db->where($where);
        }
        if(intval($params["mes"])>9){
            $this->db->where("ma.mes",$params["mes"]);
        }
        else{
            $this->db->where("(ma.mes=".$params["mes"]." or ma.mes="."0".$params["mes"].")");
        }
        
        $this->db->group_by("ma.id_limpieza");//ya no porque es por check de monitoreo
        //$this->db->group_by("ma.id"); //ULTIMO AJUSTE, AHORA ES POR PTOS INSPECCION
        //$this->db->group_by("ma.inspeccion");
        //$this->db->order_by("ma.id_limpieza","ASC");
        $this->db->order_by("ma.fecha","ASC");
        
        $query=$this->db->get();
        return $query->result();
    }

    public function get_result_monitoreoNvo($params,$tipo,$where,$id_act){
        $this->db->select("ma.*, la.nombre, LAST_DAY(ma.fecha) as ultimo, DATE_FORMAT(ma.fecha, '%Y') as anio, DATE_FORMAT(ma.fecha, '%d') as dia, c.foto
            , IFNULL(GROUP_CONCAT(1 SEPARATOR '<td>'),0) as valor,
            GROUP_CONCAT(DATE_FORMAT(ma.fecha, '%d') ORDER BY ma.fecha asc SEPARATOR ',') as dias_act,
            IF(observaciones!='', GROUP_CONCAT(ma.observaciones SEPARATOR '<br>'),'') as comentarios_br,
            GROUP_CONCAT(ma.seguimiento ORDER BY ma.fecha asc SEPARATOR ',') as seguimiento_act, count(ma.id_limpieza) as tot_act");
        $this->db->from("monitoreo_actividad ma");
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza and la.estatus=1 and la.id='.$id_act.'');
        $this->db->join('clientes c','c.id=ma.id_cliente');
        $this->db->where("ma.estatus",1);
        if($tipo!=2){
            $this->db->where("ma.tipo",$tipo);
        }
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%Y')",$params["anio"]);
        $this->db->where("ma.id_limpieza",$id_act);
        if(intval($params["mes"])>9){
            $this->db->where("ma.mes",$params["mes"]);
        }
        else{
            $this->db->where("(ma.mes=".$params["mes"]." or ma.mes="."0".$params["mes"].")");
            //$this->db->or_where("ma.mes","0".$params["mes"]);
        }
        if($where!="" && $tipo==1){
            $this->db->where($where);
        }

        $this->db->group_by("ma.id_limpieza");//ya no porque es por check de monitoreo
        //$this->db->group_by("ma.id"); //ULTIMO AJUSTE, AHORA ES POR PTOS INSPECCION
        $this->db->group_by("ma.inspeccion");
        //$this->db->order_by("ma.id_limpieza","ASC");
        $this->db->order_by("ma.fecha","ASC");
        
        $query=$this->db->get();
        return $query->result();
    }

    public function get_result_monitoreo1_2($params,$tipo,$where){
        $this->db->select("ma.*, la.nombre, LAST_DAY(ma.fecha) as ultimo, DATE_FORMAT(ma.fecha, '%Y') as anio, DATE_FORMAT(ma.fecha, '%d') as dia, c.foto
            , IFNULL(GROUP_CONCAT(1 SEPARATOR '<td>'),0) as valor,
            GROUP_CONCAT(DATE_FORMAT(ma.fecha, '%d') ORDER BY ma.fecha asc SEPARATOR ',') as dias_act,
            IF(observaciones!='', GROUP_CONCAT(ma.observaciones SEPARATOR '<br>'),'') as comentarios_br,
            GROUP_CONCAT(ma.seguimiento ORDER BY ma.fecha asc SEPARATOR ',') as seguimiento_act, count(ma.id_limpieza) as tot_act");
        $this->db->from("monitoreo_actividad ma");
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza and la.estatus=1');
        $this->db->join('clientes c','c.id=ma.id_cliente');
        $this->db->where("ma.estatus",1);
        if($tipo!=2){
            $this->db->where("ma.tipo",$tipo);
        }
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%Y')",$params["anio"]);
        if(intval($params["mes"])>9){
            $this->db->where("ma.mes",$params["mes"]);
        }
        else{
            $this->db->where("(ma.mes=".$params["mes"]." or ma.mes="."0".$params["mes"].")");
            //$this->db->or_where("ma.mes","0".$params["mes"]);
        }
        if($where!="" && $tipo==1){
            $this->db->where($where);
        }
    
        $this->db->group_by("ma.id_limpieza");//ya no porque es por check de monitoreo
        //$this->db->group_by("ma.id"); //ULTIMO AJUSTE, AHORA ES POR PTOS INSPECCION
        //$this->db->group_by("ma.inspeccion");
        $this->db->order_by("ma.fecha","ASC");
        
        $query=$this->db->get();
        return $query->result();
    }

    public function getTotalActiv($params){
        $this->db->select("ma.id_limpieza, ma.inspeccion, la.nombre, count(ma.id_limpieza) as tot_act");
        $this->db->from("monitoreo_actividad ma");
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza and la.estatus=1');
        $this->db->join('clientes c','c.id=ma.id_cliente');
        $this->db->where("ma.estatus",1);

        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%Y')",$params["anio"]);
        $this->db->where("ma.tipo",$params["tipo"]);
        if(intval($params["mes"])>9){
            $this->db->where("ma.mes",$params["mes"]);
        }
        else{
            $this->db->where("(ma.mes=".$params["mes"]." or ma.mes="."0".$params["mes"].")");
            //$this->db->or_where("ma.mes","0".$params["mes"]);
        }
        
        $this->db->group_by("ma.id_limpieza");//ya no porque es por check de monitoreo       
        $query=$this->db->get();
        return $query->result();
    }

    /*public function get_result_monitoreo2($params,$tipo,$where){
        $this->db->select('ma.*, la.nombre, LAST_DAY(ma.fecha) as ultimo, DATE_FORMAT(ma.fecha, "%Y") as anio, DATE_FORMAT(ma.fecha, "%d") as dia, c.foto,
            DATE_FORMAT(ma.fecha, "%d") as dias_act,
            ma.seguimiento as seguimiento_act,
            DATE_FORMAT(ma.fecha, "%d") AS tot_dias_act');
        $this->db->from("monitoreo_actividad ma");
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza');
        $this->db->join('clientes c','c.id=ma.id_cliente');
        $this->db->where("ma.estatus",1);
        //$this->db->where("ma.tipo",$tipo);
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%Y')",$params["anio"]);
        if(intval($params["mes"])>9){
            $this->db->where("ma.mes",$params["mes"]);
        }
        else{
            $this->db->where("ma.mes",$params["mes"]);
            $this->db->or_where("ma.mes","0".$params["mes"]);
        }
        if($where!="" && $tipo==1){
            $this->db->where($where);
        }
        //$this->db->group_by("ma.id_limpieza");
        //$this->db->group_by("ma.tipo");
        $this->db->order_by("ma.fecha","ASC");
        
        $query=$this->db->get();
        return $query->result();
    }*/

    public function get_result_monitoreo2($params,$band){
        $this->db->select('ma.*, la.nombre, LAST_DAY(ma.fecha) as ultimo, DATE_FORMAT(ma.fecha, "%Y") as anio, DATE_FORMAT(ma.fecha, "%d") as dia, c.foto, "monitoreo2" as tipo_consul,
            IFNULL(GROUP_CONCAT(1 SEPARATOR "<td>"),0) as valor,
            IF(observaciones!="", GROUP_CONCAT(ma.observaciones SEPARATOR "<br>"),"") as comentarios_br,
            GROUP_CONCAT(DATE_FORMAT(ma.fecha, "%d") ORDER BY ma.fecha asc SEPARATOR ",") as dias_act,
            GROUP_CONCAT(ma.seguimiento ORDER BY ma.fecha asc SEPARATOR ",") as seguimiento_act, count(ma.id_limpieza) as tot_act');
        $this->db->from("monitoreo_actividad ma");
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza and la.estatus=1');
        $this->db->join('clientes c','c.id=ma.id_cliente');
        $this->db->where("ma.estatus",1);
        //$this->db->where("ma.tipo",$tipo);
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%Y')",$params["anio"]);

        if(intval($params["mes"])>9){
            $this->db->where("ma.mes",$params["mes"]);
        }
        else{
            $this->db->where("(ma.mes=".$params["mes"]." or ma.mes="."0".$params["mes"].")");
            //$this->db->or_where("ma.mes","0".$params["mes"]);
        }
        
        $this->db->group_by("ma.id_limpieza");//ya no porque es por check de monitoreo
        if($band==1){ //para traer datos separados de activ
            $this->db->group_by("ma.inspeccion");
        }
        $this->db->order_by("ma.id_limpieza","asc");
        $this->db->order_by("ma.fecha","ASC");
        
        $query=$this->db->get();
        return $query->result();
    }

    public function count_limpiezaz($params,$id_limp,$dia){
        $this->db->select('count(ma.id_limpieza) as tot_act');
        $this->db->from("monitoreo_actividad ma");
        $this->db->join('limpieza_actividades la','la.id=ma.id_limpieza and la.estatus=1');
        $this->db->join('clientes c','c.id=ma.id_cliente');
        $this->db->where("ma.estatus",1);
        //$this->db->where("ma.tipo",$tipo);
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%Y')",$params["anio"]);
        $this->db->where("DATE_FORMAT(ma.fecha,'%d')",$dia);
        $this->db->where("ma.id_limpieza",$id_limp);
        if(intval($params["mes"])>9){
            $this->db->where("ma.mes",$params["mes"]);
        }
        else{
            $this->db->where("(ma.mes=".$params["mes"]." or ma.mes="."0".$params["mes"].")");
            //$this->db->or_where("ma.mes","0".$params["mes"]);
        }
        $query=$this->db->get();
        return $query->row();
    }

    public function get_graph_monitor($id_proy,$mes,$anio){
        $this->db->select('gm.*');
        $this->db->from("grafica_monitoreo gm");
        $this->db->join("limpieza_actividades la","la.id=gm.id_act and la.estatus=1");
        $this->db->where("gm.id_proyecto",$id_proy);
        $this->db->where("gm.anio",$anio);
        $this->db->where("gm.id_act!=","0");
       
        if(intval($mes)>9)
            $this->db->where("gm.mes",$mes);
        else
            $this->db->where("gm.mes","0".$mes);

        $this->db->order_by("gm.id_act","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function get_graph_monitorMes($id_proy,$mes,$anio){
        $this->db->select('*');
        $this->db->from("grafica_monitoreo gm");
        $this->db->where("gm.id_proyecto",$id_proy);
        $this->db->where("gm.anio",$anio);
        $this->db->where("gm.tipo","5");
        if(intval($mes)>9)
            $this->db->where("gm.mes",$mes);
        else
            $this->db->where("gm.mes","0".$mes);

        $query=$this->db->get();
        return $query->result();
    }

    ////////////////////////////////////////////////////////
    public function get_data_matriz_accion($params,$id_cli){
        $this->db->select('ma.*');
        $this->db->from("matriz_acciones ma");
        $this->db->where("ma.estatus",1);
        $this->db->where("ma.id_proyecto",$params["id_proy"]);
        //$this->db->where("DATE_FORMAT(ma.fecha_deteccion,'%m')=".$params['mes']."");
        //$this->db->where("DATE_FORMAT(ma.fecha_deteccion,'%Y')=".$params['anio']."");
        /*if($id_cli>0){
            $this->db->where("ma.tipo_hallazgo",2); //del cliente
        }*/
        $query=$this->db->get();
        return $query->result();
    }

     ////////////////////////////////////////////////////////
    //PRESENTISMO DE ROTACIÓN

    public function get_data_mes($params){
        $where_anio=""; $where_mes=""; $where_mes2=""; $where_mes3=""; $where_mes4=""; $where_mes5="";

        if(intval($params["mes"])>9)
            $where_mes=" and DATE_FORMAT(pp2.fecha,'%m')=".$params['mes']."";
        else
            $where_mes=" and DATE_FORMAT(pp2.fecha,'%m')=0".$params['mes']."";
        if(intval($params["mes"])>9)
            $where_mes2=" and DATE_FORMAT(pp3.fecha,'%m')=".$params['mes']."";
        else
            $where_mes2=" and DATE_FORMAT(pp3.fecha,'%m')=0".$params['mes']."";
        if(intval($params["mes"])>9)
            $where_mes3=" and DATE_FORMAT(pp4.fecha,'%m')=".$params['mes']."";
        else
            $where_mes3=" and DATE_FORMAT(pp4.fecha,'%m')=0".$params['mes']."";
        if(intval($params["mes"])>9)
            $where_mes4=" and DATE_FORMAT(pp5.fecha,'%m')=".$params['mes']."";
        else
            $where_mes4=" and DATE_FORMAT(pp5.fecha,'%m')=0".$params['mes']."";
        if(intval($params["mes"])>9)
            $where_mes5=" and DATE_FORMAT(pp6.fecha,'%m')=".$params['mes']."";
        else
            $where_mes5=" and DATE_FORMAT(pp6.fecha,'%m')=0".$params['mes']."";

        $this->db->select('pp.*, DATE_FORMAT(pp.fecha, "%Y") as anio, p.nombre, 
            GROUP_CONCAT(puesto SEPARATOR ",") as puesto_todos,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as pp2 where pp2.estatus=1 and tipo_incidencia=1 and pp2.id_personal=pp.id_personal and pp2.id_proyecto='.$params["id_proy"].' '.$where_mes.' 
                and DATE_FORMAT(pp2.fecha,"%Y")='.$params["anio"].') as tot_faltas, "FALTAS" as name_inci,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as pp3 where pp3.estatus=1 and pp3.tipo_incidencia=2 and pp3.id_personal=pp.id_personal and pp3.id_proyecto='.$params["id_proy"].' '.$where_mes2.' and DATE_FORMAT(pp3.fecha,"%Y")='.$params["anio"].') as tot_perm, "PERMISOS" as name_inci,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as pp4 where pp4.estatus=1 and pp4.tipo_incidencia=3 and pp4.id_personal=pp.id_personal and pp4.id_proyecto='.$params["id_proy"].' '.$where_mes3.' and DATE_FORMAT(pp4.fecha,"%Y")='.$params["anio"].') as tot_ret, "RETARDOS" as name_inci,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as pp5 where pp5.estatus=1 and pp5.tipo_incidencia=4 and pp5.id_personal=pp.id_personal and pp5.id_proyecto='.$params["id_proy"].' '.$where_mes4.' and DATE_FORMAT(pp5.fecha,"%Y")='.$params["anio"].') as tot_inca, "INCAPACIDAD" as name_inci,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as pp6 where pp6.estatus=1 and pp6.tipo_incidencia=6 and pp6.id_personal=pp.id_personal and pp6.id_proyecto='.$params["id_proy"].' '.$where_mes5.' and DATE_FORMAT(pp6.fecha,"%Y")='.$params["anio"].') as tot_vaca, "VACACIONES" as name_inci');
        $this->db->from("incidencia_pers_proy pp");
        //$this->db->join('personal p','p.personalId=pp.id_personal and p.estatus=1');
        $this->db->join('personal p','p.personalId=pp.id_personal ');
        $this->db->where("pp.estatus",1);
        $this->db->where("pp.id_proyecto",$params["id_proy"]);
        $this->db->where("DATE_FORMAT(pp.fecha,'%Y')",$params["anio"]);
        if(intval($params["mes"])>9)
            $this->db->where("DATE_FORMAT(pp.fecha,'%m')",$params["mes"]);
        else
            $this->db->where("DATE_FORMAT(pp.fecha,'%m')","0".$params["mes"]);

        $this->db->group_by("pp.`id_personal");
        $query=$this->db->get();
        return $query->result();
    }

    public function count_personal_activo($id_proy,$puesto,$mes,$anio){
        $this->db->select('count(DISTINCT(id_empleado)) as tot_activos');
        $this->db->from("empleados_proyecto ep");
        $this->db->join('personal p','p.personalId=ep.id_empleado and p.estatus=1');
        $this->db->where("ep.estatus",1);
        $this->db->where("ep.id_proyecto",$id_proy);
        $this->db->where("p.puesto",$puesto);
        $this->db->where("ep.tipo_empleado",1);

        //$this->db->where("DATE_FORMAT(ep.fecha_asignacion,'%m')>=".$mes."");
        $this->db->where("DATE_FORMAT(ep.fecha_asignacion,'%m')<=".$mes."");
        $this->db->where("DATE_FORMAT(ep.fecha_asignacion,'%Y')<=".$anio."");
        
        $query=$this->db->get();
        return $query->row();
    }

    public function get_detalle_proy($id_proy){
        $this->db->select('p.*, c.foto, ps.puesto as name_puesto, p.dias_laborables');
        $this->db->from("puesto_rotacion p");
        $this->db->join('proyectos pro','pro.id=p.id_proyecto');
        $this->db->join('puestos ps','ps.id=p.puesto');
        $this->db->join('clientes c','c.id=pro.id_cliente');
        $this->db->where("p.id_proyecto",$id_proy);
        $this->db->where("p.estatus",1);
        $query=$this->db->get();
        return $query->result();
    }

    public function get_bajas_proy($id_proy,$anio,$mes,$group){
        $this->db->select('pp.*, WEEKOFYEAR(pp.fecha) AS semana, p.nombre');
        $this->db->from("incidencia_pers_proy pp");
        $this->db->join('personal p','p.personalId=pp.id_personal');
        $this->db->where("DATE_FORMAT(pp.fecha,'%Y')",$anio);
        $this->db->where("pp.id_proyecto",$id_proy);
        if(intval($mes>9))
            $this->db->where("DATE_FORMAT(pp.fecha,'%m')",$mes);
        else
            $this->db->where("DATE_FORMAT(pp.fecha,'%m')","0".$mes);

        $this->db->where("pp.tipo_incidencia",5);
        $this->db->where("pp.estatus",1);
        if($group==1){
            $this->db->group_by("causa");
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function get_data_head($id_proy,$anio,$mes){
        if(intval($mes>9))
            $w2="and DATE_FORMAT(ip.fecha,'%m')=".$mes."";
        else
            $w2="and DATE_FORMAT(ip.fecha,'%m')=0".$mes."";
        if(intval($mes>9))
            $w21="and DATE_FORMAT(ip1.fecha,'%m')=".$mes."";
        else
            $w21="and DATE_FORMAT(ip1.fecha,'%m')=0".$mes."";
        if(intval($mes>9))
            $w2_2="and DATE_FORMAT(ip2.fecha,'%m')=".$mes."";
        else
            $w2_2="and DATE_FORMAT(ip2.fecha,'%m')=0".$mes."";
        if(intval($mes>9))
            $w2_3="and DATE_FORMAT(ip3.fecha,'%m')=".$mes."";
        else
            $w2_3="and DATE_FORMAT(ip3.fecha,'%m')=0".$mes."";

        $this->db->select('pr.*, ps.puesto, ps.id as id_puesto,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip 
             left join personal p on p.personalId=ip.id_personal 
             where ip.estatus=1 and ip.tipo_incidencia=4 and ip.id_proyecto=pr.id_proyecto and p.puesto=pr.puesto and DATE_FORMAT(ip.fecha,"%Y")='.$anio.' '.$w2.') as tot_inca,
            (select COUNT(DISTINCT(id_personal)) from incidencia_pers_proy as ip1 
             left join personal p on p.personalId=ip1.id_personal 
             where ip1.estatus=1 and ip1.tipo_incidencia=4 and ip1.id_proyecto=pr.id_proyecto and p.puesto=pr.puesto and DATE_FORMAT(ip1.fecha, "%Y")='.$anio.' '.$w21.') as tot_inca_pers,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip2 
             left join personal p2 on p2.personalId=ip2.id_personal
            where ip2.estatus=1 and ip2.tipo_incidencia=1 and ip2.id_proyecto=pr.id_proyecto and p2.puesto=pr.puesto and DATE_FORMAT(ip2.fecha,"%Y")='.$anio.' '.$w2_2.') as tot_falta,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip3 
             left join personal p3 on p3.personalId=ip3.id_personal
            where ip3.estatus=1 and ip3.tipo_incidencia=5 and ip3.id_proyecto=pr.id_proyecto and p3.puesto=pr.puesto and DATE_FORMAT(ip3.fecha,"%Y")='.$anio.' '.$w2_3.') as tot_bajas');
        $this->db->from("puesto_rotacion pr");
        $this->db->join('puestos ps','ps.id=pr.puesto','left');
        $this->db->where("pr.id_proyecto",$id_proy);
        $this->db->where("pr.estatus",1);
        $query=$this->db->get();
        return $query->result();
    }

    /*public function get_data_head($id_proy,$anio){
        $this->db->select('pr.*, 
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip 
             left join personal p on p.personalId=ip.id_personal 
             where ip.estatus=1 and ip.tipo_incidencia=4 and ip.id_proyecto=pr.id_proyecto and p.puesto=pr.puesto and DATE_FORMAT(ip.fecha,"%Y")='.$anio.') as tot_inca,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip2 
             left join personal p2 on p2.personalId=ip2.id_personal
            where ip2.estatus=1 and ip2.tipo_incidencia=1 and ip2.id_proyecto=pr.id_proyecto and p2.puesto=pr.puesto and DATE_FORMAT(ip2.fecha,"%Y")='.$anio.') as tot_falta,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip3 
             left join personal p3 on p3.personalId=ip3.id_personal
            where ip3.estatus=1 and ip3.tipo_incidencia=5 and ip3.id_proyecto=pr.id_proyecto and p3.puesto=pr.puesto and DATE_FORMAT(ip3.fecha,"%Y")='.$anio.') as tot_bajas,
            IFNULL((select WEEKOFYEAR(ip.fecha) AS semana from incidencia_pers_proy as ip 
             left join personal p on p.personalId=ip.id_personal 
             where ip.estatus=1 and ip.tipo_incidencia=4 and ip.id_proyecto=pr.id_proyecto and p.puesto=pr.puesto),0) as num_semana,
            IFNULL((select WEEKOFYEAR(ip2.fecha) AS semana from incidencia_pers_proy as ip2 
             left join personal p2 on p2.personalId=ip2.id_personal
             where ip2.estatus=1 and ip2.tipo_incidencia=1 and ip2.id_proyecto=pr.id_proyecto and p2.puesto=pr.puesto),0) as num_semana2');
        $this->db->from("puesto_rotacion pr");
        $this->db->where("pr.id_proyecto",$id_proy);
        $this->db->where("pr.estatus",1);
        $query=$this->db->get();
        return $query->result();
    }*/

    /*public function get_data_salida_semana($id_proy,$semana,$puesto){
        $this->db->select('pr.puesto, pr.objetivo, '.$semana.' as semana,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip2 
             left join personal p2 on p2.personalId=ip2.id_personal
            where ip2.estatus=1 and WEEKOFYEAR(ip2.fecha)='.$semana.' and ip2.tipo_incidencia=1 and ip2.id_proyecto=pr.id_proyecto and p2.puesto='.$puesto.') as tot_falta,

            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip3 
             left join personal p3 on p3.personalId=ip3.id_personal
            where ip3.estatus=1 and WEEKOFYEAR(ip3.fecha)='.$semana.' and ip3.tipo_incidencia=5 and ip3.id_proyecto=pr.id_proyecto and p3.puesto='.$puesto.') as tot_bajas');
        $this->db->from("puesto_rotacion pr");
        $this->db->where("pr.id_proyecto",$id_proy);
        $this->db->where("pr.puesto",$puesto);
        $this->db->where("pr.estatus",1);
        $query=$this->db->get();
        return $query->result();
    }*/

    public function get_data_salida_semana($id_proy,$semana,$p_sem,$mes/*,$puesto*/,$anio){
        $this->db->select('pr.puesto, pr.objetivo, '.$semana.' as semana, ps.puesto, pr.puesto as id_puesto,
            (select COUNT(*) from empleados_proyecto as ep 
            join personal p on p.personalId=ep.id_empleado and p.estatus=1
            where (ep.estatus=1 and WEEKOFYEAR(ep.fecha_asignacion)<='.$semana.'
            and DATE_FORMAT(ep.fecha_asignacion,"%Y")<='.$anio.' or 
            ep.estatus=1 and DATE_FORMAT(ep.fecha_asignacion,"m")<='.$mes.'
            and DATE_FORMAT(ep.fecha_asignacion,"%Y")<='.$anio.')
            and ep.id_proyecto='.$id_proy.' and ep.tipo_empleado=1 and p.puesto=pr.puesto) as tot_activos,

            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip2 
             left join personal p2 on p2.personalId=ip2.id_personal and p2.estatus=1
            where ip2.estatus=1 and WEEKOFYEAR(ip2.fecha)='.$semana.' and DATE_FORMAT(ip2.fecha,"%Y")='.$anio.' and ip2.tipo_incidencia=1 and ip2.id_proyecto=pr.id_proyecto and p2.puesto=pr.puesto) as tot_falta,

            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip3 
             left join personal p3 on p3.personalId=ip3.id_personal and p3.estatus=0
            where ip3.estatus=1 and WEEKOFYEAR(ip3.fecha)='.$semana.' and DATE_FORMAT(ip3.fecha,"%Y")='.$anio.' and ip3.tipo_incidencia=5 and ip3.id_proyecto=pr.id_proyecto and p3.puesto=pr.puesto) as tot_bajas,

            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip4 
             left join personal p4 on p4.personalId=ip4.id_personal and p4.estatus=1
            where ip4.estatus=1 and WEEKOFYEAR(ip4.fecha)='.$semana.' and DATE_FORMAT(ip4.fecha,"%Y")='.$anio.' and ip4.tipo_incidencia=6 and ip4.id_proyecto=pr.id_proyecto and p4.puesto=pr.puesto) as tot_vacas,

            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip5 
             left join personal p5 on p5.personalId=ip5.id_personal and p5.estatus=1
            where ip5.estatus=1 and WEEKOFYEAR(ip5.fecha)='.$semana.' and DATE_FORMAT(ip5.fecha,"%Y")='.$anio.' and ip5.tipo_incidencia=4 and ip5.id_proyecto=pr.id_proyecto and p5.puesto=pr.puesto) as tot_inca');
        $this->db->from("puesto_rotacion pr");
        $this->db->join('puestos ps','ps.id=pr.puesto','left');
        $this->db->where("pr.id_proyecto",$id_proy);
        //$this->db->where("pr.puesto",$puesto);
        $this->db->where("pr.estatus",1);
        $query=$this->db->get();
        return $query->result();
    }

    public function get_data_salida_semana2($id_proy,$semana,$puesto,$anio){
        $this->db->select('pr.objetivo, '.$semana.' as semana, ps.puesto, pr.id as id_puesto,
            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip2 
             left join personal p2 on p2.personalId=ip2.id_personal and p.estatus=1
            where ip2.estatus=1 and WEEKOFYEAR(ip2.fecha)='.$semana.' and DATE_FORMAT(ip2.fecha,"%Y")='.$anio.' and ip2.tipo_incidencia=1 and ip2.id_proyecto=pr.id_proyecto and p2.puesto=pr.puesto) as tot_falta,

            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip3 
             left join personal p3 on p3.personalId=ip3.id_personal and p3.estatus=1
            where ip3.estatus=1 and WEEKOFYEAR(ip3.fecha)='.$semana.' and DATE_FORMAT(ip3.fecha,"%Y")='.$anio.' and ip3.tipo_incidencia=5 and ip3.id_proyecto=pr.id_proyecto and p3.puesto=pr.puesto) as tot_bajas,

            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip4 
             left join personal p4 on p4.personalId=ip4.id_personal and p4.estatus=1
            where ip4.estatus=1 and WEEKOFYEAR(ip4.fecha)='.$semana.' and DATE_FORMAT(ip4.fecha,"%Y")='.$anio.' and ip4.tipo_incidencia=6 and ip4.id_proyecto=pr.id_proyecto and p4.puesto=pr.puesto) as tot_vacas,

            (select COUNT(tipo_incidencia) from incidencia_pers_proy as ip5 
             left join personal p5 on p5.personalId=ip5.id_personal and p5.estatus=1
            where ip5.estatus=1 and WEEKOFYEAR(ip5.fecha)='.$semana.' and DATE_FORMAT(ip5.fecha,"%Y")='.$anio.' and ip5.tipo_incidencia=4 and ip5.id_proyecto=pr.id_proyecto and p5.puesto=pr.puesto) as tot_inca');
        $this->db->from("puesto_rotacion pr");
        $this->db->join('puestos ps','ps.id=pr.puesto','left');
        $this->db->where("pr.id_proyecto",$id_proy);
        $this->db->where("pr.puesto",$puesto);
        $this->db->where("pr.estatus",1);
        $query=$this->db->get();
        return $query->result();
    }

    public function get_data_vacaciones($id_proy,$mes,$anio,$semana){
        $this->db->select('ip.*, p.nombre, WEEKOFYEAR(ip.fecha) AS semana, count(tipo_incidencia) as tot_vacas,
                        GROUP_CONCAT(WEEKOFYEAR(fecha) ORDER BY fecha asc SEPARATOR ", " ) as semana2'); 
        $this->db->from("incidencia_pers_proy ip");
        $this->db->join("personal p","p.personalId=ip.id_personal");
        $this->db->where("ip.id_proyecto",$id_proy);
        $this->db->where("ip.tipo_incidencia",6);
        $this->db->where("ip.estatus",1);
        $this->db->where("DATE_FORMAT(fecha,'%Y')",$anio);
        $this->db->where("WEEKOFYEAR(fecha)",$semana);
        if(intval($mes)>9){
            $this->db->where("DATE_FORMAT(fecha,'%m')",$mes);
        }
        else{
            $this->db->where("DATE_FORMAT(fecha,'%m')","0".$mes);
            $this->db->or_where("DATE_FORMAT(fecha,'%m')",$mes);
        }
        
        $this->db->group_by("ip.id_personal");
        $this->db->order_by("ip.id_personal","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function get_data_vacaciones2($id_proy,$mes,$anio,$where){
        $this->db->select('ip.*, p.nombre, WEEKOFYEAR(ip.fecha) AS semana, count(tipo_incidencia) as tot_vacas,
                        GROUP_CONCAT(WEEKOFYEAR(fecha) ORDER BY fecha asc SEPARATOR ", " ) as semana2,
                        GROUP_CONCAT(fecha ORDER BY fecha asc SEPARATOR ", " ) as fechas');
        $this->db->from("incidencia_pers_proy ip");
        $this->db->join("personal p","p.personalId=ip.id_personal");
        $this->db->where("ip.id_proyecto",$id_proy);
        $this->db->where("ip.tipo_incidencia",6);
        $this->db->where("ip.estatus",1);
        $this->db->where("DATE_FORMAT(fecha,'%Y')",$anio);
        $this->db->where($where);
        if(intval($mes)>9){
            $this->db->where("DATE_FORMAT(fecha,'%m')",$mes);
        }
        else{
            //$this->db->where("DATE_FORMAT(fecha,'%m')","0".$mes);
            $this->db->where("(DATE_FORMAT(fecha,'%m')=0".$mes." or DATE_FORMAT(fecha,'%m')=".$mes.")");
            //$this->db->or_where("DATE_FORMAT(fecha,'%m')",$mes);
        }
        
        $this->db->group_by("ip.id_personal");
        $this->db->order_by("ip.id_personal","asc");
        $query=$this->db->get();
        return $query->result();
    }

    public function get_graph_presentismo($id_proy,$mes,$anio){
        $this->db->select('*');
        $this->db->from("graficas_presentismo");
        $this->db->where("id_proyecto",$id_proy);
        $this->db->where("anio",$anio);
        if(intval($mes)>9){
            $this->db->where("mes",$mes);
        }
        else{
            $this->db->where("(mes=0".$mes." or mes=".$mes.")");
            //$this->db->or_where("mes",$mes);
        }

        $query=$this->db->get();
        return $query->result();
    }

}