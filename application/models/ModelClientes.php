<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelClientes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'c.id',
            1=>'c.empresa',
            2=>'c.contacto',
            3=>'c.telefono',
            4=>'c.correo',
            5=>'c.direccion',
            6=>'c.num_cliente',
            7=>'c.foto'
            //8=>'(select MAX(id) as total from clientes WHERE `c`.`activo` = 1) as total'
        );

        /*$columnss = array( 
            0=>'c.id',
            1=>'c.empresa',
            2=>'c.contacto',
            3=>'c.telefono',
            4=>'c.correo',
            5=>'c.direccion',
            6=>'c.num_cliente',
            7=>'c.foto',
            8=>'(select MAX(id) as total from clientes WHERE `c`.`activo` = 1)'
        );*/

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('clientes c');
        $where = array('c.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $columns = array( 
            0=>'c.id',
            1=>'c.empresa',
            2=>'c.contacto',
            3=>'c.telefono',
            4=>'c.correo',
            5=>'c.direccion',
            6=>'c.num_cliente',
            7=>'c.foto'
        );
        /*$columnss = array( 
            0=>'c.id',
            1=>'c.empresa',
            2=>'c.contacto',
            3=>'c.telefono',
            4=>'c.correo',
            5=>'c.direccion',
            6=>'c.num_cliente',
            7=>'c.foto',
            8=>'(select MAX(id) as total from clientes WHERE `c`.`activo` = 1)'
        );*/
        $this->db->select('COUNT(1) as total');
        $this->db->from('clientes c');
        $where = array('c.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


    //Mis clientes---------------
    function get_resultM($params,$clienteId){
        $columns = array( 
            0=>'c.id',
            1=>'c.contacto',
            2=>'c.correo',
            3=>'c.clienteId',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);

        /*
        SELECT `c`.`id`, `c`.`clienteId`, `c`.`contacto`, `c`.`correo`
        FROM `clientes_clientes` `c`
        WHERE `c`.`activo` = 1
        AND `c`.`clienteId` = 23
        ORDER BY `c`.`id` ASC
        */

        $this->db->from('clientes_clientes c');
        $where = array('c.clienteId'=>$clienteId,'c.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_resultM($params,$clienteId){
        $columns = array( 
            0=>'c.id',
            1=>'c.contacto',
            2=>'c.correo',
        );

        $this->db->select('COUNT(1) as total');
        $this->db->from('clientes_clientes c');
        $where = array('c.clienteId'=>$clienteId,'c.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}