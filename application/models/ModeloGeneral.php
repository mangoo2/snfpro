<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGeneral extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function tabla_inserta($tabla,$data){
        $this->db->insert($tabla,$data);   
        return $this->db->insert_id();
    }
    function insert_batch($Tabla,$data){
        $this->db->insert_batch($Tabla, $data);
    } 

    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhere2($tables,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);/// array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function getselectwhereOrWhere($tables,$where,$or_where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->where($or_where);
        $query=$this->db->get();
        return $query;
    }

    public function getselectwhere2Order($tables,$where,$order){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);/// array para n condiciones
        $this->db->order_by($order,"asc");
        $query=$this->db->get();
        return $query;
    }

    public function getselectwhereN($tables,$select,$cols,$values){
        $this->db->select($select);
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);/// Se ocupa para n select
        $this->db->update($catalogo);
        return $id;
    }

    function updateCatalogo_value($data,$info,$catalogo){
        $this->db->set($data);
        $this->db->where($info);/// Se ocupa para n select
        $this->db->update($catalogo);
    }

    public function getselectwhere_n_consulta($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwhere_n_consulta2($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function getselect($tables){
        $this->db->select("*");
        $this->db->from($tables);
        //$this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    function getData(){
        $datos = $this->db->get('tabla');
        return $datos->result();
    } 

    public function update_foto($data,$id,$tabla) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update($tabla); 
    }

    public function getselectwhererow($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query->row();
    }

    public function getselectwhererow2($tables,$value){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($value);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query->row();
    }

    public function getselect_like($tables,$title,$value){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->like($title,$value);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getselectwhere_tipo($select,$tables,$cols,$values){
        $this->db->select($select);
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    public function getHallazgoCliente($where,$tipo){
        
        if($tipo==1){
            $this->db->select("hallazgo, tipo_hallazgo, fecha_compromiso, c.correo");
            $this->db->from("matriz_acciones ma");
            //$this->db->join('empleados_proyecto ep','ep.id=ma.id_resp');
            $this->db->join('proyectos p','p.id=ma.id_proyecto and p.estatus=1');
            $this->db->join('personal c','c.personalId=ma.id_resp');
            $this->db->where($where);
            $this->db->where("ma.avance!=",4);
            $this->db->where("ma.estatus",1);
        }else{
            $this->db->select("hallazgo, tipo_hallazgo, fecha_compromiso, c.correo");
            $this->db->from("matriz_acciones ma");
            $this->db->join("clientes c","c.id=ma.id_cliente");
            $this->db->join('proyectos p','p.id=ma.id_proyecto and p.estatus=1');
            $this->db->where($where);
            $this->db->where("ma.avance!=",4);
            $this->db->where("ma.estatus",1);
        }
        $query=$this->db->get();
        return $query->result();
    }

    public function getCorreosCliente($id){
        
        $this->db->select("correo");
        $this->db->from("clientes_contactos cc");
        $this->db->join("matriz_acciones ma","ma.id_cliente = cc.clienteId");
        $this->db->where("ma.id",$id);//"ma.id"=>$id
        $this->db->where("cc.estatus",1);

        $query=$this->db->get();
        return array_column($query->result_array(), 'correo');
    }

    public function getCorreosClienteProy($id){
        
        $this->db->select("correo");
        $this->db->from("clientes_contactos cc");
        $this->db->join("proyectos p","p.id_cliente = cc.clienteId");
        $this->db->where("p.id",$id);
        $this->db->where("cc.estatus",1);

        $query=$this->db->get();
        return array_column($query->result_array(), 'correo');
    }

    public function getLogoCli($id_proy){
        $this->db->select("c.foto");
        $this->db->from("proyectos p");
        $this->db->join('clientes c','c.id=p.id_cliente');
        $this->db->where("p.id",$id_proy);
        $query=$this->db->get();
        return $query->row();
    }

    public function getDatoHallazgo($id){
        $this->db->select("ma.*, c.empresa, c.correo");
        $this->db->from("matriz_acciones ma");
        $this->db->join("clientes c","c.id=ma.id_cliente");
        $this->db->where("ma.id",$id);
        $query=$this->db->get();
        return $query->row();
    }

    public function insertToCatalogoN($data, $catalogo) {
        $this->db->insert('' . $catalogo, $data);
        $id=$this->db->insert_id();
        return $id;
    }

    public function updateCatalogoN($data,$where,$catalogo) {
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($catalogo);
    }

}