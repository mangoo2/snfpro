<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function validar_correos($fecha){
        $sql = "SELECT id,avance,tipo_hallazgo,DATEDIFF('$fecha',fecha_deteccion) AS dias
            FROM matriz_acciones WHERE avance<=3 and estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function enviar_correos($fecha){
        $sql = "SELECT ma.id,ma.tipo_hallazgo
            FROM bitacora_correo AS bc
            INNER JOIN bitacora_correo_detalles AS bcd ON bcd.idbitacora_correo=bc.id
            INNER JOIN matriz_acciones AS ma ON ma.id =bcd.idmatriz_acciones and estatus=1
            WHERE bc.fecha='$fecha'";
        $query = $this->db->query($sql);
        return $query->result();
    }

}