<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelServicios extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_result($params){
        $columns = array( 
            0=>'s.id',
            1=>'s.servicio',
            2=>'s.descripcion',
            3=>'p.nombre',
            4=>'s.reg',
            5=>'DATE_FORMAT(s.reg, "%d/%m/%Y - %r" ) AS reg2'
        );

        $columnsx = array( 
            0=>'s.id',
            1=>'s.servicio',
            2=>'s.descripcion',
            3=>'p.nombre',
            4=>'s.reg',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('servicios s');
        $this->db->join('personal p','p.personalId=s.personalId');
        $where = array('s.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsx as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsx[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_result($params){
        $columns = array( 
            0=>'s.id',
            1=>'s.servicio',
            2=>'s.descripcion',
            3=>'p.nombre',
            4=>'s.reg',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('servicios s');
        $this->db->join('personal p','p.personalId=s.personalId');
        $where = array('s.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
}