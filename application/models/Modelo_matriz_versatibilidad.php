<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelo_matriz_versatibilidad extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function empleado_proyecto($id){
        $sql = "SELECT 
                    per.personalId,
                    per.nombre 
                FROM empleados_proyecto as emplep 
                INNER JOIN personal as per on per.personalId=emplep.id_empleado and per.estatus=1 
                WHERE emplep.id_proyecto=$id AND emplep.tipo_empleado=1 and emplep.estatus=1";
        $query = $this->db->query($sql);
        return $query;
    }
    function getDatalist($params){
        $proyecto=$params['proyecto'];
        $columns = array( 
            0=>'mv.idmatriz',
            1=>'per.nombre',
            2=>'mv.cumplimiento',
            3=>'mv.objetivo',
            4=>'mv.faltante',
            5=>'mv.reg',
            6=>'mv.idempleado',
            7=>'mv.fecha_update'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('matriz_versatibilidad mv');
        $this->db->join('personal per','per.personalId=mv.idempleado and per.estatus=1');
            
        $where = array('mv.activo'=>1,'mv.idproyecto'=>$proyecto);
        $this->db->where($where);
        $this->db->group_by('idempleado');

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function getDatalist_total($params){
        $proyecto=$params['proyecto'];
        $columns = array( 
            0=>'mv.idmatriz',
            1=>'per.nombre',
            2=>'mv.cumplimiento',
            3=>'mv.objetivo',
            4=>'mv.faltante',
            5=>'mv.reg',
            6=>'mv.fecha_update'
        );
        $this->db->select('*');
        $this->db->from('matriz_versatibilidad mv');
        $this->db->join('personal per','per.personalId=mv.idempleado and per.estatus=1');
            
        $where = array('mv.activo'=>1,'mv.idproyecto'=>$proyecto);
        $this->db->where($where);
        $this->db->group_by('idempleado');
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        
        return $query->num_rows();
    }
    function list_personal_proyecto($id_proy){
        $sql = "SELECT 
                    per.personalId,
                    per.nombre,
                    per.foto,
                    per.genero,
                    mv.cumplimiento, 
                    mv.objetivo, 
                    mv.faltante, 
                    mv.reg, 
                    mv.idempleado,
                    mv.idproyecto,
                    mv.fecha_update,
                    IFNULL(ip.tipo_incidencia,'0') as tipo_incidencia
                FROM matriz_versatibilidad as mv
                JOIN personal as per ON per.personalId=mv.idempleado and estatus=1
                LEFT JOIN incidencia_pers_proy as ip ON ip.id_personal=mv.idempleado and tipo_incidencia=5 and ip.estatus=1
                WHERE mv.activo = 1 AND mv.idproyecto = '$id_proy'
                ";
        $query = $this->db->query($sql);
        return $query;
    }
    function list_actividades_proyecto($id_proy){
        $sql = "SELECT * 
                FROM limpieza_actividades 
                WHERE id_proyecto=$id_proy AND estatus=1 ORDER BY id ASC";
        $query = $this->db->query($sql);
        return $query;
    }
    function list_actividades_empleado($id_proy,$id_empleado){
        $sql = "SELECT 
                    mv.idproyecto,
                    mv.idempleado,
                    li.id,
                    mvd.valor, 
                    mvd.estatus 
                    FROM matriz_versatibilidad as mv 
                    INNER JOIN matriz_versatibilidad_detalle as mvd on mvd.idmatriz=mv.idmatriz 
                    LEFT JOIN limpieza_actividades as li on li.id=mvd.idactividad and li.estatus=1 and li.id_proyecto=mv.idproyecto AND mvd.activo=1 
                WHERE mv.idproyecto='$id_proy' and mv.activo = 1 and mvd.valor>0 AND mv.idempleado='$id_empleado' 
                
                ORDER BY `li`.`id` ASC";
        $query = $this->db->query($sql);
        return $query;
    }
    function list_actividades_empleado2($id_proy,$id_empleado){
        $sql = "SELECT 
                    mv.idproyecto, 
                    mv.idempleado, 
                    li.id, mvd.valor, 
                    mvd.estatus 
                FROM limpieza_actividades as li 
                LEFT JOIN matriz_versatibilidad as mv on li.id_proyecto=mv.idproyecto and mv.activo = 1 
                LEFT JOIN matriz_versatibilidad_detalle as mvd on mvd.idmatriz=mv.idmatriz and li.id=mvd.idactividad AND mvd.activo=1 
                WHERE li.estatus=1 AND mv.idproyecto='$id_proy' AND mv.idempleado='$id_empleado' 
                
                ORDER BY `li`.`id` ASC";
        $query = $this->db->query($sql);
        return $query;
    }

    
    public function getMVRN($id){
        $this->db->select("mvr.*");
        $this->db->from("matriz_versatibilidad_resultados_n mvr");
        $this->db->where("idproyecto",$id);
        $this->db->order_by("row","ASC");
        $query=$this->db->get();
        return $query->result();
    }
    
}