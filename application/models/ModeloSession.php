<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechaactual = date('Y-m-d');
        $this->hora = date('G:i:s');
    }

    function login($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,perf.nombre AS perfil_nombre,per.puesto,per.foto, per.empresa, per.personalId_reg, per.correo
                FROM usuarios AS usu
                INNER JOIN perfiles AS perf ON perf.perfilId = usu.perfilId
                INNER JOIN personal AS per ON per.personalId = usu.personalId
                WHERE usu.estatus = 1 AND perf.estatus = 1 AND usu.Usuario = '$usu'";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);

        $puesto="";
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId;  
            $perfil_nombre = $row->perfil_nombre;
            $puesto = $row->puesto;
            $correo = $row->correo;
            if($row->puesto==1)
                $puesto="Ajustador";
            else if($row->puesto==2)
                $puesto="Filtración";
            else if($row->puesto==3)
                $puesto="Técnico de limpieza";
            else if($row->puesto==4)
                $puesto="Otro";

            $empresa = $row->empresa;
            $personalId_reg = $row->personalId_reg;
            $foto = $row->foto;
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid_tz' => $id,
                        'usuario_tz' => $nom,
                        'perfilid_tz'=>$perfil,
                        'idpersonal_tz'=>$idpersonal,
                        'perfil_nombre'=>$perfil_nombre,
                        'tipo'=>1,
                        'foto'=>$foto,
                        'puesto'=>$puesto,
                        'empresa'=>$empresa,
                        'personalId_reg'=>$personalId_reg,
                        "correo"=>$correo,
                        "activ_monitor"=>0,
                        "tipo_reg_monitor"=>0,
                        "semana_orden"=>0,
                        "activ_orden"=>0,
                        "cuenta"=>0
                    );
                $this->session->set_userdata($data);
                $count=1;
            }
        } 

        
        $strq2 = "SELECT cli.id AS UsuarioID,0 AS personalId,cli.empresa AS nombre,5 AS perfilId, cli.contrasena,'cliente' AS perfil_nombre,cli.foto
                FROM clientes AS cli
                WHERE cli.activo=1 AND cli.correo = '$usu'";
        $query2 = $this->db->query($strq2);

        foreach ($query2->result() as $row2) {
            $passwo =$row2->contrasena;
            $id = $row2->UsuarioID;
            $id_cliente = $row2->UsuarioID;
            $nom =$row2->nombre;
            $perfil = $row2->perfilId; 
            $idpersonal = $row2->personalId;  
            $perfil_nombre = $row2->perfil_nombre;
            $foto=$row2->foto;
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid_tz' => $id,
                        //'id_cliente' => $id_cliente,
                        'usuario_tz' => $nom,
                        'perfilid_tz'=>$perfil,
                        'idpersonal_tz'=>$idpersonal,
                        'perfil_nombre'=>$perfil_nombre,
                        'tipo'=>2,
                        'foto'=>$foto,
                    );
                $this->session->set_userdata($data);
                $count=1;
            }
        } 

        $strq3 = "SELECT clic.id AS UsuarioID,0 AS personalId,5 AS perfilId, clic.contrasena,'cliente' AS perfil_nombre,clic.clienteId AS 'cliente_id'
                FROM clientes_clientes AS clic
                WHERE clic.activo=1 AND clic.correo = '$usu'";
        $query3 = $this->db->query($strq3);

        foreach ($query3->result() as $row3) {
            $passwo =$row3->contrasena;
            $id = $row3->UsuarioID;
            $id_cliente = $row3->id;
            $nom = "Usuario";
            $perfil = $row3->perfilId; 
            $idpersonal = $row3->personalId;  
            $perfil_nombre = $row3->perfil_nombre;
            $cliente_id = $row3->cliente_id;

            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid_tz' => $id,
                        //'id_cliente' => $id_cliente,
                        'usuario_tz' => $nom,
                        'perfilid_tz'=>$perfil,
                        'idpersonal_tz'=>$idpersonal,
                        'perfil_nombre'=>$perfil_nombre,
                        'tipo'=>3,
                        'clienteid' => $cliente_id,
                    );
                $this->session->set_userdata($data);
                $count=1;
            }
        } 
        echo $count;
    }


    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd 
        where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.orden ASC";
        $query = $this->db->query($strq);
        return $query;

    }
    
    public function submenus($perfil,$menu,$tipo){

        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
        from menu_sub as menus
        inner join perfiles_detalles as perfd on perfd.MenusubId=menus.MenusubId
        WHERE  perfd.PerfilId='$perfil' AND menus.tipo='$tipo' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        return $query;

    }

    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM perfiles_detalles WHERE perfilId=$perfil AND MenusubId=$modulo";
        //log_message('error', $strq);
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    } 

    public function get_proyectos($perfil){
        $strq ="SELECT p.id,p.id_servicio,p.fecha_ini,p.fecha_fin,s.servicio 
        from proyectos AS p
        INNER JOIN servicios AS s ON s.id = p.id_servicio
        where p.id_cliente='$perfil'
        and p.estatus=1";
        $query = $this->db->query($strq);
        return $query->result();

    }

}
