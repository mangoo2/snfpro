<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelEmpleado extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function getUsuarios_user($id){
        $sql = "SELECT u.UsuarioID, u.Usuario,pe.personalId, pe.nombre, p.perfilId, p.nombre AS perfil, u.contrasena FROM usuarios AS u
                INNER JOIN perfiles AS p ON p.perfilId = u.perfilId
                INNER JOIN personal AS pe ON pe.personalId = u.personalId
                WHERE u.personalId = $id";
        $query = $this->db->query($sql);
        return $query->result();
    } 

    function get_result($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'ps.puesto',
            3=>'p.correo',
            4=>'u.Usuario',
            5=>'per.nombre',
            6=>'p.empresa'
        );
        $columnsx = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'ps.puesto',
            3=>'p.correo',
            4=>'u.Usuario',
            5=>'per.nombre as perfil',
            6=>'p.foto',
            7=>'p.empresa'
        );
        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId','left');
        $this->db->join('perfiles per','per.perfilId=p.perfilId','left');
        $this->db->join('puestos ps','ps.id=p.puesto','left');

        $where = array('p.estatus'=>1);
        $this->db->where($where);
        $this->db->where('p.personalId!=1');
        if(isset($params["empresa"]) && $params["empresa"]!="0" && $this->session->userdata("usuarioid_tz")=="1"){
            $this->db->where("empresa",$params["empresa"]);
        }else if($this->session->userdata("usuarioid_tz")!="1"){
            $this->db->where("empresa",$this->session->userdata("empresa")); 
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_result($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'ps.puesto',
            3=>'p.correo',
            4=>'u.Usuario',
            5=>'per.nombre',
            6=>'p.empresa'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId','left');
        $this->db->join('perfiles per','per.perfilId=u.perfilId','left');
        $this->db->join('puestos ps','ps.id=p.puesto','left');

        $where = array('p.estatus'=>1);
        $this->db->where($where);
        $this->db->where('p.personalId!=1');
        if(isset($params["empresa"]) && $params["empresa"]!="0" && $this->session->userdata("usuarioid_tz")=="1"){
            $this->db->where("empresa",$params["empresa"]);
        }else if($this->session->userdata("usuarioid_tz")!="1"){
            $this->db->where("empresa",$this->session->userdata("empresa")); 
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function searchPersonal($tabla,$col,$data,$where){
        $this->db->select("*");
        $this->db->from($tabla);
        $this->db->where($where);
        $this->db->like($col,$data);
        $query=$this->db->get();
        return $query->result();
    }

    public function searchPersonal2($tabla,$col,$data,$where,$or_where){
        $this->db->select("*");
        $this->db->from($tabla);
        $this->db->where($where);
        $this->db->or_where($or_where);
        $this->db->like($col,$data);
        $query=$this->db->get();
        return $query->result();
    }

    public function searchServicio($serv){
        $this->db->select("*");
        $this->db->from("servicios");
        $this->db->where("activo",1);
        $this->db->like("servicio",$serv);
        $this->db->or_like("descripcion",$serv);
        $query=$this->db->get();
        return $query->result();
    }


}