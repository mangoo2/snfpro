<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Clientes extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModelClientes');
    $this->idpersonal=$this->session->userdata('idpersonal');
    $this->usuarioid_tz=$this->session->userdata('usuarioid_tz');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,1);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('clientes/index');
    $this->load->view('templates/footer');
    $this->load->view('clientes/indexjs');
  }

  public function registro($id=0)
  {
    if($id==0){
      $data['tittle']='Alta';
      $data['contactos'] = $this->ModeloGeneral->getselectwhere2("clientes_contactos", array("clienteId" => $id,"estatus" => 1)); 
      $data['id']=0;
      $data['empresa']='';
      $data['contacto']='';
      $data['telefono']='';
      $data['direccion']='';
      $data['num_cliente']='';
      $data['foto']='';
      $data['contrasena']='';
      $data['correo']='';
      //$max_id=$this->ModeloGeneral->getselectwhereN('clientes','MAX(id) as id','activo=',2);
      //$data['num_cliente']=$max_id[0]->id+1;
      $cont_reg=$this->ModeloGeneral->getselectwhere2('clientes',array('activo'=>1));
      $tot_reg=$cont_reg->num_rows()+1;
      $data['num_cliente']=$tot_reg;
    }else{
      $data['tittle']='Edición'; 
      $data['contactos'] = $this->ModeloGeneral->getselectwhere2("clientes_contactos", array("clienteId" => $id,"estatus" => 1));
      $resul=$this->ModeloGeneral->getselectwhere('clientes','id',$id);
      foreach ($resul as $x) {
        $data['id']=$x->id;
        $data['empresa']=$x->empresa;
        $data['contacto']=$x->contacto;
        $data['telefono']=$x->telefono;
        $data['direccion']=$x->direccion;
        if($x->num_cliente!=""){
          $data['num_cliente']=$x->num_cliente;
        }
        $data['foto']=$x->foto;
        $data['correo']=$x->correo;
        $data['contrasena']='xxxxxx';
      }
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('clientes/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('clientes/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $pss_verifica = $datos['contrasena'];
    $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
    $datos['contrasena'] = $pass;
    if($pss_verifica == 'xxxxxx'){
       unset($datos['contrasena']);
    }
    unset($datos['contrasena2']);
    $id_reg=0;
    //0=insert,1=editado,2=eliminado
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'clientes');
      $id_reg=$id;
      $datos["tipo_movimiento"]="1";
    }else{
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('clientes',$datos);
      $datos["tipo_movimiento"]="0";
    }
    $datos["id_cliente"]=$id_reg;
    $datos['fecha_movimiento']=$this->fechahoy;
    $datos['id_user_mov']=$this->usuarioid_tz;
    $datos['contrasena'] = $pass;
    $this->ModeloGeneral->tabla_inserta('bitacora_clientes',$datos);
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelClientes->get_result($params);
    $totaldata= $this->ModelClientes->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  
  function cargafiles(){
    $id=$this->input->post('id');
    $folder="clientes";
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $newfile='doc_'.$fecha.$nombre_archivo;
    $newfile=str_replace(" ", "", $newfile);     
    $archivador = $upload_folder . '/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('foto'=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'id',$id,'clientes');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 

  public function delete(){
    $id = $this->input->post('id');
    $data = array('activo' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'clientes');

    //para bitacora de movimientos clientes
    $getcli = $this->ModeloGeneral->getselectwhere2("clientes",array("id"=>$id));
    foreach ($getcli->result() as $x) {
      $datos['id_cliente']=$x->id;
      $datos['empresa']=$x->empresa;
      $datos['contacto']=$x->contacto;
      $datos['telefono']=$x->telefono;
      $datos['direccion']=$x->direccion;
      if($x->num_cliente!=""){
        $datos['num_cliente']=$x->num_cliente;
      }
      $datos['foto']=$x->foto;
      $datos['correo']=$x->correo;
      $datos['contrasena']=$x->contrasena;
      $datos["tipo_movimiento"]="2";
      $datos['fecha_movimiento']=$this->fechahoy;
      $datos['id_user_mov']=$this->usuarioid_tz;
      $this->ModeloGeneral->tabla_inserta('bitacora_clientes',$datos);
    }
    echo $resul;
  }

  function validar(){
    $Usuario = $this->input->post('Usuario');
    $result=$this->ModeloGeneral->getselectwhere('clientes','correo',$Usuario);
    $resultado=0;
    foreach ($result as $row){
        $resultado=1;
    }

    $result2=$this->ModeloGeneral->getselectwhere('usuarios','Usuario',$Usuario);
    foreach ($result2 as $row){
        $resultado=1;
    }

    echo $resultado;
  }

  public function insertUpdate_contactos(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    
    for ($i=0; $i<count($DATA); $i++) {
        $clienteId = $DATA[$i]->clienteId;
        $contacto = $DATA[$i]->contacto;
        $telefono = $DATA[$i]->telefono;
        $correo = $DATA[$i]->correo;
        $estatus = $DATA[$i]->estatus;

        $arr = array("clienteId"=>$clienteId,"contacto"=>$contacto,"telefono"=>$telefono,"correo"=>$correo);
        if($DATA[$i]->id == 0){
            $result = $this->ModeloGeneral->insertToCatalogoN($arr, "clientes_contactos");
        }else{
            $arr = array("clienteId"=>$clienteId,"contacto"=>$contacto,"telefono"=>$telefono,"correo"=>$correo,"estatus"=>$estatus);
            $result = $this->ModeloGeneral->updateCatalogoN($arr, array('id'=>$DATA[$i]->id), "clientes_contactos");
        }
    }
    echo $datos;
  }

}

