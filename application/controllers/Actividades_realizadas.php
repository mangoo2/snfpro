<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Actividades_realizadas extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('Modeloactividadesrealizadas');
    if(!$this->session->userdata('logeado')) {
      redirect('/Sistema');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index(){
    redirect('/Sistema');
  }

  public function listado($id_proy){
    $data['id_proy']=$id_proy;

    $result=$this->Modeloactividadesrealizadas->proyecto($id_proy);
    foreach ($result->result() as $item) {
      $empresa=$item->empresa;
      $fecha_ini=$item->fecha_ini;
      $fecha_fin=$item->fecha_fin;
    }
    $fechai=strtotime($fecha_ini);
    $fechaf=strtotime($fecha_fin);
    $mesi=date('m',$fechai);
    $mesf=date('m',$fechaf);
    $mes=intval($mesi);
    $arraymeses=array();
    /*while($mes<=$mesf){
      $arraymeses[]=array('mes'=>$mes,'mesl'=>$this->nameMes($mes)); 
      $mes=$mes+1;
    }*/
    /*for ($i=1; $i<=12 ; $i++) { 
      $arraymeses[]=array('mes'=>$mes,'mesl'=>$this->nameMes($mes));
      $mes=$mes+1;
    }*/
    $data['meses']=$arraymeses;
    $data['det_dep']=$this->ModeloGeneral->getselectwhere2('departamentos',array('id_proy'=>$id_proy,'activo'=>1));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('actividades_realizadas/list',$data);
    $this->load->view('templates/footer');
    $this->load->view('actividades_realizadas/listjs');
  }

  public function getDataTable(){
    $params = $this->input->post();
    $getdata = $this->Modeloactividadesrealizadas->get_result($params);
    $totaldata= $this->Modeloactividadesrealizadas->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  function add($id_proy,$id_acti=0){
    $data['id_proy']=$id_proy;
    $data['id_acti']=$id_acti;
    $empresa='';
    $fecha_ini='';
    $fecha_fin='';
    $result=$this->Modeloactividadesrealizadas->proyecto($id_proy);
    foreach ($result->result() as $item) {
      $empresa=$item->empresa;
      $fecha_ini=$item->fecha_ini;
      $fecha_fin=$item->fecha_fin;
    }
    $data['empresa']=$empresa;
    $data['fecha_ini']=$fecha_ini;
    $data['fecha_fin']=$fecha_fin;
    $data['det_dep']=$this->ModeloGeneral->getselectwhere2('departamentos',array('id_proy'=>$id_proy,'activo'=>1));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('actividades_realizadas/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('actividades_realizadas/formjs');
  }
  function saveform(){
    $params=$this->input->post();
    $id=$params['id'];
    unset($params['id']);
    unset($params['file1']);
    unset($params['file2']);
    unset($params['file3']);
    if($id>0){
      unset($params['idproyecto']);
      $this->ModeloGeneral->updateCatalogo_value($params,array('id'=>$id),'actividades_realizadas');
    }else{
      $id=$this->ModeloGeneral->tabla_inserta('actividades_realizadas',$params);
    }
    echo $id;

  }
  function cargaimagen(){
    $filetipo=$_POST['filetipo'];
    $actividad=$_POST['actividad'];

    $input_name='file'.$filetipo;
    $DIR_SUC=FCPATH.'uploads/files_ar';
    $config['upload_path']          = $DIR_SUC;
    $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
    $config['max_size']             = 5000;
    $file_names=date('YmdGis');
    $config['file_name']=$file_names;       
    $output = [];
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload($input_name)){
      $data = array('error' => $this->upload->display_errors());
      log_message('error', json_encode($data));                    
    }else{
      $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
      $file_name = $upload_data['file_name']; //uploded file name
      $extension=$upload_data['file_ext'];    // uploded file extension
      
      $this->resizeImage($file_name);
      $name_reg="reg_".$input_name."";
      $name_reg=str_replace(" ", "", $name_reg);
      $this->ModeloGeneral->updateCatalogo_value(array($input_name=>$file_name,$name_reg=>date("Y-m-d H:i:s")),array('id'=>$actividad),'actividades_realizadas');

      $data = array('upload_data' => $this->upload->data());
      //$output = [];
      //log_message('error', json_encode($data));
    }
    echo json_encode($output);
  }
  function resizeImage($filename){
      $source_path = FCPATH . 'uploads/files_ar/' . $filename;
      $target_path = FCPATH . 'uploads/files_ar/thumbnail/';
      $config_manip = array(
          'image_library' => 'gd2',
          'source_image' => $source_path,
          'new_image' => $target_path,
          'maintain_ratio' => TRUE,
          'create_thumb' => TRUE,
          'thumb_marker' => '',
          'width' => 150,
          'height' => 150
      );


      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }


      $this->image_lib->clear();
  }
  function obtenerdatosform(){
    $params = $this->input->post();
    $id=$params['id'];
    $result=$this->ModeloGeneral->getselectwhere2('actividades_realizadas',array('id'=>$id));

    echo json_encode($result->row());
  }
  function delete(){
    $params = $this->input->post();
    $id = $params['id'];
    $this->ModeloGeneral->updateCatalogo_value(array('activo'=>0),array('id'=>$id),'actividades_realizadas');
  }
  function savedepartamento(){
    $params = $this->input->post();
    $id=$params['iddep'];
    if($id>0){
      $this->ModeloGeneral->updateCatalogo_value(array('departamento'=>$params['dep']),array('id'=>$id),'departamentos');
    }else{
      $this->ModeloGeneral->tabla_inserta('departamentos',array('departamento'=>$params['dep'],'id_proy'=>$params['id_proy']));  
    }
    
  }
  function deletedepartemento(){
    $params = $this->input->post();
    $id = $params['id'];
    $this->ModeloGeneral->updateCatalogo_value(array('activo'=>0),array('id'=>$id),'departamentos');
  }
  function obetenerdepartamentos(){
    $id_proy = $this->input->post("id_proy");
    $det_dep=$this->ModeloGeneral->getselectwhere2('departamentos',array('id_proy'=>$id_proy,'activo'=>1));
    echo json_encode($det_dep->result());
  }
  function nameMes($m){
    $m=intval($m);
    $mes="Enero";
    switch ($m) {
      case 2: $mes="Febrero"; break;
      case 3: $mes="Marzo"; break;
      case 4: $mes="Abril"; break;
      case 5: $mes="Mayo"; break;
      case 6: $mes="Junio"; break;
      case 7: $mes="Julio"; break;
      case 8: $mes="Agosto"; break;
      case 9: $mes="Septiembre"; break;
      case 10: $mes="Octubre"; break;
      case 11: $mes="Noviembre"; break;
      case 12: $mes="Diciembre"; break;
    }
    return $mes;
  }
  function savedescripciong(){
    $params = $this->input->post();
    $id=$params['dg_id'];
    if($id>0){
      $this->ModeloGeneral->updateCatalogo_value(array('dg_mes'=>$params['dg_mes'],'dg_anio'=>$params['dg_anio'],'dg_titulo'=>$params['dg_titulo'],'dg_dep'=>$params['dg_dep'],'dg_des'=>$params['dg_des']),array('dg_id'=>$id),'actividades_realizadas_des');
    }else{
      $this->ModeloGeneral->tabla_inserta('actividades_realizadas_des',array('dg_mes'=>$params['dg_mes'],'dg_anio'=>$params['dg_anio'],'dg_titulo'=>$params['dg_titulo'],'dg_dep'=>$params['dg_dep'],'dg_des'=>$params['dg_des'],'proyecto'=>$params['proyecto'])); 
    }
  }
  function obetenerdesg(){
    $params = $this->input->post();
    //$det_dep=$this->ModeloGeneral->getselectwhere2('actividades_realizadas_des',array('proyecto'=>$params['proyecto'],'activo'=>1));
    $det_dep=$this->Modeloactividadesrealizadas->getActividades($params['proyecto'],"");
    echo json_encode($det_dep);
  }
  function delete_des(){
    $params = $this->input->post();
    $id=$params['id'];
    $this->ModeloGeneral->updateCatalogo_value(array('activo'=>0),array('dg_id'=>$id),'actividades_realizadas_des');
  }



}
