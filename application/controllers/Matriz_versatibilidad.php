<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Matriz_versatibilidad extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('Modeloactividadesrealizadas');
    $this->load->model('Modelo_matriz_versatibilidad');
    if(!$this->session->userdata('logeado')) {
      redirect('/Sistema');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index(){
    redirect('/Sistema');
  }

  public function listado($id_proy){
    $data['id_proy']=$id_proy;

    $result=$this->Modeloactividadesrealizadas->proyecto($id_proy);
    foreach ($result->result() as $item) {
      $empresa=$item->empresa;
      $fecha_ini=$item->fecha_ini;
      $fecha_fin=$item->fecha_fin;
    }
    $result=$this->ModeloGeneral->getselectwhere2('matriz_versatibilidad_resultados',array('idproyecto'=>$id_proy));
    if($result->num_rows()==0){
      $this->ModeloGeneral->tabla_inserta('matriz_versatibilidad_resultados',array('idproyecto'=>$id_proy));
    }

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('matriz_versa/list',$data);
    $this->load->view('templates/footer');
    $this->load->view('matriz_versa/listjs');
  }
  function add($id_proy,$idempleado=0){
    $data['id_proy']=$id_proy;
    $data['idempleado']=$idempleado;
    $empresa='';
    $result=$this->Modeloactividadesrealizadas->proyecto($id_proy);
    foreach ($result->result() as $item) {
      $empresa=$item->empresa;
      $fecha_ini=$item->fecha_ini;
      $fecha_fin=$item->fecha_fin;
    }
    $data['empresa']=$empresa;
    $data['empleados']=$this->Modelo_matriz_versatibilidad->empleado_proyecto($id_proy);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('matriz_versa/add',$data);
    $this->load->view('templates/footer');
    $this->load->view('matriz_versa/addjs');
  }

  public function delete(){
    $id = $this->input->post('id');
    $data = array('activo' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo_value($data,array('idmatriz'=>$id),"matriz_versatibilidad");
    echo $resul;
  }

  function saveform(){
    $params = $this->input->post();
    $idmatriz=$params['idmatriz'];
    $actividades = $params['actividades'];
    unset($params['actividades']);
    unset($params['tipo']);
    if($idmatriz>0){
      $params['fecha_update']=$this->fechahoy;
      $this->ModeloGeneral->updateCatalogo_value($params,array('idmatriz'=>$idmatriz),'matriz_versatibilidad');
      $this->ModeloGeneral->updateCatalogo_value(array("fecha_update"=>$this->fechahoy),array('idproyecto'=>$params["idproyecto"]),'matriz_versatibilidad');
    }else{
      $params['reg']=$this->fechahoy;
      $idmatriz=$this->ModeloGeneral->tabla_inserta('matriz_versatibilidad',$params);
    }
    $darrayinsert=array();

    $DATAc = json_decode($actividades);
    for ($i=0;$i<count($DATAc);$i++) {
      if($DATAc[$i]->id>0){
        $this->ModeloGeneral->updateCatalogo_value(array('valor'=>$DATAc[$i]->valor,'estatus'=>$DATAc[$i]->estatus/*,"fecha_upate"=>$this->fechahoy*/),array('idmatrizd'=>$DATAc[$i]->id),'matriz_versatibilidad_detalle');
      }else{
        $arrayinsert = array(
                            'idmatriz'=>$idmatriz,
                            'idactividad'=>$DATAc[$i]->actividad,
                            'valor'=>$DATAc[$i]->valor,
                            'estatus'=>$DATAc[$i]->estatus,
                            //'fecha_reg'=>$this->fechahoy
                          );
        $darrayinsert[]=$arrayinsert;
      }
    }
    if(count($darrayinsert)>0){
      $this->ModeloGeneral->insert_batch('matriz_versatibilidad_detalle',$darrayinsert);
    }
  }

  public function saveUnico(){
    $datos = $this->input->post();
    $id=$datos['id']; unset($datos['id']);
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'matriz_versatibilidad_detalle');
      $id_reg=$id; 
    }else{
      $id_reg=$this->ModeloGeneral->tabla_inserta('matriz_versatibilidad_detalle',$datos);
    }  
    echo $id_reg;
  }

  function obtenerinfo(){
    $params = $this->input->post();
    $idproyecto = $params['idproyecto'];
    $idempleado = $params['idempleado'];
    $result=$this->ModeloGeneral->getselectwhere2('matriz_versatibilidad',array('idproyecto'=>$idproyecto,'idempleado'=>$idempleado,'activo'=>1));
    $idmatriz=0;
    foreach ($result->result() as $item) {
        $idmatriz=$item->idmatriz;
    }
    $resultd=$this->ModeloGeneral->getselectwhere2('matriz_versatibilidad_detalle',array('idmatriz'=>$idmatriz,'activo'=>1));
    $arrayinfo=array('num'=>$result->num_rows(),'general'=>$result->row(),'detalles'=>$resultd->result());
    echo json_encode($arrayinfo);
  }
  public function getDataTable(){
    $params = $this->input->post();
    $getdata = $this->Modelo_matriz_versatibilidad->getDatalist($params);
    $totaldata= $this->Modelo_matriz_versatibilidad->getDatalist_total($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  function generar($id_proy,$viewtipe=0){
    $data['id_proy']=$id_proy;
    $data['list_per']=$this->Modelo_matriz_versatibilidad->list_personal_proyecto($id_proy);
    $data['list_act']=$this->Modelo_matriz_versatibilidad->list_actividades_proyecto($id_proy);
    if($viewtipe==0){
      $this->load->view('matriz_versa/generar',$data);
    }else{
      $this->load->view('matriz_versa/generar0',$data);
    }
    
  }
  function savegrafo(){
    $datax = $this->input->post();
    $id=$datax['id'];
    $namecol=$datax['name'];
    $base64Image=$datax['grafica'];
    //$namefile=$codigo.'.png';
    //$imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
    //file_put_contents('public/graficas/'.$namefile, $imagenBinaria);
    $this->ModeloGeneral->updateCatalogo_value(array($namecol=>$base64Image),array('idproyecto'=>$id),'matriz_versatibilidad_resultados');
  }
  function documento($id_proy){
    $data['id_proy']=$id_proy;
    $logo_cli=$this->ModeloGeneral->getLogoCli($id_proy);
    $data['logo_cli'] = $logo_cli->foto;
    //log_message('error', 'correo logo_cli : '.$data['logo_cli']->foto);
    $data['result']=$this->ModeloGeneral->getselectwhere2('matriz_versatibilidad_resultados',array('idproyecto'=>$id_proy));
    $data['result2']=$this->Modelo_matriz_versatibilidad->getMVRN($id_proy);
    $this->load->view('reportes/pdf_matriz_versatibilidad',$data);
  }

  public function exportarExcel($id_proy)
  {
    $data['id_proy']=$id_proy;
    $data['list_per']=$this->Modelo_matriz_versatibilidad->list_personal_proyecto($id_proy);
    $data['list_act']=$this->Modelo_matriz_versatibilidad->list_actividades_proyecto($id_proy);
    $this->load->view('matriz_versa/excel_versatibilidad',$data);
  }
  function savegrafo2(){
    $datax = $this->input->post();
    $id=$datax['id'];
    $namecol=$datax['name'];
    $base64Image=$datax['grafica'];
    
    $row=$datax['row'];
    $codigo=$datax['codigo'];

    $namefile=$namecol.'_'.$codigo.'_'.rand(0, 200).'.png';
    $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
    file_put_contents('public/graficas/'.$namefile, $imagenBinaria);
    
    $sql = "DELETE From matriz_versatibilidad_resultados_n where idproyecto=$id and codigo!=$codigo";
    $query = $this->db->query($sql);

    $resultex=$this->ModeloGeneral->getselectwhere2('matriz_versatibilidad_resultados_n',array('idproyecto'=>$id,'row'=>$row,'codigo'=>$codigo));
    $idegrafo=0;
    foreach ($resultex->result() as $item) {
      $idegrafo=$item->id;
    }

    if($idegrafo>0){
      $this->ModeloGeneral->updateCatalogo_value(array($namecol=>$namefile),array('id'=>$idegrafo),'matriz_versatibilidad_resultados_n');
    }else{
      $this->ModeloGeneral->tabla_inserta('matriz_versatibilidad_resultados_n',array('idproyecto'=>$id,'row'=>$row,'codigo'=>$codigo,$namecol=>$namefile));
    }
  }

  function savegrafo2_2(){
    $datax = $this->input->post();
    $id=$datax['id'];
    $namecol=$datax['name'];
    $base64Image=$datax['grafica'];
    
    $row=$datax['row'];
    $codigo=$datax['codigo'];

    $namefile=$namecol.'_'.$codigo.'_'.rand(0, 200).'.png';
    $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
    file_put_contents('public/graficas/'.$namefile, $imagenBinaria);
    
    $sql = "DELETE From matriz_versatibilidad_resultados_n where idproyecto=$id and codigo!=$codigo";
    $query = $this->db->query($sql);

    $resultex=$this->ModeloGeneral->getselectwhere2('matriz_versatibilidad_resultados_n',array('idproyecto'=>$id,'row'=>$row,'codigo'=>$codigo));
    $idegrafo=0;
    foreach ($resultex->result() as $item) {
      $idegrafo=$item->id;
    }

    if($idegrafo>0){
      $this->ModeloGeneral->updateCatalogo_value(array($namecol=>$namefile),array('id'=>$idegrafo),'matriz_versatibilidad_resultados_n');
    }else{
      $this->ModeloGeneral->tabla_inserta('matriz_versatibilidad_resultados_n',array('idproyecto'=>$id,'row'=>$row,'codigo'=>$codigo,$namecol=>$namefile));
    }
  }



}
