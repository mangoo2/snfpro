<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\ReaderFactory;  
use Box\Spout\Common\Type;

class Actividades extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->library('excel');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModelEmpleado');
    $this->load->model('ModeloProyectos');
    if(!$this->session->userdata('logeado')) {
      redirect('/Login');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {

  }

  public function listado($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('actividades/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('actividades/indexjs');
  }

  public function registro($id=0,$id_proy,$id_cli=0)
  {
    $data['id_proy']=$id_proy;
    $data['id_cli']=$id_cli;
    $data['tittle']='Registro de'; 
    if($id>0){
      $data['tittle']='Edición de'; 
      $data["a"]=$this->ModeloGeneral->getselectwhererow2("limpieza_actividades",array("estatus"=>1,"id"=>$id));
    }
    $data["dp"]=$this->ModeloGeneral->getselectwhererow2("proyectos",array("id"=>$id_proy));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('actividades/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('actividades/formjs');
  }

  public function getDataTable(){
    $params = $this->input->post();
    $getdata = $this->ModeloProyectos->getDataLimpieza($params);
    $totaldata= $this->ModeloProyectos->getDataLimpieza_total($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function submit(){
    $datos = $this->input->post();
    $id=$datos['id'];
    $tabla=$datos['tabla'];
    $band_ot=$datos['band_ot'];
    unset($datos["tabla"]);
    unset($datos["band_ot"]);
    $band_mac=$datos['band_mac'];
    unset($datos["band_mac"]);
    $id_proyecto=$datos["id_proyecto"]; $id_cliente=$datos["id_cliente"];

    //$fecha=strtotime($datos["fecha_ini"]);
    $fecha = new DateTime($datos["fecha_ini"]);
    $dia = $fecha->format('N');
    $semana = $fecha->format('W');
    $mes = $fecha->format('n');
    $dias_total_mes = $fecha->format('t');
    $fecha2 = new DateTime($datos["fecha_fin"]);
    $mes_fin = $fecha2->format('n');
    $dias_total_mes_fin = $fecha2->format('t');
    //log_message('error', 'dia : '.$dia);
    /*log_message('error', 'dias_total_mes : '.$dias_total_mes);
    log_message('error', 'dias_total_mes_fin : '.$dias_total_mes_fin);
    log_message('error', 'mes : '.$mes);
    log_message('error', 'mes_fin : '.$mes_fin);*/
    
    $days=intval($datos['days']); //unidades de frecuencia
    $dato_up["mp"]=$datos["mp"];
    if($id>0){
      if($datos["mp"]==""){
        $get_cli=$this->ModeloGeneral->getselectwhererow2("clientes",array("id"=>$datos["id_cliente"]));
        $datos["mp"]="SNF00".$id.$get_cli->empresa;
        $dato_up["mp"]=$datos["mp"];
      }
      unset($datos["id_proyecto"]); unset($datos["id_cliente"]);
      
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,$tabla);
      $id_reg=$id; 
    }else{
      $datos['id_user_reg']=$this->idpersonal;
      $datos['reg']=$this->fechahoy;
      $datos["seguimiento"]=2;
      $id_reg=$this->ModeloGeneral->tabla_inserta($tabla,$datos);
      //$id_reg=55;
      if($datos["mp"]==""){
        $get_cli=$this->ModeloGeneral->getselectwhererow2("clientes",array("id"=>$datos["id_cliente"]));
        $dato_up["mp"]="SNF00".$id_reg.$get_cli->empresa;
        $this->ModeloGeneral->updateCatalogo($dato_up,'id',$id_reg,$tabla);
      }

      /*if($dia==7){ // = de domingo
        $array_orden = array(
          "id_cliente"=>$datos["id_cliente"],
          "id_proyecto"=>$datos["id_proyecto"],
          "semana"=>$semana,
          "turno"=>1,
          "ot"=>$datos["mp"],
          "id_supervisor"=>$this->idpersonal,
          "id_limpieza"=>$id_reg,
          "tipo_act"=>1,
          "fecha"=>$datos["fecha_ini"],
          "seguimiento"=>2,
          "reg"=>$this->fechahoy,
          "id_user_reg"=>$this->idpersonal
        );
        for($i=0; $i<$days; $i++){
          $this->ModeloGeneral->tabla_inserta("ordenes_semanal",$array_orden);
        }
      }else{
        $array_orden = array(
          "id_actividad"=>$id_reg,
          "tipo_act"=>1,
          "turno"=>1,
          "estatus"=>2,
          "causas"=>"",
          "id_cliente"=>$datos["id_cliente"],
          "id_proyecto"=>$datos["id_proyecto"],
          "id_user_reg"=>$this->idpersonal,
          "fecha"=>$datos["fecha_ini"],
          "reg"=>$this->fechahoy
        );
        for($i=0; $i<$days; $i++){
          $this->ModeloGeneral->tabla_inserta("ordenes_diario",$array_orden);
        }
      }*/
    } //else 
    if($band_ot==1){
      //se comenta lo de arriba y se cambia por la nueva estructura 
      $fechaActual = date('Y-m-d'); 
      $datetime1 = date_create($datos["fecha_fin"]);
      $datetime2 = date_create($datos["fecha_ini"]);
      $fecha_ini=$datos["fecha_ini"];
      $fecha_fin=$datos["fecha_fin"];
      $contador = date_diff($datetime1, $datetime2);
      $differenceFormat = '%a';
      //log_message('error', $contador->format($differenceFormat)); 
      $fecha_mod=$datos["fecha_ini"];
      $dif_meses=$contador->format("%m");
      $dif_anios=$contador->format("%y");
      //log_message('error', "dif de meses: ".$contador->format($dif_meses)); 
      //log_message('error', "contador en format a: ".floor(($contador->format('%a'))));

      if($datos["frecuencia_cotiza"]=="1"){ //semanal
        $num_sem_perio=floor(($contador->format('%a') / 7));
      }else if($datos["frecuencia_cotiza"]=="2"){ //Quincenal
        $num_sem_perio=floor(($contador->format('%a') / 14));
      }
      else if($datos["frecuencia_cotiza"]=="3"){ //Mensual
        if($mes==2) //mes del periodo inicial
          $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes));
        else if($mes_fin==2) //mes del periodo final
          $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes_fin));
        else if($mes!=2 && $mes_fin!=2) //mes del periodo final
          $num_sem_perio=$dif_meses;
      }else if($datos["frecuencia_cotiza"]=="4"){ //Bimestral
        $num_sem_perio=floor(($contador->format('%a') / 60));
      }else if($datos["frecuencia_cotiza"]=="5"){ //Anual
        $num_sem_perio=$dif_anios;
      }
      //log_message('error', "ese parametro tiene # semanas total: ".$num_sem_perio); 
      if($dia==7){ // = de domingo
        $tipo=1;
      }else{
        $tipo=2;
      }
      $array_orden = array(
        "id_cliente"=>$id_cliente,
        "id_proyecto"=>$id_proyecto,
        "semana"=>$semana,
        "turno"=>1,
        "ot"=>$dato_up["mp"],
        "id_supervisor"=>$this->idpersonal,
        "id_limpieza"=>$id_reg,
        "tipo_act"=>1,
        "fecha"=>$fecha_mod,
        "seguimiento"=>2,
        "tipo"=>$tipo,
        "reg"=>$this->fechahoy,
        "id_user_reg"=>$this->idpersonal
      );
      $day_dif = $contador->format($differenceFormat)/$days;
      $day_dif=round($day_dif,0, PHP_ROUND_HALF_DOWN); 
      //log_message('error', 'day_dif : '.$day_dif);
      
      for($nr=0; $nr<$num_sem_perio; $nr++){
        //log_message('error', 'nr : '.$nr);
        for($i=0; $i<$days; $i++){
          //$fecha_mod= strtotime($fecha_ini."+ ".$day_dif*$i." days");
          if($datos["frecuencia_cotiza"]=="1"){//semanal
            $fecha_mod= strtotime($fecha_ini."+ ".$nr." week");
            //$fecha_mod= strtotime($fecha_ini."+ 1 days");
          }else if($datos["frecuencia_cotiza"]=="2"){//quincenal
            $fecha_mod= strtotime($fecha_ini."+ ".($nr*2)." week");
          }else if($datos["frecuencia_cotiza"]=="3"){//Mensual
            $fecha_mod= strtotime($fecha_ini."+ ".($nr)." month");
          }else if($datos["frecuencia_cotiza"]=="4"){//Bimestral
            $fecha_mod= strtotime($fecha_ini."+ ".($nr*2)." month");
          }else if($datos["frecuencia_cotiza"]=="5"){//Anual
            $fecha_mod= strtotime($fecha_ini."+ ".($nr)." year");
          }

          //log_message('error', 'fecha_mod : '.date("d-m-Y",$fecha_mod));
          $array_orden["fecha"]=date("Y-m-d",$fecha_mod);
          if(date('l', strtotime(date("d-m-Y",$fecha_mod))) == 'Sunday'){
            $array_orden["tipo"]=1;
          }else{
            $array_orden["tipo"]=2;
          }
          $fecha_mod2=date("Y-m-d",$fecha_mod);
          $fecha_nva = new DateTime($fecha_mod2);
          $array_orden["semana"] = $fecha_nva->format('W');
          //log_message('error', 'fecha_mod2 : '.$fecha_mod2);
          //log_message('error', 'semana de array_orden : '.$array_orden["semana"]);
          $this->ModeloGeneral->tabla_inserta("ordenes_semanal",$array_orden);
        }
      }
    } 
    /*if($band_mac==1){
      //se comenta lo de arriba y se cambia por la nueva estructura 
      $fechaActual = date('Y-m-d'); 
      $datetime1 = date_create($datos["fecha_fin"]);
      $datetime2 = date_create($datos["fecha_ini"]);
      $fecha_ini=$datos["fecha_ini"];
      $fecha_fin=$datos["fecha_fin"];
      $contador = date_diff($datetime1, $datetime2);
      $differenceFormat = '%a';
      //log_message('error', $contador->format($differenceFormat)); 
      $fecha_mod=$datos["fecha_ini"];
      $dif_meses=$contador->format("%m");
      $dif_anios=$contador->format("%y");
      //log_message('error', "dif de meses: ".$contador->format($dif_meses)); 
      //log_message('error', "contador en format a: ".floor(($contador->format('%a'))));

      if($datos["frecuencia_cotiza"]=="1"){ //semanal
        $num_sem_perio=floor(($contador->format('%a') / 7));
      }else if($datos["frecuencia_cotiza"]=="2"){ //Quincenal
        $num_sem_perio=floor(($contador->format('%a') / 14));
      }
      else if($datos["frecuencia_cotiza"]=="3"){ //Mensual
        if($mes==2) //mes del periodo inicial
          $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes));
        else if($mes_fin==2) //mes del periodo final
          $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes_fin));
        else if($mes!=2 && $mes_fin!=2) //mes del periodo final
          $num_sem_perio=$dif_meses;
      }else if($datos["frecuencia_cotiza"]=="4"){ //Bimestral
        $num_sem_perio=floor(($contador->format('%a') / 60));
      }else if($datos["frecuencia_cotiza"]=="5"){ //Anual
        $num_sem_perio=$dif_anios;
      }

      if($dia==7){ // = de domingo
        $tipo=1;
      }else{
        $tipo=2;
      }
      $array_monitor = array(
        "id_cliente"=>$id_cliente,
        "id_proyecto"=>$id_proyecto,
        "tipo"=>$tipo, //0=diario,1=semanal
        "id_limpieza"=>$id_reg,
        "zona"=>$datos["zona"],
        "inspeccion"=>"",
        "frecuencia"=>$datos["days"],
        "fecha"=>$fecha_mod,
        "mes"=>$mes,
        "reg"=>$this->fechahoy,
        "seguimiento"=>0, //no realizado
        "observaciones"=>"",
        "foto"=>"",
        "id_user_reg"=>$this->idpersonal
      );
      $day_dif = $contador->format($differenceFormat)/$days;
      $day_dif=round($day_dif,0, PHP_ROUND_HALF_DOWN); 
      //log_message('error', 'day_dif : '.$day_dif);
      
      for($nr=0; $nr<$num_sem_perio; $nr++){
        //log_message('error', 'nr : '.$nr);
        for($i=0; $i<$days; $i++){
          //$fecha_mod= strtotime($fecha_ini."+ ".$day_dif*$i." days");
          if($datos["frecuencia_cotiza"]=="1"){//semanal
            $fecha_mod= strtotime($fecha_ini."+ ".$nr." week");
            //$fecha_mod= strtotime($fecha_ini."+ 1 days");
          }else if($datos["frecuencia_cotiza"]=="2"){//quincenal
            $fecha_mod= strtotime($fecha_ini."+ ".($nr*2)." week");
          }else if($datos["frecuencia_cotiza"]=="3"){//Mensual
            $fecha_mod= strtotime($fecha_ini."+ ".($nr)." month");
          }else if($datos["frecuencia_cotiza"]=="4"){//Bimestral
            $fecha_mod= strtotime($fecha_ini."+ ".($nr*2)." month");
          }else if($datos["frecuencia_cotiza"]=="5"){//Anual
            $fecha_mod= strtotime($fecha_ini."+ ".($nr)." year");
          }

          //log_message('error', 'fecha_mod : '.date("Y-m-d",$fecha_mod));
          //log_message('error', 'DIA DE LA FECHA : '.date('l', strtotime(date("d-m-Y",$fecha_mod))));
          $array_monitor["fecha"]=date("Y-m-d",$fecha_mod);
          if(date('l', strtotime(date("d-m-Y",$fecha_mod))) == 'Sunday'){
            $array_monitor["tipo"]=1;
          }else{
            $array_monitor["tipo"]=0;
          }
          $fecha_mod2=date("Y-m-d",$fecha_mod);
          $fecha_nva = new DateTime($fecha_mod2);
          $array_monitor["mes"] = date('m',strtotime($fecha_mod2));
          //log_message('error', 'fecha_mod2 : '.$fecha_mod2);
          //log_message('error', 'semana de array_monitor : '.$array_monitor["semana"]);
          $this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$array_monitor);
        }
      }
    } */
    echo $id_reg;
  }

  public function delete(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    $data = array('activo' => 0);
    if($tabla=="limpieza_actividades"){
      $data = array('estatus' => 0);
    }
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  public function cambiaSeguimiento(){
    $id = $this->input->post('id');
    $estatus = $this->input->post('estatus');
    $fecha = $this->input->post('fecha_realiza');
    $data = array('seguimiento' => $estatus,"fecha_realiza"=>$fecha);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"limpieza_actividades");
    echo $resul;
  }

  public function nvaPlantilla(){
    $this->load->view('actividades/plantilla_excel');
  }

  function guardar_actividades_malla(){
    $id_proy=$_POST['id_proy'];
    $id_cli=$_POST['id_cli'];
    $genera=$_POST['genera_orden'];

    $configUpload['upload_path'] = FCPATH.'fileExcel/';
    $configUpload['allowed_types'] = 'xls|xlsx|csv';

    $this->load->library('upload', $configUpload);
    $this->upload->do_upload('inputFile');  
    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
    $file_name = $upload_data['file_name']; //uploded file name
    $extension=$upload_data['file_ext'];    // uploded file extension
    //===========================================
      $objReader= PHPExcel_IOFactory::createReader('CSV'); // For cvs delimitado por comas  
      $objReader->setReadDataOnly(true);          

      $objPHPExcel=$objReader->load(FCPATH.'fileExcel/'.$file_name);      
      $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
      $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                

      for($i=2;$i<=$totalrows;$i++){
        $nombre = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
        $tipo = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
        $zona = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
        $orden = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
        $frecuencia_cotiza = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
        $recursos = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
        $tiempo = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
        $horas_hombre = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
        $days = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
        $grupo = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
        $fecha_ini = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
        $fecha_fin = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
        $seguimiento = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();

        $fecha_ini=date("Y-m-d", strtotime($fecha_ini));
        $data = array(
          'id_cliente'=>$id_cli,
          'id_proyecto'=>$id_proy,
          'nombre' => $nombre,
          'tipo' => $tipo,
          'zona' => $zona,
          'mp' => $orden,
          'frecuencia_cotiza' => $frecuencia_cotiza,
          'recursos' => $recursos,
          'tiempo' => $tiempo,
          'horas_hombre' => $horas_hombre, 
          'days' => $days,
          'grupo' => $grupo,        
          'fecha_ini' => $fecha_ini,
          'fecha_fin' => date("Y-m-d", strtotime($fecha_fin)),
          'seguimiento' => $seguimiento,
          'reg'=>date("Y-m-d H:i:s"),
          'id_user_reg'=>$this->idpersonal
        );               
        $id_act=$this->ModeloGeneral->tabla_inserta('limpieza_actividades',$data);    

        if($genera=="1"){
          $fecha_camb = strtotime($fecha_ini);
          $semana = date('W', $fecha_camb);

          $array_ord = array(
            'id_cliente'=>$id_cli,
            'id_proyecto'=>$id_proy,
            'semana' => $semana,
            'turno' => 1,
            'id_supervisor' => $this->idpersonal,
            'ot' => $orden,
            'id_limpieza' => $id_act,
            'tipo_act' => 1,
            'fecha' => $fecha_ini, 
            'realiza' => "",
            'seguimiento' => 2, //programado        
            //'fecha_final' => $fecha_ini),
            'observaciones' => "",
            'reg'=>date("Y-m-d H:i:s"),
            'id_user_reg'=>$this->idpersonal
          );
          $fechaActual = date('Y-m-d'); 
          $datetime1 = date_create($fecha_fin);
          $datetime2 = date_create($fecha_ini);
          $fecha_ini=$fecha_ini;
          $fecha_fin=$fecha_fin;
          $contador = date_diff($datetime1, $datetime2);
          $differenceFormat = '%a';
          //log_message('error', $contador->format($differenceFormat)); 
          $fecha_mod=$fecha_ini;
          $dif_meses=$contador->format("%m");
          $dif_anios=$contador->format("%y");
          //log_message('error', "dif de meses: ".$contador->format($dif_meses)); 
          //log_message('error', "contador en format a: ".floor(($contador->format('%a'))));

          if($frecuencia_cotiza=="1"){ //semanal
            $num_sem_perio=floor(($contador->format('%a') / 7));
          }else if($frecuencia_cotiza=="2"){ //Quincenal
            $num_sem_perio=floor(($contador->format('%a') / 14));
          }
          else if($frecuencia_cotiza=="3"){ //Mensual
            if($mes==2) //mes del periodo inicial
              $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes));
            else if($mes_fin==2) //mes del periodo final
              $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes_fin));
            else if($mes!=2 && $mes_fin!=2) //mes del periodo final
              $num_sem_perio=$dif_meses;
          }else if($frecuencia_cotiza=="4"){ //Bimestral
            $num_sem_perio=floor(($contador->format('%a') / 60));
          }else if($frecuencia_cotiza=="5"){ //Anual
            $num_sem_perio=$dif_anios;
          }

          $day_dif = $contador->format($differenceFormat)/$days;
          $day_dif=round($day_dif,0, PHP_ROUND_HALF_DOWN); 
          //log_message('error', 'day_dif : '.$day_dif);
          //log_message('error', 'nr : '.$nr);
          for($nr=0; $nr<$num_sem_perio; $nr++){
            for($i=0; $i<$days; $i++){
              if($datos["frecuencia_cotiza"]=="1"){//semanal
                $fecha_mod= strtotime($fecha_ini."+ ".$nr." week");
                //$fecha_mod= strtotime($fecha_ini."+ 1 days");
              }else if($datos["frecuencia_cotiza"]=="2"){//quincenal
                $fecha_mod= strtotime($fecha_ini."+ ".($nr*2)." week");
              }else if($datos["frecuencia_cotiza"]=="3"){//Mensual
                $fecha_mod= strtotime($fecha_ini."+ ".($nr)." month");
              }else if($datos["frecuencia_cotiza"]=="4"){//Bimestral
                $fecha_mod= strtotime($fecha_ini."+ ".($nr*2)." month");
              }else if($datos["frecuencia_cotiza"]=="5"){//Anual
                $fecha_mod= strtotime($fecha_ini."+ ".($nr)." year");
              }
              //log_message('error', 'fecha_mod : '.date("d-m-Y",$fecha_mod));
              $array_ord["fecha"]=date("Y-m-d",$fecha_mod);
              if(date('l', strtotime($fecha_mod)) == 'Sunday'){
                $array_ord["tipo"]=1;
              }else{
                $array_ord["tipo"]=2;
              }
              $fecha_mod2=date("Y-m-d",$fecha_mod);
              $fecha_nva = new DateTime($fecha_mod2);
              $array_ord["semana"] = $fecha_nva->format('W');
              //log_message('error', 'fecha_mod2 : '.$fecha_mod2);
              //log_message('error', 'semana de array_orden : '.$array_orden["semana"]);
              //$id_ord=$this->ModeloGeneral->tabla_inserta("ordenes_semanal",$array_ord);
            }
          }
          $id_ord=$this->ModeloGeneral->tabla_inserta("ordenes_semanal",$array_ord);
        }

      }
      unlink('fileExcel/'.$file_name); //File Deleted After uploading in database . 
      //===========================================
    $output = [];
    echo json_encode($output);
  }

  /* *********************************************/
  public function listadoCriticas($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('actividades/indexCritica',$data);
    $this->load->view('templates/footer');
    $this->load->view('actividades/indexCriticajs');
  }

  public function registroCritica($id=0,$id_proy,$id_cli=0)
  {
    $data['id_proy']=$id_proy;
    $data['id_cli']=$id_cli;
    $data['tittle']='Registro de'; 
    if($id>0){
      $data['tittle']='Edición de'; 
      $data["a"]=$this->ModeloGeneral->getselectwhererow2("limpieza_actividades_critica",array("estatus"=>1,"id_cliente"=>$id_cli));
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('actividades/form_critica',$data);
    $this->load->view('templates/footer');
    $this->load->view('actividades/form_criticajs');
  }

  public function getDataTableCritica(){
    $params = $this->input->post();
    $getdata = $this->ModeloProyectos->getDataLimpiezaCritica($params);
    $totaldata= $this->ModeloProyectos->getDataLimpiezaCritica_total($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  function actividades_documentos(){
      $actividad=$_POST['actividad'];
      $data = [];
   
      $count = count($_FILES['files']['name']);
      $output = [];
      for($i=0;$i<$count;$i++){
    
        if(!empty($_FILES['files']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          $DIR_SUC=FCPATH.'uploads/files_ar_doc';

          $config['upload_path'] = $DIR_SUC; 
          //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
          $config['allowed_types'] = '*';

          //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
          $config['max_size'] = 5000;
          $file_names=date('Ymd_Gis').'_'.rand(0, 200);
          $config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
          //$config['file_name'] = $file_names;
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $filenametype = $uploadData['file_ext'];
   
            $data['totalFiles'][] = $filename;
            $this->ModeloGeneral->tabla_inserta('limpieza_actividades_files',array('id'=>$actividad,'file'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
          }else{
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data)); 
          }
        }
   
      }
      echo json_encode($output);
  }
  function upload_data_view(){
    $params = $this->input->post();
    $id=$params['id'];
    $resul=$this->ModeloGeneral->getselectwhere2('limpieza_actividades_files',array('id'=>$id,'activo'=>1));
    $html='';
    foreach ($resul->result() as $item) {
      $url=base_url().'uploads/files_ar_doc/'.$item->file;
      switch ($item->filetype) {
        case '.pdf':
          $filetype=base_url().'uploads/files_ar_doc/pdf.svg';
          break;
        case '.doc':
          $filetype=base_url().'uploads/files_ar_doc/doc.svg';
          break;
        case '.docx':
          $filetype=base_url().'uploads/files_ar_doc/doc.svg';
          break;
        case '.xlsx':
          $filetype=base_url().'uploads/files_ar_doc/xls.svg';
          break;
        case '.xls':
          $filetype=base_url().'uploads/files_ar_doc/xls.svg';
          break;
        case '.pptx':
          $filetype=base_url().'uploads/files_ar_doc/powerpoint.svg';
          break;
        
        default:
          $filetype=base_url().'uploads/files_ar_doc/unknown.svg';
          break;
      }

      $html.='<tr><td><a href="'.$url.'" download><img src="'.$filetype.'" style="width: 100px;">'.$item->file.'</a></td><td>'.$item->reg.'</td><td><button type="button" onclick="delete_files('.$item->idd.')" class="btn btn-danger"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button></td></tr>';
    }
    echo $html;
  }
  function delete_files(){
    $params = $this->input->post();
    $id=$params['id'];
    $this->ModeloGeneral->updateCatalogo_value(array('activo'=>0),array('idd'=>$id),'limpieza_actividades_files');
  }

}
