<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MatrizAcciones extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloProyectos');
    if(!$this->session->userdata('logeado')) {
      redirect('/Login');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {

  }

  public function listado($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('matrizAcciones/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('matrizAcciones/indexjs');
  }

  public function registro($id=0,$id_proy,$id_cli=0)
  {
    $data['id_proy']=$id_proy;
    $data['id_cli']=$id_cli;
    $data['tittle']='Registro de'; 

    $data["emp"]=$this->ModeloProyectos->getProyectoEmpleados($id_proy,array("tipo_empleado"=>1));
    $data["c"]=$this->ModeloGeneral->getselectwhererow2("clientes",array("id"=>$id_cli));
    if($id>0){
      $data['tittle']='Edición de'; 
      $data["a"]=$this->ModeloGeneral->getDatoHallazgo($id);
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('matrizAcciones/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('matrizAcciones/formjs');
  }

  public function getDataTable(){
    $params = $this->input->post();
    $getdata = $this->ModeloProyectos->getDataMatrizAcciones($params);
    $totaldata= $this->ModeloProyectos->getDataMatrizAcciones_total($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function submit(){
    $datos = $this->input->post();
    $id=$datos['id'];
    $id_cliente=$datos["id_cliente"];
    $tipo_hallazgo=$datos["tipo_hallazgo"];
    if($id>0){ 
      unset($datos["id_proyecto"]); unset($datos["id_cliente"]);
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,"matriz_acciones");
      $id_reg=$id; 
      $this->sendMail($id,$tipo_hallazgo);
    }else{
      $datos['id_user_reg']=$this->idpersonal;
      $datos['fecha_reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta("matriz_acciones",$datos);
      $this->sendMail($id_reg,$tipo_hallazgo); //enviar mail de notificacion, para no hacerlo masivamente desde header
    }  
    echo $id_reg;
  }

  public function delete(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    if($tabla!="matriz_acciones")
      $data = array('activo' => 0);
    else
      $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  public function deleteEvidencia(){
    $id = $this->input->post('id');
    $name = $this->input->post('name');
    $data = array($name => "");
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"matriz_acciones");
    echo $resul;
  }

  public function cargafiles(){
    $id=$this->input->post('id');
    $name=$this->input->post('name');
    $folder="evidencias";
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $newfile='doc_'.$fecha.$nombre_archivo; 
    $newfile=str_replace(" ", "", $newfile);      
    $archivador = $upload_folder.'/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array($name=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'id',$id,'matriz_acciones');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 


  public function sendMail($id,$tipo){   
    $result=$this->ModeloGeneral->getHallazgoCliente(array("ma.id"=>$id),$tipo);
    $data["hallazgo"]="";
    $data["fecha_compromiso"]="";
    $correo="";
    
    foreach ($result as $item) {
      $data["hallazgo"]=$item->hallazgo;
      $data["fecha_compromiso"]=$item->fecha_compromiso;
      //$correo=$item->correo;

      if($tipo==1){
        $correo = $item->correo;
      }else{
        $correo = $this->ModeloGeneral->getCorreosCliente($id);
        array_push($correo, $item->correo);
      }

    }

    log_message('error', 'correo Matriz: ' . json_encode($correo));
    //log_message('error', 'correo de sesion : '.$this->session->userdata("correo"));
    $this->load->library('email');
    $config = array();
    $config['protocol'] = 'smtp';
    /*$config['smtp_host'] = 'snfpro.sicoi.net';                     
    $config['smtp_user'] = 'hallazgos@snfpro.sicoi.net';
    $mail_dom = 'hallazgos@snfpro.sicoi.net';
    $config['smtp_pass'] = '(JSp*%TIi{8S';*/

    $config['smtp_host'] = 'mail.snf.app';                     
    $config['smtp_user'] = 'hallazgos@snf.app';
    $mail_dom = 'hallazgos@snf.app';
    $config['smtp_pass'] = 'f.Rj5eA[,8$l';

    $config['smtp_port'] = 465;
    $config["smtp_crypto"] = 'ssl';
    //$config['newline']   = "\r\n";
    //$config["crlf"] = "\r\n";
    $config['mailtype'] = "html";
    $config['charset'] = "utf-8";
    $config['wordwrap'] = TRUE;
    $config['validate'] = true;

    $this->email->initialize($config);
    $this->email->from($mail_dom);
    $this->email->to($correo);
    if($this->session->userdata("correo")!=""){
      //$this->email->cc($this->session->userdata("correo"));
    }
    $this->email->subject('Hallazo nuevo registrado');
    $msj=$this->load->view('matrizAcciones/mail', $data,true);
    $this->email->message($msj);
    $this->email->send();

    $this->ModeloGeneral->updateCatalogo(array("notificado"=>1,"fecha_notifica"=>$this->fechahoy),'id',$id,"matriz_acciones");
  }

  public function mailView(){
    $this->load->view('matrizAcciones/mail');
  } 

}
