<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proyectos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModelEmpleado');
    $this->load->model('ModeloProyectos');
    if(!$this->session->userdata('logeado')) {
      redirect('/Login');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('proyectos/index');
    $this->load->view('templates/footer');
    $this->load->view('proyectos/indexjs');
  }

  public function registro($id=0)
  {
    $data['tittle']='Nuevo'; 
    $data['id']=$id; 
    $data["html"]=""; $data["html2"]=""; $data["html3"]=""; $data["cont_pr"]=0; $contp=0;
    $data["puestos"]=$this->ModeloGeneral->getselectwhere('puestos','estatus',1);
    if($id>0){
      $data['tittle']='Edición de'; 
      $data["p"]=$this->ModeloProyectos->getProyecto($id);
      $cont=0;
      $cont2=0;
      $result=$this->ModeloProyectos->getProyectoEmpleados($id,0);
      foreach ($result as $x) {
        if($x->tipo_empleado==1){
          $cont++;
          $btn='<button onclick="eliminaEmple('.$x->id.',1)" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
          $id_row="idrow_emp_".$x->id."";
          $data["html"].='<tr id="'.$id_row.'">
              <td>'.$cont.'<input id="id" type="hidden" value="'.$x->id.'" data-id_emp="'.$x->id_empleado.'"></td>
              <td><input id="id_emp" type="hidden" value="'.$x->id_empleado.'">'.$x->nombre.'</td>
              <td><input class="form-control" id="fecha_asignacion" type="date" value="'.$x->fecha_asignacion.'"></td>
              <td>'.$btn.'</td>
          </tr>'; 
        }
        else{
          $cont2++;
          $btn2='<button onclick="eliminaEmple('.$x->id.',2)" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
          $id_row2="idrow_emp_sup_".$x->id."";
          $data["html2"].='<tr id="'.$id_row2.'">
              <td>'.$cont2.'<input id="id" type="hidden" value="'.$x->id.'" data-id_emp="'.$x->id_empleado.'"></td>
              <td><input id="id_emp" type="hidden" value="'.$x->id_empleado.'">'.$x->nombre.'</td>
              <td><input class="form-control" id="fecha_asignacion" type="date" value="'.$x->fecha_asignacion.'"></td>
              <td>'.$btn2.'</td>
          </tr>'; 
        }
      }

      // *************************************
      $get_rota=$this->ModeloProyectos->getPuestoRotacion($id);
      foreach ($get_rota as $r) {
        $data["cont_pr"]++;
        $contp++;
        $btn3='<button onclick="eliminaPuesto('.$r->id.')" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
        $puesto=$r->puesto;
        /*$puesto="";
        if($r->puesto==1)
          $puesto="Ajustador";
        else if($r->puesto==2)
          $puesto="Filtración";
        else if($r->puesto==3)
          $puesto="Técnico de limpieza";
        else if($r->puesto==4)
          $puesto="Administrativo";
        else if($r->puesto==5)
          $puesto="Staff";
        else if($r->puesto==6)
          $puesto="Otro";*/

        $data["html3"].='<tr id="idrow_puesto_'.$r->id.'">
                        <td>'.$contp.'</td>
                        <td>'.$puesto.'<input id="id" type="hidden" value="'.$r->id.'">
                          <input id="puesto" type="hidden" value="'.$r->id_puesto.'">
                        </td>
                        <td><input class="form-control" id="objetivo" type="number" value="'.$r->objetivo.'"></td>
                        <td><input class="form-control" id="dias_laborables" type="number" value="'.$r->dias_laborables.'"></td>
                        <td>'.$btn3.'</td>
                      </tr>';
      }
      ////////////////////////////////////////
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('proyectos/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('proyectos/formjs');
  }

  public function getDataTable(){
    $params = $this->input->post();
    $getdata = $this->ModeloProyectos->get_result($params);
    $totaldata= $this->ModeloProyectos->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function submit(){
    $datos = $this->input->post();
    $id=$datos['id'];
    if(isset($datos['puesto'])){
      unset($datos['puesto']);
    }
    if($id>0){
      //$datos['reg']=$this->fechahoy;
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'proyectos');
      $id_reg=$id; 
    }else{
      $datos['id_user_reg']=$this->idpersonal;
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('proyectos',$datos);
    }  
    echo $id_reg;
  }

  function submitEmpleadosProyecto(){
    $params = $this->input->post();
    $id = $params['id_proyecto'];
    unset($params['id_proyecto']);
    $array_emp = $params['array_emp'];
    unset($params['array_emp']);
    $array_empsup = $params['array_empsup'];
    unset($params['array_empsup']);

    $DATAc = json_decode($array_emp);  
    for ($i=0;$i<count($DATAc);$i++) {
      $data = array(
        'id_proyecto'=>$id,
        'id_empleado'=>$DATAc[$i]->id_empleado,
        'tipo_empleado'=>1,
        'fecha_asignacion' =>$DATAc[$i]->fecha_asignacion,
      );
      //log_message('error', 'id_empleado : '.$DATAES[$i]->id_empleado);
      if($DATAc[$i]->id==0){ 
        $this->ModeloGeneral->tabla_inserta('empleados_proyecto',$data);
      }else{
        unset($data["id"]);
        unset($data["id_proyecto"]);
        $this->ModeloGeneral->updateCatalogo_value($data,array('id'=>$DATAc[$i]->id),'empleados_proyecto');
      }
    }

    $DATAES = json_decode($array_empsup);  
    for ($i=0;$i<count($DATAES);$i++) {
      $data = array(
        'id_proyecto'=>$id,
        'id_empleado'=>$DATAES[$i]->id_empleado,
        'tipo_empleado'=>2,
        'fecha_asignacion' =>$DATAES[$i]->fecha_asignacion,
      );
      //log_message('error', 'id_empleado sup: '.$DATAES[$i]->id_empleado);
      if($DATAES[$i]->id==0){ 
        $this->ModeloGeneral->tabla_inserta('empleados_proyecto',$data);
      }else{
        unset($data["id"]);
        unset($data["id_proyecto"]);
        $this->ModeloGeneral->updateCatalogo_value($data,array('id'=>$DATAES[$i]->id),'empleados_proyecto');
      }
    }
  }

  function submitPuestosRota(){
    $params = $this->input->post();
    $id = $params['id_proyecto'];
    unset($params['id_proyecto']);
    $array_pues = $params['array_pues'];
    unset($params['array_pues']);

    $DATAc = json_decode($array_pues);  
    for ($i=0;$i<count($DATAc);$i++) {
      $data = array(
        'id_proyecto'=>$id,
        'puesto'=>$DATAc[$i]->puesto,
        'objetivo'=>$DATAc[$i]->objetivo,
        'dias_laborables'=>$DATAc[$i]->dias_laborables
      );

      if($DATAc[$i]->id==0){ 
        $this->ModeloGeneral->tabla_inserta('puesto_rotacion',$data);
      }else{
        unset($data["id"]);
        unset($data["id_proyecto"]);
        $this->ModeloGeneral->updateCatalogo_value($data,array('id'=>$DATAc[$i]->id),'puesto_rotacion');
      }
    }
  }

  public function getAllEmpleados(){
    $html=""; $i=0;
    $id_cli = $this->input->post('id_cli');
    $id = $this->input->post('id');
    /*if($this->session->userdata("empresa")!=""){
      $get_emp=$this->ModeloGeneral->getselectwhere2("personal",array("empresa"=>$this->session->userdata("empresa"),"estatus"=>1));
    }
    else{*/
      $get_emp=$this->ModeloGeneral->getselectwhere2Order("personal",array("empresa"=>$id_cli,"estatus"=>1),"personalId");
    //}
    
    
    foreach ($get_emp->result() as $e){
      $i++; $aux=0;
      $get_emp_proy=$this->ModeloGeneral->getselectwhere2Order("empleados_proyecto",array("id_proyecto"=>$id,"id_empleado"=>$e->personalId,"estatus"=>1),"id_empleado");
      foreach ($get_emp_proy->result() as $ep){
        $aux++;
      }
      if($aux==0){
        $html.='<tr class="row_emp_'.$i.'">
          <td>'.$i.'<input id="id" type="hidden" value="0"></td>
          <td><input id="id_emp" type="hidden" value="'.$e->personalId.'">'.$e->nombre.'</td>
          <td><input class="form-control" id="fecha_asignacion" type="date" value="'.date("Y-m-d").'"></td>
          <td><button onclick="eliminaRow('.$i.',1)" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
        </tr>';
      }
    }
    echo $html;
  }

  public function getEmpleadosProyecto()
  {
    $id = $this->input->post('id');
    $html="";  
    $cont=0;
    
    $resul=$this->ModeloProyectos->getProyectoEmpleados($id,0);
    foreach ($resul as $x) {
      $cont++;
      if($x->tipo_empleado==1){
        $btn='<button onclick="eliminaEmple('.$x->id.')" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
        $id_row="idrow_emp_".$x->id."";
      }
      else{
        $btn='<button onclick="eliminaEmple('.$x->id.')" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
        $id_row="idrow_emp_sup_".$x->id."";
      }
      $html.='<tr id="'.$id_row.'">\
            <td>'.$cont.'<input id="id" type="hidden" value="'.$x->id.'"></td>\
            <td><input id="id_emp" type="hidden" value="'.$x->id_emp.'">'.$x->nombre.'</td>\
            <td>'.$btn.'</td>\
        </tr>'; 
    }
    echo $html;
  }

  public function delete(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'proyectos');
    echo $resul;
  }

  public function deleteEmpleado(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'empleados_proyecto');
    echo $resul;
  }

  public function deletePuestoRotacion(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'puesto_rotacion');
    echo $resul;
  }

  public function searchPersonal(){
    $per = $this->input->get('search');
    $tipo = $this->input->get('tipo');
    $id_cli = $this->input->get('id_cli');
    $tabla="personal";
    $col="nombre";

    /*if($this->session->userdata("idpersonal_tz")==1){
      if($tipo==1){
        $array=array("estatus"=>1,"perfilId"=>1); //admin
        $array2=array("perfilId"=>3); //supervisor
      }else{
        $array=array("estatus"=>1,"perfilId!="=>1);
        $array2=array("perfilId!="=>1);
      }
    }else{
      if($tipo==1){
        $array=array("empresa"=>$this->session->userdata("empresa"),"estatus"=>1,"perfilId"=>1); //admin
        $array2=array("perfilId"=>3); //supervisor
      }else{
        $array=array("empresa"=>$this->session->userdata("empresa"),"estatus"=>1,"perfilId!="=>1);
        $array2=array("perfilId!="=>1);
      }
    }
    $results=$this->ModelEmpleado->searchPersonal2($tabla,$col,$per,$array,$array2);*/
    /*if($this->session->userdata("empresa")!=""){
      $results=$this->ModelEmpleado->searchPersonal($tabla,$col,$per,array("empresa"=>$this->session->userdata("empresa"),"estatus"=>1));
    }
    else{*/
      $results=$this->ModelEmpleado->searchPersonal($tabla,$col,$per,array("empresa"=>$id_cli,"estatus"=>1));
    //}
    echo json_encode($results);
  }

  public function searchClientes(){
    $cli = $this->input->get('search');
    $tabla="clientes";
    $col="empresa";
    //$results=$this->ModelEmpleado->searchPersonal($tabla,$col,$cli,array("activo"=>1));
    if($this->session->userdata("idpersonal_tz")!="1"){
      $results=$this->ModelEmpleado->searchPersonal($tabla,$col,$cli,array("id"=>$this->session->userdata("empresa"),"activo"=>1));
    }
    else{
      $results=$this->ModelEmpleado->searchPersonal($tabla,$col,$cli,array("activo"=>1));
    }

    echo json_encode($results);
  }

  public function searchServicios(){
    $serv = $this->input->get('search');
    $results=$this->ModelEmpleado->searchServicio($serv);
    echo json_encode($results);
  }

  /* ***************************** */
  public function ejecucion($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli; 
    $data["cliente_text"]=$this->ModeloGeneral->getselectwhererow('clientes','id',$id_cli);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('proyectos/ejecucion',$data);
    $this->load->view('templates/footer');
  }

  /* *********************************/
  public function limpieza($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli;
    $data["cliente_text"]=$this->ModeloGeneral->getselectwhererow('clientes','id',$id_cli);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('proyectos/progra_limpieza',$data);
    $this->load->view('templates/footer');
  }

  /* *********************************/
  public function ordenes($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli;
    $data["cliente_text"]=$this->ModeloGeneral->getselectwhererow('clientes','id',$id_cli);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('proyectos/ordenes',$data);
    $this->load->view('templates/footer');
  }


}
