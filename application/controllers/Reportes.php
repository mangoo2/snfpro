<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reportes extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModelEmpleado');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModelReportes');
    $this->load->model('Modeloactividadesrealizadas');
    if(!$this->session->userdata('logeado')) {
      redirect('/Login');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {

  }

  public function Salida()
  {
    if($this->session->userdata("idpersonal_tz")!=1){
      $data["clis"]=$this->ModeloGeneral->getselectwhere2("clientes",array("id"=>$this->session->userdata("empresa")));
    }
    //$data['det_dep']=$this->ModeloGeneral->getselectwhere2('departamentos',array('activo'=>1));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('reportes/listado_salida',$data);
    $this->load->view('templates/footer');
    $this->load->view('reportes/listado_salidajs');
  }

  public function searchClientes(){
    $cli = $this->input->get('search');
    $tabla="clientes";
    $col="empresa";
    if($this->session->userdata("idpersonal_tz")==1){
      $results=$this->ModelEmpleado->searchPersonal($tabla,$col,$cli,array("activo"=>1));
      echo json_encode($results);
    }
    else{
      $results=$this->ModeloGeneral->getselectwhere2($tabla,array("id"=>$this->session->userdata("empresa")));
      echo json_encode($results->result());
    }
  }

  public function getProyectosCliente(){
    $id = $this->input->post('id_cli');
    $results=$this->ModelReportes->getProyectos_cliente($id);
    $html="";
    foreach ($results as $p) {
      $html.='<option data-fi="'.$p->fecha_ini.'" data-ff="'.$p->fecha_fin.'" data-anio_ini="'.$p->anio_ini.'" data-anio_fin="'.$p->anio_fin.'" data-nameserv="'.$p->servicio.'" data-ini="'.$p->mes_ini.'" data-fin="'.$p->mes_fin.'" value="'.$p->id.'">'.$p->servicio.' / Inicio: '.$p->fecha_ini.' Fin: '.$p->fecha_fin.'</option>';
    }
    echo $html;
  }

  public function getDeptoProy(){
    $id = $this->input->post('id_proy');
    $results=$this->ModeloGeneral->getselectwhere2('departamentos',array('id_proy'=>$id,'activo'=>1));
    $html="";
    foreach ($results->result() as $p) {
      $html.='<option value="'.$p->id.'">'.$p->departamento.'</option>';
    }
    echo $html;
  }

  public function getDataTableReporte(){
    $params = $this->input->post();
    $datos = $this->ModelReportes->get_table_salidas($params);
    $json_data = array("data" => $datos);
    echo json_encode($json_data);
  }

  function saber_dia($name) {
    //log_message('error', 'name: '.$name);
    $dias = array('','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo');
    $fecha = $dias[date('N', strtotime($name))];
    return $fecha;
  }

  public function resultadosOrden($id,$mes,$anio){
    $params['id_proy']=$id;
    $params['mes']=$mes;
    $params['anio']=$anio;
    $data['id_proy']=$id;
    $data['mes']=$mes;
    $data['anio']=$anio;
    $data["name_mes"] = $this->nameMes($mes);
    $band=0;
    $dayend = cal_days_in_month(CAL_GREGORIAN,$mes,$anio);
    if ($mes<10) { 
      $add = "-0"; 
    } 
    else {
      $add = "-"; 
    }
    $date1 = $anio.$add.$mes."-01";
    $date2 = $anio.$add.$mes."-".$dayend;
    $weeks = date("W", strtotime($date2))-date("W", strtotime($date1)) + 1;
    //log_message('error', "weeks ".$weeks);
    $data["weeks"]=$weeks;

    $data["res"]=$this->ModelReportes->get_data_ordenes($params,$band);
    $data["table_com"]="";
    $repro=$this->ModelReportes->get_ordenes_coments($params);
    foreach($repro as $r){
      $fecha_comp=$this->saber_dia(date("Y-m-d",strtotime($r->fecha)))." ".date("d",strtotime($r->fecha)) ." de ".$this->nameMes(date("m",strtotime($r->fecha)));
      if($r->seguimiento==1){//Cancelado
        $estatus="<span class='btn btn-danger'>Cancelado</span>";
      }
      elseif($r->seguimiento==4){//Reprogramado
        $estatus="<span class='btn btn-warning'>Reprogramado</span>";
      }elseif($r->seguimiento==5){//No realizado
        $estatus="<span class='btn btn-warning'>No realizado</span>";
      }
      $data["table_com"].="<tr>
            <td>".$fecha_comp."</td>
            <td>".$r->actividad."</td>
            <td>".$r->ot."</td>
            <td>".$r->observaciones."</td>
            <td>".$estatus."</td>
          </tr>";
    }
    //$data["act"]=$this->ModelReportes->get_data_limpieza_orden($params);
    $data["logo_cli"] = $data["res"][0]->logo_cli;
    //log_message('error', "logo_cli ".$data["logo_cli"]);
    $this->load->view('reportes/resultados_orden',$data);
    $this->load->view('reportes/resultados_ordenjs');
  }

  public function exportarListaOrdenes($id_proy,$semana,$mes,$anio){
    $params['id_proy']=$id_proy;
    $params['mes']=$mes;
    $params['semana']=$semana;
    $params['anio']=$anio;
    $data["act"]=$this->ModelReportes->get_data_limpieza_orden($params);

    $data['id_proy']=$id_proy;
    $data['mes']=$mes;
    $data['semana']=$semana;
    $data['anio']=$anio;
    $data["name_mes"] = $this->nameMes($mes);
    $this->load->view('reportes/excelListaOrd',$data);
  }

  public function getSemanaProyecto(){
    $id_proyecto = $this->input->post("id_proyecto");
    $anio = $this->input->post("anio");
    $mes = $this->input->post("mes");
    $html="";
    /*$get=$this->ModelReportes->get_mes_actividades($id_proyecto);
    foreach($get as $a){
      $fecha = strtotime($a->fecha_ini);
      $semana = date('W', $fecha);
      $html.="<option value='".$semana."'>".$semana."</option>";
    }*/
    $get=$this->ModelReportes->get_semanas_programa($id_proyecto,$anio,$mes);
    foreach($get as $a){
      $html.="<option value='".$a->semana."'>".$a->semana."</option>";
    }

    //log_message('error', "html ".$html); 
    echo $html;
  }

  public function getActivSemana(){
    $params = $this->input->post();
    $semana = $this->input->post("semana");
    $anio = $this->input->post("anio");
    $act=$this->ModelReportes->get_data_limpieza_orden($params);
    /*$json_data = array("data" => $act);
    echo json_encode($json_data);*/
    //armar tabla html-body acá
    $html_head=array(); $html=""; $html_foot=""; $i=0; $turno=""; $logo=""; $empresa="";
    //$anio=0; 
    $fecha_ini=""; $fecha_final="";
    $fin_cumple=0;
    $fin_progra=0;
    $fin_efi=0;
    $fin_ok=0;
    $fin_efica=0;
    $cont_can=0; 
    $cont_pro=0;
    $cont_ok=0; 
    $cont_re=0; 
    $cont_no=0; 
    $html_foot_can="";
    $html_foot_pro="";
    $html_foot_ok="";
    $html_foot_re="";
    $html_foot_no="";
    $cont_ok2=0; 
    $cont_ok3=0;
    $cont_ok4=0;
    $cont_ok5=0;
    $cont_ok6=0;
    $cont_ok7=0;
    $cont_pro2=0; 
    $cont_pro3=0;
    $cont_pro4=0;
    $cont_pro5=0;
    $cont_pro6=0;
    $cont_pro7=0;

    $cont_item=0;
    /*$html='<table style="text-align: center; vertical-align: middle;" class="table" border="1" width="100%" id="table_rep_sem">
            <thead>
                <tr>
                    <th colspan="3" rowspan="4">
                        <table>
                            <tr>
                                <td width="50%"><img class="img-fluid" width="160px" src="'.base_url().'public/img/logofinalsys.png"></td>
                                <td width="50%"><b>TIME LINE</b>
                                    <p>SNF PRO</p>
                                    <b>PLANEACIÓN GERENCIAL</b>
                                    <p id="name_cli"> CLIENTE: </p>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%"></td>
                                <td width="50%"><img id="logo_cli" class="img-fluid" width="160px" src="#"></td>
                            </tr>
                        </table>   
                    </th>
                    <th rowspan="2">%PORCENTAJE A LA EFICACIA (CALIDAD EN EL TRABAJO REALIZADO):</th>
                    <th rowspan="2">%PORCENTAJE DE CUMPLIMIENTO EFICIENCIA:</th>
                    <th colspan="9">TITULO/ PROYECTO: Plan de trabajo semana </th>
                    <th>Nivel de cambios: </th>
                </tr>
                <tr>
                    <th>Fecha inicial:</th>
                    <th>26/09/2022</th>
                    <th colspan="7" rowspan="2">MES AÑO / ¿Cuándo?</th>
                    <th>Codigo doc: SNF-CAL-10</th>
                </tr>
                <tr>
                    <th>%</th>
                    <th>%</th>
                    <th>Fecha final programada:</th>
                    <th>02/10/2022</th>
                    <th rowspan="3">MINUTA / NOTA:</th>
                </tr>
                <tr>
                    <th colspan="2">CANCEL           PROG                OK                RE-PROG</th>
                    <th>Proceso:</th>
                    <th>Limpieza Industrial</th>
                    <th colspan="7">SEMANA #</th>
                </tr>
                <tr>
                    <th>OT</th>
                    <th>ITEM</th>
                    <th>¿QUE?</th>
                    <th>ACTIVIDADES / ¿COMO?</th>
                    <th>¿Porque?</th>
                    <th>¿Quien?</th>
                    <th>¿Donde?</th>
                    <th id="day1">26 </th>
                    <th id="day2">27 </th>
                    <th id="day3">28 </th> 
                    <th id="day4">29 </th>
                    <th id="day5">30 </th>
                    <th id="day6">1 </th>
                    <th id="day7">2 </th>
                </tr>
            </thead>
            <tbody id="body_table_rep_sem">';*/
    
    for($day=1; $day<=7; $day++)
    {
      $html_head[]=array(date('d', strtotime($anio."W".$semana.$day)));
      $count_tot=$this->ModelReportes->count_estatus(0,0,date('Y-m-d', strtotime($anio."W".$semana.$day)),$params["id_proy"],0);
      //log_message('error', 'fecha : '.date('Y-m-d', strtotime($anio."W".$semana.$day)));
      //totales
      ${'cont_total_'.$day}=$count_tot->tot_estatus;
      //$cont_total=$count_tot->tot_estatus;
      //log_message('error', 'cont_total : '.$cont_total);
    }
    
    foreach ($act as $a) {
      $i++;
      $cont_item++;
      
      if($a->turno==1){
        $turno="1ER";
      }
      else if($a->turno==2){
        $turno="2DO";
      }
      else if($a->turno==3){
        $turno="3ER";
      }

      $anio=$a->anio; $logo=$a->foto; $empresa=$a->empresa; $fecha_ini=$a->fecha_ini; $fecha_final=$a->fecha_final;
      $html.="<tr>
              <td>".$a->ot."</td>
              <td>".$i."</td>
              <td>".$a->nombre."</td>
              <td>EN BASE A TASK SEGÚN ORDEN DE TRABAJO</td>
              <td>ASEGURAR LIMPIEZA Y PROGRAMA SEMANAL DE LIMPIEZA CUMPLIMIENTO AL 100%</td>
              <td>".$turno." TURNO</td>
              <td>".$a->empresa."</td>";
              for($day=1; $day<=7; $day++)
              { 
                /*if($day==1){
                  $cont_ok++;
                }else if($day==2){
                  $cont_ok2++;
                }else if($day==3){
                  $cont_ok3++;
                }else if($day==4){
                  $cont_ok4++;
                }else if($day==5){
                  $cont_ok5++;
                }else if($day==6){
                  $cont_ok6++;
                }else if($day==7){
                  $cont_ok7++;
                }*/
                //log_message('error', 'day : '.date('d', strtotime($anio."W".$semana.$day)));
                if(date('d', strtotime($anio."W".$semana.$day))==$a->dia){
                  //log_message('error', 'dias iguales : '.$a->dia);
                  //$html.="<td>".$a->dia."</td>";
                  
                  if($a->seguimiento==1){//Cancelado
                    $cont_can++;
                    $html.="<td><span class='btn btn-danger'>X</span></td>";
                  }
                  else if($a->seguimiento==2){//Programado
                    $html.="<td><span class='btn btn-info'>P</span></td>";
                    if($day==1){
                      $cont_pro++;
                    }else if($day==2){
                      $cont_pro2++;
                    }else if($day==3){
                      $cont_pro3++;
                    }else if($day==4){
                      $cont_pro4++;
                    }else if($day==5){
                      $cont_pro5++;
                    }else if($day==6){
                      $cont_pro6++;
                    }else if($day==7){
                      $cont_pro7++;
                    }
                  }
                  else if($a->seguimiento==3){//OK
                    $html.="<td><span class='btn btn-success'>1</span></td>";
                    if($day==1){
                      $cont_ok++;
                    }else if($day==2){
                      $cont_ok2++;
                    }else if($day==3){
                      $cont_ok3++;
                    }else if($day==4){
                      $cont_ok4++;
                    }else if($day==5){
                      $cont_ok5++;
                    }else if($day==6){
                      $cont_ok6++;
                    }else if($day==7){
                      $cont_ok7++;
                    }
                  }
                  else if($a->seguimiento==4){//Reprogramado
                    $cont_re++;
                    $html.="<td><span class='btn btn-warning'>-</span></td>";
                  }else if($a->seguimiento==5){//No realizado
                    $cont_no++;
                    $html.="<td><span class='btn btn-warning'>N</span></td>";
                  }
                  else{
                    $html.="<td></td>";
                  }
                }else{
                  $html.="<td></td>";
                }
              }
        $html.="<td>".$a->observaciones."</td>
          </tr>";

    }
    /*$html.="</tbody>
          </table>";
    echo $html;*/

    $fin_ok=$cont_ok+$cont_ok2+$cont_ok3+$cont_ok4+$cont_ok5+$cont_ok6+$cont_ok7;
    $fin_progra=$cont_pro+$cont_pro2+$cont_pro3+$cont_pro4+$cont_pro5+$cont_pro6+$cont_pro7;
    /*if($cont_pro>0)
      $efi=$cont_ok/$cont_pro*100;
    else
      $efi=0;
    if($cont_pro2>0)
      $efi2=$cont_ok2/$cont_pro2*100;
    else
      $efi2=0;
    if($cont_pro3>0)
      $efi3=$cont_ok3/$cont_pro3*100;
    else
      $efi3=0;
    if($cont_pro4>0)
      $efi4=$cont_ok4/$cont_pro4*100;
    else
      $efi4=0; 
    if($cont_pro5>0)
      $efi5=$cont_ok5/$cont_pro5*100;
    else
      $efi5=0; 
    if($cont_pro6>0)
      $efi6=$cont_ok6/$cont_pro6*100;
    else
      $efi6=0; 
    if($cont_pro7>0)
      $efi7=$cont_ok7/$cont_pro7*100;
    else
      $efi7=0;*/

    if($cont_ok>0)
      $efi=$cont_ok/$cont_ok*100;
    else
      $efi=0;
    if($cont_ok2>0)
      $efi2=$cont_ok2/$cont_ok2*100;
    else
      $efi2=0;
    if($cont_ok3>0)
      $efi3=$cont_ok3/$cont_ok3*100;
    else
      $efi3=0;
    if($cont_ok4>0)
      $efi4=$cont_ok4/$cont_ok4*100;
    else
      $efi4=0; 
    if($cont_ok5>0)
      $efi5=$cont_ok5/$cont_ok5*100;
    else
      $efi5=0; 
    if($cont_ok6>0)
      $efi6=$cont_ok6/$cont_ok6*100;
    else
      $efi6=0; 
    if($cont_ok7>0)
      $efi7=$cont_ok7/$cont_ok7*100;
    else
      $efi7=0;  

    if($cont_ok==${'cont_total_1'} && $cont_ok>0){
      $eef=100;
    }
    else{
      if(${'cont_total_1'}>0)
        $eef=round(($cont_ok/${'cont_total_1'})*100,3);
      else
        $eef=0;        
    }

    if($cont_ok2==${'cont_total_2'} && $cont_ok2>0){
      $eef2=100;
    }
    else{
      if(${'cont_total_2'}>0)
        $eef2=round(($cont_ok2/${'cont_total_2'})*100,3);
      else
        $ee2=0;
    }

    if($cont_ok3==${'cont_total_3'} && $cont_ok3>0){
      $eef3=100;
    }
    else{
      if(${'cont_total_3'}>0) 
        $eef3=round(($cont_ok3/${'cont_total_3'})*100,3);
      else
        $eef3=0;
    }

    if($cont_ok4==${'cont_total_4'} && $cont_ok4>0){
      $eef4=100;
    }
    else{
      if(${'cont_total_4'}>0) 
        $eef4=round(($cont_ok4/${'cont_total_4'})*100,3);
      else
        $eef4=0;
    }

    if($cont_ok5==${'cont_total_5'} && $cont_ok5>0){
      $eef5=100;
    }
    else{
      if(${'cont_total_5'}>0) 
        $eef5=round(($cont_ok5/${'cont_total_5'})*100,3);
      else
        $eef5=0;
    }

    if($cont_ok6==${'cont_total_6'} && $cont_ok6>0){
      $eef6=100;
    }
    else{
      if(${'cont_total_6'}>0) 
        $eef6=round(($cont_ok6/${'cont_total_6'})*100,3);
      else
        $eef6=0;
    }

    if($cont_ok7==${'cont_total_7'} && $cont_ok7>0){
      $eef7=100;
    }
    else{
      if(${'cont_total_7'}>0) 
        $eef7=round(($cont_ok7/${'cont_total_7'})*100,3);
      else
        $eef7=0;
    }

    if(is_nan($eef))
      $eef=0;
    if(is_nan($eef2))
      $eef2=0;
    if(is_nan($eef3))
      $eef3=0;
    if(is_nan($eef4))
      $eef4=0;
    if(is_nan($eef5))
      $eef5=0;
    if(is_nan($eef6))
      $eef6=0;
    if(is_nan($eef7))
      $eef7=0;

    /*log_message('error', 'cont_ok4 : '.$cont_ok4);
    log_message('error', 'cont_pro4 : '.$cont_pro4);
    log_message('error', 'efi4 : '.$efi4);*/

    $html_foot="
            <tr>
              <td colspan='6'></td>
              <td>CUMPLIMIENTO</td>
              <td id='cont_ok'>".$cont_ok."</td>
              <td id='cont_ok2'>".$cont_ok2."</td>
              <td id='cont_ok3'>".$cont_ok3."</td>
              <td id='cont_ok4'>".$cont_ok4."</td>
              <td id='cont_ok5'>".$cont_ok5."</td>
              <td id='cont_ok6'>".$cont_ok6."</td>
              <td id='cont_ok7'>".$cont_ok7."</td>
              <td id='fin_ok'>".$fin_ok."</td>
            </tr>
          <!--<tr>
            <td colspan='6'></td>
            <td>PROGRAMADO</td>
            <td id='cont_pro'>".$cont_pro."</td>
            <td id='cont_pro2'>".$cont_pro2."</td>
            <td id='cont_pro3'>".$cont_pro3."</td>
            <td id='cont_pro4'>".$cont_pro4."</td>
            <td id='cont_pro5'>".$cont_pro5."</td>
            <td id='cont_pro6'>".$cont_pro6."</td>
            <td id='cont_pro7'>".$cont_pro7."</td>
            <td>".$fin_progra."</td>
          </tr>-->
          <tr>
            <td colspan='6'></td>
            <td>PROGRAMADO</td>
            <td id='cont_pro'>".${'cont_total_1'}."</td>
            <td id='cont_pro2'>".${'cont_total_2'}."</td>
            <td id='cont_pro3'>".${'cont_total_3'}."</td>
            <td id='cont_pro4'>".${'cont_total_4'}."</td>
            <td id='cont_pro5'>".${'cont_total_5'}."</td>
            <td id='cont_pro6'>".${'cont_total_6'}."</td>
            <td id='cont_pro7'>".${'cont_total_7'}."</td>
            <td>".($cont_item)."</td>
          </tr>
          <tr>
            <td colspan='5'></td>
            <td>CUMPLIMIENTO AL OBJETIVO</td>
            <td>EFICIENCIA</td>
            <td id='efi'>".$eef."%</td>
            <td id='efi2'>".$eef2."%</td>
            <td id='efi3'>".$eef3."%</td>
            <td id='efi4'>".$eef4."%</td>
            <td id='efi5'>".$eef5."%</td>
            <td id='efi6'>".$eef6."%</td>
            <td id='efi7'>".$eef7."%</td>
            <td>".round((($eef+$eef2+$eef3+$eef4+$eef5+$eef6+$eef7)/7),3)."%</td>
          </tr>
          <tr>
            <td colspan='6'></td>
            <td>LIMPIEZAS OK</td>
            <td>".$cont_ok."</td>
            <td>".$cont_ok2."</td>
            <td>".$cont_ok3."</td>
            <td>".$cont_ok4."</td>
            <td>".$cont_ok5."</td>
            <td>".$cont_ok6."</td>
            <td>".$cont_ok7."</td>
            <td>".$fin_ok."</td>
          </tr>
          <tr>
            <td colspan='5'></td>
            <td>CALIDAD EN LA LIMPIEZA</td>
            <td>EFICACIA</td>
            <td>".$eef."%</td>
            <td>".$eef2."%</td>
            <td>".$eef3."%</td>
            <td>".$eef4."%</td>
            <td>".$eef5."%</td>
            <td>".$eef6."%</td>
            <td>".$eef7."%</td>
            <td id='fin_eficacia'>".round((($eef+$eef2+$eef3+$eef4+$eef5+$eef6+$eef7)/7),3)."%</td>
          </tr>";

    /*for($day=1; $day<=7; $day++)
    {
      //$html_head.="<td>".date('d', strtotime($anio."W".$semana.$day))."</td>";
      //log_message('error', 'dias de la semana : '.date('d', strtotime($anio."W".$semana.$day)));
      $html_head[]=array(date('d', strtotime($anio."W".$semana.$day)));
    }*/
    echo json_encode(array("html"=>$html,"html_head"=>$html_head,"logo"=>$logo,"empresa"=>$empresa,"fecha_ini"=>$fecha_ini,"fecha_final"=>$fecha_final,"anio"=>$anio,"html_foot"=>$html_foot));
  }

  function insertGraph(){
    $datax = $this->input->post();
    $id=$datax['id'];
    if($datax['tipo']!="7")
      $result=$this->ModeloGeneral->getselectwhere_n_consulta('graficas_limpieza',array("id_proyecto"=>$datax['id_proyecto'],"tipo"=>$datax['tipo'],"mes"=>$datax['mes'],"anio"=>$datax['anio']));
    else
      $result=$this->ModeloGeneral->getselectwhere_n_consulta('graficas_limpieza',array("id_proyecto"=>$datax['id_proyecto'],"tipo"=>$datax['tipo'],"mes"=>$datax['mes'],"semana"=>$datax['semana'],"anio"=>$datax['anio']));

    $cont=0;
    $id_cons=0;
    foreach ($result as $row){
      $cont=1;
      $id_cons=$row->id;
    }
    if($cont==0){
      $this->ModeloGeneral->tabla_inserta('graficas_limpieza',$datax);
    }
    else{
      unset($datax["id"]);
      $this->ModeloGeneral->updateCatalogo($datax,'id',$id_cons,'graficas_limpieza');
    }
  }

  function pdfLimpieza($id,$mes,$anio){
    $data["id_proy"]=$id;
    $data["mes"]=$mes;
    $data["anio"]=$anio;
    $data["name_mes"] = $this->nameMes($mes);
    $logo_cli=$this->ModeloGeneral->getLogoCli($id);
    $data['logo_cli'] = $logo_cli->foto;
    $data["gra"]=$this->ModeloGeneral->getselectwhere_n_consulta('graficas_limpieza',array('id_proyecto'=>$id,'mes'=>$mes,'anio'=>$anio));
    $params['id_proy']=$id;
    $params['mes']=$mes;
    $params['anio']=$anio;
    $table_com="";
    $data["table_com"]='
        <style type="text/css">
          .table{ font-size:10px; text-align:center; font-family:monospace; }
          .table_thead{
            background-color:#00B0F0;
            color: white !important;
            text-align: center;
            vertical-align: middle;
          }
          .btn {
            border: none;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 10px;
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
          }
          .btn-warning {
            color: #000;
            background-color: #ffc107;
            border-color: #ffc107; 
          }
          .btn-danger {
            color: #fff;
            background-color: #dc3545;
            border-color: #dc3545; 
          }
        </style>
          <table cellpadding="4" class="table" border="1" width="100%">
            <thead>
              <tr style="font-weight: bold; color: white; background-color:#00B0F0;">
                <th colspan="5"><b>COMENTARIOS A ACTIVIDADES NO REALIZADAS</b></th>
              </tr>
              <tr style="font-weight: bold; color: white; background-color:#00B0F0;">
                <th width="15%"><b>FECHA</b></th>
                <th width="22%"><b>ACTIVIDAD</b></th>
                <th width="18%"><b>OT</b></th>
                <th width="33%"><b>COMENTARIOS</b></th>
                <th width="12%"><b>ESTATUS</b></th>
              </tr>
            </thead>
            <tbody>';
    $repro=$this->ModelReportes->get_ordenes_coments($params);
    $cont=0;
    foreach($repro as $r){
      $cont++;
      $fecha_comp=$this->saber_dia(date("Y-m-d",strtotime($r->fecha)))." ".date("d",strtotime($r->fecha)) ." de ".$this->nameMes(date("m",strtotime($r->fecha)));
      if($r->seguimiento==1){//Cancelado
        $estatus='<span class="btn btn-danger"><b>Cancelado</span></b>';
      }
      elseif($r->seguimiento==4){//Reprogramado
        $estatus='<span class="btn btn-warning"><b>Reprogramado</span></b>';
      }elseif($r->seguimiento==5){//No realizado
        $estatus='<span class="btn btn-warning"><b>No realizado</span></b>';
      }
      $table_com.='<tr>
            <td width="15%">'.$fecha_comp.'</td>
            <td width="22%">'.$r->actividad.'</td>
            <td width="18%">'.$r->ot.'</td>
            <td width="33%">'.$r->observaciones.'</td>
            <td width="12%">'.$estatus.'</td>
          </tr>';
    }
    if($cont>0){
      $data["table_com"].='<tbody>
                  '.$table_com.'
              </tbody>
          </table>';
    }else{
      $data["table_com"].='<tbody>
            <tr>
              <td width="15%"></td>
              <td width="22%"></td>
              <td width="18%"></td>
              <td width="33%"></td>
              <td width="12%"></td>
            </tr>      
          </tbody>
        </table>';
    }
    $this->load->view('reportes/pdf_limpieza',$data);
  }

  ////////////////////////////////////////////
  public function resultadosMatrizAccion($id,$mes,$anio){
    $id_cli=0;
    $params['id_proy']=$id;
    $params['mes']=$mes;
    $params['anio']=$anio;
    $data['id_proy']=$id;
    $data['id_cli']=$id_cli;
    $data['mes']=$mes;
    $data['anio']=$anio;
    $data["res"]=$this->ModelReportes->get_data_matriz_accion($params,$id_cli);

    $data["total"]=0;
    $data["cerrado"]=0;
    $data["progreso"]=0;
    $data["abierto"]=0;
    $data["cerrado_cli"]=0;
    $data["progreso_cli"]=0;
    $data["abierto_cli"]=0;

    foreach($data["res"] as $r){
      $fecha2=date("Y-m-d", strtotime($r->fecha_deteccion));
      //log_message('error', 'fecha2 : '.$fecha2);
      $mes_detec=date("m", strtotime($fecha2));
      $anio_detec=date("Y", strtotime($fecha2));
      /*log_message('error', 'mes_detec : '.$mes_detec);
      log_message('error', 'anio_detec : '.$anio_detec);
      log_message('error', 'avance : '.$r->avance);*/
      
      if($r->avance==4 && $r->tipo_hallazgo==1 && $mes_detec==$mes && $anio_detec==$anio){
        $data["cerrado"]++;
        $data["total"]++;
      }
      if($r->avance>0 && $r->avance<4 && $r->tipo_hallazgo==1){
        $data["progreso"]++;
        $data["total"]++;
      }
      if($r->avance==0 && $r->tipo_hallazgo==1){
        $data["abierto"]++;
        $data["total"]++;
      }

      if($r->avance==4 && $r->tipo_hallazgo==2 && $mes_detec==$mes && $anio_detec==$anio){
        $data["cerrado_cli"]++;
        $data["total"]++;
      }
      if($r->avance>0 && $r->avance<4 && $r->tipo_hallazgo==2){
        $data["progreso_cli"]++;
        $data["total"]++;
      }
      if($r->avance==0 && $r->tipo_hallazgo==2){
        $data["abierto_cli"]++;
        $data["total"]++;
      }
    }
  
    $this->load->view('reportes/resultados_matriz_accion',$data);
    $this->load->view('reportes/resultados_matriz_accionjs');
  }

  function pdfMatrizAcciones2($id){
    $data["id_proy"]=$id;
    $params['id_proy']=$id;
    $data["res"]=$this->ModelReportes->get_data_matriz_accion($params,0);
    $data["total"]=0;
    $data["cerrado"]=0;
    $data["progreso"]=0;
    $data["abierto"]=0;

    foreach($data["res"] as $r){
      $fecha2=date("Y-m-d", strtotime($r->fecha_deteccion));
      $mes_detec=date("m", strtotime($fecha2));
      $anio_detec=date("Y", strtotime($fecha2));
      /*log_message('error', 'mes_detec : '.$mes_detec);
      log_message('error', 'anio_detec : '.$anio_detec);
      log_message('error', 'avance : '.$r->avance);*/
      
      if($r->avance==4 && $r->tipo_hallazgo==1 && $mes_detec==$mes && $anio_detec==$anio){
        $data["cerrado"]++;
        $data["total"]++;
      }
      if($r->avance>0 && $r->avance<4 && $r->tipo_hallazgo==1){
        $data["progreso"]++;
        $data["total"]++;
      }
      if($r->avance==0 && $r->tipo_hallazgo==1){
        $data["abierto"]++;
        $data["total"]++;
      }

      /*$data["total"]++;
      if($r->avance==4)
        $data["cerrado"]++;
      if($r->avance>0 && $r->avance<4)
        $data["progreso"]++;
      if($r->avance==0)
        $data["abierto"]++;*/
    }
    $logo_cli=$this->ModeloGeneral->getLogoCli($id);
    $data['logo_cli'] = $logo_cli->foto;
    $data["gra"]=$this->ModeloGeneral->getselectwhere_n_consulta('graficas_matriz_accion',array('id_proyecto'=>$id));
    $this->load->view('reportes/tablas_test',$data);
  }

  function pdfMatrizAcciones($id,$mes,$anio){
    $data["id_proy"]=$id;
    $params['id_proy']=$id;
    $params["mes"]=$mes;
    $params["anio"]=$anio;
    $data["mes"]=$mes;
    $data["anio"]=$anio;
    $data["res"]=$this->ModelReportes->get_data_matriz_accion($params,0);
    $data["total"]=0;
    $data["cerrado"]=0;
    $data["progreso"]=0;
    $data["abierto"]=0;
    $data["cerrado_cli"]=0;
    $data["progreso_cli"]=0;
    $data["abierto_cli"]=0;

    foreach($data["res"] as $r){
      $fecha2=date("Y-m-d", strtotime($r->fecha_deteccion));
      $mes_detec=date("m", strtotime($fecha2));
      $anio_detec=date("Y", strtotime($fecha2));

      if($r->avance==4 && $r->tipo_hallazgo==1 && $mes_detec==$mes && $anio_detec==$anio){
        $data["cerrado"]++;
        $data["total"]++;
      }
      if($r->avance>0 && $r->avance<4 && $r->tipo_hallazgo==1){
        $data["progreso"]++;
        $data["total"]++;
      }
      if($r->avance==0 && $r->tipo_hallazgo==1){
        $data["abierto"]++;
        $data["total"]++;
      }

      if($r->avance==4 && $r->tipo_hallazgo==2 && $mes_detec==$mes && $anio_detec==$anio){
        $data["cerrado"]++;
        $data["total"]++;
      }
      if($r->avance>0 && $r->avance<4 && $r->tipo_hallazgo==2){
        $data["progreso"]++;
        $data["total"]++;
      }
      if($r->avance==0 && $r->tipo_hallazgo==2){
        $data["abierto"]++;
        $data["total"]++;
      }

      /*$data["total"]++;
      if($r->avance==4)
        $data["cerrado"]++;
      if($r->avance>0 && $r->avance<4)
        $data["progreso"]++;
      if($r->avance==0)
        $data["abierto"]++;*/
    }
    $logo_cli=$this->ModeloGeneral->getLogoCli($id);
    $data['logo_cli'] = $logo_cli->foto;
    /*if($data["cerrado"]>0){
      $this->pdfMatrizAccionesTipoWhere($id,1,$mes,$anio);
    }
    if($data["progreso"]>0){
      $this->pdfMatrizAccionesTipoWhere($id,2,$mes,$anio);
    }
    if($data["abierto"]>0){
      $this->pdfMatrizAccionesTipoWhere($id,3,$mes,$anio);
    }*/

    $data["gra"]=$this->ModeloGeneral->getselectwhere_n_consulta('graficas_matriz_accion',array('id_proyecto'=>$id));
    $this->load->view('reportes/pdf_matriz_acciones',$data);
  }

  /*function pdfMatrizAccionesTipoWhere($id_proy,$tipo,$mes,$anio){
    $logo_cli=$this->ModeloGeneral->getLogoCli($id_proy);
    $data['logo_cli'] = $logo_cli->foto;

    $data["id_proy"]=$id_proy;
    $data['mes']=$mes;
    $data['anio']=$anio;
    $params['id_proy']=$id_proy;
    $params['mes']=$mes;
    $params['anio']=$anio;
    $data["res"]=$this->ModelReportes->get_data_matriz_accion($params,0);
    if($tipo==1)
      $this->load->view('reportes/pdf_matriz_acciones_cerrado',$data);
    else if($tipo==2)
      $this->load->view('reportes/pdf_matriz_acciones_progreso',$data);
    else if($tipo==3)
      $this->load->view('reportes/pdf_matriz_acciones_abierto',$data);
  }*/

  function pdfMatrizAccionesTipo(){
    $id_proy = $this->input->post("id_proy");
    $tipo = $this->input->post("tipo");
    $mes = $this->input->post("mes");
    $anio = $this->input->post("anio");
    $logo_cli=$this->ModeloGeneral->getLogoCli($id_proy);
    $data['logo_cli'] = $logo_cli->foto;

    $data["id_proy"]=$id_proy;
    $data['mes']=$mes;
    $data['anio']=$anio;
    $params['id_proy']=$id_proy;
    $params['mes']=$mes;
    $params['anio']=$anio;
    $data["res"]=$this->ModelReportes->get_data_matriz_accion($params,0);
    if($tipo==1)
      $this->load->view('reportes/pdf_matriz_acciones_cerrado',$data);
    else if($tipo==2)
      $this->load->view('reportes/pdf_matriz_acciones_progreso',$data);
    else if($tipo==3)
      $this->load->view('reportes/pdf_matriz_acciones_abierto',$data);
  }

  function saveGrafica(){
    $datax = $this->input->post();
    $result=$this->ModeloGeneral->getselectwhere_n_consulta('graficas_matriz_accion',array("id_proyecto"=>$datax['id_proyecto'],"tipo"=>$datax['tipo'],"mes"=>$datax['mes'],"anio"=>$datax['anio']));
    $cont=0;
    $id_cons=0;
    foreach ($result as $row){
      $cont=1;
      $id_cons=$row->id;
    }
    if($cont==0){
      $this->ModeloGeneral->tabla_inserta('graficas_matriz_accion',$datax);
    }
    else{
      unset($datax["id"]);
      $this->ModeloGeneral->updateCatalogo($datax,'id',$id_cons,'graficas_matriz_accion');
    }
  }

  /* ****************************MONITOREO DE ACTIVIDADES***************************************** */
  public function resultadosMonitoreo($id,$id_cli=0,$mes,$anio){
    $params['id_proy']=$id;
    $params['mes']=$mes;
    $params['anio']=$anio;
    $data['id_proy']=$id;
    $data['id_cli']=$id_cli;
    $data['mes']=$mes;
    $data['anio']=$anio;
    //$data["res"]=$this->ModelReportes->get_result_monitoreo($params,1);
    $get_logo=$this->ModeloGeneral->getselectwhereN("clientes","foto","id",$id_cli);
    $data['logo']=$get_logo[0]->foto;
    $where="";  
    $data["name_mes"] = mb_strtoupper($this->nameMes($mes));  
    //log_message('error', 'logo : '.$data['logo']);
    //$data["act"]=$this->ModelReportes->getTotalActiv($params);
    $this->load->view('reportes/resultados_monitoreo2',$data);
    $this->load->view('reportes/resultados_monitoreojs');
  }

  public function insertEditaEstatus(){
    $id_proy = $this->input->post("id_proy");
    $mes = $this->input->post("mes");
    $anio = $this->input->post("anio");
    $val = $this->input->post("val");
    $dia = $this->input->post("dia");
    $id_cli = $this->input->post("id_cli");
    $id_act = $this->input->post("id_act");
    $tipo = $this->input->post("tipo");
    $insp = $this->input->post("insp");
    if($dia<=9){
      $dia="0".$dia;
    }

    $id=0;
    $get=$this->ModeloGeneral->getselectwhereOrWhere("monitoreo_actividad",array("id_proyecto"=>$id_proy,"DATE_FORMAT(fecha,'%Y')"=>$anio,"DATE_FORMAT(fecha,'%d')"=>$dia,"id_limpieza"=>$id_act,"tipo"=>$tipo,"inspeccion"=>$insp,"estatus"=>1),"(mes=".$mes." or mes=0".$mes.")");
    foreach($get->result() as $g){
      $id=$g->id;
      $this->ModeloGeneral->updateCatalogo(array("seguimiento"=>$val),'id',$g->id,'monitoreo_actividad');
    }
    if($id==0){
      $fecha=$anio."/".$mes."/".$dia;

      $getact=$this->ModeloGeneral->getselectwhererow2('limpieza_actividades',array("id"=>$id_act));
      $this->ModeloGeneral->tabla_inserta('monitoreo_actividad',array("id_cliente"=>$id_cli,"id_proyecto"=>$id_proy,"tipo"=>$tipo,"id_limpieza"=>$id_act,"zona"=>$getact->zona,"inspeccion"=>$insp,"num_inspeccion"=>1,"frecuencia"=>$getact->frecuencia_cotiza,"fecha"=>$fecha,"mes"=>$mes,"reg"=>date("Y-m-d H:i:s"),"seguimiento"=>$val,"id_user_reg"=>$this->idpersonal));
    }
    
  }

  public function exportExcelMonitoreo($id,$mes,$anio,$id_act){
    $params["id_proy"] = $id;
    $params["mes"] = $mes;
    $params["anio"] = $anio;
    $params["id_act"] = $id_act;
    $where="";
    $data["id_proy"]=$id;
    $data["mes"]=$mes;
    $data["anio"]=$anio;
    $data["id_act"] = $id_act;
    //$data["act"]=$this->ModelReportes->get_result_monitoreo($params,1,$where);
    $data["act"]=$this->ModelReportes->get_result_monitoreo($params,1,$where,$id_act);
    $this->load->view('reportes/excelMonitoreo',$data);
  }

  public function exportExcelMonitoreoDiario($id,$mes,$anio,$id_act){
    $params["id_proy"] = $id;
    $params["mes"] = $mes;
    $params["anio"] = $anio;
    $params["id_act"] = $id_act;
    $where="";
    $data["id_proy"]=$id;
    $data["mes"]=$mes;
    $data["anio"]=$anio;
    $data["id_act"] = $id_act;
    //$data["act"]=$this->ModelReportes->get_result_monitoreo($params,0,$where);
    $data["act"]=$this->ModelReportes->get_result_monitoreoNvo($params,0,$where,$id_act);
    $this->load->view('reportes/excelMonitoreoDia',$data);
  }

  public function getMonitoreoFin(){
    $params = $this->input->post();
    $where=""; $logo="";
    $act=$this->ModelReportes->get_result_monitoreo($params,1,$where);
    $html_head=""; $html=""; $html_foot=""; $i=0; $array=array();
    $anio=0; $fecha_f="";
    foreach ($act as $a) {
      $fecha_i = ''.$a->mes.'/01/'.$a->anio.'';
      //$fecha_f = ''.$a->mes.'/'.$a->ultimo.'/'.$a->anio.'';
      $fecha_f = $a->ultimo;
    }

    if($fecha_f==""){
      $fecha_i = ''.$params["mes"].'/01/'.date("Y").'';
      $ult=date("t",mktime(0,0,0,$params["mes"],1,date("Y")));
      $fecha_f = ''.$params["mes"].'/'.$ult.'/'.date("Y").'';
    }
    //log_message('error', 'fecha_f : '.$fecha_f);

    $begin = new DateTime($fecha_i);
    $end = new DateTime($fecha_f);
    $end = $end->modify( '+1 day' );
    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);
    $html_head="<tr>
                  <th>#</th>
                  <th>ZONA</th>
                  <th>INSPECCIÓN</th>
                  <th>DESCRIPCIÓN DE LA ACTIVIDAD</th>
                  <th>COMENTARIOS</th>
                  <th>FRECUENCIA</th>";

    $band_dia=0;
    foreach($daterange as $date){
      if(date('l', strtotime($date->format("d-m-Y"))) == 'Sunday'){
        $band_dia++;
        //log_message('error', 'dia del fin : '.$date->format("d"));
        $html_head.="<th id='dia_num".$band_dia."'>".$date->format("d")."</th>";
        array_push($array,$date->format("d"));
      }
    }
    $html_head.="<th>ÁREA DE OPORTUNIDAD</th><th></th>
              </tr>";

    //log_message('error', 'contenido del array : '.$array[0]);
    //log_message('error', 'html_head : '.$html_head);
    $cont=0; $cont2=0; $cont3=0; $cont4=0; $cont5=0; 
    //log_message('error', 'count del array : '.count($array));

    $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].')';
    if(isset($array[4])){
      $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].' or DATE_FORMAT(ma.fecha, "%d")='.$array[4].')';
    }

    $cont_area=0; $cont_area1=0; $cont_area2=0; $cont_area3=0; $cont_area4=0; $tmp2=[]; $i_aux=0; $i=0; $i1=0; $i2=0; $i3=0; $i4=0;
    $val_activ=0; $cont_activ=0;
    $dia_ant=0; $inputs_data=""; $array_act=array();
    $item=0;
    $act=$this->ModelReportes->get_result_monitoreo($params,1,$where);
    foreach ($act as $a) {
      $cont=0;
      $fecha_camb = strtotime($a->fecha);
      $semana = date('W', $fecha_camb);
      $i_aux++;
      $cont_exist=0;
      $logo=$a->foto;
      $anio=$a->anio;
      $tmp=explode(",",$a->dias_act);
      $tot_act=$a->tot_act;
      //$tmp=ksort($tmp);
      $tmp2=$tmp;
      //log_message('error', 'dias_act  : '.$a->dias_act);
      //log_message('error', 'seguimiento_act  : '.$a->seguimiento_act);
      $tmp_seg=explode(", ",$a->seguimiento_act);
      //$tmp=array_unique($tmp);
      //log_message('error', 'tmp count: '.count($tmp));
      //log_message('error', 'tmp2 ksort: '.$tmp2);
      $item++;
      $html.="<tr>
              <td>".$item."</td>
              <td><input id='zona' type='hidden' value='".$a->zona."'><input id='inspeccion' type='hidden' value='".$a->inspeccion."'><input id='activ' type='hidden' value='".$a->nombre."'><input id='id_act' type='hidden' value='".$a->id_limpieza."'>".$a->zona."</td>
              <td>".$a->inspeccion."</td>
              <td>".$a->nombre."</td>
              <td>".$a->comentarios_br."</td>
              <td>".$a->frecuencia."</td>";
              for($j=0; $j<count($array); $j++){
                //for($k=0; $k<count($tmp); $k++){       

                $tot_act2=$tot_act-($tot_act+$j);
                //log_message('error', 'tot_act2: '.abs($tot_act2));
                //log_message('error', 'j: '.$j);
                //log_message('error', 'tot_act2: '.abs($tot_act2));

                //if($dia_ant!=$tmp_seg[$cont_exist]){
                  if (in_array($array[$j], $tmp))
                  {
                    if(isset($tmp_seg[$cont_exist])){
                      $html.="<td>".$tmp_seg[$cont_exist]."</td>";
                      if($tmp_seg[$cont_exist]==1){
                        $cont++;
                      }
                    }/*else{
                      $html.="<td>".$tmp_seg[$j]."</td>";
                      if($tmp_seg[$j]==1){
                        $cont++;
                      }
                    }*/
                  }else{
                    $html.="<td></td>";
                  }

                  if (in_array($array[$j], $tmp))
                  {
                    /*log_message('error', 'array: '.$array[$j]);
                    log_message('error', 'existe en: '.$j);
                    log_message('error', 'cont_exist : '.$cont_exist);
                    
                    log_message('error', 'tmp_seg cont_exist: '.$tmp_seg[abs($tot_act2)]);*/
                    
                    if(isset($j) && $j==0 && $tmp_seg[$cont_exist]==1){
                      $cont_area++;
                    }
                    if(isset($j) && $j==0 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==0 && $tmp_seg[$cont_exist]==0){
                      $i++;
                    }
                    if(isset($j) && $j==1 && $tmp_seg[$cont_exist]==1){
                      $cont_area1++;
                    }
                    if(isset($j) && $j==1 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==1 && $tmp_seg[$cont_exist]==0){
                      $i1++;
                    }
                    if(isset($j) && $j==2 && $tmp_seg[$cont_exist]==1){
                      $cont_area2++;
                    }
                    if(isset($j) && $j==2 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==2 && $tmp_seg[$cont_exist]==0){
                      $i2++;
                    }
                    if(isset($j) && $j==3 && $tmp_seg[$cont_exist]==1){
                      $cont_area3++;
                    }
                    if(isset($j) && $j==3 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==3 && $tmp_seg[$cont_exist]==0){
                      $i3++;
                    }
                    if(isset($j) && $j==4 && $tmp_seg[$cont_exist]==1){
                      $cont_area4++;
                    }
                    if(isset($j) && $j==4 && $tmp_seg[$cont_exist]==1 || isset($j) && $j==4 && $tmp_seg[$cont_exist]==0){
                      $i4++;
                    }
                    $cont_exist++;
                  }
                //}
                //$dia_ant=$tmp_seg[$j];
              }

        /*$html.="<td><input id='val_area' type='hidden' value='".round(($cont/count($array)*100),2)."'>".round(($cont/count($array)*100),2)."</td>
              </tr>";*/
        if($a->id_limpieza==$id_limpieza_ant){
          $cont_activ++;
          $val_activ = $val_activ+(round(($cont/$a->tot_act*100),2));
          //$val_activ = (round(($cont/$a->tot_act*100),2));
        }else{
          $cont_activ=1;
          $val_activ = (round(($cont/$a->tot_act*100),2));
        }
        $inputs_data="<td class='".$a->id_limpieza."'><input class='id_".$a->id_limpieza."' data-limp='".$a->id_limpieza."' type='hidden' id='".$cont_activ."s_f_activ_".$a->id_limpieza."' value='".round($val_activ/$cont_activ,2)."'></td>";
        $html.="<td><input id='val_area' type='hidden' value='".round(($cont/$a->tot_act*100),2)."'>".round(($cont/$a->tot_act*100),2)."</td>
              ".$inputs_data."</tr>";
        
        /*log_message('error', 'nombre: '.$a->nombre);
        log_message('error', 'id limpieza: '.$a->id_limpieza);
        log_message('error', 'val_activ: '.$val_activ);*/
        $id_limpieza_ant=$a->id_limpieza;
    }
    //log_message('error', 'band_dia : '.$band_dia);
    if($i_aux==0){
      $html_foot="<tr>
              <td colspan='3'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>
              <td><input type='hidden' id='ft1' value='0'>0</td>
              <td><input type='hidden' id='ft2' value='0'>0</td>
              <td><input type='hidden' id='ft3' value='0'>0</td>
              <td><input type='hidden' id='ft4' value='0'>0</td>";
            if($band_dia==5){
              $html_foot.="<td><input type='hidden' id='ft5' value='0'>0</td>";
            }
          $html_foot.="</tr>";
    }
    else{
      //log_message('error', 'cont_area : '.$cont_area);
      //log_message('error', 'i : '.$i);
      $ft1=0; $ft2=0; $ft3=0; $ft4=0; $ft5=0;
      if($i>0){
        $ft1=round(($cont_area/$i*100),2);
      }
      if($i1>0){
        $ft2=round(($cont_area1/$i1*100),2);
      }
      if($i2>0){
        $ft3=round(($cont_area2/$i2*100),2);
      }
      if($i3>0){
        $ft4=round(($cont_area3/$i3*100),2);
      }
      if($i4>0){
        $ft5=round(($cont_area4/$i4*100),2);
      }
      
      $html_foot="<tr>
              <td colspan='3'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>
              <td><input type='hidden' id='ft1' value='".$ft1."'>".$ft1."</td>
              <td><input type='hidden' id='ft2' value='".$ft2."'>".$ft2."</td>
              <td><input type='hidden' id='ft3' value='".$ft3."'>".$ft3."</td>
              <td><input type='hidden' id='ft4' value='".$ft4."'>".$ft4."</td>";
          if($band_dia==5){
            $html_foot.="<td><input type='hidden' id='ft5' value='".$ft5."'>".$ft5."</td>";
          }
      $html_foot.="<td></td>
      </tr>";
    }
    echo json_encode(array("html"=>$html,"html_head"=>$html_head,"anio"=>$anio,"html_foot"=>$html_foot,"cant_dias"=>$band_dia,"logo"=>$logo));
  }

  public function getMonitoreoDiario(){
    $params = $this->input->post();

    $where="";    
    $act=$this->ModelReportes->get_result_monitoreo($params,0,$where);
    $html_head=""; $html=""; $html_foot=""; $i=0; $array=array();
    $anio=0; $fecha_f=""; $logo="";
    foreach ($act as $a) {
      $fecha_i = ''.$a->mes.'/01/'.$a->anio.'';
      $fecha_f = $a->ultimo;
    }
    if($fecha_f==""){
      $fecha_i = ''.$params["mes"].'/01/'.date("Y").'';
      $fecha_f = ''.$params["mes"].'/31/'.date("Y").'';
    }

    $begin = new DateTime($fecha_i);
    $end = new DateTime($fecha_f);
    $end = $end->modify( '+1 day' );
    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);
    $html_head="<tr>
                  <th>ZONA</th>
                  <th>INSPECCIÓN</th>
                  <th>DESCRIPCIÓN DE LA ACTIVIDAD</th>
                  <th>COMENTARIOS</th>
                  <th>FRECUENCIA</th>";

    $html_foot="<tr>
              <td colspan='3'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>";

    foreach($daterange as $date){
      if(date('l', strtotime($date->format("d-m-Y"))) != 'Sunday'){
        //log_message('error', 'dia del fin : '.$date->format("d"));
        $html_head.="<th><input id='num_dia' type='hidden' value='".$date->format("d")."'>".$date->format("d")."</th>";
        ${'dias_'.intval($date->format("d"))}=array();
        array_push($array,$date->format("d"));
        $html_foot.="<td id='ft_".intval($date->format("d"))."'></td>";
      }
    }
    $html_foot.="<td></td>
            </tr>";

    $html_head.="<th>ÁREA DE OPORTUNIDAD</th><th></th>
              </tr>";
    $cont=0; $cont2=0; $cont3=0; $cont4=0; $cont5=0; 

    $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].')';
    if(isset($array[4])){
      $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].' or DATE_FORMAT(ma.fecha, "%d")='.$array[4].')';
    }

    $html_foot2=""; $r=0; $cont_item=0;
    $cont_area=0; $cont_area1=0; $cont_area2=0; $cont_area3=0; $cont_area4=0; ${'cont_1'}=0;
    $act=$this->ModelReportes->get_result_monitoreo($params,0,$where);
    foreach ($act as $a) {
      $cont_item++;
      $cont=0;
      $cont_exist=0;
      $fecha_camb = strtotime($a->fecha);
      $semana = date('W', $fecha_camb);
      $i++;
      $logo=$a->foto;
      $anio=$a->anio;
      $tmp=explode(", ",$a->dias_act);
      $tmp_seg=explode(", ",$a->seguimiento_act);
      //log_message('error', 'tmp_seg : '.count($tmp_seg));
      //log_message('error', 'id : '.$a->id);
      //log_message('error', 'seguimiento_act : '.$a->seguimiento_act);
      $html.="<tr>
              <td><input id='zona' type='hidden' value='".$a->zona."'>".$a->zona."</td>
              <td><input id='inspeccion' type='hidden' value='".$a->inspeccion."'>".$a->inspeccion."<input id='activ' type='hidden' value='".$a->nombre."'><input id='id_act' type='hidden' value='".$a->id_limpieza."'></td>
              <td>".$a->nombre."</td>
              <td>".$a->comentarios_br."</td>
              <td>".$a->frecuencia."</td>";
              for($j=$r; $j<count($array); $j++){
                //log_message('error', 'j : '.$j);
                //for($k=0; $k<count($tmp); $k++){
                //log_message('error', 'array : '.$array[$j]);
                //log_message('error', 'tmp : '.$tmp[$j]);
                //log_message('error', 'count tmp_seg : '.$tmp_seg);
              
                if(in_array($array[$j], $tmp)){ //son dias del mes
                  //log_message('error', 'el dia existe : '.$array[$j]);
                  //log_message('error', 'cont_exist : '.$cont_exist);
                  //log_message('error', 'j : '.$j);
                  if(isset($tmp_seg[$cont_exist])){
                    //log_message('error', 'tmp_seg existe : '.$tmp_seg[$cont_exist]);
                    if($tmp_seg[$cont_exist]=="1"){
                      ${'dias_'.intval($array[$j])}[]=1;
                    }
                    else if($tmp_seg[$cont_exist]=="0"){
                      ${'dias_'.intval($array[$j])}[]=1;
                    }
                    else if($tmp_seg[$cont_exist]=="2"){ //N/A
                      ${'dias_'.intval($array[$j])}[]=1;
                    }
                
                    $html.="<td><input data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='".$tmp_seg[$cont_exist]."'>".$tmp_seg[$cont_exist]."</td>";

                    if($tmp_seg[$cont_exist]=="1"){
                      $cont++;
                      ${'cont_'.intval($array[$j])}++;
                    }
                  }else{
                    //${'cont_'.intval($array[$j])}=0;
                    ${'dias_'.intval($array[$j])}[]=0;

                    $html.="<td><input data-id='".$a->id."' data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='0'>0</td>";
                  }
                  $cont_exist++;
                }else{
                  ${'dias_'.intval($array[$j])}[]=0;
                  $html.="<td><input data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='0'></td>";
                }
                $html_foot="<tr>
                <td colspan='3'></td>
                  <td colspan='2'>% CUMPLIMIENTO</td>
                  ".$html_foot2."
                  <td></td>
                </tr>";
              }

          $html_foot2x='';

          for($j=0; $j<count($array); $j++){
            
            //$html_foot2x.='<td>'.array_sum(${'dias_'.intval($array[$j])}).'</td>';
            //$porcentaje=(array_sum(${'dias_'.intval($array[$j])})/count(${'dias_'.intval($array[$j])}))*100; //por el numero de filas
            if(isset(${'cont_'.intval($array[$j])}) && ${'cont_'.intval($array[$j])}>0){ //son dias del mes
              //log_message('error', "cont_: ".${'cont_'.intval($array[$j])});
              //log_message('error', "dias_: ".array_sum(${'dias_'.intval($array[$j])}));
              //log_message('error', 'el dia existe : '.$array[$j]);
              $porcentaje=(${'cont_'.intval($array[$j])}/array_sum(${'dias_'.intval($array[$j])}))*100;
            }else{
              $porcentaje=0;
            }
            $html_foot2x.='<td>'.round($porcentaje,2).'</td>';
          }
          $html_foot="<tr>
            <td colspan='3'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>
              ".$html_foot2x."
              <td></td>
            </tr>";
        //log_message('error', 'count array : '.count($array));
        //log_message('error', 'cont item : '.$cont_item);
        if($a->id_limpieza==$id_limpieza_ant){
          $cont_activ++;
          $val_activ = $val_activ+(round(($cont/$a->tot_act*100),2));
          //$val_activ = (round(($cont/$a->tot_act*100),2));
        }else{
          $cont_activ=1;
          $val_activ = (round(($cont/$a->tot_act*100),2));
        }
        $inputs_data="<td class='".$a->id_limpieza."'><input class='id_".$a->id_limpieza."' data-limp='".$a->id_limpieza."' type='hidden' id='".$cont_activ."s_f_activ_".$a->id_limpieza."' value='".round($val_activ/$cont_activ,2)."'></td>";

        $html.="<td><input id='val_area' type='hidden' value='".round(($cont/$a->tot_act*100),2)."'>".round(($cont/$a->tot_act*100),2)."</td>
              ".$inputs_data."</tr>";
    }
    /*$html_foot="<tr>
              <td colspan='2'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>
              ".$html_foot2."
              <td></td>
            </tr>";*/
    echo json_encode(array("html"=>$html,"html_head"=>$html_head,"anio"=>$anio,"html_foot"=>$html_foot,"logo"=>$logo));
  }

  public function getMonitoreoCompleta(){
    $params = $this->input->post();
    $where="";    
    $act=$this->ModelReportes->get_result_monitoreo2($params);
    $html_head=""; $html=""; $html_foot=""; $i=0; $array=array();
    $anio=0; $fecha_f="";
    foreach ($act as $a) {
      $fecha_i = ''.$a->mes.'/01/'.$a->anio.'';
      $fecha_f = $a->ultimo;
    }
    if($fecha_f==""){
      $fecha_i = ''.$params["mes"].'/01/'.date("Y").'';
      $fecha_f = ''.$params["mes"].'/31/'.date("Y").'';
    }

    $begin = new DateTime($fecha_i);
    $end = new DateTime($fecha_f);
    $end = $end->modify( '+1 day' );
    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);
    $html_head="<tr>
                  <th>ZONA</th>
                  <th>INSPECCIÓN</th>
                  <th>DESCRIPCIÓN DE LA ACTIVIDAD</th>
                  <th>FRECUENCIA</th>";

    $html_foot="<tr>
              <td colspan='2'></td>
              <td colspan='2'>% CUMPLIMIENTO</td>";

    $band_dia=0;
    foreach($daterange as $date){
      $band_dia++;
      //if(date('l', strtotime($date->format("d-m-Y"))) != 'Sunday' || date('l', strtotime($date->format("d-m-Y"))) == 'Sunday'){
        //log_message('error', 'dia de la semana : '.date('l', strtotime($date->format("d-m-Y"))));
        $html_head.="<th><input id='num_dia' type='hidden' value='".$date->format("d")."'>".$date->format("d")."</th>";
        ${'dias_'.intval($date->format("d"))}=array();
        array_push($array,$date->format("d"));
        $html_foot.="<td id='ft_".intval($date->format("d"))."'></td>";
      //}
    }
    $html_foot.="<td></td>
            </tr>";

    $html_head.="<th>ÁREA DE OPORTUNIDAD</th>
              </tr>";
    $cont=0; $cont2=0; $cont3=0; $cont4=0; $cont5=0; 

    /*$where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].')';
    if(isset($array[4])){
      $where='(DATE_FORMAT(ma.fecha, "%d")='.$array[0].' or DATE_FORMAT(ma.fecha, "%d")='.$array[1].' or DATE_FORMAT(ma.fecha, "%d")='.$array[2].' or DATE_FORMAT(ma.fecha, "%d")='.$array[3].' or DATE_FORMAT(ma.fecha, "%d")='.$array[4].')';
    }*/

    $html_foot2=""; 
    $cont_area=0; $cont_area1=0; $cont_area2=0; $cont_area3=0; $cont_area4=0; ${'cont_1'}=0;
    $act=$this->ModelReportes->get_result_monitoreo2($params);
    foreach ($act as $a){
      $fecha_camb = strtotime($a->fecha);
      $semana = date('W', $fecha_camb);
      $i++;
      $cont_exist=0;
      $anio=$a->anio;
      $tmp=explode(", ",$a->dias_act);
      $tmp_seg=explode(", ",$a->seguimiento_act);
      //log_message('error', 'tmp : '.count($tmp));
      $html.="<tr>
              <td><input id='zona' type='hidden' value='".$a->zona."'>".$a->zona."</td>
              <td>".$a->inspeccion."</td>
              <td>".$a->nombre."</td>
              <td>".$a->frecuencia."</td>";
              for($j=0; $j<count($array); $j++){
                if(in_array($array[$j], $tmp)){ //son dias del mes
                  $cont_limpieza=$this->ModelReportes->count_limpiezaz($params,$a->id_limpieza,$array[$j]);
                  //log_message('error', 'el dia existe : '.$array[$j]);
                  //log_message('error', 'cont_exist : '.$cont_exist);
                  if(isset($tmp_seg[$cont_exist])){
                    //log_message('error', 'tmp_seg existe : '.$tmp_seg[$cont_exist]);
                    if($tmp_seg[$cont_exist]=="1"){
                      ${'dias_'.intval($array[$j])}[]=1;
                    }else if($tmp_seg[$cont_exist]=="0"){
                      ${'dias_'.intval($array[$j])}[]=1;
                    }else if($tmp_seg[$cont_exist]=="2"){ //N/A
                      ${'dias_'.intval($array[$j])}[]=1;
                    }
                
                    $html.="<td><input data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='".$tmp_seg[$cont_exist]."'>".$tmp_seg[$cont_exist]."</td>";

                    if($tmp_seg[$cont_exist]=="1"){
                      //$cont++;
                      ${'cont_'.intval($array[$j])}++;
                    }
                    //log_message('error', "cont: ".$cont);
                  }else{
                    ${'dias_'.intval($array[$j])}[]=0;
                    $html.="<td><input data-id='".$a->id."' data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='0'>0</td>";
                  }
                  $cont_exist++;
                }else{
                  //$cont=0;
                  //${'cont_'.intval($array[$j])}=0;
                  ${'dias_'.intval($array[$j])}[]=0;
                  $html.="<td><input data-dia='".intval($array[$j])."' id='val_dia".intval($array[$j])."' type='hidden' value='0'></td>";
                }
                $html_foot="<tr>
                <td colspan='2'></td>
                  <td colspan='2'>% CUMPLIMIENTO</td>
                  ".$html_foot2."
                  <td></td>
                </tr>";
                
                //log_message('error', "dias_array: ".print_r(${'dias_'.intval($array[$j])}));
              }
              //log_message('error', "cont: ".$cont);
              //log_message('error', "limpieza nombre: ".$a->nombre);
              //log_message('error', "tot_act: ".$cont_limpieza->tot_act);
              $html_foot2x='';
              $band_aux=0;
              for($j=0; $j<count($array); $j++){   
                //log_message('error', "dias_array: ".print_r(${'dias_'.intval($array[$j])}));
                $band_aux++;
                //$html_foot2x.='<td>'.array_sum(${'dias_'.intval($array[$j])}).'</td>';
                //$porcentaje=($cont/$a->tot_act)*100;
                if(isset(${'cont_'.intval($array[$j])}) && ${'cont_'.intval($array[$j])}>0){ //son dias del mes
                  //log_message('error', 'el dia existe : '.$array[$j]);
                  $porcentaje=(${'cont_'.intval($array[$j])}/array_sum(${'dias_'.intval($array[$j])}))*100;
                }else{
                  $porcentaje=0;
                }
                $html_foot2x.='<td><input id="porc_'.$band_aux.'" type="hidden" value="'.round($porcentaje,2).'">'.round($porcentaje,2).'</td>';
                //$html_foot2x.='<td>'.round($porcentaje,2).'</td>';
              }
              $html_foot="<tr>
                <td colspan='2'></td>
                  <td colspan='2'>% CUMPLIMIENTO</td>
                  ".$html_foot2x."
                  <td></td>
                </tr>";
        $html.="<td><input id='val_area' type='hidden' value='".round(($cont/count($array)*100),2)."'>".round(($cont/count($array)*100),2)."</td>
              </tr>";
        /*$html.="<td><input id='val_area' type='hidden' value='".round(($cont/$cont_limpieza->tot_act*100),2)."'>".round(($cont/$cont_limpieza->tot_act*100),2)."</td>
                </tr>";*/
        
    }
    echo json_encode(array("html"=>$html,"html_head"=>$html_head,"anio"=>$anio,"html_foot"=>$html_foot,"band_dia"=>$band_dia));
  }

  function insertGraphMonitor(){
    $datax = $this->input->post();
    $id=$datax['id'];
    $img = $datax["grafica"];
    $id_proy=$datax['id_proyecto'];
    $mes=$datax['mes'];
    $tipo=$datax['tipo'];
    $id_act=$datax['id_act'];
    //unset($datax['id_act']);

    $result=$this->ModeloGeneral->getselectwhere_n_consulta('grafica_monitoreo',array("id_proyecto"=>$datax['id_proyecto'],"tipo"=>$datax['tipo'],"mes"=>$datax['mes'],"anio"=>$datax['anio'],"id_act"=>$id_act));

    $cont=0;
    $id_cons=0;
    foreach ($result as $row){
      $cont=1;
      $id_cons=$row->id;
    }
    //$name_file=date("YmdHis").".png";
    $name_file=$id_proy."_".$mes."_".$tipo."_".$id_act.".png";
    $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $img));
    if(file_exists(FCPATH.'pdf_monitoreo/'.$name_file)){
      unlink(FCPATH.'pdf_monitoreo/'.$name_file);
    }    
    file_put_contents(FCPATH.'pdf_monitoreo/'.$name_file, $imagenBinaria);

    //if($data["tipo"]==2){
      $datax["grafica"]=$name_file;
    //}
    if($cont==0){
      $this->ModeloGeneral->tabla_inserta('grafica_monitoreo',$datax);
    }
    else{
      unset($datax["id"]);
      $this->ModeloGeneral->updateCatalogo($datax,'id',$id_cons,'grafica_monitoreo');
    }
  }

  function pdfMonitoreo($id,$mes,$anio){
    $data["id_proy"]=$id;
    $data["mes"]=$mes;
    $data["anio"]=$anio;
    $data["name_mes"] = $this->nameMes($mes);
    $logo_cli=$this->ModeloGeneral->getLogoCli($id);
    $data['logo_cli'] = $logo_cli->foto;
    $data["gra"]=$this->ModelReportes->get_graph_monitor($id,$mes,$anio);
    $data["gra_mes"]=$this->ModelReportes->get_graph_monitorMes($id,$mes,$anio);
    $this->load->view('reportes/pdf_monitoreo',$data);
  }

  /* *************************************************************/

  /* ****************************PRESENTISMO DE ROTACIÓN ***************************************** */
  public function resultadosPresentismo($id,$mes,$anio){
    $params['id_proy']=$id;
    $params['mes']=$mes;
    $params['anio']=$anio;
    $data['id_proy']=$id;
    $data['mes']=$mes;
    $data['anio']=$anio;
    $data["namemes"]=$this->nameMes($mes);
    $data["res"]=$this->ModelReportes->get_data_mes($params);
    foreach ($data["res"] as $r) {
      $data["anio_proy"]=$r->anio;
      //$anio=$r->anio;
    }

    $data["foto"]="";
    $data["det_proy"]=$this->ModelReportes->get_detalle_proy($id,$anio);
    foreach ($data["det_proy"] as $d) {
      $data["foto"]=$d->foto;
    }
    $data["bajas"]=$this->ModelReportes->get_bajas_proy($id,$anio,$mes,0);
    $data["head"]=$this->ModelReportes->get_data_head($id,$anio,$mes);

    /*log_message('error', "mes: ".$mes);
    log_message('error', "anio: ".$anio);*/
    list($primeraSemana,$ultimaSemana)=$this->semanasMes($anio,$mes);
    $data["primeraSemana"]=$primeraSemana;
    $data["ultimaSemana"]=$ultimaSemana;
    $this->load->view('reportes/resultados_presentismo',$data);
    $this->load->view('reportes/resultados_presentismojs');
  }

  function pdfPresentismo($id,$mes,$anio){
    $data["id_proy"]=$id;
    $data["mes"]=$mes;
    $data["anio"]=$anio;
    $data["name_mes"] = $this->nameMes($mes);
    $logo_cli=$this->ModeloGeneral->getLogoCli($id);
    $data['logo_cli'] = $logo_cli->foto;
    $data["gra"]=$this->ModelReportes->get_graph_presentismo($id,$mes,$anio);
    $this->load->view('reportes/pdf_presentismo',$data);
  }

  function semanasMes($year,$month){
    # Obtenemos el ultimo dia del mes
    $ultimoDiaMes=date("t",mktime(0,0,0,$month,1,$year));
    # Obtenemos la semana del primer dia del mes
    $primeraSemana=date("W",mktime(0,0,0,$month,1,$year));
    # Obtenemos la semana del ultimo dia del mes
    $ultimaSemana=date("W",mktime(0,0,0,$month,$ultimoDiaMes,$year));
    //log_message('error', "primeraSemana: ".$primeraSemana);
    //log_message('error', "ultimaSemana: ".$ultimaSemana);
    return array($primeraSemana,$ultimaSemana);
  }

  function insertGraphPresentismo(){
    $datax = $this->input->post();
    $id=$datax['id'];
    $img = $datax["grafica"];
    $id_proy=$datax['id_proyecto'];
    $mes=$datax['mes'];
    $tipo=$datax['tipo'];

    $result=$this->ModeloGeneral->getselectwhere_n_consulta('graficas_presentismo',array("id_proyecto"=>$datax['id_proyecto'],"tipo"=>$datax['tipo'],"mes"=>$datax['mes'],"anio"=>$datax['anio']));

    $cont=0;
    $id_cons=0;
    foreach ($result as $row){
      $cont=1;
      $id_cons=$row->id;
    }
    //$name_file=date("YmdHis").".png";
    $name_file=$id_proy."_".$mes."_".$tipo.".png";
    $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $img));
    if(file_exists(FCPATH.'pdf_presentismo/'.$name_file)){
      unlink(FCPATH.'pdf_presentismo/'.$name_file);
    }    
    file_put_contents(FCPATH.'pdf_presentismo/'.$name_file, $imagenBinaria);

    //if($data["tipo"]==2){
      $datax["grafica"]=$name_file;
    //}
    if($cont==0){
      $this->ModeloGeneral->tabla_inserta('graficas_presentismo',$datax);
    }
    else{
      unset($datax["id"]);
      $this->ModeloGeneral->updateCatalogo($datax,'id',$id_cons,'graficas_presentismo');
    }
  }

  /* ********************************************************************* */

  public function enviarMail(){   
    $id_proy = $this->input->post("id_proy");
    $tipo = $this->input->post("tipo");
    //log_message('error', "tipo: ".$tipo);
    if($tipo==4){
      $tipo_dpto = $this->input->post("tipo_dpto");
    }

    $result=$this->Modeloactividadesrealizadas->proyecto($id_proy);
    foreach ($result->result() as $item) {
      $correo=$item->correo;
    }
    $correo = $this->ModeloGeneral->getCorreosClienteProy($id_proy);
    array_push($correo, $item->correo);

    $mes = $this->input->post("mes");
    $anio = $this->input->post("anio");
    //$gtm = $this->ModeloGeneral->getselectwhererow2('clientes',array('id'=>$id_cli));

    $this->load->library('email');
    $config = array();
    $config['protocol'] = 'smtp';
    
    /*$config['smtp_host'] = 'snfpro.sicoi.net';                     
    $config['smtp_user'] = 'reportes@snfpro.sicoi.net';
    $mail_dom = 'reportes@snfpro.sicoi.net';
    $config['smtp_pass'] = 'Sy)rORl5FaCW';*/

    $config['smtp_host'] = 'mail.snf.app';                     
    $config['smtp_user'] = 'reportes@snf.app';
    $mail_dom = 'reportes@snf.app';
    $config['smtp_pass'] = ';@9A5wR#$02c';

    $config['smtp_port'] = 465;
    $config["smtp_crypto"] = 'ssl';
    //$config['newline']   = "\r\n";
    //$config["crlf"] = "\r\n";
    $config['mailtype'] = "html";
    $config['charset'] = "utf-8";
    $config['wordwrap'] = TRUE;
    $config['validate'] = true;

    //log_message('error', 'correo cliente : '.$gtm->correo);

    $this->email->initialize($config);
    $this->email->from($mail_dom);
    $this->email->to($correo);
    $this->email->subject('Reporte de cumplimiento');
    if($tipo==4){ //actividades_realizadas
      $this->email->attach(FCPATH.'pdf_limpieza/limpieza_ar_'.$id_proy.'_'.$mes.'_'.$anio.'_'.$tipo_dpto.'.pdf');
      $this->email->message("REPORTE DE SALIDA - ACTIVIDADES REALIZADAS");  
    }else if($tipo==1){ //limpieza: ordenes_semanal
      $this->email->attach(FCPATH.'pdf_limpieza/limpieza_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf');
      $this->email->message("REPORTE DE SALIDA - ORDENES, CUMPLIMIENTO DE OTS");
    }else if($tipo==2){ //matriz_acciones
      $this->email->attach(FCPATH.'pdf_matriz_acciones/matriz_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf');
      $this->email->message("REPORTE DE SALIDA - MATRIZ DE ACCIONES");
    }else if($tipo==3){ //Matriz de Versatilidad
      $this->email->attach(FCPATH.'pdf_matriz_versa/matriz_'.$id_proy.'.pdf');
      $this->email->message("REPORTE DE SALIDA - MATRIZ DE VERSATILIDAD");
    }else if($tipo==5){ //monitoreo de actividades
      $this->email->attach(FCPATH.'pdf_monitoreo/monitoreo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf');
      $this->email->message("REPORTE DE SALIDA - MONITOREO DE ACTIVIDADES");
    }else if($tipo==6){ //presentismo 
      $this->email->attach(FCPATH.'pdf_presentismo/presentismo_'.$id_proy.'_'.$mes.'_'.$anio.'.pdf');
      $this->email->message("REPORTE DE SALIDA - PRESENTISMO DE ROTACIÓN");
    }
    $this->email->send();
  }

  function nameMes($m){
    $mes="Enero";
    switch ($m) {
      case 2: $mes="Febrero"; break;
      case 3: $mes="Marzo"; break;
      case 4: $mes="Abril"; break;
      case 5: $mes="Mayo"; break;
      case 6: $mes="Junio"; break;
      case 7: $mes="Julio"; break;
      case 8: $mes="Agosto"; break;
      case 9: $mes="Septiembre"; break;
      case 10: $mes="Octubre"; break;
      case 11: $mes="Noviembre"; break;
      case 12: $mes="Diciembre"; break;
    }
    return $mes;
  }
  function pdfactividades_realizadas($id,$mes,$dpto,$anio){
    $data['id_proy']=$id;
    $data['mes']=$mes;
    $data['dpto']=$dpto;
    $data['anio']=$anio;
    $namemes=$this->nameMes($mes);
    $fechaselect=date('Y').'-'.$mes.'-01';
    $fecha = new DateTime($fechaselect);
    
    $fecha->modify('first day of this month');
    $primerdiadelmes=$fecha->format('Y-m-d');

    $fecha->modify('last day of this month');
    $ultimodiadelmes=$fecha->format('Y-m-d'); 

    $servicio='';
    $result=$this->Modeloactividadesrealizadas->proyecto($id);
    foreach ($result->result() as $item) {
      $servicio=$item->servicio;
    }
    $data['dg_titulo']='Reporte de actividades para plan de renovacion y conservacion de las diferentes areas del departamento duante el mes de '.$namemes.' de '.date('Y');
    $data['de_dep']=$servicio;
    $data['dg_des']='A continuación se presenta un resumen de actividades de Limpieza y pulido de pisos, área de cabinas de pintura.';

    //$det_dep=$this->ModeloGeneral->getselectwhere2('actividades_realizadas_des',array('proyecto'=>$id,'dg_mes'=>$mes,'dg_anio'=>$anio,'activo'=>1));
    $data['fechai']="";
    $data['fechaf']="";

    $det_dep=$this->Modeloactividadesrealizadas->getActividades($id,array('dg_mes'=>$mes,'dg_anio'=>$anio));
    foreach ($det_dep as $item) {
      $data['dg_titulo']=$item->dg_titulo;
      $data['de_dep']=$item->departamento;
      $data['dg_des']=$item->dg_des;
      
    }

    $logo_cli=$this->ModeloGeneral->getLogoCli($id);
    $data['logo_cli'] = $logo_cli->foto;
    if(intval($mes<10)){
      $mes="0".$mes;
    }
    //$data['resultitem']=$this->ModeloGeneral->getselectwhere2('actividades_realizadas',array('idproyecto'=>$id,'departamento'=>$dpto,'activo'=>1,'fecha >='=>$primerdiadelmes,'fecha <='=>$ultimodiadelmes));
    $data['resultitem']=$this->ModeloGeneral->getselectwhere2('actividades_realizadas',array('idproyecto'=>$id,'departamento'=>$dpto,'activo'=>1,'DATE_FORMAT(fecha,"%m")'=>$mes));

    $row_item=$data["resultitem"]->row();
    $data['fechai']=$row_item->fechai;
    $data['fechaf']=$row_item->fechaf;

    $this->load->view('reportes/pdf_actividades_realizadas',$data);
  }

  public function carrusel($id=0)
  {  
    $data['resultdepa']=$this->ModeloGeneral->getselectwhere2('departamentos',array('id_proy'=>$id,'activo'=>1));
    $data['idproyecto']=$id;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('reportes/carrusel',$data);
    $this->load->view('templates/footer');
    $this->load->view('reportes/carruseljs');
  }

  function get_actividad_img(){ 
    $idproyecto=$this->input->post('idproyecto');
    $iddepa=$this->input->post('iddepa');
    $html='<div class="slider">
            <div class="slider__inner">
              ';
    $resultitem=$this->ModeloGeneral->getselectwhere2('actividades_realizadas',array('idproyecto'=>intval($idproyecto),'departamento'=>intval($iddepa),'activo'=>1));
    $resultitemnumrow=$resultitem->num_rows();
    foreach ($resultitem->result() as $item) {
              $imagenes='';
      

          if($item->file1!=''){
              $filename1 = FCPATH.'/uploads/files_ar/'.$item->file1;
              $filename1b = base_url().'uploads/files_ar/'.$item->file1;
              if(file_exists($filename1)){
                  $imagenes.='<div class="slider__item">
                           <div class="slider__item-container">
                            <img src="'.$filename1b.'" class="slider__item-img">
                              <div class="slider__item-datas">
                                <span>'.$item->descripcion.'</span>
                              </div>
                            </div>
                          </div>
                          ';
              }
          }
          if($item->file2!=''){
              $filename2 = FCPATH.'/uploads/files_ar/'.$item->file2;
              $filename2b = base_url().'uploads/files_ar/'.$item->file2;
              if(file_exists($filename2)){
                  //$html.='<img src="'.$filename2b.'" >';
                  $imagenes.='<div class="slider__item">
                           <div class="slider__item-container">
                            <img src="'.$filename2b.'" class="slider__item-img">
                              <div class="slider__item-datas">
                                <span>'.$item->descripcion.'</span>
                              </div>
                            </div>
                          </div>
                          ';
              }
          }
          if($item->file3!=''){
              $filename3 = FCPATH.'/uploads/files_ar/'.$item->file3;
              $filename3b = base_url().'uploads/files_ar/'.$item->file3;
              if(file_exists($filename3)){
                  //$html.='<img src="'.$filename3b.'" >';
                  $imagenes.='<div class="slider__item">
                           <div class="slider__item-container">
                            <img src="'.$filename3b.'" class="slider__item-img">
                              <div class="slider__item-datas">
                                <span>'.$item->descripcion.'</span>
                              </div>
                            </div>
                          </div>
                          ';
              }
          }
          $html.=$imagenes;
          if($resultitemnumrow<2){
            $html.=$imagenes;
          }


      
    }
    $html.='<div class="slider__controls"></div>
            </div>
          </div>';
        //log_message('error',$html);

    echo $html;
  }
   
  function get_actividad_img2()
  { 
    $idproyecto=$this->input->post('idproyecto');
    $iddepa=$this->input->post('iddepa');
    $resultimg=$this->ModeloGeneral->getselectwhere2('actividades_realizadas',array('idproyecto'=>intval($idproyecto),'departamento'=>intval($iddepa),'activo'=>1));
    $html.='';
    $html.='<div align="center">';  
      $cont2=0; 
      foreach ($resultimg->result() as $p){
        $resultimg2=$this->ModeloGeneral->getselectwhere2('actividades_realizadas',array('id'=>$p->id));
        foreach ($resultimg2->result() as $p2){

          if($p2->file1!=''){
            $html.='<a class="btn_color btn_background_naranja btn_'.$cont2.'" onclick="btn_caja('.$cont2.')"></a>';
            $cont2++; 
          }
          if($p2->file2!=''){
            $html.='<a class="btn_color btn_background_naranja btn_'.$cont2.'" onclick="btn_caja('.$cont2.')"></a>';
            $cont2++; 
          }
          if($p2->file3!=''){
            $html.='<a class="btn_color btn_background_naranja btn_'.$cont2.'" onclick="btn_caja('.$cont2.')"></a>';
            $cont2++; 
          }   

        }

      } 
    $html.='</div>';  
    $html.='<br><br>';
    $html.='<div class="img" align="center">';
      $cont=0;          
      foreach ($resultimg->result() as $p){
        $resultimg2=$this->ModeloGeneral->getselectwhere2('actividades_realizadas',array('id'=>$p->id));
        foreach ($resultimg2->result() as $p2){

          if($p2->file1!=''){
            $etiq='';
            if($cont>0){
              $etiq='style="display:none"';
            }else{
              $etiq='';
            }
            $html.='<div class="imgcj img_'.$cont.'" '.$etiq.'"><h3>'.$p2->descripcion.'</h3>
                <img style="width: 550px;" src="'.base_url().'uploads/files_ar/'.$p2->file1.'">
              </div>';
            $cont++; 
          }
          if($p2->file2!=''){
            $etiq='';
            if($cont>0){
              $etiq='style="display:none"';
            }else{
              $etiq='';
            }
            $html.='<div class="imgcj img_'.$cont.'" '.$etiq.'"><h3>'.$p2->descripcion.'</h3>
                <img style="width: 550px;" src="'.base_url().'uploads/files_ar/'.$p2->file2.'">
              </div>';
            $cont++; 
          }
          if($p2->file3!=''){
            $etiq='';
            if($cont>0){
              $etiq='style="display:none"';
            }else{
              $etiq='';
            }
            $html.='<div class="imgcj img_'.$cont.'" '.$etiq.'"><h3>'.$p2->descripcion.'</h3>
                <img style="width: 550px;" src="'.base_url().'uploads/files_ar/'.$p2->file3.'">
              </div>';
            $cont++; 
          }   

        }

      } 
    $html.='</div>';  
    
    echo $html; 
  }



  /*
      <div class="imgcj img_2" style="margin: 30px; display: none;">
        <img src="http://plastimegademo.plastimega.com.mx/public/img/cajas/1/2.png">
      </div>
      <div class="imgcj img_3" style="margin: 30px; display: none;">
        <img src="http://plastimegademo.plastimega.com.mx/public/img/cajas/1/3.png">
      </div>
      <div class="imgcj img_4" style="margin: 30px; display: none;">
        <img src="http://plastimegademo.plastimega.com.mx/public/img/cajas/1/4.png">
      </div>
    </div>
    <br><br>
    <div align="center">
      <a class="btn_color btn_background_naranja" onclick="btn_caja(1)"></a>
      <a class="btn_color btn_background_naranja" onclick="btn_caja(2)"></a>
      <a class="btn_color btn_background_naranja" onclick="btn_caja(3)"></a>
      <a class="btn_color btn_background_naranja" onclick="btn_caja(4)"></a>
    </div>
  */

}

