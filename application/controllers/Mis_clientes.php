<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mis_clientes extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModelClientes');
    $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
  }

  public function index()
  {
    $data['idPadre'] = $this->session->userdata('usuarioid_tz');

    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('mis_clientes/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('mis_clientes/indexjs');
  }

  public function registro($id=0)
  {
    $data['idPadre'] = $this->session->userdata('usuarioid_tz');
    if($id==0){
      $data['tittle']='Alta';
      $data['contactos'] = $this->ModeloGeneral->getselectwhere2("clientes_contactos", array("clienteId" => $id,"estatus" => 1)); 
      $data['id']=0;
      $data['empresa']='';
      $data['contacto']='';
      $data['telefono']='';
      $data['direccion']='';
      $data['num_cliente']='';
      $data['foto']='';
      $data['contrasena']='';
      $data['correo']='';
      $cont_reg=$this->ModeloGeneral->getselectwhere2('clientes_clientes',array('activo'=>1));
      $tot_reg=$cont_reg->num_rows()+1;
      $data['num_cliente']=$tot_reg;
    }else{
      $data['tittle']='Edición'; 
      $data['contactos'] = $this->ModeloGeneral->getselectwhere2("clientes_contactos", array("clienteId" => $id,"estatus" => 1));
      $resul=$this->ModeloGeneral->getselectwhere('clientes_clientes','id',$id);
      foreach ($resul as $x) {
        $data['id']=$x->id;
        $data['empresa']=$x->empresa;
        $data['contacto']=$x->contacto;
        $data['telefono']=$x->telefono;
        $data['direccion']=$x->direccion;
        if($x->num_cliente!=""){
          $data['num_cliente']=$x->num_cliente;
        }
        $data['foto']=$x->foto;
        $data['correo']=$x->correo;
        $data['contrasena']='xxxxxx';
      }
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('mis_clientes/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('mis_clientes/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    unset($datos['id']);
    $pss_verifica = $datos['contrasena'];
    $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
    $datos['contrasena'] = $pass;
    if($pss_verifica == 'xxxxxx'){
       unset($datos['contrasena']);
    }
    unset($datos['contrasena2']);
    $id_reg=0;
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'clientes_clientes');
      $id_reg=$id; 
    }else{
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('clientes_clientes',$datos);
    }  
    echo json_encode($datos);
  }

  public function getlistado(){
    $params = $this->input->post();
    $clienteId = $this->input->post('cliente_id');
    //log_message('error', 'params : ' . $clienteId);
    $getdata = $this->ModelClientes->get_resultM($params, $clienteId);
    $totaldata= $this->ModelClientes->total_resultM($params, $clienteId); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  
  function cargafiles(){
    $id=$this->input->post('id');
    $folder="clientes";
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $newfile='doc_'.$fecha.$nombre_archivo;
    $newfile=str_replace(" ", "", $newfile);   
    $archivador = $upload_folder . '/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('foto'=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'id',$id,'clientes_clientes');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 

  public function delete(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'clientes_clientes');
      echo $resul;
  }

  function validar(){
    $Usuario = $this->input->post('Usuario');
    $result=$this->ModeloGeneral->getselectwhere('clientes_clientes','correo',$Usuario);
    $resultado=0;
    foreach ($result as $row){
        $resultado=1;
    }

    $result2=$this->ModeloGeneral->getselectwhere('usuarios','Usuario',$Usuario);
    foreach ($result2 as $row){
        $resultado=1;
    }

    echo $resultado;
  }

  public function insertUpdate_contactos(){
    $datos = $this->input->post('data');
    $DATA = json_decode($datos);
    
    for ($i=0; $i<count($DATA); $i++) {
        $clienteId = $DATA[$i]->clienteId;
        $contacto = $DATA[$i]->contacto;
        $telefono = $DATA[$i]->telefono;
        $correo = $DATA[$i]->correo;
        $estatus = $DATA[$i]->estatus;

        $arr = array("clienteId"=>$clienteId,"contacto"=>$contacto,"telefono"=>$telefono,"correo"=>$correo);
        if($DATA[$i]->id == 0){
            $result = $this->ModeloGeneral->insertToCatalogoN($arr, "clientes_contactos");
        }else{
            $arr = array("clienteId"=>$clienteId,"contacto"=>$contacto,"telefono"=>$telefono,"correo"=>$correo,"estatus"=>$estatus);
            $result = $this->ModeloGeneral->updateCatalogoN($arr, array('id'=>$DATA[$i]->id), "clientes_contactos");
        }
    }
    echo $datos;
  }

}

