<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Servicios extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModelServicios');
    $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal_tz');
            $this->perfilid=$this->session->userdata('perfilid_tz');
            $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,3);
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
  }

  public function index()
  {
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('servicios/index');
    $this->load->view('templates/footer');
    $this->load->view('servicios/indexjs');
  }

  public function registro($id=0)
  {
    if($id==0){
      $data['tittle']='Alta'; 
      $data['id']=0;
      $data['servicio']='';
      $data['descripcion']='';
    }else{
      $data['tittle']='Edición'; 
      $resul=$this->ModeloGeneral->getselectwhere('servicios','id',$id);
      foreach ($resul as $x) {
        $data['id']=$x->id;
        $data['servicio']=$x->servicio;
        $data['descripcion']=$x->descripcion;
      }
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('servicios/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('servicios/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['id'];
    if($id>0){
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'servicios'); 
    }else{
      $datos['personalId']=$this->idpersonal;
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('servicios',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelServicios->get_result($params);
    $totaldata= $this->ModelServicios->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function delete(){
      $id = $this->input->post('id');
      $data = array('activo' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'servicios');
      echo $resul;
  }
  
}

