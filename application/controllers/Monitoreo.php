<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Monitoreo extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->library('excel');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloProyectos');
    if(!$this->session->userdata('logeado')) {
      redirect('/Login');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {

  }

  public function listado($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli;
    $data["act"]=$this->ModeloProyectos->getActividadesMonitoreo($id,$id_cli,1);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('monitoreo/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('monitoreo/indexjs');
  }

  public function registro($id=0,$id_proy,$id_cli=0)
  {
    $data['id_proy']=$id_proy;
    $data['id_cli']=$id_cli;
    $data['tittle']='Registro de'; 
    if($id>0){
      $data['tittle']='Edición de'; 
      $data["a"]=$this->ModeloGeneral->getselectwhererow2("monitoreo_actividad",array("id"=>$id));
      $data["dia_activ"]=date("d", strtotime($data["a"]->fecha));
      $data["dia_activ_name"]=date('l', strtotime($data["a"]->fecha));
      log_message('error', 'dia_activ_name : '.$data["dia_activ_name"]);
    }   
    $data["act"]=$this->ModeloProyectos->getActividadesMonitoreo($id_proy,$id_cli,1);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('monitoreo/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('monitoreo/formjs');
  }

  public function getDataTable(){
    $params = $this->input->post();
    $getdata = $this->ModeloProyectos->getDataMonitoreo($params);
    $totaldata= $this->ModeloProyectos->getDataMonitoreo_total($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function submit2(){ //version sobre version actulizada 2.2
    $datos = $this->input->post();
    $id=$datos['id'];
    $num_inspeccion=$datos['num_inspeccion'];
    $frecuencia=$datos['frecuencia'];
    $band=$datos['band'];
    unset($datos["band"]);
    $fecha=$datos["fecha"];
    $fecha_fin=$datos["fecha_fin"];
    unset($datos["fecha_fin"]);
    //log_message('error', 'fecha : '.$fecha);
    $dia_activ=$datos['dia_activ'];
    unset($datos["dia_activ"]);
   
    $fecha2=date("Y-m-d", strtotime($fecha));
    //log_message('error', 'fecha2 : '.$fecha2);
    $mes=date("m", strtotime($fecha2));
    $anio=date("Y", strtotime($fecha2));
    $datos["mes"]=$mes;
    $datos["fecha"]=date("Y-m-d", strtotime($fecha));
    //log_message('error', 'fecha de datos : '.$datos["fecha"]);
    if($band==0){
      if($id>0){
        unset($datos["id_proyecto"]); unset($datos["id_cliente"]);
        $this->ModeloGeneral->updateCatalogo($datos,'id',$id,"monitoreo_actividad");
        $id_reg=$id; 
      }else{
        $datos['id_user_reg']=$this->idpersonal;
        $datos['reg']=$this->fechahoy;  
        $id_reg=$this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$datos);
      } 
    } 
    else if($band==1){
      unset($datos["num_inspeccion"]);
      $datos['id_user_reg']=$this->idpersonal;
      $datos['reg']=$this->fechahoy; 
      $datos['num_inspeccion']=1;
      $fechaini=$datos["fecha"];
      $datetime1 = date_create($datos["fecha"]);
      //$datetime2 = date_create($datos["fecha"]);
      $fecha = new DateTime(date('Y-m-d', strtotime($datos["fecha"])));
      $fecha->modify('last day of this month');
      //echo $fecha->format('d');
      //log_message('error', $fecha->format('d')); 
      $dia_fin=$fecha->format('d')-1;
      $fff = ($datos["fecha"]."- ".$dia_fin." days");
      $datetime2 = date_create($fff);

      $contador = date_diff($datetime1, $datetime2);
      $differenceFormat = '%a';
      $dif_meses=$contador->format("%m");

      if($datos["frecuencia_tipo"]=="1"){ //semanal
        $num_sem_perio=floor(($contador->format('%a') / 7));
        $val_por_frec=1;
      }else if($datos["frecuencia_tipo"]=="2"){ //Quincenal
        $num_sem_perio=floor(($contador->format('%a') / 14));
        $val_por_frec=2;
      }
      else if($datos["frecuencia_tipo"]=="3"){ //Mensual
        if($mes==2) //mes del periodo inicial
          $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes));
        else if($mes_fin==2) //mes del periodo final
          $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes_fin));
        else if($mes!=2 && $mes_fin!=2) //mes del periodo final
          $num_sem_perio=$dif_meses;
          $val_por_frec=$dif_meses;
      }
      if($datos["tipo"]==1){
        if($datos["frecuencia_tipo"]=="1"){ //semanal
          $num_sem_perio=$frecuencia;
        }else if($datos["frecuencia_tipo"]=="2"){ //Quincenal
          $num_sem_perio=2;
        }
        else if($datos["frecuencia_tipo"]=="3"){ //Mensual
          if($mes==2) //mes del periodo inicial
            $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes));
          else if($mes_fin==2) //mes del periodo final
            $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes_fin));
          else if($mes!=2 && $mes_fin!=2) //mes del periodo final
            $num_sem_perio=$dif_meses;
        }
      }
      //log_message('error', "ese parametro tiene # semanas total: ".$num_sem_perio);

      $number = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
      //$fecha_i = ''.$mes.'/01/'.$anio.'';
      //$ = ''.$mes.'/'.$number.'/'.$anio.'';
      $fecha_i=date('Y-m-d', strtotime($datos["fecha"]));
      $fecha_f=date('Y-m-d', strtotime($fecha_fin));

      //log_message('error', 'fecha_i : '.$fecha_i);
      //log_message('error', 'fecha_f : '.$fecha_f);
      $begin = new DateTime($fecha_i);
      $end = new DateTime($fecha_f);
      $end = $end->modify( '+1 day' );
      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval ,$end);
      //log_message('error', 'num_sem_perio : '.$num_sem_perio);
      //log_message('error', 'frecuencia : '.$frecuencia);

      //$fechaInicio = $anio.'-'.$mes.'-01';
      //$fechaFin = $anio.'-'.$mes.'-'.$number;
      $fechaInicio=$fecha_i;
      $fechaFin=$fecha_f;
      log_message('error', 'fechaInicio : '.$fechaInicio);
      log_message('error', 'fechaFin : '.$fechaFin);
      $fechaInicio=strtotime($fechaInicio);
      $fechaFin=strtotime($fechaFin);

      $frec_por=$frecuencia*count($dia_activ);
      $sem_ant=0; $r=0; $cont_days=0;
      for($i=0; $i<$num_sem_perio; $i++){ //ya solo crea las inspecciones restantes
        /*foreach($daterange as $date){
          $fecha_mod2=date("Y-m-d",$fecha_mod);
          $fecha_nva = new DateTime($fecha_mod2);
          $array_orden["semana"] = $fecha_nva->format('W');
          if(date('l', strtotime($date->format("d-m-Y"))) != 'Sunday' && $datos["tipo"]==0){ //diario
            $datos["fecha"]=$date->format("Y-m-d");
            //log_message('error', 'fecha de diario : '.$date->format("Y-m-d"));
            //$id_reg=$this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$datos);
          }else if(date('l', strtotime($date->format("d-m-Y"))) == 'Sunday' && $datos["tipo"]==1){ //semanal
            $datos["fecha"]=$date->format("Y-m-d");
            //log_message('error', 'fecha de semanal : '.$date->format("Y-m-d"));
            unset($datos["id"]);
            //$id_reg=$this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$datos);
          }
        }*/
        $r++;
        if($datos["tipo"]==0){ //diario
          $j_aux=0; $j_aux2=0; $j_ant=0;
          for($j=0; $j<$frecuencia; $j++){
            if($datos["frecuencia_tipo"]=="1"){//semanal
              //$fecha_mod= strtotime($fechaini."+ ".$i." week");
              $fecha_mod= strtotime($fechaini."+ ".$j_aux." days");
              //$fecha_mod= strtotime($fechaini."+ ".$i." week");
              if($j_aux==0 && $i>0){
                $j_aux=$j_aux+1;
                $fecha_mod= strtotime($fechaini."+ ".$j_aux." days");
              }
            }else if($datos["frecuencia_tipo"]=="2"){//quincenal
              $fecha_mod= strtotime($fechaini."+ ".($i*2)." week+ ".($j)." days");
            }else if($datos["frecuencia_tipo"]=="3"){//Mensual
              $fecha_mod= strtotime($fechaini."+ ".($i)." month");
            }
            if(date('l', strtotime(date("d-m-Y",$fecha_mod)))=='Sunday'){ //domingo no se cuenta acá
              $j_aux2=$j_aux++;
              //log_message('error', 'j_aux2 de domingo : '.$j_aux2);
              $fecha_mod= strtotime(date("d-m-Y",$fecha_mod)."+ ".$j_aux2." days");
            }
            //log_message('error', 'sem_ant : '.$sem_ant);
            //log_message('error', 'i : '.$i);
            //log_message('error', 'frecuencia : '.$frecuencia);

            if($i%$frecuencia || $sem_ant==$i){
              if(date('l', strtotime(date("d-m-Y",$fecha_mod)))=='Saturday' && $sem_ant!=$i){
                $j_aux3=$j_aux2+2;
                $fecha_mod= strtotime(date("d-m-Y",$fecha_mod)."+ ".$j_aux3." days");
                //$j_aux2=$j_aux3++;
              }
              $fecha_mod= strtotime(date("d-m-Y",$fecha_mod)."+ ".$i." week");
            }
      
            $fecha_mod2=date("Y-m-d",$fecha_mod);
            //log_message('error', 'fecha_mod2 de diario : '.$fecha_mod2);
            $datos["fecha"]=$fecha_mod2;
            /*$mes=date("m", strtotime($fecha_mod2));
            $datos["mes"]=$mes;*/
            log_message('error', 'fecha_mod2 antes del for dias : '.$fecha_mod2);
            log_message('error', 'val_por_frec antes del for dias : '.$val_por_frec);
          
            for($ds=0;$ds<count($dia_activ);$ds++){ 
              if($ds==0){
                $val_por_frec_aux=$val_por_frec;
              }else{
                $val_por_frec_aux=$val_por_frec;
              }
              log_message('error', 'val_por_frec_aux: '.$val_por_frec_aux);
              for($da=$fechaInicio; $da<=$fechaFin; $da+=86400 * $val_por_frec_aux){
                //log_message('error', 'Date del for: '.date("Y-m-d", strtotime('monday this week', $da)));
                $dia = date('N', $da);
                if($dia==$dia_activ[$ds]){
                  log_message('error', 'Dia del for por dia select: '.date ("Y-m-d", $da));
                }
                //log_message('error', 'ds: '.$ds);
              }
              $cont_days++;
              log_message('error', 'frec_por: '.$frec_por);
              log_message('error', 'cont_days: '.$cont_days);
              if(count($dia_activ)==intval($cont_days)){
                break;
              }
            }

            if(date('l', strtotime(date("d-m-Y",$fecha_mod)))!='Sunday'){
              //$id_reg=$this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$datos);
            }
            $j_aux++;
            $j_ant=$j_aux;
          }
          $sem_ant++;
        }
        if(intval($frec_por)==intval($cont_days)){
          break;
        }
      }//for de num_sem
      $jj_aux=0;
      if($datos["tipo"]==1){
        for($j=0; $j<$frecuencia; $j++){
          $j2=$j; if($j==0){ $j2=1; }
          if($datos["frecuencia_tipo"]=="1"){//semanal
            $fecha_mod= strtotime($fechaini."+ ".$j2." week");
          }else if($datos["frecuencia_tipo"]=="2"){//quincenal
            $fecha_mod= strtotime($fechaini."+ ".($j2*2)." week");
          }else if($datos["frecuencia_tipo"]=="3"){//Mensual
            $fecha_mod= strtotime($fechaini."+ 1 week");
          }
          $fecha_mod2=date("Y-m-d", ($fecha_mod));
          //log_message('error', 'fecha_mod2 de semanal : '.$fecha_mod2);
          
          $conts=0;
          foreach($daterange as $date){
            if(date('l', strtotime($date->format("d-m-Y"))) == 'Sunday'){
              $conts++;
              $jj_aux++;
              $datos["fecha"]=$date->format("Y-m-d");
              if($datos["frecuencia_tipo"]=="2" && $jj_aux==$frecuencia){//quincenal
                $fecha2=$date->format("d-m-Y");
                $fecha3=date("d-m-Y",strtotime($fecha2."+ 1 week"));
                $datos["fecha"]=date("Y-m-d", strtotime($fecha3));;
                $jj_aux=0;
              }
              $mes=date("m", strtotime($date->format("Y-m-d")));
              $datos["mes"]=$mes;
              log_message('error', 'fecha_mod de semanal : '.$datos["fecha"]);
              //$datos["fecha"]=$fecha_mod2;
              //$id_reg=$this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$datos);
              if($conts==$frecuencia){
                break;
              }
            }
          }
        }
        /*for($j=0; $j<$frecuencia; $j++){
          if($datos["frecuencia_tipo"]=="1"){//semanal
            $fecha_mod= strtotime($fechaini."+ ".$i." week");
          }else if($datos["frecuencia_tipo"]=="2"){//quincenal
            $fecha_mod= strtotime($fechaini."+ ".($i*2)." week");
          }else if($datos["frecuencia_tipo"]=="3"){//Mensual
            $fecha_mod= strtotime($fechaini."+ 1 week");
          }
          //log_message('error', 'fecha de semanal : '.$date->format("Y-m-d"));
          //$id_reg=$this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$datos);
        }*/
      }//tipo 1
    }
    echo $id_reg;
  }

  public function submit(){ //ultima version con dias seleccionables - union de tipos 
    $datos = $this->input->post();
    $id=$datos['id'];
    $num_inspeccion=$datos['num_inspeccion'];
    $frecuencia=$datos['frecuencia'];
    $band=$datos['band'];
    unset($datos["band"]);
    $fecha=$datos["fecha"];
    $fecha_fin=$datos["fecha_fin"];
    unset($datos["fecha_fin"]);
    //log_message('error', 'fecha : '.$fecha);
    $dia_activ=$datos['dia_activ'];
    unset($datos["dia_activ"]);
    //$fecha_insert = $fecha;
   
    $fecha2=date("Y-m-d", strtotime($fecha));
    //log_message('error', 'fecha2 : '.$fecha2);
    $mes=date("m", strtotime($fecha2));
    $mes_fin=date("m", strtotime($fecha_fin));
    $anio=date("Y", strtotime($fecha2));
    $datos["mes"]=$mes;
    $datos["fecha"]=date("Y-m-d", strtotime($fecha));
    //log_message('error', 'fecha de datos : '.$datos["fecha"]);
    if($band==0){
      if($id>0){
        unset($datos["id_proyecto"]); unset($datos["id_cliente"]);
        $this->ModeloGeneral->updateCatalogo($datos,'id',$id,"monitoreo_actividad");
        $id_reg=$id; 
      }else{
        $datos['id_user_reg']=$this->idpersonal;
        $datos['reg']=$this->fechahoy;  
        $id_reg=$this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$datos);
      } 
    } 
    else if($band==1){
      unset($datos["num_inspeccion"]);
      $datos['id_user_reg']=$this->idpersonal;
      $datos['reg']=$this->fechahoy; 
      $datos['num_inspeccion']=1;
      $fechaini=$datos["fecha"];
      $datetime1 = date_create($datos["fecha"]);
      //$datetime2 = date_create($datos["fecha"]);
      $fecha = new DateTime(date('Y-m-d', strtotime($datos["fecha"])));
      $fecha->modify('last day of this month');
      //echo $fecha->format('d');
      //log_message('error', $fecha->format('d')); 
      $dia_fin=$fecha->format('d')-1;
      $fff = ($datos["fecha"]."- ".$dia_fin." days");
      $datetime2 = date_create($fff);

      $contador = date_diff($datetime1, $datetime2);
      $differenceFormat = '%a';
      $dif_meses=$contador->format("%m");

      if($datos["frecuencia_tipo"]=="1"){ //semanal
        $num_sem_perio=floor(($contador->format('%a') / 7));
        $val_por_frec=1;
      }else if($datos["frecuencia_tipo"]=="2"){ //Quincenal
        $num_sem_perio=floor(($contador->format('%a') / 14));
        $val_por_frec=2;
      }
      else if($datos["frecuencia_tipo"]=="3"){ //Mensual
        if($mes==2) //mes del periodo inicial
          $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes));
        else if($mes_fin==2) //mes del periodo final
          $num_sem_perio=floor(($contador->format('%a') / $dias_total_mes_fin));
        else if($mes!=2 && $mes_fin!=2) //mes del periodo final
          $num_sem_perio=$dif_meses;
          $val_por_frec=$dif_meses;
      }

      $fecha_i=date('Y-m-d', strtotime($datos["fecha"]));
      $fecha_f=date('Y-m-d', strtotime($fecha_fin));
      $fechaInicio=$fecha_i;
      $fechaFin=$fecha_f;
      //log_message('error', 'fechaInicio : '.$fechaInicio);
      //log_message('error', 'fechaFin : '.$fechaFin);
      $fechaInicio=strtotime($fechaInicio);
      $fechaFin=strtotime($fechaFin);

      $frec_por=$frecuencia*count($dia_activ);
      $sem_ant=0; $r=0; $cont_days=0;
      for($i=0; $i<$num_sem_perio; $i++){ //ya solo crea las inspecciones restantes
        $r++;
        $j_aux=0; $j_aux2=0; $j_ant=0;
        for($j=0; $j<$frecuencia; $j++){
          for($ds=0;$ds<count($dia_activ);$ds++){ 
            if($ds==0){
              $val_por_frec_aux=$val_por_frec;
            }else{
              $val_por_frec_aux=$val_por_frec;
            }
            log_message('error', 'val_por_frec_aux: '.$val_por_frec_aux);
            for($da=$fechaInicio; $da<=$fechaFin; $da+=86400 * $val_por_frec_aux){
              //log_message('error', 'Date del for: '.date("Y-m-d", strtotime('monday this week', $da)));
              $dia = date('N', $da);
              if($dia==$dia_activ[$ds]){
                log_message('error', 'Dia del for por dia select: '.date ("Y-m-d", $da));
                $fecha_insert = date ("Y-m-d", $da);
                if(date('l', strtotime($fecha_insert))!='Sunday'){
                  $datos["tipo"]=0;
                }else{
                  $datos["tipo"]=1;
                }
                $datos["fecha"]=date ("Y-m-d", $da);
                $mes=date ("m", $da);
                $datos["mes"]=$mes;
                //log_message('error', 'Dia a insertar: '.$fecha_insert);
                //log_message('error', 'tipo: '.$datos["tipo"]);
                $id_reg=$this->ModeloGeneral->tabla_inserta("monitoreo_actividad",$datos);
              }
              //log_message('error', 'ds: '.$ds);
            }
            $cont_days++;
            //log_message('error', 'frec_por: '.$frec_por);
            //log_message('error', 'cont_days: '.$cont_days);
            if(count($dia_activ)==intval($cont_days)){
              break;
            }
          }
          
          $j_aux++;
          $j_ant=$j_aux;
        }
        $sem_ant++;
        if(intval($frec_por)==intval($cont_days)){
          break;
        }
      }//for de num_sem
    }
    echo $id_reg;
  }

  public function delete(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"monitoreo_actividad");
    echo $resul;
  }

  public function cambiaSeguimiento(){
    $id = $this->input->post('id');
    $seguimiento = $this->input->post('estatus');
    $observaciones = $this->input->post('observaciones');
    $inspeccion = $this->input->post('inspeccion');
    $data = array('seguimiento' => $seguimiento,'observaciones' => $observaciones,'inspeccion' => $inspeccion);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"monitoreo_actividad");
    echo $resul;
  }

  function excel_monitoreo($id,$tipo,$id_act)
  {
    $this->load->library('image_lib');
    $data['get_result'] = $this->ModeloProyectos->get_monitoreo($id,$tipo,$id_act);
    $this->load->view('monitoreo/excel_monitoreo',$data);
  }

  public function cargafiles(){
    $id=$this->input->post('id');
    $name=$this->input->post('name');
    $upload_folder ='uploads/monitoreo';
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $newfile=$fecha.$nombre_archivo;
    $newfile=str_replace(" ", "", $newfile);     
    $archivador = $upload_folder.'/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array($name=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'id',$id,'monitoreo_actividad');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 

  public function deleteFoto(){
    $id = $this->input->post('id');
    $foto = $this->input->post('foto');
    //log_message('error', 'foto : '.$foto);
    $data = array('foto'=>"");
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"monitoreo_actividad");
    unlink(FCPATH."uploads/monitoreo/".$foto);
    echo $resul;
  }

  public function cambiaFiltro()
  {
    $tipo = $this->input->post('tipo');
    $id_limpieza = $this->input->post('id_limpieza');

    $this->session->set_userdata('tipo_reg_monitor',$tipo);
    $this->session->set_userdata('activ_monitor',$id_limpieza);
  }

}
