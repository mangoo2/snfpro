<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pagina extends CI_Controller
{
  function __construct(){
      parent::__construct();
      $this->load->model('ModeloSession');
      $this->load->helper('url');
  }

  public function index()
  {
    $this->load->view('pagina/index');
    $this->load->view('login/pages-logintpl');
  }
}
