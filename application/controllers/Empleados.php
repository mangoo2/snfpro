<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Empleados extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloSession');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModelEmpleado');
    $this->idpersonal=$this->session->userdata('idpersonal');
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fecha_reciente = date('Y-m-d');
    if ($this->session->userdata('logeado')){
      $this->idpersonal=$this->session->userdata('idpersonal_tz');
      $this->cuenta=$this->session->userdata('cuenta');
      $this->perfilid=$this->session->userdata('perfilid_tz');
      $permiso=$this->ModeloSession->getviewpermiso($this->perfilid,1);
      if ($permiso==0) {
        redirect('Login');
      }
    }else{
      redirect('/Login');
    }
  }

  public function index()
  { 
    $data["s"]=$this->cuenta;
    $data['cli']=$this->ModeloGeneral->getselectwhere('clientes','activo',1);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('empleados/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('empleados/indexjs');
  }

  public function registro($id=0)
  {
    if($id==0){
      $data['tittle']='Alta'; 
      $data['personalId']=0;
      $data['nombre']='';
      $data['celular']='';
      $data['correo']='';
      $data['genero']='';
      $data['foto']='';
      $data['puesto']='';
      $data['empresa']='';
      $data['perfilId']='';
      $data['UsuarioID']=0;
      $data['Usuario']='';
      $data['contrasena']='';
      $data['check_acceso']='';
      $data['firma']='';
    }else{
      $data['tittle']='Edición'; 
      $resul=$this->ModeloGeneral->getselectwhere('personal','personalId',$id);
      foreach ($resul as $x) {
        $data['personalId']=$x->personalId;
        $data['nombre']=$x->nombre;
        $data['celular']=$x->celular;
        $data['correo']=$x->correo;
        $data['genero']=$x->genero;
        $data['foto']=$x->foto;
        $data['puesto']=$x->puesto;
        $data['empresa']=$x->empresa;
        $data['perfilId']=$x->perfilId;
        $data['check_acceso']=$x->check_acceso;
        $data['firma']=$x->firma;
      }
      $resulusuario=$this->ModeloGeneral->getselectwhere('usuarios','personalId',$id);
      foreach ($resulusuario as $x){
        $data['UsuarioID']=$x->UsuarioID;
        $data['Usuario']=$x->Usuario;
        $data['contrasena']='xxxxxx';
        
        //$data['perfilId']=$x->perfilId;
      }
    }
    $data['resultperfil']=$this->ModeloGeneral->getselectwhere('perfiles','estatus',1);
    $data['cli']=$this->ModeloGeneral->getselectwhere('clientes','activo',1);
    $data["puestos"]=$this->ModeloGeneral->getselectwhere('puestos','estatus',1);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('empleados/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('empleados/formjs');
  }

  public function guardar(){
    $datos = $this->input->post();
    $id=$datos['personalId'];
    $id_reg=0;
    if(isset($datos['check_acceso'])){
      $datos['check_acceso']='on';
    }else{
      $datos['check_acceso']='';
    }
    unset($datos['Usuario']);
    unset($datos['contrasena']);
    unset($datos['contrasena2']);

    $this->session->set_userdata('cuenta',$datos["empresa"]);
    if($id>0){
      $datos['personalId_reg']=$this->idpersonal;
      $this->ModeloGeneral->updateCatalogo($datos,'personalId',$id,'personal');
      $id_reg=$id; 
    }else{
      $datos['personalId_reg']=$this->idpersonal;
      $datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('personal',$datos);
    }  
    echo $id_reg;
  }

  public function getlistado(){
    $params = $this->input->post();
    $getdata = $this->ModelEmpleado->get_result($params);
    $totaldata= $this->ModelEmpleado->total_result($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }
  
  function cargafiles(){
    $id=$this->input->post('id');
    $folder="empleados";
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $newfile='doc_'.$fecha.$nombre_archivo;
    $newfile=str_replace(" ", "", $newfile);
    $archivador = $upload_folder.'/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('foto'=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'personalId',$id,'personal');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 

  public function delete(){
      $id = $this->input->post('id');
      $data = array('estatus' => 0);
      $resul = $this->ModeloGeneral->updateCatalogo($data,'personalId',$id,'personal');
      echo $resul;
  }

  function validar(){
    $Usuario = $this->input->post('Usuario');
    $result=$this->ModeloGeneral->getselectwhere('usuarios','Usuario',$Usuario);
    $resultado=0;
    foreach ($result as $row) {
        $resultado=1;
    }

    $result2=$this->ModeloGeneral->getselectwhere('clientes','correo',$Usuario);
    foreach ($result2 as $row2){
        $resultado=1;
    }
    echo $resultado;
  }

  public function add_user(){
    $id = $this->input->post('UsuarioID');
    $datos['personalId'] = $this->input->post('personalId');
    $datos['perfilId'] = $this->input->post('perfilId');
    $datos['Usuario'] = $this->input->post('Usuario');
    $datos['contrasena'] = $this->input->post('contrasena');
    $datos['contrasena2'] = $this->input->post('contrasena2');

    $pss_verifica = $datos['contrasena'];
    $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
    $datos['contrasena'] = $pass;
    if($pss_verifica == 'xxxxxx'){
       unset($datos['contrasena']);
    }
    unset($datos['UsuarioID']);
    unset($datos['contrasena2']);
    if ($id>0) {
        $this->ModeloGeneral->updateCatalogo($datos,'UsuarioID',$id,'usuarios');
    }else{
        $id=$this->ModeloGeneral->tabla_inserta('usuarios',$datos);
    }   
    echo $id;
  }

  function cargafiles_firma(){
    $id=$this->input->post('id');
    $folder="firma";
    $upload_folder ='uploads/'.$folder;
    $nombre_archivo = $_FILES['foto']['name'];
    $tipo_archivo = $_FILES['foto']['type'];
    $tamano_archivo = $_FILES['foto']['size'];
    $tmp_archivo = $_FILES['foto']['tmp_name'];
    $fecha=date('ymd-His');
    $newfile='doc_'.$fecha.$nombre_archivo;
    $newfile=str_replace(" ", "", $newfile);        
    $archivador = $upload_folder.'/'.$newfile;
    if (!move_uploaded_file($tmp_archivo, $archivador)) {
      $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
    }else{
      $array = array('firma'=>$newfile);
      $this->ModeloGeneral->updateCatalogo($array,'personalId',$id,'personal');
      $return = Array('ok'=>TRUE);
    }
    echo json_encode($return);
  } 

  public function getPuestos(){
    $html="";
    $ps=$this->ModeloGeneral->getselectwhere('puestos','estatus',1);
    foreach ($ps as $p) {
      $html.='<tr class="tr_puesto'.$p->id.'"><td>'.$p->puesto.'</td>
                <td><button type="button" onclick="editPuesto('.$p->id.',\''.$p->puesto.'\')"><i class="fa fa-edit" style="font-size: 20px;"></i></button>
                  <button type="button" onclick="delete_data('.$p->id.')"><i class="fa fa-trash-o" style="font-size: 20px;"></i></button></td>
              </tr>';
    }
    echo $html;
  }

  public function deletePuesto(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'puestos');
    echo $resul;
  }

  public function getPuestosSelect()
  {
    $html="";
    $ps=$this->ModeloGeneral->getselectwhere('puestos','estatus',1);
    foreach ($ps as $p) {
      $html.='<option value="'.$p->id.'">'.$p->puesto.'</option>';
    }
    echo $html;
  }

  public function savePuestos(){
    $datos = $this->input->post();
    $id=$datos['id'];
    if($id>0){
      //$datos['personalId_reg']=$this->idpersonal;
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,'puestos');
      $id_reg=$id; 
    }else{
      //$datos['personalId_reg']=$this->idpersonal;
      //$datos['reg']=$this->fechahoy;
      $id_reg=$this->ModeloGeneral->tabla_inserta('puestos',$datos);
    }  
    echo $id_reg;
  }

}

