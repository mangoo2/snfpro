<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ordenes extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloCatalogos');
    $this->load->model('ModeloProyectos');
    if(!$this->session->userdata('logeado')) {
      redirect('/Login');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $fecha = strtotime($this->fechahoy);
    $this->semana = date('W', $fecha);
  }

  public function index()
  {

  }

  public function listado($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli;
    $fecha = strtotime($this->fechahoy);
    $semana = date('W', $fecha);
    $data["semana"]=$semana;
    $data["act"]=$this->ModeloProyectos->getActividadesMonitoreo($id,$id_cli);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('ordenes/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('ordenes/indexjs');
  }

  public function registro($id=0,$id_proy,$id_cli=0)
  {
    $data['id_proy']=$id_proy;
    $data['id_cli']=$id_cli;
    $data['tittle']='Registro de'; 
    $data["semana"]=$this->semana;
    $data["emp"]=$this->ModeloProyectos->getProyectoEmpleados($id_proy,0);
    $data["sup"]=$this->ModeloProyectos->getProyectoEmpleados($id_proy,array("tipo_empleado"=>2));
    //$data["act"]=$this->ModeloGeneral->getselectwhere_n_consulta("limpieza_actividades",array("id_proyecto"=>$id_proy,"estatus"=>1));
    $data["act"]=$this->ModeloProyectos->getActividadesLimpieza($id_proy);
    $data["cli"]=$this->ModeloGeneral->getselectwhererow2("clientes",array("id"=>$id_cli));
    $data["html"]="";
    $data["act"]=$this->ModeloProyectos->getActividadesMonitoreo($id,$id_cli);

    if($id>0){
      $data['tittle']='Edición de'; 
      $data["a"]=$this->ModeloGeneral->getselectwhererow2("ordenes_semanal",array("id"=>$id));
      $cont=0;
      $result=$this->ModeloProyectos->getOrdenesResp($id);
      foreach ($result as $x) {
        $cont++;
        $btn='<button onclick="eliminaEmple('.$x->id.',1)" class="btn btn-danger" type="button" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
        $id_row="idrow_emp_".$x->id."";
        $data["html"].='<tr id="'.$id_row.'">
            <td>'.$cont.'<input id="id" type="hidden" value="'.$x->id.'" data-id_emp="'.$x->id_empleado.'"></td>
            <td><input id="id_emp" type="hidden" value="'.$x->id_empleado.'">'.$x->nombre.'</td>
            <td>'.$btn.'</td>
        </tr>'; 
      }
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('ordenes/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('ordenes/formjs');
  }

  public function getDataTable(){
    $params = $this->input->post();
    $getdata = $this->ModeloProyectos->getDataOrdenes($params);
    $totaldata= $this->ModeloProyectos->getDataOrdenes_total($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function submit(){
    $datos = $this->input->post();
    $id=$datos['id'];
    $tabla=$datos['tabla'];
    unset($datos["tabla"]);

    if(date('l', strtotime($datos["fecha"])) == 'Sunday'){ //para validar el dia de la semana y saber tipo
      $datos["tipo"]=1;
    }else{
      $datos["tipo"]=2;
    }
    if($datos["seguimiento"]=="0"){
      $datos["seguimiento"]=2;
    }
    //log_message('error', 'tipo: '.$datos["tipo"]);
    if($tabla=="ordenes_diario"){
      $datos["fecha"]=date("Y-m-d", strtotime($datos["fecha"]));
    }
    if($id>0){
      unset($datos["id_proyecto"]); unset($datos["id_cliente"]);
      $fecha = new DateTime($datos["fecha"]);
      $datos["semana"] = $fecha->format('W');
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,$tabla);
      $id_reg=$id; 
    }else{
      $datos['id_user_reg']=$this->idpersonal;
      $datos['reg']=$this->fechahoy;
      $fecha = new DateTime($datos["fecha"]);
      $datos["semana"] = $fecha->format('W');
      $id_reg=$this->ModeloGeneral->tabla_inserta($tabla,$datos);
    }  
    echo $id_reg;
  }

  public function delete(){
    $id = $this->input->post('id');
    $tabla = $this->input->post('tabla');
    if($tabla=="ordenes_semanal"){
      $data = array('estatus' => 0);
    }else{
      $data = array('activo' => 0);
    }
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,$tabla);
    echo $resul;
  }

  public function cambiaSeguimiento(){
    $id = $this->input->post('id');
    $estatus = $this->input->post('estatus');
    $data = array('seguimiento' => $estatus);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"ordenes_semanal");
    echo $resul;
  }

  public function guardarFirma(){
    $datax = $this->input->post();
    $firma = $datax['firma'];
    $id = $datax['id'];
    $id_proyecto = $datax['id_proyecto'];
    $id_limpieza = $datax['id_limpieza'];
    $tipo = $datax['tipo'];
    $tipo_act = $datax['tipo_act'];
    
    if($tipo==1){
      $col="firma";
      $namefile='firma_proy_'.$id_proyecto.'_'.$id_limpieza.'_'.$tipo_act.'.txt';
    }else if($tipo==11){
      $col="firma2";
      $namefile='firma_proy_2'.$id_proyecto.'_'.$id_limpieza.'_'.$tipo_act.'.txt';
    }
    else if($tipo==2){
      $col="firma_cliente";
      $namefile='firma_cli_proy_'.$id_proyecto.'_'.$id_limpieza.'_'.$tipo_act.'.txt';
    }else if($tipo==22){
      $col="firma_cliente2";
      $namefile='firma_cli2_proy_'.$id_proyecto.'_'.$id_limpieza.'_'.$tipo_act.'.txt';
    }
    file_put_contents('uploads/firma/'.$namefile,$firma);

    if($id>0){
      $this->ModeloGeneral->updateCatalogo(array($col=>$namefile),"id",$id,'ordenes_semanal');
    }
    echo $namefile;
  }

  public function nvaPlantilla(){
    $this->load->view('ordenes/plantilla_excel');
  }

  function guardar_actividades_malla(){ 
    $id_proy=$_POST['id_proy'];
    $id_cli=$_POST['id_cli'];

    $configUpload['upload_path'] = FCPATH.'fileExcel/';
    $configUpload['allowed_types'] = 'xls|xlsx|csv';

    $this->load->library('upload', $configUpload);
    $this->upload->do_upload('inputFile');  
    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
    $file_name = $upload_data['file_name']; //uploded file name
    $extension=$upload_data['file_ext'];    // uploded file extension
    //===========================================
      $objReader= PHPExcel_IOFactory::createReader('CSV'); // For cvs delimitado por comas  
      $objReader->setReadDataOnly(true);          

      $objPHPExcel=$objReader->load(FCPATH.'fileExcel/'.$file_name);      
      $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
      $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                

      for($i=2;$i<=$totalrows;$i++){
        $semana = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
        $turno = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
        $id_supervisor = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
        $ot = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
        $id_limpieza = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
        $fecha = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
        $realiza = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
        $seguimiento = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
        $fecha_final = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
        $observaciones = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();

        $data = array(
          'id_cliente'=>$id_cli,
          'id_proyecto'=>$id_proy,
          'semana' => $semana,
          'turno' => $turno,
          'id_supervisor' => $id_supervisor,
          'ot' => $ot,
          'id_limpieza' => $id_limpieza,
          'tipo_act' => 1,
          'fecha' => date("Y-m-d", strtotime($fecha)), 
          'realiza' => $realiza,
          'seguimiento' => $seguimiento,        
          'fecha_final' => date("Y-m-d", strtotime($fecha_final)),
          'observaciones' => $observaciones,
          'reg'=>date("Y-m-d H:i:s"),
          'id_user_reg'=>$this->idpersonal
        );               
        $id_ord= $this->ModeloGeneral->tabla_inserta('ordenes_semanal',$data);        
      }
      unlink('fileExcel/'.$file_name); //File Deleted After uploading in database . 
      //===========================================
    $output = [];
    echo json_encode($output);
  }

  /* ****************************************************** */
  public function listadoDiario($id_proy,$id_cli=0)
  {
    $data['id_proy']=$id_proy;
    $data['id_cli']=$id_cli;
    //$data["a"]=$this->ModeloGeneral->getselectwhere_n_consulta("ordenes_diario",array("id_cliente"=>$id_cli,"id_proyecto"=>$id_proy,"activo"=>1));
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('ordenes/listado_diario',$data);
    $this->load->view('templates/footer');
    $this->load->view('ordenes/listado_diariojs');
  }

  public function getDataDiario() {
    $fecha = $this->input->post('fecha');
    $id_cli = $this->input->post('id_cli');
    $id_proy = $this->input->post('id_proy');
    $mont = $this->input->post('mont');
    $fecha=date("Y-m-d", strtotime($fecha));
    $pormes="";
    if($mont==1){
      $pormes=date("m", strtotime($fecha));
    }
    $listado=$this->ModeloProyectos->getActividadesDiario($fecha,$id_cli,$id_proy,$pormes);
    $json_data = array("data" => $listado);
    echo json_encode($json_data);
  }

  public function getDataDiario2() {
    $params = $this->input->post();

    $listado=$this->ModeloProyectos->getActividadesDiario2($params);
    $json_data = array("data" => $listado);
    echo json_encode($json_data);
  }

  public function getHeadMes() {
    $fecha = $this->input->post('fecha');
    $fecha=date("Y-m-d", strtotime($fecha));
    $pormes="";
    $pormes=date("m", strtotime($fecha));
    $anio=date("Y", strtotime($fecha));
    $number = cal_days_in_month(CAL_GREGORIAN, $pormes, $anio);
    //log_message('error', 'number: '.$number);
    $html_head="";
    $html_head.='<tr>
          <th scope="col">#</th>
          <th scope="col">Actividad</th>
          <th scope="col">Turno</th>';
    for ($i=1; $i<=$number; $i++) {
      $html_head.='<th>'.$i.'</th>';              
    }
    $html_head.='</tr>';
    
    echo $html_head;
  }

  public function getActividadesMes() {
    $fecha = $this->input->post('fecha');
    $id_cli = $this->input->post('id_cli');
    $id_proy = $this->input->post('id_proy');

    $fecha=date("Y-m-d", strtotime($fecha));
    $pormes="";
    $pormes=date("m", strtotime($fecha));
    $anio=date("Y", strtotime($fecha));
    $number = cal_days_in_month(CAL_GREGORIAN, $pormes, $anio); // 31
    //log_message('error', 'number: '.$number);
    
    $html=""; $i=0;
    $listado=$this->ModeloProyectos->getActividadesMes($id_cli,$id_proy,$pormes);
    
    foreach ($listado as $a) {
      $i++;
      $html.="<tr>";
      $html.="<td>".$i."</td>";
      if($a->tipo_act==1)
        $html.="<td>".$a->nombre."</td>";
      else
        $html.="<td>".$a->nombre_crit."</td>";

      if($a->turno=="1"){
        $html.="<td>1ERO</td>";
      }else if($a->turno=="2"){
        $html.="<td>2ERO</td>";
      }else{
        $html.="<td>3ERO</td>";
      }
      //log_message('error', 'dia: '.intval($a->dia));
      //log_message('error', 'i: '.$i);
      /*if(intval($a->dia)==$i){
        $html.="<td>".$a->estatus."</td>";
      }*/
      /*if($a->estatus==1)
        $style="background-color:green";
      else if($a->estatus==2)
        $style="background-color:yellow";
      else if($a->estatus==3)
        $style="background-color:red";*/

      $title="";
      if($a->estatus=="1"){ //cancelado
        $style='background-color:red';
        $sta="X";
        $title=$a->causas;
      }else if($a->estatus=="2"){ //programado
        $style='background-color:blue';
        $sta="P";
        $title=$a->causas;
      }else if($a->estatus=="3"){ //ok
        $style='background-color:green';
        $sta="1";
        $title=$a->causas;
      }else if($a->estatus=="4"){ //reporgramado
        $style='background-color:yellow';
        $sta="-";
        $title=$a->causas;
      }

      for ($j=1; $j<=intval($a->dia); $j++) { 
        if($j<intval($a->dia))
          $html.="<td></td>";
        else
          $html.="<td><p type='button' align='center' title='".$title."' style='font-weight: bold; min-width:20px; color:white; ".$style."'>".$sta."</p></td>";
      }
      for ($k=intval($a->dia); $k<$number; $k++) { 
          $html.="<td></td>";
      }

      $html.="/<tr>";
    }
    
    echo $html;
  }

  public function registroDiario($id_proy,$id_cli=0)
  {
    $data['id_proy']=$id_proy;
    $data['id_cli']=$id_cli;
    $data['tittle']='Registro diario de'; 
    $data["act"]=$this->ModeloProyectos->getActividadesLimpieza($id_proy);
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('ordenes/form_diario',$data);
    $this->load->view('templates/footer');
    $this->load->view('ordenes/form_diariojs');
  }

  public function editaEstatus(){
    $id = $this->input->post('id');
    $estatus = $this->input->post('estatus');
    $causas = $this->input->post('causas');
    $data = array('estatus' => $estatus,'causas' => $causas);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"ordenes_diario");
    echo $resul;
  }

  function submitEmpleados(){
    $params = $this->input->post();
    $id = $params['id_proyecto'];
    unset($params['id_proyecto']);
    $array_emp = $params['array_emp'];
    unset($params['array_emp']);

    $DATAc = json_decode($array_emp);  
    for ($i=0;$i<count($DATAc);$i++) {
      $data = array(
        'id_proyecto'=>$id,
        'id_empleado'=>$DATAc[$i]->id_empleado,
        'id_orden'=>$DATAc[$i]->id_orden
      );

      if($DATAc[$i]->id==0){ 
        $this->ModeloGeneral->tabla_inserta('ordenes_empleado',$data);
      }else{
        unset($data["id"]);
        unset($data["id_proyecto"]);
        $this->ModeloGeneral->updateCatalogo_value($data,array('id'=>$DATAc[$i]->id),'ordenes_empleado');
      }
    }
  }

  public function deleteEmpleado(){
    $id = $this->input->post('id');
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,'ordenes_empleado');
    echo $resul;
  }

  public function cambiaFiltro()
  {
    $semana = $this->input->post('semana');
    $id_act = $this->input->post('id_act');
    //log_message('error', 'semana: '.$semana);
    $this->session->set_userdata('semana_orden',$semana);
    $this->session->set_userdata('activ_orden',$id_act);
  }

  public function delete_select(){
    $id = $this->input->post('ordenes');
    $DATAa = json_decode($id);
    for ($i = 0; $i < count($DATAa); $i++) {
      $this->ModeloGeneral->updateCatalogo(array("estatus"=>0),'id',$DATAa[$i]->id_ord,'ordenes_semanal'); 
    }
  }

}
