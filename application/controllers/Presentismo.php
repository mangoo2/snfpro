<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Presentismo extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('ModeloGeneral');
    $this->load->model('ModeloProyectos');
    if(!$this->session->userdata('logeado')) {
      redirect('/Login');
    }else{
      $this->perfilid = $this->session->userdata('perfilid_tz');
      $this->idpersonal = $this->session->userdata('idpersonal_tz');
      //ira el permiso del modulo
    }
    date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
  }

  public function index()
  {

  }

  public function listado($id=0,$id_cli=0)
  {
    $data["id_proy"]=$id;
    $data["id_cli"]=$id_cli;
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('presentismo/index',$data);
    $this->load->view('templates/footer');
    $this->load->view('presentismo/indexjs');
  }

  public function registro($id=0,$id_proy,$id_cli=0)
  {
    $data['id_proy']=$id_proy;
    $data['id_cli']=$id_cli;
    $data['tittle']='Registro de'; 

    $data["emp"]=$this->ModeloProyectos->getProyectoEmpleados($id_proy,0);
    if($id>0){
      $data['tittle']='Edición de'; 
      $data["a"]=$this->ModeloGeneral->getselectwhererow2("incidencia_pers_proy",array("id"=>$id));
    }
    $this->load->view('templates/header');
    $this->load->view('templates/navbar');
    $this->load->view('presentismo/form',$data);
    $this->load->view('templates/footer');
    $this->load->view('presentismo/formjs');
  }

  public function getDataTable(){
    $params = $this->input->post();
    $getdata = $this->ModeloProyectos->getDataPresentismo($params);
    $totaldata= $this->ModeloProyectos->getDataPresentismo_total($params); 
    $json_data = array(
        "draw"            => intval( $params['draw'] ),   
        "recordsTotal"    => intval($totaldata),  
        "recordsFiltered" => intval($totaldata),
        "data"            => $getdata->result(),
        "query"           =>$this->db->last_query()   
    );
    echo json_encode($json_data);
  }

  public function submit(){
    $datos = $this->input->post();
    $id=$datos['id'];
    $tipo=$datos["tipo_incidencia"];
    unset($datos["tipo"]);
    if($id>0){
      unset($datos["id_proyecto"]);
      $this->ModeloGeneral->updateCatalogo($datos,'id',$id,"incidencia_pers_proy");
      $id_reg=$id;
      if($tipo=="5"){ //baja 
        $this->ModeloGeneral->updateCatalogo(array("estatus"=>0),'personalId',$datos["id_personal"],"personal");
      }
    }else{
      $datos['id_user_reg']=$this->idpersonal;
      $datos['reg']=$this->fechahoy;

      $begin = new DateTime($datos["fecha"]);
      $end = new DateTime($datos["fecha_fin"]);
      $end = $end->modify( '+1 day' );
      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval ,$end);

      foreach($daterange as $date){
        $datos["fecha"]=$date->format("Y-m-d");
        $datos["fecha_fin"]=$date->format("Y-m-d");
        //log_message('error', 'fecha de diario : '.$date->format("Y-m-d"));
        $id_reg=$this->ModeloGeneral->tabla_inserta("incidencia_pers_proy",$datos);
      }      
      if($tipo=="5"){ //baja
        $this->ModeloGeneral->updateCatalogo(array("estatus"=>0),'personalId',$datos["id_personal"],"personal");
      }
    }  
    echo $id_reg;
  }

  public function delete(){
    $id = $this->input->post('id');
    $get_inci=$this->ModeloGeneral->getselectwhererow2("incidencia_pers_proy",array("id"=>$id));
    if($get_inci->tipo_incidencia=="5"){
      $this->ModeloGeneral->updateCatalogo(array("estatus"=>1),'personalId',$get_inci->id_personal,"personal");
    }
    $data = array('estatus' => 0);
    $resul = $this->ModeloGeneral->updateCatalogo($data,'id',$id,"incidencia_pers_proy");
    echo $resul;
  }

  function excel_incidencias($id,$tipo)
  {
    $data['get_result'] = $this->ModeloProyectos->get_incidencia($id,$tipo);
    $this->load->view('presentismo/excel_incidencias',$data);
  }

}
