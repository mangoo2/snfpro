<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModelReportes');
        $this->load->model('Modeloactividadesrealizadas');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid_tz');
            $this->idpersonal=$this->session->userdata('idpersonal_tz');
            $this->id_cliente=$this->session->userdata('usuarioid_tz');
            $this->tipo=$this->session->userdata('tipo');
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio');
        $this->load->view('templates/footer');
        /*unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();*/
	}

    function limpieza($id_proy){
        $data['id_proy']=$id_proy;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio/limpieza',$data);
        $this->load->view('templates/footer');
        $this->load->view('proyectos/formjs');
    }

    function orden_trabajo_from($id_proy,$tipo)
    {
        $data["id_proy"]=$id_proy;
        $data["tipo"]=$tipo;
        $data["tipo_txt"]="";
        if($tipo==1){
            $data["tipo_txt"]="Órdenes de trabajo";
        }else if($tipo==2){
            $data["tipo_txt"]="Matriz de Acciones";
        }else if($tipo==3){
            $data["tipo_txt"]="Monitoreo Áreas Críticas";
        }else if($tipo==4){
            $data["tipo_txt"]="Actividades Realizadas";
        }else if($tipo==5){
            $data["tipo_txt"]="Matríz de Versatilidad";
        }else if($tipo==6){
            $data["tipo_txt"]="Presentismo Rotación";
        }
        $data['det_dep']=$this->ModeloGeneral->getselectwhere2('departamentos',array('id_proy'=>$id_proy,'activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio/orden_trabajo',$data);
        $this->load->view('templates/footer');
        $this->load->view('inicio/orden_trabajo_js');
    }

    public function getProyectosCliente(){
        $id_cliente = $this->session->userdata('tipo') == 3 ? $this->session->userdata('clienteid') : $this->session->userdata('usuarioid_tz');
        $id_proy=$this->input->post("id_proy");
        $results=$this->ModelReportes->getProyectos_cliente($id_cliente);
        $html=""; $sel="";
        foreach ($results as $p) {
            if($id_proy==$p->id){ $sel="selected"; } else { $sel=""; }
            //$html.='<option data-nameserv="'.$p->servicio.'" data-ini="'.$p->mes_ini.'" data-fin="'.$p->mes_fin.'" value="'.$p->id.'">'.$p->servicio.' / Inicio: '.$p->fecha_ini.' Fin: '.$p->fecha_fin.'</option>';
            $html.='<option '.$sel.' data-fi="'.$p->fecha_ini.'" data-ff="'.$p->fecha_fin.'" data-anio_ini="'.$p->anio_ini.'" data-anio_fin="'.$p->anio_fin.'" data-nameserv="'.$p->servicio.'" data-ini="'.$p->mes_ini.'" data-fin="'.$p->mes_fin.'" value="'.$p->id.'">'.$p->servicio.' / Inicio: '.$p->fecha_ini.' Fin: '.$p->fecha_fin.'</option>';
        }
        echo $html;
    }

    public function getDataTableReporte(){
        $params = $this->input->post();
        $params["id_cliente"]=$this->id_cliente;
        $datos = $this->ModelReportes->get_table_salidasCliente($params);
        $json_data = array("data" => $datos);
        echo json_encode($json_data);
    }
    function ar_list($id_proy){
        $data['id_proy']=$id_proy;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio/ar_list',$data);
        $this->load->view('templates/footer');
        $this->load->view('inicio/ar_listjs');
    }
    public function ar_listDataTable(){
        $params = $this->input->post();
        $getdata = $this->Modeloactividadesrealizadas->get_result_cli($params);
        $totaldata= $this->Modeloactividadesrealizadas->total_result_cli($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    /* ******************************************** */
    public function guardar_correos(){
        $result=$this->ModeloGeneral->getselectwhere('bitacora_correo','fecha',$this->fechahoy);
        $validar=0;
        $fecha='';
        $id_reg=0;
        foreach ($result as $x){
            $fecha=$x->fecha;
        }
        if($fecha!=$this->fechahoy){   
            $validar=0;
            $data = array('fecha'=>$this->fechahoy);      
            $id_reg=$this->ModeloGeneral->tabla_inserta('bitacora_correo',$data);
        }else{
            $validar=1; 
        }
        $arraydata = array('id_reg'=>$id_reg,'validar'=>$validar);
        echo json_encode($arraydata);
    }

    public function guardar_correos_detalles()
    {
        $id=$this->input->post('id_reg');
        $result=$this->ModeloCatalogos->validar_correos($this->fechahoy); 
        foreach ($result as $x){
            if(intval($x->avance)<=3){
                if(intval($x->dias)>=3){  
                    $arraydata = array('idbitacora_correo'=>$id,'idmatriz_acciones'=>$x->id);
                    $this->ModeloGeneral->tabla_inserta('bitacora_correo_detalles',$arraydata);
                }
            }
        }
    }

    public function enviar_correos_hallazgo()
    {
        $result=$this->ModeloCatalogos->enviar_correos($this->fechahoy);
        foreach ($result as $x){
            $this->sendMail(intval($x->id),intval($x->tipo_hallazgo));
        }
    }

    public function fileExistPDF(){
        $path = $this->input->post('path');
        //$url=$_SERVER['DOCUMENT_ROOT']."/snfpro/".$path;
        $url=$_SERVER['DOCUMENT_ROOT']."/".$path; //server
        if($path!=""){
            if(file_exists($url)){
                echo "exist";
                //log_message('error', 'exist: '.$url);
            }
            else{
                echo "no exist";
                //log_message('error', 'no exist: '.$url);   
            }
        }else{
            echo "no exist";
        }
    }

    public function sendMail($id,$tipo){ 
        $result=$this->ModeloGeneral->getHallazgoCliente(array("ma.id"=>$id),$tipo);
        $data["hallazgo"]="";
        $data["fecha_compromiso"]="";
        $correo="";
    
        foreach ($result as $item) {
            $data["hallazgo"]=$item->hallazgo;
            $data["fecha_compromiso"]=$item->fecha_compromiso;
            //$correo=$item->correo;

            if($tipo==1){
                $correo=$item->correo;
            }else{
                $correo = $this->ModeloGeneral->getCorreosCliente($id);
                array_push($correo, $item->correo);
            }
        }
        //log_message('error', 'correo inicio: ' . json_encode($correo));
        $this->load->library('email');
        $config = array();
        $config['protocol'] = 'smtp';
        /*$config['smtp_host'] = 'snfpro.sicoi.net';                     
        $config['smtp_user'] = 'hallazgos@snfpro.sicoi.net';
        $mail_dom = 'hallazgos@snfpro.sicoi.net';
        $config['smtp_pass'] = '(JSp*%TIi{8S';*/

        $config['smtp_host'] = 'mail.snf.app';                     
        $config['smtp_user'] = 'hallazgos@snf.app';
        $mail_dom = 'hallazgos@snf.app';
        $config['smtp_pass'] = 'f.Rj5eA[,8$l';

        $config['smtp_port'] = 465;
        $config["smtp_crypto"] = 'ssl';
        //$config['newline']   = "\r\n";
        //$config["crlf"] = "\r\n";
        $config['mailtype'] = "html";
        $config['charset'] = "utf-8";
        $config['wordwrap'] = TRUE;
        $config['validate'] = true;

        $this->email->initialize($config);
        $this->email->from($mail_dom);
        $this->email->to($correo);
        if($this->session->userdata("correo")!=""){
            //$this->email->cc($this->session->userdata("correo"));
        }
        $this->email->subject('Recordatorio de hallazgo');
        $msj=$this->load->view('matrizAcciones/mail', $data,true);
        $this->email->message($msj);
        $this->email->send();

        $this->ModeloGeneral->updateCatalogo(array("notificado"=>1,"fecha_notifica"=>$this->fechahoy),'id',$id,"matriz_acciones");
    }

}