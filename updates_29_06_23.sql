ALTER TABLE `incidencia_pers_proy` ADD `fecha_fin` DATE NOT NULL AFTER `fecha`;
ALTER TABLE `grafica_monitoreo` ADD `id_act` INT NOT NULL AFTER `anio`;

/* ****************12-07-23********************* */

ALTER TABLE `monitoreo_actividad` ADD `frecuencia_tipo` VARCHAR(1) NOT NULL AFTER `frecuencia`;

/* ******************18-07-23************************** */
ALTER TABLE `graficas_limpieza` CHANGE `tipo` `tipo` VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT '1=turno, 2=mes, 3=anual';

ALTER TABLE `matriz_acciones` CHANGE `hallazgo` `hallazgo` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL, CHANGE `falla` `falla` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL, CHANGE `acciones` `acciones` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL, CHANGE `afectacion` `afectacion` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;

/* *********************01-09-23*********************************/
ALTER TABLE `personal` ADD `genero` VARCHAR(1) NOT NULL COMMENT '1=hombre,2=mujer' AFTER `correo`;