ALTER TABLE `actividades_realizadas` ADD `reg_file1` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `file1`;
ALTER TABLE `actividades_realizadas` ADD `reg_file2` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `file2`;
ALTER TABLE `actividades_realizadas` ADD `reg_file3` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `file3`;

ALTER TABLE `empleados_proyecto` ADD `fecha_asignacion` DATE NOT NULL AFTER `tipo_empleado`;

CREATE TABLE `snf_pro`.`clientes_contactos` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `clienteId` INT(11) NOT NULL , `contacto` VARCHAR(255) NOT NULL , `telefono` VARCHAR(30) NOT NULL , `correo` VARCHAR(255) NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
------------------------------------------------

CREATE TABLE `snf_pro`.`clientes_clientes` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `clienteId` INT(11) NOT NULL , `contacto` VARCHAR(255) NOT NULL , `correo` VARCHAR(255) NOT NULL , `contrasena` VARCHAR(100) NOT NULL , `reg` DATETIME NOT NULL , `activo` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
